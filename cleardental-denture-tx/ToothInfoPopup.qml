// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

Popup {
    id: toothInfo


    background: Rectangle {
        anchors.fill: parent
        color: "white"
        opacity: .95
        radius: 10
    }

    property int popNumb: 0
    property var buccalPockets:["_","_","_"]
    property var buccalRec:["_","_","_"]
    property var lingualPockets:["_","_","_"]
    property var lingualRec:["_","_","_"]

    property var txPlanItems: []



    function runPop(toothNumb,getTxItems) {
        popNumb = toothNumb;
        txPlanItems = getTxItems;
        perioChartSettings.category = toothNumb
        var fullBuccalPockets =perioChartSettings.value("BuccalPockets","_,_,_");
        if(fullBuccalPockets !== "WNL" ) {
            buccalPockets = fullBuccalPockets.split(",");
        }
        else {
            buccalPockets = ["😊","😊","😊"];
        }
        var fullBuccalRec = perioChartSettings.value("BuccalRecession","_,_,_");
        buccalRec = fullBuccalRec.split(",");

        var fullLingualPockets = perioChartSettings.value("LingualPockets","_,_,_");
        if(fullLingualPockets !== "WNL") {
            lingualPockets = fullLingualPockets.split(",");
        }
        else {
            lingualPockets = ["😊","😊","😊"];
        }

        var fullLingualRec = perioChartSettings.value("LingualRecession","_,_,_");
        lingualRec = fullLingualRec.split(",");

        open();
    }

    function isAlreadyScheduledForExt(getTooth) {
        if(rootWin.selectedTxPlan === rootWin.fakeTxPlanName) {
            return;
        }

        var returnMe = false;
        var txPlanObj = rootWin.treatmentPlansObj[rootWin.selectedTxPlan];
        var phaseNames = Object.keys(txPlanObj);
        for(var phasei=0;phasei<phaseNames.length;phasei++) {
            var phaseName = phaseNames[phasei];
            var phaseList = txPlanObj[phaseName];
            for(var txi =0; txi < phaseList.length; txi++) {
                var txObj =  phaseList[txi];
                if(("Tooth" in txObj) &&
                        (txObj["Tooth"] === getTooth) &&
                        (txObj["ProcedureName"] === "Extraction")) {
                    return true;
                }
            }
        }
        return returnMe;
    }


    CDTranslucentPane {
        backMaterialColor: Material.Grey
        ColumnLayout {
            Label {
                text: "Tooth #" + toothInfo.popNumb
                font.pointSize: 24
            }
            Label {
                text: "Periodontal Values"
                font.pointSize: 16
                font.underline: true
            }

            Label {
                Layout.fillWidth: true
                text: "Buccal"
                font.bold: true
                horizontalAlignment: Text.AlignHCenter
            }

            GridLayout {
                columns: 4
                Label {
                    text: "Pocket Depths"
                }

                Repeater {
                    model: 3
                    CDPerioLabel {
                        perioVal: toothInfo.buccalPockets[index]
                    }
                }

                Label {
                    text: "Recession"
                }

                Repeater {
                    model: 3
                    CDPerioLabel {
                        perioVal: toothInfo.buccalRec[index]
                    }
                }
            }

            GridLayout {
                columns: 4
                Label {
                    text: "Pocket Depths"
                }

                Repeater {
                    model: 3
                    CDPerioLabel {
                        perioVal: toothInfo.lingualPockets[index]
                    }
                }

                Label {
                    text: "Recession"
                }

                Repeater {
                    model: 3
                    CDPerioLabel {
                        perioVal: toothInfo.lingualRec[index]
                    }
                }
            }
            Label {
                Layout.fillWidth: true
                text: "Lingual"
                font.bold: true
                horizontalAlignment: Text.AlignHCenter
            }

            MenuSeparator {
                Layout.fillWidth: true
            }

            Label {
                text: "Treatment Plan"
                font.pointSize: 16
                font.underline: true
            }

            Repeater {
                model: txPlanItems.length
                Label {
                    text: txPlanItems[index]
                }
            }

            Label {
                text: "Nothing treatment planned"
                visible: txPlanItems.length === 0
            }

            Button {
                text: "Extract this tooth"
                highlighted: true
                Material.accent: Material.Red
                visible: !isAlreadyScheduledForExt(popNumb)
                onClicked: {
                    var unphList = rootWin.treatmentPlansObj[rootWin.selectedTxPlan]
                            ["Unphased"];
                    var addExt = {
                        Tooth: popNumb,
                        ProcedureName: "Extraction" ,
                        ExtractionType:  "Regular",
                        DCode:  "D7140",
                        BasePrice: pricesSettings.value("D7140",0)
                    };
                    unphList.push(addExt);

                    var realTxPlanName = rootWin.selectedTxPlan
                    rootWin.selectedTxPlan = rootWin.fakeTxPlanName;
                    rootWin.selectedTxPlan=realTxPlanName;

                    toothInfo.close();
                }
            }
        }
    }



}
