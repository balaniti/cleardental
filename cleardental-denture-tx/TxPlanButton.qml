// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1
import QtGraphicalEffects 1.14

Button {
    id: control

    property var missingColor: Material.color(Material.LightBlue)
    property int toothNumb: 0
    property bool amIMissing: true
    property string currentTxPlan: rootWin.selectedTxPlan
    property var myToothTxPlan: []
    property bool thereisAnRPD: false

    onCurrentTxPlanChanged: {
        myToothTxPlan = [];
        extractLabel.visible = false;
        crownIconLabel.visible = false;
        implantIconLabel.visible = false;
        toothIconLabel.visible = false;
        thereisAnRPD= false;

        if(rootWin.selectedTxPlan === rootWin.fakeTxPlanName) {
            return;
        }

        var txPlanObj = rootWin.treatmentPlansObj[currentTxPlan];
        var phaseNames = Object.keys(txPlanObj);
        for(var phasei=0;phasei<phaseNames.length;phasei++) {
            var phaseName = phaseNames[phasei];
            var phaseList = txPlanObj[phaseName];
            for(var txi =0; txi < phaseList.length; txi++) {
                reviewTxItem(phaseList[txi]);
            }
        }
    }

    function reviewTxItem(txObj) {
        //First check to see if the is a removable for this arch
        var isMax = toothNumb < 17;
        var thirdMolars = [1,16,17,32];
        if(!thirdMolars.includes(toothNumb)) {
            if(txObj["ProcedureName"] === "Complete Denture") {
                if((isMax && (txObj["Arch"] === "UA") ) ||
                        ( (!isMax) && (txObj["Arch"] === "LA"))) {
                    toothIconLabel.visible = true;
                }
            } else if(txObj["ProcedureName"] === "Partial Denture") {
                if((isMax && (txObj["Arch"] === "UA") ) ||
                        ( (!isMax) && (txObj["Arch"] === "LA"))) {
                    if(amIMissing || extractLabel.visible) {
                         //missing teeth get RPD teeth
                        toothIconLabel.visible = true;
                    }
                    thereisAnRPD = true;
                }
            }
        }

        //Now the the fixed treatments
        if(!("Tooth" in txObj) ||
                txObj["Tooth"] !== toothNumb) {
            return;
        }

        //This now applies to this button
        myToothTxPlan.push(txObj["ProcedureName"]);

        if(txObj["ProcedureName"] === "Extraction") {
            extractLabel.visible = true;
            if(thereisAnRPD) {
                toothIconLabel.visible = true;
            }
        }

        if((txObj["ProcedureName"] === "Crown") ||
                (txObj["ProcedureName"] === "Abutment Crown") ||
                (txObj["ProcedureName"] === "Pontic Crown") ||
                (txObj["ProcedureName"] === "Implant Crown")){
            crownIconLabel.visible = true;
        }

        if(txObj["ProcedureName"] === "Implant Placement") {
            implantIconLabel.visible = true;
        }

    }

    function combineVals(a,b) {
        if(a === "_") {
            a = 0;
        } else {
            a = parseInt(a);
        }
        if(b==="_") {
            b=0;
        } else {
            b = parseInt(b);
        }

        return a+b;
    }

    function getAccentColor() {
        var biggestNumb = 1;

        perioChartSettings.category = toothNumb;
        var buccalPockets = perioChartSettings.value("BuccalPockets","_,_,_");
        if(buccalPockets !== "WNL") {
            var buccalPocketArray = buccalPockets.split(",");
            var buccalRec = perioChartSettings.value("BuccalRecession","_,_,_");
            var buccalRecArray = buccalRec.split(",");
            for(var b=0;b<buccalRecArray.length;b++) {
                var sum = combineVals(buccalPocketArray[b],buccalRecArray[b]);
                biggestNumb = Math.max(biggestNumb,sum);
            }
        }

        var lingualPockets = perioChartSettings.value("LingualPockets","_,_,_");
        if(lingualPockets !== "WNL") {
            var lingualPocketArray = lingualPockets.split(",");
            var lingualRec = perioChartSettings.value("LingualRecession","_,_,_");
            var lingualRecArray = lingualRec.split(",");
            for(var l=0;l<lingualPocketArray.length;l++) {
                sum = combineVals(lingualPocketArray[l],lingualRecArray[l]);
                biggestNumb = Math.max(biggestNumb,sum);
            }
        }

        if(biggestNumb <= 3) {
            return Material.Green;
        }
        else if(biggestNumb <= 6) {
            return Material.Orange;
        }
        else {
            return Material.Red;
        }
    }

    function handleButtonPress(x,y) {
        tInfoPop.close();
        txPlanPop.close();
        toothNumb = parseInt(toothNumb);
        if(amIMissing || extractLabel.visible) {
            txPlanPop.runPop(toothNumb);
        }
        else {
            tInfoPop.runPop(toothNumb,myToothTxPlan);
        }
    }

    function setMissing() {
        amIMissing = false;
        var info = hardTissueChart.value(toothNumb,"");
        var parts = info.split("|");
        for(var i=0;i<parts.length;i++) {
            if(parts[i].trim().startsWith("Missing")) {
                amIMissing = true;
                return;
            }
        }
    }

    contentItem: Label {
        text: toothNumb
        font.pointSize: 16
        font.bold: true
        color: amIMissing ?
                   Material.foreground :
                   Material.color(getAccentColor(control.text))
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }

    background: Rectangle {
        id: backRect
        border.color: amIMissing ?
                          missingColor :  Material.color(getAccentColor(text))
        color: amIMissing ? missingColor : Material.background
        radius: 5

        Label {
            id: extractLabel
            font.pointSize: 64
            text: "X"
            anchors.centerIn: parent
            color: Material.color(Material.Red)
            opacity: .3
            visible: false
        }

        Label {
            id: toothIconLabel
            font.pointSize: 42
            text: "\uD83E\uDDB7"
            anchors.centerIn: parent
            opacity: .3
            rotation: toothNumb > 16 ? 0 : 180
            visible: false
        }
        Label {
            id: crownIconLabel
            text: "\uD83D\uDC51"
            font.pointSize: 42
            anchors.centerIn: parent
            opacity: .3
            visible: false
        }
        Label {
            id: bridgeIconLabel
            text: "\uD83C\uDF09"
            font.pointSize: 42
            anchors.centerIn: parent
            opacity: .7
            visible: false
        }
        Label {
            id: implantIconLabel
            text: "\uD83D\uDD29"
            font.pointSize: 42
            anchors.centerIn: parent
            opacity: .7
            visible: false
        }
    }

    layer.enabled: true
    layer.effect: DropShadow {
        color: backRect.border.color
        transparentBorder: true
        horizontalOffset: 0
        verticalOffset: 0
        spread: .5

    }

    onClicked: {
        handleButtonPress(x,y);
    }

    Component.onCompleted: {
        setMissing();
    }

}
