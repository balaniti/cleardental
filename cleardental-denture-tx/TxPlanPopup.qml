// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

Popup {
    id: toothInfo

    property int popNumb: 0

    background: Rectangle {
        anchors.fill: parent
        color: "white"
        opacity: .95
        radius: 10
    }

    function runPop(toothNumb) {
        popNumb = toothNumb;
        open();
    }

    function isMissing(getTooth) {
        if((getTooth < 1) || (getTooth > 32)) {
            return true;
        }
        var returnMe = false;
        var info = hardTissueChart.value(getTooth,"");
        var parts = info.split("|");
        for(var i=0;i<parts.length;i++) {
            if(parts[i].trim().startsWith("Missing")) {
                return true;
            }
        }

        return returnMe;
    }

    function isAlreadyScheduledForExt(getTooth) {
        var returnMe = false;
        var txPlanObj = rootWin.treatmentPlansObj[rootWin.selectedTxPlan];
        var phaseNames = Object.keys(txPlanObj);
        for(var phasei=0;phasei<phaseNames.length;phasei++) {
            var phaseName = phaseNames[phasei];
            var phaseList = txPlanObj[phaseName];
            for(var txi =0; txi < phaseList.length; txi++) {
                var txObj =  phaseList[txi];
                if(("Tooth" in txObj) &&
                        (txObj["Tooth"] === getTooth) &&
                        (txObj["ProcedureName"] === "Extraction")) {
                    return true;
                }
            }
        }
        return returnMe;
    }

    function findBridgeTeeth() {
        var abutment = [];
        var pontic = [];

        pontic.push(popNumb);

        var i=popNumb;
        var goOn = true;
        var thirdMolars = [1,16,17,32];


        for(i=popNumb+1;goOn;i++ ) {
            if(isMissing(i) || isAlreadyScheduledForExt(i)) {
                pontic.push(i);
            }
            else {
                abutment.push(i);
                goOn = false;
            }
            if(thirdMolars.includes(i)) {
                goOn = false;
            }
        }

        goOn = true;
        for(i=popNumb-1;goOn;i-- ) {
            if(isMissing(i) || isAlreadyScheduledForExt(i)) {
                pontic.push(i);
            }
            else {
                abutment.push(i);
                goOn = false;
            }
            if(thirdMolars.includes(i)) {
                goOn = false;
            }
        }
        return [abutment,pontic];

    }

    CDTranslucentPane {
        backMaterialColor: Material.BlueGrey
        ColumnLayout {
            Label {
                text: "Treatment Plan for #" + popNumb
                font.pointSize: 24
                font.underline: true
            }

            RadioButton {
                id: nothingRadio
                text: "Nothing"
                checked: true
            }

            RadioButton {
                id: bridgeRadio
                text: "Bridge"
            }

            RadioButton {
                id: implantRadio
                text: "Implant"
            }

            Pane {
                visible: !nothingRadio.checked

                CrownTypeColumn {
                    id: crownTypeCol

                }
            }

            Button {
                text: "Done"
                highlighted: true
                Material.accent: Material.Cyan
                onClicked: {
                    var phases = rootWin.treatmentPlansObj[rootWin.selectedTxPlan];
                    if(!("Unphased" in phases)) {
                        phases["Unphased"] = [];
                    }

                    if(bridgeRadio.checked) {
                        var crownType = crownTypeCol.getCrownTypeName();
                        var crownTeeth = findBridgeTeeth();
                        var abutmentArray = crownTeeth[0];
                        var ponticArray = crownTeeth[1];
                        var pontI=0;
                        var abutI=0;
                        var addPontic = ({});
                        var addAbutment = ({});
                        if((crownType === "Zirconia") ||
                                (crownType === "Lithium Disilicate")) {
                            for(pontI=0;pontI<ponticArray.length;pontI++) {
                                addPontic = {
                                    Tooth: ponticArray[pontI],
                                    ProcedureName: "Pontic Crown",
                                    Material: crownType,
                                    DCode: "D6245",
                                    BasePrice: pricesSettings.value("D6245",0)
                                };
                                phases["Unphased"].push(addPontic);
                            }
                            for(abutI=0;abutI<abutmentArray.length;abutI++) {
                                addAbutment = {
                                    Tooth: abutmentArray[abutI],
                                    ProcedureName: "Abutment Crown",
                                    Material: crownType,
                                    DCode: "D6740",
                                    BasePrice: pricesSettings.value("D6740",0)
                                }
                                phases["Unphased"].push(addAbutment);
                            }
                        }
                        else if(crownType === "Porcelain Fused to Metal") {
                            for(pontI=0;pontI<ponticArray.length;pontI++) {
                                addPontic = {
                                    Tooth: ponticArray[pontI],
                                    ProcedureName: "Pontic Crown",
                                    Material: crownType,
                                    DCode: "D6240",
                                    BasePrice: pricesSettings.value("D6240",0)
                                };
                                phases["Unphased"].push(addPontic);
                            }
                            for(abutI=0;abutI<abutmentArray.length;abutI++) {
                                addAbutment = {
                                    Tooth: abutmentArray[abutI],
                                    ProcedureName: "Abutment Crown",
                                    Material: crownType,
                                    DCode: "D6750",
                                    BasePrice: pricesSettings.value("D6750",0)
                                }
                                phases["Unphased"].push(addAbutment);
                            }
                        }
                        else if(crownType === "Full Cast Gold") {
                            for(pontI=0;pontI<ponticArray.length;pontI++) {
                                addPontic = {
                                    Tooth: ponticArray[pontI],
                                    ProcedureName: "Pontic Crown",
                                    Material: crownType,
                                    DCode: "D6210",
                                    BasePrice: pricesSettings.value("D6210",0)
                                };
                                phases["Unphased"].push(addPontic);
                            }
                            for(abutI=0;abutI<abutmentArray.length;abutI++) {
                                addAbutment = {
                                    Tooth: abutmentArray[abutI],
                                    ProcedureName: "Abutment Crown",
                                    Material: crownType,
                                    DCode: "D6750",
                                    BasePrice: pricesSettings.value("D6750",0)
                                }
                                phases["Unphased"].push(addAbutment);
                            }
                        }
                    }

                    else if(implantRadio.checked) {
                        var placeImp = {
                            Tooth: popNumb,
                            ProcedureName: "Implant Placement",
                            DCode: "D6010",
                            BasePrice: pricesSettings.value("D6010",0)
                        }
                        phases["Unphased"].push(placeImp);

                        var addImpCrwn = ({});
                        crownType = crownTypeCol.getCrownTypeName();
                        if((crownType === "Zirconia") ||
                                (crownType === "Lithium Disilicate")) {
                            addImpCrwn = {
                                Tooth: popNumb,
                                ProcedureName: "Implant Crown",
                                Material: crownType,
                                DCode: "D6058",
                                BasePrice: pricesSettings.value("D6058",0)
                            }
                            phases["Unphased"].push(addImpCrwn);
                        }
                        else if(crownType === "Porcelain Fused to Metal") {
                            addImpCrwn = {
                                Tooth: popNumb,
                                ProcedureName: "Implant Crown",
                                Material: crownType,
                                DCode: "D6059",
                                BasePrice: pricesSettings.value("D6059",0)
                            }
                            phases["Unphased"].push(addImpCrwn);
                        }
                        else if(crownType === "Full Cast Gold") {
                            addImpCrwn = {
                                Tooth: popNumb,
                                ProcedureName: "Implant Crown",
                                Material: crownType,
                                DCode: "D6062",
                                BasePrice: pricesSettings.value("D6062",0)
                            }
                            phases["Unphased"].push(addImpCrwn);
                        }
                    }

                    //Force the reload of all the buttons
                    var actTxPlanName = rootWin.selectedTxPlan;
                    rootWin.selectedTxPlan = rootWin.fakeTxPlanName;
                    rootWin.selectedTxPlan = actTxPlanName;

                    toothInfo.close();
                }
            }
        }

    }


}
