// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtGraphicalEffects 1.12
import QtQuick.Window 2.10

CDAppWindow {
    id: rootWin
    property var procedure_OBJ: JSON.parse(PROCEDURE_JSON)
    title:  comFuns.makeTxItemString(procedure_OBJ) + " [ID: " + PATIENT_FILE_NAME + "]"


    CDCommonFunctions {
        id: comFuns
    }

    header: CDPatientToolBar {
        headerText: comFuns.makeTxItemString(procedure_OBJ)
        ToolButton {
             icon.name: "application-menu"
             icon.width: 64
             icon.height: 64
             onClicked: drawer.open();
             anchors.left: parent.left
         }
    }

    RowLayout {
        anchors.fill: parent
        anchors.margins: 10
        ColumnLayout {
            CDMedReviewPane{id: medHis; Layout.minimumWidth: 720}
            CDReviewRadiographPane{toothToReview: 0;Layout.fillWidth: true}
        }
        ColumnLayout {
            CDLAPane{id: laPlane; showMarcaine:true; Layout.fillWidth: true}
            CDTxPlanPane{Layout.fillWidth: true}
            CDFinishProcedureButton {
                text: "Finish Procedure"
                Layout.alignment: Qt.AlignRight
                onClicked: {
                    finishDialog.caseNoteString = "Patient presented for " + comFuns.makeTxItemString(procedure_OBJ) +
                            "\n";
                    finishDialog.caseNoteString += laPlane.generateCaseNoteString();
                    finishDialog.txItemsToComplete = [procedure_OBJ]
                    finishDialog.open();
                }
            }
        }
    }

    CDFinishProcedureDialog {
        id: finishDialog
    }
}
