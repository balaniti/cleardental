// Copyright 2022 Clear.Dental; Alex Vernes
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1
import QtQml.Models 2.1

CDTranslucentPane {


    property var scheduleButtons: [];

    Settings {id: apptLoader}

    function getPixelsFromTimeString(timeString) {
        var hour = timeString.split(":")[0];
        var minAndAMPM = timeString.split(":")[1].split(" ");
        var mins = parseInt(minAndAMPM[0]);
        var AMPM = minAndAMPM[1];

        var totalMins =0;

        if(hour === "12") {
            if(AMPM === "AM") {
                totalMins = mins;
            }
            else {
                totalMins = (12*60) + mins;
            }
        }
        else if(AMPM === "AM") {
            totalMins = (60*hour) + mins
        }
        else if(AMPM === "PM") {
            hour = parseInt(hour);
            totalMins = (60*(hour+12)) + mins
        }

        return totalMins * pixelsPerMin;
    }

    function doButtonsOverlap(butA, butB) {
        var returnMe = false;

        if(butA.y < butB.y) {
            returnMe = (butA.y + butA.height) > butB.y;
        } else {
            returnMe = (butB.y + butB.height) > butA.y;
        }

        return returnMe;
    }

    function findInList(butA,butB,overlapLists) {
        var added= false;
        for(var i=0;i<overlapLists.length;i++) {
            var lapList = overlapLists[i];
            var ai = lapList.indexOf(butA);
            var bi = lapList.indexOf(butB);
            if(ai >= 0) {
                lapList.push(butB);
                added = true;
            }
            else if(bi >= 0) {
                lapList.push(butA);
                added = true;
            }
        }
        if(!added) {
            overlapLists.push([butA,butB]);
        }
    }

    function sortButtons(a,b) {
        return a.y-b.y;
    }

    function loadApptButtons() {
        var apps = scheduleDB.getAppointments(selectedDay);
        var i=0;
        var chairWidth = (masterPane.width - timeMargin) / rootWin.selectedDayChairs.length;

        // empties the schedule buttons list
        for(i=0;i<scheduleButtons.length;i++) {
            scheduleButtons[i].destroy();
        }
        scheduleButtons = [];

        var chairMatrix = [];
        for(i=0;i<selectedDayChairs.length;i++) {
            chairMatrix.push([]);
        }


        for(i=0;i<apps.length;i++) {
            apptLoader.fileName = apps[i];
            apptLoader.sync();
            var newButton = appsButton.createObject(currentDayFlickable.contentItem);

            newButton.fileName = apptLoader.fileName;
            newButton.y = getPixelsFromTimeString(apptLoader.value("StartTime"),"12:00 AM");
            newButton.topInset = 0;
            newButton.bottomInset = 0;
            newButton.leftInset = 0;
            newButton.rightInset = 0;

            var duration = parseInt(apptLoader.value("Duration", 15));
            newButton.height = pixelsPerMin * duration;
            newButton.width = chairWidth;
            newButton.duration = duration;
            newButton.startTime = apptLoader.value("StartTime","12:00 AM");

            newButton.comments = apptLoader.value("Comments", "")
            newButton.patientID = apptLoader.value("PatientID", "")
            newButton.procedures = apptLoader.value("Procedures", "")
            newButton.providerID = apptLoader.value("ProviderID", "")
            newButton.status = apptLoader.value("Status", "")


            var chairName = apptLoader.value("Chair","");
            if(chairName === "ALL_CHAIRS") {
                newButton.x = timeMargin;
                newButton.width = masterPane.width - timeMargin;
            }
            else {
                var chairIndex = selectedDayChairs.indexOf(chairName);
                newButton.x = timeMargin + (chairIndex * chairWidth);
                chairMatrix[chairIndex].push(newButton);
            }

            scheduleButtons.push(newButton);
        }


        for(i=0;i<selectedDayChairs.length;i++) { //for each chair
            var chairList = chairMatrix[i];
            var prevButton;
            var overLapLists = [];
            for(var w=0;w<chairList.length;w++) {//for each Appointment in chair
                for(var v=w+1;v<chairList.length;v++) {//look at other appts
                    if(doButtonsOverlap(chairList[w],chairList[v])) {
                        findInList(chairList[w],chairList[v],overLapLists);
                    }
                }
            }

            for(w=0;w<overLapLists.length;w++) {
                var sortedButtons = overLapLists[w].sort(sortButtons);
                for(var g=0;g<sortedButtons.length;g++) {
                    var but = sortedButtons[g];
                    but.width = but.width / sortedButtons.length;
                    but.x += but.width * g;
                }


            }
        }

        currentDayFlickable.contentItem.childAt(100,100);


    }

    /**
      updates the ghost appointment
      chair: string, ex. "Hyrule"
      time: string, ex. "8:00 AM"
      duration: int, ex. 60
        if duration is set to 0, no changes are made to it
      */
    function updateGhostAppt(chair, time, duration) {
        apptLoader.fileName = PATIENT_FILE_NAME;
        apptLoader.sync();

        ghostAppt.fileName = apptLoader.fileName;

        var chairWidth = (masterPane.width - timeMargin) / rootWin.selectedDayChairs.length;
        ghostAppt.width = chairWidth;
        if (duration !== 0) {
            ghostAppt.height = pixelsPerMin * duration;
        }
        ghostAppt.topInset = 0;
        ghostAppt.bottomInset = 0;
        ghostAppt.leftInset = 0;
        ghostAppt.rightInset = 0;

        var chairIndex = selectedDayChairs.indexOf(chair);
        ghostAppt.x = timeMargin + (chairIndex * chairWidth);
        ghostAppt.y = getPixelsFromTimeString(time);
        ghostAppt.z = 1;

        ghostAppt.patientID = apptLoader.value("PatientID", PATIENT_FILE_NAME)
        ghostAppt.providerID = apptLoader.value("ProviderID", "")

    }

    id: masterPane
    backMaterialColor: Material.Grey
    Layout.fillHeight: true
    Layout.fillWidth: true

    // Chair labels
    Item {
        id: chairHeader
//        anchors.top: masterPane.top
        x: timeMargin
        width: masterPane.width - timeMargin
        height: children[0].height
        Repeater {
            model: rootWin.selectedDayChairs.length
            Label {
                id: chairLabel
                text: rootWin.selectedDayChairs[index]
                width: chairHeader.width/rootWin.selectedDayChairs.length
                x: index * width
                horizontalAlignment: Text.AlignHCenter
                font.bold: true
            }
        }
        z: 1024
    }

    Flickable {
        id: currentDayFlickable
        clip: true
        anchors.fill: parent
        contentHeight: 24*60*pixelsPerMin
        contentY: ((dayStartTime-(1*60)) * pixelsPerMin)

        ScrollBar.vertical: ScrollBar {}

        // Shows the times on the side of the screen
        Repeater {
            id: timeRep
            model: 24*10

            function makeTextFromIndex(getIndex) {
                var hourTime = Math.floor(getIndex/6);
                var tenMins = getIndex%6
                var returnMe ="";
                if(hourTime === 0) {
                    returnMe = "12:" + tenMins + "0 AM";
                }
                else if(hourTime === 12) {
                    returnMe = hourTime + ":" + tenMins + "0 PM";
                }
                else if(hourTime < 13) {
                    returnMe = hourTime + ":" + tenMins + "0 AM";
                }
                else {
                    returnMe = (hourTime-12) + ":" + tenMins + "0 PM";
                }

                return returnMe;
            }
            Label {
                id: timeLabel
                x: 0
                y: (pixelsPerMin * index * 10) - (height /2)
                width:100
                horizontalAlignment: Text.AlignRight
                text: timeRep.makeTextFromIndex(index)
                visible: ((showTimeMins == 30 ) && (text.endsWith(":00 AM") ||
                                                   text.endsWith(":00 PM") ||
                                                   text.endsWith(":30 AM") ||
                                                    text.endsWith(":30 PM") )) ||
                         ((showTimeMins == 60) && (text.endsWith(":00 AM") ||
                                                   text.endsWith(":00 PM")  )) ||
                         (showTimeMins == 10)
            }

        }


        // horizontal lines separating chunks of 10 minues
        Repeater {
            id: timeLineRep
            model: 24*10
            Rectangle {
                width: masterPane.width - timeMargin
                height: 1
                color: Material.color(Material.Grey)
                x: timeMargin
                y: pixelsPerMin * index * 10
                opacity: (index%6==0) ? 1 : 0.25
            }
        }

        // separator lines for different chairs
        Repeater {
            id: horzLineRep
            model: selectedDayChairs.length
            Rectangle {
                width: 1
                height: currentDayFlickable.contentHeight
                color: Material.color(Material.Grey)
                x: timeMargin + (((masterPane.width -timeMargin) /
                                  selectedDayChairs.length) * index)
                y: 0
                opacity:  0.25
            }
        }

        // pre-9AM greyout
        Rectangle {
            id: beforeOpenGrey
            property int bounceMargin: 100
            color: Material.color(Material.Grey)
            opacity: .5
            width: masterPane.width - timeMargin
            x: timeMargin
            y: -bounceMargin
            height: (dayStartTime * pixelsPerMin) + bounceMargin
            visible: openThisDay
        }

        // post-5PM greyout
        Rectangle {
            id: afterOpenGrey
            color: Material.color(Material.Grey)
            opacity: .5
            width: masterPane.width - timeMargin
            x: timeMargin
            y: dayEndTime * pixelsPerMin
            height: currentDayFlickable.contentHeight // - y
            visible: openThisDay
        }

        // lunchtime greyout
        Rectangle {
            id: breakGrey
            color: Material.color(Material.Grey)
            opacity: .5
            width: masterPane.width - timeMargin
            x: timeMargin
            y: breakStart * pixelsPerMin
            height: (breakEnd - breakStart) * pixelsPerMin
            visible: openThisDay && haveBreakThisDay
        }

        // greyout if office is closed for this day
//        Rectangle {
//            id: closedForDayGrey

//            color: Material.color(Material.Grey)
//            opacity: .5
//            x: timeMargin
//            width: masterPane.width -timeMargin
//            y: -100
//            height: currentDayFlickable.contentHeight + 100
//            visible: !openThisDay
//        }

        // current time yellow line
        Rectangle {
            id: nowRect
            color: Material.color(Material.Yellow)
            opacity: .5
            x:0
            width: parent.width
            height: 2
            visible: selectedDay.toDateString() ===
                     (new Date()).toDateString()
            y: (((new Date()).getHours() * 60) + (new Date()).getMinutes()) *
               pixelsPerMin

            SequentialAnimation {
                running: true
                loops: Animation.Infinite
                ScriptAction {
                    script: {
                        nowRect.y = (((new Date()).getHours() * 60) +
                                      (new Date()).getMinutes()) *
                                pixelsPerMin
                    }
                }

                PauseAnimation {
                    duration: 1000 * 60
                }
            }
        }

        ScheduleButton {
            id: ghostAppt

            SequentialAnimation {
                PropertyAnimation {
                    target: ghostAppt
                    property: "opacity"
                    from: .2
                    to: 1
                    duration: 2000
                }

                PropertyAnimation {
                    target: ghostAppt
                    property: "opacity"
                    from: 1
                    to: .2
                    duration: 2000
                }

                running: true
                loops: Animation.Infinite
            }
        }

        Component {
            id: appsButton
            ScheduleButton {
            }
        }

        Component {
            id: ghostApptRect
            Rectangle {
            }
        }

        MouseArea {
            anchors.fill: parent
            onPressAndHold: {
                if (mouse.x <= timeMargin) {
                    return;
                }

                var chairWidth = (parent.width - timeMargin) / selectedDayChairs.length;
                var chairIndex = Math.floor((mouse.x - timeMargin) / chairWidth);
                var chairName = selectedDayChairs[chairIndex];
                var apptStartTime = mouse.y/pixelsPerMin;
//                updateGhostAppt(chairName, minutesToTimeString(apptStartTime), 0);
                var apptStartTimeString = minutesToTimeString(apptStartTime);
                setApptDetails(chairName, apptStartTimeString);
            }
        }

        Component.onCompleted: {
            loadApptButtons();
        }
    }
}
