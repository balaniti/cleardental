// Copyright 2022 Clear.Dental; Alex Vernes
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1
import QtQml.Models 2.1


CDTranslucentPane {
    id: masterPane
    backMaterialColor: Material.Green
    Layout.minimumWidth: gridLay.width + 40

    function parseTimeBlock(getTimeBlock, apptStartTime,apptDuration) {
        var returnMe = false;
        var openTimesList=getTimeBlock.split("|");
        var endingMins = apptStartTime + apptDuration;
        for(var i=0;i<openTimesList.length;i++) {
            var schChunk = openTimesList[i].trim();
            var schTimeParts = schChunk.split(" ");
            if(schChunk.startsWith("Open")) { //check before open
                var openTime = parseTime(schTimeParts[1] + " " +
                                         schTimeParts[2]);
                if(apptStartTime < openTime) {
                    return true;
                }
            }
            else if(schChunk.startsWith("Closed")) {
                var closingTime =parseTime(schTimeParts[1] + " " +
                                           schTimeParts[2]);
                if(endingMins > closingTime) {
                    return true;
                }
            }
            else if(schChunk.startsWith("Lunch")) {
                var lunchStart = parseTime(schTimeParts[6] + " "
                                           + schTimeParts[7]);
                var lunchDuration = parseInt(schTimeParts[3]);
                var lunchEnd = lunchStart + lunchDuration;
                if((apptStartTime > lunchStart) && (apptStartTime < lunchEnd)) {
                    return true;
                } else if((endingMins > lunchStart) &&
                          (endingMins < lunchEnd)) {
                    return true;
                }
            }
        }
        return returnMe;
    }

    function checkTime() {
        //first check if the clinic is open or not
        var dateObj = apptDate.myDate;
        var timeString = (startHour.currentIndex + 1) + ":" +
                startMin.displayText + " " + startAMPM.currentText;
        var myStartTime = parseTime(timeString);

        var dayList = ["Sunday","Monday","Tuesday","Wednesday","Thursday",
                       "Friday","Saturday"]
        var dayOfWeek = dateObj.getDay();
        var isOpen = readOpenCloseTimes.value("Open"+dayList[dayOfWeek],"false")
        if( isOpen === "true") {
            var openTimes =
                    readOpenCloseTimes.value(dayList[dayOfWeek]+"Hours","");
            var apptDuration = (durationBox.currentIndex+1) * 15;
            closedTimeWarning.opacity = parseTimeBlock(openTimes,myStartTime,
                                                       apptDuration) ? 1: 0
        }
        else {
            closedTimeWarning.opacity = 1;
        }

        //Assuming we are open, check the chair
        doubleBookWarning.opacity = 0;
        if(!closedTimeWarning.visible) {
            var apps = scheduleDB.getAppointments(dateObj);
            for(var i=0;i<apps.length;i++) {
                apptLoader.fileName = apps[i];
                var apptStartTimeString = apptLoader.value("StartTime",
                                                           "12:00 AM");
                var startTimeMins = parseTime(apptStartTimeString);
                var checkerApptDur = parseInt(apptLoader.value("Duration",15));
                var endTimeMins = startTimeMins+checkerApptDur;
                var checkChair = apptLoader.value("Chair","");
                var myChair = chairBox.displayText;
                if(checkChair === myChair) {
                    var myEndTime = myStartTime + apptDuration;
                    if((myStartTime >= startTimeMins)
                            && (myStartTime < endTimeMins)) {
                        doubleBookWarning.opacity = 1;
                    }
                    else if((myEndTime >= startTimeMins)
                            && (myEndTime < endTimeMins)) {
                        doubleBookWarning.opacity = 1;
                    }
                }
            }
        }
        qrImage.generateScheduleCode();
    }

    function updateGhostChair() {
        rootWin.apptChair = chairBox.currentText;
    }

    function updateGhostTime() {

        rootWin.apptTime = startHour.currentText + ":" + startMin.currentText + " " + startAMPM.currentText;
    }

    function updateGhostDuration() {
        var durString = durationBox.currentText;
        var durList = durString.split(" ");
        var totalMins = 0;

        if (durList.length == 2) { // just minutes or just hours
            if (durList[1] === "minutes") { // just minutes
                totalMins += parseInt(durList[0]);
            } else { // just hours
                totalMins += (parseInt(durList[0]) * 60);
            }
        } else { // minutes and hours
            totalMins += (parseInt(durList[0]) * 60);
            totalMins += parseInt(durList[3]);
        }

        rootWin.apptDuration = totalMins;
    }

    function updateAllGhostFields() {
        updateGhostChair();
        updateGhostDuration();
        updateGhostTime();
    }

    function updateApptDetails(chair, time) {
        chairBox.currentIndex = selectedDayChairs.indexOf(chair);
        var timeList = time.split(":");
        var hour = timeList[0];
        var min = timeList[1].split(" ")[0];
        var AMPM = timeList[1].split(" ")[1];
        startHour.currentIndex = (parseInt(hour) - 1);
        startMin.currentIndex = Math.floor(parseInt(min) / 5);
        if (AMPM === "AM") {
            startAMPM.currentIndex = 0;
        } else {
            startAMPM.currentIndex = 1;
        }
        checkTime();
    }

    ColumnLayout {
        id: newApptDetailsColLayout
        anchors.fill: parent
        Flickable {
            id: flicker
//            anchors.fill: parent
            Layout.fillHeight: true
            Layout.fillWidth: true
//            anchors.margins: 20
            contentWidth: gridLay.implicitWidth
            contentHeight: gridLay.implicitHeight + 50
            clip: true

            flickableDirection: Flickable.VerticalFlick
            ScrollBar.vertical: ScrollBar {}

            Label {
                id: newApptLabel
                Layout.fillWidth: true
                font.pointSize: 24
                text: "Appointment Details"
                font.underline: true
            }

            GridLayout {
                id: gridLay

                anchors.top: newApptLabel.bottom

                columns: 2
                //columnSpacing: 50

                Label {
                    text: "Schedule what"
                    font.bold: true
                }

                ScheduleWhatCol {
                    id: scheduleWhatCol
                }

                Label {
                    text: "Date"
                    font.bold: true
                }

                CDCalendarDatePicker {
                    id: apptDate
                    onMyDateChanged: {
                        rootWin.selectedDay = myDate;
                        checkTime();
                    }
                }

                Label {
                    text: "Time"
                    font.bold: true
                }

                RowLayout {
                    ComboBox {
                        id: startHour
                        model: ["1","2","3","4","5","6","7","8","9","10","11","12"]
                        Layout.preferredWidth: 75
                        currentIndex: 7
                        onCurrentTextChanged: {
                            checkTime();
                            rootWin.requestUpdateGhostAppt();
                        }
                    }

                    Label {
                        text: ":"
                    }

                    ComboBox {
                        id: startMin
                        property var minModel: []
                        Layout.preferredWidth: 75
                        Component.onCompleted:  {
                            for(var i=0;i<60;i+=5) {
                                if(i < 10) {
                                    minModel.push("0"+i);
                                }
                                else {
                                    minModel.push(i);
                                }
                            }
                            model = minModel
                        }
                        onCurrentTextChanged: {
                            checkTime();
                            rootWin.requestUpdateGhostAppt();
                        }
                    }

                    ComboBox {
                        id: startAMPM
                        model: ["AM","PM"]
                        Layout.preferredWidth: 75
                        onCurrentTextChanged: {
                            checkTime();
                            rootWin.requestUpdateGhostAppt();
                        }
                    }
                }

                Label {
                    text: "Duration"
                    font.bold: true
                }

                ComboBox {
                    id: durationBox
                    property var durModel: []
                    Layout.fillWidth: true
                    Component.onCompleted:  {
                        for(var i=15;i<=180;i+=15) {
                            var hours = Math.floor(i / 60);
                            var mins = i%60;
                            if(hours === 0) {
                                durModel.push(mins + " minutes")
                            }
                            else if(hours === 1) {
                                if(mins === 0) {
                                    durModel.push("1 hour")
                                }
                                else {
                                    durModel.push("1 hour and " + mins + " minutes")
                                }
                            }
                            else {
                                if(mins === 0) {
                                    durModel.push(hours + " hours")
                                }
                                else {
                                    durModel.push(hours + " hours and " + mins +
                                                  " minutes")
                                }
                            }
                        }
                        model = durModel
                        currentIndex =1; //30 mins
                    }
                    onCurrentTextChanged: {
                        checkTime();
                        rootWin.requestUpdateGhostAppt();
                    }
                }

                Label {
                    text: "Chair"
                    font.bold: true
                }

                ComboBox {
                    id: chairBox
                    property var chairModel: []
                    Layout.fillWidth: true
                    onCurrentTextChanged: {
                        checkTime();
                        rootWin.requestUpdateGhostAppt();
                    }
                    Component.onCompleted: {
                        var selectedDayChairs = scheduleDB.getChairs(selectedDay)
                        for (var i=0; i < selectedDayChairs.length; i++) {
                            chairModel.push(selectedDayChairs[i]);
                        }
                        model = chairModel;
                    }
                }

                Label {
                    text: "Provider"
                    font.bold: true
                }

                ComboBox {
                    id: providerBox
                    Layout.fillWidth: true

                    property var providerNames: []
                    property var providerIDs: []

                    CDDoctorListManager {id: providerMan}
                    Settings {id: docPrefReader}

                    Component.onCompleted: {
                        var allProvs = providerMan.getListOfProviderFiles();
                        for(var i=0;i<allProvs.length;i++) {
                            docPrefReader.fileName = allProvs[i];

                            var addID = allProvs[i].split("/")[
                                        allProvs[i].split("/").length-2]
                            var addName = docPrefReader.value("FirstName","") + " "
                                    + docPrefReader.value("LastName","") + " (" +
                                    addID + ")"
                            var provType = docPrefReader.value("ProviderType","");
                            if(provType === "General Dentist") {
                                addName = "Dr. " + addName;
                            }
                            providerNames.push(addName);
                            providerIDs.push(addID);
                        }
                        providerBox.model = providerNames;
                    }
                }

                Label {
                    text: "Comment"
                    font.bold: true
                }

                TextField {
                    id: commentField
                    Layout.fillWidth: true
                }

                Label {
                    id: doubleBookWarning
                    Layout.fillWidth: true
                    Layout.columnSpan: 2
                    text: "Warning: There is already an appointment in this" +
                          " chair and time"
                    opacity: 0
                    color: Material.color(Material.Red)
                    Behavior on opacity {
                        PropertyAnimation {
                            duration: 300
                        }
                    }
                }

                Label {
                    id: closedTimeWarning
                    Layout.fillWidth: true
                    Layout.columnSpan: 2
                    text: "Warning: Clinic is closed during this appointment"
                    opacity: 0
                    color: Material.color(Material.Red)
                    Behavior on opacity {
                        PropertyAnimation {
                            duration: 300
                        }
                    }
                }

                CDAddButton {
                    text: "Add Appointment"
                    Layout.alignment: Qt.AlignRight
                    Layout.columnSpan: 2

                    CDGitManager {
                        id: gitMan
                    }

                    onClicked: {
                        var addDate = apptDate.myDate;
                        var addTime = (startHour.currentIndex + 1) + ":" +
                                startMin.currentText + " " + startAMPM.currentText;
                        var addChair = chairBox.displayText;
                        var addPatID = PATIENT_FILE_NAME;
                        var addProcedure = scheduleWhatCol.generateProceduresString();
                        var addProviderID = providerBox.providerIDs[providerBox.currentIndex];
                        var addDuration = (durationBox.currentIndex+1) * 15;
                        var addComment = commentField.text;
                        scheduleDB.addAppointment(addDate, addTime, addChair, addPatID, addProcedure, addProviderID,
                                                  addDuration,addComment);
                        gitMan.commitData("Scheduled an appontment for " + PATIENT_FILE_NAME + " for " + apptDate.myDate);
                        Qt.quit();
                    }
                }
            }

        }
        Component.onCompleted: {
            rootWin.selectedDay = apptDate.myDate;
            rootWin.requestUpdateGhostAppt();
            checkTime();
        }
    }

    CDAppWindow {
        id: qrCodeApp
        width: 1024
        height: 600
        x: 0
        y: 0
        visibility: ApplicationWindow.Windowed
        flags: Qt.WA_TranslucentBackground | Qt.FramelessWindowHint

        CDFileLocations {
            id: m_FileLocs
        }
        Settings {
            id: pracInfo
            fileName: m_FileLocs.getLocalPracticePreferenceFile()
            category: "GeneralInfo"
        }

        Image {
            id: qrImage

            function generateScheduleCode() {
                var codeStr = "BEGIN:VEVENT\n"
                codeStr+= "SUMMARY:Dental Appointment\n"

                var dateString = apptDate.myDate.toISOString().split("T")[0];
                dateString = dateString.replace(/-/g,'');
                var startString ="";
                var startMinVal=0;


                if(startAMPM.currentText == "AM") {
                    if(startHour.currentIndex < 9) {
                        startString+= "0";
                    }
                    startString+= (startHour.currentIndex+1);
                }
                else {
                    if(startHour.currentIndex == 11) {//12PM
                        startString += "12";
                    }
                    else {
                        startString+= (13+startHour.currentIndex);
                    }

                }
                var endDateObj = new Date(apptDate.myDate);
                endDateObj.setHours(parseInt(startString));
                endDateObj.setMinutes(parseInt(startMin.currentText));
                var minutesLongAppt = (durationBox.currentIndex+1) * 15;
                minutesLongAppt = Math.max(minutesLongAppt,60); //underpromise and overdeliver
                endDateObj.setMinutes(endDateObj.getMinutes() + minutesLongAppt);

                startString+= startMin.currentText;
                var endString = endDateObj.toString().split(" ")[3];
                if(endString === undefined) {
                    return;
                }

                endString = endString.replace(/:/g,'');

                codeStr+= "DTSTART:" + dateString + "T" + startString + "00\n";
                codeStr+= "DTEND:" + dateString + "T" + endString + "\n";

                var addrLine = pracInfo.value("AddrLine1","");
                if(pracInfo.value("AddrLine2","").length > 1) {
                    addrLine += ", " + pracInfo.value("AddrLine2","");
                }
                addrLine += ", " +pracInfo.value("City","") + " " + pracInfo.value("State","") + " " +
                        pracInfo.value("Zip","");
                codeStr+="LOCATION:"+addrLine+"\n";

                codeStr+= "END:VEVENT"
                //console.debug(codeStr);
                source = "image://QRCode/" + codeStr;
            }
            anchors.fill: parent
            anchors.margins: 10
            fillMode: Image.PreserveAspectFit
            smooth: false

            Component.onCompleted: generateScheduleCode();
        }

    }

}
