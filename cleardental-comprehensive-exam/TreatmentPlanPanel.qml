// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.3
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1

CDTranslucentPane {
    id: txPlanPane
    backMaterialColor: Material.Brown

    property var txPlansObj: ({})
    property var txPlanNames: Object.keys(txPlansObj);


    function loadTxPlans() {
        if(txPlansRow.children.length > 0) { //prevent dupes
            txPlansRow.children = "";
        }

        var jsonTxPlans = textMan.readFile(fileLocs.getTreatmentPlanFile(PATIENT_FILE_NAME));
        if(jsonTxPlans.length === 0) {
            return;
        }
        txPlansObj = JSON.parse(jsonTxPlans);

        for(var i=0;i<txPlanNames.length;i++) {
            var objArgs = {"treatmentPlanName": txPlanNames[i],
                "treatmentPlanObj": txPlansObj[txPlanNames[i]]
            };
            var txPlanObj = txPlanComp.createObject(txPlansRow,objArgs);
        }

    }

    CDTextFileManager {
        id: textMan
    }

    CDFileWatcher {
        fileName:fileLocs.getTreatmentPlanFile(PATIENT_FILE_NAME)
        onFileUpdated: loadTxPlans()
    }

    ColumnLayout {
        anchors.fill: parent
        Label {
            font.pointSize: 24
            text: "Treatment Plans"
            font.underline: true
        }

        Flickable {
            Layout.fillWidth: true
            Layout.fillHeight: true
            contentWidth: (txPlanNames.length * 505) + 5
            contentHeight: parent.height
            clip: true

            ScrollBar.vertical: ScrollBar { }

            RowLayout {
                id: txPlansRow
                anchors.fill: parent
            }
        }
    }

    Label {
        id: noneLabel
        font.pointSize: 32
        text: "No treatment plans made"
        anchors.centerIn: parent
        visible: txPlanNames.length ==0

    }

    Component {
        id: txPlanComp
        TxPlanFlick {

        }
    }

    Component.onCompleted: loadTxPlans()
}
