// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.3
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1

CDTranslucentPane {
    backMaterialColor: Material.BlueGrey
    Layout.minimumHeight: flickRes.contentHeight + 25
    Layout.preferredHeight: flickRes.contentHeight + 25

    function generateCaseNote() {
        var returnMe = "";
        returnMe += "Conditions: " + conditionListLabel.text + "\n";
        returnMe += "Allergies: " + allergyListLabel.text + "\n";
        returnMe += "Medications: " + medListLabel.text + "\n";
        returnMe += "Extraoral Exam: " + eoeResultsLabel.text + "\n";
        returnMe += "Intraoral Exam: " + ioeResultsLabel.text + "\n";
        returnMe += "Caries: " + cariesLabel.text + "\n";
        returnMe += "Periodontal Diagnosis: " + perioLabel.text + "\n";
        return returnMe;
    }


    CDFileLocations {
        id: fileLocs
    }

    CDTextFileManager {
        id: textMan
    }

    Flickable {
        id: flickRes
        anchors.fill: parent
        contentHeight: allFindingsCol.height
        contentWidth: allFindingsCol.width
        clip: true
        flickableDirection: Flickable.HorizontalFlick

        ScrollBar.horizontal: ScrollBar{}

        ColumnLayout {
            id: allFindingsCol
            Label {
                text: "Summary of findings:"
                font.pointSize: 24
                font.underline: true
            }

            Label {
                id: alertLabel
                color: Material.color(Material.Red);
                Settings {
                    id: medInfoSettings
                    fileName: fileLocs.getOtherMedInfoFile(PATIENT_FILE_NAME);
                    category: "Alerts"
                }
                visible: text.length > 1
                text: medInfoSettings.value("MedicalAlert","")

                CDFileWatcher {
                    fileName: fileLocs.getOtherMedInfoFile(PATIENT_FILE_NAME);
                    onFileUpdated: {
                        medInfoSettings.sync();
                        alertLabel.text = medInfoSettings.value("MedicalAlert","")
                    }
                }
            }

            RowLayout {
                id: conditionRow

                function parseConditions() {
                    var returnMe = ""
                    var jsonConditions = textMan.readFile(fileLocs.getConditionsFile(PATIENT_FILE_NAME));
                    if(jsonConditions.length === 0) {
                        returnMe += "None"
                    }
                    else {
                        var conditionsObj = JSON.parse(jsonConditions);
                        for(var i=0;i<conditionsObj.length;i++) {
                            var condition = conditionsObj[i];
                            returnMe += condition.ConditionName;
                            if(condition.ConditionInfo.length > 0) {
                                returnMe+= " (" + condition.ConditionInfo + ")"
                            }


                            if(i !== conditionsObj.length-1) {
                                returnMe += ", "
                            }
                        }

                        if(conditionsObj.length === 0) {
                            returnMe += "None"
                        }
                    }

                    return returnMe;
                }

                Label {
                    text: "Conditions:";
                    font.bold: true
                }
                Label {
                    id: conditionListLabel
                }

                CDFileWatcher {
                    fileName: fileLocs.getConditionsFile(PATIENT_FILE_NAME)
                    onFileUpdated: conditionListLabel.text =conditionRow.parseConditions()
                }

                Component.onCompleted: conditionListLabel.text =parseConditions();
            }

            RowLayout {
                id: allergyRow

                function parseAllergies() {
                    var returnMe = ""
                    var jsonAllergies = textMan.readFile(fileLocs.getAllergiesFile(PATIENT_FILE_NAME));
                    if(jsonAllergies.length === 0) {
                        returnMe += "None"
                    }
                    else {
                        var allergyObj = JSON.parse(jsonAllergies);
                        for(var i=0;i<allergyObj.length;i++) {
                            var allergy = allergyObj[i];
                            returnMe += allergy.AllergyName;
                            returnMe+= " (" + allergy.AllergyReaction + ")"
                            if(i !== allergyObj.length-1) {
                                returnMe += ", "
                            }
                        }

                        if(allergyObj.length === 0) {
                            returnMe += "None"
                        }
                    }

                    return returnMe;
                }

                Label {
                    text: "Allergies:";
                    font.bold: true
                }
                Label {
                    id: allergyListLabel
                }

                CDFileWatcher {
                    fileName: fileLocs.getAllergiesFile(PATIENT_FILE_NAME)
                    onFileUpdated: allergyListLabel.text = allergyRow.parseAllergies()
                }

                Component.onCompleted: allergyListLabel.text = parseAllergies();
            }

            RowLayout {
                id: medRow

                function parseMedications() {
                    var returnMe = ""
                    var jsonMeds = textMan.readFile(fileLocs.getMedicationsFile(PATIENT_FILE_NAME));
                    if(jsonMeds.length === 0) {
                        return "None"
                    }
                    var medsObj = JSON.parse(jsonMeds);
                    for(var i=0;i<medsObj.length;i++) {
                        var med = medsObj[i];
                        returnMe +=  med.DrugName;
                        if(med.UsedFor.length > 1) {
                            returnMe+= " (" + med.UsedFor + ")"
                        }

                        if(i !== medsObj.length-1) {
                            returnMe += ", "
                        }
                    }

                    if(medsObj.length === 0) {
                        returnMe += "None"
                    }

                    return returnMe;
                }

                Label {
                    text: "Medications:";
                    font.bold: true
                }
                Label {
                    id: medListLabel
                }

                CDFileWatcher {
                    fileName: fileLocs.getMedicationsFile(PATIENT_FILE_NAME)
                    onFileUpdated: medListLabel.text = medRow.parseMedications()
                }

                Component.onCompleted: medListLabel.text = parseMedications();
            }

            RowLayout {
                id: eoERow

                Settings {
                    id: eoELoader
                    fileName: fileLocs.getEoEFile(PATIENT_FILE_NAME)
                }

                function parseEoE() {
                    var returnMe = "";
                    returnMe+= "Neck:" + eoELoader.value("Neck", "?")+ "; ";
                    returnMe+= "Submandibular Nodes: "
                            + eoELoader.value("SubmandibularNodes", "?")+ "; ";
                    returnMe+= "Maxillary Nodes: " +
                            eoELoader.value("MaxillaryNodes", "?")+ "; ";
                    returnMe+= "TMJ: " + eoELoader.value("TMJ", "?")+ "; ";
                    returnMe+= "Deviation/Deflection: " +
                            eoELoader.value("Deviation-Deflection", "?")+ "; ";
                    returnMe+= "Visual Asymmetry: "
                            + eoELoader.value("VisualAsymmetry", "?");

                    return returnMe;
                }

                Label {
                    text: "Extraoral Exam:";
                    font.bold: true
                }
                Label {
                    id: eoeResultsLabel
                }

                CDFileWatcher {
                    fileName: fileLocs.getEoEFile(PATIENT_FILE_NAME)
                    onFileUpdated: {
                        eoELoader.sync();
                        eoeResultsLabel.text = eoERow.parseEoE();

                    }
                }

                Component.onCompleted: eoeResultsLabel.text = parseEoE();
            }

            RowLayout {
                id: ioERow

                Settings {
                    id: ioELoader
                    fileName: fileLocs.getIoEFile(PATIENT_FILE_NAME)
                }

                function parseIoE() {
                    var returnMe = "";
                    returnMe+= "Attrition:" +
                            ioELoader.value("Attrition", "?")+ "; ";
                    returnMe+= "Floor Of Mouth:" +
                            ioELoader.value("FloorOfMouth", "?")+ "; ";
                    returnMe+= "Mucosa:" +
                            ioELoader.value("Mucosa", "?")+ "; ";
                    returnMe+= "Occlusion:" +
                            ioELoader.value("Occlusion", "?")+ "; ";
                    returnMe+= "Palate:" +
                            ioELoader.value("Palate", "?")+ "; ";
                    returnMe+= "Tongue:" +
                            ioELoader.value("Tongue", "?");

                    return returnMe;
                }

                Label {
                    text: "Intraoral Exam:";
                    font.bold: true
                }
                Label {
                    id: ioeResultsLabel
                }

                CDFileWatcher {
                    fileName: fileLocs.getIoEFile(PATIENT_FILE_NAME)
                    onFileUpdated: {
                        ioELoader.fileName = "";
                        ioELoader.fileName = fileLocs.getIoEFile(PATIENT_FILE_NAME);
                        ioeResultsLabel.text = ioERow.parseIoE();

                    }
                }

                Component.onCompleted: ioeResultsLabel.text = parseIoE();
            }

            RowLayout {
                id: cariesRow

                CDTextFileManager {
                    id: hardTissueLoader
                }

                function parseHardTissueFile() {
                    var returnMe = "Negative";
                    var full = hardTissueLoader.readFile(fileLocs.getHardTissueChartFile(PATIENT_FILE_NAME));
                    if((full.indexOf("Decay") !== -1) || (full.indexOf("Fracture") !== -1)) {
                        returnMe = "Positive";
                    }
                    return returnMe;
                }

                Label {
                    text: "Caries:";
                    font.bold: true
                }
                Label {
                    id: cariesLabel
                }

                CDFileWatcher {
                    fileName: fileLocs.getHardTissueChartFile(PATIENT_FILE_NAME)
                    onFileUpdated: cariesLabel.text = cariesRow.parseHardTissueFile()
                }

                Component.onCompleted: cariesLabel.text=parseHardTissueFile();
            }

            RowLayout {
                id: perioRow

                Settings {
                    id: perioLoader
                    fileName: fileLocs.getPerioChartFile(PATIENT_FILE_NAME)
                    category: "Diagnosis"
                }

                function parsePerioFile() {
                    return perioLoader.value("GeneralDx","?")
                }

                Label {
                    text: "Periodontal Diagnosis:";
                    font.bold: true
                }
                Label {
                    id: perioLabel
                }

                CDFileWatcher {
                    fileName: fileLocs.getPerioChartFile(PATIENT_FILE_NAME)
                    onFileUpdated: {
                        perioLoader.sync();
                        perioLabel.text = perioRow.parsePerioFile();
                    }
                }

                Component.onCompleted: perioLabel.text=parsePerioFile();
            }

        }
    }



}
