// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtGraphicalEffects 1.12
import QtQuick.Window 2.10

CDAppWindow {
    id: rootWin
    title: "Pediatric Exam " + " [ID: " + PATIENT_FILE_NAME + "]"

    property string caseNoteString: ""
    property bool isCompExam: true

    header: CDPatientToolBar {
        headerText: "Pediatric Exam:"
        ToolButton {
            id: appMenuButton
            icon.name: "application-menu"
            icon.width: 64
            icon.height: 64
            onClicked: drawer.open();
            anchors.left: parent.left
        }

        ToolButton {
            id: goLeftButton
            icon.name: "go-previous"
            icon.width: 64
            icon.height: 64
            visible: mainStack.depth > 1
            anchors.left: appMenuButton.right
            onClicked: {
                mainStack.pop();
            }

        }
    }

    CDHygieneRoomStatus {
        id: drawer
        height: rootWin.height
        width: 0.33 * rootWin.width
    }

    StackView {
        id: mainStack
        anchors.fill: parent

        initialItem: InitColumn{}

    }

    CDToothScene3D { //this is a stupid workaround to prevent the application from freezing
        //https://bugreports.qt.io/browse/QTBUG-86460
        toothNumb: "1"
        width: 1
        height: 1
    }
}
