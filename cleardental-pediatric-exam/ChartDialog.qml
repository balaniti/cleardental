// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtGraphicalEffects 1.12

CDTranslucentDialog {
    id: chartDialog
    property string toothNumber


    CDFileLocations {
        id: diaLocs
    }

    CDCommonFunctions {
        id: diaComFuns
    }

    CDTextFileManager {
        id: diaTextMan
    }

    Settings {
        id: hardTissueSettings
        fileName: diaLocs.getHardTissueChartFile(PATIENT_FILE_NAME)
    }
    RowLayout {
        ColumnLayout {

            CDTranslucentPane {
                Layout.fillWidth: true
                backMaterialColor: Material.Pink
                CDHeaderLabel {
                    text: "Tooth " + chartDialog.toothNumber
                }
            }

            CDTranslucentPane {
                backMaterialColor: Material.Cyan
                Layout.fillWidth: true

                ColumnLayout {
                    Label {
                        text: "Existing"
                        font.pointSize: 24
                        font.underline: true
                    }

                    RowLayout {
                        CheckBox {
                            id: exChip
                            text: "Chip/Fracture"
                        }
                        CDSurfaceRow {
                            id: chipSurfaces
                            opacity: exChip.checked ? 1:0
                            workingToothNumb: chartDialog.toothNumber
                            Behavior on opacity {
                                PropertyAnimation {
                                    duration: 300
                                }
                            }
                            enabled: exChip.checked
                        }
                    }

                    RowLayout {
                        CheckBox {
                            id: exDecay
                            text: "Decay"
                        }
                        CDSurfaceRow {
                            id: decaySurfaces
                            opacity: exDecay.checked ? 1:0
                            workingToothNumb: chartDialog.toothNumber
                            Behavior on opacity {
                                PropertyAnimation {
                                    duration: 300
                                }
                            }
                            enabled: exDecay.checked
                        }
                    }

                    RowLayout {
                        CheckBox {
                            id: exComposite
                            text: "Composite"
                        }
                        CDSurfaceRow {
                            id: compositeSurfaces
                            opacity: exComposite.checked ? 1:0
                            workingToothNumb: chartDialog.toothNumber
                            Behavior on opacity {
                                PropertyAnimation {
                                    duration: 300
                                }
                            }
                            enabled: exComposite.checked
                        }
                    }

                    CheckBox {
                        id: exSealant
                        text: "Sealant"
                    }

                    RowLayout {
                        CheckBox {
                            id: exAmalgam
                            text: "Amalgam"
                        }
                        CDSurfaceRow {
                            id: amalgamSurfaces
                            opacity: exAmalgam.checked ? 1:0
                            workingToothNumb: chartDialog.toothNumber
                            Behavior on opacity {
                                PropertyAnimation {
                                    duration: 300
                                }
                            }
                            enabled: exAmalgam.checked
                        }
                    }

                    CheckBox {
                        id: exSS
                        text: "Stainless Steel Crown"
                    }
                }
            }

            CDTranslucentPane {
                backMaterialColor: Material.Lime
                Layout.fillWidth: true

                ColumnLayout {
                    Label {
                        text: "Treatment Plan"
                        font.pointSize: 24
                        font.underline: true
                    }
                    CheckBox {
                        id: txWatch
                        text: "Watch"
                    }
                    RowLayout {
                        CheckBox {
                            id: txComposite
                            text: "Composite"
                        }
                        CDSurfaceRow {
                            id: txCompositeSurfaces
                            opacity: txComposite.checked ? 1:0
                            workingToothNumb: chartDialog.toothNumber
                            Behavior on opacity {
                                PropertyAnimation {
                                    duration: 300
                                }
                            }
                            enabled: txComposite.checked
                        }
                    }

                    CheckBox {
                        id: txSealant
                        text: "Sealant"
                    }

                    RowLayout {
                        CheckBox {
                            id: txAmalgam
                            text: "Amalgam"
                        }
                        CDSurfaceRow {
                            id: txAmalgamSurfaces
                            opacity: txAmalgam.checked ? 1:0
                            workingToothNumb: chartDialog.toothNumber
                            Behavior on opacity {
                                PropertyAnimation {
                                    duration: 300
                                }
                            }
                            enabled: txAmalgam.checked
                        }
                    }
                    CheckBox {
                        id: txSS
                        text: "Stainless Steel Crown"
                    }
                    RowLayout {
                        CheckBox {
                            id: txExt
                            text: "Extract"
                        }
                        ComboBox {
                            id: extractionType
                            model: ["Regular Extraction", "Rootless Extraction"]
                            Layout.minimumWidth: 240
                            Behavior on opacity {
                                PropertyAnimation {
                                    duration: 300
                                }
                            }
                            enabled: txExt.checked && isNaN(chartDialog.toothNumber)
                            opacity: enabled ? 1:0
                        }
                    }
                }
            }


            RowLayout {
                CDCancelButton {
                    onClicked: {
                        chartDialog.reject();
                    }
                }

                Label {
                    Layout.fillWidth: true;
                }

                CDSaveAndCloseButton {
                    onClicked: {
                        //All existings
                        var existingArray = [];

                        if(exDecay.checked) {
                            var decayString = "Decay: " + decaySurfaces.getSurfaces();
                            existingArray.push(decayString);
                        }

                        if(exComposite.checked) {
                            var exCompStr = "Composite: " + compositeSurfaces.getSurfaces();
                            existingArray.push(exCompStr);
                        }

                        if(exAmalgam.checked) {
                            var exAmalStr = "Amalgam: " + amalgamSurfaces.getSurfaces();
                            existingArray.push(exAmalStr);
                        }

                        if(exChip.checked) {
                            var exChippedStr = "Chipped: " + chipSurfaces.getSurfaces();
                            existingArray.push(exChippedStr);
                        }

                        if(exSS.checked) {
                            existingArray.push("Stainless Steel Crown");
                        }

                        if(exSealant.checked) {
                            existingArray.push("Sealant");
                        }

                        hardTissueSettings.setValue(chartDialog.toothNumber,diaComFuns.makeHardTissueString(existingArray));
                        hardTissueSettings.sync();


                        //All treatment plans

                        //first remove everything related to this tooth
                        var jsonTxPlans = diaTextMan.readFile(diaLocs.getTreatmentPlanFile(PATIENT_FILE_NAME));
                        var txPlansObj = ({});
                        if(jsonTxPlans.length > 2) {
                            txPlansObj = JSON.parse(jsonTxPlans);
                        }
                        diaComFuns.ensurePrimaryAndUnphased(txPlansObj);
                        var procedures = txPlansObj["Primary"]["Unphased"];
                        for(var i=0;i<procedures.length;i++) {
                            var txItem = procedures[i];
                            //@disable-check M126
                            if(txItem["Tooth"] == chartDialog.toothNumber) {
                                procedures.splice(i,1);
                                i--;
                            }
                        }
                        diaTextMan.saveFile(diaLocs.getTreatmentPlanFile(PATIENT_FILE_NAME),
                                            JSON.stringify(txPlansObj, null, '\t'));

                        if(txComposite.checked) {
                            var compObj = ({});
                            compObj["Tooth"] = chartDialog.toothNumber;
                            compObj["ProcedureName"] = "Composite";
                            compObj["Surfaces"] = txCompositeSurfaces.getSurfaces();
                            if(txCompositeSurfaces.isPost) {
                                switch(compObj["Surfaces"].length) {
                                default:
                                case 1:
                                    compObj["DCode"] = "D2391";
                                    break;
                                case 2:
                                    compObj["DCode"] = "D2392";
                                    break;
                                case 3:
                                    compObj["DCode"] = "D2393";
                                    break;
                                case 4:
                                case 5:
                                    compObj["DCode"] = "D2394";
                                    break;
                                }
                            }
                            else {
                                switch(compObj["Surfaces"].length) {
                                default:
                                case 1:
                                    compObj["DCode"] = "D2330";
                                    break;
                                case 2:
                                    compObj["DCode"] = "D2331";
                                    break;
                                case 3:
                                    compObj["DCode"] = "D2332";
                                    break;
                                case 4:
                                case 5:
                                    compObj["DCode"] = "D2335";
                                    break;
                                }
                            }
                            diaComFuns.addTxItem(PATIENT_FILE_NAME,"Primary","Unphased",compObj);
                        }
                        if(txAmalgam.checked) {
                            var amalObj = ({});
                            amalObj["Tooth"] = chartDialog.toothNumber;
                            amalObj["ProcedureName"] = "Amalgam";
                            amalObj["Surfaces"] = txAmalgamSurfaces.getSurfaces();
                            switch(amalObj["Surfaces"].length) {
                            default:
                            case 1:
                                amalObj["DCode"] = "D2140";
                                break;
                            case 2:
                                amalObj["DCode"] = "D2150";
                                break;
                            case 3:
                                amalObj["DCode"] = "D2160";
                                break;
                            case 4:
                            case 5:
                                amalObj["DCode"] = "D2161";
                                break;
                            }
                            diaComFuns.addTxItem(PATIENT_FILE_NAME,"Primary","Unphased",amalObj);
                        }

                        if(txExt.checked) {
                            var extObj = ({});
                            extObj["Tooth"] = chartDialog.toothNumber;
                            extObj["ProcedureName"] = "Extraction";
                            if(extractionType.currentIndex == 0) { //regular
                                extObj["DCode"] = "D7140";
                            }
                            else { //rootless
                                extObj["DCode"] = "D7111";
                            }
                            extObj["ExtractionType"] = extractionType.currentText
                            diaComFuns.addTxItem(PATIENT_FILE_NAME,"Primary","Unphased",extObj);
                        }

                        if(txSS.checked) {
                            var ssObj = ({});
                            ssObj["Tooth"] = chartDialog.toothNumber;
                            ssObj["ProcedureName"] = "Crown";
                            if(isNaN(chartDialog.toothNumber)) { //primary tooth S/S
                                ssObj["DCode"] = "D2930"
                            }
                            else {
                                ssObj["DCode"] = "D2931"
                            }
                            diaComFuns.addTxItem(PATIENT_FILE_NAME,"Primary","Unphased",ssObj);
                        }

                        if(txSealant.checked) {
                            var sealantObj = ({});
                            sealantObj["Tooth"] = chartDialog.toothNumber;
                            sealantObj["ProcedureName"] = "Sealant";
                            sealantObj["DCode"] = "D1351"
                            diaComFuns.addTxItem(PATIENT_FILE_NAME,"Primary","Unphased",sealantObj);
                        }

                        if(txWatch.checked) {
                            var watchObj= ({});
                            watchObj["Tooth"] = chartDialog.toothNumber;
                            watchObj["ProcedureName"] = "Watch";
                            diaComFuns.addTxItem(PATIENT_FILE_NAME,"Primary","Unphased",watchObj);
                        }

                        editedTeeth += "-"+ chartDialog.toothNumber+"-"
                        chartDialog.accept();
                    }
                }
            }

        }

        CDReviewRadiographPane {
            id: revPlane

            toothToReview: chartDialog.toothNumber

            Layout.minimumWidth: 640
            Layout.fillHeight: true
        }
    }
    


    onVisibleChanged: {
        if(visible) {
            //First put in the existings
            var attrs = diaComFuns.getHardTissueAttrs(hardTissueSettings.value(chartDialog.toothNumber,""));
            for(var i=0;i<attrs.length;i++) {
                if(attrs[i].startsWith("Decay")) {
                    exDecay.checked = true;
                    decaySurfaces.parseSurfaces(attrs[i].split(":")[1]);
                }
                else if(attrs[i].startsWith("Composite")) {
                    exComposite.checked = true;
                    compositeSurfaces.parseSurfaces(attrs[i].split(":")[1]);
                }
                else if(attrs[i].startsWith("Amalgam")) {
                    exAmalgam.checked = true;
                    amalgamSurfaces.parseSurfaces(attrs[i].split(":")[1]);
                }
                else if(attrs[i].startsWith("Chipped")) {
                    exChip.checked = true;
                    chipSurfaces.parseSurfaces(attrs[i].split(":")[1]);
                }
                else if(attrs[i].startsWith("Stainless Steel Crown")) {
                    exSS.checked = true;
                }
            }

            //Then fill out the treatment plan
            var jsonTxPlans = diaTextMan.readFile(diaLocs.getTreatmentPlanFile(PATIENT_FILE_NAME));
            var txPlansObj = ({});
            if(jsonTxPlans.length > 2) {
                txPlansObj = JSON.parse(jsonTxPlans);
            }
            diaComFuns.ensurePrimaryAndUnphased(txPlansObj);


            var procedures = diaComFuns.getAllTxPlanItems(PATIENT_FILE_NAME)
            for(i=0;i<procedures.length;i++) {
                var txItem = procedures[i];
                //@disable-check M126
                if(txItem["Tooth"] == chartDialog.toothNumber) {
                    if(txItem["ProcedureName"].startsWith("Composite")) {
                        txComposite.checked = true;
                        txCompositeSurfaces.parseSurfaces(txItem["Surfaces"]);
                    }
                    else if(txItem["ProcedureName"].startsWith("Amalgam")) {
                        txAmalgam.checked = true;
                        txAmalgamSurfaces.parseSurfaces(txItem["Surfaces"]);
                    }
                    else if(txItem["ProcedureName"].startsWith("Extraction")) {
                        txExt.checked = true;
                        if(txItem["ExtractionType"].startsWith("Rootless")) {
                            extractionType.currentIndex = 1;
                        }
                    }
                    else if(txItem["ProcedureName"].startsWith("Crown")) {
                        txSS.checked = true;
                    }
                    else if(txItem["ProcedureName"].startsWith("Watch")) {
                        txWatch.checked = true;
                    }
                    else if(txItem["ProcedureName"].startsWith("Sealant")) {
                        txSealant.checked = true;
                    }
                }
            }
        }
        else {
            exDecay.checked = false;
            decaySurfaces.clearSurfaces();
            exComposite.checked = false;
            compositeSurfaces.clearSurfaces();
            exAmalgam.checked = false;
            amalgamSurfaces.clearSurfaces();
            exSS.checked = false;

            txWatch.checked = false;
            txComposite.checked = false;
            txCompositeSurfaces.clearSurfaces();
            txAmalgam.checked = false;
            txAmalgamSurfaces.clearSurfaces();
            txSS.checked = false;
            txExt.checked = false;
            txSealant.checked = false;
        }
    }
}
