// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTranslucentDialog {
    id: sealantDialog

    property var checkBoxArray: []
    property var toothList: [2,3,14,15,17,18,19,30,31]

    ColumnLayout {
        CDTranslucentPane {
            ColumnLayout {
                CDHeaderLabel {
                    text: "Select teeth for sealants"
                }
                Repeater {
                    id: toothListReaper

                    model: toothList.length
                    delegate: CheckBox {
                        text: "Tooth #" + sealantDialog.toothList[index];
                        Component.onCompleted: {
                            checkBoxArray.push(this);
                        }
                    }
                }
            }
        }

        RowLayout {
            CDCancelButton {
                onClicked: {
                    sealantDialog.reject();
                }
            }

            Label {
                Layout.fillWidth: true;
            }

            CDAddButton {
                text: "Tx plan teeth"

                CDCommonFunctions {
                    id: m_comFuns
                }

                onClicked:  {
                    for(var i=0;i<checkBoxArray.length;i++) {
                        if(checkBoxArray[i].checked) {
                            var addMe = {
                                Tooth: toothList[i],
                                ProcedureName: m_comFuns.getNameForCode("D1351"),
                                DCode: "D1351",
                            }

                            m_comFuns.addTxItem(PATIENT_FILE_NAME, "Primary", "1",addMe);
                        }
                    }
                    sealantDialog.accept();
                }

            }
        }

    }
}
