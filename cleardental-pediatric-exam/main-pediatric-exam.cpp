// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickImageProvider>
#include <QFont>
#include <QIcon>
#include <QDebug>
#include <QProcess>

#include "cddefaults.h"

int main(int argc, char *argv[])
{
    CDDefaults::setAppDefaults();
    QCoreApplication::setApplicationName("ClearDental-Pediatric-Exam");

    QGuiApplication app(argc, argv);
    app.setWindowIcon(CDDefaults::defaultColorIcon());

    if(QCoreApplication::arguments().count()<2) {
        qDebug()<<"You need to give the patient's name as an argument";
        return -32;
    }



    CDDefaults::registerQMLTypes();
    CDDefaults::setGuiApplicationSettings(&app);

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("PATIENT_FILE_NAME",QString(argv[1]));
    if(QCoreApplication::arguments().count()>1) {
        engine.rootContext()->setContextProperty("PROCEDURE_JSON",QString(argv[2]));
    }
    else {
        engine.rootContext()->setContextProperty("PROCEDURE_JSON","{}");
    }
    engine.load(QUrl(QStringLiteral("qrc:/MainPediatricExam.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    CDDefaults::enableBlurBackground();

    return app.exec();
}
