// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTransparentPage {
    id: chartPage
    property int patAge: getAge() //in months

    property string editedTeeth: ""
    property var doubleArray: []

    font.family: "Monda"
    font.pointSize: 24

    CDFileLocations {
        id: ch_Locs
    }

    Settings {
        id: ch_patPersonalSettings
        fileName: ch_Locs.getPersonalFile(PATIENT_FILE_NAME)
        category: "Personal"
    }

    Settings {
        id: ch_hardTissue
        fileName: ch_Locs.getHardTissueChartFile(PATIENT_FILE_NAME)
    }


    CDGitManager {
        id: ch_gitMan
    }

    CDFileLocations {
        id: fLocs
    }

    Settings {
        id: hardTissueReader
        fileName: fLocs.getHardTissueChartFile(PATIENT_FILE_NAME);
    }

    CDCommonFunctions {
        id: comFuns
    }

    function getAge() {
        var returnMe =0;
        var textDOBArray = ch_patPersonalSettings.value("DateOfBirth","1/1/0001").split("/");
        var dobMonth = parseInt(textDOBArray[0]);
        var dobDay = parseInt(textDOBArray[1]);
        var dobYear =  parseInt(textDOBArray[2]);
        var today = new Date();

        var monthDiff = (today.getFullYear() - dobYear) * 12;
        monthDiff -= dobMonth;
        monthDiff += today.getMonth() + 1;

        return monthDiff;
    }

    function makeTxPlanArray(getTooth) {
        var txPlanItems = comFuns.getAllTxPlanItems(PATIENT_FILE_NAME);
        var returnMe = [];
        for(var i=0;i<txPlanItems.length;i++) {
            var txItem = txPlanItems[i];
            if("Tooth" in txItem) {
                // @disable-check M126
                if(txItem["Tooth"] == getTooth) {
                    returnMe.push(txItem);
                }
            }
        }
        return returnMe;
    }

    function makeExistingArray(getTooth) {
        var fullString = hardTissueReader.value(getTooth,"");
        return comFuns.getHardTissueAttrs(fullString);
    }

    function setCheckedPerm(getTooth) {
        //first check if it is already charted
        var setAlready = ch_hardTissue.value(getTooth,"");
        if(setAlready.length > 1) {
            if(setAlready === "Missing") {
                return false;
            }
            else {
                return true;
            }
        }

        //Otherwise go based on age
        getTooth = parseInt(getTooth);

        var ageYear = patAge / 12;
        if(ageYear < 6) {
            return false;
        }

        var fakeNumber =0; //compress the left and right side

        if(getTooth <= 16) { //maxillary
            if(getTooth > 8) { //left
                fakeNumber = 17 -getTooth;
            }
            else {
                fakeNumber = getTooth;
            }
        }
        else { //mandibular
            if(getTooth < 25) { //left
                fakeNumber = 32 - (getTooth - 17)
            }
            else {
                fakeNumber = getTooth;
            }
        }

        switch(fakeNumber) {
        case 1:
        case 32:
            return ageYear >= 17;
        case 2:
            return ageYear >= 12;
        case 3:
            return ageYear >= 6;
        case 4:
            return ageYear >= 10;
        case 5:
            return ageYear >= 10;
        case 6:
            return ageYear >= 11;
        case 7:
            return ageYear >= 8;
        case 8:
            return ageYear >= 7;
        case 25:
            return ageYear >= 6;
        case 26:
            return ageYear >= 7;
        case 27:
            return ageYear >= 9;
        case 28:
            return ageYear >= 10;
        case 29:
            return ageYear >= 11;
        case 30:
            return ageYear >= 6;
        case 31:
            return ageYear >= 11;
        default:
            return false;
        }
    }

    function setVisibleDec(getIndex) {
        if( (getIndex < 4) ||
                ((getIndex > 13) && (getIndex <20)) ||
                (getIndex > 29)) {
            return false;
        }
        return true;

    }

    function setTextDec(getIndex) {
        if(!setVisibleDec(getIndex)) {
            return "X"
        }

        if(getIndex < 16) {
            return String.fromCharCode(61 + getIndex);
        }
        else {
            return String.fromCharCode(55 + getIndex);
        }
    }

    function setCheckedDec(getIndex) {
        //first check if it is already charted
        var setAlready = ch_hardTissue.value(setTextDec(getIndex),"");
        if(setAlready.length > 1) {
            if(setAlready === "Missing") {
                return false;
            }
            else {
                return true;
            }
        }

        //Otherwise go based on age

        var ageYear = patAge / 12;

        if(patAge < 6) {
            return false;
        }
        else if(ageYear > 12) {
            return false;
        }

        switch(getIndex) {
        case 4:
        case 13: //max 2nd molar
            return ((patAge >= 25) && (ageYear < 12));
        case 5:
        case 12://max 1st molar
            return ((patAge >= 13) && (ageYear < 11));
        case 6:
        case 11: //max canine
            return ((patAge >= 16) && (ageYear < 12));
        case 7:
        case 10: //max lateral
            return ((patAge >= 9) && (ageYear < 8));
        case 8:
        case 9: //max central
            return ((patAge >= 8) && (ageYear < 7));
        case 20:
        case 29: //man 2nd molar
            return ((patAge >= 23) && (ageYear < 12));
        case 21:
        case 28: //man 1st molar
            return ((patAge >= 14) && (ageYear < 11));
        case 22:
        case 27: //man canine
            return ((patAge >= 17) && (ageYear < 12));
        case 23:
        case 26: //man lateral
            return ((patAge >= 10) && (ageYear < 8));
        case 24:
        case 25: //man central
            return ((patAge >= 6) && (ageYear < 7));
        default:
            return false;
        }

    }

    CDAddButton {
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.margins: 10
        text: "Tx Plan Sealants"
        onClicked: {
            sealDia.open();
        }

        SealantDialog {
            id: sealDia

        }

    }


    CDTranslucentPane {
        anchors.centerIn: parent
        backMaterialColor: Material.Orange

        GridLayout {
            columns: 16
            CDHeaderLabel {
                text: "Chart Teeth"
                Layout.columnSpan: 16
            }

            Repeater { ////////////MAXILLARY
                model: 16
                ColumnLayout {
                    Button {
                        id: permButton
                        text: index +1
                        checkable: true
                        checked: setCheckedPerm(text)
                        Layout.minimumWidth: 100
                        Layout.minimumHeight: 175


                        CDToothScene3D {
                            id: toothScene3D
                            anchors.fill: parent
                            anchors.topMargin: 10
                            toothNumb: parent.text
                            existingList: makeExistingArray(parent.text)
                            txPlanList: makeTxPlanArray(parent.text)
                        }

                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                permButton.toggle();
                            }
                        }

                        Button {
                            icon.name: "document-edit"
                            icon.width: 32
                            icon.height: 32
                            anchors.right: parent.right
                            anchors.top: parent.top
                            anchors.left: parent.left
                            anchors.margins: 5
                            width: 42
                            height: 42
                            highlighted: true
                            Material.accent: Material.Lime
                            onClicked: {
                                chartDialog.toothNumber = parent.text;
                                chartDialog.open();
                                toothScene3D.reloadInformation();
                            }
                        }
                    }
                    Button {
                        id: deciduousButton
                        text: setTextDec(index+1)
                        checkable: true
                        visible: setVisibleDec(index+1)
                        checked: setCheckedDec(index+1)
                        Layout.minimumWidth: 100
                        Layout.minimumHeight: 175

                        CDToothScene3D {
                            id: toothScene3DmaxDe
                            anchors.fill: parent
                            anchors.topMargin: 10
                            toothNumb: parent.text
                            existingList: makeExistingArray(parent.text)
                            txPlanList: makeTxPlanArray(parent.text)
                        }

                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                deciduousButton.toggle();
                            }
                        }

                        Button {
                            icon.name: "document-edit"
                            icon.width: 32
                            icon.height: 42
                            anchors.right: parent.right
                            anchors.top: parent.top
                            anchors.left: parent.left
                            anchors.margins: 5
                            width: 42
                            height: 42
                            highlighted: true
                            Material.accent: Material.Lime
                            onClicked: {
                                chartDialog.toothNumber = parent.text;
                                chartDialog.open();
                            }
                        }
                    }

                    ButtonGroup {
                        buttons: [permButton,deciduousButton]
                        exclusive: false
                        Component.onCompleted: {
                            doubleArray.push(buttons);
                        }
                    }

                }
            }
            MenuSeparator {
                Layout.columnSpan: 16
                Layout.fillWidth: true
            }
            Repeater {  ////////////MANDIBULAR
                model: 16
                ColumnLayout {
                    Button {
                        id: permManButton
                        text: 32-index
                        checkable: true
                        Layout.minimumWidth: 100
                        Layout.minimumHeight: 175
                        checked: setCheckedPerm(text)

                        CDToothScene3D {
                            id: toothScene3DManPerm
                            anchors.fill: parent
                            anchors.topMargin: 10
                            toothNumb: parent.text
                            existingList: makeExistingArray(parent.text)
                            txPlanList: makeTxPlanArray(parent.text)
                        }

                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                permManButton.toggle();
                            }
                        }

                        Button {
                            icon.name: "document-edit"
                            icon.width: 32
                            icon.height: 32
                            anchors.right: parent.right
                            anchors.top: parent.top
                            anchors.left: parent.left
                            anchors.margins: 5
                            width: 42
                            height: 42
                            highlighted: true
                            Material.accent: Material.Lime
                            onClicked: {
                                chartDialog.toothNumber = parent.text;
                                chartDialog.open();
                            }
                        }
                    }
                    Button {
                        id: deciduousManButton
                        text: setTextDec(32-index)
                        visible: setVisibleDec(32-index)
                        checkable: true
                        checked: setCheckedDec(32-index)
                        Layout.minimumWidth: 100
                        Layout.minimumHeight: 175

                        CDToothScene3D {
                            id: toothScene3DManDec
                            anchors.fill: parent
                            anchors.topMargin: 10
                            toothNumb: parent.text
                            existingList: makeExistingArray(parent.text)
                            txPlanList: makeTxPlanArray(parent.text)
                        }

                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                deciduousManButton.toggle();
                            }
                        }

                        Button {
                            icon.name: "document-edit"
                            icon.width: 32
                            icon.height: 32
                            anchors.right: parent.right
                            anchors.top: parent.top
                            anchors.left: parent.left
                            anchors.margins: 5
                            width: 42
                            height: 42
                            highlighted: true
                            Material.accent: Material.Lime
                            onClicked: {
                                chartDialog.toothNumber = parent.text;
                                chartDialog.open();
                            }
                        }
                    }

                    ButtonGroup {
                        buttons: [permManButton,deciduousManButton]
                        exclusive: false
                        Component.onCompleted: {
                            doubleArray.push(buttons);
                        }
                    }
                }
            }
        }
    }

    ChartDialog {
        id: chartDialog
        anchors.centerIn: parent
        toothNumber: "A"

        onAccepted: {
            for(var i=0;i<doubleArray.length;i++) {
                var doub = doubleArray[i];
                var perm = doub[0];
                var dec = doub[1];

                if(perm.text === toothNumber) {
                    perm.children[0].existingList= makeExistingArray(toothNumber)
                    perm.children[0].txPlanList= makeTxPlanArray(toothNumber)
                    perm.children[0].reloadInformation();
                }
                else if(dec.text === toothNumber) {
                    dec.children[0].existingList =makeExistingArray(toothNumber)
                    dec.children[0].txPlanList=makeTxPlanArray(toothNumber)
                    dec.children[0].reloadInformation();
                }
            }
        }
    }

    CDButton {
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: 10
        highlighted: true
        text: "Next"
        Material.accent: Material.Green
        icon.name: "go-next"
        onClicked: {
            for(var i=0;i<doubleArray.length;i++) {
                //0 = perm; 1 = dec;
                var doub = doubleArray[i];
                var perm = doub[0];
                if(perm.checked) {
                    var originalPermVal = ch_hardTissue.value(perm.text,"");
                    if((originalPermVal.length < 1) || (originalPermVal === "Missing")) {
                        ch_hardTissue.setValue(perm.text,"Present"); //makes it so it shows up
                    }
                }
                else {
                    ch_hardTissue.setValue(perm.text,"Missing");
                }

                var dec = doub[1];
                if(dec.text !== "X") {
                    if(dec.checked) {
                        var originalDecVal = ch_hardTissue.value(dec.text,"");
                        if(originalDecVal.length < 1) {
                            ch_hardTissue.setValue(dec.text,"Present"); //makes it so it shows up
                        }
                    }
                    else {
                        ch_hardTissue.setValue(dec.text,"Missing");

                    }
                }
            }

            ch_hardTissue.sync();
            ch_gitMan.commitData("Updated hard tissue charting for " + PATIENT_FILE_NAME);
            caseNoteString += "Updated hard tissue charting.\n";
            mainStack.push("PediatricProphyPage.qml")
        }
    }
}


