// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtGraphicalEffects 1.12

Flickable {
    id: radFlick
    contentHeight: 240
    contentWidth:  (240*radiographLocs.length) +10
    clip: true
    //Layout.fillWidth: true
    Layout.minimumWidth: 260
    Layout.minimumHeight: 240
    property int getTypeR: CDRadiographSensor.BW_Left_Distal

    property var radiographLocs: radioMan.getRadiographFromType(PATIENT_FILE_NAME,getTypeR,includeBad)
    property bool includeBad: false


    ScrollBar.horizontal: ScrollBar {
        id: scrollbar
        policy: radiographLocs.length > 1 ? ScrollBar.AlwaysOn : ScrollBar.AsNeeded

        SequentialAnimation {
            PropertyAnimation {
                from: 1
                to: 0
                target: scrollbar
                property: "opacity"
                duration: 3000
                easing.type: Easing.InQuad
            }
            PropertyAnimation {
                from: 0
                to: 1
                target: scrollbar
                property: "opacity"
                duration: 3000
                easing.type: Easing.OutQuad
            }
            running: true
            loops: Animation.Infinite
        }

        layer.enabled: true
        layer.effect: Glow {
            samples: 15
            color: "#64aae5"
            transparentBorder: false
        }
    }

    CDRadiographManager{
        id: radioMan
    }
    CDFileLocations {
        id: m_fLocs
    }


    RowLayout {
        Repeater {
            model: radiographLocs.length
            Image {
                source: "file://" + radiographLocs[index]
                Layout.maximumWidth: 240
                Layout.maximumHeight: 240
                fillMode: Image.PreserveAspectCrop
                mipmap: true

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        radioDia.setSource = parent.source
                        radioDia.open()
                    }
                }

                Label {
                    id: radioLabel
                    anchors.bottom: parent.bottom
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.margins: 15
                    style: Text.Outline
                    styleColor: Material.background
                    font.pointSize: 16

                    function updateText() {
                        var scr = radiographLocs[index]
                        var filename = scr.substr(scr.lastIndexOf("/") + 1);

//                        var fileSee = filename.substr(0,filename.lastIndexOf("."));

//                        var fileUser = fileSee.replace(/_/g, " ");
//                        if(fileUser.endsWith(".png")) { //Because it was a "bad" radiograph
//                            fileUser = fileUser.replace(".png" , " (bad)");
//                        }

                        var radioDir = m_fLocs.getRadiographDir(PATIENT_FILE_NAME);
                        var dateLong = scr.replace(radioDir,"");
                        var dater = dateLong.replace( "/"  + filename,"");

                        text = dater;
                    }

                    Component.onCompleted: {
                        updateText();
                    }

                    CDRadioDia {
                        id: radioDia
                    }
                }
            }
        }
    }
}
