// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDAppWindow {
    id: rootWin
    visible: true
    title: qsTr("Periodic Exam [ID: " + PATIENT_FILE_NAME + "]")

    CDToolLauncher {
        id: launcher
    }

    CDFileLocations {
        id: fileLocs
    }

    header: CDPatientToolBar {
        headerText: "Periodic Exam:"
    }

    RowLayout {
        anchors.fill: parent
        anchors.margins: 10

        Flickable {
            Layout.alignment: Qt.AlignTop
            Layout.fillHeight: true
            width: contentWidth +10
            height: 300
            clip: true
            flickableDirection: Flickable.VerticalFlick
            contentHeight: diagnosticColumn.height
            contentWidth: diagnosticColumn.width
            ScrollBar.vertical: ScrollBar { }
            DiagnosticPane {
                id: diagnosticColumn
            }
        }

        ColumnLayout {
            RowLayout {
                PatientInfoPanel {
                    id: patInfoPanel
                    Layout.fillWidth: true
                }
                CDCaseNotePane {
                    Layout.fillWidth: true
                    Layout.preferredHeight: patInfoPanel.height
                }
            }


            RowLayout {
                CDReviewRadiographPane {
                    Layout.minimumWidth: 720
                    Layout.fillHeight: true
                }

                TreatmentPlanPanel {
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                }

                Layout.fillWidth: true
                Layout.fillHeight: true
            }

            CDFinishProcedureButton {
                Layout.alignment: Qt.AlignRight
                text: "Complete Exam"

                CDCommonFunctions {
                    id: comFuns
                }

                onClicked: {
                    var caseNote = diagnosticColumn.generateCaseNote();
                    caseNote += patInfoPanel.generateCaseNote();
                    finProDia.caseNoteString = caseNote;
                    comFuns.updateReviewFile("PeriodicExam",PATIENT_FILE_NAME);
                    var completeMe = comFuns.findTx(PATIENT_FILE_NAME,"Periodic Exam");
                    finProDia.txItemsToComplete = [completeMe[0]];
                    finProDia.open();
                }
            }
        }
    }

    CDFinishProcedureDialog {
        id: finProDia
    }


}
