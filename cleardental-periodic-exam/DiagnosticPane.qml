// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDTranslucentPane {
    backMaterialColor: Material.Blue

    function generateCaseNote() {
        var returnMe = "";

        if(noCCButton.checked) {
            returnMe += "Chief Complaint: None\n";
        }
        else {
            returnMe += "Chief Complaint: " + patCC.text + "\n";
        }

        var reviews = [];

        if(perReviewButton.wasReviewed) {
            reviews.push("Reviewed Personal Information");
        }

        if(covidButton.wasReviewed) {
            reviews.push("Reviewed COVID Screening");
        }

        if(vitalButton.wasReviewed) {
            reviews.push("Reviewed Vitals");
        }

        if(medReviewButton.wasReviewed) {
            reviews.push("Reviewed Medical History");
        }

        if(takeRadio.wasReviewed) {
            reviews.push("Radiographs taken");
        }

        if(takePhoto.wasReviewed) {
            reviews.push("Photographs taken");
        }

        if(extraOralButton.wasReviewed) {
            reviews.push("Extraoral Exam Done");
        }

        if(intraOralButton.wasReviewed) {
            reviews.push("Intraoral Exam Done");
        }

        returnMe += reviews.join("; ") + "\n";

        if(ocsNeg.checked) {
            returnMe += "Oral Cancer screening: Negative\n";
        }
        else {
            returnMe += "Oral Cancer screening: Positive\n";
        }

        if(perioButton.wasReviewed) {
            returnMe +="Periodontal Exam Done\n";
        }

        if(hardTissueChartButton.wasReviewed) {
            returnMe += "Hard Tissue Charting done\n";
        }

        return returnMe;
    }

    ColumnLayout {
        id: diagnosticColumn
        RowLayout {
            id: ccRow
            Label {
                text:"Chief Complaint:"
                font.pointSize: 16
                verticalAlignment: Text.AlignVCenter
            }

            RadioButton {
                id: noCCButton
                checked: true
                text: "None"
            }

            RadioButton {
                id: isACCButton
                text: "Other"
            }

            TextField {
                id: patCC
                opacity: isACCButton.checked? 1:0
                Behavior on opacity {
                    PropertyAnimation {
                        duration: 300
                    }
                }
                Layout.minimumWidth: 200
            }
        }

        CDReviewLauncher {
            id: perReviewButton
            text: "Personal Information Review"
            launchEnum: CDToolLauncher.ReviewPatPersonal
            iniProp: "Personal"
        }

        CDReviewLauncher {
            id: covidButton
            text: "COVID-19 Screening"
            launchEnum: CDToolLauncher.COVID_Screen
            iniProp: "COVID"
        }

        CDReviewLauncher {
            id: vitalButton
            text: "Take Vitals"
            launchEnum: CDToolLauncher.Vitals
            iniProp: "Vitals"
        }

        CDReviewLauncher {
            id: medReviewButton
            text: "Medical Review"
            launchEnum: CDToolLauncher.ReviewPatMedical
            iniProp: "Medical"
        }

        CDReviewLauncher {
            id: takeRadio
            text: "Take Radiographs"
            launchEnum: CDToolLauncher.TakeRadiograph
            iniProp: "Radiograph"
        }

        CDReviewLauncher {
            id: takePhoto
            text: "Take Photographs"
            launchEnum: CDToolLauncher.TakePhotograph
            iniProp: "Photograph"
        }

        CDReviewLauncher {
            id: extraOralButton
            text: "Extra Oral exam"
            launchEnum: CDToolLauncher.ExtraoralExam
            iniProp: "EoE"
        }

        CDReviewLauncher {
            id: intraOralButton
            text: "Intraoral / Soft tissue exam"
            launchEnum: CDToolLauncher.IntraoralExam
            iniProp: "IoE"
        }

        RowLayout {
            Label {
                text: "Oral Cancer screening:";
                font.bold: true
            }
            RadioButton {
                id: ocsNeg
                text: "Negative"
                checked: true
            }
            RadioButton {
                text: "Positive"
            }
        }

        CDReviewLauncher {
            id: perioButton
            text: "Periodontal charting (Last Done: "+perioButton.lastDone+")"
            launchEnum: CDToolLauncher.PerioCharting
            iniProp: "PerioCharting"
        }

        CDReviewLauncher {
            id: perioDxTxButton
            text: "Periodontal Diagnosis and Treatment Planning"
            launchEnum: CDToolLauncher.PerioDxTx
            iniProp: "PerioDxTx"
        }

        CDReviewLauncher {
            id: hardTissueChartButton
            text: "Hard tissue charting"
            launchEnum: CDToolLauncher.HardTissueCharting
            iniProp: "HardTissueExam"
        }

        CDReviewLauncher {
            text: "Fixed/Removable Partial Denture Treatment Planning"
            launchEnum: CDToolLauncher.DentureTx
            iniProp: "DentureTx"
        }

        CDReviewLauncher {
            text: "Review Treatment Plan"
            launchEnum: CDToolLauncher.ReviewTxPlan
            iniProp: "ReviewTx"
        }
    }

}

