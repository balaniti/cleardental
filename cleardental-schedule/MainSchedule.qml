// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

// Updated 5/21/2022 Alex Vernes

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Window 2.10


CDAppWindow {
    id: rootWin

    title: qsTr("Schedule")

    property var selectedDay: new Date(); //default is today
    onSelectedDayChanged: loadUpDay(selectedDay);

    property var selectedDayChairs: []

    property real pixelsPerMin: 4.0
    property int timeMargin: 100
    property string viewMode: "Full"
    property int showTimeMins: 30
    property bool showButtonText: true

    //Everything is minutes since midnight
    property int dayStartTime: 7 * 60
    property int dayEndTime: 17 * 60
    property int breakStart: 12 * 60
    property int breakEnd: 13 * 60
    property bool openThisDay: true
    property bool haveBreakThisDay: true

    property bool dialogShown: false

    SequentialAnimation {
        loops: Animation.Infinite
        running: true
        PauseAnimation {
            duration: 60 * 1000
        }

        ScriptAction {
            script: {
                if(!dialogShown) {
                    loadUpDay(selectedDay);
                }
            }
        }
    }

    //Just store the passwords + local phone number so it doesn't have to be looked up each time
    property string voipUsername: "";
    property string voipPassword: "";
    property string localPhoneNumber: "";


    function loadUpDay(newDay) {
        selectedDayChairs = scheduleDB.getChairs(newDay);
        setOpenAndBreakTimes(newDay);
        currentDayFlickable.loadApptButtons();
        selectDayDia.setDate = newDay;
    }

    function getDateString(theDate) {
        var dd = String(theDate.getDate()).padStart(2, '0');
        var mm = String(theDate.getMonth() + 1).padStart(2, '0');//January is 0!
        var yyyy = theDate.getFullYear();

        var weekday=new Array(7);
        weekday[0]="Sun";
        weekday[1]="Mon";
        weekday[2]="Tues";
        weekday[3]="Wed";
        weekday[4]="Thurs";
        weekday[5]="Fri";
        weekday[6]="Sat";

        var returnMe = weekday[theDate.getDay()] + " " +
                mm + '/' + dd + '/' + yyyy;
        return returnMe;
    }

    function parseTime(inputTime) {
        //11:30 AM
        var hourMin = inputTime.split(":");
        var hour = parseInt(hourMin[0]);
        var min = hourMin[1].substring(0,2);
        var AMPM = inputTime.split(" ")[1];

        if(AMPM === "PM") {
            if(hour !== 12) {
                hour += 12;
            }
        }
        return (60*hour) + parseInt(min);
    }

    function parseTimeBlock(getTimeBlock) {
        var openTimesList=getTimeBlock.split("|");
        haveBreakThisDay = false;
        for(var i=0;i<openTimesList.length;i++) {
            var schChunk = openTimesList[i].trim();
            var schTimeParts = schChunk.split(" ");
            if(schChunk.startsWith("Open")) {
                rootWin.dayStartTime = parseTime(schTimeParts[1] + " " +
                                                 schTimeParts[2]);
            }
            else if(schChunk.startsWith("Closed")) {
                rootWin.dayEndTime = parseTime(schTimeParts[1] + " " +
                                               schTimeParts[2]);
            }
            else if(schChunk.startsWith("Lunch")) {
                haveBreakThisDay = true;
                rootWin.breakStart = parseTime(schTimeParts[6] + " "
                                               + schTimeParts[7]);
                rootWin.breakEnd = breakStart + parseInt(schTimeParts[3]);
            }
        }
    }

    function setOpenAndBreakTimes(getDate) {
        var dayOfWeek = getDate.getDay();
        var openTimes;
        var dayList = ["Sunday","Monday","Tuesday","Wednesday","Thursday",
                "Friday","Saturday"]
        var isOpen = readOpenCloseTimes.value("Open"+dayList[dayOfWeek],"false")
        if( isOpen === "true") {
            openThisDay = true;
            openTimes = readOpenCloseTimes.value(dayList[dayOfWeek]+"Hours","");
            parseTimeBlock(openTimes);
        }
        else {
            openThisDay = false;
        }
    }

    function remindPatient(apptFile) {
        var username = rootWin.voipUsername
        var password = rootWin.voipPassword
        var apiRequestStringStart = "https://voip.ms/api/v1/rest.php?api_username="+username+
                "&api_password="+password+"&method=sendSMS&did=5083873733&dst=";

        apptRdr.fileName = apptFile
        apptRdr.sync();
        var patID = apptRdr.value("PatientID", "");
        var apptChair = apptRdr.value("Chair","");
        var currentStatus = apptRdr.value("Status","");
        patRdr.fileName = fLocs.getPersonalFile(patID);
        patRdr.sync()
        if((patID.length > 1) && (apptChair !== "Waitlist") && (currentStatus !== "Confirmed") &&
                (!JSON.parse(patRdr.value("Phones/DoNotText", false))) ) {

            var temp = "";
            var dst = "";
            if (patRdr.value("Phones/CellPhone", "") !== "")
                temp = patRdr.value("Phones/CellPhone","");
            else if (patRdr.value("Phones/HomePhone", "") !== "")
                temp = patRdr.value("Phones/HomePhone","");
            for (var idx=0; idx<temp.length; idx++) {
                if (temp[idx] <= '9' && temp[idx] >= '0')
                    dst += temp[idx];
            }

            var name = "";
            if (patRdr.value("Name/PreferredName", "").length > 0)
                name = patRdr.value("Name/PreferredName", "");
            else
                name = patRdr.value("Name/FirstName","");

            var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };

            if ((patRdr.value("Preferences/PreferredLanguage","") === "en") ||
                    (patRdr.value("Preferences/PreferredLanguage","") === "")){
                // English translation
                var message1 = "Hello " + name + ", this is Zen Family Dental giving you a reminder text for "+
                        "your appointment on " + selectedDay.toLocaleDateString()
                            + " at " + apptRdr.value("StartTime", "") + ". ";
                var location1 = apiRequestStringStart + dst + "&message=" + message1;
                patReminder.get(location1);

                var message2 = "Please respond YES to confirm appointment or RESCHEDULE if you are unable to"+
                        " make this time.";
                var location2 = apiRequestStringStart + dst + "&message=" + message2;
                patReminder.get(location2);


            } else if (patRdr.value("Preferences/PreferredLanguage","") === "pt") {
                // Portugeuses trnaslation
                message1 = "Ola " + name + ", aqui de Zen Family Dental te relembrando que você tem uma " +
                        "consulta conosco dia " + selectedDay.toLocaleDateString(Qt.locale("pt-BR"))
                            + " at " + apptRdr.value("StartTime", "") + ". ";
                location1 = apiRequestStringStart + dst + "&message=" + message1;
                patReminder.get(location1);

                message2 = "Por favor, responda SIM para confirmar sua consulta ou REAGENDAR se você " +
                        "não puder comparecer.";
                location2 = apiRequestStringStart + dst + "&message=" + message2;
                patReminder.get(location2);


            } else if (patRdr.value("Preferences/PreferredLanguage","") === "es") {
                // Spanish translation
                message1 = "Hola " + name + ", aquí es de Zen Family Dental te recordando de que tienes una " +
                        "cita con nosotros día " + selectedDay.toLocaleDateString(Qt.locale("es-ES"))
                            + " at " + apptRdr.value("StartTime", "") + ". ";
                location1 = apiRequestStringStart + dst + "&message=" + message1;
                patReminder.get(location1);

                message2 = "Por favor, conteste SÍ para confirmar su cita o REPROGRAMAR si no " +
                        "puedes vir para su cita."
                location2 = apiRequestStringStart + dst + "&message=" + message2;
                patReminder.get(location2);
            }
            apptRdr.setValue("Status", "Left Message");

            var messageJSON = writeMessages.readFile(fLocs.getPatientMessagesFile(patID));
            var messageList = [];
            if(messageJSON.length > 1) {
                messageList = JSON.parse(messageJSON);
            }
            var addMe1 = {
                "Time" : Date(),
                "Sender" : "Us ("+rootWin.localPhoneNumber+")",
                "Reciever" : name + " " + patRdr.value("Name/LastName","") + " (" + dst + ")",
                "Contents" : message1,
                "ApptTime" : selectedDay
            };
            messageList.push(addMe1);
            var addMe2 = {
                "Time" : Date(),
                "Sender" : "Us ("+rootWin.localPhoneNumber+")",
                "Reciever" : name + " " + patRdr.value("Name/LastName","") + " (" + dst + ")",
                "Contents" : message2,
                "ApptTime" : selectedDay
            };

            messageList.push(addMe2);
            writeMessages.saveFile(fLocs.getPatientMessagesFile(patID),
                                   JSON.stringify(messageList, null, '\t'));
        }

    }


    CDFileLocations {
        id: fLocs
    }

    CDPatientManager {
        id: patMan
    }

    CDGitManager {
        id: gitMan
    }

    Settings {
        id: readOpenCloseTimes
        fileName: fLocs.getLocalPracticePreferenceFile()
        category: "Hours"
    }

    Settings {
        id: readAccountInfo
        fileName: fLocs.getLocalPracticePreferenceFile()
        category: "Accounts"
        Component.onCompleted: {
            rootWin.voipUsername = readAccountInfo.value("VoipMSUsername","");
            rootWin.voipPassword = readAccountInfo.value("VoipMSPassword","");
        }
    }

    Settings {
        id: readPhoneNumber
        fileName: fLocs.getLocalPracticePreferenceFile()
        category: "GeneralInfo"
        Component.onCompleted: {
            rootWin.localPhoneNumber = readPhoneNumber.value("PhoneNumber","");
        }
    }

    CDScheduleManager {
        id: scheduleDB
    }

    CDConstLoader {
        id: constLoader
    }

    CDGitManager {
        id: gitManager
    }

    CDTextFileManager {
        id: writeMessages
    }

    Settings {
        id: showLabelsSettings
        property alias showLabels: rootWin.showButtonText
    }

    Settings { id: apptRdr }
    Settings { id: patRdr }

    CDPatientReminder {
        id: patReminder
    }

    header: CDBlankToolBar {
        RowLayout {
            anchors.fill: parent

            ToolButton {
                 icon.name: "application-menu"
                 icon.width: 32
                 icon.height: 32
                 onClicked: drawer.open();
             }

            ToolButton {
                id: forceRefresh
                icon.name: "view-refresh"
                text: showButtonText ? "Refresh" : ""
                onClicked: {
                    gitManager.pullData();
                    loadUpDay(selectedDay);
                }
            }
            Label {
                Layout.fillWidth: true
            }

            ToolButton {
                id: backAWeek
                icon.name: "arrow-left-double"
                text: showButtonText ? "One week before" : ""
                onClicked:  {
                     var newDate = selectedDay;
                     newDate.setDate(newDate.getDate()-7);
                    selectedDay = newDate;
                }
            }
            ToolButton {
                id: backADay
                icon.name: "go-previous"
                text: showButtonText ? "One day before" :""
                onClicked:  {
                     var newDate = selectedDay;
                     newDate.setDate(newDate.getDate()-1);
                    selectedDay = newDate;
                }
            }

            ToolButton {
                id: selectDayToolButton

                SelectCurrentDayDialog {
                    id: selectDayDia
                    x: selectDayDia.contentWidth / -4
                    setDate: rootWin.selectedDay
                }

                text: getDateString(selectedDay)
                font.pointSize: 24
                Layout.minimumWidth: 235
                onClicked:  {
                    selectDayDia.open();
                }
            }

            ToolButton {
                id: toToday
                icon.name:  "go-jump-today"
                text: showButtonText ? "Today" : ""
                enabled: selectedDay.toDateString() !=
                         (new Date()).toDateString()
                onClicked:  {
                    selectedDay = new Date();
                }
            }

            ToolButton {
                id: forwardADay
                icon.name: "go-next"
                text: showButtonText ? "Next Day" : ""
                onClicked:  {
                     var newDate = selectedDay;
                     newDate.setDate(newDate.getDate()+1);
                    selectedDay = newDate;
                }
            }

            ToolButton {
                icon.name: "arrow-right-double"
                text: showButtonText ? "Next week" : ""
                onClicked:  {
                    var newDate = selectedDay;
                    newDate.setDate(newDate.getDate()+7);
                    selectedDay = newDate;
                }
            }

            ToolButton {
                icon.name: "go-last"
                text: showButtonText ? "6 Months" : ""
                onClicked:  {
                    var newDate = selectedDay;
                    newDate.setMonth(newDate.getMonth()+6);
                    selectedDay = newDate;
                }
            }

            ToolButton {
                icon.name: "zoom-in"
                text: showButtonText ? "Zoom In" :""
                onClicked:  {
                    pixelsPerMin++;
                    loadUpDay(selectedDay);
                }
                enabled: pixelsPerMin < 10
            }

            ToolButton {
                icon.name: "zoom-out"
                text: showButtonText ? "Zoom Out" : ""
                onClicked:  {
                    pixelsPerMin--;
                    loadUpDay(selectedDay);
                }
                enabled: pixelsPerMin > 1.0
            }
            ToolButton {
                id: searchButton
                SearchAppointmentsDialog {
                    id: searchApptsDia
                    y: 50
                    x: searchApptsDia.contentWidth / -2
                    onJumpDateSelected: {
                        rootWin.selectedDay = newDate
                        searchApptsDia.close();
                    }
                }
                icon.name: "edit-find"
                text: showButtonText ? "Search" : ""
                onClicked: {
                    searchApptsDia.open();
                }
            }

            Label {
                text: "Schedule"
                font.pointSize: 32
                Layout.fillWidth: true
                horizontalAlignment: Qt.AlignHCenter
            }
        }
    }

    ScheduleDrawer {
        id: drawer
        height: rootWin.height
        width:  rootWin.width/4
    }

    Item {
        id: chairHeader
        anchors.top: rootWin.top
        x: timeMargin
        width: rootWin.width - timeMargin
        height: children[0].height
        Repeater {
            model: rootWin.selectedDayChairs.length
            Label {
                id: chairLabel
                text: rootWin.selectedDayChairs[index]
                width: chairHeader.width/rootWin.selectedDayChairs.length
                x: index * width
                horizontalAlignment: Text.AlignHCenter
                font.bold: true
            }
        }
        z: 1024
    }

    CurrentDayFlickable {
        id: currentDayFlickable
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: chairHeader.bottom
        anchors.margins: 0
    }

    AddAptDialog {
        id: addAptDialog
    }

    AddBlockerDialog {
        id: addBlockerDialog
    }

    CDCancelButton {
        text: "Add Closed/Blocker"
        anchors.right: parent.right
        anchors.bottom: addApptButton.top
        anchors.margins: 10
        highlighted: false
        icon.name: "dialog-cancel"
        onClicked: {
            addBlockerDialog.open();
            drawer.close();
        }
    }

    CDAddButton {
        id: addApptButton
        text: "Add Appointment"
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        onClicked: {
            addAptDialog.open();
            drawer.close();
        }
    }

    CDCommonFunctions {
        id: commonFuns
    }

    //This makes it load twice, but fixes strange issues with the
    //Breakes taking up the whole screen
    Component.onCompleted: {
        var nodeType = constLoader.getNodeType();
        if(nodeType.startsWith("touch")) {
            viewMode = "Procedure Only";
        }
        else {
            viewMode = "Full";
        }
        selectedDay: new Date();
        loadUpDay(selectedDay);
    }

    Shortcut {
        sequence: "Right"
        onActivated: {
            var newDate = selectedDay;
            newDate.setDate(newDate.getDate()+1);
            selectedDay = newDate;
        }
    }

    Shortcut {
        sequence: "Left"
        onActivated: {
            var newDate = selectedDay;
            newDate.setDate(newDate.getDate()-1);
            selectedDay = newDate;
        }
    }

    Shortcut {
        sequence: "Up"
        onActivated: {
            currentDayFlickable.flick(0,1000);
        }
    }

    Shortcut {
        sequence: "Down"
        onActivated: {
            currentDayFlickable.flick(0,-1000);
        }
    }

    Shortcut {
        sequence: "T"
        onActivated: {
            selectedDay = new Date();
        }
    }
}
