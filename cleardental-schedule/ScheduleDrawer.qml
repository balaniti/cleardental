// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Window 2.10


Drawer {
    id: drawer

    background: Rectangle {
        gradient: Gradient {
                 GradientStop { position: 0.0; color: "white" }
                 GradientStop { position: 0.5; color: "white" }
                 GradientStop { position: 1.0; color: "transparent" }
             }
        anchors.fill: parent
    }

    ColumnLayout {
        anchors.left:  parent.left
        anchors.right: parent.right
        anchors.margins: 20
        
        RowLayout {
            Layout.fillWidth: true
            Label {
                text: "View Mode"
                font.bold: true
            }
            ComboBox {
                Layout.fillWidth: true
                model: ["Full", "Procedure Only"]
                onCurrentIndexChanged: {
                    rootWin.viewMode = model[currentIndex]
                }
                
            }
        }
        
        RowLayout {
            Layout.fillWidth: true
            Label {
                text: "Display the time"
                font.bold: true
            }
            ComboBox {
                Layout.fillWidth: true
                textRole: "showText"
                valueRole: "value"
                
                model: ListModel {
                    ListElement {
                        showText: "Every 30 mins"
                        value: 30
                    }
                    ListElement {
                        showText: "Every 10 mins"
                        value: 10
                    }
                    ListElement {
                        showText: "Every hour"
                        value: 60
                    }
                }
                onCurrentValueChanged: {
                    if(typeof currentValue !== "undefined") {
                        rootWin.showTimeMins = currentValue
                    }
                }
            }
        }

        CheckBox {
            text: "Show Header Button Labels"
            checked: showButtonText
            onClicked: {
                showButtonText = checked
            }
        }

        MenuSeparator{Layout.fillWidth: true}


        CDToolLauncher {
            id: toolLauncher
        }

        CDButton {
            text: "Patient Roster"
            onClicked: {
                toolLauncher.launchTool(CDToolLauncher.SelectPatient);
            }
        }
        CDButton {
            text: "Import New Patients"
            onClicked: {
                toolLauncher.launchTool(CDToolLauncher.NewPatient);
            }
        }
        MenuSeparator{Layout.fillWidth: true}
        CDButton {
            text: "Production Report"
            onClicked: {
                toolLauncher.launchTool(CDToolLauncher.ProductionReport);
            }
        }
        CDButton {
            text: "Claims"
            onClicked: {
                toolLauncher.launchTool(CDToolLauncher.DentalPlanClaims);
            }
        }
        MenuSeparator{Layout.fillWidth: true}

        CDButton {
            text: "Send out reminder texts"

            onClicked: {
                var appts = scheduleDB.getAppointments(selectedDay);

                for (var i = 0; i < appts.length; i++) {
                    rootWin.remindPatient(appts[i])
                }

                gitMan.commitData("Sent out reminder texts for " + selectedDay);
                doneLabel.text = "Sent out texts succesfully for " + selectedDay;
            }
        }

        Label {
            id: doneLabel
            color: Material.color(Material.Green)
        }

    }
}
