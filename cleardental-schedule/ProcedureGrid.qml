// Copyright 2022 Clear.Dental; Alex Vernes
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQml.Models 2.1
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1
import dental.clear 1.0

ColumnLayout {

    function launchProcedure(procedureName,index) {
        var age = commonFuns.getYearsOld(schButton.patientID);
        if(procedureName.startsWith("Comprehensive Exam")) {
            if(age <= 13) {
                toolLauncher.launchTool(CDToolLauncher.PediatricExam,schButton.patientID,
                                        JSON.stringify(procedRepeater.procedureItemObjs[index]));
            }
            else {
                toolLauncher.launchTool(CDToolLauncher.CompExam,schButton.patientID);
            }
        }
        else if(procedureName.startsWith("Periodic Exam")) {
            if(age <= 13) {
                toolLauncher.launchTool(CDToolLauncher.PediatricExam,schButton.patientID,
                                        JSON.stringify(procedRepeater.procedureItemObjs[index]));
            }
            else {
                toolLauncher.launchTool(CDToolLauncher.PeriodicExam,schButton.patientID);
            }
        }
        else if(procedureName.startsWith("Limited Exam")) {
            toolLauncher.launchTool(CDToolLauncher.LimitedExam,schButton.patientID);
        }
        else if( procedureName.startsWith("Prophy") || procedureName.startsWith("Adult Prophy") ) {
            toolLauncher.launchTool(CDToolLauncher.Prophy,schButton.patientID);
        }
        else if(procedureName.startsWith("Full Mouth debridement")) {
            toolLauncher.launchTool(CDToolLauncher.Prophy,schButton.patientID);
        }
        else if(procedureName.includes("Denture")) {
            toolLauncher.launchTool(CDToolLauncher.RemPros,schButton.patientID);
        }
        else if(procedureName.startsWith("Extraction")) {
            toolLauncher.launchTool(CDToolLauncher.Extraction,schButton.patientID,
                                   JSON.stringify(procedRepeater.procedureItemObjs[index]));
        }
        else if(procedureName.startsWith("Regular Extraction")) {
            toolLauncher.launchTool(CDToolLauncher.Extraction,schButton.patientID,
                                   JSON.stringify(procedRepeater.procedureItemObjs[index]));
        }
        else if(procedureName.startsWith("Composite")) {
            toolLauncher.launchTool(CDToolLauncher.Operative,schButton.patientID,
                                   JSON.stringify(procedRepeater.procedureItemObjs[index]));
        }
        else if(procedureName.startsWith("Periodontal Follow")) {
            toolLauncher.launchTool(CDToolLauncher.PeriodontalFollowup,schButton.patientID);
        }
        else if(procedureName.includes("Recement crown")) {
            toolLauncher.launchTool(CDToolLauncher.Generic_Procedure,schButton.patientID,
                                   JSON.stringify(procedRepeater.procedureItemObjs[index]));
        }
        else if(procedureName.includes("Crown")) {
            toolLauncher.launchTool(CDToolLauncher.FixedPros,schButton.patientID,
                                   JSON.stringify(procedRepeater.procedureItemObjs[index]));
        }
        else if(procedureName.includes("crown")) {
            toolLauncher.launchTool(CDToolLauncher.FixedPros,schButton.patientID,
                                   JSON.stringify(procedRepeater.procedureItemObjs[index]));
        }
        else if(procedureName.includes("Sealant")) {
            toolLauncher.launchTool(CDToolLauncher.Sealant,schButton.patientID,
                                   JSON.stringify(procedRepeater.procedureItemObjs[index]));
        }
        else if(procedureName.includes("Composite")) {
            toolLauncher.launchTool(CDToolLauncher.Operative,schButton.patientID,
                                   JSON.stringify(procedRepeater.procedureItemObjs[index]));
        }
        else if(procedureName.includes("RCT")) {
            toolLauncher.launchTool(CDToolLauncher.RCT,schButton.patientID,
                                   JSON.stringify(procedRepeater.procedureItemObjs[index]));
        }
        else if(procedureName.includes("SCRP")) {
            toolLauncher.launchTool(CDToolLauncher.SCRP,schButton.patientID,
                                   JSON.stringify(procedRepeater.procedureItemObjs[index]));
        }
        else if(procedureName.includes("Scaling & Root Planing")) {
            toolLauncher.launchTool(CDToolLauncher.SCRP,schButton.patientID,
                                   JSON.stringify(procedRepeater.procedureItemObjs[index]));
        }
        else if(procedureName.includes("Space Maintainer")) {
            toolLauncher.launchTool(CDToolLauncher.SpaceMaintainer,schButton.patientID,
                                   JSON.stringify(procedRepeater.procedureItemObjs[index]));
        }
        else if(procedureName.includes("RPD")) {
            toolLauncher.launchTool(CDToolLauncher.RemPros,schButton.patientID);
        }

        else {
            toolLauncher.launchTool(CDToolLauncher.Generic_Procedure,schButton.patientID,
                                   JSON.stringify(procedRepeater.procedureItemObjs[index]));
        }
    }

    CDHeaderLabel {
        text: "Procedures"
        Layout.columnSpan: 2
    }

    Flickable {
        Layout.maximumHeight: 175
        Layout.minimumHeight: 175
//        Layout.fillWidth: true
        Layout.minimumWidth: procedureColLayout.width
        contentHeight: procedureColLayout.height
        contentWidth: procedureColLayout.width
        clip: true
        Layout.columnSpan: 2

        ScrollBar.vertical: ScrollBar{}

        ColumnLayout {
            id: procedureColLayout

            Repeater {
                id: procedRepeater
                property var procedureItemObjs: [];
                model: procedureItemObjs.length

                function refreshProcedureList() {
                    procedureItemObjs = JSON.parse(schButton.procedures)
                }

                RowLayout {
                    Label {
                        text: commonFuns.makeTxItemString(procedRepeater.procedureItemObjs[index])
                    }
                    Button {
                        text: "Start Procedure"
                        onClicked: {
                            var pro = procedRepeater.procedureItemObjs[index]["ProcedureName"];
                            launchProcedure(pro,index);


                        }
                    }
                }

                Component.onCompleted: {
                    refreshProcedureList();
                }
            }
        }
    }

    RowLayout {
        Layout.columnSpan: 2
        Button {
            text: "Manage Procedures"
            ManageProceduresDia {
                id: changeDia
                onAccepted: {
                    schButton.procedures = JSON.stringify( changeDia.currentTxPlanList)
                    procedRepeater.refreshProcedureList();
                }
            }

            onClicked: {
                changeDia.patientID = patientID;
                changeDia.currentTxPlanList = JSON.parse( schButton.procedures)
                changeDia.open();
            }
        }

        Button {
            text: "Edit text file"
            onClicked: {
                toolLauncher.launchKate(schButton.fileName);
            }
            CDConstLoader {
                id: constsDebug
            }
            visible: constsDebug.isDebugMode();
        }
    }
}
