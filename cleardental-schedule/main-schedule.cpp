// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickImageProvider>
#include <QFont>
#include <QIcon>
#include <QDebug>
#include <QCoreApplication>

#include "cdpatientfilemanager.h"
#include "cddefaults.h"
#include "cdschedulemanager.h"
#include "cdappointment.h"
#include "cdpatientmanager.h"
#include "cdfilelocations.h"
#include "cddoctorlistmanager.h"
#include "cdpatientreminder.h"

#include "cdspellchecker.h"

int main(int argc, char *argv[])
{
    CDDefaults::setAppDefaults();
    QCoreApplication::setApplicationName("ClearDental-Schedule");

    QGuiApplication app(argc, argv);
    app.setFont(QFont("Barlow Semi Condensed"));
    app.setWindowIcon(CDDefaults::defaultColorIcon());

    CDDefaults::registerQMLTypes();
    qmlRegisterType<CDScheduleManager>("dental.clear", 1, 0, "CDScheduleManager");
    qmlRegisterType<CDAppointment>("dental.clear", 1, 0, "CDAppointment");
    qmlRegisterType<CDPatientManager>("dental.clear", 1, 0, "CDPatientManager");
    qmlRegisterType<CDFileLocations>("dental.clear", 1, 0, "CDFileLocations");
    qmlRegisterType<CDDoctorListManager>("dental.clear", 1, 0,"CDDoctorListManager");
    qmlRegisterType<CDPatientReminder>("dental.clear", 1, 0,"CDPatientReminder");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/MainSchedule.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    CDDefaults::enableBlurBackground();

    return app.exec();
}
