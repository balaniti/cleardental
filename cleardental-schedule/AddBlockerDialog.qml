// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Window 2.10


CDTranslucentDialog {
    id: addBlockerDialog

    onVisibleChanged: {
        blDate.myDate = selectedDay;

        //I have no clue why I have to do this, but it is the only way
        //to make the model show up in the ComboBox
        var setMyModel= [];
        for(var i=0;i<rootWin.selectedDayChairs.length;i++) {
            setMyModel.push(rootWin.selectedDayChairs[i]);
        }
        chairBox.model = setMyModel;

        rootWin.dialogShown = visible;
    }

    ColumnLayout {

        CDTranslucentPane {
            backMaterialColor: Material.LightBlue

            GridLayout {
                columns: 2

                Label {
                    text: "Block out time"
                    font.pointSize: 24
                    font.underline: true
                    Layout.columnSpan: 2
                }

                Label {
                    text: "Date"
                    font.bold: true
                }

                CDCalendarDatePicker {
                    id: blDate
                }

                Label {
                    text: "Length"
                    font.bold: true
                }
                RowLayout {
                    RadioButton {
                        id: yesFullDay
                        text: "Full day"
                    }
                    RadioButton {
                        checked: true
                        text: "Specific time"
                    }
                }

                Label {
                    visible: !yesFullDay.checked
                    text: "Start Time"
                    font.bold: true
                }

                RowLayout {
                    visible: !yesFullDay.checked
                    ComboBox {
                        id: startHour
                        model: ["1","2","3","4","5","6","7","8","9","10","11","12"]
                        Layout.preferredWidth: 75
                        currentIndex: 7
                    }

                    Label {
                        text: ":"
                    }

                    ComboBox {
                        id: startMin
                        property var minModel: []
                        Layout.preferredWidth: 75
                        Component.onCompleted:  {
                            for(var i=0;i<60;i+=5) {
                                if(i < 10) {
                                    minModel.push("0"+i);
                                }
                                else {
                                    minModel.push(i);
                                }
                            }
                            model = minModel
                        }
                    }

                    ComboBox {
                        id: startAMPM
                        model: ["AM","PM"]
                        Layout.preferredWidth: 75
                    }
                }

                Label {
                    visible: !yesFullDay.checked
                    text: "Duration"
                    font.bold: true
                }
                ComboBox {
                    id: durationBox
                    visible: !yesFullDay.checked
                    property var durModel: []
                    Layout.fillWidth: true
                    Component.onCompleted:  {
                        for(var i=15;i<=480;i+=15) {
                            var hours = Math.floor(i / 60);
                            var mins = i%60;
                            if(hours === 0) {
                                durModel.push(mins + " minutes")
                            }
                            else if(hours === 1) {
                                if(mins === 0) {
                                    durModel.push("1 hour")
                                }
                                else {
                                    durModel.push("1 hour and " + mins + " minutes")
                                }
                            }
                            else {
                                if(mins === 0) {
                                    durModel.push(hours + " hours")
                                }
                                else {
                                    durModel.push(hours + " hours and " + mins +
                                                  " minutes")
                                }
                            }
                        }
                        model = durModel
                        currentIndex =3;
                    }
                }
                Label {
                    text: "Block Columns"
                    font.bold: true
                }
                RowLayout {
                    RadioButton {
                        id: singleChair
                        text: "Single Chair"
                        checked: true
                    }
                    RadioButton {
                        text: "Whole clinic"
                    }
                }
                Label {
                    visible: singleChair.checked
                    text: "Chair"
                    font.bold: true
                }
                ComboBox {
                    visible: singleChair.checked
                    id: chairBox
                    Layout.fillWidth: true
                }
                Label {
                    text: "Comment / Reason"
                    font.bold: true
                }
                TextField {
                    id: commentField
                    Layout.fillWidth: true
                    selectByMouse: true
                }


            }

        }

        RowLayout {
            Layout.fillWidth: true
            Layout.columnSpan: 2

            CDCancelButton {
                text: "Cancel"
                onClicked: {
                    addBlockerDialog.close();
                }
            }

            Label {
                Layout.fillWidth: true
            }

            CDAddButton {
                text: "Add Blockout"
                onClicked: {
                    var addDate = blDate.myDate;
                    var addTime = (startHour.currentIndex + 1) + ":" +
                            startMin.displayText + " " + startAMPM.currentText;
                    var addChair = chairBox.displayText;
                    var addDuration = (durationBox.currentIndex+1) * 15;
                    var addComment = commentField.text;
                    var addAllDay =yesFullDay.checked;
                    var addAllChairs = !singleChair.checked;
                    scheduleDB.addBlocker(addDate, addAllDay, addTime,
                                              addAllChairs, addChair,
                                              addDuration,addComment);
                    currentDayFlickable.loadApptButtons();
                    addBlockerDialog.close();
                }

            }
        }
    }
}
