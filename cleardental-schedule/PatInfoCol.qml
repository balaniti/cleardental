// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

// Updated 5/22/2022 Alex Vernes

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQml.Models 2.1
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1
import dental.clear 1.0

ColumnLayout {
    id: patInfoCol

    CDHeaderLabel {
        text: "Review/Edit Information"
    }

    GridLayout {
        id: patInfoRowSolo
        columns: 2
        Button {
            text: "Personal"
            onClicked: {
                toolLauncher.launchTool(CDToolLauncher.ReviewPatPersonal,schButton.patientID);
            }
        }

        Button {
            text: "Medical"
            onClicked: {
                toolLauncher.launchTool(CDToolLauncher.ReviewPatMedical,schButton.patientID);
            }
        }

        Button {
            text: "Prescription"
            onClicked: {
                toolLauncher.launchTool(CDToolLauncher.Prescription,schButton.patientID);
            }
        }

        Button {
            text: "Case notes"
            onClicked: {
                toolLauncher.launchTool(CDToolLauncher.ReviewCaseNotes,schButton.patientID);
            }
        }

        Button {
            text: "Schedule Patient"
            onClicked: {
                toolLauncher.launchTool(CDToolLauncher.SchedulePatient,schButton.patientID);
            }
        }

        Button {
            text: "Vitals"
            onClicked: {
                toolLauncher.launchTool(CDToolLauncher.Vitals,schButton.patientID);
            }
        }

        Button {
            text: "Tx Plan"
            onClicked: {
                toolLauncher.launchTool(CDToolLauncher.ReviewTxPlan,schButton.patientID);
            }
        }

        Button {
            text: "Radiographs"
            onClicked: {
                toolLauncher.launchTool(CDToolLauncher.ReviewRadiographs,schButton.patientID);
            }
        }

        Button {
            text: "Billing"
            onClicked: {
                toolLauncher.launchTool(CDToolLauncher.Billing,schButton.patientID);
            }
        }
        Button {
            text: "Old Billing"
            onClicked: {
                toolLauncher.launchTool(CDToolLauncher.LegacyBilling,schButton.patientID);
            }
        }

        Button {
            text: "Messaging"
            onClicked: {
                toolLauncher.launchTool(CDToolLauncher.Messaging,schButton.patientID);
            }
        }

        Button {
            text: "View Folder"
            onClicked: {
                toolLauncher.launchDolphin(fileLocs.getPatientDirectory(schButton.patientID))
            }
        }

        Button {
            text: "Hard Tissue"
            onClicked: {
                toolLauncher.launchTool(CDToolLauncher.HardTissueCharting,schButton.patientID);
            }
        }
        Button {
            text: "Perio DxTx"
            onClicked: {
                toolLauncher.launchTool(CDToolLauncher.PerioDxTx,schButton.patientID);
            }
        }
    }
}
