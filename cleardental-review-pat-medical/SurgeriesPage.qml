// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

// Updated 5/21/2022 Alex Vernes

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12

Page {
    id: surgeryPage

    // Save all data
    function saveData() {
        var jsonData = [];
        for(var i=5;i<surgeryGrid.children.length;i+=4) {
            if(surgeryGrid.children[i].text.length > 2) {
                var addMe = {"Procedure": surgeryGrid.children[i].text,
                    "When": surgeryGrid.children[i+1].text,
                    "AdditionalInfo": surgeryGrid.children[i+2].text};
                jsonData.push(addMe);
            }
        }
        var jsonString= JSON.stringify(jsonData, null, '\t');
        textManager.saveFile(fileLocs.getSurgeriesFile(PATIENT_FILE_NAME),jsonString);

        rootWin.updateReview();
        gitManager.commitData("Updated surgeries for " + PATIENT_FILE_NAME);
    }

    background: Rectangle {
        color: "white"
        opacity: 0
    }

    CDGitManager {id: gitManager}

    CDTextFileManager {id: textManager}

    Label {
        id: noConditionsLabel
        text: "There are currently no recorded surgeries"
        anchors.fill: parent
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.pointSize: 24
        visible: opacity > 0
        opacity: surgeryGrid.children.length < 6
        Behavior on opacity {
            PropertyAnimation {
                duration: 300
            }
        }
    }


    CDTranslucentPane {
        id: surgeryPane
        anchors.centerIn: parent
        visible: opacity > 0
        opacity: surgeryGrid.children.length > 6
        backMaterialColor: Material.Yellow
        Behavior on opacity {
            PropertyAnimation {
                duration: 300
            }
        }

        GridLayout {
            id: surgeryGrid
            columns: 4

            columnSpacing: 25

            Label {
                text: "Patient's Past Surgeries"
                //font.family: "Montserrat"
                font.pointSize: 24
                horizontalAlignment: Qt.AlignHCenter
                font.underline: true
                Layout.columnSpan: 4
            }

            Label {
                font.bold: true
                text: "What what done"
            }

            Label {
                font.bold: true
                text: "When"
            }

            Label {
                font.bold: true
                text: "Additional information"
            }

            Label { //just for placeholder for delete button

            }

            Component {
                id: textFieldComp
                TextField {
                    Layout.minimumWidth: 250
                }
            }

            Component {
                id: deleteButton
                Button {
                    icon.name: "list-remove"
                    text: "Remove Surgery"
                    onClicked: {
                        text = "Kill me now!";
                        surgeryGrid.killTheZombie();
                    }
                    Material.accent: Material.Red
                    highlighted: true
                }
            }


            Component.onCompleted:  {
                var result = JSON.parse(textManager.readFile(fileLocs.getSurgeriesFile(PATIENT_FILE_NAME)));
                for(var i=0;i<result.length;i++) {
                    var obj = result[i];
                    textFieldComp.createObject(surgeryGrid,{"text":obj["Procedure"]});
                    textFieldComp.createObject(surgeryGrid,{"text":obj["When"]});
                    textFieldComp.createObject(surgeryGrid,{"text":obj["AdditionalInfo"]});
                    deleteButton.createObject(surgeryGrid,{});
                }
            }

            function makeNewRow() {
                textFieldComp.createObject(surgeryGrid,{"placeholderText":"Procedure done"});
                textFieldComp.createObject(surgeryGrid,{"placeholderText":"When it was done"});
                textFieldComp.createObject(surgeryGrid,{"placeholderText":"More Information"});
                deleteButton.createObject(surgeryGrid,{});
            }

            function killTheZombie() {
                for(var i=5;i<surgeryGrid.children.length;i+=4) {
                    var killBut = surgeryGrid.children[i+3];
                    if(killBut.text === "Kill me now!") {
                        var kill1 = surgeryGrid.children[i];
                        var kill2 = surgeryGrid.children[i+1];
                        var kill3 = surgeryGrid.children[i+2];
                        kill1.destroy();
                        kill2.destroy();
                        kill3.destroy();
                        killBut.destroy();
                        return; //probably only one zombie to kill
                    }
                }
            }
        }
    }



    Button {
        id: addButton
        icon.name: "list-add"
        text: "Add Surgery"
        anchors.left: surgeryPane.left
        anchors.top: surgeryPane.bottom
        anchors.margins: 10
        Material.accent: Material.Green
        highlighted: true
        onClicked: surgeryGrid.makeNewRow();

    }

    CDSaveButton {
        id: saveButton
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: 5
        onClicked: {
            saveData();
            showSaveToolTip();
        }

    }
    Button {
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        highlighted: true
        Material.accent: Material.Teal
        icon.name: "preflight-verifier"
        text: "Save and Exit"
        icon.width: 32
        icon.height: 32
        font.pointSize: 18
        onClicked: {
            saveData();
            Qt.quit();
        }
    }
}
