// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickImageProvider>
#include <QFont>
#include <QIcon>
#include <QDebug>

#include "cddefaults.h"
#include "cddruglist.h"

int main(int argc, char *argv[])
{
    CDDefaults::setAppDefaults();
    QCoreApplication::setApplicationName("ClearDental-Review-Pat-Medical");

    QGuiApplication app(argc, argv);
    app.setWindowIcon(CDDefaults::defaultColorIcon());

    if(QCoreApplication::arguments().count()<2) {
        qDebug()<<"You need to give the patient's name as an argument!";
        return -32;
    }

    CDDefaults::registerQMLTypes();
    qmlRegisterType<CDDrugList>("dental.clear", 1, 0, "CDDrugList");


    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("PATIENT_FILE_NAME",QString(argv[1]));
    engine.load(QUrl(QStringLiteral("qrc:/MainReviewPatMedical.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    CDDefaults::enableBlurBackground();

    return app.exec();
}
