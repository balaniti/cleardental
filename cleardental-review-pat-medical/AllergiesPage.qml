// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

// Updated 5/21/2022 Alex Vernes

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12

Page {
    id: allergyPage

    // Saves all data
    function saveData() {
        var jsonData = [];
        for(var i=4;i<allergyGrid.children.length;i+=3) {
            if(allergyGrid.children[i].text.length > 2) {
                var addMe = {"AllergyName": allergyGrid.children[i].text,
                    "AllergyReaction": allergyGrid.children[i+1].editText};
                jsonData.push(addMe);
            }
        }
        var jsonString= JSON.stringify(jsonData, null, '\t');
        textManager.saveFile(fileLocs.getAllergiesFile(PATIENT_FILE_NAME),jsonString);
        rootWin.updateReview();
        gitManager.commitData("Updated allergies for " + PATIENT_FILE_NAME);
    }

    background: Rectangle {
        color: "white"
        opacity: 0
    }

    CDGitManager {id: gitManager}

    CDTextFileManager {id: textManager}

    Label {
        id: noAllergiesLabel
        text: "There are currently no recorded allergies"
        anchors.fill: parent
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.pointSize: 24
        visible: opacity > 0
        opacity: allergyGrid.children.length < 5
        Behavior on opacity {
            PropertyAnimation {
                duration: 300
            }
        }
    }


    CDTranslucentPane {
        id: allergyPane
        anchors.centerIn: parent
        backMaterialColor: Material.Purple
        visible: opacity > 0
        opacity: allergyGrid.children.length > 5

        GridLayout {
            id: allergyGrid
            columns: 3

            columnSpacing: 25

            Behavior on opacity {
                PropertyAnimation {
                    duration: 300
                }
            }

            Label {
                text: "Patient's known allergies"
                font.pointSize: 24
                horizontalAlignment: Qt.AlignHCenter
                font.underline: true
                Layout.columnSpan: 3
            }

            Label {
                font.bold: true
                text: "Allergy"
            }
            Label {
                font.bold: true
                text: "Reaction"
            }

            Label { //just for placeholder to make a new row

            }

            Component {
                id: textFieldComp
                TextField {
                    Layout.minimumWidth: 300
                }
            }

            Component {
                id: comboBoxComp
                ComboBox {
                    property var setMe;
                    model: ["Unknown", "Mild / Rash" , "Moderate", "Severe"]
                    editable: true
                    Layout.minimumWidth: 200
                    Component.onCompleted: {
                        if(setMe) {
                            var setIndex = find(setMe);
                            if(setIndex >-1) {
                                currentIndex = setIndex;
                            } else {
                                model =  [setMe,"Unknown", "Mild / Rash" , "Moderate", "Severe"];
                                currentIndex=0;
                            }
                        }
                    }
                }
            }

            Component {
                id: deleteButton
                Button {
                    icon.name: "list-remove"
                    text: "Remove Allergy"
                    onClicked: {
                        text = "Kill me now!";
                        allergyGrid.killTheZombie();
                    }
                    Material.accent: Material.Red
                    highlighted: true
                }
            }

            Component.onCompleted:  {
                var result = JSON.parse(textManager.readFile(fileLocs.getAllergiesFile(PATIENT_FILE_NAME)));
                for(var i=0;i<result.length;i++) {
                    var obj = result[i];
                    textFieldComp.createObject(allergyGrid,{"text":obj["AllergyName"]});
                    comboBoxComp.createObject(allergyGrid,{"setMe":obj["AllergyReaction"]});
                    deleteButton.createObject(allergyGrid,{});
                }
            }

            function makeNewRow() {
                textFieldComp.createObject(allergyGrid,{"placeholderText":"Allergy Name"});
                comboBoxComp.createObject(allergyGrid,{"placeholderText":"Condition Information"});
                deleteButton.createObject(allergyGrid,{"placeholderText":"Condition Information"});
            }

            function killTheZombie() {
                for(var i=4;i<allergyGrid.children.length;i+=3) {
                    var killBut = allergyGrid.children[i+2];
                    if(killBut.text === "Kill me now!") {
                        var kill1 = allergyGrid.children[i];
                        var kill2 = allergyGrid.children[i+1];
                        kill1.destroy();
                        kill2.destroy();
                        killBut.destroy();
                        return; //probably only one zombie to kill
                    }
                }
            }
        }

    }



    Button {
        id: addButton
        icon.name: "list-add"
        text: "Add Allergy"
        anchors.left: allergyPane.left
        anchors.top: allergyPane.bottom
        anchors.margins: 10
        Material.accent: Material.Green
        highlighted: true
        onClicked: allergyGrid.makeNewRow();
    }

    CDSaveButton {
        id: saveButton
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: 10
        onClicked: {
            saveData();
            showSaveToolTip();
        }
    }
    Button {
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        highlighted: true
        Material.accent: Material.Teal
        icon.name: "preflight-verifier"
        text: "Save and Exit"
        icon.width: 32
        icon.height: 32
        font.pointSize: 18
        onClicked: {
            saveData();
            Qt.quit();
        }
    }
}
