// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

// Updated 5/21/2022 Alex Vernes

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1

Page {
    id: conditionPage

    // Saves all data
    function saveData() {
        if(pregGrid.visible) {
            otherMedFile.category = "Pregnancy";
            otherMedFile.setValue("IsPregnant",yesPregnant.checked);
            otherMedFile.setValue("DueDate",dueDatePicker.getDate());
            otherMedFile.setValue("OBGYN",patOBGYN.text);
            otherMedFile.setValue("IsBreastfeeding",
                                  yesBreastfeeding.checked);
        }
        otherMedFile.category = "AntibioticPreMed";
        otherMedFile.setValue("NeedPreMed",yesPremed.checked);
        otherMedFile.setValue("WhatTakePremed",whichPremed.text);
        otherMedFile.setValue("WhoToldTakePremed",whoTellingPremed.text);
        otherMedFile.setValue("WhyTakePremed",whyPremed.text);

        otherMedFile.category = "Cancer";
        otherMedFile.setValue("HxOfCancer",yesCancer.checked);
        otherMedFile.setValue("CancerType",cancerType.text);
        otherMedFile.setValue("HxChemo",yesChemotherapy.checked);
        otherMedFile.setValue("HxRad",yesHeadNeckRad.checked);

        otherMedFile.category = "Smoking";
        otherMedFile.setValue("DoesSmoke",doesSmoke.checked);
        otherMedFile.setValue("HowMany",smokeOften.currentText);
        otherMedFile.setValue("HowSpan",smokeSpan.currentValue);

        otherMedFile.category = "Alerts";
        otherMedFile.setValue("MedicalAlert",alertField.text);

        otherMedFile.sync();

        rootWin.updateReview();

        gitManager.commitData("Updated other medical info for " + PATIENT_FILE_NAME);
    }

    background: Rectangle {
        color: "white"
        opacity: 0
    }

    CDGitManager {id: gitManager}

    Settings {
        id: otherMedFile
        fileName: fileLocs.getOtherMedInfoFile(PATIENT_FILE_NAME);
    }

    Settings {
        id: personalFile
        fileName: fileLocs.getPersonalFile(PATIENT_FILE_NAME);
        category: "Personal"
    }


    ColumnLayout {
        id: conditionGrid
        anchors.centerIn: parent

        RowLayout {

            Layout.alignment: Qt.AlignHCenter

            CDTranslucentPane {
                backMaterialColor: Material.Red
                Layout.alignment: Qt.AlignHCenter

                ColumnLayout {
                    CDHeaderLabel {
                        text: "Medical / Clinical Alerts"
                    }
                    TextField {
                        id: alertField
                        Layout.fillWidth: true
                    }
                }
            }

            CDTranslucentPane {
                backMaterialColor: Material.Orange
                Layout.alignment: Qt.AlignHCenter

                ColumnLayout {
                    CDHeaderLabel {
                        text:"Smoking"
                    }

                    CheckBox {
                        id: doesSmoke
                        text: "Patient Smokes"
                    }

                    RowLayout {
                        visible: doesSmoke.checked
                        CDDescLabel {
                            text: "How often"
                        }
                        ComboBox {
                            id: smokeOften
                            Layout.minimumWidth: 150
                            model: ["One cigarette","A Pack", "More than a pack"]
                            editable: true
                        }
                        CDDescLabel {
                            text: "per"
                        }
                        ComboBox {
                            id: smokeSpan
                            model: ["Day","Week","Year"]
                        }
                    }
                }
            }

        }



        CDTranslucentPane {
            id: pregGrid
            visible: false
            backMaterialColor: Material.BlueGrey
            Layout.alignment: Qt.AlignHCenter

            GridLayout {

                columns: 2

                Label {
                    text: "Pregnancy"
                    font.underline: true
                    font.pointSize: 24
                    Layout.columnSpan: 2
                }

                Label {
                    text:"Is currently pregnant?"
                    font.bold: true
                }

                RowLayout {
                    RadioButton {
                        id: yesPregnant
                        text: "Yes"
                    }
                    RadioButton {
                        text: "No"
                        checked: true
                    }
                }

                Label {
                    text: "Due Date"
                    font.bold: true
                    enabled: yesPregnant.checked
                }

                DatePicker {
                    id: dueDatePicker
                    enabled: yesPregnant.checked
                }

                Label {
                    text: "Patient's OBGYN"
                    font.bold: true
                    enabled: yesPregnant.checked
                }

                TextField {
                    id: patOBGYN
                    enabled: yesPregnant.checked
                    Layout.fillWidth: true
                }


                Label {
                    text:"Is currently breastfeeding?"
                    font.bold: true
                }

                RowLayout {
                    RadioButton {
                        id: yesBreastfeeding
                        text: "Yes"
                    }
                    RadioButton {
                        text: "No"
                        checked: true
                    }
                }
            }
        }

        RowLayout {

            CDTranslucentPane {
                backMaterialColor: Material.Purple
                Layout.alignment: Qt.AlignHCenter

                GridLayout {
                    id: premedGrid
                    columns: 2
                    Label {
                        text: "Antibiotic Premed"
                        font.underline: true
                        font.pointSize: 24
                        Layout.columnSpan: 2
                    }

                    Label {
                        text:"Is taking antibiotic premed?"
                        font.bold: true
                    }

                    RowLayout {
                        RadioButton {
                            id: yesPremed
                            text: "Yes"
                        }
                        RadioButton {
                            text: "No"
                            checked: true
                        }
                    }

                    Label {
                        text: "Which one normally"
                        font.bold: true
                        enabled: yesPremed.checked
                    }

                    TextField {
                        id: whichPremed
                        enabled: yesPremed.checked
                        Layout.fillWidth: true
                    }

                    Label {
                        text: "Doctor telling the patient to take the premed"
                        font.bold: true
                        enabled: yesPremed.checked
                    }

                    TextField {
                        id: whoTellingPremed
                        enabled: yesPremed.checked
                        Layout.fillWidth: true
                        Layout.minimumWidth: 250
                    }

                    Label {
                        text: "Main reason for premed"
                        font.bold: true
                        enabled: yesPremed.checked
                    }

                    TextField {
                        id: whyPremed
                        enabled: yesPremed.checked
                        Layout.fillWidth: true
                    }
                    Label {
                        text: "Did the patient take the premed today?"
                        font.bold: true
                        enabled: yesPremed.checked
                    }
                    RowLayout {
                        enabled: yesPremed.checked
                        RadioButton {
                            id: yesTookPremed
                            text: "Yes"
                        }
                        RadioButton {
                            text: "No"
                            checked: true
                        }
                    }
                }
            }

            CDTranslucentPane {
                backMaterialColor: Material.Yellow
                Layout.alignment: Qt.AlignHCenter

                GridLayout {
                    id: cancerGrid
                    columns: 2
                    Label {
                        text: "History of Cancer"
                        font.underline: true
                        font.pointSize: 24
                        Layout.columnSpan: 2
                    }

                    Label {
                        text:"Any history of cancer?"
                        font.bold: true
                    }

                    RowLayout {
                        RadioButton {
                            id: yesCancer
                            text: "Yes"
                        }
                        RadioButton {
                            text: "No"
                            checked: true
                        }
                    }

                    Label {
                        text: "What kind and when"
                        font.bold: true
                        enabled: yesCancer.checked
                    }

                    TextField {
                        id: cancerType
                        enabled: yesCancer.checked
                        Layout.fillWidth: true
                    }

                    Label {
                        text: "Taking chemotherapy?"
                        font.bold: true
                        enabled: yesCancer.checked
                    }

                    RowLayout {
                        enabled: yesCancer.checked
                        RadioButton {
                            id: yesChemotherapy
                            text: "Yes"
                        }
                        RadioButton {
                            text: "No"
                            checked: true
                        }
                    }

                    Label {
                        text: "History of Head/Neck Radiation?"
                        font.bold: true
                        enabled: yesCancer.checked
                    }

                    RowLayout {
                        enabled: yesCancer.checked
                        RadioButton {
                            id: yesHeadNeckRad
                            text: "Yes"
                        }
                        RadioButton {
                            text: "No"
                            checked: true
                        }
                    }
                }
            }
        }

    }

    CDSaveButton {
        id: saveButton
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: 10
        onClicked: {
            saveData();
            showSaveToolTip();
        }
    }

    Button {
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.margins: 10
            highlighted: true
            Material.accent: Material.Teal
            icon.name: "preflight-verifier"
            text: "Save and Exit"
            icon.width: 32
            icon.height: 32
            font.pointSize: 18
            onClicked: {
                saveData();
                Qt.quit();
            }
        }


    Component.onCompleted: {
        var isFemale = personalFile.value("Sex","Female") === "Female";
        if(isFemale) {
            pregGrid.visible = true;
            otherMedFile.category = "Pregnancy";
            yesPregnant.checked =JSON.parse(otherMedFile.value("IsPregnant",false));
            dueDatePicker.setDate(otherMedFile.value("DueDate","1/1/2011"));
            patOBGYN.text = otherMedFile.value("OBGYN","");
            yesBreastfeeding.checked =JSON.parse(otherMedFile.value("IsBreastfeeding",false));
        }

        otherMedFile.category = "AntibioticPreMed";

        yesPremed.checked =  JSON.parse(otherMedFile.value("NeedPreMed"));
        whichPremed.text = otherMedFile.value("WhatTakePremed","");
        whoTellingPremed.text = otherMedFile.value("WhoToldTakePremed","");
        whyPremed.text = otherMedFile.value("WhyTakePremed","");

        otherMedFile.category = "Cancer";
        yesCancer.checked = JSON.parse(otherMedFile.value("HxOfCancer"));
        cancerType.text = otherMedFile.value("CancerType","");
        yesChemotherapy.checked = JSON.parse(otherMedFile.value("HxChemo",false));
        yesHeadNeckRad.checked = JSON.parse(otherMedFile.value("HxRad",false));

        otherMedFile.category = "Smoking";
        doesSmoke.checked = JSON.parse(otherMedFile.value("DoesSmoke"));
        smokeOften.editText = otherMedFile.value("HowMany");
        smokeSpan.currentIndex = smokeSpan.find(otherMedFile.value("HowSpan"));

        otherMedFile.category = "Alerts";
        alertField.text = otherMedFile.value("MedicalAlert","");
    }
}
