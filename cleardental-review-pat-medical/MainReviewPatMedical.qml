// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

// Updated 5/21/2022 Alex Vernes

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDAppWindow {
    id: rootWin
    title: qsTr("Medical History [ID: " + PATIENT_FILE_NAME + "]")

    header: CDPatientToolBar {
        headerText: "Medical History:"
        ToolButton {
             icon.name: "application-menu"
             icon.width: 64
             icon.height: 64
             onClicked: drawer.open();
             anchors.left: parent.left
         }
    }

    function updateReview() {
        var today = new Date();
        var dateString = (today.getMonth() +1) + "/" + (today.getDate()) +
                "/" + today.getFullYear();
        noChangeSettings.setValue("Medical",dateString);
        noChangeSettings.sync();
    }

    Drawer {
        id: drawer
        width: 0.33 * rootWin.width
        height: rootWin.height
        spacing: 0

        background: Rectangle {
            gradient: Gradient {
                     GradientStop { position: 0.0; color: "white" }
                     GradientStop { position: 0.5; color: "white" }
                     GradientStop { position: 1.0; color: "transparent" }
                 }
            anchors.fill: parent
        }

        ColumnLayout {
            anchors.left:  parent.left
            anchors.right: parent.right

            TabButton {
                text: "Other Medical Information"
                Layout.fillWidth: true
                onClicked: {
                    view.replace(view.currentItem,"OtherMedPage.qml");
                    drawer.close();
                }
            }


            TabButton {
                text: qsTr("Conditions")
                Layout.fillWidth: true
                onClicked: {
                    view.replace(view.currentItem,"ConditionsPage.qml");
                    drawer.close();
                }
            }

            TabButton {
                text: qsTr("Medications")
                Layout.fillWidth: true
                onClicked: {
                    view.replace(view.currentItem,"MedicationsPage.qml");
                    drawer.close();
                }
            }
            TabButton {
                text: qsTr("Allergies")
                Layout.fillWidth: true
                onClicked: {
                    view.replace(view.currentItem,"AllergiesPage.qml");
                    drawer.close();
                }
            }
            TabButton {
                text: qsTr("Past Surgeries")
                Layout.fillWidth: true
                onClicked: {
                    view.replace(view.currentItem,"SurgeriesPage.qml");
                    drawer.close();
                }
            }
        }
    }

    StackView   {
        id: view
        anchors.fill: parent
        initialItem: OtherMedPage{}
    }

    CDFileLocations {
        id: fileLocs
    }

    CDGitManager {
        id: noChangeGitman
    }

    Settings {
        id: noChangeSettings
        fileName: fileLocs.getReviewsFile(PATIENT_FILE_NAME);
    }

}

