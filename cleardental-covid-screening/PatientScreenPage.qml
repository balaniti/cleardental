// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtGraphicalEffects 1.12
import Qt.labs.settings 1.1

CDTransparentPage {
    id: patientScreenPage

    CDCommonFunctions {
        id: comFun
    }

    CDGitManager {
        id: gitMan
    }

    CDScheduleManager {
        id: scheduleDB
    }

    Flickable {
        anchors.fill: parent

        contentWidth: colLayout.width
        contentHeight: colLayout.height + 10
        flickableDirection: Flickable.VerticalFlick

        ScrollBar.vertical: ScrollBar {}

        Settings { id: apptLoader }

        ColumnLayout {
            id: colLayout
            x: (patientScreenPage.width - colLayout.width)/2
            y: 10
            CDTranslucentPane {
                backMaterialColor: Material.Yellow
                Layout.fillWidth: true
                visible: PATIENT_FILE_NAME.length === 0
                RowLayout {
                    anchors.horizontalCenter: parent.horizontalCenter

                    Label {
                        text: rootWin.rootPatName
                        font.pixelSize: 64
                    }
                    Image {
                        CDFileLocations {
                            id: fLocs
                        }
                        Layout.maximumHeight: 64
                        Layout.maximumWidth: 64
                        fillMode: Image.PreserveAspectFit
                        source: "file://" + fLocs.getProfileImageFile(rootWin.rootPatName)
                    }
                }
            }

            ScreenPane {
                id: screenPane
            }

            CDSaveAndCloseButton {
                Layout.alignment: Qt.AlignRight
                property var selectedDay: new Date();   // Default today's date
                onClicked: {
                    comFun.updateReviewFile("COVID",rootWin.rootPatName);
                    screenPane.saveResultsToSettings();
                    gitMan.commitData("Updated COVID screening information for " + rootWin.rootPatName);
                    var apps = scheduleDB.getAppointments(selectedDay);
                    for(var i=0;i<apps.length;i++) {
                        console.debug(apps[i]);
                        apptLoader.fileName = apps[i];
                        apptLoader.sync();
                        if (apptLoader.value("PatientID", "") === rootWin.rootPatName) {
                            apptLoader.setValue("Status", "Here");
                        }
                    }
                    Qt.quit();
                }
            }
        }

    }
}
