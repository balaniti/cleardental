// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtGraphicalEffects 1.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDAppWindow {
    id: rootWin
    property string rootPatName : PATIENT_FILE_NAME
    title: PATIENT_FILE_NAME.length > 0 ? "COVID-19 Screening " + " [ID: " + PATIENT_FILE_NAME + "]" :
                                          "COVID-19 Screening"

    font.pointSize: 16

    function refreshPat(newPatName) {
        rootPatName = newPatName;
        title = "COVID-19 Screening " + " [ID: " + rootPatName + "]";
        blankToolBar.headerText = "COVID-19 Screening: " + rootPatName;
    }

     CDPatientToolBar {
         id: patToolBar
         headerText: "COVID-19 Screening:";
         visible: PATIENT_FILE_NAME.length > 0
         ToolButton {
             icon.name: "go-previous"
             onClicked: stackView.pop()
             icon.width: 64
             icon.height: 64
             anchors.left: parent.left
             visible: stackView.depth > 1
         }
     }

     CDBlankToolBar{
         id: blankToolBar
         headerText: "COVID-19 Screening"
         visible: PATIENT_FILE_NAME.length === 0
         ToolButton {
             icon.name: "go-previous"
             onClicked: stackView.pop()
             icon.width: 64
             icon.height: 64
             anchors.left: parent.left
             visible: stackView.depth > 1
         }
     }

    header: PATIENT_FILE_NAME.length > 0 ? patToolBar :blankToolBar

    StackView {
        id: stackView
        anchors.fill: parent
        Component.onCompleted: {
            if(PATIENT_FILE_NAME.length > 0) {
                stackView.push("PatientScreenPage.qml");
            }
            else {
                stackView.push("SelectPatTypePage.qml");
            }
        }
    }
}
