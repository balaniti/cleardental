// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtGraphicalEffects 1.12
import Qt.labs.settings 1.1

CDTranslucentPane {
    id: screenPane

    backMaterialColor: Material.Green

    CDFileLocations {
        id: fLocs
    }

    Settings {
        id: covidFile
        fileName: fLocs.getCOVIDStatusFile(rootWin.rootPatName)
    }

    CDTextFileManager {
        id: textMan
    }

    function saveResultsToSettings() {
        covidFile.setValue("FeverOrChills",haveFever.isYesSelected);
        covidFile.setValue("Cough",haveCough.isYesSelected);
        covidFile.setValue("ShortnessOfBreathOrDifficultyBreathing",haveShortness.isYesSelected);
        covidFile.setValue("Fatigue",haveFatigue.isYesSelected);
        covidFile.setValue("MuscleOrBodyAches",haveMuscle.isYesSelected);
        covidFile.setValue("Headache",haveHeadache.isYesSelected);
        covidFile.setValue("NewLossOfTasteOrSmell",haveNewLoss.isYesSelected);
        covidFile.setValue("SoreThroat",haveSoreThroat.isYesSelected);
        covidFile.setValue("CongestionOrRunnyNose",haveCongestion.isYesSelected);
        covidFile.setValue("NauseaOrVomiting",haveNausea.isYesSelected);
        covidFile.setValue("Diarrhea",haveDiarrhea.isYesSelected);
        covidFile.setValue("Temperature",todayTemp.text);
        covidFile.setValue("ContactWithPositiveCOVID",haveContact.isYesSelected);
        covidFile.setValue("HadPositiveCOVID",havePositive.isYesSelected);
        covidFile.setValue("WhenPositiveCOVID",whenPos.text);
        covidFile.setValue("PatientSeenToday",beSeen.isYesSelected);
        covidFile.sync();

        covidFile.fileName = fLocs.getVitalsFile(rootWin.rootPatName);
        covidFile.setValue("BodyTemp",todayTemp.text);
        covidFile.sync();
    }

    function saveResultsToJSON(name, phone) {
        var jsonText = textMan.readFile(fLocs.getLocalPracticeCOVIDFile());
        var jsonObj = [];
        if(jsonText.length > 0) {
            jsonObj = JSON.parse(jsonText);
        }
        var addMe = ({});
        addMe["Name"] = name;
        addMe["Phone"] = phone;
        addMe["FeverOrChills"] = haveFever.isYesSelected;
        addMe["Cough"] = haveCough.isYesSelected;
        addMe["ShortnessOfBreathOrDifficultyBreathing"] = haveShortness.isYesSelected;
        addMe["Fatigue"] = haveFatigue.isYesSelected;
        addMe["MuscleOrBodyAches"] = haveMuscle.isYesSelected;
        addMe["Headache"] = haveHeadache.isYesSelected;
        addMe["NewLossOfTasteOrSmell"] = haveNewLoss.isYesSelected;
        addMe["SoreThroat"] = haveSoreThroat.isYesSelected;
        addMe["CongestionOrRunnyNose"] = haveCongestion.isYesSelected;
        addMe["NauseaOrVomiting"] = haveNausea.isYesSelected;
        addMe["Diarrhea"] = haveDiarrhea.isYesSelected;
        addMe["Temperature"] = todayTemp.text;
        addMe["ContactWithPositiveCOVID"] = haveContact.isYesSelected;
        addMe["HadPositiveCOVID"] = havePositive.isYesSelected;
        addMe["WhenPositiveCOVID"] = whenPos.text;
        addMe["PatientSeenToday"] = beSeen.isYesSelected;
        addMe["DateTime"] = new Date();

        jsonObj.push(addMe);

        jsonText = JSON.stringify(jsonObj, null, '\t');
        textMan.saveFile(fLocs.getLocalPracticeCOVIDFile(),jsonText);
    }
    
    GridLayout {
        columns: 2
        
        CDHeaderLabel {
            text: "Today's Screening"
            Layout.columnSpan: 2
        }
        
        //https://www.cdc.gov/coronavirus/2019-ncov/symptoms-testing/symptoms.html
        CDDescLabel {
            text: "Does the patient have a Fever or chills?"
        }
        CDYesNoRow {
            id: haveFever
        }
        
        CDDescLabel {
            text: "Does the patient have a Cough?"
        }
        CDYesNoRow {
            id: haveCough
        }
        
        CDDescLabel {
            text: "Does the patient have Shortness of breath or difficulty breathing?"
        }
        CDYesNoRow {
            id: haveShortness
        }
        
        CDDescLabel {
            text: "Does the patient have Fatigue?"
        }
        CDYesNoRow {
            id: haveFatigue
        }
        
        CDDescLabel {
            text: "Does the patient have Muscle or body aches?"
        }
        CDYesNoRow {
            id: haveMuscle
        }
        
        CDDescLabel {
            text: "Does the patient have a Headache?"
        }
        CDYesNoRow {
            id: haveHeadache
        }
        
        CDDescLabel {
            text: "Does the patient have a New loss of taste or smell?"
        }
        CDYesNoRow {
            id: haveNewLoss
        }
        
        CDDescLabel {
            text: "Does the patient have a Sore throat?"
        }
        CDYesNoRow {
            id: haveSoreThroat
        }
        
        CDDescLabel {
            text: "Does the patient have Congestion or runny nose?"
        }
        CDYesNoRow {
            id: haveCongestion
        }
        
        CDDescLabel {
            text: "Does the patient have Nausea or vomiting?"
        }
        CDYesNoRow {
            id: haveNausea
        }
        
        CDDescLabel {
            text: "Does the patient have Diarrhea?"
        }
        CDYesNoRow {
            id: haveDiarrhea
        }
        
        CDDescLabel {
            text: "Today's Temperature (°F)"
        }

        RowLayout {
            Layout.fillWidth: true
            TextField {
                id: todayTemp
                inputMethodHints: Qt.ImhDigitsOnly
            }
            Label {
                color: Material.color(Material.Red)
                opacity: todayTemp.text >= 99
                text: "Fever Temperature!"
                Behavior on opacity {
                    NumberAnimation {
                        duration: 500
                    }
                }
            }
        }
        

        
        CDDescLabel {
            text: "Has the patient been in contact with anyone who has been\nconfirmed to be COVID-19 positive?"
        }
        CDYesNoRow {
            id: haveContact
        }
        
        CDDescLabel {
            text: "Has the patient gotten a COVID-19 positive result?"
        }
        
        RowLayout {
            CDYesNoRow {
                id: havePositive
            }
            CDDescLabel {
                text: "When?"
                opacity: havePositive.isYesSelected ? 1 : 0
                Behavior on opacity {
                    PropertyAnimation {
                        duration: 300
                    }
                }
            }
            TextField {
                id: whenPos
                opacity: havePositive.isYesSelected ? 1 : 0
                Behavior on opacity {
                    PropertyAnimation {
                        duration: 300
                    }
                }
            }
        }
        
        CDDescLabel {
            text: "Will patient be seen today?"
        }
        
        CDYesNoRow {
            id: beSeen
        }
    }
}
