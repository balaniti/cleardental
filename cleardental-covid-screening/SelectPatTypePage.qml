// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtGraphicalEffects 1.12

CDTransparentPage {
    CDTranslucentPane {
        anchors.centerIn: parent
        ColumnLayout {
            spacing: 20
            CDHeaderLabel {
                text: "Type of patient"
            }

            Button {
                text: "Patient for dentistry"
                font.pointSize: 18
                onClicked: {
                    stackView.push("SelectPatientPage.qml");
                }
            }

            Button {
                text: "Patient for visiting"
                font.pointSize: 18
                onClicked: {
                    stackView.push("VisitorScreenPage.qml");
                }
            }

            Button {
                text: "Staff/Employee"
                font.pointSize: 18
                onClicked: {
                    stackView.push("StaffScreenPage.qml");
                }
            }
        }
    }
}
