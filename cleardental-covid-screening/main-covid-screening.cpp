// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickImageProvider>
#include <QFont>
#include <QIcon>
#include <QDebug>

#include "cddefaults.h"
#include "cddoctorlistmanager.h"
#include "cdschedulemanager.h"

int main(int argc, char *argv[])
{
    CDDefaults::setAppDefaults();
    QCoreApplication::setApplicationName("ClearDental-COVID-Screening");

    QGuiApplication app(argc, argv);
    app.setFont(QFont("Barlow Semi Condensed"));
    app.setWindowIcon(CDDefaults::defaultColorIcon());

    QString patientFileName =  "";

    if(QCoreApplication::arguments().count()>1) {
        patientFileName = argv[1];
    }

    CDDefaults::registerQMLTypes();
    qmlRegisterType<CDScheduleManager>("dental.clear", 1, 0, "CDScheduleManager");
    qmlRegisterType<CDDoctorListManager>("dental.clear", 1, 0,"CDDoctorListManager");

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("PATIENT_FILE_NAME",patientFileName);
    engine.load(QUrl(QStringLiteral("qrc:/MainCovidScreening.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    CDDefaults::enableBlurBackground();

    return app.exec();
}
