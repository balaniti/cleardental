// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtGraphicalEffects 1.12

CDTransparentPage {
    CDTranslucentPane {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.margins: 10
        ColumnLayout {
            CDHeaderLabel {
                text: "Select Patient"
            }

            CDPatientComboBox {
                id: patCombo
                Layout.minimumWidth: 350
            }

            Button {
                text: "Select Patient"
                enabled: patCombo.text.length > 0
                onClicked: {
                    rootWin.refreshPat(patCombo.text);
                    stackView.push("PatientScreenPage.qml");
                }
            }
        }
    }
}
