// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtGraphicalEffects 1.12
import Qt.labs.settings 1.1

CDTransparentPage {
    id: vistorScreenPage

    CDFileLocations {
        id: fLocs
    }

    CDCommonFunctions {
        id: comFun
    }

    CDGitManager {
        id: gitMan
    }

    CDDoctorListManager {
        id: staffListMan
    }

    Flickable {
        anchors.fill: parent

        contentWidth: colLayout.width
        contentHeight: colLayout.height + 10
        flickableDirection: Flickable.VerticalFlick

        ScrollBar.vertical: ScrollBar {}

        ColumnLayout {
            id: colLayout
            x: (vistorScreenPage.width - colLayout.width)/2
            y: 10
            CDTranslucentPane {
                backMaterialColor: Material.Grey
                Layout.fillWidth: true
                GridLayout {
                    columns: 2
                    width: parent.width
                    CDHeaderLabel {
                        text: "Staff Information"
                        Layout.columnSpan: 2
                    }
                    CDDescLabel {
                        text: "ID"
                    }
                    ColumnLayout {
                        Repeater {
                            id: reps
                            property var docPrefList : staffListMan.getListOfStaffFiles()
                            property var selectedID: ""
                            model: docPrefList.length


                            RadioButton {
                                Settings {
                                    id: mySet
                                }

                                Component.onCompleted: {
                                    mySet.fileName = reps.docPrefList[index];
                                    text = mySet.value("UnixID")
                                }

                                onCheckedChanged: {
                                    if(checked) {
                                        reps.selectedID = text;
                                    }
                                }
                            }

                        }
                    }
                }
            }

            ScreenPane {
                id: screenPane
            }

            CDSaveAndCloseButton {
                Layout.alignment: Qt.AlignRight

                Settings {
                    id: covidFile
                    fileName: fLocs.getCOVIDStatusFile(rootWin.rootPatName)
                }

                onClicked: {
                    screenPane.saveResultsToJSON(reps.selectedID,"Staff");
                    gitMan.commitData("Added covid screening results");
                    Qt.quit();
                }
            }
        }

    }
}
