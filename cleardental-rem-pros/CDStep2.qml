// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtGraphicalEffects 1.11
import QtQuick.Layouts 1.3
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1
import dental.clear 1.0


GridLayout {
    columns: 2
    columnSpacing: 20

    property bool isMax: parent.isMaxillary;
    property int nextStep: 0

    function generateCaseNote() {
        if(isMax) {
            var returnMe = "Patient presented for Maxillary Complete Denture Step 2: Secondary Impressions\n";
        }
        else {
            returnMe = "Patient presented for Mandibular Complete Denture Step 2: Secondary Impressions\n";
        }

        returnMe +=  trayMaterial.currentText + " tray was used.\n";
        returnMe += "Border molded using " + borderMaterial.currentText + ".\n";
        returnMe += "Impressed with " + impressMaterial.currentText + ".\n";
        returnMe += "Pour up: " + stonePour.currentText + ".\n";
        returnMe += "Case to be sent to: " + labBox.currentText + ".\n";
        returnMe += "Next step: " + nvBox.currentText + ".\n";
        nextStep = nvBox.currentIndex + 1;
        return returnMe;
    }

    Label {
        text: "Border molding"
        font.bold: true
    }

    ComboBox {
        id: borderMaterial
        Layout.fillWidth: true
        model: ["Polyvinyl Siloxane (PVS)", "Green Stick Compound", "Condensation Silicone","Polyether",
            "Polysulfide", "Zinc Oxide Eugenol (ZOE)"]
    }

    Label {
        text: "Impression Material"
        font.bold: true
    }

    ComboBox {
        id: impressMaterial
        Layout.fillWidth: true
        model: ["Polyvinyl Siloxane (PVS)", "Condensation Silicone","Polyether","Polysulfide",
            "Zinc Oxide Eugenol (ZOE)"]
    }

    Label {
        text: "Trays"
        font.bold: true
    }

    ComboBox {
        id: trayMaterial
        Layout.fillWidth: true
        model: ["Custom","Stock Plastic","Stock Metal", "Accu-Dent"]
    }

    Label {
        text: "Pour up"
        font.bold: true
    }

    ComboBox {
        id: stonePour
        Layout.fillWidth: true
        model: ["Lab will pour it up","Type 2 Dental Plaster","Type 1 Plaster of Paris"]
    }

    Label {
        text: "Lab to be Sent To"
        font.bold: true
    }

    SelectLabBox {
        id: labBox
        Layout.fillWidth: true
    }

    Label {
        text: "Next Step"
        font.bold: true
    }

    CDNextStepBox {
        id: nvBox
        currentIndex: 2
    }
}
