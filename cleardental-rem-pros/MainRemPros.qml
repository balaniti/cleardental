// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtGraphicalEffects 1.11
import QtQuick.Layouts 1.3
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDAppWindow {
    id: rootWin
    title: qsTr("Removable Complete / Partial Denture" + "[ID: " + PATIENT_FILE_NAME + "]")

    property var compProcedureList: []
    property var maxTxItem: ({})
    property var manTxItem: ({})
    property var stepModels: ["1 (Primary Impressions)",
        "2 (Secondary Impressions)",
        "3 (Wax Rim Adjustments)",
        "4 (Wax Try-in)",
        "5 (Delivery)",
        "6 (Adjustments)"]

    header: CDPatientToolBar {
        headerText: "Removable Fabrication:"
        ToolButton {
             icon.name: "application-menu"
             icon.width: 64
             icon.height: 64
             onClicked: drawer.open();
             anchors.left: parent.left
         }
    }

    CDHygieneRoomStatus {
        id: drawer
        height: rootWin.height
        width: 0.33 * rootWin.width
    }

    CDTextFileManager {
        id: textMan
    }

    CDFileLocations {
        id: fileLocs
    }

    CDCommonFunctions {
        id: comFuns
    }

    Settings {
        id: remStepSettings
    }

    ColumnLayout {
        anchors.centerIn: parent
        spacing: 10
        CDMedReviewPane {
            Layout.fillWidth: true
            Layout.minimumWidth: 800
            id: medHis
        }
        RowLayout {
            spacing: 10
            CDTranslucentPane {
                id: maxArchPane
                visible: false;
                backMaterialColor: Material.Pink
                ColumnLayout {
                    CDHeaderLabel {
                        text: "Maxillary Arch"
                    }
                    RowLayout {
                        CDDescLabel {
                            text: "Select Step"
                        }
                        ComboBox {
                            id: maxStepCombo
                            Layout.fillWidth: true
                            model: stepModels
                            onCurrentIndexChanged: {
                                var newStep = currentIndex +1;
                                maxLoader.source = "CDStep"+newStep+".qml";
                            }
                        }
                    }
                    Loader {
                        id: maxLoader
                        property bool isMaxillary: true;
                    }
                }
            }
            CDTranslucentPane {
                id: manArchPane
                visible: false;
                backMaterialColor: Material.LightBlue
                ColumnLayout {
                    id: manArchCol
                    CDHeaderLabel {
                        text: "Mandibular Arch"
                    }
                    RowLayout {
                        CDDescLabel {
                            text: "Select Step"
                        }
                        ComboBox {
                            id: manStepCombo
                            Layout.fillWidth: true
                            model: stepModels
                            onCurrentIndexChanged: {
                                var newStep = currentIndex +1;
                                manLoader.source = "CDStep"+newStep+".qml";
                            }
                        }
                    }

                    Loader {
                        id: manLoader
                        property bool isMaxillary: false;
                    }
                }
            }
        }
    }

    CDTimerPane {
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.margins: 10
    }

    CDFinishProcedureButton {
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        onClicked: {
            finDia.caseNoteString= "";
            if(maxArchPane.visible) {
                finDia.caseNoteString = maxLoader.item.generateCaseNote();
                remStepSettings.setValue("MaxStep",maxLoader.item.nextStep)
                if(maxLoader.item.nextStep === 6) {
                    finDia.txItemsToComplete.push(maxTxItem);
                }
            }

            finDia.caseNoteString += "\n";

            if(manArchPane.visible) {
                finDia.caseNoteString += manLoader.item.generateCaseNote();
                remStepSettings.setValue("ManStep",manLoader.item.nextStep)
                if(manLoader.item.nextStep === 6) {
                    finDia.txItemsToComplete.push(manTxItem);
                }
            }
            remStepSettings.sync();



            finDia.open();
        }
    }

    CDFinishProcedureDialog {
        id: finDia
    }

    Component.onCompleted: {
        remStepSettings.fileName = fileLocs.getRemovableStatusFile(PATIENT_FILE_NAME);

        //First get the type of pros from the Tx Plan
        var txPlanList = comFuns.getPrimaryTxPlanItems(PATIENT_FILE_NAME);

        for(var i=0;i<txPlanList.length;i++) {
            var txItem = txPlanList[i];

            if(txItem["ProcedureName"].includes("Complete")) {
                if(txItem["Location"] === "UA") { //complete maxillary denture
                    maxTxItem = txItem;
                    maxArchPane.visible = true;
                    var step = remStepSettings.value("MaxStep",1);
                    maxLoader.source = "CDStep"+step+".qml";
                    maxStepCombo.currentIndex = parseInt(step) - 1;
                }
                else {
                    manTxItem = txItem;
                    manArchPane.visible = true;
                    step = remStepSettings.value("ManStep",1);
                    manLoader.source = "CDStep"+step+".qml";
                }
            }
            else if(txItem["ProcedureName"].includes("RPD")) {
                if(txItem["Location"] === "UA") { //Max RPD
                    maxTxItem = txItem;
                    maxArchPane.visible = true;
                    step = remStepSettings.value("MaxStep",1);
                    maxLoader.source = "CDStep"+step+".qml";
                    maxStepCombo.currentIndex = parseInt(step) - 1;
                }
                else {
                    manTxItem = txItem;
                    manArchPane.visible = true;
                    step = remStepSettings.value("ManStep",1);
                    manLoader.source = "CDStep"+step+".qml";
                }

            }
        }
    }
}
