// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.10

ComboBox {
    id: nvBox
    Layout.minimumWidth: 300
    model: ["Step 1 (Primary Impressions)",
        "Step 2 (Secondary/Final Impressions)",
        "Step 3 (Occlusal Records)",
        "Step 4 (Wax try-in)",
        "Step 5 (Delivery)",
        "Step PRN (Follow-up)"]
}
