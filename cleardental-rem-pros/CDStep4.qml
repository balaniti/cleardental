// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtGraphicalEffects 1.11
import QtQuick.Layouts 1.3
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1
import dental.clear 1.0

GridLayout {
    columns: 2
    columnSpacing: 20

    property bool isMax: parent.isMaxillary;
    property int nextStep: 0

    function generateCaseNote() {
        if(isMax) {
            var returnMe = "Patient presented for Maxillary Complete Denture Step 4: Wax Try-in\n";
        }
        else {
            returnMe = "Patient presented for Mandibular Complete Denture Step 4: Wax Try-in\n";
        }

        if(happyEst.checked) {
            returnMe += happyEst.text + ".\n";
        }

        if(happyBite.checked) {
            returnMe += happyBite.text + ".\n";
        }

        if(changesRequired.checked) {
            returnMe += changesRequired.text + ".\n";
        }

        returnMe += "Case to be sent to: " + labBox.currentText + ".\n";
        returnMe += "Next step: " + nvBox.currentText + ".\n";
        nextStep = nvBox.currentIndex + 1;
        return returnMe;
    }

    CheckBox {
        id: happyEst
        text: "Patient happy with esthetics"
        checked: true
        Layout.columnSpan: 2
    }

    CheckBox {
        id: happyBite
        text: "Patient happy with bite"
        checked: true
        Layout.columnSpan: 2
    }

    CheckBox {
        id: changesRequired
        text: "Modification of setup required"
        checked: false
        Layout.columnSpan: 2
    }

    Label {
        text: "Lab to be Sent To"
        font.bold: true
    }

    SelectLabBox {
        id: labBox
        Layout.fillWidth: true
    }

    Label {
        text: "Next Step"
        font.bold: true
    }

    CDNextStepBox {
        id: nvBox
        currentIndex: 4
    }
}
