// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTransparentPage {
    id: chairsPane

    property var chairTypes: ["Operative", "Hygiene", "OP + OMFS", "OMFS Only"]

    function loadData() {
        var jsonText = textFileMan.readFile(fileLocs.getPracticeChairFile(locationID));
        if(jsonText.length === 0) {
            return;
        }

        var result = JSON.parse(jsonText);
        for(var i=0;i<result.length;i++) {
            var obj = result[i];
            textFieldComp.createObject(chairGrid,{"text":obj["ChairName"]});
            var chairType = obj["ChairType"];
            var chairIndex = chairTypes.indexOf(chairType)
            chairTypeComp.createObject(chairGrid,{"currentIndex":chairIndex});
            deleteButton.createObject(chairGrid,{});
        }
    }

    function saveData() {
        var jsonData = [];
        for(var i=4;i<chairGrid.children.length;i+=3) {
            if(chairGrid.children[i].text.length > 0) {
                var addMe = {"ChairName": chairGrid.children[i].text,
                    "ChairType": chairGrid.children[i+1].displayText};
                jsonData.push(addMe);
            }
        }
        var jsonString= JSON.stringify(jsonData, null, '\t');
        textFileMan.saveFile(fileLocs.getPracticeChairFile(locationID),jsonString);
    }

    CDTextFileManager {
        id: textFileMan
    }

    Label {
        id: noChairsLabel
        text: "No chairs designated"
        anchors.fill: parent
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.pointSize: 24
        visible: opacity > 0
        opacity: chairGrid.children.length < 5
        Behavior on opacity {
            PropertyAnimation {
                duration: 300
            }
        }
    }

    CDTranslucentPane {
        id: chairPane
        anchors.centerIn: parent
        backMaterialColor: Material.LightGreen
        GridLayout {
            id: chairGrid
            columnSpacing: 25
            columns: 3

            visible: opacity > 0
            opacity: chairGrid.children.length > 5
            Behavior on opacity {
                PropertyAnimation {
                    duration: 300
                }
            }

            function makeNewRow() {
                textFieldComp.createObject(chairGrid,
                                           {"placeholderText":"Chair Name"});
                chairTypeComp.createObject(chairGrid);
                deleteButton.createObject(chairGrid);
            }

            function killTheZombie() {
                for(var i=4;i<chairGrid.children.length;i+=3) {
                    var killBut = chairGrid.children[i+2];
                    if(killBut.text === "Kill me now!") {
                        var kill1 = chairGrid.children[i];
                        var kill2 = chairGrid.children[i+1];
                        kill1.destroy();
                        kill2.destroy();
                        killBut.destroy();
                        return; //probably only one zombie to kill
                    }
                }
            }

            Label {
                text: "Assigned Chairs"
                font.pointSize: 24
                horizontalAlignment: Qt.AlignHCenter
                font.underline: true
                Layout.columnSpan: 3
            }

            Label {
                font.bold: true
                text: "Chair Name"
            }
            Label {
                font.bold: true
                text: "Chair Type"
            }

            Label { //just for placeholder to make a new row

            }

            Component {
                id: textFieldComp
                TextField {
                    Layout.minimumWidth: 300
                }
            }

            Component {
                id: chairTypeComp
                ComboBox {
                    id: chairTypeComboBox
                    model: chairsPane.chairTypes
                }
            }

            Component {
                id: deleteButton
                Button {
                    icon.name: "list-remove"
                    text: "Remove Chair"
                    onClicked: {
                        text = "Kill me now!";
                        chairGrid.killTheZombie();
                    }
                    Material.accent: Material.Red
                    highlighted: true
                }
            }
        }

    }

    CDAddButton {
        id: addButton
        text: "Add Chair"
        anchors.right: chairPane.right
        anchors.top: chairPane.bottom
        anchors.margins: 10
        onClicked: chairGrid.makeNewRow();
    }
}
