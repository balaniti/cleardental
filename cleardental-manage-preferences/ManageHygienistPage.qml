// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTransparentPage {
    id: manHygPage

    property string providerID: ""

    title: "Manage Dental Hygienist (" + providerID + ")"

    function getBoolValue(inputVal) {
        switch(inputVal){
                case true:
                case "true":
                case 1:
                case "1":
                case "on":
                case "yes":
                    return true;
                default:
                    return false;
            }
    }

    onProviderIDChanged: {
        docFile.fileName = fileLocs.getDocInfoFile(providerID)
        firstNameField.text = docFile.value("FirstName")
        lastNameField.text = docFile.value("LastName")
        providerTypeBox.currentIndex =
                providerTypeBox.find(docFile.value("ProviderType"));
        schColor.setColor(docFile.value("ProviderColor","Blue"));
        licenseField.text = docFile.value("LicenseID","")
        npiField.text = docFile.value("NPINumb","")
        deaField.text = docFile.value("DEANumb","")
        doesSCRP.checked =
                getBoolValue(docFile.value("DoesSCRP", false))
        doesLA.checked =
                getBoolValue(docFile.value("DoesLA", false))
    }

    CDFileLocations {
        id: fileLocs
    }

    Settings {
        id: docFile
        fileName: fileLocs.getDocInfoFile(providerID)
    }

    CDGitManager {
        id: gitMan
    }

    CDTranslucentPane {
        id: backPane
        anchors.centerIn: parent
        backMaterialColor: schColor.buttonColors[schColor.setColorIndex]
        GridLayout {
            columnSpacing: 25
            columns: 2
            Label {
                text: "First Name:"
                font.bold: true
            }
            TextField {
                id: firstNameField
            }
            Label {
                text: "Last Name:"
                font.bold: true
            }
            TextField {
                id: lastNameField
            }
            Label {
                text: "Provider Type:"
                font.bold: true
            }
            ComboBox {
                id: providerTypeBox
                Layout.minimumWidth: 250
                model: ["General Dentist","Hygienist"]
            }
            Label {
                text: "Schedule Color:"
                font.bold: true
            }
            GridLayout {
                id: schColor
                columns: 10

                property int setColorIndex: 0

                function setColor(stringName) {
                    setColorIndex = buttonColorStrings.indexOf(stringName);

                }

                property var buttonColors: [Material.Red,Material.Pink,
                    Material.Purple, Material.DeepPurple, Material.Indigo,
                    Material.Blue, Material.LightBlue, Material.Cyan, Material.Teal,
                    Material.Green, Material.LightGreen, Material.Lime,
                    Material.Yellow, Material.Amber, Material.Orange,
                    Material.DeepOrange, Material.Brown, Material.Grey,
                    Material.BlueGrey]

                property var buttonColorStrings: ["Red","Pink","Purple",
                    "DeepPurple","Indigo","Blue","LightBlue","Cyan","Teal","Green",
                    "LightGreen","Lime","Yellow","Amber","Orange","DeepOrange",
                    "Brown","Grey","BlueGrey"]

                Repeater {
                    model: schColor.buttonColors.length
                    Button {
                        property string colorName: schColor.buttonColorStrings[index]
                        highlighted: true
                        Layout.maximumWidth: 50
                        Material.accent:schColor.buttonColors[index]
                        checkable: true
                        checked: index==schColor.setColorIndex
                        icon.name: checked ? "dialog-ok" : ""
                        onClicked: {
                            backPane.backMaterialColor =  schColor.buttonColors[index]
                        }
                    }
                }

                ButtonGroup {
                    id: checkedColor
                    buttons: schColor.children
                }

            }
            Label {
                text: "License ID:"
                font.bold: true
            }
            TextField {
                id: licenseField
            }
            Label {
                text: "NPI Number:"
                font.bold: true
            }
            TextField {
                id: npiField
            }
            Label {
                text: "DEA Number:"
                font.bold: true
            }
            TextField {
                id: deaField
            }

            MenuSeparator{Layout.columnSpan: 2;Layout.fillWidth: true}

            CheckBox {
                id: doesLA
                text: "Can admnister local anesthesia"
            }

            CheckBox {
                id: doesSCRP
                text: "Will do SCRP"
            }
        }

    }

    CDSaveButton {
        text: "Save + Go Back"
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        enabled: firstNameField.text.length > 1 &&
                 lastNameField.text.length > 1
        onClicked: {
            docFile.setValue("FirstName",firstNameField.text);
            docFile.setValue("LastName",lastNameField.text);
            docFile.setValue("ProviderType",providerTypeBox.displayText);
            docFile.setValue("ProviderColor",checkedColor.checkedButton.colorName);
            docFile.setValue("LicenseID",licenseField.text);
            docFile.setValue("NPINumb",npiField.text);
            docFile.setValue("DEANumb", deaField.text);
            docFile.setValue("DoesSCRP",doesSCRP.checked);
            docFile.setValue("DoesLA",doesLA.checked);
            docFile.sync();
            gitMan.commitData("Updated information for " + providerID);
            mainView.pop();
        }
    }
}
