// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTransparentPage {
    id: defaultUpdateTimesPane

    function loadData() {
        locFile.category = "Updates"
        recareTimeBox.value = locFile.value("Recare",recareTimeBox.value);
        bwTimeBox.value = locFile.value("BWs",bwTimeBox.value);
        panTimeBox.value = locFile.value("Pan",panTimeBox.value);
        perioChartBox.value = locFile.value("Perio",perioChartBox.value);
    }

    function saveData() {
        locFile.category = "Updates";
        locFile.setValue("Recare",recareTimeBox.value);
        locFile.setValue("BWs",bwTimeBox.value);
        locFile.setValue("Pan",panTimeBox.value);
        locFile.setValue("Perio",perioChartBox.value);
    }

    CDTranslucentPane {
        backMaterialColor: Material.DeepPurple
        anchors.centerIn: parent
        GridLayout {
            columnSpacing: 25
            columns: 2
            Label {
                text: "Default time between "+
                      "Recare and Prophy (D0120 and D1110) " +
                      "Appointments"
                font.bold: true
            }

            SpinBox {
                id: recareTimeBox
                from: 1
                to: 24
                value: 6
                textFromValue: function(value) {
                    return value + " months"
                }
            }

            Label {
                text: "Default time to take 4 BW Radiographs"
                font.bold: true
            }

            SpinBox {
                id: bwTimeBox
                from: 0
                to: 24
                value: 12
                textFromValue: function(value) {
                    return value + " months"
                }
            }

            Label {
                text: "Default time to take Panoraomic or FMX Radiographs"
                font.bold: true
            }

            SpinBox {
                id: panTimeBox
                from: 0
                to: 10
                value: 5
                textFromValue: function(value) {
                    return value + " years"
                }
            }

            Label {
                text: "Default time to redo Periodontal Charting"
                font.bold: true
            }

            SpinBox {
                id: perioChartBox
                from: 1
                to: 10
                value: 2
                textFromValue: function(value) {
                    return value + " years"
                }
            }
        }
    }


}
