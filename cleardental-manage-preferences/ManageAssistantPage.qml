// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTransparentPage {
    id: manHygPage

    property string providerID: ""

    title: "Manage Dental Assistant (" + providerID + ")"

    onProviderIDChanged: {
        docFile.fileName = fileLocs.getDocInfoFile(providerID)
        firstNameField.text = docFile.value("FirstName")
        lastNameField.text = docFile.value("LastName")
        licenseField.text = docFile.value("LicenseID","")
    }

    CDFileLocations {
        id: fileLocs
    }

    Settings {
        id: docFile
        fileName: fileLocs.getDocInfoFile(providerID)
    }

    CDGitManager {
        id: gitMan
    }

    CDTranslucentPane {
        id: backPane
        anchors.centerIn: parent
        backMaterialColor: Material.Green
        GridLayout {
            columnSpacing: 25
            columns: 2
            Label {
                text: "First Name:"
                font.bold: true
            }
            TextField {
                id: firstNameField
            }
            Label {
                text: "Last Name:"
                font.bold: true
            }
            TextField {
                id: lastNameField
            }
            Label {
                text: "License ID:"
                font.bold: true
            }
            TextField {
                id: licenseField
            }
        }

    }

    CDSaveButton {
        text: "Save + Go Back"
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        enabled: firstNameField.text.length > 1 &&
                 lastNameField.text.length > 1
        onClicked: {
            docFile.setValue("FirstName",firstNameField.text);
            docFile.setValue("LastName",lastNameField.text);
            docFile.setValue("LicenseID",licenseField.text);
            docFile.sync();
            gitMan.commitData("Updated information for " + providerID);
            mainView.pop();
        }
    }

}
