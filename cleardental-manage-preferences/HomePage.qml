// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1

Page {
    id: homePage
    title: "Home"

    background: Rectangle {
        anchors.fill: parent
        color: "transparent"
    }

    CDTranslucentPane {
        anchors.centerIn: parent
        ColumnLayout {
            spacing: 25
            CDHeaderLabel {
                text: "What would you like to manage?"
            }
            Button {
                text: "Providers"
                onClicked: mainView.push("ListDoctorsPage.qml")
                font.pointSize: 16
            }
            Button {
                text: "Locations"
                onClicked: mainView.push("ListLocationsPage.qml")
                font.pointSize: 16
            }
        }
    }
}
