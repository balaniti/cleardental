// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTranslucentDialog {
    id: addNewFeeScheduleDia

    CDConstLoader{id: m_ConstLoader}

    property alias newName: newFeeScheduleName.editText

    ColumnLayout {
        CDTranslucentPane {
            ColumnLayout {
                CDHeaderLabel {
                    text: "Add New Fee Schedule"
                }
                GridLayout {
                    id: inputGrid
                    columns: 2
                    property var existingNameList: feeMan.getLocalListOfPlans();
                    CDDescLabel {
                        text: "Fee Schedule Name"
                    }
                    ComboBox {
                        id: newFeeScheduleName
                        editable: true
                        Layout.minimumWidth: 480
                        property var planStringList: []
                        Component.onCompleted: {
                            //Now get the list of known dental plans
                            var planList = JSON.parse(m_ConstLoader.getUSDentalPlanListJSON());
                            for(var i=0;i<planList.length;i++) {
                                planStringList.push(planList[i]["PlanName"]);
                            }

                            //Now remove the ones that have already been done
                            for(i=0;i<inputGrid.existingNameList.length;i++) {
                                var index = planStringList.indexOf(inputGrid.existingNameList[i])
                                if(index >= 0) {
                                    planStringList.splice(index,1);
                                }
                            }

                            model = planStringList;
                        }

                        onEditTextChanged: {
                            var index = inputGrid.existingNameList.indexOf(editText)
                            takenLabel.visible = index >= 0;
                            addFeeButton.enabled = index<0;
                        }
                    }
                    Label {
                        id: takenLabel
                        color: Material.color( Material.Red);
                        text: "This name is already taken, please choose another"
                        visible: false
                    }


                }
            }
        }

        RowLayout {
            CDCancelButton {
                onClicked:  {
                    addNewFeeScheduleDia.reject();
                }
            }
            Label {
                Layout.fillWidth: true
            }
            CDAddButton {
                id: addFeeButton
                text: "Add New Blank Fee Schedule"
                onClicked: {
                    addNewFeeScheduleDia.accept();
                }
            }
        }
    }
}
