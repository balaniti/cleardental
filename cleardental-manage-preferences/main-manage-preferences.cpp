// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickImageProvider>
#include <QFont>
#include <QIcon>
#include <QDebug>

#include "cdtextfilemanager.h"
#include "cdgitmanager.h"
#include "cdimagemanager.h"
#include "cdtoollauncher.h"
#include "cddefaults.h"
#include "cdfilelocations.h"
#include "cddoctorlistmanager.h"
#include "cdlocationmanager.h"
#include "cddefaultformsprovider.h"
#include "cdfeeschedulemanager.h"

int main(int argc, char *argv[])
{
    CDDefaults::setAppDefaults();
    QCoreApplication::setApplicationName("ClearDental-Manage-Preferences");

    QGuiApplication app(argc, argv);
    app.setWindowIcon(CDDefaults::defaultColorIcon());

    CDDefaults::registerQMLTypes();

    qmlRegisterType<CDDoctorListManager>("dental.clear", 1, 0,"CDDoctorListManager");
    qmlRegisterType<CDLocationManager>("dental.clear", 1, 0,"CDClinicLocationManager");
    qmlRegisterType<CDDefaultFormsProvider>("dental.clear", 1, 0,"CDDefaultForms");
    qmlRegisterType<CDFeeScheduleManager>("dental.clear", 1, 0,"CDFeeScheduleManager");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/MainManagePrefs.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    CDDefaults::enableBlurBackground();

    return app.exec();
}
