// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTransparentPage {
    id:postOpInstructionsPane

    Flickable {
        id: flickView
        anchors.fill: parent
        anchors.margins: 10
        contentWidth: gridPostOp.implicitWidth
        contentHeight: gridPostOp.implicitHeight + 25

        ScrollBar.vertical: ScrollBar {}

        CDTranslucentPane {
            backMaterialColor: Material.Cyan
            x: (flickView.width - gridPostOp.implicitWidth)/2
            GridLayout {
                id: gridPostOp
                columnSpacing: 25
                columns: 2

                Label {
                    text: "Operative Post Op"
                    font.bold: true
                }

                TextArea {
                    Layout.minimumHeight: 300
                    Layout.fillWidth: true
                    textFormat: TextEdit.RichText
                    Layout.maximumWidth: 700
                    Layout.minimumWidth: 640
                    placeholderText: "Please paste your post op form here"
                    wrapMode: TextEdit.WordWrap
                }

                Label {
                    text: "Endo Post Op"
                    font.bold: true
                }

                TextArea {
                    Layout.minimumHeight: 300
                    Layout.fillWidth: true
                    textFormat: TextEdit.RichText
                    Layout.maximumWidth: 700
                    placeholderText: "Please paste your post op form here"
                    wrapMode: TextEdit.WordWrap
                }

                Label {
                    text: "Oral Surgery Post Op"
                    font.bold: true
                }

                TextArea {
                    Layout.minimumHeight: 300
                    Layout.fillWidth: true
                    textFormat: TextEdit.RichText
                    Layout.maximumWidth: 700
                    placeholderText: "Please paste your post op form here"
                    wrapMode: TextEdit.WordWrap
                }

                Label {
                    text: "Fixed Pros Post Op"
                    font.bold: true
                }

                TextArea {
                    Layout.minimumHeight: 300
                    Layout.fillWidth: true
                    textFormat: TextEdit.RichText
                    Layout.maximumWidth: 700
                    placeholderText: "Please paste your post op form here"
                    wrapMode: TextEdit.WordWrap
                }

                Label {
                    text: "Removable Pros\nInstructions"
                    font.bold: true
                }

                TextArea {
                    Layout.minimumHeight: 300
                    Layout.fillWidth: true
                    textFormat: TextEdit.RichText
                    Layout.maximumWidth: 700
                    placeholderText: "Please paste your post op form here"
                    wrapMode: TextEdit.WordWrap
                }
            }
        }


    }


}
