// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTransparentPage {
    id: accountsPane

    function loadData() {
        locFile.category = "Accounts"
        voipMSUsername.text = locFile.value("VoipMSUsername","");
        voipMSPassword.text = locFile.value("VoipMSPassword","");

        edsEDIUsername.text = locFile.value("EDSEDIUsername","");
        edsEDIPassword.text = locFile.value("EDSEDIPassword","");

        dentalXChangeUsername.text = locFile.value("DentalXChangeUsername","");
        dentalXChangePassword.text = locFile.value("DentalXChangePassword","");

        tesiaUsername.text = locFile.value("TesiaUsername","");
        tesiaPassword.text = locFile.value("TesiaPassword","");
    }

    function saveData() {
        locFile.category = "Accounts"
        locFile.setValue("VoipMSUsername",voipMSUsername.text);
        locFile.setValue("VoipMSPassword",voipMSPassword.text);

        locFile.setValue("EDSEDIUsername",edsEDIUsername.text);
        locFile.setValue("EDSEDIPassword",edsEDIPassword.text);

        locFile.setValue("DentalXChangeUsername",dentalXChangeUsername.text);
        locFile.setValue("DentalXChangePassword",dentalXChangePassword.text);

        locFile.setValue("TesiaUsername",tesiaUsername.text);
        locFile.setValue("TesiaPassword",tesiaPassword.text);
    }

    ColumnLayout {
        anchors.centerIn: parent

        CDTranslucentPane {
            backMaterialColor: Material.LightGreen
            ColumnLayout {
                CDHeaderLabel {
                    text: "voip.ms"
                }
                GridLayout {
                    columns: 2
                    CDDescLabel {
                        text: "Username"
                    }
                    TextField {
                        id: voipMSUsername
                        Layout.minimumWidth: 360
                    }
                    CDDescLabel {
                        text: "Password"
                    }
                    TextField {
                        id: voipMSPassword
                        Layout.fillWidth: true
                    }
                }
            }
        }

        CDTranslucentPane {
            backMaterialColor: Material.LightBlue
            ColumnLayout {
                CDHeaderLabel {
                    text: "EDS EDI"
                }
                GridLayout {
                    columns: 2
                    CDDescLabel {
                        text: "Username"
                    }
                    TextField {
                        id: edsEDIUsername
                        Layout.minimumWidth: 360
                    }
                    CDDescLabel {
                        text: "Password"
                    }
                    TextField {
                        id: edsEDIPassword
                        Layout.fillWidth: true
                    }
                }
            }
        }

        CDTranslucentPane {
            backMaterialColor: Material.Blue
            ColumnLayout {
                CDHeaderLabel {
                    text: "DentalXChange"
                }
                GridLayout {
                    columns: 2
                    CDDescLabel {
                        text: "Username"
                    }
                    TextField {
                        id: dentalXChangeUsername
                        Layout.minimumWidth: 360
                    }
                    CDDescLabel {
                        text: "Password"
                    }
                    TextField {
                        id: dentalXChangePassword
                        Layout.fillWidth: true
                    }
                }
            }
        }

        CDTranslucentPane {
            backMaterialColor: Material.Cyan
            ColumnLayout {
                CDHeaderLabel {
                    text: "Vyne / Tesia"
                }
                GridLayout {
                    columns: 2
                    CDDescLabel {
                        text: "Username"
                    }
                    TextField {
                        id: tesiaUsername
                        Layout.minimumWidth: 360
                    }
                    CDDescLabel {
                        text: "Password"
                    }
                    TextField {
                        id: tesiaPassword
                        Layout.fillWidth: true
                    }
                }
            }
        }


    }
}
