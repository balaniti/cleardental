// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTransparentPage {
    id: listDocPage
    title: "Pick a Location"

    CDClinicLocationManager {
        id: locListMan
    }

    CDTranslucentPane {
        anchors.centerIn: parent
        anchors.fill: parent
        anchors.margins: 100
        backMaterialColor: Material.Blue

        ListView {
            id: provList
            anchors.centerIn: parent
            model: locListMan.getListOfLocationFiles()
            anchors.fill: parent
            spacing: 25
            clip: true
            delegate: RowLayout {
                property string locationID:
                    modelData.split("/")[modelData.split("/").length-2]

                spacing: 50

                Settings {
                    id: locationInfo
                    fileName: modelData
                }

                anchors.horizontalCenter: parent.horizontalCenter
                Label {
                    text: "Location Name: "
                    font.bold: true
                }
                Label {
                    text: locationID
                    Component.onCompleted: {
                        if(locationID === locListMan.getLocalLocationName()) {
                            text = locationID + " (Here)"
                        }
                    }
                }
                Button {
                    text: "Edit"
                    icon.name: "document-edit"
                    onClicked: {
                        mainView.push("ManageLocationPage.qml");
                        mainView.currentItem.locationID = locationID;
                    }
                }

                Button {
                    icon.name: "list-remove"
                    Material.accent: Material.Red
                    highlighted: true
                    onClicked: {
                        locListMan.removeLocation(locationID)
                        provList.model =locListMan.getListOfLocationFiles();
                    }
                }
            }
        }
    }



    CDAddButton {
        text: "Add a Location"
        onClicked: {
            addLocDialog.open();
        }
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: 10
    }

    CDTranslucentDialog {
        id: addLocDialog

        ColumnLayout {
            CDTranslucentPane {
                backMaterialColor: Material.Purple
                GridLayout {
                    columns: 2
                    CDHeaderLabel {
                        text: "Add Location"
                        Layout.columnSpan: 2
                    }
                    Label {
                        text: "Location nickname:"
                        font.bold: true
                    }
                    TextField {
                        id: nickNameField
                        Layout.fillWidth: true
                    }
                    Label {
                        text: "Are you in this location now?"
                        font.bold: true
                    }

                    RowLayout {
                        RadioButton {
                            id: isLocal
                            text: "Yes, I am in this location now"
                        }

                        RadioButton {
                            checked: true
                            text: "No, I am somewhere else"
                        }
                    }
                }
            }
            RowLayout {
                Button {
                    text: "Back to list"
                    Material.accent: Material.Red
                    icon.name: "dialog-cancel"
                    highlighted: true
                    onClicked: addLocDialog.close()
                }

                Label {
                    Layout.fillWidth: true
                }

                Button {
                    text: "Add Location"
                    Material.accent: Material.Green
                    icon.name: "dialog-ok"
                    highlighted: true
                    enabled: nickNameField.text.length > 1
                    onClicked: {
                        var addMe = {};
                        addMe["nickName"] = nickNameField.text;
                        addMe["isLocal"] = isLocal.checked;
                        locListMan.addLocation(addMe);
                        provList.model =0;
                        provList.model =locListMan.getListOfLocationFiles();
                        addLocDialog.close();
                    }
                }
            }
        }
    }


}
