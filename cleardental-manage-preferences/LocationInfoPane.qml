// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTransparentPage {
    id: locationInfoPane

    function loadData() {
        locFile.fileName = fileLocs.getPracticePreferenceFile(locationID)
        locFile.category = "GeneralInfo"
        pracName.text = locFile.value("PracticeName",locationID)
        practiceTypeBox.currentIndex = practiceTypeBox.find(locFile.value("PracticeType","General Dentist Practice"))
        addr1.text = locFile.value("AddrLine1","")
        addr2.text = locFile.value("AddrLine2","")
        city.text = locFile.value("City","")
        state.setStateVal = locFile.value("State","")
        zip.text = locFile.value("Zip","")
        phoneNumber.text = locFile.value("PhoneNumber","")
        email.text = locFile.value("Email","")
        website.text = locFile.value("Website","")
        fTaxID.text = locFile.value("FederalTaxID","")
        newPatURL.text = locFile.value("NewPatURL","")
        pracNPI.text = locFile.value("PracticeNPI","")
        defClearingHouse.currentIndex = defClearingHouse.find(locFile.value("DefaultClearingHouse",""));

        sendCheckSame.checked = JSON.parse(locFile.value("SendCheckSame",true))
        if(!sendCheckSame.checked) {
            sendCheckAddr1.text = locFile.value("SendCheckAddr1","")
            sendCheckAddr2.text = locFile.value("SendCheckAddr2","")
            sendCheckCity.text = locFile.value("SendCheckCity","")
            sendCheckState.setStateVal = locFile.value("SendCheckState","")
            sendCheckZip.text = locFile.value("SendCheckZip","")
        }
    }

    function saveData() {
        locFile.category = "GeneralInfo"
        locFile.setValue("PracticeName",pracName.text);
        locFile.setValue("AddrLine1",addr1.text);
        locFile.setValue("AddrLine2",addr2.text);
        locFile.setValue("City",city.text);
        locFile.setValue("State",state.currentValue);
        locFile.setValue("Zip",zip.text);
        locFile.setValue("PhoneNumber",phoneNumber.text);
        locFile.setValue("Email",email.text);
        locFile.setValue("Website",website.text);
        locFile.setValue("FederalTaxID",fTaxID.text)
        locFile.setValue("NewPatURL",newPatURL.text)
        locFile.setValue("PracticeNPI",pracNPI.text)
        locFile.setValue("DefaultClearingHouse", defClearingHouse.displayText);

        locFile.setValue("SendCheckSame",sendCheckSame.checked);
        if(!sendCheckSame.checked) {
            locFile.setValue("SendCheckAddr1",sendCheckAddr1.text)
            locFile.setValue("SendCheckAddr2",sendCheckAddr2.text)
            locFile.setValue("SendCheckCity",sendCheckCity.text)
            locFile.setValue("SendCheckState",sendCheckState.currentValue)
            locFile.setValue("SendCheckZip",sendCheckZip.text)
        }

        locFile.setValue("PracticeType", practiceTypeBox.displayText);
        //https://www.findacode.com/pdf.html?id=taxonomy-codes-2023-231
        if(practiceTypeBox.displayText === "General Dentist Practice") {
            locFile.setValue("Taxonomy", "1223G0001X");
        }
        else if(practiceTypeBox.displayText === "Dental Anesthesiology") {
            locFile.setValue("Taxonomy", "1223D0004X");
        }
        else if(practiceTypeBox.displayText === "Dental Public Health") {
            locFile.setValue("Taxonomy", "1223D0001X");
        }
        else if(practiceTypeBox.displayText === "Endodontics") {
            locFile.setValue("Taxonomy", "1223E0200X");
        }
        else if(practiceTypeBox.displayText === "Oral and Maxillofacial Pathology") {
            locFile.setValue("Taxonomy", "1223P0106X");
        }
        else if(practiceTypeBox.displayText === "Oral and Maxillofacial Radiology") {
            locFile.setValue("Taxonomy", "1223D0008X");
        }
        else if(practiceTypeBox.displayText === "Oral and Maxillofacial Surgery") {
            locFile.setValue("Taxonomy", "1223S0112X");
        }
        else if(practiceTypeBox.displayText === "Oral Medicine") {
            locFile.setValue("Taxonomy", "125Q00000X");
        }
        else if(practiceTypeBox.displayText === "Orofacial Pain") {
            locFile.setValue("Taxonomy", "1223X2210X");
        }
        else if(practiceTypeBox.displayText === "Orthodontics and Dentofacial Orthopedics") {
            locFile.setValue("Taxonomy", "1223X0400X");
        }
        else if(practiceTypeBox.displayText === "Pediatric Dentistry") {
            locFile.setValue("Taxonomy", "1223P0221X");
        }
        else if(practiceTypeBox.displayText === "Periodontics") {
            locFile.setValue("Taxonomy", "1223P0300X");
        }
        else if(practiceTypeBox.displayText === "Prosthodontics") {
            locFile.setValue("Taxonomy", "1223P0700X");
        }
    }

    CDTranslucentPane {
        anchors.centerIn: parent
        backMaterialColor: Material.Red
        GridLayout {
            columnSpacing: 25
            columns: 2
            CDDescLabel {
                text: "Practice Name"
            }
            TextField {
                id: pracName
                Layout.minimumWidth: 350
                Layout.fillWidth: true
            }

            CheckBox {
                text: "I am at this location right now"
                checked: locationID === locListMan.getLocalLocationName()
                Layout.columnSpan: 2
            }

            CDDescLabel {
                text: "Type of Practice"
            }

            ComboBox {
                id: practiceTypeBox
                model: ["General Dentist Practice",
                    "Dental Anesthesiology",
                    "Dental Public Health",
                    "Endodontics",
                    "Oral and Maxillofacial Pathology",
                    "Oral and Maxillofacial Radiology",
                    "Oral and Maxillofacial Surgery",
                    "Oral Medicine",
                    "Orofacial Pain",
                    "Orthodontics and Dentofacial Orthopedics",
                    "Pediatric Dentistry",
                    "Periodontics",
                    "Prosthodontics"]
                Layout.fillWidth: true
            }

            CDDescLabel {
                text: "Address (Line 1)"
            }

            TextField {
                id: addr1
                Layout.fillWidth: true
            }

            CDDescLabel {
                text: "Address (Line 2)"
            }

            TextField {
                id: addr2
                Layout.fillWidth: true
            }
            CDDescLabel {
                text: "City"
            }

            TextField {
                id: city
                Layout.fillWidth: true
            }

            CDDescLabel {
                text: "State"
            }

            CDStatePicker {
                id: state
                Layout.fillWidth: true
            }

            Label {
                text: "Zip Code"
                font.bold: true
            }

            TextField {
                id: zip
                Layout.fillWidth: true
            }

            Label {
                text: "Phone Number"
                font.bold: true
            }

            TextField {
                id: phoneNumber
                Layout.fillWidth: true
            }

            Label {
                text: "Practice E-mail"
                font.bold: true
            }

            TextField {
                id: email
                Layout.fillWidth: true
            }

            Label {
                text: "Website"
                font.bold: true
            }

            TextField {
                id: website
                Layout.fillWidth: true
            }

            CDDescLabel {
                text: "New Patient URL"
                font.bold: true
            }

            TextField {
                id: newPatURL
                Layout.fillWidth: true
            }

            CDDescLabel {
                text: "Federal Tax ID Number"
            }

            TextField {
                id: fTaxID
                Layout.fillWidth: true
            }

            CDDescLabel {
                text: "Practice Location NPI"
            }

            TextField {
                id: pracNPI
                Layout.fillWidth: true
            }


            CDDescLabel {
                text: "Default Clearing House"
            }

            ComboBox {
                id: defClearingHouse
                model: ["EDS EDI","DentalXChange","Vyne/Tesia"]
                Layout.fillWidth: true
            }
        }
    }

    CDTranslucentPane {
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.margins: 10
        backMaterialColor: Material.Blue

        ColumnLayout {

            CDHeaderLabel {
                text: "Send Checks to"
            }

            CheckBox {
                id: sendCheckSame
                text: "Same address"
            }

            GridLayout {
                id: sendAddrGrid
                visible: !sendCheckSame.checked
                columns: 2
                Layout.fillWidth: true

                CDDescLabel {
                    text: "Address (Line 1)"
                }

                TextField {
                    id: sendCheckAddr1
                    Layout.fillWidth: true
                }

                CDDescLabel {
                    text: "Address (Line 2)"
                }

                TextField {
                    id: sendCheckAddr2
                    Layout.fillWidth: true
                }
                CDDescLabel {
                    text: "City"
                }

                TextField {
                    id: sendCheckCity
                    Layout.fillWidth: true
                }

                CDDescLabel {
                    text: "State"
                }

                CDStatePicker {
                    id: sendCheckState
                    Layout.fillWidth: true
                }

                Label {
                    text: "Zip Code"
                    font.bold: true
                }

                TextField {
                    id: sendCheckZip
                    Layout.fillWidth: true
                }
            }

        }
    }
}
