// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Window 2.10

CDTransparentPage {
    id: listDocPage
    title: "Pick a Provider"

    CDDoctorListManager {
        id: dListMan
    }

    CDTranslucentPane {
        anchors.centerIn: parent
        anchors.fill: parent
        anchors.margins: 100
        ListView {
            id: provList
            model: dListMan.getListOfStaffFiles()
            anchors.fill: parent
            spacing: 25
            clip: true
            delegate: RowLayout {
                property string providerID:
                    modelData.split("/")[modelData.split("/").length-2]

                spacing: 50

                Settings {
                    id: providerInfo
                    fileName: modelData
                }

                anchors.horizontalCenter: parent.horizontalCenter
                CDDescLabel {
                    text: "Name: "
                }
                Label {
                    Component.onCompleted: {
                        text = providerInfo.value("FirstName") + " " +
                                providerInfo.value("LastName") + " ";
                    }
                }
                CDDescLabel {
                    text: "Provider Type: "
                }
                Label {
                    Component.onCompleted: {
                        text = providerInfo.value("ProviderType");
                    }
                }
                Button {
                    text: "Edit"
                    icon.name: "document-edit"
                    onClicked: {
                        if(providerInfo.value("ProviderType") === "General Dentist") {
                            mainView.push("ManageGeneralDentistPage.qml");
                            mainView.currentItem.providerID = providerID;
                        }
                        else if(providerInfo.value("ProviderType") === "Hygienist") {
                            mainView.push("ManageHygienistPage.qml");
                            mainView.currentItem.providerID = providerID;
                        }
                        else if(providerInfo.value("ProviderType") === "Assistant") {
                            mainView.push("ManageAssistantPage.qml");
                            mainView.currentItem.providerID = providerID;
                        }
                        else {
                            mainView.push("ManageDentalSpecialistPage.qml");
                            mainView.currentItem.providerID = providerID;
                        }
                    }
                }

                Button {
                    icon.name: "list-remove"
                    Material.accent: Material.Red
                    highlighted: true
                    onClicked: {
                        dListMan.removeProvider(providerID);
                        provList.model = dListMan.getListOfStaffFiles()
                    }
                }
            }
        }
    }

    CDAddButton {
        text: "Add Provider"
        onClicked: {
            addProviderDialog.open();
        }
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: 10
    }

    CDTranslucentDialog {
        id: addProviderDialog

        ColumnLayout {
            CDTranslucentPane {
                backMaterialColor: Material.Purple
                GridLayout {
                    columns: 2
                    CDHeaderLabel {
                        text: "Add Provider"
                        Layout.columnSpan: 2
                    }
                    CDDescLabel {
                        text: "First Name"
                    }
                    TextField {
                        id: firstNameField
                        Layout.fillWidth: true
                    }
                    CDDescLabel {
                        text: "Last Name"
                    }
                    TextField {
                        id: lastNameField
                        Layout.fillWidth: true
                    }
                    CDDescLabel {
                        text: "Username"
                    }
                    TextField {
                        id: unixID

                        text: firstNameField.text.substring(0,1).toLocaleLowerCase()
                              + lastNameField.text.toLocaleLowerCase()

                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Provider Type"
                    }
                    ComboBox {
                        id: providerTypeBox
                        Layout.minimumWidth: 350
                        model: ["General Dentist","Dental Specialist","Hygienist","Assistant"]
                    }

                    CDDescLabel {
                        visible: providerTypeBox.currentIndex === 1
                        text: "Specialist Type"
                    }

                    ComboBox {
                        id: specialistTypeBox
                        visible: providerTypeBox.currentIndex === 1
                        Layout.fillWidth: true
                        model: ["Dental Anesthesiology",
                            "Dental Public Health",
                            "Endodontics",
                            "Oral and Maxillofacial Pathology",
                            "Oral and Maxillofacial Radiology",
                            "Oral and Maxillofacial Surgery",
                            "Oral Medicine",
                            "Orofacial Pain",
                            "Orthodontics and Dentofacial Orthopedics",
                            "Pediatric Dentistry",
                            "Periodontics",
                            "Prosthodontics"]
                    }


                }
            }
            RowLayout {
                Layout.fillWidth: true
                Button {
                    text: "Back to list"
                    Material.accent: Material.Red
                    icon.name: "dialog-cancel"
                    highlighted: true
                    onClicked: addProviderDialog.close()
                }

                Label {
                    Layout.fillWidth: true
                }

                Button {
                    text: "Add Provider"
                    Material.accent: Material.Green
                    icon.name: "dialog-ok"
                    highlighted: true
                    enabled: firstNameField.text.length > 1 &&
                             lastNameField.text.length > 1 &&
                             unixID.text.length > 1
                    onClicked: {
                        var addMe = {};
                        addMe["FirstName"] = firstNameField.text;
                        addMe["LastName"] = lastNameField.text;
                        addMe["UnixID"] = unixID.text;
                        if(providerTypeBox.currentIndex==1) {
                            addMe["ProviderType"] = specialistTypeBox.displayText;
                        }
                        else {
                            addMe["ProviderType"] = providerTypeBox.displayText;
                        }

                        dListMan.addProvider(addMe);
                        addProviderDialog.close();
                        provList.model = dListMan.getListOfStaffFiles();
                    }
                }
            }
        }


    }


}
