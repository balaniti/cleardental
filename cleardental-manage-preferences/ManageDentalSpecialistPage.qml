// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTransparentPage {
    id: manDSpecialistPage

    property string providerID: ""

    title: "Manage Dentist (" + providerID + ")"

    function loadData() {
        docFile.fileName = fileLocs.getDocInfoFile(providerID);
        firstNameField.text = docFile.value("FirstName","")
        lastNameField.text = docFile.value("LastName","")
        licenseField.text = docFile.value("LicenseID","")
        providerTypeBox.currentIndex = providerTypeBox.find( docFile.value("ProviderType","Oral Medicine"))
        npiField.text = docFile.value("NPINumb","")
        deaField.text = docFile.value("DEANumb","")
    }


    CDFileLocations {
        id: fileLocs
    }

    Settings {
        id: docFile
        fileName: fileLocs.getDocInfoFile(providerID)
    }

    CDGitManager {
        id: gitMan
    }

    CDTranslucentPane {
        id: backPane
        anchors.centerIn: parent
        backMaterialColor: schColor.buttonColors[schColor.setColorIndex]
        GridLayout {
            columnSpacing: 25
            columns: 2
            CDDescLabel {
                text: "First Name"
            }
            TextField {
                id: firstNameField
            }
            CDDescLabel {
                text: "Last Name"
            }
            TextField {
                id: lastNameField
            }
            CDDescLabel {
                text: "Specialty Type"
            }
            ComboBox {
                id: providerTypeBox
                Layout.minimumWidth: 250
                Layout.fillWidth: true
                model: ["Dental Anesthesiology",
                    "Dental Public Health",
                    "Endodontics",
                    "Oral and Maxillofacial Pathology",
                    "Oral and Maxillofacial Radiology",
                    "Oral and Maxillofacial Surgery",
                    "Oral Medicine",
                    "Orofacial Pain",
                    "Orthodontics and Dentofacial Orthopedics",
                    "Pediatric Dentistry",
                    "Periodontics",
                    "Prosthodontics"]
            }
            CDDescLabel {
                text: "Schedule Color"
            }
            GridLayout {
                id: schColor
                columns: 10

                property int setColorIndex: 0

                function setColor(stringName) {
                    setColorIndex = buttonColorStrings.indexOf(stringName);

                }

                property var buttonColors: [Material.Red,Material.Pink,
                    Material.Purple, Material.DeepPurple, Material.Indigo,
                    Material.Blue, Material.LightBlue, Material.Cyan, Material.Teal,
                    Material.Green, Material.LightGreen, Material.Lime,
                    Material.Yellow, Material.Amber, Material.Orange,
                    Material.DeepOrange, Material.Brown, Material.Grey,
                    Material.BlueGrey]

                property var buttonColorStrings: ["Red","Pink","Purple",
                    "DeepPurple","Indigo","Blue","LightBlue","Cyan","Teal","Green",
                    "LightGreen","Lime","Yellow","Amber","Orange","DeepOrange",
                    "Brown","Grey","BlueGrey"]

                Repeater {
                    model: schColor.buttonColors.length
                    Button {
                        property string colorName:
                            schColor.buttonColorStrings[index]
                        highlighted: true
                        Layout.maximumWidth: 50
                        Material.accent:schColor.buttonColors[index]
                        checkable: true
                        checked: index==schColor.setColorIndex
                        icon.name: checked ? "dialog-ok" : ""
                        onClicked: {
                            backPane.backMaterialColor =  schColor.buttonColors[index]
                        }
                    }
                }

                ButtonGroup {
                    id: checkedColor
                    buttons: schColor.children
                }

            }

            CDDescLabel {
                text: "License ID"
            }
            TextField {
                id: licenseField
            }
            CDDescLabel {
                text: "NPI Number"
            }
            TextField {
                id: npiField
            }
            CDDescLabel {
                text: "DEA Number"
            }
            TextField {
                id: deaField
            }
        }
    }



    CDSaveButton {
        text: "Save + Go Back"
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        enabled: firstNameField.text.length > 1 &&
                 lastNameField.text.length > 1
        onClicked: {
            docFile.setValue("FirstName",firstNameField.text);
            docFile.setValue("LastName",lastNameField.text);
            docFile.setValue("ProviderType",providerTypeBox.displayText);
            docFile.setValue("ProviderColor",checkedColor.checkedButton.colorName);
            docFile.setValue("LicenseID",licenseField.text);
            docFile.setValue("NPINumb",npiField.text);
            docFile.setValue("DEANumb", deaField.text);


            //https://www.findacode.com/pdf.html?id=taxonomy-codes-2023-231
            if(providerTypeBox.displayText === "Dental Anesthesiology") {
                docFile.setValue("Taxonomy", "1223D0004X");
            }
            else if(providerTypeBox.displayText === "Dental Public Health") {
                docFile.setValue("Taxonomy", "1223D0001X");
            }
            else if(providerTypeBox.displayText === "Endodontics") {
                docFile.setValue("Taxonomy", "1223E0200X");
            }
            else if(providerTypeBox.displayText === "Oral and Maxillofacial Pathology") {
                docFile.setValue("Taxonomy", "1223P0106X");
            }
            else if(providerTypeBox.displayText === "Oral and Maxillofacial Radiology") {
                docFile.setValue("Taxonomy", "1223D0008X");
            }
            else if(providerTypeBox.displayText === "Oral and Maxillofacial Surgery") {
                docFile.setValue("Taxonomy", "1223S0112X");
            }
            else if(providerTypeBox.displayText === "Oral Medicine") {
                docFile.setValue("Taxonomy", "125Q00000X");
            }
            else if(providerTypeBox.displayText === "Orofacial Pain") {
                docFile.setValue("Taxonomy", "1223X2210X");
            }
            else if(providerTypeBox.displayText === "Orthodontics and Dentofacial Orthopedics") {
                docFile.setValue("Taxonomy", "1223X0400X");
            }
            else if(providerTypeBox.displayText === "Pediatric Dentistry") {
                docFile.setValue("Taxonomy", "1223P0221X");
            }
            else if(providerTypeBox.displayText === "Periodontics") {
                docFile.setValue("Taxonomy", "1223P0300X");
            }
            else if(providerTypeBox.displayText === "Prosthodontics") {
                docFile.setValue("Taxonomy", "1223P0700X");
            }



            docFile.sync();
            gitMan.commitData("Updated information for " + providerID);
            mainView.pop();
        }
    }

    onProviderIDChanged: {
        loadData();
    }

}
