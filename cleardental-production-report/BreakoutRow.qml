// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtGraphicalEffects 1.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

Row {
    id: breakoutRow
    property int setTheMinWidth: 1024
    Layout.minimumHeight: totalPatLabel.height
    Layout.minimumWidth: setTheMinWidth;

    function setBreakdown(breakdownObj) {
        var totalCount =0;
        //console.debug(JSON.stringify(breakdownObj));

        if("DISMISSED" in breakdownObj) {
            dismissedText.text = breakdownObj["DISMISSED"];
            dismissedRect.width = Math.max(breakdownObj["DISMISSED"],dismissedText.width + 10);
            totalCount+= breakdownObj["DISMISSED"];
        }

        if("NEVERMINDER" in breakdownObj) {
            neverminderText.text = breakdownObj["NEVERMINDER"];
            neverminderRct.width = Math.max(breakdownObj["NEVERMINDER"],neverminderText.width + 10);
            totalCount+= breakdownObj["NEVERMINDER"];
        }

        if("NEVER_SEEN" in breakdownObj) {
            neverSeenText.text = breakdownObj["NEVER_SEEN"];
            neverSeenRct.width = Math.max(breakdownObj["NEVER_SEEN"],neverSeenText.width + 10);
            totalCount+= breakdownObj["NEVER_SEEN"];
        }

        if("ONCE_SEEN" in breakdownObj) {
            onceSeenText.text = breakdownObj["ONCE_SEEN"];
            onceSeenRct.width = Math.max(breakdownObj["ONCE_SEEN"],onceSeenText.width + 10);
            totalCount+= breakdownObj["ONCE_SEEN"];
        }

        if("GHOSTED" in breakdownObj) {
            ghostedText.text = breakdownObj["GHOSTED"];
            ghostedRct.width = Math.max(breakdownObj["GHOSTED"],ghostedText.width + 10);
            totalCount+= breakdownObj["GHOSTED"];
        }

        if("FORGOTTEN" in breakdownObj) {
            forgottenText.text = breakdownObj["FORGOTTEN"];
            forgottenRct.width = Math.max(breakdownObj["FORGOTTEN"],forgottenText.width + 10);
            totalCount+= breakdownObj["FORGOTTEN"];
        }

        if("ACTIVE_PATIENT" in breakdownObj) {
            activePatText.text = breakdownObj["ACTIVE_PATIENT"];
            activePatRct.width = Math.max(breakdownObj["ACTIVE_PATIENT"],activePatText.width + 10);
            totalCount+= breakdownObj["ACTIVE_PATIENT"];
        }

        //totalCountLabel.text = totalCount + " ";
        //setTheMinWidth = totalCount + totalCountLabel.width;
        setTheMinWidth = totalCount;

        return totalCount;
    }

//    Label {
//        id: totalCountLabel
//    }

    Rectangle {
        id: dismissedRect
        height: totalPatLabel.height
        width: 0
        color: Material.color(Material.Red);
        Label {
            id: dismissedText
            anchors.centerIn: parent
        }
    }

    Rectangle {
        id: neverminderRct
        height: totalPatLabel.height
        width: 0
        color: Material.color(Material.Grey);
        Label {
            id: neverminderText
            anchors.centerIn: parent
        }
    }
    Rectangle {
        id: neverSeenRct
        height: totalPatLabel.height
        width: 0
        color: Material.color(Material.BlueGrey);
        Label {
            id: neverSeenText
            anchors.centerIn: parent
        }
    }
    Rectangle {
        id: onceSeenRct
        height: totalPatLabel.height
        width: 0
        color: Material.color(Material.Indigo);
        Label {
            id: onceSeenText
            anchors.centerIn: parent
        }
    }
    Rectangle {
        id: ghostedRct
        height: totalPatLabel.height
        width: 0
        color: Material.color(Material.Cyan);
        Label {
            id: ghostedText
            anchors.centerIn: parent
        }
    }
    Rectangle {
        id: forgottenRct
        height: totalPatLabel.height
        width: 0
        color: Material.color(Material.Lime);
        Label {
            id: forgottenText
            anchors.centerIn: parent
        }
    }
    Rectangle {
        id: activePatRct
        height: totalPatLabel.height
        width: 0
        color: Material.color(Material.Green);
        Label {
            id: activePatText
            anchors.centerIn: parent
        }
    }

}
