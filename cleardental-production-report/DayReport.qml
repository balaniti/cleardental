// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtGraphicalEffects 1.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDTransparentPage {
    ColumnLayout {
        id: dayReport
        anchors.fill: parent
        CDTranslucentPane {
            Layout.alignment: Qt.AlignHCenter
            backMaterialColor: Material.Green

            ColumnLayout {
                CDHeaderLabel {
                    text: "Select Day"
                }
                CDCalendarDatePicker {
                    id: dayPicker

                }

            }
        }

        CDTranslucentPane {
            Layout.alignment: Qt.AlignHCenter
            backMaterialColor: Material.LightBlue

            GridLayout {
                columns: 2
                CDHeaderLabel {
                    text: "Overall Report"
                    Layout.columnSpan: 2
                }

                CDDescLabel {
                    text: "Patients Seen"
                }

                Label {
                    text: reportMaker.patientsSeen(dayPicker.myDate,dayPicker.myDate)

                }

                CDDescLabel {
                    text: "Patients No-showed"
                }

                Label {
                    text: reportMaker.patientsNoShowed(dayPicker.myDate,dayPicker.myDate)

                }

                CDDescLabel {
                    text: "Total Production"
                }

                Label {
                    text: "$" + (reportMaker.totalProduction(dayPicker.myDate,dayPicker.myDate) / 100.0)

                }

                CDDescLabel {
                    text: "Total Collections"
                }

                Label {
                    text: "$" + (reportMaker.totalCollections(dayPicker.myDate,dayPicker.myDate) / 100.0)

                }

                CDDescLabel {
                    text: "Total Writeoffs"
                }

                Label {
                    text: "$" + (reportMaker.totalWriteoffs(dayPicker.myDate,dayPicker.myDate) / 100.0)

                }
            }
        }

        CDTranslucentPane {
            Layout.alignment: Qt.AlignHCenter
            backMaterialColor: Material.Orange

            ColumnLayout {
                CDHeaderLabel {
                    text: "Provider Report"
                }

                CDDoctorListManager {
                    id: docMan
                }

                Repeater {

                    model: docMan.getListOfProviderFiles().length

                    RowLayout {
                        Settings {
                            id: docInfo
                            fileName: docMan.getListOfProviderFiles()[index]
                        }
                        CDDescLabel {
                            id: desLabel
                            Component.onCompleted: {
                                text =  docInfo.value("UnixID")
                            }
                        }

                        Label{Layout.minimumWidth: 50;}

                        Label {
                            id: totalProductionLabel
                            text: "Production: $" + (reportMaker.totalProduction(dayPicker.myDate,
                                                                          dayPicker.myDate,
                                                                          desLabel.text) /100.0)
                        }

                        Label{Layout.minimumWidth: 50;}

                        Label {
                            id: totalCollectionLabel
                            text: "Collection: $" + (reportMaker.totalCollections(dayPicker.myDate,
                                                                          dayPicker.myDate,
                                                                          desLabel.text) /100.0)
                        }
                    }
                }
            }
        }
    }


}

