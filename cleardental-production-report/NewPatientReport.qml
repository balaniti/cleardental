// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtGraphicalEffects 1.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDTransparentPage {
    ColumnLayout {
        id: newPatientReport
        anchors.fill: parent
        CDTranslucentPane {
            Layout.alignment: Qt.AlignHCenter
            backMaterialColor: Material.Blue

            GridLayout {
                columns: 3
                CDHeaderLabel {
                    text:"Last 30 days"
                    Layout.columnSpan: 3
                }

                CDDescLabel {
                    text: "New Patients"
                }
                Label {
                    id: grandTotalLabel30
                }
                BreakoutRow {
                    id: last30Row
                }

                MenuSeparator {Layout.columnSpan: 3;Layout.fillWidth: true}

                CDDescLabel {
                    text: "Referral Sources"
                }

                ColumnLayout {
                    Layout.columnSpan: 2;
                    Repeater {
                        id: last30SourceRep
                        property var refModel : []
                        property var refModelSourceNames: []
                        property var totalsArray: []
                        RowLayout {
                            Label {
                                text: last30SourceRep.refModelSourceNames[index]
                                Layout.minimumWidth: 150
                            }
                            Label {
                                id: totalLabel30
                                Layout.minimumWidth: 25
                            }
                            BreakoutRow {
                                Component.onCompleted: {
                                    var totalAdd = setBreakdown(last30SourceRep.refModel[index]);
                                    last30SourceRep.totalsArray[index] = totalAdd;
                                    totalLabel30.text = last30SourceRep.totalsArray[index]
                                }
                            }
                        }
                    }
                }
            }

            Component.onCompleted: {
                var last30RefModel = reportMaker.get30DayReferrals();
                grandTotalLabel30.text = last30Row.setBreakdown(last30RefModel["Total"]);

                delete last30RefModel["Total"];
                last30SourceRep.refModelSourceNames = Object.keys(last30RefModel);

                for(var k=0;k<last30SourceRep.refModelSourceNames.length;k++) {
                    last30SourceRep.refModel.push(last30RefModel[last30SourceRep.refModelSourceNames[k]]);
                }
                last30SourceRep.model = last30SourceRep.refModelSourceNames.length;

            }
        }

        CDTranslucentPane {
            Layout.alignment: Qt.AlignHCenter
            backMaterialColor: Material.Green

            GridLayout {
                columns: 3
                CDHeaderLabel {
                    text:"Overall"
                    Layout.columnSpan: 3
                }

                CDDescLabel {
                    id: totalPatLabel
                    text: "Total Patients"
                }

                Label {
                    id: grandTotalLabel
                }

                BreakoutRow {
                    id: totalRow
                }

                MenuSeparator {Layout.columnSpan: 3;Layout.fillWidth: true}


                CDDescLabel {
                    text: "Referral Sources"
                }

                ColumnLayout {
                    Layout.columnSpan: 2;
                    Repeater {
                        id: totalSourceRep
                        property var totalRefModel : []
                        property var totalRefModelSourceNames: []
                        property var totalsArray: []
                        property var productionTotalArray: []
                        RowLayout {
                            Label {
                                id: totalSourceLab
                                text: totalSourceRep.totalRefModelSourceNames[index]
                                Layout.minimumWidth: 250
                            }
                            Label {
                                id: totalLabel
                                Layout.minimumWidth: 25
                            }
                            BreakoutRow {
                                Component.onCompleted: {
                                    var totalAdd = setBreakdown(totalSourceRep.totalRefModel[index]);
                                    totalSourceRep.totalsArray[index] = totalAdd;
                                    totalLabel.text = totalSourceRep.totalsArray[index]
                                    var totalProFloat=parseFloat(totalSourceRep.productionTotalArray[index]).toFixed(2);
                                    productionLabel.text = "$" + totalProFloat + " ($" +
                                            (totalProFloat / parseFloat(totalLabel.text)).toFixed(2) + ")";
                                }
                            }
                            Label {
                                id: productionLabel
                                text: "$?"
                            }
                        }
                    }
                }
            }

            Component.onCompleted: {
                var totalRefModel = reportMaker.getTotalReferrals();
                grandTotalLabel.text = totalRow.setBreakdown(totalRefModel["Total"]);

                delete totalRefModel["Total"];
                totalSourceRep.totalRefModelSourceNames = Object.keys(totalRefModel);

                for(var i=0;i<totalSourceRep.totalRefModelSourceNames.length;i++) {
                    var sourceName = totalSourceRep.totalRefModelSourceNames[i];
                    totalSourceRep.totalRefModel.push(totalRefModel[sourceName]);
                    totalSourceRep.productionTotalArray.push(reportMaker.getCollectionsFromRefferalType(sourceName) / 100);
                }
                totalSourceRep.model = totalSourceRep.totalRefModelSourceNames.length;

            }
        }
    }

    CDTranslucentPane {
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.margins: 10
        backMaterialColor: Material.Pink
        ColumnLayout {
            CDHeaderLabel {
                text: "Legend"
            }

            Repeater {
                id: legRep
                model: 7
                property var legendNames: ["Dismissed","Neverminder","Never Seen","Once Seen","Ghosted","Forgotten",
                    "Active"]
                property var legendColors: [
                    Material.color(Material.Red),
                    Material.color(Material.Grey),
                    Material.color(Material.BlueGrey),
                    Material.color(Material.Indigo),
                    Material.color(Material.Cyan),
                    Material.color(Material.Lime),
                    Material.color(Material.Green),
                ]
                Label {
                    text: legRep.legendNames[index]
                    background: Rectangle {
                        color: legRep.legendColors[index]
                        anchors.fill: parent
                    }
                }
            }
        }
    }

}

