// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtGraphicalEffects 1.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDTransparentPage {
    ColumnLayout {
        id: dayReport
        anchors.fill: parent
        CDTranslucentPane {
            Layout.alignment: Qt.AlignHCenter
            backMaterialColor: Material.Green

            GridLayout {
                columns: 2
                CDHeaderLabel {
                    text: "Select Year"
                    Layout.columnSpan: 2
                }

                ComboBox {
                    id: yearPicker

                    property date startDay
                    property date endDay

                    Component.onCompleted: {
                        var makeModel = [];
                        for(var i=2000;i<2050;i++) {
                            makeModel.push(i)
                        }
                        model = makeModel;

                        var today = new Date()
                        currentIndex = today.getFullYear() - 2000;
                    }

                    onCurrentIndexChanged: {
                        startDay = new Date(currentIndex + 2000, 0,1);
                        endDay = new Date(currentIndex + 2001, 0,0);
                    }
                }
            }
        }

        CDTranslucentPane {
            Layout.alignment: Qt.AlignHCenter
            backMaterialColor: Material.LightBlue

            GridLayout {
                columns: 2
                CDHeaderLabel {
                    text: "Overall Report"
                    Layout.columnSpan: 2
                }

                CDDescLabel {
                    text: "Patients Seen"
                }

                Label {
                    text: reportMaker.patientsSeen(yearPicker.startDay,yearPicker.endDay)
                }

                CDDescLabel {
                    text: "Patients No-showed"
                }

                Label {
                    text: reportMaker.patientsNoShowed(yearPicker.startDay,yearPicker.endDay)

                }

                CDDescLabel {
                    text: "Total Production"
                }

                Label {
                    text: "$" + (reportMaker.totalProduction(yearPicker.startDay,yearPicker.endDay)/100)
                }

                CDDescLabel {
                    text: "Total Collections"
                }

                Label {
                    text: "$" + (reportMaker.totalCollections(yearPicker.startDay,yearPicker.endDay)/100)

                }

                CDDescLabel {
                    text: "Total Writeoffs"
                }

                Label {
                    text: "$" + (reportMaker.totalWriteoffs(yearPicker.startDay,yearPicker.endDay)/100)

                }
            }
        }

        CDTranslucentPane {
            Layout.alignment: Qt.AlignHCenter
            backMaterialColor: Material.Orange

            ColumnLayout {
                CDHeaderLabel {
                    text: "Provider Report"
                }

                CDDoctorListManager {
                    id: docMan
                }

                Repeater {
                    model: docMan.getListOfProviderFiles().length
                    RowLayout {
                        Settings {
                            id: docInfo
                            fileName: docMan.getListOfProviderFiles()[index]
                        }
                        CDDescLabel {
                            id: desLabel
                            Component.onCompleted: {
                                text =  docInfo.value("UnixID")
                            }
                        }

                        Label{Layout.minimumWidth: 50;}

                        Label {
                            id: totalProductionLabel
                            text: "Production: $" + (reportMaker.totalProduction(yearPicker.startDay,yearPicker.endDay,
                                                                          desLabel.text) /100.0)
                        }

                        Label{Layout.minimumWidth: 50;}

                        Label {
                            id: totalCollectionLabel
                            text: "Collection: $" + (reportMaker.totalCollections(yearPicker.startDay,yearPicker.endDay,
                                                                          desLabel.text) /100.0)
                        }
                    }
                }
            }
        }
    }


}

