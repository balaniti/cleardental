// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtGraphicalEffects 1.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

ListView {
    id: balanceListView

    CDToolLauncher {id: toolLauncher}

    property int rowWidth: (rootWin.width-100) / 7
    ScrollBar.vertical: ScrollBar { }
    headerPositioning: ListView.OverlayHeader
    clip: true

    property var dueBalancesList: reportMaker.getDueBalances()
    model: dueBalancesList.length
    header: Pane {
        z: 1024
        background: Rectangle {
            color: Material.color(Material.LightBlue)
            anchors.fill: parent
            radius: 10
            opacity: .75
        }

        RowLayout {
            Label {
                font.bold: true
                Layout.minimumWidth: balanceListView.rowWidth*2
                text: "Patient Name"
            }
            Label {
                font.bold: true
                Layout.minimumWidth: balanceListView.rowWidth
                text: "Dental Plan"
            }
            Label {
                font.bold: true
                Layout.minimumWidth: balanceListView.rowWidth
                text: "Balance"
            }
            Label {
                font.bold: true
                Layout.minimumWidth: balanceListView.rowWidth
                text: "Total Collections"
            }
            Label {
                font.bold: true
                Layout.minimumWidth: balanceListView.rowWidth
                text: "Total Charged"
            }
            Label {
                font.bold: true
                Layout.minimumWidth: balanceListView.rowWidth
            }
        }
    }

    delegate: Component {
        Pane {
            background: Rectangle {
                color: "white"
                opacity: .25

                MouseArea {
                    anchors.fill: parent
                    onContainsMouseChanged: {
                        if(containsMouse) {
                            parent.opacity = .75
                        }
                        else {
                            parent.opacity = .25
                        }
                    }
                    hoverEnabled: true
                }

                Behavior on opacity {
                    PropertyAnimation {
                        duration: 150
                    }
                }

            }
            RowLayout {
                Label {
                    text: dueBalancesList[index]
                    Layout.minimumWidth: balanceListView.rowWidth*2
                    Layout.maximumWidth: balanceListView.rowWidth*2
                }
                Label {
                    text: reportMaker.getDentalPlanName(dueBalancesList[index])
                    Layout.minimumWidth: balanceListView.rowWidth
                    Layout.maximumWidth: balanceListView.rowWidth
                }
                Label {
                    text: "$" + (reportMaker.getBalance(dueBalancesList[index])/100)
                    Layout.minimumWidth: balanceListView.rowWidth
                    Layout.maximumWidth: balanceListView.rowWidth
                }
                Label {
                    text: "$" + (reportMaker.getPayments(dueBalancesList[index])/100)
                    Layout.minimumWidth: balanceListView.rowWidth
                    Layout.maximumWidth: balanceListView.rowWidth
                }
                Label {
                    text: "$" + (reportMaker.getCharges(dueBalancesList[index])/100)
                    Layout.minimumWidth: balanceListView.rowWidth
                    Layout.maximumWidth: balanceListView.rowWidth
                }
                Button {
                    Layout.minimumWidth: balanceListView.rowWidth/2
                    Layout.maximumWidth: balanceListView.rowWidth/2
                    text: "Billing"
                    onClicked: toolLauncher.launchTool(CDToolLauncher.Billing,dueBalancesList[index]);
                }

                Button {
                    Layout.minimumWidth: balanceListView.rowWidth/2
                    Layout.maximumWidth: balanceListView.rowWidth/2
                    text: "Old Billing"
                    onClicked: toolLauncher.launchTool(CDToolLauncher.LegacyBilling,dueBalancesList[index]);
                }
            }
        }
    }
}
