// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtGraphicalEffects 1.12
import QtQuick.Window 2.10

CDAppWindow {
    id: rootWin
    title: "Production Report: " + reportButtons.checkedButton.text

    header: CDBlankToolBar {
        headerText: rootWin.title

        ToolButton {
             icon.name: "application-menu"
             icon.width: 64
             icon.height: 64
             onClicked: drawer.open();
             anchors.left: parent.left
         }

    }

    Drawer {
        id: drawer
        width: 0.33 * rootWin.width
        height: rootWin.height

        background: Rectangle {
            gradient: Gradient {
                     GradientStop { position: 0.0; color: "white" }
                     GradientStop { position: 1; color: "transparent" }
                 }
            anchors.fill: parent
            radius: 10
        }
        modal: false

        ColumnLayout {
            anchors.left:  parent.left
            anchors.right: parent.right
            TabButton {
                id: dayTab
                text: "Day Report"
                Layout.fillWidth: true
                onClicked: {
                    stackView.replace(stackView.currentItem,"DayReport.qml");
                    drawer.close();
                }
                checked: true
            }

            TabButton {
                id: weekTab
                text: "Week Report"
                Layout.fillWidth: true
                onClicked: {
                    stackView.replace(stackView.currentItem,"WeekReport.qml");
                    drawer.close();
                }
            }

            TabButton {
                id: monthTab
                text: "Month Report"
                Layout.fillWidth: true
                onClicked: {
                    stackView.replace(stackView.currentItem,"MonthReport.qml");
                    drawer.close();
                }
            }

            TabButton {
                id: yearTab
                text: "Year Report"
                Layout.fillWidth: true
                onClicked: {
                    stackView.replace(stackView.currentItem,"YearReport.qml");
                    drawer.close();
                }
            }

            TabButton {
                id: allTimeTab
                text: "All Time Report"
                Layout.fillWidth: true
                onClicked: {
                    stackView.replace(stackView.currentItem,"AllTimeReport.qml");
                    drawer.close();
                }
            }
            TabButton {
                id: customTimeTab
                text: "Custom Rage Report"
                Layout.fillWidth: true
                onClicked: {
                    stackView.replace(stackView.currentItem,"CustomRageReport.qml");
                    drawer.close();
                }
            }

            TabButton {
                id: negBalanceTab
                text: "Balance Due Report"
                Layout.fillWidth: true
                onClicked: {
                    stackView.replace(stackView.currentItem,"NegBalanceReport.qml");
                    drawer.close();
                }
            }

            TabButton {
                id: needApptTab
                text: "Need Appointment Report"
                Layout.fillWidth: true
                onClicked: {
                    stackView.replace(stackView.currentItem,"NeedApptReport.qml");
                    drawer.close();
                }
            }

            TabButton {
                id: newPatReport
                text: "New Patients Report"
                Layout.fillWidth: true
                onClicked: {
                    stackView.replace(stackView.currentItem,"NewPatientReport.qml");
                    drawer.close();
                }
            }




            ButtonGroup {
                id: reportButtons
                buttons: [dayTab,weekTab,monthTab,yearTab,allTimeTab,negBalanceTab,needApptTab,newPatReport]
            }

        }
    }

    StackView {
        id: stackView
        anchors.fill: parent

        initialItem: ProcedureCountReport {
        }

    }

    CDReporter {
        id: reportMaker
    }
}
