// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtGraphicalEffects 1.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDTransparentPage {
    ColumnLayout {
        id: procedureCountReport
        anchors.fill: parent

        property int millisecondsPerDay: 24 * 60 * 60 * 1000;

        function updateReport() {
            var setText = "Showing rage from: " + fromDayPicker.myDate.toLocaleDateString()
                    + " to " + toDayPicker.myDate.toLocaleDateString() + " (";
            var differenceInDays = (toDayPicker.myDate.getTime() - fromDayPicker.myDate.getTime()) / millisecondsPerDay;
            differenceInDays = Math.floor(differenceInDays);
            setText += differenceInDays + " days)"

            console.debug(JSON.stringify(reportMaker.getProcedureCounts(fromDayPicker.myDate,toDayPicker.myDate)));
        }

        CDTranslucentPane {
            Layout.alignment: Qt.AlignHCenter
            backMaterialColor: Material.Green

            GridLayout {
                columns: 2
                columnSpacing: 10
                CDHeaderLabel {
                    Layout.columnSpan: 2
                    text: "Select Range"
                }
                CDDescLabel {
                    text: "From"
                }
                CDDescLabel {
                    text: "To"
                }

                CDCalendarDatePicker {
                    id: fromDayPicker
                    onMyDateChanged: procedureCountReport.updateReport();
                    Component.onCompleted: {
                        var today = new Date();
                        var setDate = new Date(today.getFullYear(),today.getMonth(),today.getDate()-1);
                        myDate = setDate;
                    }
                }

                CDCalendarDatePicker {
                    id: toDayPicker
                    onMyDateChanged: procedureCountReport.updateReport();
                }
                Label {
                    id: infoLabel
                    Layout.columnSpan: 2
                }
            }
        }

        CDTranslucentPane {
            Layout.alignment: Qt.AlignHCenter
            backMaterialColor: Material.LightBlue

            GridLayout {
                columns: 2
                CDHeaderLabel {
                    text: "Procedure Count"
                    Layout.columnSpan: 2
                }




            }
        }


    }

}
