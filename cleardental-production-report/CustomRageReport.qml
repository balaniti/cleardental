// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtGraphicalEffects 1.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDTransparentPage {
    ColumnLayout {
        id: customRageReport
        anchors.fill: parent

        property int millisecondsPerDay: 24 * 60 * 60 * 1000;

        function updateReport() {
            var setText = "Showing rage from: " + fromDayPicker.myDate.toLocaleDateString()
                    + " to " + toDayPicker.myDate.toLocaleDateString() + " (";
            var differenceInDays = (toDayPicker.myDate.getTime() - fromDayPicker.myDate.getTime()) / millisecondsPerDay;
            differenceInDays = Math.floor(differenceInDays);
            setText += differenceInDays + " days)"
            infoLabel.text = setText;
        }

        CDTranslucentPane {
            Layout.alignment: Qt.AlignHCenter
            backMaterialColor: Material.Green

            GridLayout {
                columns: 2
                columnSpacing: 10
                CDHeaderLabel {
                    Layout.columnSpan: 2
                    text: "Select Range"
                }
                CDDescLabel {
                    text: "From"
                }
                CDDescLabel {
                    text: "To"
                }

                CDCalendarDatePicker {
                    id: fromDayPicker
                    onMyDateChanged: customRageReport.updateReport();
                    Component.onCompleted: {
                        var today = new Date();
                        var setDate = new Date(today.getFullYear(),today.getMonth(),today.getDate()-1);
                        myDate = setDate;
                    }
                }

                CDCalendarDatePicker {
                    id: toDayPicker
                    onMyDateChanged: customRageReport.updateReport();
                }
                Label {
                    id: infoLabel
                    Layout.columnSpan: 2
                }
            }
        }

        CDTranslucentPane {
            Layout.alignment: Qt.AlignHCenter
            backMaterialColor: Material.LightBlue

            GridLayout {
                columns: 2
                CDHeaderLabel {
                    text: "Range Report"
                    Layout.columnSpan: 2
                }

                CDDescLabel {
                    text: "Patients Seen"
                }

                Label {
                    text: reportMaker.patientsSeen(fromDayPicker.myDate,toDayPicker.myDate)
                }

                CDDescLabel {
                    text: "Patients No-showed"

                }

                Label {
                    text: reportMaker.patientsNoShowed(fromDayPicker.myDate,toDayPicker.myDate)
                }

                CDDescLabel {
                    text: "Total Production"
                }

                Label {
                    text: "$" + (reportMaker.totalProduction(fromDayPicker.myDate,toDayPicker.myDate)/100)
                }

                CDDescLabel {
                    text: "Total Collections"
                }

                Label {
                    text: "$" + (reportMaker.totalCollections(fromDayPicker.myDate,toDayPicker.myDate)/100)
                }

                CDDescLabel {
                    text: "Total Writeoffs"
                }

                Label {
                    text: "$" + (reportMaker.totalWriteoffs(fromDayPicker.myDate,toDayPicker.myDate)/100)
                }
            }
        }

        CDTranslucentPane {
            Layout.alignment: Qt.AlignHCenter
            backMaterialColor: Material.Orange

            ColumnLayout {
                CDHeaderLabel {
                    text: "Provider Report"
                }

                CDDoctorListManager {
                    id: docMan
                }

                Repeater {
                    model: docMan.getListOfProviderFiles().length
                    RowLayout {
                        Settings {
                            id: docInfo
                            fileName: docMan.getListOfProviderFiles()[index]
                        }
                        CDDescLabel {
                            id: desLabel
                            Component.onCompleted: {
                                text =  docInfo.value("UnixID")
                            }
                        }

                        Label{Layout.minimumWidth: 50;}

                        Label {
                            id: totalProductionLabel
                            text: "Production: $"+(reportMaker.totalProduction(fromDayPicker.myDate,toDayPicker.myDate,
                                                                          desLabel.text) /100.0)
                        }

                        Label{Layout.minimumWidth: 50;}

                        Label {
                            id: totalCollectionLabel
                            text: "Collection: $"+(reportMaker.totalCollections(fromDayPicker.myDate,toDayPicker.myDate,
                                                                          desLabel.text) /100.0)
                        }
                    }
                }
            }
        }

    }

}
