// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.2
import dental.clear 1.0
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDAppWindow {
    id: rootWin
    title: qsTr("Intraoral Exam " + " [ID: " + PATIENT_FILE_NAME + "]")

    header: CDPatientToolBar {
         headerText: "Intraoral Exam:"
    }

    function loadPrevData() {
        tongueRow.loadPrevData(ioESaver.value("Tongue",""));
        fomRow.loadPrevData(ioESaver.value("FloorOfMouth",""));
        palRow.loadPrevData(ioESaver.value("Palate",""));
        occRow.loadPrevData(ioESaver.value("Occlusion",""));
        crossRow.loadPrevData(ioESaver.value("Crossbite",""));
        attrRow.loadPrevData(ioESaver.value("Attrition",""));
        bMucoRow.loadPrevData(ioESaver.value("BuccalMucosa",""));
        mucoRow.loadPrevData(ioESaver.value("Mucosa",""));
    }

    CDTranslucentPane {
        backMaterialColor: Material.Green
        anchors.centerIn:  parent
        GridLayout {
            id: allTheThings
            columns: 2

            rowSpacing: 20

            CDDescLabel {
                text: "Tongue"
            }

            TongueRow {
                id: tongueRow
            }

            CDDescLabel {
                text: "Floor of mouth"
            }
            FloorOfMouthRow {
                id: fomRow
            }

            CDDescLabel {
                text: "Palate"
            }
            PalateRow {
                id: palRow
            }

            CDDescLabel {
                text: "Occlusion"
                font.bold: true
                verticalAlignment: Text.AlignVCenter
            }

            OcclusionRow {
                id: occRow
            }

            CDDescLabel {
                text: "Crossbite"
            }

            CrossbiteRow {
                id: crossRow
            }

            CDDescLabel {
                text: "Attrition"
            }

            AttritionRow {
                id: attrRow
            }

            CDDescLabel {
                text: "Buccal Mucosa"
            }

            BuccalMucosaRow {
                id: bMucoRow
            }

            CDDescLabel {
                text: "Other Mucosa"
            }
            MucosaRow {
                id: mucoRow
            }
        }
    }

    CDCommonFunctions {
        id: commonFuns
    }

    CDSaveAndCloseButton {
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        Material.accent: Material.Cyan
        icon.name: "document-save"
        highlighted: true
        onClicked: {
            ioESaver.setValue("Tongue",tongueRow.generateSaveString());
            ioESaver.setValue("FloorOfMouth",fomRow.generateSaveString());
            ioESaver.setValue("Palate", palRow.generateSaveString());
            ioESaver.setValue("Occlusion", occRow.generateSaveString());
            ioESaver.setValue("Crossbite", crossRow.generateSaveString());
            ioESaver.setValue("Attrition",attrRow.generateSaveString());
            ioESaver.setValue("BuccalMucosa", bMucoRow.generateSaveString());
            ioESaver.setValue("Mucosa", mucoRow.generateSaveString());
            ioESaver.sync();

            commonFuns.updateReviewFile("IoE",PATIENT_FILE_NAME);
            gitMan.commitData("Updated Intraoral Exam for " + PATIENT_FILE_NAME );
            Qt.quit();
        }

        CDFileLocations {
            id: fileLocs
        }

        Settings {
            id: ioESaver
            fileName: fileLocs.getIoEFile(PATIENT_FILE_NAME)
        }

        CDGitManager {
            id: gitMan
        }
    }

    Component.onCompleted: {
        rootWin.loadPrevData();
    }



}
