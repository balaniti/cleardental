// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.2
import dental.clear 1.0
import QtQuick.Window 2.10

Row {

    function loadPrevData(prevString) {
        if(prevString.length > 1) {
            if(prevString === classIOcc.text) {
                classIOcc.checked = true;
            }
            else if(prevString.startsWith(classIIOcc.text+":")) {
                classIIOcc.checked = true;
                classIIDiv1.checked = prevString.includes("Div 1");
                classIIDiv2.checked = prevString.includes("Div 2");
            }
            else {
                classIIIOcc.checked = true;
            }
        }
    }

    function generateSaveString() {
        var returnMe = "";
        if(classIOcc.checked) {
            returnMe = classIOcc.text
        }
        else if(classIIOcc.checked) {
            returnMe = classIIOcc.text

            if(classIIDiv1.checked) {
                returnMe += ": " + classIIDiv1.text
            }

            else if(classIIDiv2.checked) {
                returnMe += ": " + classIIDiv2.text
            }
        }
        else if(classIIIOcc.checked) {
            returnMe = classIIIOcc.text;
        }
        return returnMe;
    }

    RadioButton {
        id: classIOcc
        text: "Class I"
        checked: true
        height: classIIPane.height
        ToolTip.visible: pressed
        ToolTip.text: "MB cusp of Max 1st molar top of buccal groove of man 1st molar"
    }
    RadioButton {
        id: classIIOcc
        text: "Class II"
        height: classIIPane.height
        ToolTip.visible: pressed
        ToolTip.text: "MB cusp of Max 1st molar mesial to buccal groove of man 1st molar"
    }

    Pane {
        id: classIIPane
        background: Rectangle {
            color: Material.color(Material.BlueGrey)
            radius: 10
            opacity: .5
        }

        opacity: classIIOcc.checked ? 1: 0
        visible: opacity > 0
        Behavior on opacity {
            PropertyAnimation {
                duration: 200
            }
        }

        RowLayout {
            RadioButton {
                id: classIIDiv1
                text: "Div 1"
                checked: true
                ToolTip.visible: pressed
                ToolTip.text: "Flaired out maxillary anteriors"
            }

            RadioButton {
                id: classIIDiv2
                text: "Div 2"
                ToolTip.visible: pressed
                ToolTip.text: "Lingually inclined maxillary anteriors"
            }
        }
    }
    

    
    RadioButton {
        id: classIIIOcc
        text: "Class III"
        height: classIIPane.height
        ToolTip.visible: pressed
        ToolTip.text: "MB cusp of Max 1st molar distal to buccal groove of man 1st molar"
    }

    ButtonGroup {
        buttons: [classIOcc,classIIOcc,classIIIOcc]
    }

    ButtonGroup {
        buttons: [classIIDiv1,classIIDiv2]
    }
    
    move: Transition {
        NumberAnimation {
            properties: "x,y"
            duration: 200
        }
    }
}
