// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.2
import dental.clear 1.0
import QtQuick.Window 2.10

RowLayout {

    function loadPrevData(prevString) {
        if((prevString.length < 2) || (prevString === "WNL")) {
            return;
        }

        if(prevString.includes(tongueUlcer.text)) {
            tongueUlcer.checked = true;
        }
        else if(prevString.includes(tongueMacro.text)) {
            tongueMacro.checked = true;
        }
        else {
            tongueOther.checked = true;
            tongueOtherInput.text = prevString;
        }
    }

    function generateSaveString() {
        var returnMe = "";
        if(tongueWNL.checked) {
            returnMe = "WNL";
        }
        else if(tongueUlcer.checked) {
            returnMe = tongueUlcer.text
        }
        else if(tongueMacro.checked) {
            returnMe = tongueMacro.text
        }
        else if(tongueOther.checked) {
            returnMe = tongueOtherInput.text;
        }

        return returnMe;
    }

    RadioButton {
        id: tongueWNL
        text: "WNL"
        checked: true
    }
    RadioButton {
        id: tongueUlcer
        text: "Ulceration due to nibbling"
    }
    RadioButton {
        id: tongueMacro
        text: "Macroglossia"
    }
    
    RadioButton {
        id: tongueOther
        text: "Other"
    }
    TextField {
        id: tongueOtherInput
        opacity: tongueOther.checked ? 1 : 0
        Layout.minimumWidth: 300
        Behavior on opacity {
            PropertyAnimation {
                duration: 300
            }
        }
    }
}
