// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.2
import dental.clear 1.0
import QtQuick.Window 2.10

RowLayout {

    function loadPrevData(prevString) {
        if((prevString.length < 2) || (prevString === "Normal")) {
            return;
        }
        else {
            lesionNoted.checked = true;
            lesionNotedInput.text = prevString;
        }
    }

    function generateSaveString() {
        var returnMe = "";
        if(mucoWNL.checked) {
            returnMe = mucoWNL.text
        }
        else if(lesionNoted.checked) {
            returnMe = lesionNotedInput.text
        }

        return returnMe;
    }

    RadioButton {
        id: mucoWNL
        text: "Normal"
        checked: true
    }
    RadioButton {
        id: lesionNoted
        text: "Lesion noted"
    }
    TextField {
        id: lesionNotedInput
        opacity: lesionNoted.checked ? 1 : 0
        Layout.minimumWidth: 300
        Behavior on opacity {
            PropertyAnimation {
                duration: 300
            }
        }
    }
}
