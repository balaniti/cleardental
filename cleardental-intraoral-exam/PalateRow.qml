// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.2
import dental.clear 1.0
import QtQuick.Window 2.10

RowLayout {

    function loadPrevData(prevString) {
        if((prevString.length < 2) || (prevString === "No significant findings")) {
            return;
        }

        if(prevString.includes(palateTorus.text)) {
            palateTorus.checked = true;
        }
        else {
            palOther.checked = true;
            palOtherInput.text = prevString;
        }
    }

    function generateSaveString() {
        var returnMe = "";
        if(palateWNL.checked) {
            returnMe = palateWNL.text
        }
        else if(palateTorus.checked) {
            returnMe = palateTorus.text
        }
        else if(palOther.checked) {
            returnMe = palOtherInput.text;
        }
        return returnMe;
    }

    RadioButton {
        id: palateWNL
        text: "No significant findings"
        checked: true
    }
    RadioButton {
        id: palateTorus
        text: "Palatal torus present"
    }
    RadioButton {
        id: palOther
        text: "Other"
    }
    TextField {
        id: palOtherInput
        opacity: palOther.checked ? 1 : 0
        Layout.minimumWidth: 300
        Behavior on opacity {
            PropertyAnimation {
                duration: 300
            }
        }
    }
}
