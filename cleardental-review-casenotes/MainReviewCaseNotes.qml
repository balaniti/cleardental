// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDAppWindow {
    id: rootWin
    title: "Review Case Notes"

    property alias filterBox: filterComboBox
    property alias typedPatName: patBox.text

    property var caseNotesObj: [];

    function loadCaseNotes() {
        if (typeof PATIENT_FILE_NAME !== 'undefined') {
            var notesJSON = textMan.readFile(fileLocs.getCaseNoteFile(PATIENT_FILE_NAME));
            if(notesJSON.length < 2) {
                rootWin.caseNotesObj = [];
            }
            else {
                rootWin.caseNotesObj = JSON.parse(notesJSON);
            }
            patBox.text = PATIENT_FILE_NAME
            filterComboBox.currentIndex = 4;
            reviewPage.forceRefresh();
        }

    }


    CDPatientManager {
        id: patMan
    }

    CDFileLocations {
        id: fileLocs
    }

    CDTextFileManager {
        id: textMan
    }

    CDCommonFunctions {
        id: comFuns
    }

    CDProviderInfo {
        id: providerInfo
    }

    CDGitManager {
        id: gitMan
    }

    CDPrinter {
        id: printer
    }


    header: CDBlankToolBar {
        id: blTool
        headerText: "Case Notes"

        ComboBox {
            id: filterComboBox
            anchors.left: parent.left
            anchors.margins: 25
            anchors.verticalCenter: parent.verticalCenter
            model: ["Show All Unsigned Procedures Done Today",
                "Show All Procedures Done Today",
                "Show All My Procedures Done Today",
                "Show All My Procedures Ever Done",
                "See all Patient's Case Notes",
                "Show All Unsigned Case Notes"]
            height: 64
            width: 350
            onCurrentIndexChanged: {
                searchField.text = "";
                reviewPage.forceRefresh();
            }
        }

        CDPatientComboBox {
            id: patBox
            visible: filterComboBox.currentIndex === 4
            placeholderText: "Enter Patient Here"
            anchors.left: filterComboBox.right
            anchors.margins: 25
            anchors.verticalCenter: parent.verticalCenter
            width: 360
            //text: PATIENT_FILE_NAME ? PATIENT_FILE_NAME : 0
            onTextChanged: {
                reviewPage.forceRefresh();
            }
        }

        TextField {
            id: searchField
            anchors.right: parent.right
            anchors.rightMargin: blTool.rightSideMargin
            anchors.verticalCenter: parent.verticalCenter
            width: 360
            placeholderText: "Search Case Notes"
            onTextChanged: {
                if (text.length > 1) {
                    reviewPage.forceRefreshSearch(searchField.text);
                } else {
                    reviewPage.forceRefresh();
                }
            }
        }


    }

    ReviewAllCaseNotesPage{
        id: reviewPage
        anchors.fill: parent
    }

    RowLayout {
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        spacing: 10

        CDCancelButton {
            icon.name: "document-print"
            text: "Print Case Notes"
            onClicked: {
                printer.printCaseNotes(reviewPage.displayAlias)
            }
        }

        CDAddButton {
            text: "Add New Case Note"
            onClicked: {
                if(filterComboBox.currentIndex === 4) {
                    addDia.patID = patBox.text
                }
                addDia.open();
            }
        }
    }

    AddNewCaseNoteDialog {
        id: addDia
        onAccepted: {
            reviewPage.forceRefresh();
        }
    }

    Component.onCompleted: {
        loadCaseNotes();
    }
}
