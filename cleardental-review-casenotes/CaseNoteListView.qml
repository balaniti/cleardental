// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

ListView {
    id: caseNoteView
    property int rowWidth: (rootWin.width-100) / 5
    anchors.margins: 20
    ScrollBar.vertical: ScrollBar { }
    spacing: 5

    property var displayList: []

    header: CDTranslucentPane {
        id: headerPane
        z: 1024
        backMaterialColor: Material.LightBlue
        setRadius: 3

        RowLayout {
            Label {
                font.bold: true
                Layout.minimumWidth: caseNoteView.rowWidth
                text: "Date and Time"
            }
            Label {
                font.bold: true
                Layout.minimumWidth: caseNoteView.rowWidth
                text: "Procedure"
            }
            Label {
                font.bold: true
                Layout.minimumWidth: caseNoteView.rowWidth /2
                text: "Signed"
            }

            Label {
                font.bold: true
                Layout.minimumWidth: caseNoteView.rowWidth /2
                text: "Provider"
            }
            Label {
                font.bold: true
                Layout.minimumWidth: caseNoteView.rowWidth
                text: "Patient"
            }
            Label { //placeholder for 2 button s
                Layout.minimumWidth: caseNoteView.rowWidth
            }
        }
    }

    headerPositioning: ListView.OverlayHeader
    clip: true
    //snapMode: ListView.SnapToItem
    model: displayList.length

    delegate: Component {
        CDTranslucentPane {
            width: caseNoteView.headerItem.width
            setOpacity: .25
            setRadius: 5
            borderWidth: 1
            borderColor: "black"
            // @disable-check M126
            backMaterialColor: (displayList[index]["Final"] == true) ? Material.LightGreen : Material.Pink

            ColumnLayout {
                RowLayout {
                    Label { //datetime
                        Layout.minimumWidth: caseNoteView.rowWidth
                        Layout.maximumWidth: caseNoteView.rowWidth
                        elide: Text.ElideRight
                        text: {
                            var dateTimeObj = new Date(displayList[index]["DateTime"]);
                            return dateTimeObj.toLocaleString();
                        }
                    }
                    Label { //procedure
                        id: casenoteProcedureNameLabel
                        Layout.minimumWidth: caseNoteView.rowWidth
                        Layout.maximumWidth: caseNoteView.rowWidth
                        elide: Text.ElideRight
                        text: {
                            if (index == displayList.length-1) {
                            }
                            var procedureObj = JSON.parse(displayList[index]["ProcedureObject"]);
                            if(procedureObj[0] !== null) {
                                var text = comFuns.makeTxItemString(procedureObj[0]);
                                return text;
                            }
                        }
                    }
                    Label { //signed
                        Layout.minimumWidth: caseNoteView.rowWidth /2
                        Layout.maximumWidth: caseNoteView.rowWidth /2
                        text: displayList[index]["Final"]
                    }
                    Label { //provider
                        Layout.minimumWidth: caseNoteView.rowWidth /2
                        Layout.maximumWidth: caseNoteView.rowWidth /2
                        text: displayList[index]["Provider"]
                    }
                    RowLayout {
                        Layout.minimumWidth: caseNoteView.rowWidth
                        Layout.maximumWidth: caseNoteView.rowWidth
                        Label { //patient
                            text: {
                                displayList[index]["PatientID"];
                            }
                            Layout.fillWidth: true
                        }
                        Image {
                            Layout.maximumHeight: 64
                            Layout.maximumWidth: 64
                            source: "file://" + fileLocs.getProfileImageFile(displayList[index]["PatientID"])
                        }

                    }

                    Button {
                        Layout.minimumWidth: caseNoteView.rowWidth /2
                        icon.name: "document-edit"
                        enabled: !displayList[index]["Final"]
                        text: "Edit"

                        Material.accent: Material.Cyan
                        highlighted: true
                        icon.width: 32
                        icon.height: 32
                        font.pointSize: 18

                        EditCaseNoteDialog {
                            id: editCaseNoteDia

                            onAccepted: {
                                var caseNoteFile = fileLocs.getCaseNoteFile(patientName);
                                var caseNoteJSON = textMan.readFile(caseNoteFile);
                                var caseNoteListObj = JSON.parse(caseNoteJSON);
                                var editMe = caseNoteListObj[displayList[index]["CaseNoteIndex"]];
                                editMe["CaseNote"] = caseNote;
                                editMe["Final"] =isFinal;
                                if(isFinal) {
                                    editMe["Signature"] = providerInfo.signData(caseNote);
                                }
                                caseNoteListObj[displayList[index]["CaseNoteIndex"]] = editMe;

                                textMan.saveFile(fileLocs.getCaseNoteFile(patientName),
                                                 JSON.stringify(caseNoteListObj, null, '\t'))
                                gitMan.commitData("Edited case note for " + patientName);
                                reviewPage.forceRefresh();
                            }

                        }

                        onClicked: {
                            editCaseNoteDia.patientName = displayList[index]["PatientID"];
                            editCaseNoteDia.procedure= casenoteProcedureNameLabel.text
                            editCaseNoteDia.caseNote = displayList[index]["CaseNote"]
                            editCaseNoteDia.open();
                        }
                    }

                    CDDeleteButton {
                        Layout.minimumWidth: caseNoteView.rowWidth / 2
                        enabled: !displayList[index]["Final"]
                        text: "Delete"

                        DeleteNoteDialog {
                            id: delCaseNoteDia

                            onAccepted: {
                                var patientName = displayList[index]["PatientID"];
                                var caseNoteFile = fileLocs.getCaseNoteFile(patientName);
                                var caseNoteJSON = textMan.readFile(caseNoteFile);
                                var caseNoteListObj = JSON.parse(caseNoteJSON);
                                var editMe = caseNoteListObj[displayList[index]["CaseNoteIndex"]];
                                editMe["Hidden"] = true;
                                caseNoteListObj[displayList[index]["CaseNoteIndex"]] = editMe;
                                textMan.saveFile(fileLocs.getCaseNoteFile(patientName),
                                                 JSON.stringify(caseNoteListObj, null, '\t'));
                                gitMan.commitData("Deleted case note for " + patientName);
                                reviewPage.forceRefresh();
                            }
                        }

                        onClicked: {
                            delCaseNoteDia.open();
                        }
                    }
                }

                Label { //casenote
                    id: caseNoteBodyLabel
                    font.family: "Monda"
                    //font.pointSize: 8
                    text: displayList[index]["CaseNote"]
                }

            }
        }

    } //end of delegate

}
