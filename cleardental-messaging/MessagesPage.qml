// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1


CDTransparentPage {
    id: messagesPage


    CDPatientManager {
        id: patMan
    }

    CDFileLocations {
        id: fileLocs
    }

    CDTextFileManager {
        id: textMan
    }

    CDCommonFunctions {
        id: comFuns
    }

    CDProviderInfo {
        id: providerInfo
    }

    CDGitManager {
        id: gitMan
    }

    property string selectedPat: ""

    property var showMessageList: []

    onSelectedPatChanged: {
        refreshList();
    }

    function compareMsg(a,b) {
        var timeA = new Date (a["Time"]);
        var timeB = new Date (b["Time"]);
        if(timeA < timeB) {
            return -1;
        } else if(timeA > timeB) {
            return 1;
        } else {
            return 0;
        }
    }

    function refreshList() {
        if(selectedPat.length > 0) {
            var messagesLocation = fileLocs.getPatientMessagesFile(selectedPat);
            var messagesJSON = textMan.readFile(messagesLocation);
            if(messagesJSON.length > 1) {
                var messagesList = JSON.parse(messagesJSON);
                showMessageList = messagesList;
                msgListView.model = 0;
                msgListView.model = messagesList.length;
                msgListView.contentY = msgListView.contentHeight;
            }
            else {
                msgListView.model = 0;
            }
        }
        else { //show all the messages
            var makeModel = [];
            var patList = patMan.getAllPatientIds();
            for(var i=0;i<patList.length;i++) {
                messagesLocation = fileLocs.getPatientMessagesFile(patList[i]);
                messagesJSON = textMan.readFile(messagesLocation);
                if(messagesJSON.length > 1) {
                    makeModel = makeModel.concat(JSON.parse(messagesJSON));
                }
            }
            makeModel.sort(compareMsg);
            showMessageList = makeModel;
            msgListView.model = 0;
            msgListView.model = makeModel.length;
            msgListView.contentY = msgListView.contentHeight;
        }

    }

    ListView {
        id: msgListView
        anchors.fill: parent
        anchors.margins: 10
        clip: true

        footer: Label {
            height: 72
        }

        spacing: 10

        ScrollBar.vertical: ScrollBar { }

        delegate: RowLayout {
            anchors.left: parent.left
            anchors.right: parent.right

            CDTranslucentPane {
                Layout.maximumWidth: rootWin.width / 2
                visible: !showMessageList[index]["Sender"].startsWith("Us (")

                ColumnLayout {
                    CDDescLabel {
                        text: showMessageList[index]["Sender"] + " " +
                              new Date(showMessageList[index]["Time"]).toLocaleString();
                        Layout.maximumWidth: (rootWin.width / 2) - 20
                        elide: Text.ElideRight
                    }
                    Label {
                        text: showMessageList[index]["Contents"]
                        Layout.maximumWidth: (rootWin.width / 2) - 20
                        wrapMode: Text.WordWrap
                    }
                }
            }

            Label {
                Layout.fillWidth: true
            }

            CDTranslucentPane {
                backMaterialColor: Material.Blue
                Layout.maximumWidth: rootWin.width / 2
                visible: showMessageList[index]["Sender"].startsWith("Us (")
                ColumnLayout {
                    CDDescLabel {
                        visible: selectedPat == ""
                        text: "To: " + showMessageList[index]["Reciever"]
                    }

                    CDDescLabel {
                        text: new Date(showMessageList[index]["Time"]).toLocaleString();
                        Layout.maximumWidth: (rootWin.width / 2) - 20
                        elide: Text.ElideRight
                    }
                    Label {
                        text: showMessageList[index]["Contents"]
                        Layout.maximumWidth: (rootWin.width / 2) - 20
                        wrapMode: Text.WordWrap
                    }
                }
            }
        }
    }

    RowLayout {
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 20
        spacing: 10

        CDAddButton {
            text: "Send New Message"
            onClicked: {
                if(messagesPage.selectedPat.length > 1) {
                    newMsgDia.forceLoadPat(messagesPage.selectedPat);
                }
                newMsgDia.open();
            }

            NewMessageDialog {
                id: newMsgDia

                onAccepted: {
                    refreshList();
                }
            }
        }
    }

    Component.onCompleted: {
        refreshList();
    }
}
