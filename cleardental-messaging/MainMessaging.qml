// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDAppWindow {
    id: rootWin
    title: "Patient Messaging"

    property alias filterBox: filterComboBox
    property alias typedPatName: patBox.text



    header: CDBlankToolBar {
        headerText: "Patient Messaging"

        ComboBox {
            id: filterComboBox
            anchors.left: parent.left
            anchors.margins: 25
            anchors.verticalCenter: parent.verticalCenter
            model: ["All Messages", "Messages from Patient"]
            height: 64
            width: 280
            onCurrentIndexChanged: {
                if(currentIndex === 0) {
                    messPage.selectedPat = "";
                } else {
                    messPage.selectedPat = patBox.text
                }

            }
        }

        CDPatientComboBox {
            id: patBox
            visible: filterComboBox.currentIndex === 1
            placeholderText: "Enter Patient Here"
            anchors.left: filterComboBox.right
            anchors.margins: 25
            anchors.verticalCenter: parent.verticalCenter
            width: 280
            onTextChanged: {
                messPage.selectedPat = text
            }
        }
    }

    MessagesPage {
        id: messPage
        anchors.fill: parent
    }

    Component.onCompleted: {
        if (PATIENT_FILE_NAME.length > 1) {
            filterComboBox.currentIndex =1;
            patBox.text = PATIENT_FILE_NAME;
            messPage.selectedPat = patBox.text
        }
    }

}
