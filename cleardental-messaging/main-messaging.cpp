// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickImageProvider>
#include <QFont>
#include <QIcon>
#include <QDebug>

#include "cddefaults.h"
#include "cdpatientreminder.h"

int main(int argc, char *argv[]) {
    CDDefaults::setAppDefaults();
    QCoreApplication::setApplicationName("ClearDental-Messaging");

    QGuiApplication app(argc, argv);
    app.setWindowIcon(CDDefaults::defaultColorIcon());

    CDDefaults::registerQMLTypes();
    qmlRegisterType<CDPatientReminder>("dental.clear", 1, 0,"CDPatientReminder");

    QQmlApplicationEngine engine;
    if (argc > 1) {
        engine.rootContext()->setContextProperty("PATIENT_FILE_NAME",QString(argv[1]));
    } else {
        engine.rootContext()->setContextProperty("PATIENT_FILE_NAME", "");
    }
    const QUrl url(QStringLiteral("qrc:/MainMessaging.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);


    CDDefaults::enableBlurBackground();


    return app.exec();
}
