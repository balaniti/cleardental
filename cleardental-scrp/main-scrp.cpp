// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickImageProvider>
#include <QFont>
#include <QIcon>
#include <QDebug>
#include <QHostInfo>

#include "cdtextfilemanager.h"
#include "cdgitmanager.h"
#include "cdimagemanager.h"
#include "cdtoollauncher.h"
#include "cdpatientfilemanager.h"
#include "cddefaults.h"
#include "cdpatientmanager.h"
#include "cddruglist.h"
#include "cdfilelocations.h"
#include "cdfilewatcher.h"
#include "cdradiographmanager.h"

int main(int argc, char *argv[])
{
    CDDefaults::setAppDefaults();
    QCoreApplication::setApplicationName("ClearDental-SCRP");
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);
    app.setWindowIcon(CDDefaults::defaultColorIcon());

    if(QCoreApplication::arguments().count()<3) {
        qDebug()<<"You need to give the patient's name and the quads as an argument!";
        return -32;
    }

    CDDefaults::registerQMLTypes();

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("PATIENT_FILE_NAME",QString(argv[1]));
    engine.rootContext()->setContextProperty("PROCEDURE_JSON",QString(argv[2]));
    const QUrl url(QStringLiteral("qrc:/MainSCRP.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    CDDefaults::enableBlurBackground();


    return app.exec();
}
