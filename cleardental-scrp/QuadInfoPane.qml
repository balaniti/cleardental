// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTranslucentPane {
    id: quadInfoPane
    backMaterialColor: Material.Purple

    function getStartToothNumber() {
        var quadName =  procedureObj["Location"];
        if(quadName === "UR") {
            return 1;
        }
        else if(quadName === "UL") {
            return 9;
        }
        else if(quadName === "LL") {
            return 17;
        }
        else if(quadName === "LR") {
            return 25;
        }
    }

    function isMissing(toothNumb) {
        if((toothNumb < 1) || (toothNumb > 32)) {
            return true;
        }
        var returnMe = false;
        var info = missingTeethSettings.value(toothNumb,"");
        var parts = info.split("|");
        for(var i=0;i<parts.length;i++) {
            if(parts[i].trim().startsWith("Missing")) {
                return true;
            }
        }

        return returnMe;
    }

    function combineVals(a,b) {
        if(a === "_") {
            a = 0;
        } else {
            a = parseInt(a);
        }
        if(b==="_") {
            b=0;
        } else {
            b = parseInt(b);
        }

        return a+b;
    }

    function getColor(toothNumb) {
        var biggestNumb = 1;

        perioChartSettings.category = toothNumb;
        var buccalPockets = perioChartSettings.value("BuccalPockets","_,_,_");
        if(buccalPockets !== "WNL") {
            var buccalPocketArray = buccalPockets.split(",");
            var buccalRec = perioChartSettings.value("BuccalRecession","_,_,_");
            var buccalRecArray = buccalRec.split(",");
            for(var b=0;b<buccalRecArray.length;b++) {
                var sum = combineVals(buccalPocketArray[b],buccalRecArray[b]);
                biggestNumb = Math.max(biggestNumb,sum);
            }
        }

        var lingualPockets = perioChartSettings.value("LingualPockets","_,_,_");
        if(lingualPockets !== "WNL") {
            var lingualPocketArray = lingualPockets.split(",");
            var lingualRec = perioChartSettings.value("LingualRecession","_,_,_");
            var lingualRecArray = lingualRec.split(",");
            for(var l=0;l<lingualPocketArray.length;l++) {
                sum = combineVals(lingualPocketArray[l],lingualRecArray[l]);
                biggestNumb = Math.max(biggestNumb,sum);
            }
        }

        if(biggestNumb <= 3) {
            return Material.Green;
        }
        else if(biggestNumb <= 6) {
            return Material.Orange;
        }
        else {
            return Material.Red;
        }
    }

    ColumnLayout {
        Layout.fillWidth: true
        Label {
            text: "Periodontal Charting"
            font.pointSize: 24
            font.underline: true
        }
        RowLayout {
            spacing: 20
            Button {
                text: "New Periodontal Chart"
                onClicked: {
                    launcher.launchTool(ToolLauncher.PerioCharting,PATIENT_FILE_NAME);
                }
            }
        }

        RowLayout {
            Layout.fillWidth: true
            Repeater {
                model: 8
                Button {
                    text: index +getStartToothNumber()
                    enabled: !isMissing(index +getStartToothNumber())
                    Material.accent: getColor(index +getStartToothNumber())
                    highlighted: true
                    Layout.minimumWidth: 128
                    Layout.minimumHeight: 128
                    font.pointSize: 24
                    CDPerioLabel {
                        id: buccLeft
                        anchors.left: parent.left
                        anchors.top: parent.top
                        font.pointSize: 18
                        style: Text.Outline
                        styleColor: Material.foreground
                        anchors.margins: 10
                    }

                    CDPerioLabel {
                        id: buccCenter
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.top: parent.top
                        font.pointSize: 18
                        style: Text.Outline
                        styleColor: Material.foreground
                        anchors.margins: 10
                    }

                    CDPerioLabel {
                        id: buccRight
                        anchors.right: parent.right
                        anchors.top: parent.top
                        font.pointSize: 18
                        style: Text.Outline
                        styleColor: Material.foreground
                        anchors.margins: 10
                    }

                    CDPerioLabel {
                        id: lingualLeft
                        anchors.left: parent.left
                        anchors.bottom: parent.bottom
                        font.pointSize: 18
                        style: Text.Outline
                        styleColor: Material.foreground
                        anchors.margins: 10
                    }

                    CDPerioLabel {
                        id: lingualCenter
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.bottom: parent.bottom
                        font.pointSize: 18
                        style: Text.Outline
                        styleColor: Material.foreground
                        anchors.margins: 10
                    }

                    CDPerioLabel {
                        id: lingualRight
                        anchors.right: parent.right
                        anchors.bottom: parent.bottom
                        font.pointSize: 18
                        style: Text.Outline
                        styleColor: Material.foreground
                        anchors.margins: 10
                    }

                    Component.onCompleted: {
                        perioChartSettings.category = index +getStartToothNumber()
                        var buccalPockets =perioChartSettings.value("BuccalPockets","_,_,_");
                        if(buccalPockets !== "WNL") {
                            buccalPockets = buccalPockets.split(",");
                            var buccalRecession=perioChartSettings.value("BuccalRecession","_,_,_").split(",");
                            buccLeft.combineVals(buccalPockets[0],buccalRecession[0]);
                            buccCenter.combineVals(buccalPockets[1],buccalRecession[1]);
                            buccRight.combineVals(buccalPockets[2],buccalRecession[2]);
                        }
                        else {
                            buccLeft.perioVal = "😊";
                            buccCenter.perioVal = "😊";
                            buccRight.perioVal = "😊";
                        }

                        var lingualPockets =perioChartSettings.value("LingualPockets","_,_,_");
                        if(lingualPockets !== "WNL") {
                            lingualPockets = lingualPockets.split(",");
                            var lingualRecession=perioChartSettings.value("LingualRecession","_,_,_").split(",");
                            lingualLeft.combineVals(lingualPockets[0],lingualRecession[0]);
                            lingualCenter.combineVals(lingualPockets[1],lingualRecession[1]);
                            lingualRight.combineVals(lingualPockets[2],lingualRecession[2]);
                        }
                        else {
                            lingualLeft.perioVal = "😊";
                            lingualCenter.perioVal = "😊";
                            lingualRight.perioVal = "😊";
                        }
                    }
                }
            }
        }
    }
}
