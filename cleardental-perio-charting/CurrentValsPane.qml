// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.2
import dental.clear 1.0
import QtQuick.Window 2.10

CDTranslucentPane {
    id: currentValsPane
    backMaterialColor: Material.Brown

    property var pdArray: [pdA,pdB,pdC]
    property var recArray: [reA,reB,reC]

    function generatePocketString() {
        if(pdA.text === "😊") {
            return "WNL";
        }

        return pdA.text + "," + pdB.text + "," + pdC.text;
    }

    function parsePocketString(newVal) {
        if(newVal === "WNL") {
            fillPD("😊")
        }

        else {
            var pockets = newVal.split(",");
            for(var i=0;i<pdArray.length;i++) {
                if(pockets[i] === "_") {
                    pdArray[i].unSetVal();
                } else {
                    pdArray[i].perioVal = pockets[i];
                }
            }
        }
    }

    function generateRecString() {
        return reA.text + "," + reB.text + "," + reC.text;
    }

    function parseRecString(newVal) {
        var recess = newVal.split(",");
        for(var i=0;i<recArray.length;i++) {
            if(recess[i] === "_") {
                recArray[i].unSetVal();
            } else {
                recArray[i].perioVal = recess[i];
            }
        }
    }

    function addPD(newVal) {
        for(var i=0;i<pdArray.length;i++) {
            if(!pdArray[i].isSet()) {
                pdArray[i].perioVal = newVal;
                i = pdArray.length;
            }
        }
    }

    function fillPD(newVal) {
        pdA.perioVal = newVal;
        pdB.perioVal = newVal;
        pdC.perioVal = newVal;
    }

    function fillRec(newVal) {
        reA.perioVal = newVal;
        reB.perioVal = newVal;
        reC.perioVal = newVal;
    }

    function rmPD() {
        for(var i=pdArray.length-1;i>-1;i--) {
            if(pdArray[i].isSet()) {
                pdArray[i].unSetVal();
                i = -1
            }
        }
    }

    function addRec(newVal) {
        for(var i=0;i<recArray.length;i++) {
            if(!recArray[i].isSet()) {
                recArray[i].perioVal = newVal;
                i = recArray.length;
            }
        }
    }

    function rmRec() {
        for(var i=recArray.length-1;i>-1;i--) {
            if(recArray[i].isSet()) {
                recArray[i].unSetVal();
                i = -1
            }
        }
    }
    
    ColumnLayout {
        Label {
            text: "Current Values"
            font.pointSize: 24
            font.underline: true
        }
        RowLayout {
            Label {
                text: "Current Tooth: "
                font.bold: true
                font.pointSize: 24
            }
            Label {
                text: toothRunList[currentIndex].tooth
                font.pointSize: 24
            }
            Label {
                Layout.minimumWidth: 200
            }

            Label {
                text: "Current Side: "
                font.bold: true
                font.pointSize: 24
            }
            Label {
                text: toothRunList[currentIndex].side
                font.pointSize: 24
            }
        }

        RowLayout {
            spacing: 100
            Label {
                id: leftSide
                text: toothRunList[currentIndex].goDistToMes?"Distal":"Mesial"
                font.pointSize: 24
            }

            GridLayout {
                columns: 4
                Label {
                    text: "Pocket Depths"
                    font.pointSize: 24
                }
                CDPerioLabel {
                    id: pdA
                    font.pointSize: 32
                }
                CDPerioLabel {
                    id: pdB
                    font.pointSize: 32
                }
                CDPerioLabel {
                    id: pdC
                    font.pointSize: 32
                }
                Label {
                    text: "Recession"
                    Layout.alignment: Qt.AlignRight
                    font.pointSize: 24
                }
                CDPerioLabel {
                    id: reA
                    font.pointSize: 32
                }
                CDPerioLabel {
                    id: reB
                    font.pointSize: 32
                }
                CDPerioLabel {
                    id: reC
                    font.pointSize: 32
                }
            }

            Label {
                id: rightSide
                text: toothRunList[currentIndex].goDistToMes?"Mesial":"Distal"
                font.pointSize: 24
            }
        }
    }
    
}
