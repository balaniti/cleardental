// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.2
import dental.clear 1.0
import QtQuick.Window 2.10

CDTranslucentPane {
    id: mobilityPane
    backMaterialColor: Material.Orange

    function generateMobilityString() {
        return gradesGroup.checkedButton.text;
    }

    function parseMobilityString(newVal) {
        for(var i=0;i<gradesGroup.buttons.length;i++) {
            if(newVal === gradesGroup.buttons[i].text) {
                gradesGroup.buttons[i].checked = true;
            }
        }
    }

    ButtonGroup {
        id: gradesGroup
        buttons: gradesRow.children
    }

    ColumnLayout {
        Label {
            text: "Mobility"
            font.pointSize: 24
            font.underline: true
        }
        
        RowLayout {
            id: gradesRow
            RadioButton {
                text: "None"
                checked: true
            }
            
            RadioButton {
                text: "Grade 1"
            }
            RadioButton {
                text: "Grade 2"
            }
            RadioButton {
                text: "Grade 3"
            }
        }
    }
}
