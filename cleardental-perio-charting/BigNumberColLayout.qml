// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.2
import dental.clear 1.0
import QtQuick.Window 2.10

ColumnLayout {
    id: bigNumberColLayout
    RowLayout {
        Layout.alignment: Qt.AlignHCenter
        Label {
            id: currToothLabel
            Layout.alignment: Qt.AlignHCenter
            text: "Current tooth: " + currentTooth
            font.pointSize: 32
        }
        Label {
            Layout.minimumWidth: 200
        }
        
        Label {
            id: blLabel
            text: "Side: " + currentSide
            font.pointSize: 32
            Layout.alignment: Qt.AlignHCenter
        }
    }
    
    
    
    RowLayout {
        id: currentSel
        Layout.alignment: Qt.AlignHCenter
        
        property var curArrayLR: [curA,curB,curC]
        property var curArrayRL: [curC,curB,curA]
        
        property var currArray: curArrayLR
        
        function handleNextTooth() {
            
            //first handle making it the next tooth
            if((currentTooth < 17) && (currentSide == "Buccal")) { //max buccal
                currentTooth++;
            }
            else if((currentTooth < 17) && (currentTooth > 1) && (currentSide == "Lingual")) { //max lingual
                currentTooth--;
            }
            else if((currentTooth > 16) && (currentSide == "Buccal")) { //mandibular buccal
                currentTooth--;
            }
            else {
                currentTooth++
            }
            
            //Now handle updating the other parts of the UI based on the increment
            if((currentTooth == 9) && (currentSide == "buccal")) {
                //speech.sayThis("You are now on the mesial facial of tooth number 9");
                currArray = curArrayRL;
                leftSide.text = "Distal"
                rightSide.text = "Mesial"
            }
            else if((currentTooth == 16) && (currentSide == "buccal")) {
                currentTooth = 15;
                //speech.sayThis("You are now on the distal lingual of tooth number 15");
                currentSide = "Lingual"
                currArray = curArrayRL;
                leftSide.text = "Distal"
                rightSide.text = "Mesial"
            }
            else if((currentTooth == 1) && (currentSide == "lingual")) {
                currentTooth = 31;
                currentSide = "Buccal";
                currArray= curArrayLR;
                leftSide.text = "Distal"
                rightSide.text = "Mesial"
            }
            else if((currentTooth == 17) && (currentSide == "Buccal")) {
                currentTooth = 18;
                currentSide = "Lingual"
            }
            else if((currentTooth == 31) && (currentSide == "Lingual")) {
                doneButton.visible = true;
                doneButton.opacity = 1;
                
            }
            
        }
        
        function handleInput(inputMM, inputColor) {
            var fillArray = currArray;
            var continueLoop = true;
            for(var i=0;(i<fillArray.length) && continueLoop;i++) {
                if(fillArray[i].text === "_") {
                    fillArray[i].text = inputMM;
                    fillArray[i].color = inputColor;
                    continueLoop = false;
                }
            }
            
            if(fillArray[2].text !== '_') { //go to the next tooth or side
                fadeToNext.start();
                handleNextTooth();
            }
            //speech.sayThis(inputMM);
        }
        
        function handleInputNoSay(inputMM) {
            //speech.sayThis(inputMM);
            
            var fillArray = currArray;
            var continueLoop = true;
            for(var i=0;(i<fillArray.length) && continueLoop;i++) {
                if(fillArray[i].text === "_") {
                    fillArray[i].text = inputMM;
                    fillArray[i].color = "black";
                    continueLoop = false;
                }
            }
            
            if(fillArray[2].text !== '_') { //go to the next tooth or side
                fadeToNext.start();
                handleNextTooth();
            }
        }
        
        
        
        function handleDelete() {
            var fillArray = currArray;
            for(var i=0;i<fillArray.length;i++) {
                if(fillArray[i].text !== "_") {
                    fillArray[i].text = "_";
                    fillArray[i].color = currToothLabel.color
                    return;
                }
            }
        }
        
        Label {
            id: leftSide
            text: "Mesial"
            font.pointSize: 32
        }
        
        Label {
            id: curA
            text: "_"
            font.pointSize: 64
        }
        Label {
            id: curB
            text: "_"
            font.pointSize: 64
        }
        Label {
            id: curC
            text: "_"
            font.pointSize: 64
        }
        Label {
            id: rightSide
            text: "Distal"
            font.pointSize: 32
        }
        
        SequentialAnimation {
            id: fadeToNext
            PropertyAnimation {
                targets: [curA,curB,curC]
                property: "opacity"
                from: 1
                to: 0
                duration: 1000
            }
            ScriptAction {
                script: {
                    curA.text = "_";
                    curB.text = "_";
                    curC.text = "_";
                    curA.color = currToothLabel.color
                    curB.color = currToothLabel.color
                    curC.color = currToothLabel.color
                }
            }
            PropertyAnimation {
                targets: [curA,curB,curC]
                property: "opacity"
                from: 0
                to: 1
                duration: 300
            }
        }
    }
    
    GridLayout {
        columns: 4
        Layout.alignment: Qt.AlignHCenter
        Layout.fillHeight: true
        Layout.fillWidth: true
        Repeater {
            model: 11
            RoundButton {
                text: index
                font.pointSize: 32
                Layout.minimumWidth: rootWin.height / 6
                Layout.minimumHeight: rootWin.height / 6
                highlighted: true
                Material.accent: {
                    if(index< 2) {
                        return Material.LightGreen
                    } else if(index < 4) {
                        return Material.Green
                    } else if(index < 6) {
                        return Material.Orange
                    } else if(index < 8) {
                        return Material.Red
                    } else {
                        return Material.Pink
                    }
                }
                
                onClicked: {
                    currentSel.handleInput(index,Material.accent)
                }
            }
        }
        
        RoundButton {
            icon.name: "edit-clear"
            icon.width: 50
            icon.height: 50
            Layout.minimumWidth: rootWin.height / 6
            Layout.minimumHeight: rootWin.height / 6
            Material.accent:Material.Grey
            //highlighted: true
            onClicked: currentSel.handleDelete();
        }
    }
}
