// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.2
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Window 2.10

Drawer {
    id: drawer
    height: rootWin.height
    width: rootWin.width /3

    background: Rectangle {
        gradient: Gradient {
            GradientStop { position: 0.0; color: "white" }
            GradientStop { position: 0.5; color: "white" }
            GradientStop { position: 1.0; color: "transparent" }
        }
        anchors.fill: parent
    }
    
    ColumnLayout {
        anchors.horizontalCenter: parent.horizontalCenter
        
        Button {
            text: "Clear all Previous Values"
            onClicked: {
                for(var i=1;i<33;i++) {
                    perioChartSettings.category = i;
                    perioChartSettings.setValue("Bleeding","");
                    perioChartSettings.setValue("BuccalPockets","_,_,_");
                    perioChartSettings.setValue("BuccalRecession","_,_,_");
                    perioChartSettings.setValue("LingualPockets","_,_,_");
                    perioChartSettings.setValue("LingualRecession","_,_,_");
                    perioChartSettings.setValue("Mobility","");

                    if(isMolar(i)) {
                        perioChartSettings.setValue("Furcation", "");
                    }
                }
                currentIndex =0;
                loadToothData();
                drawer.close();
            }
        }
        
        CheckBox {
            text: "Run Voice Recognition"
            onCheckStateChanged: {
                if(checked) {
                    listener.listenForPerio();
                }
                else {
                    listener.stopListeningForPerio();
                }
            }
            
        }
    }
}
