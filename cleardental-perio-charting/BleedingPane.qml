// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.2
import dental.clear 1.0
import QtQuick.Window 2.10

CDTranslucentPane {
    id: bleedingPane
    backMaterialColor: Material.Red

    function generateString() {
        var returnMe = "";
        returnMe += bleeda.checked ? "B" : ""
        returnMe += ","
        returnMe += bleedb.checked ? "B" : ""
        returnMe += ","
        returnMe += bleedc.checked ? "B" : ""
        return returnMe;
    }

    function parseString(newVal) {
        var bleeds = newVal.split(",");
        bleeda.checked = bleeds[0] === "B";
        bleedb.checked = bleeds[1] === "B";
        bleedc.checked = bleeds[2] === "B";
    }
    
    ColumnLayout {
        Label {
            text: "Bleeding"
            font.pointSize: 24
            font.underline: true
        }
        
        RowLayout {
            Button {
                id: bleeda
                text: toothRunList[currentIndex].goDistToMes?"Distal":"Mesial"
                checkable: true
            }

            Button {
                id: bleedb
                text: "Central"
                checkable: true
            }
            Button {
                id: bleedc
                text: toothRunList[currentIndex].goDistToMes?"Mesial":"Distal"
                checkable: true
            }

        }
    }
}
