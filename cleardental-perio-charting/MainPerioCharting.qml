// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.2
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Window 2.10
import QtMultimedia 5.12

CDAppWindow {
    id: rootWin
    title: qsTr("Periodontal Exam [ID: " + PATIENT_FILE_NAME + "]")

    property int currentIndex: 0
    property var toothRunList: []

    function generateRunList() {
        //right buccal maxillary
        for(var a=1;a<=8;a++) {
            if(!isMissing(a)) {
                var toothSide = {
                    tooth: a,
                    side: "Buccal",
                    goDistToMes: true};
                toothRunList.push(toothSide);
            }

        }

        //left buccal maxillary
        for(var b=9;b<=16;b++) {
            if(!isMissing(b)) {
                var toothSideb = {
                    tooth: b,
                    side: "Buccal",
                    goDistToMes: false};
                toothRunList.push(toothSideb);
            }

        }

        //left lingual maxillary
        for(var c=16;c>=9;c--) {
            if(!isMissing(c)) {
                var toothSidec = {
                    tooth: c,
                    side: "Lingual",
                    goDistToMes: true};
                toothRunList.push(toothSidec);
            }

        }

        //right lingual maxillary
        for(var d=8;d>=1;d--) {
            if(!isMissing(d)) {
                var toothSided = {
                    tooth: d,
                    side: "Lingual",
                    goDistToMes: false};
                toothRunList.push(toothSided);
            }

        }

        //right buccal mandibular
        for(var e=32;e>=25;e--) {
            if(!isMissing(e)) {
                var toothSidee = {
                    tooth: e,
                    side: "Buccal",
                    goDistToMes: true};
                toothRunList.push(toothSidee);
            }

        }

        //left buccal mandibular
        for(var f=24;f>=17;f--) {
            if(!isMissing(f)) {
                var toothSidef = {
                    tooth: f,
                    side: "Buccal",
                    goDistToMes: false};
                toothRunList.push(toothSidef);
            }

        }

        //left lingual mandibular
        for(var g=17;g<=24;g++) {
            if(!isMissing(g)) {
                var toothSideg = {
                    tooth: g,
                    side: "Lingual",
                    goDistToMes: true};
                toothRunList.push(toothSideg);
            }

        }

        //right lingual mandibular
        for(var h=25;h<=32;h++) {
            if(!isMissing(h)) {
                var toothSideh = {
                    tooth: h,
                    side: "Lingual",
                    goDistToMes: false};
                toothRunList.push(toothSideh);
            }

        }
    }

    function isMissing(toothNumb) {
        if((toothNumb < 1) || (toothNumb > 32)) {
            return true;
        }
        var returnMe = false;
        var info = missingTeethSettings.value(toothNumb,"");
        var parts = info.split("|");
        for(var i=0;i<parts.length;i++) {
            if(parts[i].trim().startsWith("Missing")) {
                return true;
            }
            else if(parts[i].trim().startsWith("Impacted")) {
                //as far as periodontal charting is concerned, impacted is the same as missing
                return true;
            }
        }

        return returnMe;
    }

    function isMolar(toothNumb) {
        var returnMe = true;

        if((toothNumb > 3) && (toothNumb < 14)) { //max
            returnMe = false;
        } else if((toothNumb > 19)
                  && (toothNumb < 30)) { //man
            returnMe = false;
        }
        return returnMe;
    }

    function saveInput() {
        perioChartSettings.category = toothRunList[currentIndex].tooth;
        var currentSide = toothRunList[currentIndex].side;
        var bleedings = bleedingPane.generateString();
        perioChartSettings.setValue("Bleeding",bleedings);
        var pockets = currentValsPane.generatePocketString();
        perioChartSettings.setValue(currentSide + "Pockets", pockets);
        var recession = currentValsPane.generateRecString();
        perioChartSettings.setValue(currentSide + "Recession", recession);
        var mobility = mobilityPane.generateMobilityString();
        perioChartSettings.setValue("Mobility", mobility);
        if(isMolar(toothRunList[currentIndex].tooth)) {
            var furcation = furcationPane.generateFurcationString();
            perioChartSettings.setValue("Furcation", furcation);
        }
    }

    function loadToothData() {
        perioChartSettings.category = toothRunList[currentIndex].tooth;
        var currentSide = toothRunList[currentIndex].side;
        var bleedings = perioChartSettings.value("Bleeding"," , , ");
        bleedingPane.parseString(bleedings);
        var pockets = perioChartSettings.value(
                    currentSide + "Pockets", "_,_,_");
        currentValsPane.parsePocketString(pockets);
        var recessions = perioChartSettings.value(
                    currentSide + "Recession", "_,_,_");
        currentValsPane.parseRecString(recessions);
        var mobility = perioChartSettings.value("Mobility" , "None");
        mobilityPane.parseMobilityString(mobility);
        if(isMolar(toothRunList[currentIndex].tooth)) {
            var furcation = perioChartSettings.value("Furcation","None");
            furcationPane.parseFurcationString(furcation);
        }
    }

    Component.onCompleted: {
        generateRunList();
        currentIndex = 1;
        currentIndex = 0;
        loadToothData();
    }

    CDFileLocations {
        id: fileLocs
    }

    Settings {
        id: missingTeethSettings
        fileName: fileLocs.getHardTissueChartFile(PATIENT_FILE_NAME)
    }

    Settings {
        id: perioChartSettings
        fileName: fileLocs.getPerioChartFile(PATIENT_FILE_NAME)
    }

    CDListenEngine {
        id: listener

        Component.onDestruction: {
            listener.stopListeningForPerio();
        }

        onNewPerioTrio: {
            currentValsPane.addPD(a);
            currentValsPane.addPD(b);
            currentValsPane.addPD(c);
            trioTimer.start();
        }

    }

    Timer {
        id: trioTimer
        interval: 1000
        onTriggered: {
            audioComp.play();
            saveInput();
            currentIndex++;
            loadToothData();
        }

    }

    Audio {
        id: audioComp
        source: "file:///usr/share/sounds/Oxygen-Sys-App-Positive.ogg"
    }

    header: CDPatientToolBar {
        headerText: "Periodontal Exam:"
        ToolButton {
            icon.name: "application-menu"
            icon.width: 64
            icon.height: 64
            onClicked: drawer.open();
        }
    }

    PerioDrawer {
        id: drawer
    }

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 10
        CurrentValsPane {
            id: currentValsPane
            Layout.alignment: Qt.AlignHCenter
        }
        PerioDepthsPane {
            id: perioDepthsPane
            Layout.alignment: Qt.AlignHCenter
        }
        PerioRecessionPane {
            id: perioRecessionPane
            Layout.alignment: Qt.AlignHCenter
        }

        RowLayout {
            Layout.alignment: Qt.AlignHCenter
            BleedingPane {
                id: bleedingPane
            }
            MobilityPane {
                id: mobilityPane
            }
            FurcationPane {
                id: furcationPane
                visible: isMolar(toothRunList[currentIndex].tooth);
            }
        }
    }

    CDButton {
        id: everythingIsAwesome //Everything is cool when you're part of a team
        text: "All Teeth WNL"
        anchors.right: parent.right
        anchors.top:  parent.top
        anchors.margins: 10
        Material.background: Material.LightGreen
        onClicked: {
            allTeethWNLDia.open();
        }
    }

    CDButton {
        id: toothGoodButton
        text: "Tooth WNL"
        anchors.right: everythingIsAwesome.left
        anchors.top:  parent.top
        anchors.margins: 10
        Material.background: Material.Lime
        onClicked: {
            currentValsPane.fillPD("😊")
            currentValsPane.fillRec(0);
        }
    }

    CDButton {
        id: wnlRangeButton
        text: "WNL Range"
        anchors.right: toothGoodButton.left
        anchors.top:  parent.top
        anchors.margins: 10
        Material.background: Material.Green
        onClicked: {
            wnlDia.open();
        }
    }

    CDButton {
        text: "Jump to Tooth"
        anchors.left: parent.left
        anchors.top:  parent.top
        anchors.margins: 10
        onClicked: {
            jumpToDia.open();
        }

    }

    WNLDialog {
        id: wnlDia
    }

    AllTeethWNLDialog {
        id: allTeethWNLDia
    }

    JumpToToothDialog {
        id: jumpToDia

        onAccepted: {
            var jumpToIndex =0;
            for(var i=0;i<toothRunList.length;i++) {
                var rev = toothRunList[i];
                if( (rev["tooth"] == selectedTooth) && //@disable-check M126
                        (rev["side"] == selectedSide)) { //@disable-check M126
                    jumpToIndex = i;
                }
            }
            //If the end user somehow selected a missing tooth, then it will just jump back to index 0
            currentIndex = jumpToIndex;

        }

    }

    Button {
        text: "Previous Tooth"
        icon.name: "go-previous"
        icon.width: 32
        icon.height: 32
        enabled: currentIndex > 0
        highlighted: true
        font.pointSize: 18
        onClicked: {
            saveInput();
            currentIndex--;
            loadToothData();
        }

        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: 10
    }

    Button {
        text: "Next Tooth"
        icon.name: "go-next"
        icon.width: 32
        icon.height: 32
        visible: currentIndex < (toothRunList.length-1)
        highlighted: true
        font.pointSize: 18
        onClicked: {
            saveInput();
            currentIndex++;
            loadToothData();
        }

        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
    }

    CDSaveAndCloseButton {
        id: doneButton
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        visible: opacity > 0
        opacity: (currentIndex == (toothRunList.length-1)) ? 1 : 0
        Behavior on opacity {
            PropertyAnimation {
                duration: 300
            }
        }

        CDGitManager {
            id: gitMan
        }

        CDCommonFunctions {
            id: comFun
        }

        CDPatientFileManager {
            id: patFileMan
        }

        onClicked: {
            comFun.updateReviewFile("PerioCharting",PATIENT_FILE_NAME);
            patFileMan.saveTodaysPerioChartAsHistory(PATIENT_FILE_NAME);
            gitMan.commitData("Updated Periodontal chart for " + PATIENT_FILE_NAME);
            Qt.quit();
        }
    }

}
