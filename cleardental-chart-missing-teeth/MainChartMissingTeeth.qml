// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.15
import dental.clear 1.0
import Qt.labs.settings 1.1

import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Extras 2.15
import QtQuick.Scene3D 2.14



CDAppWindow {
    id: rootWin
    title: "Chart Missing Teeth: [ID: " + PATIENT_FILE_NAME + "]"

    CDGitManager {
        id: gitMan
    }

    CDFileLocations {
        id: fileLocs
    }

    Settings {
        id: missingTeethSettings
        fileName: fileLocs.getHardTissueChartFile(PATIENT_FILE_NAME)
    }

    CDCommonFunctions {
        id: comFuns
    }

    function showAsMissing(toothNumb) {
        var missingArray = ["Missing"];
        if(toothNumb < 17) {
            maxButs.itemAt(toothNumb-1).children[0].existingList = missingArray;
            maxButs.itemAt(toothNumb-1).children[0].reloadInformation();
        }
        else {
            manButs.itemAt(32-toothNumb).children[0].existingList = missingArray;
            manButs.itemAt(32-toothNumb).children[0].reloadInformation();
        }
    }

    function showAsImpacted(toothNumb) {
        if(toothNumb < 17) {
            maxButs.itemAt(toothNumb-1).children[0].makeGreen = true;
        }
        else {
            manButs.itemAt(32-toothNumb).children[0].makeGreen = true;
        }
    }

    function setMissing(toothNumb) {
        var fullStr = missingTeethSettings.value(toothNumb,"");
        fullStr= comFuns.addHardTissueAttr(fullStr,"Missing")
        missingTeethSettings.setValue(toothNumb,fullStr);
    }

    function setNotMissing(toothNumb) {
        var fullStr = missingTeethSettings.value(toothNumb,"");
        fullStr= comFuns.remHardTissueAttr(fullStr,"Missing")
        missingTeethSettings.setValue(toothNumb,fullStr);
    }

    function setImpacted(toothNumb) {
        var fullStr = missingTeethSettings.value(toothNumb,"");
        fullStr= comFuns.addHardTissueAttr(fullStr,"Impacted")
        missingTeethSettings.setValue(toothNumb,fullStr);
    }

    function setNotImpacted(toothNumb) {
        var fullStr = missingTeethSettings.value(toothNumb,"");
        fullStr= comFuns.remHardTissueAttr(fullStr,"Impacted")
        missingTeethSettings.setValue(toothNumb,fullStr);
    }


    header: CDPatientToolBar {
        headerText: "Chart Missing Teeth:"
    }

    ColumnLayout {
        anchors.centerIn: parent
        CDTranslucentPane {
            backMaterialColor: Material.Green
            font.pointSize: 24
            GridLayout {
                columns: 2

                CDHeaderLabel {
                    text: "Which Teeth are missing?"
                    Layout.columnSpan: 2
                    Layout.alignment: Qt.AlignHCenter
                }
                Button {
                    text: "All 3rds"
                    Layout.columnSpan: 2
                    Layout.alignment: Qt.AlignHCenter

                    onClicked:  {
                        var missingArray = ["Missing"];
                        maxButs.itemAt(0).children[0].existingList = missingArray;
                        maxButs.itemAt(0).children[0].reloadInformation();

                        maxButs.itemAt(15).children[0].existingList = missingArray;
                        maxButs.itemAt(15).children[0].reloadInformation();

                        manButs.itemAt(0).children[0].existingList = missingArray;
                        manButs.itemAt(0).children[0].reloadInformation();


                        manButs.itemAt(15).children[0].existingList = missingArray;
                        manButs.itemAt(15).children[0].reloadInformation();
                    }
                }

                Button {
                    text: "All Maxillary"
                    onClicked:  {
                        var missingArray = ["Missing"];
                        for(var i=0;i<16;i++) {
                            maxButs.itemAt(i).children[0].existingList = missingArray;
                            maxButs.itemAt(i).children[0].reloadInformation();
                        }
                    }
                }

                RowLayout {
                    Repeater {
                        id: maxButs
                        model: 16
                        Button {
                            text: index+1
                            Layout.minimumWidth: 75
                            Layout.minimumHeight: 150


                            CDToothScene3D {
                                id: toothScene3D
                                anchors.fill: parent
                                anchors.topMargin: 10
                                toothNumb: parent.text
                            }
                            MouseArea {
                                anchors.fill: parent
                                onClicked: {
                                    var missingArray = ["Missing"];
                                    if(toothScene3D.existingList.length > 0) {
                                        toothScene3D.existingList = [];
                                    }
                                    else {
                                        toothScene3D.existingList = missingArray;
                                    }
                                    toothScene3D.reloadInformation();
                                }
                            }

                            Button {
                                highlighted: true
                                font.pointSize: 12
                                width: 24
                                height: 32
                                x: 10
                                y: 10
                                text: "I"
                                onClicked:  {
                                    var impactedArray = ["Impacted"];
                                    if(toothScene3D.existingList.length > 0) {
                                        toothScene3D.existingList = [];
                                    }
                                    else {
                                        toothScene3D.existingList = impactedArray;
                                    }
                                    toothScene3D.reloadInformation();
                                }
                            }
                        }
                    }
                }
                Button {
                    text: "All Mandibular"
                    onClicked:  {
                        var missingArray = ["Missing"];
                        for(var i=0;i<16;i++) {
                            manButs.itemAt(i).children[0].existingList = missingArray;
                            manButs.itemAt(i).children[0].reloadInformation();
                        }
                    }
                }

                RowLayout {
                    Repeater {
                        id: manButs
                        model: 16
                        Button {
                            text: 32-index
                            Layout.minimumWidth: 75
                            Layout.minimumHeight: 150

                            CDToothScene3D {
                                id: toothScene3DBottom
                                anchors.fill: parent
                                anchors.bottomMargin: 10
                                toothNumb: parent.text
                            }

                            MouseArea {
                                anchors.fill: parent
                                onClicked: {
                                    var missingArray = ["Missing"];
                                    if(toothScene3DBottom.existingList.length > 0) {
                                        toothScene3DBottom.existingList = [];
                                    }
                                    else {
                                        toothScene3DBottom.existingList = missingArray;
                                    }
                                    toothScene3DBottom.reloadInformation();
                                }
                            }
                            Button {
                                highlighted: true
                                font.pointSize: 12
                                width: 24
                                height: 32
                                x: 10
                                y: 10
                                text: "I"
                                onClicked:  {
                                    var impactedArray = ["Impacted"];
                                    if(toothScene3DBottom.existingList.length > 0) {
                                        toothScene3DBottom.existingList = [];
                                    }
                                    else {
                                        toothScene3DBottom.existingList = impactedArray;
                                    }
                                    toothScene3DBottom.reloadInformation();
                                }
                            }
                        }
                    }
                }
            }
        }

        CDReviewRadiographPane {
            Layout.fillWidth: true
        }
    }

    CDSaveAndCloseButton {
        id: saveButton

        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10

        onClicked: {
            for(var i=0;i<16;i++) {
                if(maxButs.itemAt(i).children[0].existingList[0] === "Missing") {
                    setMissing(i+1);
                }
                else {
                    setNotMissing(i+1);
                }
                if(maxButs.itemAt(i).children[0].existingList[0] === "Impacted") {
                    setImpacted(i+1);
                }
                else {
                    setNotImpacted(i+1);
                }
            }
            for(var m=0;m<16;m++) {
                if(manButs.itemAt(m).children[0].existingList[0] === "Missing") {
                    setMissing(32-m);
                }
                else {
                    setNotMissing(32-m);
                }
                if(manButs.itemAt(m).children[0].existingList[0] === "Impacted") {
                    setImpacted(32-m);
                }
                else {
                    setNotImpacted(32-m);
                }
            }

            missingTeethSettings.sync();
            gitMan.commitData("Updated missing teeth for " + PATIENT_FILE_NAME);

            Qt.quit();
        }
    }

    Component.onCompleted: {
        for(var i=1;i<=32;i++) {
            var currentVal = missingTeethSettings.value(i,"");
            var attrs = currentVal.split("|");
            for(var attrI=0;attrI<attrs.length;attrI++) {
                if(attrs[attrI].includes("Missing")) {
                    showAsMissing(i);
                }
                if(attrs[attrI].includes("Impacted")) {
                    showAsImpacted(i);
                }
            }
        }
    }
}

