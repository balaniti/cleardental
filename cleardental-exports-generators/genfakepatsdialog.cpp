// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "genfakepatsdialog.h"
#include "ui_genfakepatsdialog.h"

#include "cdpatientfilemanager.h"
#include "cdaquireradiograph.h"
#include "cdfilelocations.h"
#include "cdpatientmanager.h"
#include "cdschedulemanager.h"
#include "cddoctorlistmanager.h"

#include <QDebug>
#include <QRandomGenerator>
#include <QFile>
#include <QDir>
#include <QPainter>
#include <QSettings>
#include <QProgressDialog>

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValue>
#include <QJsonObject>


GenFakePatsDialog::GenFakePatsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::GenFakePatsDialog)
{
    ui->setupUi(this);
    QDate today = QDate::currentDate();
    ui->startDayBox->setDate(today);
    ui->endDayBox->setDate(today.addYears(10));
    connect(ui->generateButton,SIGNAL(clicked()),this,SLOT(handleGenPatients()));

    QSettings fileDirsSettings("Clear.Dental","TestFileDirs");
    ui->photoDirLineMen->setText(fileDirsSettings.value("photoDirLineMen","").toString());
    ui->photoDirLineBoys->setText(fileDirsSettings.value("photoDirLineBoys","").toString());
    ui->photoDirLineWomen->setText(fileDirsSettings.value("photoDirLineWomen","").toString());
    ui->photoDirLineGirls->setText(fileDirsSettings.value("photoDirLineGirls","").toString());
}

GenFakePatsDialog::~GenFakePatsDialog()
{
    delete ui;
}

void GenFakePatsDialog::handleGenPatients() {
    QSettings fileDirsSettings("Clear.Dental","TestFileDirs");
    fileDirsSettings.setValue("photoDirLineMen",ui->photoDirLineMen->text());
    fileDirsSettings.setValue("photoDirLineBoys",ui->photoDirLineBoys->text());
    fileDirsSettings.setValue("photoDirLineWomen",ui->photoDirLineWomen->text());
    fileDirsSettings.setValue("photoDirLineGirls",ui->photoDirLineGirls->text());
    fileDirsSettings.sync();



    //int hoursPerDay = ui->openTimeBox->time().secsTo(ui->closedTimeBox->time());
    int numbDays = ui->startDayBox->date().daysTo(ui->endDayBox->date());
    //ui->progressBar->setMaximum(ui->numbPatBox->value() + (ui->apptColumsnBox->value() * hoursPerDay * numbDays));

    QDate startDOB = QDate::currentDate().addYears(-99);
    QDate endDOB = QDate::currentDate().addMonths(-6);
    int daysBetween = startDOB.daysTo(endDOB);
    QRandomGenerator rGen = QRandomGenerator::securelySeeded();

    QStringList maleFirstNames;
    QFile readmaleFile(":/nameFiles/dist.male.first.txt");
    readmaleFile.open(QIODevice::ReadOnly|QIODevice::Text);
    while(!readmaleFile.atEnd()) {
        QString line =readmaleFile.readLine();
        int stopAt = line.indexOf(" ");
        QString maleName = line.left(stopAt).toLower();
        maleName[0] = maleName[0].toUpper();
        maleFirstNames.append(maleName);
    }
    readmaleFile.close();

    QStringList femaleFirstNames;
    QFile readFemaleFile(":/nameFiles/dist.female.first.txt");
    readFemaleFile.open(QIODevice::ReadOnly|QIODevice::Text);
    while(!readFemaleFile.atEnd()) {
        QString line =readFemaleFile.readLine();
        int stopAt = line.indexOf(" ");
        QString femaleName = line.left(stopAt).toLower();
        femaleName[0] = femaleName[0].toUpper();
        femaleFirstNames.append(femaleName);
    }
    readFemaleFile.close();

    QStringList lastNames;
    QFile readLastNames(":/nameFiles/lastNames.txt");
    readLastNames.open(QIODevice::ReadOnly|QIODevice::Text);
    while(!readLastNames.atEnd()) {
        QString lastName = readLastNames.readLine().toLower().replace("\n","");
        lastName[0] = lastName[0].toUpper();
        lastNames.append(lastName);
    }
    readFemaleFile.close();

    QStringList adultMaleDir = getPhotosFromDir(ui->photoDirLineMen->text());
    QStringList childMaleDir = getPhotosFromDir(ui->photoDirLineBoys->text());
    QStringList adultFemaleDir = getPhotosFromDir(ui->photoDirLineWomen->text());
    QStringList childFemaleDir = getPhotosFromDir(ui->photoDirLineGirls->text());

    QFile dCodesFile(":/DCodes.json");
    dCodesFile.open(QIODevice::ReadOnly | QIODevice::Text);
    QString dCodesBA = dCodesFile.readAll();
    dCodesFile.close();
    QJsonDocument dCodesDoc = QJsonDocument::fromJson(dCodesBA.toUtf8());
    QJsonArray procedureListArray = dCodesDoc.array();
    CDAquireRadiograph aquRad;

    int radiographTypeDiff = CDAquireRadiograph::FMX_SINGLE_IMAGE - CDAquireRadiograph::INVALID_RADIOGRAPH_TYPE;

    QProgressDialog patProgress("Generating Patients", "Abort", 0, ui->numbPatBox->value(), this);
    patProgress.setWindowModality(Qt::WindowModal);

    for(int i=0;i<ui->numbPatBox->value();i++) {
        //First generate the patients
        bool isMale = rGen.bounded(0,2) > 0;
        QString firstName;
        if(isMale) {
            firstName = maleFirstNames.at(rGen.bounded(maleFirstNames.length()));
        }
        else {
            firstName = femaleFirstNames.at(rGen.bounded(maleFirstNames.length()));
        }
        QString lastName = lastNames.at(rGen.bounded(lastNames.length()));

        QDate DOB = startDOB.addDays(rGen.bounded(daysBetween));


        QVariantMap jsonData; //used to mimic the import via web portal
        jsonData["lastName"] = lastName;
        jsonData["firstName"] = firstName;
        jsonData["dob"] = DOB.toString("yyyy-MM-dd");
        jsonData["submitTime"] = QString::number(QDateTime::currentDateTime().toSecsSinceEpoch());
        jsonData["HomeCity"] = lastNames.at(rGen.bounded(lastNames.length()));
        jsonData["HomeState"] = "MA";
        jsonData["addr1"] = QString::number(rGen.bounded(1000)) + "Main St.";
        jsonData["HomeZip"] = QString::number(rGen.bounded(99999));
        if(isMale) {
            jsonData["sex"] = "Male";
            if(rGen.bounded(1024) > 1000) {
                jsonData["gender"] = "Transgender Female";
            }
        }
        else {
            jsonData["sex"] = "Female";
            if(rGen.bounded(1024) > 1000) {
                jsonData["gender"] = "Transgender Male";
            }
        }
        jsonData["CellPhone"] = QString::number(rGen.bounded(1000)) + "-" + QString::number(rGen.bounded(1000)) +
                "-" + QString::number(rGen.bounded(10000));
        jsonData["consentValues"] = "123,456,789,123";

        QString patID = CDPatientFileManager::importPatient(jsonData);

        QString photoURL;

        if(DOB.daysTo(QDate::currentDate()) > 7665) { //>21 years ; adult
            if(isMale) {
                photoURL = adultMaleDir.at(rGen.bounded(adultMaleDir.length()));
            } else {
                photoURL = adultFemaleDir.at(rGen.bounded(adultFemaleDir.length()));

            }
        } else { //child
            if(isMale) {
                photoURL = childMaleDir.at(rGen.bounded(childMaleDir.length()));
            } else {
                photoURL = childFemaleDir.at(rGen.bounded(childFemaleDir.length()));
            }
        }

        CDPatientFileManager::importNewProfileImage(patID,photoURL,0,0,512);

        //Add the treatment to the patients
        int numbTreatments = rGen.bounded(25);
        QJsonDocument txPlanDoc;
        QJsonObject allTxPlanObject;
        QJsonObject primaryTxPlanObject;
        QJsonArray unPhasedTx;
        for(int tx=0;tx<numbTreatments;tx++) {
            QJsonObject procedureObject = procedureListArray.at(rGen.bounded(procedureListArray.size())).toObject();
            unPhasedTx.append(procedureObject);

            //Add to case note
            //Add to ledger


        }
        primaryTxPlanObject.insert("Unphased",unPhasedTx);
        allTxPlanObject.insert("Primary",primaryTxPlanObject);
        txPlanDoc.setObject(allTxPlanObject);
        QFile writeTx(CDFileLocations::getTreatmentPlanFile(patID));
        writeTx.open(QIODevice::WriteOnly | QIODevice::Text);
        writeTx.write(txPlanDoc.toJson(QJsonDocument::Indented));
        writeTx.close();



        //Add the fake radiographs
        int numbRadiographs = rGen.bounded(24);
        for(int ri=0; ri<numbRadiographs;ri++) {
            QImage fakeImg = generateFakeRadiograph(1024,1024);
            fakeImg.save("/var/tmp/radiographImage.png");
            aquRad.saveExposure(patID, (CDAquireRadiograph::RadiographType) (1 + CDAquireRadiograph::INVALID_RADIOGRAPH_TYPE +
                                                                             rGen.bounded(radiographTypeDiff)));
        }


        //Add the fake things to the ledger

        //Add in the fake things to the treatment plan
        patProgress.setLabelText("Made Patient: " + patID);
        patProgress.setValue(i);
    }
    patProgress.close();

    //Now set the open / closed times / columns
    QDir apptDir(CDFileLocations::getLocalAppointmentsDirectory());
    apptDir.mkpath(("."));

    QSettings pracInfoSettings(CDFileLocations::getLocalPracticePreferenceFile(),QSettings::IniFormat);
    pracInfoSettings.beginGroup("Hours");
    pracInfoSettings.setValue("OpenSunday",false);
    pracInfoSettings.setValue("OpenMonday",true);
    pracInfoSettings.setValue("MondayHours","Open " + ui->openTimeBox->time().toString("h:mm AP") +
                              " | Closed " + ui->closedTimeBox->time().toString("h:mm AP"));
    pracInfoSettings.setValue("OpenTuesday",true);
    pracInfoSettings.setValue("OpenTuesday","Open " + ui->openTimeBox->time().toString("h:mm AP") +
                              " | Closed " + ui->closedTimeBox->time().toString("h:mm AP"));
    pracInfoSettings.setValue("OpenWednesday",true);
    pracInfoSettings.setValue("WednesdayHours","Open " + ui->openTimeBox->time().toString("h:mm AP") +
                              " | Closed " + ui->closedTimeBox->time().toString("h:mm AP"));
    pracInfoSettings.setValue("OpenThursday",true);
    pracInfoSettings.setValue("ThursdayHours","Open " + ui->openTimeBox->time().toString("h:mm AP") +
                              " | Closed " + ui->closedTimeBox->time().toString("h:mm AP"));
    pracInfoSettings.setValue("OpenFriday",true);
    pracInfoSettings.setValue("FridayHours","Open " + ui->openTimeBox->time().toString("h:mm AP") +
                              " | Closed " + ui->closedTimeBox->time().toString("h:mm AP"));
    pracInfoSettings.setValue("OpenSaturday",false);
    pracInfoSettings.sync();

    char startChar = 'A';
    QStringList chairTypes = {"Hygiene","OP + OMFS","Operative"};
    QJsonDocument chairDoc;
    QJsonArray chairList;
    for(int chairI=0;chairI< ui->apptColumsnBox->value();chairI++) {
        QJsonObject chair;
        chair.insert("ChairName",QString(startChar++));
        chair.insert("ChairType",chairTypes.at(rGen.bounded(chairTypes.length())));
        chairList.append(chair);
    }

    QJsonObject waitChair;
    waitChair.insert("ChairName","Waitlist");
    waitChair.insert("ChairType","Operative");
    chairList.append(waitChair);

    chairDoc.setArray(chairList);
    QFile writeChair(CDFileLocations::getLocalDefaultChairFile());
    writeChair.open(QIODevice::WriteOnly | QIODevice::Text);
    writeChair.write(chairDoc.toJson(QJsonDocument::Indented));
    writeChair.close();


    //Add some random providers
    QStringList providerTypes ={"General Dentist","Hygienist","Assistant"};
    QStringList colorStrings = {"Red","Pink","Purple",
                                "DeepPurple","Indigo","Blue","LightBlue","Cyan","Teal","Green",
                                "LightGreen","Lime","Yellow","Amber","Orange","DeepOrange",
                                "Brown","Grey","BlueGrey"};
    int numbProviders = rGen.bounded(colorStrings.length());
    QStringList providerUnixIDs;
    for(int providerI=0;providerI<numbProviders;providerI++) {
        bool isMale = rGen.bounded(0,2) > 0;
        QString firstName;
        if(isMale) {
            firstName = maleFirstNames.at(rGen.bounded(maleFirstNames.length()));
        }
        else {
            firstName = femaleFirstNames.at(rGen.bounded(maleFirstNames.length()));
        }
        QString lastName =  lastNames.at(rGen.bounded(lastNames.length()));
        QString unixID = firstName.toLower().at(0) + lastName.toLower();
        providerUnixIDs.append(unixID);
        QVariantMap proData;
        proData["FirstName"] = firstName;
        proData["LastName"] = lastName;
        proData["ProviderColor"] =  colorStrings.at(rGen.bounded(colorStrings.length()));;
        proData["ProviderType"] =  providerTypes.at(rGen.bounded(providerTypes.length()));;
        proData["UnixID"] = unixID;
        CDDoctorListManager::addProvider(proData);
    }

    //Now add the patients to the schedule
    QStringList patIDList = CDPatientManager::getAllPatientIds();
    QTime startTime = ui->openTimeBox->time();
    QTime endTime = ui->closedTimeBox->time();
    int hoursBetween = endTime.hour() - startTime.hour();
    QProgressDialog schProgress("Generating schedule", "Abort", 0, numbDays, this);
    char aChar = 'A';
    for(int dayI=0;dayI<numbDays;dayI++) {
        QDate injectDate = ui->startDayBox->date().addDays(dayI);
        if(injectDate.dayOfWeek() < 6) { //Mon - Fri
            for(int chairI=0;chairI< ui->apptColumsnBox->value();chairI++) {
                for(int hourI=0;hourI<hoursBetween;hourI++) {
                    QTime injectTime = startTime.addSecs(60*60*hourI);

                    int aCharI = (int)aChar;
                    aCharI+=chairI;
                    char postAChair = (char)aCharI;
                    QString chairString(postAChair);

                    QString patID = patIDList.at(rGen.bounded(patIDList.length()));
                    QFile readTxPlan(CDFileLocations::getTreatmentPlanFile(patID));
                    readTxPlan.open(QIODevice::ReadOnly | QIODevice::Text);
                    QString patTxStr = readTxPlan.readAll();
                    readTxPlan.close();
                    QJsonDocument txPlanDoc = QJsonDocument::fromJson(patTxStr.toUtf8());
                    QJsonArray txList = txPlanDoc.object().value("Primary").toObject().value("Unphased").toArray();
                    while(txList.count() > 2) {
                        txList.pop_back();
                    }

                    QJsonDocument txListDoc;
                    txListDoc.setArray(txList);
                    CDScheduleManager::addAppointment(injectDate,injectTime.toString("h:mm AP"),chairString,patID,
                                                      txListDoc.toJson(QJsonDocument::Compact),
                                                      providerUnixIDs.at(rGen.bounded(providerUnixIDs.length())),
                                                      60,"Random Comment");
                }
            }
        }
        schProgress.setLabelText("Done with day: " + injectDate.toString("MMMM d yyyy"));
        qDebug()<<"Done with day: " << injectDate.toString("MMMM d yyyy");
        schProgress.setValue(dayI);
    }

}

QStringList GenFakePatsDialog::getPhotosFromDir(QString dir)
{
    QStringList returnMe;
    dir = dir.replace("file://","");
    QDir photoDir(dir);
    foreach(QString filename, photoDir.entryList(QDir::AllEntries | QDir::NoDotAndDotDot)) {
        returnMe.append(dir + "/" + filename);
    }

    return returnMe;
}

QImage GenFakePatsDialog::generateFakeRadiograph(int width, int height)
{
    QImage returnMe(width,height,QImage::Format_Grayscale8);
    QPainter painter(&returnMe);
    //QRandomGenerator rGen = QRandomGenerator::securelySeeded();
    for(int x=0;x<width;x++) {
        for(int y=0;y<height;y++) {
            //int val = rGen.bounded(255);
            int val =  QRandomGenerator::global()->bounded(255);
            QColor color(val,val,val);
            returnMe.setPixelColor(x,y,color);
        }
    }
    return returnMe;
}
