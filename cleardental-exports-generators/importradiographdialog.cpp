// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "importradiographdialog.h"
#include "ui_importradiographdialog.h"

#include "cdpatientmanager.h"
#include "cdfilelocations.h"
#include "cdradiographmanager.h"
#include "cdaquireradiograph.h"
#include "cdgitmanager.h"

#include <QDebug>
#include <QFile>
#include <QDir>


ImportRadiographDialog::ImportRadiographDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ImportRadiographDialog)
{
    ui->setupUi(this);

    QStringList patList = CDPatientManager::getAllPatientIds();
    ui->patBox->addItems(patList);
    ui->patBox->setCurrentText("");

    for(int i=CDAquireRadiograph::BW_Right_Distal;i<=CDAquireRadiograph::FMX_SINGLE_IMAGE;i++) {
        ui->radiographTypeBox->addItem(CDAquireRadiograph::getStringFromType((CDAquireRadiograph::RadiographType) i));
    }

    ui->dateBox->setDate(QDate::currentDate());

    connect(ui->patBox,SIGNAL(currentIndexChanged(int)),this,SLOT(handlePatSelected()));
    connect(ui->importButton,SIGNAL(clicked()),this,SLOT(doImport()));

}

ImportRadiographDialog::~ImportRadiographDialog()
{
    delete ui;
}

QString ImportRadiographDialog::getPatID()
{
    return ui->patBox->currentText();
}

QString ImportRadiographDialog::getSource()
{
    return ui->sourceLine->text();
}


QString ImportRadiographDialog::getRadiographType()
{
    return ui->radiographTypeBox->currentText();
}

QDate ImportRadiographDialog::getDate()
{
    return ui->dateBox->date();
}

void ImportRadiographDialog::handlePatSelected()
{
    ui->importButton->setEnabled(ui->patBox->currentIndex()>0);
}

void ImportRadiographDialog::doImport()
{
    QString destURLString = CDFileLocations::getRadiographDir(ui->patBox->currentText());
    destURLString += ui->dateBox->date().toString("MMM/d/yyyy");

    QDir destDir(destURLString);
    if(!destDir.exists()) {
        destDir.mkpath(".");
    }

    QString fileLocation = destURLString+ "/" + ui->radiographTypeBox->currentText() + ".png";
    QFile dest(fileLocation);
    int fileCounter = 1;
    while(dest.exists()) {
        fileLocation = destURLString+ "/" + ui->radiographTypeBox->currentText() + "-" + QString::number(fileCounter)
                + ".png";
        dest.setFileName(fileLocation);
        fileCounter++;
    }

    QString scrStr = ui->sourceLine->text().remove("file://");
    QImage source(scrStr);
    source.save(fileLocation);

    QString statusMsg = "Imported radiographs from "  + scrStr + " for " + ui->patBox->currentText();
    CDGitManager::commitData(statusMsg);
    ui->statusLabel->setText(statusMsg);
}
