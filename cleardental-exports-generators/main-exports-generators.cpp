// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "mainwindow.h"

#include <QApplication>

#include "cddefaults.h"

int main(int argc, char *argv[])
{
    CDDefaults::setAppDefaults();
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}
