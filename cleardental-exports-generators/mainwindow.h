// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void handleGeneratePhoneCSV();
    void handleRenameSeats();
    void handleSwitchToNewHardTissue();
    void handleImportRadiograph();
    void handleGenFakePats();
    void handleGenerateDentalPlanCSV();
    void handleConvertBilling();

private:
    void replaceChairNames(QString oldName, QString newName);
    QString getSurfaces(QString input);
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
