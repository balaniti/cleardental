QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    genfakepatsdialog.cpp \
    importradiographdialog.cpp \
    main-exports-generators.cpp \
    mainwindow.cpp \
    renameroomdialog.cpp

HEADERS += \
    genfakepatsdialog.h \
    importradiographdialog.h \
    mainwindow.h \
    renameroomdialog.h

FORMS += \
    genfakepatsdialog.ui \
    importradiographdialog.ui \
    mainwindow.ui \
    renameroomdialog.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /usr/bin/
desktop.path = /usr/share/applications
!isEmpty(target.path): INSTALLS += target desktop

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../cleardental-backend/release/ -lcleardental-backend
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../cleardental-backend/debug/ -lcleardental-backend
else:unix: LIBS += -L$$OUT_PWD/../cleardental-backend/ -lcleardental-backend

INCLUDEPATH += $$PWD/../cleardental-backend
DEPENDPATH += $$PWD/../cleardental-backend

RESOURCES += \
    names.qrc

