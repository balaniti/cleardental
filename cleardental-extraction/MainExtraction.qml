// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtGraphicalEffects 1.12
import QtQuick.Window 2.10

CDAppWindow {
    id: rootWin
    title:  comFuns.makeTxItemString(procedureObj) + " [ID: " + PATIENT_FILE_NAME + "]"

    property var procedureObj: JSON.parse(PROCEDURE_JSON)

    CDCommonFunctions {
        id: comFuns
    }

    header: CDPatientToolBar {
        headerText: comFuns.makeTxItemString(procedureObj)
        ToolButton {
             icon.name: "application-menu"
             icon.width: 64
             icon.height: 64
             onClicked: drawer.open();
             anchors.left: parent.left
         }
    }

    RowLayout {
        anchors.fill: parent
        anchors.margins: 10
        ColumnLayout {
            CDConsentFormPane {
                id: consentFormPane;
                Layout.fillWidth: true;
                consentType: "ext"
                consentSpec: procedureObj["Tooth"]
            }
            Flickable {
                Layout.fillWidth: true
                Layout.minimumHeight: 200
                Layout.maximumHeight: 300
                Layout.preferredHeight: medHisPane.implicitHeight
                Layout.minimumWidth: 360
                contentWidth: medHisPane.implicitWidth
                contentHeight: medHisPane.implicitHeight
                flickableDirection: Flickable.VerticalFlick
                clip: true
                interactive: height != contentHeight
                ScrollBar.vertical: ScrollBar { }

                CDMedReviewPane {
                    id: medHisPane
                    width: consentFormPane.width
                }
            }

            CDReviewRadiographPane{
                CDRadiographManager {
                    id: radioMan
                }

                Layout.fillWidth: true
                toothToReview: procedureObj["Tooth"]
            }
        }
        ColumnLayout {
            CDParentConsentPane {
                id: patWhoWith
                Layout.fillWidth: true
            }
            CDLAPane {
                id: laPane
                showMarcaine: true
                Layout.fillWidth: true
            }
            ApptInfoPane{id: appInfoPane;Layout.fillWidth: true}
        }
    }

    CDFinishProcedureButton {
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        highlighted: true
        onClicked: {
            var caseNoteString = "Patient presented for extraction of ";
            caseNoteString += "tooth #" + procedureObj["Tooth"] + ".\n";
            caseNoteString += consentFormPane.generateCaseNoteString();
            caseNoteString += patWhoWith.generateCaseNoteString();
            caseNoteString += medHisPane.generateCaseNoteString();
            caseNoteString += laPane.generateCaseNoteString();
            caseNoteString += appInfoPane.generateCaseNoteString();
            finishDialog.caseNoteString = caseNoteString;
            finishDialog.txItemsToComplete = [procedureObj]
            finishDialog.open();
        }
    }


    CDHygieneRoomStatus {
        id: drawer
        height: rootWin.height
        width: 0.33 * rootWin.width
    }

    CDFinishProcedureDialog {
        id: finishDialog
        willNeedRx: true
    }

}
