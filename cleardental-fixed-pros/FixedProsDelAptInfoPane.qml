// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0

CDTranslucentPane {
    backMaterialColor: Material.Green

    function generateCaseNoteString() {
        var returnMe = "Patient presented for delivery of " + rootWin.procedure_OBJ.Material + " " +
                rootWin.procedure_OBJ.ProcedureName + " on tooth #" + rootWin.procedure_OBJ.Tooth + ".\n";

        var checkArray = [bwBox,adjBox,patHappy]

        for(var i=0;i<checkArray.length;i++) {
            var box = checkArray[i];
            if(box.checked) {
                returnMe += box.text + ".\n";
            }
        }

        returnMe += "Cemented using " + cementBox.currentText + ".\n";

        return returnMe;
    }

    ColumnLayout {
        id: prepColumn
        CDHeaderLabel {
            text: "Today's Appointment (Delivery)"
        }

        CheckBox {
            id: bwBox
            text: "Bitewing Radiograph taken"
        }

        CheckBox {
            id: adjBox
            text: "Adjustment required"
        }

        CheckBox {
            id: patHappy
            text: "Patient Happy with Esthetics"
        }

        RowLayout {
            CDDescLabel {
                text: "Cemented Using"
            }

            ComboBox {
                id: cementBox
                editable: true
                model: ["RelyX™ Luting", "RelyX™ Unicem","GC Fuji PLUS®","GC FujiCEM® 2"]
                Layout.minimumWidth: 300
            }
        }
    }
}
