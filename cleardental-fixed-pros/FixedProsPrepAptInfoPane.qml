// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0

CDTranslucentPane {
    backMaterialColor: Material.Green

    property alias doingCoreBuildup: coreCheck.checked

    function generateCaseNoteString() {
        var returnMe = "Patient presented for crown prep for " + rootWin.procedure_OBJ.Material + " " +
                rootWin.procedure_OBJ.ProcedureName + " on tooth #" + rootWin.procedure_OBJ.Tooth + ".\n";

        if(coreCheck.checked) {
            returnMe += "Core buildup was done.\n"


            //First check if the D2950 is already in
            var procedureObject = {
                ProcedureName: "Core build-up",
                DCode: "D2950",
                Tooth: rootWin.procedure_OBJ["Tooth"]
            }

            comFuns.ensureSingleTxItem(PATIENT_FILE_NAME,procedureObject)
        }

        returnMe += "Retracted using " + retractBox.currentText + ".\n";
        returnMe += "Impressed with  " + impressBox.currentText + ".\n";

        if(tempCrownFabCheckbox.checked) {
            returnMe += "Temporary crown was fabricated.\n";
            returnMe += "Temporary crown was cemented using "+ tempCrownBondBox.currentText +".\n";
        }

        returnMe += "Impression to be sent to  " + labBox.currentText + ".\n";

        if(finalShade.text.length > 0) {
            returnMe += "Shade: " + finalShade.text + ".\n";
        }


        return returnMe;
    }

    ColumnLayout {
        id: prepColumn
        CDHeaderLabel {
            text: "Today's Appointment (Prep Stage)"
        }

        CheckBox {
            id: coreCheck
            text: "Core buildup"
        }

        RowLayout {
            Label {
                text: "Retraction"
            }
            ComboBox {
                id: retractBox
                editable: true
                model: ["Traxodent","Single Cord","Double Cord"]
            }
        }
        RowLayout {
            Label {
                text: "Impressed with"
            }
            ComboBox {
                id: impressBox
                editable: true
                model: ["3Shape Trios Scanner",
                    "Vinyl Polysiloxane (VPS)",
                "Polyether (Impregum)",
                "Alginate",
                "Medit Scanner"]
                Layout.fillWidth: true
            }
        }

        RowLayout {

            CheckBox {
                id: tempCrownFabCheckbox
                text: "Temp crown fabricated"
            }
            Label {
                opacity: tempCrownFabCheckbox.checked ? 1 :0
                text: "Temp crown cemented using"
                Behavior on opacity {
                    PropertyAnimation {
                        duration: 300
                    }
                }
            }
            ComboBox {
                id: tempCrownBondBox
                opacity: tempCrownFabCheckbox.checked ? 1 :0
                model: ["TempBond"]
                Layout.fillWidth: true
                editable: true
                Behavior on opacity {
                    PropertyAnimation {
                        duration: 300
                    }
                }
            }
        }

        RowLayout {
            CDDescLabel {
                text: "Shade"
            }
            TextField {
                id: finalShade
            }
        }

        RowLayout {
            Label {
                text: "Lab to send crown"
            }
            ComboBox {
                id: labBox
                textRole: "LabName"

                CDFileLocations {
                    id: fLocs
                }

                CDTextFileManager {
                    id: textFileMan
                }

                Component.onCompleted:  {
                    var labJSON = textFileMan.readFile(fLocs.getPracticeLabsFile("local"));
                    var labsObj = ({});
                    if(labJSON.length > 0) {
                        labsObj = JSON.parse(labJSON);
                    }
                    model = labsObj
                }
            }
        }
    }
}
