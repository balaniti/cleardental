// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTransparentPage {

    CDTranslucentPane {
        id: centerPane
        anchors.centerIn: parent
        ColumnLayout {
            spacing: 20

            Button {
                text: "Import patient from online"
                font.pointSize: 24
                onClicked: {
                    mainView.push("ImportOnlinePage.qml")
                }
            }

            Button {
                text: "Direct input of patient information"
                font.pointSize: 24
                onClicked: {
                    mainView.push("ImportDirectPage.qml")
                }
            }
        }
    }
}
