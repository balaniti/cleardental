// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

// updated 6/17/2022 Alex Vernes

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1

/**
 * The method used is a little slow sometimes,
 * but it is capable for fetching a filtered list of patients
 */

CDTransparentPage {

    property int colWidth: (rootWin.width / 7) - 20

    property string searchedName;

    /**
     * Takes an object string (as of now) and
     * checks if the given string is part of the object string
     */
    function isSearched(objectName, string) {
        var patFileName = objectName.split(".")[0]
        return (patFileName.toLowerCase()).includes((string.toLowerCase()));
    }

    /**
     * Refreshes the list of patients that match the searched string
     */
    function updatePatList() {
        var filteredModel = ({});

        var fullPatListKeys = Object.keys(rootWin.patList);
        var fullPatListValues = Object.values(rootWin.patList);

        if (searchBox.text.length >= 1) {
            for (var i=0; i < Object.keys(rootWin.patList).length; i++) {
                if (isSearched(fullPatListKeys[i], searchBox.text)) {
                    var key = fullPatListKeys[i];
                    var value = fullPatListValues[i];
                    filteredModel[key] = value;
                }
            }

            patientList.model =  Object.keys(filteredModel)

        } else {
            patientList.model = Object.keys(rootWin.patList)
        }
    }

    CDTranslucentPane {
        backMaterialColor: Material.Lime
        anchors.centerIn: parent
        width: rootWin.width - 50
        height: rootWin.height - 100
        ColumnLayout {
            id: colWin

            anchors.fill: parent

            // search bar
            RowLayout {
                id: searchBar
                Label {
                    text: "Search"
                    font.bold: true
                }
                TextField {
                    id: searchBox
                    Layout.fillWidth: true
                    onTextChanged: {
                        updatePatList()
                    }
                }
            }

            // patient list
            ListView {
                id: patientList
                Layout.fillHeight: true
                Layout.fillWidth: true
                headerPositioning:ListView.OverlayHeader
            clip: true
            anchors.margins: 20

            ScrollBar.vertical: ScrollBar{}

            header: RowLayout {
                CDDescLabel {
                    text: "Filename"
                    Layout.preferredWidth: colWidth
                }

                CDDescLabel {
                    text: "Submit Date/Time"
                    Layout.preferredWidth: colWidth
                }

                CDDescLabel {
                    text: "In database?"
                    Layout.preferredWidth: colWidth
                }

                CDDescLabel {
                    text: "Has an appointment?"
                    Layout.preferredWidth: colWidth
                }

                CDDescLabel {
                    Layout.preferredWidth: colWidth *2
                }

                CDDescLabel {
                    text: "Comments"
                    Layout.preferredWidth: colWidth
                }

            }

            model: Object.keys(rootWin.patList)

            delegate: Component {

                RowLayout {
                    id: listRowLayout

                    CDPatientFileManager {
                        id: patFileMan
                    }

                    CDScheduleManager {
                        id: schMan
                    }

                    Label {
                        Layout.preferredWidth: colWidth
//                        text: Object.keys(rootWin.patList)[index]
                        text: patientList.model[index]
                    }

                    Label {
                        function timeConverter(UNIX_timestamp){
                            var a = new Date(UNIX_timestamp * 1000);
                            var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
                            var year = a.getFullYear();
                            var month = months[a.getMonth()];
                            var date = a.getDate();
                            var hour = a.getHours();
                            var min = a.getMinutes();
                            var sec = a.getSeconds();
                            if(min < 10) {
                                min = "0" + min;
                            }

                            var time = month + "/" + date + "/" + year + ' ' + hour + ':' + min;
                            return time;
                        }
                        Layout.preferredWidth: colWidth
                        text: timeConverter(rootWin.patList[patientList.model[index]])
                    }

                    Label {
                        id: patExistLabel
                        Layout.preferredWidth: colWidth
                        text: patFileMan.patientExists(patientList.model[index].split(".")[0]) ?
                                  "Yes" : "No"
                    }

                    Label {
                        Layout.preferredWidth: colWidth
                        text: schMan.getAppointments(patientList.model[index].split(".")[0]).length > 0 ?
                                  "Yes" : "No"
                        color: (text === "No") ? Material.color(Material.Red) : Material.foreground
                        font.bold: text === "No"
                    }

                    CDAddButton {
                        text: patExistLabel.text === "Yes" ? "Re-import patient" : "Import Patient"
                        Layout.preferredWidth: colWidth
                        onClicked:  {
                            importTooltip.show("Starting import",5000);
                            var xhr = new XMLHttpRequest();
                            xhr.open("GET", rootWin.currentURL + patientList.model[index] ,true);
                            xhr.onreadystatechange = function() {
                                if (xhr.readyState === XMLHttpRequest.DONE) {
                                    var patName = patFileMan.importPatient(JSON.parse(xhr.responseText));
                                    importTooltip.show("Done Importing",1000);
                                    patExistLabel.text = "Yes";
                                }
                            }
                            xhr.send();
                        }
                        ToolTip {
                            id: importTooltip
                            anchors.centerIn: parent
                        }

                    }

                    CDScheduleButton {
                        text: "Schedule Patient"
                        Layout.preferredWidth: colWidth
                        enabled: patExistLabel.text === "Yes"
                        onClicked:  {
                            schPatientDia.patientID = patientList.model[index].split(".json")[0];
                            schPatientDia.open();
                        }
                    }

                    ScheduleNewPatientDialog {
                        id: schPatientDia
                        onAccepted: {
                            mainView.pop();
                            mainView.push("ListPatientsPage.qml");
                        }
                    }

                    TextField {
                        id: commentText
                        Layout.preferredWidth: colWidth

                        CDGitManager {
                            id: gitMan
                        }

                        Settings {
                            id: commentSettings
                            fileName: fileLocs.getLocalNewPatientCommentFile();
                        }

                        Component.onCompleted: {
                            text=commentSettings.value(patientList.model[index],"")
                        }

                        onTextChanged: {
                            commentSettings.setValue(patientList.model[index],text);
                        }

                        onEditingFinished: {
                            gitMan.commitData("Updated comment for " + patientList.model[index])
                        }
                    }
                }
            }
        }
        }
    }
}
