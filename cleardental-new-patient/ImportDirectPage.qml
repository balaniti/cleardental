// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTransparentPage {
    CDTranslucentPane {
        id: centerPane
        anchors.centerIn: parent
        GridLayout {
            columns: 2
            CDHeaderLabel {
                text: "Personal Info"
                Layout.columnSpan: 2
            }

            CDDescLabel {text: "First Name"}

            TextField {id: firstName;Layout.fillWidth: true}

            CDDescLabel {text: "Middle Name"}

            TextField {id: middleName;Layout.fillWidth: true}

            CDDescLabel {text: "Last Name"}

            TextField {id: lastName;Layout.fillWidth: true}

            CDDescLabel {text: "Preferred/Nick Name"}

            TextField {id: preferredName;Layout.fillWidth: true}

            CDDescLabel{text:"Date of Birth";}
            CDDatePicker{ id:patDOB;}

            CDDescLabel {text: "Sex"}

            ButtonGroup { id: radioGroup }
            Row {
                id: sexRow

                RadioButton {
                    id: maleRadio
                    checked: true
                    text: qsTr("Male")
                    ButtonGroup.group: radioGroup
                }

                RadioButton {
                    id: femaleRadio
                    text: qsTr("Female")
                    ButtonGroup.group: radioGroup
                }

                RadioButton {
                    id: otherRadio
                    text: "Other"
                    ButtonGroup.group: radioGroup
                }
            }

            CDDescLabel {text: "Gender (if different)"}

            TextField {id: gender;Layout.fillWidth: true}

            CDDescLabel{text:"Race";}
            ComboBox {
                id: race
                model: ["I choose not to answer","American Indian or Alaska native",
                    "Asian", "Black or African American", "Polynesian","White","Other"]
                Layout.fillWidth: true
            }

            CDDescLabel{text:"Ethnicity";}
            ComboBox {
                id: ethnicity
                model: ["I choose not to answer","Hispanic Or Latino",
                    "Not Hispanic or Latino"]
                Layout.fillWidth: true
            }
        }
    }

    CDAddButton {
        icon.name: "go-next"
        text: "Next"
        anchors.top: centerPane.bottom
        anchors.right: centerPane.right
        anchors.margins: 10
        onClicked: {
            rootWin.impNewPatObj["lastName"] = lastName.text;
            rootWin.impNewPatObj["firstName"] = firstName.text;
            rootWin.impNewPatObj["dob"] = patDOB.getPHPStyleDate();
            rootWin.impNewPatObj["middleName"] = middleName.text;
            rootWin.impNewPatObj["sex"] = radioGroup.checkedButton.text
            rootWin.impNewPatObj["preferredName"] = preferredName.text;
            rootWin.impNewPatObj["Ethnicity"] = race.currentText
            rootWin.impNewPatObj["gender"] = gender.text;
            rootWin.impNewPatObj["Race"] = ethnicity.currentText;
            rootWin.impNewPatObj["consentValues"] = "123,456,789,123";

            mainView.push("PatientContactPage.qml")
        }
    }

}
