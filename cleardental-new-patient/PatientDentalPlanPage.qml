// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTransparentPage {

    CDConstLoader {id: constLoader}

    CDGitManager {id: gitMan}

    CDFileLocations {id: fLocs}

    Settings {
        id: dentalPlanSettings
        fileName: fLocs.getDentalPlansFile(rootWin.impNewPatId);
    }

    CDTranslucentPane {
        id: centerPane
        anchors.centerIn: parent

        ColumnLayout {
            CDHeaderLabel {
                text: "Dental Plan Information"
            }
            ButtonGroup {
                id: paymentType
            }

            RadioButton {
                id: cash
                text: "Patient will pay via cash"
                checked: true
                ButtonGroup.group: paymentType
            }

            RadioButton {
                id: ins
                text: "Patient has a dental plan"
                ButtonGroup.group: paymentType
            }

            ScrollView {
                Layout.fillWidth: true
                Layout.maximumHeight: 520
                clip: true
                GridLayout {
                    id: insGrid
                    enabled: ins.checked
                    columns: 2

                    CDDescLabel {
                        text: "Dental Plan Company Name"
                    }

                    ComboBox {
                        id: priDenPlanName
                        Layout.fillWidth: true
                        Layout.minimumWidth: 360
                        textRole: "PlanName"
                        model: JSON.parse(constLoader.getUSDentalPlanListJSON())
                        editable: true

                        onActivated: {
                            var selectedObj = priDenPlanName.model[index];
                            priDenPlanName.editText = selectedObj["PlanName"]
                            priDenPlanAddr.text = selectedObj["Address"]
                            priDenPlanCity.text = selectedObj["City"]
                            priDenPlanState.setStateVal = selectedObj["State"]
                            priDenPlanZip.text = selectedObj["Zip"]
                            priClearingHouseID.text = selectedObj["PayerID"]
                        }
                    }

                    CDDescLabel {
                        text: "Dental Plan Company Address"
                    }

                    CDCopyLabel {
                        id: priDenPlanAddr
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Dental Plan Company City"
                    }

                    CDCopyLabel {
                        id: priDenPlanCity
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Dental Plan Company State"
                    }

                    CDStatePicker {
                        id: priDenPlanState
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Dental Plan Company Zip"
                    }

                    CDCopyLabel {
                        id: priDenPlanZip
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Policyholder/Subscriber ID"
                    }

                    CDCopyLabel {
                        id: priSubID
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Plan/Group Number"
                    }

                    CDCopyLabel {
                        id: priGroupNumb
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Employer Name"
                    }

                    CDCopyLabel {
                        id: priEmployerName
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Policyholder First Name"
                    }

                    CDCopyLabel {
                        id: priPolFirstName
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Policyholder Middle Name"
                    }

                    CDCopyLabel {
                        id: priPolMiddleName
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Policyholder Last Name"
                    }

                    CDCopyLabel {
                        id: priPolLastName
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Policyholder Address"
                    }

                    CDCopyLabel {
                        id: priPolAddr
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Policyholder City"
                    }

                    CDCopyLabel {
                        id: priPolCity
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Policyholder State"
                    }

                    CDStatePicker {
                        id: priPolState
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Policyholder Zip"
                    }

                    CDCopyLabel {
                        id: priPolZip
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Policyholder Date of Birth"
                    }

                    CDDatePicker {
                        id: priPolDob
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Policyholder Gender"
                    }

                    RowLayout {
                        id: priPolGender
                        RadioButton {
                            id: priPolMale
                            text: "Male"
                            checked: true
                        }

                        RadioButton {
                            id: priPolFemale
                            text: "Female"
                        }

                        RadioButton {
                            id: priPolOtherGender
                            text: "Other"
                        }

                        ButtonGroup {
                            id: priPolGenderGroup
                            buttons: [priPolMale,priPolFemale,priPolOtherGender]
                        }
                    }

                    CDDescLabel {
                        text: "Relationship to Policyholder"
                    }

                    ComboBox {
                        id: priPolRel
                        Layout.fillWidth: true
                        model: ["Self","Spouse","Dependent","Other"]
                    }

                    CDDescLabel {
                        text: "Claims phone number"
                    }

                    CDCopyLabel {
                        id: priClaimsPhoneNumber
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Claims fax number"
                    }

                    CDCopyLabel {
                        id: priClaimsFaxNumber
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Clearing House ID"
                    }

                    CDCopyLabel {
                        id: priClearingHouseID
                        Layout.fillWidth: true
                    }

                    CheckBox {
                        id: priPayDoctor
                        text: "Pays OON Doctor Directly"
                        Layout.columnSpan: 2
                    }

                    CDDescLabel {
                        text: "Diagnostic Coverage"
                    }

                    CDPercentSpinBox {
                        id: priDiagPercent
                    }

                    CDDescLabel {
                        text: "Preventative Coverage"
                    }

                    CDPercentSpinBox {
                        id: priPrevPercent
                    }

                    CDDescLabel {
                        text: "Basic Coverage"
                    }

                    CDPercentSpinBox {
                        id: priBasicPercent
                    }

                    CDDescLabel {
                        text: "Major Coverage"
                    }

                    CDPercentSpinBox {
                        id: priMajorPercent
                    }

                    CDDescLabel {
                        text: "Annual Max ($)"
                    }

                    CDCopyLabel {
                        id: priAnnMax
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Annual Deductable\n(including exams / cleaning) ($) "
                    }

                    CDCopyLabel {
                        id: priAnnDeductAll
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Annual Deductable\n(basic and major) ($)"
                    }

                    CDCopyLabel {
                        id: priAnnDeductBasicMajor
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Last Exam"
                    }

                    CDDatePicker {
                        id: priLastExam
                    }

                    CDDescLabel {
                        text: "Last BWs"
                    }

                    CDDatePicker {
                        id: priLastBW
                    }

                    CDDescLabel {
                        text: "Last Pan"
                    }

                    CDDatePicker {
                        id: priLastPan
                    }

                    MenuSeparator{Layout.columnSpan: 2;Layout.fillWidth: true;}

                    CDHeaderLabel {
                        text: "Secondary Dental Plan"
                        Layout.columnSpan: 2
                        Layout.fillWidth: true
                    }

                    CheckBox {
                        id: secCoverDental
                        text: "Covers Dental"
                    }

                    CheckBox {
                        id: secCoverMed
                        text: "Covers Medical"
                    }

                    CDDescLabel {
                        text: "Dental Plan Company Name"
                    }

                    CDCopyLabel {
                        id: secDenPlanName
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Dental Plan Company Address"
                    }

                    CDCopyLabel {
                        id: secDenPlanAddr
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Dental Plan Company City"
                    }

                    CDCopyLabel {
                        id: secDenPlanCity
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Dental Plan Company State"
                    }

                    CDStatePicker {
                        id: secDenPlanState
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Dental Plan Company Zip"
                    }

                    CDCopyLabel {
                        id: secDenPlanZip
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Policyholder/Subscriber ID"
                    }

                    CDCopyLabel {
                        id: secSubID
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Plan/Group Number"
                    }

                    CDCopyLabel {
                        id: secGroupNumb
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Employer Name"
                    }

                    CDCopyLabel {
                        id: secEmployerName
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Policyholder First Name"
                    }

                    CDCopyLabel {
                        id: secPolFirstName
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Policyholder Middle Name"
                    }

                    CDCopyLabel {
                        id: secPolMiddleName
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Policyholder Last Name"
                    }

                    CDCopyLabel {
                        id: secPolLastName
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Policyholder Address"
                    }

                    CDCopyLabel {
                        id: secPolAddr
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Policyholder City"
                    }

                    CDCopyLabel {
                        id: secPolCity
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Policyholder State"
                    }

                    CDStatePicker {
                        id: secPolState
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Policyholder Zip"
                    }

                    CDCopyLabel {
                        id: secPolZip
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Policyholder Date of Birth"
                    }

                    CDDatePicker {
                        id: secPolDob
                        Layout.fillWidth: true
                    }

                    CDDescLabel {
                        text: "Policyholder Gender"
                    }

                    RowLayout {
                        id: secPolGender
                        RadioButton {
                            id: secPolMale
                            text: "Male"
                            checked: true
                        }

                        RadioButton {
                            id: secPolFemale
                            text: "Female"
                        }

                        RadioButton {
                            id: secPolOtherGender
                            text: "Other"
                        }

                        ButtonGroup {
                            id: secPolGenderGroup
                            buttons: [priPolMale,priPolFemale,priPolOtherGender]
                        }
                    }

                    CDDescLabel {
                        text: "Relationship to Policyholder"
                    }

                    ComboBox {
                        id: secPolRel
                        Layout.fillWidth: true
                        model: ["Self","Spouse","Dependent","Other"]
                    }

                }//end of GridLayout
            } //end of scrollview
        } //end of ColumnLayout
    } //end of Pane

    CDAddButton {
        icon.name: "go-next"
        text: "Next"
        anchors.top: centerPane.bottom
        anchors.right: centerPane.right
        anchors.margins: 10

        CDCommonFunctions {
            id: comFuns
        }

        CDToolLauncher {
            id: launcher
        }

        CDTextFileManager {
            id: textMan
        }

        onClicked: {
            dentalPlanSettings.category = "Primary";

            if(cash.checked) {
                dentalPlanSettings.setValue("Name","");
            }
            else {
                dentalPlanSettings.category = "Primary";
                dentalPlanSettings.setValue("Name",priDenPlanName.currentText);
                dentalPlanSettings.setValue("Addr",priDenPlanAddr.text);
                dentalPlanSettings.setValue("City",priDenPlanCity.text);
                dentalPlanSettings.setValue("State",priDenPlanState.currentValue);
                dentalPlanSettings.setValue("Zip",priDenPlanZip.text);
                dentalPlanSettings.setValue("SubID",priSubID.text);
                dentalPlanSettings.setValue("GroupID",priGroupNumb.text);
                dentalPlanSettings.setValue("EmployerName",priEmployerName.text);
                dentalPlanSettings.setValue("PolHolderFirst",priPolFirstName.text);
                dentalPlanSettings.setValue("PolHolderMiddle",priPolMiddleName.text);
                dentalPlanSettings.setValue("PolHolderLast",priPolLastName.text);
                dentalPlanSettings.setValue("PolHolderAddress",priPolAddr.text);
                dentalPlanSettings.setValue("PolHolderCity",priPolCity.text);
                dentalPlanSettings.setValue("PolHolderState",priPolState.currentValue);
                dentalPlanSettings.setValue("PolHolderZip",priPolZip.text);
                dentalPlanSettings.setValue("PolHolderDOB",priPolDob.getDate());
                dentalPlanSettings.setValue("PolHolderGender",priPolGenderGroup.checkedButton.text);
                dentalPlanSettings.setValue("PolHolderRel",priPolRel.currentText);
                dentalPlanSettings.setValue("ClaimsPhoneNumber",priClaimsPhoneNumber.text);
                dentalPlanSettings.setValue("ClaimsFaxNumber",priClaimsFaxNumber.text);
                dentalPlanSettings.setValue("ClearingHouseID",priClearingHouseID.text);
                dentalPlanSettings.setValue("PayDoctor",priPayDoctor.checked);
                dentalPlanSettings.setValue("DiagPercent",priDiagPercent.value);
                dentalPlanSettings.setValue("PrevPercent",priPrevPercent.value);
                dentalPlanSettings.setValue("BasicPercent",priBasicPercent.value);
                dentalPlanSettings.setValue("MajorPercent",priMajorPercent.value);
                dentalPlanSettings.setValue("AnnMax",priAnnMax.text);
                dentalPlanSettings.setValue("AnnDeductAll",priAnnDeductAll.text);
                dentalPlanSettings.setValue("AnnDeductBasicMajor",priAnnDeductBasicMajor.text);
                dentalPlanSettings.setValue("LastExam",priLastExam.getDate());
                dentalPlanSettings.setValue("LastBW",priLastBW.getDate());
                dentalPlanSettings.setValue("LastPan",priLastPan.getDate());

                dentalPlanSettings.category = "Secondary";
                dentalPlanSettings.setValue("Dental",secCoverDental.checked);
                dentalPlanSettings.setValue("Medical",secCoverMed.checked);
                dentalPlanSettings.setValue("Name",secDenPlanName.text);
                dentalPlanSettings.setValue("Addr",secDenPlanAddr.text);
                dentalPlanSettings.setValue("City",secDenPlanCity.text);
                dentalPlanSettings.setValue("State",secDenPlanState.currentValue);
                dentalPlanSettings.setValue("Zip",secDenPlanZip.text);
                dentalPlanSettings.setValue("SubID",secSubID.text);
                dentalPlanSettings.setValue("GroupID",secGroupNumb.text);
                dentalPlanSettings.setValue("EmployerName",secEmployerName.text);
                dentalPlanSettings.setValue("PolHolderFirst",secPolFirstName.text);
                dentalPlanSettings.setValue("PolHolderMiddle",secPolMiddleName.text);
                dentalPlanSettings.setValue("PolHolderLast",secPolLastName.text);
                dentalPlanSettings.setValue("PolHolderAddress",secPolAddr.text);
                dentalPlanSettings.setValue("PolHolderCity",secPolCity.text);
                dentalPlanSettings.setValue("PolHolderState",secPolState.currentValue);
                dentalPlanSettings.setValue("PolHolderZip",secPolZip.text);
                dentalPlanSettings.setValue("PolHolderDOB",secPolDob.getDate());
                dentalPlanSettings.setValue("PolHolderGender",secPolGenderGroup.checkedButton.text);
                dentalPlanSettings.setValue("PolHolderRel",secPolRel.currentText);
            }

            dentalPlanSettings.sync();

            gitMan.commitData("Updated patient's dental plan for " + rootWin.impNewPatId);

            comFuns.addTxItemFromDCode(rootWin.impNewPatId,"D0150");
            comFuns.addTxItemFromDCode(rootWin.impNewPatId,"D0120");
            comFuns.addTxItemFromDCode(rootWin.impNewPatId,"D0274");
            comFuns.addTxItemFromDCode(rootWin.impNewPatId,"D1110");

            launcher.launchTool(CDToolLauncher.SchedulePatient,rootWin.impNewPatId);
            Qt.quit();
        }
    }
}
