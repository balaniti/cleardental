// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

ListView {
    id: ledgerView
    property int rowWidth: (rootWin.width - 200) / 4
    anchors.margins: 20
    ScrollBar.vertical: ScrollBar { }
    property real balance: 0
    property real writeOffs: 0
    property real totalCollections: 0

    property var officialLedger: [];
    property var viewToLedgerMap: []

    function filterLedgerObj() {
        officialLedger = [];
        viewToLedgerMap = [];
        for (let i = 0; i < rootWin.ledgerObj.length; i++) {
            if (rootWin.ledgerObj[i]["Deleted"] !== true) {
                officialLedger.push(rootWin.ledgerObj[i]);
                viewToLedgerMap.push(i);
            }
        }
    }

    header: Pane {
        z: 1024
        background: Rectangle {
            color: Material.color(Material.LightBlue)
            anchors.fill: parent
            opacity: .75
            //radius: 10
        }
        
        RowLayout {
            Label {
                font.bold: true
                Layout.minimumWidth: ledgerView.rowWidth
                text: "Date"
            }
            Label {
                font.bold: true
                Layout.minimumWidth: ledgerView.rowWidth
                text: "Amount"
            }
            Label {
                font.bold: true
                Layout.minimumWidth: ledgerView.rowWidth
                text: "Event"
            }
            Label {
                font.bold: true
                Layout.minimumWidth: ledgerView.rowWidth
                text: "Notes"
            }
        }
    }

    footer: Pane {
        z: 1024
        background: Rectangle {
            color: (balanceLabel.text.indexOf("-") === -1) ?
                       Material.color(Material.Green) :
                       Material.color(Material.Red)
            anchors.fill: parent
            opacity: .5
            radius: 10
        }

        RowLayout {
            Label {
                font.bold: true
                Layout.minimumWidth: ledgerView.rowWidth
                text: "Balance"
            }
            Label {
                id: balanceLabel
                font.bold: true
                text: writeOffs > 0 ? "$" + balance.toFixed(2) + "\n" + "[" + writeOffs.toFixed(2) + "]" :
                                      "$" + balance.toFixed(2)
            }
            Label {
                text: "Total production: $" + ledgerView.totalCollections.toFixed(2)
            }
        }
    }
    
    headerPositioning: ListView.OverlayHeader
    clip: true
    model: {
        filterLedgerObj();
        return officialLedger;
    }

    delegate: Component {
        Pane {
            background: Rectangle {
                color: Material.background
                anchors.fill: parent
                opacity: .75
            }
            
            RowLayout {
                Label {
                    Layout.minimumWidth: ledgerView.rowWidth
                    text: {
                        var dateObj = new Date(officialLedger[index]["Date"]);
                        return dateObj.toLocaleDateString();
                    }
                }
                Label {
                    Layout.minimumWidth: ledgerView.rowWidth

                    text: {
                        var rawAmount =0;
                        if(officialLedger[index]["Type"] === "Procedure") {
                            rawAmount = -1 * officialLedger[index]["Procedure"]["BasePrice"];
                        }
                        else if(officialLedger[index]["Type"] === "Cash Payment") {
                            rawAmount = officialLedger[index]["CashAmount"];
                        }
                        else if(officialLedger[index]["Type"] === "Check Payment") {
                            rawAmount = officialLedger[index]["CheckAmount"];
                        }
                        else if(officialLedger[index]["Type"] === "Credit Card Payment") {
                            rawAmount = officialLedger[index]["CCAmount"];
                        }

                        else if(officialLedger[index]["Type"] === "Dental Plan Payment") {
                            rawAmount = officialLedger[index]["PaidAmount"];
                        }

                        else if(officialLedger[index]["Type"] === "Credit") {
                            rawAmount = officialLedger[index]["CreditAmount"];
                        }

                        var legitTheAmount = rawAmount.toString();
                        var returnMe = "";
                        if(legitTheAmount.indexOf(".") === -1) {
                            returnMe = "$" + legitTheAmount + ".00";
                        } else {
                            returnMe = "$" + legitTheAmount ;
                        }
                        if(legitTheAmount.indexOf("-") === -1) {
                            color = Material.color(Material.Green)
                        }
                        else {
                            color = Material.color(Material.Red)
                        }
                        return returnMe;
                    }
                }
                Label {
                    Layout.minimumWidth: ledgerView.rowWidth
                    text: officialLedger[index]["Type"]
                }
                Label {
                    CDCommonFunctions {
                        id: comFuns
                    }

                    Layout.minimumWidth: ledgerView.rowWidth
                    Layout.maximumWidth: ledgerView.rowWidth
                    elide: Label.ElideRight
                    text: {
                        if(officialLedger[index]["Type"] === "Procedure") {
                            return comFuns.makeTxItemString(officialLedger[index]["Procedure"]);
                        }
                        else if(officialLedger[index]["Type"] === "Cash Payment") {
                            return officialLedger[index]["Notes"];
                        }
                        else if(officialLedger[index]["Type"] === "Check Payment") {
                            return officialLedger[index]["Notes"];
                        }
                        else if(officialLedger[index]["Type"] === "Credit Card Payment") {
                            return officialLedger[index]["Notes"];
                        }
                        else if(officialLedger[index]["Type"] === "Dental Plan Payment") {
                            var returnMe = officialLedger[index]["DentalPlanName"];
                            var writeoff = officialLedger[index]["WriteOff"];
                            var procedureName = officialLedger[index]["ProcedureObj"]["ProcedureName"];
                            returnMe = procedureName + "; " + returnMe;
                            if(parseFloat(writeoff) > 0) {
                                returnMe += "; Writeoff: $" + writeoff;
                            }

                            return returnMe;
                        }
                        else if(officialLedger[index]["Type"] === "Credit") {
                            return officialLedger[index]["Reason"];
                        }
                    }
                }

                CDDeleteButton {
                    id: deleteButton
                    text: "Delete"
                    onClicked: {
                        deleteLedgerItemDia.open();
                    }
                    DeleteLedgerItemDialog {
                        id: deleteLedgerItemDia

                        onAccepted: {
                            var realIndex = viewToLedgerMap[index];
                            rootWin.ledgerObj[realIndex]["Deleted"] = true;
                            rootWin.saveEditedLedger();
                        }
                    }
                }
            }
        }
        
    } //end of delegate

    function forceRefresh() {
        ledgerView.model = [];
        filterLedgerObj();
        ledgerView.model = officialLedger;
        ledgerView.balance = 0;
        ledgerView.writeOffs = 0;
        ledgerView.totalCollections = 0;
        for(var i=0;i<officialLedger.length;i++) {
            if(officialLedger[i]["Type"] === "Procedure") {
                ledgerView.balance -=  parseFloat(officialLedger[i]["Procedure"]["BasePrice"]);
            }
            else if(officialLedger[i]["Type"] === "Cash Payment") {
                ledgerView.balance+= parseFloat(officialLedger[i]["CashAmount"]);
                ledgerView.totalCollections+= parseFloat(officialLedger[i]["CashAmount"]);
            }
            else if(officialLedger[i]["Type"] === "Check Payment") {
                ledgerView.balance+= parseFloat(officialLedger[i]["CheckAmount"]);
                ledgerView.totalCollections+= parseFloat(officialLedger[i]["CheckAmount"]);
            }
            else if(officialLedger[i]["Type"] === "Credit Card Payment") {
                ledgerView.balance+= parseFloat(officialLedger[i]["CCAmount"]);
                ledgerView.totalCollections+= parseFloat(officialLedger[i]["CCAmount"]);
            }
            else if(officialLedger[i]["Type"] === "Dental Plan Payment") {
                ledgerView.balance+= parseFloat(officialLedger[i]["PaidAmount"]);
                ledgerView.balance+= parseFloat(officialLedger[i]["WriteOff"]);
                ledgerView.writeOffs +=parseFloat(officialLedger[i]["WriteOff"]);
                ledgerView.totalCollections+= parseFloat(officialLedger[i]["PaidAmount"]);
            }
            else if(officialLedger[i]["Type"] === "Credit") {
                ledgerView.balance+= parseFloat(officialLedger[i]["CreditAmount"]);
                ledgerView.writeOffs +=parseFloat(officialLedger[i]["CreditAmount"]);
            }
        }

        if(Math.abs(ledgerView.balance) < 0.01) {
            ledgerView.balance = 0;
        }
    }
}
