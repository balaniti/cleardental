// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

Pane {
    id: checkPaymentPane

    background: Rectangle {
        color: "white"
        opacity: 0
    }

    ColumnLayout {
        RowLayout {
            Label {
                text: "Amount in check ($)"
                font.bold: true
            }
            TextField {
                id: amountGotten
                Layout.minimumWidth: 150
            }
        }
        CDAddButton {
            text: "Add check payment"
            Layout.alignment: Qt.AlignRight
            onClicked: {
                var addMe = {
                    Date :  new Date(),
                    Type: "Check Payment",
                    CheckAmount : amountGotten.text,
                    Notes : "N/A"
                }

                rootWin.ledgerObj.push(addMe);
                rootWin.saveLedger();

                addPaymentDialog.close();
            }
        }
    }
}
