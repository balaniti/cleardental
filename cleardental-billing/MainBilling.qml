// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDAppWindow {
    id: rootWin
    title: qsTr("Billing [ID: " + PATIENT_FILE_NAME + "]")

    property var ledgerObj: []
    property bool newIns: false;

    header: CDPatientToolBar {
         headerText: "Billing:"

        ToolButton {
            icon.name: "application-menu"
            icon.width: 64
            icon.height: 64
            onClicked: patBillingdrawer.open();
            anchors.left: parent.left
        }

    }

    function saveEditedLedger() {
        var ledgJSON = JSON.stringify(rootWin.ledgerObj, null, '\t');
        ledMan.saveFile(fLocs.getLedgerFile(PATIENT_FILE_NAME),ledgJSON);
        gitMan.commitData("Deleted ledger entry for " + PATIENT_FILE_NAME);
        loadLedger();
        ledgerView.forceRefresh();
    }

    function saveLedger() {
        var ledgJSON  = JSON.stringify(rootWin.ledgerObj, null, '\t');
        ledMan.saveFile(fLocs.getLedgerFile(PATIENT_FILE_NAME),ledgJSON);
        gitMan.commitData("Updated ledger for " + PATIENT_FILE_NAME);
        ledgerView.forceRefresh();
    }

    function loadLedger() {
        var ledgJSON = ledMan.readFile(fLocs.getLedgerFile(PATIENT_FILE_NAME));
        if(ledgJSON.length < 2) {
            rootWin.ledgerObj = [];
        }
        else {
            rootWin.ledgerObj = JSON.parse(ledgJSON);
        }
        ledgerView.forceRefresh();
    }

    CDFileLocations {
        id: fLocs
    }

    CDTextFileManager {
        id: ledMan
    }

    CDGitManager {
        id: gitMan
    }

    Settings {
        id: dentalPlanSettings
        fileName: fLocs.getDentalPlansFile(PATIENT_FILE_NAME);
    }

    PatBillingdrawer {
        id: patBillingdrawer
        width: 0.33 * rootWin.width
        height: rootWin.height
    }

    LedgerView {
        id: ledgerView
        anchors.fill: parent
    }

    Label {
        font.pointSize: 24
        anchors.centerIn: parent
        text: "There appears to be nothing in the ledger"
        visible: ledgerView.count == 0
    }

    CDAddButton {
        text: "Add Payment"
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: 20
        onClicked:  {
            addPaymentDialog.open();
        }
    }

    CDPrinter {
        id: printer
    }

    CDCancelButton {
        icon.name: "document-print"
        text: "Print Ledger"
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins: 10
        onClicked: {
            printer.printPatientLedger(PATIENT_FILE_NAME,ledgerObj)
        }
    }

    AddPaymentDialog {
        id: addPaymentDialog
        anchors.centerIn: parent
    }

    Component.onCompleted: {
        loadLedger();
    }
}
