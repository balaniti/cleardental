// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

Pane {
    id: dentalPlanClaimPane

    property var claimsObj: []
    property var comboModel: []

    background: Rectangle {
        color: "white"
        opacity: 0
    }

    function updateUnpaidClaims() {
        var claimsJSON =ledMan.readFile(fLocs.getClaimsFile(PATIENT_FILE_NAME));
        if(claimsJSON.length < 2) {
            claimsObj = [];
            return;
        }
        claimsObj = JSON.parse(claimsJSON);


        comboModel = [];
        for(var i=0;i<claimsObj.length;i++) {
            var claim = claimsObj[i];
            if(claim["Status"] === "Sent") {
                var setText = comFuns.makeTxItemString(claim["Procedure"]) +
                        "; " + claim["Procedure"]["DCode"] +
                        "; Completed: " + new Date(claim["ProcedureDate"]).toLocaleDateString() +
                        "; Base Price: $" + claim["Procedure"]["BasePrice"];
                var addMe = {
                    claimText: setText,
                    procedureObj : claim["Procedure"],
                    procedureDate : claim["ProcedureDate"],
                    claimIndex: i
                };
                comboModel.push(addMe);
            }
        }
        whatProcedure.model = comboModel;
    }


    function updateInsPayValues(whereInput) {
        var basePrice = parseFloat(comboModel[whatProcedure.currentIndex]["procedureObj"]["BasePrice"]);
        if(whereInput === "maxAllowed") {
            writeOffText.text = (basePrice - parseFloat(maxAllowed.text)).toFixed(2);
            amountGotten.text = maxAllowed.text
        }
        else if(whereInput === "amountGotten") {
            if(maxAllowed.text.length == 0) {
                maxAllowed.text = amountGotten.text
            }

            patPortion.text = (basePrice - parseFloat(writeOffText.text) -  parseFloat(amountGotten.text)).toFixed(2);
        }
        else if(whereInput === "patPortion") {

        }
        else if(whereInput === "writeOffText") {

        }


    }

    GridLayout {
        columns: 2

        CDDescLabel {
            text: "Which Dental Plan"
        }

        ComboBox {
            id: whichPlanBox
            Layout.minimumWidth: 360
            property bool newInsVal: rootWin.newIns
            editable: true

            onNewInsValChanged: {
                if(rootWin.newIns) {
                    refreshDentalPlans();
                    rootWin.newIns = false;
                }
            }

            function refreshDentalPlans() {
                dentalPlanSettings.category = "Primary";
                var priName = dentalPlanSettings.value("Name","N/A");

                dentalPlanSettings.category = "Secondary";
                var secName = dentalPlanSettings.value("Name","N/A");

                model = [priName,secName];
            }

            Component.onCompleted: {
                refreshDentalPlans();
            }
        }
        
        CDDescLabel {
            text: "What Procedure"
        }
        
        ComboBox {
            id: whatProcedure
            Layout.minimumWidth: 560
            textRole: "claimText"
            valueRole: "claimIndex"
            onCurrentIndexChanged: {
                updateInsPayValues("comboBox")
            }
        }

        CDDescLabel {
            text: "Max Allowed ($)"
        }

        TextField {
            id: maxAllowed
            onTextChanged: updateInsPayValues("maxAllowed");
            onActiveFocusChanged: {
                if(activeFocus) {
                    selectAll();
                }
            }
        }

        CDDescLabel {
            text: "How much paid by plan ($)"
        }

        TextField {
            id: amountGotten
            onTextChanged: updateInsPayValues("amountGotten");
            onActiveFocusChanged: {
                if(activeFocus) {
                    selectAll();
                }
            }
        }

        CDDescLabel {
            text: "Patient portion ($)"
        }

        TextField {
            id: patPortion
            onTextChanged: updateInsPayValues("patPortion");
            onActiveFocusChanged: {
                if(activeFocus) {
                    selectAll();
                }
            }
        }

        CDDescLabel {
            text: "Write-off Amount ($)"
        }

        TextField {
            id: writeOffText
            onTextChanged: updateInsPayValues("writeOffText");
            onActiveFocusChanged: {
                if(activeFocus) {
                    selectAll();
                }
            }
        }

        Label {
            font.bold: true
            text: "Billed Procedure (if downgraded)"
        }

        TextField {
            id: downgradeCode
        }

        CDAddButton {
            text: "Add dental plan payment"
            Layout.alignment: Qt.AlignRight
            Layout.columnSpan: 2
            highlighted: true
            Material.accent: Material.Green
            onClicked: {
                var selectedClaim = claimsObj[whatProcedure.currentValue];
                selectedClaim["Status"] = "Paid";
                selectedClaim["MaxAllowed"] = maxAllowed.text;
                selectedClaim["PaidAmount"] = amountGotten.text;
                selectedClaim["PatientPortion"] = patPortion.text;
                selectedClaim["WriteOff"] = writeOffText.text;
                if(downgradeCode.text.length > 1) {
                    selectedClaim["DowngradeCode"] = downgradeCode.text;
                }
                var newClaimJSON = JSON.stringify(claimsObj, null, '\t');
                ledMan.saveFile(fLocs.getClaimsFile(PATIENT_FILE_NAME),newClaimJSON);

                var addMe = {
                    Date : new Date(),
                    PaidAmount : amountGotten.text,
                    Type : "Dental Plan Payment",
                    DentalPlanName: whichPlanBox.currentText,
                    WriteOff:writeOffText.text,
                    ProcedureObj: comboModel[whatProcedure.currentIndex]["procedureObj"],
                    ProcedureDate: comboModel[whatProcedure.currentIndex]["procedureDate"]
                }

                rootWin.ledgerObj.push(addMe);
                rootWin.saveLedger();

                gitMan.commitData("Updated claim and ledger for " + PATIENT_FILE_NAME);
                addPaymentDialog.close();
            }
        }
    }

    CDCommonFunctions {
        id: comFuns
    }

    Component.onCompleted: {
        updateUnpaidClaims();
    }

    onVisibleChanged:  {
        if(visible) {
            updateUnpaidClaims();
        }
    }
}
