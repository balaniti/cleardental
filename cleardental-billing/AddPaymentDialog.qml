// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTranslucentDialog {
    id: addPaymentDialog

    CDTranslucentPane {

        ColumnLayout {
            Label {
                text: "Add new Payment"
                font.pointSize: 24
                font.underline: true

            }

            RowLayout {
                Label {
                    font.bold: true
                    text: "Payment Type"
                }

                ComboBox {
                    id: paymentTypeBox
                    model: ["Credit Card","Cash","Check","Dental Plan Payment", "Credit"]
                    Layout.minimumWidth: 200
                }
            }

            DentalPlanClaimPane {
                id: dentalPlanClaimPane
                visible: paymentTypeBox.currentIndex == 3
            }

            CashPaymentPane {
                id: cashPaymentPane
                visible: paymentTypeBox.currentIndex == 1
            }

            CheckPaymentPane {
                id: checkPaymentPane
                visible: paymentTypeBox.currentIndex == 2
            }

            CreditCardPaymentPane {
                id: creditCardPaymentPane
                visible: paymentTypeBox.currentIndex == 0
            }
            CreditPane {
                id: creditPane
                visible: paymentTypeBox.currentIndex == 4
            }
        }
    }
}


