// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtGraphicalEffects 1.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDAppWindow {
    id: rootWin
    title: qsTr("Sealants #" + sealantObj["Tooth"] + " [ID: " + PATIENT_FILE_NAME + "]")
    property var sealantObj: JSON.parse(SEALANT_JSON)

    header: CDPatientToolBar {
        headerText: "Sealant: #" + sealantObj["Tooth"] + ":"
        ToolButton {
             icon.name: "application-menu"
             icon.width: 64
             icon.height: 64
             onClicked: drawer.open();
             anchors.left: parent.left
         }
    }

    ColumnLayout {
        anchors.centerIn: parent
        anchors.margins: 10

        Pane {
            Layout.alignment: Qt.AlignHCenter
            background: Rectangle {
                color: Material.color( Material.Green)
                opacity: .5
                radius: 10
            }

            GridLayout {
                columns: 2
                CDHeaderLabel {
                    Layout.columnSpan: 2
                    text: "Sealant Placement"
                }

                CDDescLabel {
                    text: "Patient presented with"
                }

                RowLayout {
                    ComboBox {
                        id: whoWithPatientBox
                        model: ["Mother", "Father", "Guardian", "Other"]
                    }
                    TextField {
                        id: otherPersonTextField

                        opacity: (whoWithPatientBox.currentIndex ===3) ? 1:0
                        Behavior on opacity {
                            PropertyAnimation {
                                duration: 300
                            }
                        }
                        Layout.minimumWidth: 300
                    }
                }

                CDDescLabel {
                    text: "Isolation"
                }

                ComboBox {
                    id: isolationBox
                    model: ["Dry Shield","Cotton Rolls", "Rubber Dam", "Dry-Angle"]
                    Layout.fillWidth: true
                }

                CDDescLabel {
                    text: "Cleanup"
                }

                RowLayout {
                    CheckBox {
                        id: airBox
                        text: "Air Abrasion"
                    }

                    CheckBox {
                        id: etchBox
                        text: "Etch"
                        checked: true
                    }
                    CheckBox {
                        id: pumpiceBox
                        text: "Pumice"
                    }

                    CheckBox {
                        id: prophyPasteBox
                        text: "Prophy Paste"
                    }
                }

                CheckBox {
                    Layout.columnSpan: 2
                    id: usedBondBox
                    text: "Used Bonding Agent"
                }

                CDDescLabel {
                    text: "Sealant Material"
                }

                ComboBox {
                    id: materialBox
                    model: ["Clinpro"]
                    Layout.fillWidth: true
                }
            }
        }
    }

    CDFinishProcedureButton {
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10

        CDFileLocations {
            id: fLocs
        }

        Settings {
            id: hardTissueSettings
            fileName: fLocs.getHardTissueChartFile(PATIENT_FILE_NAME);
        }

        CDCommonFunctions {
            id: comFuns
        }

        onClicked: {
            var caseNote = "Patient presented for sealant on tooth #" + sealantObj["Tooth"] + ".\n";
            caseNote += "Patient presented with ";
            if(whoWithPatientBox.currentIndex == 3) {
                caseNote += otherPersonTextField.text;
            }
            else {
                caseNote += whoWithPatientBox.currentText;
            }
            caseNote += ".\n";
            caseNote += isolationBox.currentText + " was used for isolation.\n";
            if(airBox.checked) {
                caseNote += airBox.text + " used.\n";
            }
            if(etchBox.checked) {
                caseNote += "Etch placed and rinsed.\n";
            }
            if(pumpiceBox.checked) {
                caseNote += pumpiceBox.text + " used.\n";
            }
            if(prophyPasteBox.checked) {
                caseNote += prophyPasteBox.text + " used.\n";
            }
            if(usedBondBox.checked) {
                caseNote += "Bonding agent used.\n";
            }

            caseNote += "Sealed with " + materialBox.currentText + ".\n";

            var fullAttr = hardTissueSettings.value(sealantObj["Tooth"],"");
            fullAttr = comFuns.addHardTissueAttr(fullAttr, "Sealant");
            hardTissueSettings.setValue(sealantObj["Tooth"], fullAttr);

            finishDialog.txItemsToComplete = [sealantObj];
            finishDialog.caseNoteString = caseNote;
            finishDialog.open();
        }
    }

    CDHygieneRoomStatus {
        id: drawer
        height: rootWin.height
        width: 0.33 * rootWin.width
    }

    CDFinishProcedureDialog {
        id: finishDialog

    }
}
