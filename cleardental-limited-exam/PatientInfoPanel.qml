// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.3
import dental.clear 1.0
import QtQuick.Controls.Material 2.12

Pane {

    background: Rectangle {
        color: Material.color(Material.BlueGrey)
        radius: 10
        opacity: .1
    }

    INIReader {
        id: nameReader
        patientName: patientFileName;
        infoType: INIReader.Personal
        groupName: "Name"
    }

    INIReader {
        id: personalReader
        patientName: patientFileName;
        infoType: INIReader.Personal
        groupName: "Personal"
    }

    ColumnLayout {
        anchors.fill: parent
        Label {
            text:"Chief Complaint tooth:"
            font.pointSize: 16
        }

        RowLayout {
            Repeater {
                model: 16
                RoundButton {
                    text: index+1
                    checkable: true
                }
            }
        }

        RowLayout {
            Repeater {
                model: 16
                RoundButton {
                    text: 32 - index
                    checkable: true
                }
            }
        }

        RowLayout {
            CheckBox {
                id: otherCCBox
                text: "Not a tooth / other"
            }
            TextField {
                id: otherCCField
                visible: opacity != 0
                opacity: otherCCBox.checked ? 1: 0
                placeholderText: "Please enter the patient's Chief Complaint"
                Layout.fillWidth: true
                Behavior on opacity {
                    PropertyAnimation {
                        duration: 300
                    }
                }
            }
        }

        MenuSeparator {Layout.fillWidth: true}

        Label {
            text:"Diagnosis:"
            font.pointSize: 16
        }

        ToothDiagnosisPanel {
            visible: opacity != 0
            opacity: otherCCBox.checked ? 0:1
            Behavior on opacity {
                PropertyAnimation {
                    duration: 300
                }
            }
        }

        TextField {
            id: otherDx
            visible: opacity != 0
            opacity: otherCCBox.checked ? 1:0
            Layout.fillWidth: true
            placeholderText: "Please enter the diagnosis"
            Behavior on opacity {
                PropertyAnimation {
                    duration: 300
                }
            }
        }




    }

}
