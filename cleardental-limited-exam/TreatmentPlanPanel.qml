// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.12

Pane {
    background: Rectangle {
        color: Material.color(Material.Brown)
        radius: 10
        opacity: .1
    }

    ColumnLayout {

        Label {
            font.pointSize: 16
            text: "Treatment Plan"
        }

        Label {
            font.pointSize: 32
            text: "None"
        }
    }



}
