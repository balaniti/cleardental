// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTransparentPage {
    CDTranslucentPane {
        backMaterialColor: Material.Green
        anchors.centerIn: parent
        GridLayout {

            columns: 2
            Label {
                text:"CC: " + rootWin.patCC
                font.underline: true
                font.pointSize: 24
                Layout.columnSpan: 2
            }

            CDDescLabel {
                text: "Diagnosis"
            }
            TextField {
                id: dxField
                Layout.minimumWidth: 350
            }

            CDDescLabel {
                text: "Treatment Name"
            }

            ComboBox {
                id: otherTxBox
                Layout.minimumWidth: 350

                CDConstLoader {
                    id: constLoader
                }

                valueRole: "DCode"
                textRole: "ProcedureName"
                editable: true

                Component.onCompleted: {
                    var jsonText = constLoader.getDCodesJSON();
                    var jsonObj = JSON.parse(jsonText);
                    model = jsonObj;
                }

                onCurrentValueChanged: {
                    dCodeField.text = currentValue
                }
            }

            CDDescLabel {
                text: "DCode"
            }

            TextField {
                id: dCodeField
                placeholderText: "DCode"
                Layout.minimumWidth: 350
            }
        }
    }

    CDFinishProcedureButton {
        text: "Finish Exam"
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10

        CDCommonFunctions {
            id: comFuns
        }

        onClicked: {
            var caseNote= "Patient presented for a limited exam.\n";
            caseNote += "CC: " + rootWin.patCC + "\n";
            caseNote += "Dx: " + dxField.text + "\n";
            caseNote += "Tx: " + otherTxBox.currentText + "\n";

            var customTxObj = ({});
            customTxObj["ProcedureName"] = otherTxBox.currentText;
            customTxObj["DCode"] =otherTxBox.currentValue;
            comFuns.addTxItem(PATIENT_FILE_NAME,"Primary","Unphased",customTxObj);
            var limitedExObj = comFuns.findTxDCode(PATIENT_FILE_NAME,"D0140");
            finishDialog.txItemsToComplete = limitedExObj;

            finishDialog.caseNoteString = caseNote;
            finishDialog.open();
        }
    }

    CDFinishProcedureDialog {
        id: finishDialog
    }
}


