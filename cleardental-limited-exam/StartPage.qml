// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.3
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1

CDTransparentPage {
    ColumnLayout {
        anchors.centerIn:  parent


        CDCommonFunctions {
            id: comFuns
        }

        CDMedReviewPane {
            id: medReview
            Layout.fillWidth: true
            Layout.maximumHeight: 480

        }


        CDTranslucentPane {
            backMaterialColor: Material.Green
            ColumnLayout {
                CDHeaderLabel {
                    text:"Chief Complaint tooth:"
                }

                RowLayout {
                    id: maxRow
                    Layout.alignment: Qt.AlignHCenter
                    Repeater {
                        model: 16
                        RoundButton {
                            id: maxButton
                            text: index+1
                            checkable: true
                            Layout.minimumWidth: 72
                            Layout.minimumHeight: 72
                            Component.onCompleted: {
                                butGroup.buttons.push(maxButton);
                            }
                        }
                    }

                    opacity: otherCCBox.checked ? 0 :1
                    Behavior on opacity {
                        PropertyAnimation {
                            duration: 300
                        }
                    }
                }

                RowLayout {
                    id: maxPedRow

                    visible: comFuns.getYearsOld(PATIENT_FILE_NAME) < 15

                    Layout.alignment: Qt.AlignHCenter
                    Repeater {
                        model: 10
                        RoundButton {
                            id: maxPedButton
                            text: String.fromCharCode(65+index)
                            checkable: true
                            Layout.minimumWidth: 72
                            Layout.minimumHeight: 72
                            Component.onCompleted: {
                                butGroup.buttons.push(maxPedButton);
                            }
                        }
                    }

                    opacity: otherCCBox.checked ? 0 :1
                    Behavior on opacity {
                        PropertyAnimation {
                            duration: 300
                        }
                    }
                }

                RowLayout {
                    id: manPedRow

                    visible: comFuns.getYearsOld(PATIENT_FILE_NAME) < 15

                    Layout.alignment: Qt.AlignHCenter
                    Repeater {
                        model: 10
                        RoundButton {
                            id: manPedButton
                            text: String.fromCharCode(84-index)
                            checkable: true
                            Layout.minimumWidth: 72
                            Layout.minimumHeight: 72
                            Component.onCompleted: {
                                butGroup.buttons.push(manPedButton);
                            }
                        }
                    }

                    opacity: otherCCBox.checked ? 0 :1
                    Behavior on opacity {
                        PropertyAnimation {
                            duration: 300
                        }
                    }
                }

                RowLayout {
                    Layout.alignment: Qt.AlignHCenter
                    Repeater {
                        model: 16
                        RoundButton {
                            id: manButton
                            text: 32 - index
                            checkable: true
                            Layout.minimumWidth: 72
                            Layout.minimumHeight: 72
                            Component.onCompleted: {
                                butGroup.buttons.push(manButton);
                            }
                        }
                    }
                    opacity: otherCCBox.checked ? 0 :1
                    Behavior on opacity {
                        PropertyAnimation {
                            duration: 300
                        }
                    }
                }

                ButtonGroup {
                    id: butGroup
                }

                RowLayout {
                    CheckBox {
                        id: otherCCBox
                        text: "Not a tooth / other"
                    }
                    TextField {
                        id: otherCCField
                        visible: opacity != 0
                        opacity: otherCCBox.checked ? 1: 0
                        placeholderText: "Please enter the patient's Chief Complaint"
                        Layout.fillWidth: true
                        Behavior on opacity {
                            PropertyAnimation {
                                duration: 300
                            }
                        }
                    }
                }
            }
        }
    }





    CDButton {

        text: "Next"
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        icon.name: "go-next"
        Material.accent: Material.Lime
        highlighted: true
        enabled: otherCCBox.checked || butGroup.checkState !== Qt.Unchecked
        onClicked: {
            rootWin.currentCaseNote = medReview.generateCaseNoteString();

            if(otherCCBox.checked) {
                rootWin.patCC = otherCCField.text;
                mainStack.push("OtherIssueDxTx.qml")
            }

            else if (butGroup.checkState !== Qt.Unchecked) {
                var selectedBut = butGroup.checkedButton;
                rootWin.ccTooth = selectedBut.text
                mainStack.push("TakeRadiographs.qml")

            }
        }

    }

}
