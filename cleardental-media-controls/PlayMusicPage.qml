// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Window 2.10
import QtMultimedia 5.12

CDTransparentPage {
    id: playMusicPage
    CDTranslucentPane {
        anchors.centerIn: parent
        ColumnLayout {
            CDHeaderLabel {
                text: rootWin.selectedMediaCat + " Music"
            }
            Label {
                text: playMusic.playlist.currentItemSource
            }

            RowLayout {
                Layout.fillWidth: true
                CDDescLabel {
                    text: "Volume"
                }
                Slider {
                    id: volSilder
                    Layout.fillWidth: true
                    from: 0
                    to: 1
                    value: 1
                }
            }
        }
    }

    Audio {
        id: playMusic
        playlist: musicPlaylist
        volume:volSilder.value
    }

    Playlist {
        id: musicPlaylist
        playbackMode: Playlist.Loop
    }

    Component {
        id: makeWindow
        CDAppWindow {
            x: 0
            y: 0
            width: 1920
            height: 1080

            Image {
                source: "file://" + mediaFiles.getMusicPics(rootWin.selectedMediaCat)[0]
                anchors.fill: parent
            }
            visible: true

            Label {
                id: currTimeLabel

                anchors.bottom: parent.bottom
                anchors.right: parent.right
                anchors.margins: 25
                font.pixelSize: 72
                color: Material.backgroundColor
                horizontalAlignment: Qt.AlignRight
                styleColor: Material.foreground
                style: Text.Outline

                function getDateTimeString() {
                    var currentTime = new Date();
                    text= currentTime.toLocaleTimeString('en-US') + "\n" + currentTime.toLocaleDateString();
                }

                SequentialAnimation {
                    id: updateTimeAni
                    loops: Animation.Infinite
                    ScriptAction {
                        script: currTimeLabel.getDateTimeString()
                    }
                    PauseAnimation { duration: 15 * 1000 }
                }

                Component.onCompleted: updateTimeAni.running = true
            }
        }
    }

    Component.onCompleted: {
        var makeMusicList = mediaFiles.getMusicList(rootWin.selectedMediaCat);
        for(var i=0;i<makeMusicList.length;i++) {
            musicPlaylist.addItem("file://" + makeMusicList[i]);
        }
        playMusic.play();

        for(var screenI=0; screenI <= 1;screenI++) {
            var window    = makeWindow.createObject(playMusicPage);
            if(screenI === 0) {
                window.y = 0;
            }
            else if(screenI === 1) {
                window.y = 1080;
            }
        }
    }
}
