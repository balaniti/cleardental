// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0

CDTransparentPage {
    CDTranslucentPane {
        anchors.centerIn: parent
        ColumnLayout {
            spacing: 20
            CDHeaderLabel {
                text: "Select Music Type"
            }

            Repeater {
                model: mediaFiles.getMusicTypes().length
                CDCancelButton {
                    icon.name: ""
                    text: mediaFiles.getMusicTypes()[index] + " Music"
                    onClicked: {
                        rootWin.selectedMediaCat = mediaFiles.getMusicTypes()[index];
                        mainView.push("PlayMusicPage.qml");
                    }
                }
            }
        }
    }
}
