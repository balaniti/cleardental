// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0

CDTransparentPage {
    CDTranslucentPane {
        anchors.centerIn: parent
        enabled: !rootWin.runningGame
        ColumnLayout {
            spacing: 20
            CDHeaderLabel {
                text: "Select Game"
            }
            CheckBox {
                id: topMonitor
                text: "Launch on top monitor"
                checked: true
            }
            CDCancelButton {
                icon.name: ""
                text: "Pengiun Racer"
                onClicked: {
                    toolLauncher.launchGame(CDToolLauncher.TUX_RACER,topMonitor.checked);
                    rootWin.runningGame = true;
                }
            }
            CDCancelButton {
                icon.name: ""
                text: "Cave Story"
                onClicked: {
                    toolLauncher.launchGame(CDToolLauncher.CAVE_STORY,topMonitor.checked);
                    rootWin.runningGame = true;
                }
            }
            CDCancelButton {
                icon.name: ""
                text: "Wonderboy"
                onClicked: {
                    toolLauncher.launchGame(CDToolLauncher.WONDER_BOY,topMonitor.checked);
                    rootWin.runningGame = true;
                }
            }
            CDCancelButton {
                icon.name: ""
                text: "All Stars"
                onClicked: {
                    toolLauncher.launchGame(CDToolLauncher.ALL_STAR,topMonitor.checked);
                    rootWin.runningGame = true;
                }
            }
        }
    }
}
