// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0

CDTransparentPage {
    CDTranslucentPane {
        anchors.centerIn: parent
        GridLayout {
            columns: 4
            columnSpacing: 20
            rowSpacing: 20
            CDHeaderLabel {
                text: "Select Video Type"
                Layout.columnSpan: 4
                font.pointSize: 42
            }
            Repeater {
                model: mediaFiles.getVideoTypes().length
                CDCancelButton {
                    icon.name: ""
                    font.pointSize: 32
                    text: mediaFiles.getVideoTypes()[index] + " Videos"
                    onClicked: {
                        rootWin.selectedMediaCat = mediaFiles.getVideoTypes()[index];
                        mainView.push("PlayVideoPage.qml");
                    }
                }
            }
        }
    }
}
