// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtGraphicalEffects 1.12
import QtQuick.Window 2.10

CDAppWindow {
    id: rootWin
    title: qsTr(comFuns.makeTxItemString(procedureObj) + " [ID: " + PATIENT_FILE_NAME + "]")
    property var procedureObj: JSON.parse(PROCEDURE_JSON)

    CDCommonFunctions {
        id: comFuns
    }

    header: CDPatientToolBar {
        headerText:comFuns.makeTxItemString(procedureObj)
        ToolButton {
             icon.name: "application-menu"
             icon.width: 64
             icon.height: 64
             onClicked: drawer.open();
             anchors.left: parent.left
         }
    }

    ColumnLayout {
        anchors.centerIn: parent
        anchors.margins: 10

        Pane {
            Layout.alignment: Qt.AlignHCenter
            background: Rectangle {
                color: Material.color( Material.Green)
                opacity: .5
                radius: 10
            }

            GridLayout {
                columns: 2
                CDHeaderLabel {
                    Layout.columnSpan: 2
                    text: "Space Maintainer"
                }

                CDDescLabel {
                    text: "Patient presented with"
                }

                RowLayout {
                    ComboBox {
                        id: whoWithPatientBox
                        model: ["Mother", "Father", "Guardian", "Other"]
                    }
                    TextField {
                        id: otherPersonTextField

                        opacity: (whoWithPatientBox.currentIndex ===3) ? 1:0
                        Behavior on opacity {
                            PropertyAnimation {
                                duration: 300
                            }
                        }
                        Layout.minimumWidth: 300
                    }
                }

                CDDescLabel {
                    text: "Band Size"
                }

                TextField {
                    id: bandSizeField
                }

                CDDescLabel {
                    text: "Cemented Using"
                }

                ComboBox {
                    id: cementBox
                    editable: true
                    model: ["RelyX™ Luting", "RelyX™ Unicem","GC Fuji PLUS®", "GC FujiCEM® 2"]
                    Layout.minimumWidth: 300
                }
            }
        }
    }

    CDFinishProcedureButton {
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        onClicked: {
            var caseNote = "Patient presented for space maintainer on tooth #" + procedureObj["Tooth"] + ".\n";
            caseNote += "Patient presented with ";
            if(whoWithPatientBox.currentIndex == 3) {
                caseNote += otherPersonTextField.text;
            }
            else {
                caseNote += whoWithPatientBox.currentText;
            }

            caseNote += ".\n";

            caseNote += "Size: " + bandSizeField.text + ".\n";

            caseNote += "Cemented space maintainer using " + cementBox.currentText + ".\n";

            finishDialog.txItemsToComplete = [procedureObj];
            finishDialog.caseNoteString = caseNote;
            finishDialog.open();
        }
    }

    CDHygieneRoomStatus {
        id: drawer
        height: rootWin.height
        width: 0.33 * rootWin.width
    }

    CDFinishProcedureDialog {
        id: finishDialog

    }
}
