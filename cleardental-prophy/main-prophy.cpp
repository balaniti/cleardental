// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickImageProvider>
#include <QFont>
#include <QIcon>
#include <QDebug>
#include <QHostInfo>

#include "cddefaults.h"
#include "cdradiographmanager.h"

int main(int argc, char *argv[])
{
    CDDefaults::setAppDefaults();
    QCoreApplication::setApplicationName("ClearDental-Prophy");

    QGuiApplication app(argc, argv);
    app.setWindowIcon(CDDefaults::defaultColorIcon());

    if(QCoreApplication::arguments().count()<2) {
        qDebug()<<"You need to give the patient's name as an argument!";
        return -32;
    }

    CDDefaults::registerQMLTypes();
    qmlRegisterType<CDRadiographManager>("dental.clear", 1, 0, "CDRadiographManager");

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("PATIENT_FILE_NAME",QString(argv[1]));
    engine.rootContext()->setContextProperty("CHAIR_NAME",QHostInfo::localHostName());
    engine.load(QUrl(QStringLiteral("qrc:/MainProphy.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    CDDefaults::enableBlurBackground();


    return app.exec();
}
