// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.0
import QtQuick.Window 2.12
import QtQuick.Controls 2.12

CDTranslucentDialog {
    id: imageWinDia
    property string imgSource
    Image {
        source: imgSource
        anchors.fill: parent
        fillMode: Image.PreserveAspectFit
        MouseArea {
            anchors.fill: parent
            onClicked: {
                imageWinDia.close();
            }
        }
    }

    CDAppWindow {
        x: 0
        y: 0
        width: 1920
        height: 1080
        visibility: ApplicationWindow.Windowed
        visible: imageWinDia.visible

        Image {
            source: imgSource
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
        }
    }

    CDAppWindow {
        x: 0
        y: 1080
        width: 1920
        height: 1080
        visibility: ApplicationWindow.Windowed
        visible: imageWinDia.visible

        Image {
            source: imgSource
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
        }
    }
}


