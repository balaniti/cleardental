// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10

RowLayout {
    ComboBox {
        id: setHourBox
        model: ["1","2","3","4","5","6","7","8","9","10","11","12"]
        Layout.preferredWidth: 75
        currentIndex: 7
        //onCurrentIndexChanged: {checkTime();}
    }

    Label {
        text: ":"
    }

    ComboBox {
        id: setMinBox
        property var minModel: []
        Layout.preferredWidth: 75
        Component.onCompleted:  {
            for(var i=0;i<60;i+=5) {
                if(i < 10) {
                    minModel.push("0"+i);
                }
                else {
                    minModel.push(i);
                }
            }
            model = minModel
        }
        //onCurrentIndexChanged: {checkTime();}
    }

    ComboBox {
        id: setAMPMBox
        model: ["AM","PM"]
        Layout.preferredWidth: 75
        //onCurrentIndexChanged: {checkTime();}
    }
}
