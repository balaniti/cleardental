// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cdtextfilemanager.h"

#include <QCoreApplication>
#include <QFile>
#include <QTextStream>

#include "cddefaults.h"

CDTextfileManager::CDTextfileManager(QObject *parent) : QObject(parent){}

void CDTextfileManager::saveFile(QString getFileName, QString getFileText) {
    QFile file(getFileName);
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream stream(&file);
    stream<<getFileText;
    file.close();
}

QString CDTextfileManager::readFile(QString getFileName) {
    QString returnMe="";
    QFile file(getFileName);
    if(!file.exists()) {
        return "";
    }
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream stream(&file);
    returnMe = stream.readAll();
    file.close();

    return returnMe;
}

bool CDTextfileManager::doesFileExist(QString getFileName)
{
    QString patientName = QCoreApplication::arguments().at(1);
    QString workingPath = CDDefaults::getPatDir() + patientName;

    QFile file(workingPath + "/" + getFileName);
    return file.exists();
}
