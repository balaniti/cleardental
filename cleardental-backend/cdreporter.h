﻿// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef CDREPORTER_H
#define CDREPORTER_H

#include <QObject>
#include <QDateTime>

#include <QDebug>
#include <QList>
#include <QSettings>
#include <QJsonDocument>
#include <QFile>
#include <QJsonArray>
#include <QJsonValue>
#include <QJsonObject>

#include "cdschedulemanager.h"
#include "cdpatientmanager.h"
#include "cdfilelocations.h"

class CDReporter : public QObject
{
    Q_OBJECT
public:
    explicit CDReporter(QObject *parent = nullptr);

    enum PatientStatusType {
        INVALID_STATUS_TYPE,
        DISMISSED,
        NEVERMINDER,
        NEVER_SEEN,
        ONCE_SEEN,
        GHOSTED,
        FORGOTTEN,
        ACTIVE_PATIENT
    };
    Q_ENUM(PatientStatusType)

public slots:
    static int patientsSeen(QDateTime startDay, QDateTime endday);
    static int patientsNoShowed(QDateTime startDay, QDateTime endday);

    //returns value in cents
    static int totalProduction(QDateTime startDay, QDateTime endday);
    static int totalProduction(QDateTime startDay, QDateTime endday, QString providerID);
    static int totalCollections(QDateTime startDay, QDateTime endday);
    static int totalCollections(QDateTime startDay, QDateTime endday, QString providerID);
    static int totalWriteoffs(QDateTime startDay, QDateTime endday);
    static int totalAmountBilledOut(QDateTime startDay, QDateTime endday);
    static int getBalance(QString patID);
    static int getPayments(QString patID); //Credit / writeoffs are taken to account
    static int getCollections(QString patID);
    static int getCharges(QString patID);

    static QVariantMap getProcedureCounts(QDateTime startDay, QDateTime endday);
    static QString getDentalPlanName(QString patID);
    static QStringList getDueBalances();
    static QStringList needsAppt();
    static QString getLastAppt(QString patID);
    static QString getLastApptFile(QString patID);
    static int getTotalPatCount();
    static int get30DayPatCount();
    static QVariantMap getTotalReferrals();
    static QVariantMap get30DayReferrals();
    static PatientStatusType getPatientStatusType(QString patID);
    static qreal getCollectionsFromRefferalType(QString referralSource);

private:
    static QList<QDateTime> getDaysFromRange(QDateTime startDay, QDateTime endday);
    static bool dayInRange(QDate getDate, QDateTime startDay, QDateTime endday);
    static bool checkValid(QDateTime startDay, QDateTime endday);
};

#endif // CDREPORTER_H
