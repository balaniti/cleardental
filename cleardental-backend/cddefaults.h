// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef CDDEFAULTS_H
#define CDDEFAULTS_H

#include <QObject>
#include <QGuiApplication>

class CDDefaults : public QObject
{
    Q_OBJECT
public:
    explicit CDDefaults(QObject *parent = nullptr);

    static QString getBaseDataPath();
    static QString getPatDir();
    static QString getPatID();
    static QString getDocPrefDir();
    static QString getLocationsDir();
    static QString getLocalLocationInfoDir();
    static QString getScheduleDir(QString location);
    static QString getLocalScheduleDir();
    static QString getExtraSoftwareDir();
    static void setAppDefaults();
    static void registerQMLTypes();
    static QIcon defaultColorIcon();
    static void enableBlurBackground();
    static void setGuiApplicationSettings(QGuiApplication* app);

signals:

public slots:
};

#endif // CDDEFAULTS_H
