// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtGraphicalEffects 1.12
import QtQuick.Window 2.10

CDTranslucentPane {
    id: consentFormPane
    contentWidth: colForm.implicitWidth
    contentHeight: colForm.implicitHeight
    backMaterialColor: Material.Indigo

    property bool isSigned
    property string consentType: ""
    property string consentSpec: ""
    property string consentString: consentType + "-" + consentSpec

    function generateCaseNoteString() {
        var returnMe = ""
        if(isSigned) {
            returnMe = "Consent for today's procedure was signed.\n";
        }
        return returnMe;
    }

    CDFileLocations {
        id: m_fileLocs
    }

    CDTextFileManager {
        id: m_fileMan

    }

    CDFileWatcher {
        id: m_watchConsent
        fileName: m_fileLocs.getConsentImageFile(PATIENT_FILE_NAME, new Date(), consentString)
        onFileUpdated: {
            var signURL = m_fileLocs.getConsentImageFile(PATIENT_FILE_NAME, new Date(), consentString);
            var fullRead = m_fileMan.readFile(signURL);
            isSigned = fullRead.length > 0;
        }
    }

    CDPatientFileManager {
        id: m_patFileMan
    }


    ColumnLayout {
        id: colForm
        Label {
            text: "Consent Form"
            font.pointSize: 24
            font.underline: true
        }
        RowLayout {
            Label {
                id: isSignedLabel
                text: "Signed"
                color: Material.color(Material.Green)
                visible: isSigned
            }
            Label {
                text: "Not Signed"
                color: Material.color(Material.Red)
                visible: !isSigned
            }

            Component.onCompleted: {
                var signURL = m_fileLocs.getConsentImageFile(PATIENT_FILE_NAME, new Date(), consentString);
                var fullRead = m_fileMan.readFile(signURL);
                isSigned = fullRead.length > 0;
            }
        }
    }

    CDConsentDialog {
        id: m_consentDia
    }

    Button {
        parent: consentFormPane.contentItem
        text: "Show Consent Form"
        anchors.right: consentFormPane.contentItem.right
        onClicked: m_consentDia.open();
    }

}
