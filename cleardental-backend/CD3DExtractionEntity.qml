// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick 2.2 as QQ2
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

import Qt3D.Core 2.0
import Qt3D.Render 2.14
import Qt3D.Input 2.0
import Qt3D.Extras 2.15
import QtQuick.Scene3D 2.14

Entity {

    id: extractionEntity


    PhongMaterial {
        id: material
        ambient:  redRGBA
        diffuse: redRGBA
        specular: redRGBA

        property real alphaAmount: .2

        SequentialAnimation {
            PropertyAnimation {
                duration: 500
                from: .2
                to: .8
                target: material
                property: "alphaAmount"
            }
            PropertyAnimation {
                duration: 500
                from: .8
                to: .2
                target: material
                property: "alphaAmount"
            }
            loops: Animation.Infinite
            running: true
        }

        property color redRGBA: Qt.rgba( 1,0, 0, alphaAmount);
    }


//    ExtrudedTextMesh { //the offset makes it really hard to make it hard to rotate along the center
//        id: extGeo
//        text: " X"
//        depth: .2
//    }

    Mesh {
        id: extGeo
        source: "qrc:/3dModels/X.stl"

    }

    Transform {
        id: extTransForm

        rotationX: 90
        scale3D.z: 1.5
        translation.y: 3


        PropertyAnimation {
            duration: 5000
            from: 0
            to: (360)
            loops: Animation.Infinite
            target: extTransForm
            property: "rotationY"
            running: true
        }
    }




    components: [ material, extGeo, extTransForm ]

}

