#version 330 core
out vec4 fragColor;

uniform vec2 u_resolution;
uniform sampler2D u_Smiley;

void main() {
    vec2 tex_coords =  gl_FragCoord.xy * 0.10 /u_resolution.xy;
    vec2 repeat = vec2(25, 50);
    gl_FragColor = texture(u_Smiley,vec2(mod(tex_coords.x * repeat.x, 1), mod(tex_coords.y * repeat.y, 1)));
}
