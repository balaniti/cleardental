#-------------------------------------------------
#
# Project created by QtCreator 2019-05-18T17:59:02
#
#-------------------------------------------------

QT       += widgets qml quick texttospeech multimedia printsupport xml 3dcore 3drender 3dinput 3dlogic 3dextras 3dquick 3danimation

TARGET = cleardental-backend
TEMPLATE = lib

DEFINES += CLEARDENTALBACKEND_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


unix:images.path=/usr/share/icons/hicolor/512x512/apps/
unix:images.files=clearDentalLogo.png
unix:images.files+=icons/clearDental-bareLogo.svg
unix:images.files+=icons/clearDental-caseNotes.svg
unix:images.files+=icons/clearDental-importNewPatient.svg
unix:images.files+=icons/clearDental-mediaControls.svg
unix:images.files+=icons/clearDental-messaging.svg
unix:images.files+=icons/clearDental-reports.svg
unix:images.files+=icons/clearDental-schedule.svg
unix:images.files+=icons/clearDental-selectPatient.svg
unix:images.files+=icons/clearDental-sendClaims.svg
unix:images.files+=icons/clearDental-task-manager.svg
INSTALLS+=images

SOURCES += \
    cdbillingestimator.cpp \
    cdclearinghousecomm.cpp \
    cdconstloader.cpp \
    cddefaults.cpp \
    cdfeeschedulemanager.cpp \
    cdgitmanager.cpp \
    cdmediafilemanager.cpp \
    cdpatientreminder.cpp \
    cdqrcodeimageprovider.cpp \
    cdscanner.cpp \
    cdsensorinterface.cpp \
    cdsensorworker.cpp \
    cdspellchecker.cpp \
    cdsyntaxhighlighter.cpp \
    cdtextfilemanager.cpp \
    cdtoollauncher.cpp \
    cdimagemanager.cpp \
    cdproviderinfo.cpp \
    cdaquireradiograph.cpp \
    cdpatientmanager.cpp \
    cdspeech.cpp \
    cdpatientfilemanager.cpp \
    cddruglist.cpp \
    cdspeechtotext.cpp \
    cdappointment.cpp \
    cdschedulemanager.cpp \
    cdfilelocations.cpp \
    cddoctorlistmanager.cpp \
    cdlocationmanager.cpp \
    cdfilewatcher.cpp \
    cdprinter.cpp \
    cddefaultformsprovider.cpp\
    cdradiographmanager.cpp\
    cdreporter.cpp

HEADERS += \
    cdbillingestimator.h \
    cdclearinghousecomm.h \
    cdconstloader.h \
    cddefaults.h \
    cdfeeschedulemanager.h \
    cdgitmanager.h \
    cdmediafilemanager.h \
    cdpatientreminder.h \
    cdqrcodeimageprovider.h \
    cdscanner.h \
    cdsensorinterface.h \
    cdsensorworker.h \
    cdspellchecker.h \
    cdsyntaxhighlighter.h \
    cdtextfilemanager.h \
    cdtoollauncher.h \
    cdimagemanager.h \
    cdproviderinfo.h \
    cdaquireradiograph.h \
    cdpatientmanager.h \
    cdspeech.h \
    cdpatientfilemanager.h \
    cddruglist.h \
    cdspeechtotext.h \
    cdappointment.h \
    cdschedulemanager.h \
    cdfilelocations.h \
    cddoctorlistmanager.h \
    cdlocationmanager.h \
    cdfilewatcher.h \
    cdprinter.h \
    cddefaultformsprovider.h \
    cdradiographmanager.h \
    cdreporter.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

RESOURCES += \
    3DModels.qrc \
    adaForms.qrc \
    audioCues.qrc \
    commonQML.qrc \
    constNamesValues.qrc \
    druglist.qrc \
    glsl.qrc \
    icons.qrc \
    defaultconsentforms.qrc \
    patientGuides.qrc

LIBS += -lusb-1.0 -lhunspell -lqrencode

DISTFILES +=
