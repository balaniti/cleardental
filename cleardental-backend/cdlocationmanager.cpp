// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cdlocationmanager.h"

#include <QDir>
#include <QFile>
#include <QDebug>
#include <QRandomGenerator>
#include <QSettings>

#include "cddefaults.h"

CDLocationManager::CDLocationManager(QObject *parent) : QObject(parent)
{

}

QStringList CDLocationManager::getListOfLocationFiles()
{
    QStringList returnMe;
    QDir locsDir(CDDefaults::getLocationsDir());
    foreach(QString locFolder,
            locsDir.entryList(QDir::AllEntries | QDir::NoDotAndDotDot
                              | QDir::NoSymLinks)) {
        //qDebug()<<locFolder;
        QString addMe =
                locsDir.canonicalPath() + "/" + locFolder + "/prefs.ini";
        returnMe.append(addMe);
    }
    return returnMe;
}

QString CDLocationManager::getLocalLocationName()
{
    QString realDir =
            QFile::symLinkTarget(CDDefaults::getLocalLocationInfoDir());
    QStringList dirNames = realDir.split("/");
    return realDir.split("/").last();
}

QString CDLocationManager::addLocation(QVariantMap data)
{
    QDir locDir(CDDefaults::getLocationsDir());
    locDir.mkdir(data["nickName"].toString());

    if(data["isLocal"].toBool()) {
        QFile::remove(CDDefaults::getLocalLocationInfoDir());
        QFile::link(CDDefaults::getLocationsDir() + data["nickName"].toString(),
                CDDefaults::getLocalLocationInfoDir());
    }
    return data["nickName"].toString();
}

void CDLocationManager::removeLocation(QString locationID)
{
    QDir loc(CDDefaults::getLocationsDir() + "/" + locationID);
    loc.removeRecursively();
}

