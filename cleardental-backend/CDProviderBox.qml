// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1
import dental.clear 1.0

ComboBox {
    id: providerBox

    property var providerNames: []
    property var providerIDs: []
    property var providerNPIs: []

    CDDoctorListManager {id: providerMan}
    Settings {id: docPrefReader}

    Component.onCompleted: {
        var allProvs = providerMan.getListOfProviderFiles();
        for(var i=0;i<allProvs.length;i++) {
            docPrefReader.fileName = allProvs[i];

            var addID = allProvs[i].split("/")[
                        allProvs[i].split("/").length-2]
            var addName = docPrefReader.value("FirstName","") + " " +
                    docPrefReader.value("LastName","") + " (" + addID +
                    ")"

            var provType = docPrefReader.value("ProviderType","");
            if(provType === "General Dentist") { //TODO: add the other provider types
                addName = "Dr. " + addName;
            }
            var addNPI = docPrefReader.value("NPINumb","");

            providerNames.push(addName);
            providerIDs.push(addID);
            providerNPIs.push(addNPI);
        }
        providerBox.model = providerNames;
    }
}
