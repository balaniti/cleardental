// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0

ComboBox {
    property string patientID: ""

    textRole: "UserViewName"
    valueRole: "TxObj"

    CDFileLocations {
        id: m_fileLocs
    }

    CDTextFileManager {
        id: m_textMan
    }

    CDCommonFunctions {
        id: m_ComFun
    }

    function refreshList() {
        var txPlanTxt = m_textMan.readFile(m_fileLocs.getTreatmentPlanFile(patientID));
        if(txPlanTxt.length < 2) {
            return;
        }
        var txPlanObj = JSON.parse(txPlanTxt);

        var makeMeModel = [];
        var txPlanNames = Object.keys(txPlanObj);
        for(var txPlanI=0; txPlanI<txPlanNames.length;txPlanI++) {
            var phaseLists = txPlanObj[txPlanNames[txPlanI]];
            var phaseNames = Object.keys(phaseLists);
            for(var phaseI=0; phaseI<phaseNames.length;phaseI++) {
                var txPlanItems = phaseLists[phaseNames[phaseI]];
                for(var txItemI=0; txItemI<txPlanItems.length;txItemI++) {
                    var addMe = ({});
                    addMe["UserViewName"] = m_ComFun.makeTxItemString(txPlanItems[txItemI]);
                    addMe["TxObj"] = txPlanItems[txItemI];
                    makeMeModel.push(addMe);
                }
            }
        }

        model = makeMeModel;

    }

    onPatientIDChanged: {
        refreshList();
    }

}
