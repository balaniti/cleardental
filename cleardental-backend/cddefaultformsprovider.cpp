// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cddefaultformsprovider.h"

#include <QFile>

CDDefaultFormsProvider::CDDefaultFormsProvider(QObject *parent) : QObject(parent)
{

}

QString CDDefaultFormsProvider::getDefaultConsent(QString consentName)
{
    Q_INIT_RESOURCE(defaultconsentforms);
    QFile f(":/defaultConsentForms/"+consentName+".html");
    QString returnMe = "";
    if(f.exists()) {
        f.open(QIODevice::ReadOnly | QIODevice::Text);
        returnMe = f.readAll();
    }
    else {
        QFile fe(":/defaultConsentForms/"+consentName+".txt");
        fe.open(QIODevice::ReadOnly | QIODevice::Text);
        returnMe = fe.readAll();
    }

    return returnMe;
}

QString CDDefaultFormsProvider::getDefaultPostOp(QString procedureName)
{
    return procedureName;
}
