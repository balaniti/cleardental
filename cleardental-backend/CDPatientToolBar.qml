// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

ToolBar {
    height: 72
    property string headerText: ""

    background: Rectangle{
        color: Material.primaryColor
        opacity: .5
    }

    RowLayout {
        anchors.centerIn: parent

        Label {
            text: headerText
            font.pointSize: 32
            font.bold: true
        }

        Label {

            font.pointSize: 32
            font.italic: true

            CDFileLocations {
                id: tBarfLocs
            }

            Settings {
                id: patPersonalSettings
                fileName: tBarfLocs.getPersonalFile(PATIENT_FILE_NAME)
            }

            Component.onCompleted: {
                patPersonalSettings.category = "Name"
                var fName = patPersonalSettings.value("FirstName","")
                var lName = patPersonalSettings.value("LastName","")
                var prefName = patPersonalSettings.value("PreferredName","")

                patPersonalSettings.category = "Personal"
                var dob = patPersonalSettings.value("DateOfBirth","1/1/0001");

                if(prefName.length > 1) {
                    text= fName + " \"" + prefName + "\" " + lName+ " [" + dob + "]";
                }
                else {
                    text= fName + " " + lName+ " [" + dob + "]";
                }


            }

        }
        Image {
            source: "file://" + tBarfLocs.getProfileImageFile(PATIENT_FILE_NAME)
            Layout.maximumHeight: 64
            Layout.maximumWidth: 64
            fillMode: Image.PreserveAspectFit
        }
    }

    CDConstLoader {
        id: constsL;
    }

    ToolButton {
        icon.name: "window-minimize"
        onClicked: rootWin.showMinimized();
        icon.width: 64
        icon.height: 64
        anchors.right: closeButton.visible ? closeButton.left : parent.right
        anchors.margins: 10
        visible: constsL.getNodeType() === "touch"
    }

    ToolButton {
        id: closeButton
        icon.name: "dialog-close"
        icon.color: Material.color(Material.Red)
        onClicked: Qt.quit()
        icon.width: 64
        icon.height: 64
        anchors.right: parent.right
        visible: constsL.getNodeType() === "touch"
    }
}
