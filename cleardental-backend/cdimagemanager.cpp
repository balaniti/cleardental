// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cdimagemanager.h"

#include "cddefaults.h"

#include <QQuickImageProvider>
#include <QDebug>
#include <QCoreApplication>

CDImageManager::CDImageManager()  : QQuickImageProvider(QQuickImageProvider::Pixmap)
{

}

QPixmap CDImageManager::requestPixmap(const QString &id, QSize *size, const QSize &requestedSize) {
    QPixmap returnMe;

    QString patDir = CDDefaults::getPatDir();
    QString patName = QCoreApplication::arguments().at(1);
    returnMe.load(patDir+ patName + "/images/" + id);
    size->setWidth(returnMe.width());
    size->setHeight(returnMe.height());
    if(requestedSize.isValid()) {
        returnMe = returnMe.scaled(requestedSize);
    }

    return returnMe;
}
