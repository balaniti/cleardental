// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.12

Dialog {
    parent:Overlay.overlay
    anchors.centerIn: parent
    modal: Qt.WindowModal
    background: Rectangle {
        anchors.fill: parent
        color: "white"
        opacity: .75
        radius: 10
    }


}
