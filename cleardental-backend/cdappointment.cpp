// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cdappointment.h"

CDAppointment::CDAppointment(QObject *parent) : QObject(parent)
{

}

QDateTime CDAppointment::appointmentStartTime() const
{
    return m_appointmentStartTime;
}

QString CDAppointment::chair() const
{
    return m_chair;
}

QString CDAppointment::patientID() const
{
    return m_patientID;
}

QString CDAppointment::appointmentComment() const
{
    return m_appointmentComment;
}

CDAppointment::AppointmentStatus CDAppointment::appointmentStatus() const
{
    return m_appointmentStatus;
}

QDateTime CDAppointment::appointmentEndTime() const
{
    return m_appointmentEndTime;
}

QString CDAppointment::procedureName() const
{
    return m_procedureName;
}

QString CDAppointment::doctorID() const
{
    return m_doctorID;
}

QString CDAppointment::filePath() const
{
    return m_filePath;
}

void CDAppointment::setAppointmentStartTime(QDateTime appointmentTime)
{
    m_appointmentStartTime = appointmentTime;
}

void CDAppointment::setChair(QString chair)
{
    m_chair = chair;
}

void CDAppointment::setPatientID(QString patientID)
{
    m_patientID = patientID;
}

void CDAppointment::setAppointmentComment(QString appointmentComment)
{
    m_appointmentComment = appointmentComment;
}

void CDAppointment::setAppointmentStatus(CDAppointment::AppointmentStatus
                                         appointmentStatus)
{
    m_appointmentStatus = appointmentStatus;
}

void CDAppointment::setAppointmentEndTime(QDateTime appointmentEndTime)
{
    m_appointmentEndTime = appointmentEndTime;
}

void CDAppointment::setProcedureName(QString procedureName)
{
    m_procedureName = procedureName;
}

void CDAppointment::setDoctorID(QString doctorID)
{
    m_doctorID = doctorID;
}

void CDAppointment::setFilePath(QString filePath)
{
    m_filePath = filePath;
}
