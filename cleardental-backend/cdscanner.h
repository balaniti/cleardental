// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef CDSCANNER_H
#define CDSCANNER_H

#include <QObject>

class CDScanner : public QObject
{
    Q_OBJECT
public:
    explicit CDScanner(QObject *parent = nullptr);

    Q_INVOKABLE static QString scanCard(bool withDriversLicense);
    Q_INVOKABLE static void saveCard(QString patID);


signals:

};

#endif // CDSCANNER_H
