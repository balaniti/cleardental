// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.0

RowLayout {
    property var choosenSurfaces: getSurfaces();
    signal surfaceChanged(string newSurfaces)
    property string workingToothNumb: "0"
    property bool isPost: true
    onWorkingToothNumbChanged:  {
        isPost = commonFuns.isPost(workingToothNumb);
    }

    function parseSurfaces(inputSurfaces) {
        for(var i=0;i<inputSurfaces.length;i++) {
            var surf = inputSurfaces[i];
            if(surf === "M") {
                mSurface.checked = true;
            }
            if(surf === "O") {
                iSurface.checked = true;
            }
            if(surf === "I") {
                iSurface.checked = true;
            }
            if(surf === "D") {
                dSurface.checked = true;
            }
            if(surf === "B") {
                fSurface.checked = true;
            }
            if(surf === "F") {
                fSurface.checked = true;
            }
            if(surf === "L") {
                lSurface.checked = true;
            }
        }
    }

    function getSurfaces() {
        var returnMe="";
        if(mSurface.checked) {
            returnMe +="M";
        } if(iSurface.checked) {
            returnMe +=iSurface.text
        } if(dSurface.checked) {
            returnMe +="D";
        } if(fSurface.checked) {
            returnMe += fSurface.text;
        }if(lSurface.checked) {
            returnMe +="L";
        }
        return returnMe;
    }

    function clearSurfaces() {
        mSurface.checked = false;
        iSurface.checked = false;
        dSurface.checked = false;
        fSurface.checked = false;
        lSurface.checked = false;
    }

    CDCommonFunctions {
        id: commonFuns
    }

    Button {
        id: mSurface
        text: "M"
        checkable: true
        onCheckedChanged: surfaceChanged(getSurfaces())
    }
    Button {
        id: iSurface
        text: isPost ? "O" : "I"
        checkable: true
        onCheckedChanged: surfaceChanged(getSurfaces())
    }
    Button {
        id: dSurface
        text: "D"
        checkable: true
        onCheckedChanged: surfaceChanged(getSurfaces())
    }
    Button {
        id: fSurface
        text: isPost ? "B" : "F"
        checkable: true
        onCheckedChanged: surfaceChanged(getSurfaces())
    }
    Button {
        id: lSurface
        text: "L"
        checkable: true
        onCheckedChanged: surfaceChanged(getSurfaces())
    }

    Behavior on opacity {
        NumberAnimation {
            duration: 300
        }
    }

}
