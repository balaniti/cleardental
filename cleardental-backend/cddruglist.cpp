// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cddruglist.h"

#include <QFile>
#include <QTextStream>

CDDrugList::CDDrugList(QObject *parent) : QObject(parent)
{

}

QStringList CDDrugList::getSmallDrugList()
{
    QStringList returnMe;
    QFile smallFile(":/druglists/shortList.txt");
    if(smallFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QTextStream in(&smallFile);
        QString line = in.readLine();
        while (!line.isNull()) {
            returnMe.append(line);
            line = in.readLine();
        }
    }

    return returnMe;
}

QStringList CDDrugList::getBigDrugList()
{
    QStringList returnMe;
    QFile bigFile(":/druglists/shortList.txt");
    if(bigFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QTextStream in(&bigFile);
        QString line = in.readLine();
        while (!line.isNull()) {
            returnMe.append(line);
            line = in.readLine();
        }
    }

    return returnMe;
}
