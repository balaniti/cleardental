// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef CDFILELOCATIONS_H
#define CDFILELOCATIONS_H

#include <QObject>
#include <QDate>

class CDFileLocations : public QObject
{
    Q_OBJECT
public:
    explicit CDFileLocations(QObject *parent = nullptr);

    //Patient Text Files
    Q_INVOKABLE static QString getPatientDirectory(QString patID);
    Q_INVOKABLE static QString getPatientAppointmentDirectory(QString patID);
    Q_INVOKABLE static QString getPersonalFile(QString patID);
    Q_INVOKABLE static QString getMessagesFile(QString patID);
    Q_INVOKABLE static QString getMedicationsFile(QString patID);
    Q_INVOKABLE static QString getAllergiesFile(QString patID);
    Q_INVOKABLE static QString getConditionsFile(QString patID);
    Q_INVOKABLE static QString getSurgeriesFile(QString patID);
    Q_INVOKABLE static QString getRecallFile(QString patID);
    Q_INVOKABLE static QString getVitalsFile(QString patID);
    Q_INVOKABLE static QString getReviewsFile(QString patID);
    Q_INVOKABLE static QString getHardTissueChartFile(QString patID);
    Q_INVOKABLE static QString getPerioChartFile(QString patID);
    Q_INVOKABLE static QString getEoEFile(QString patID);
    Q_INVOKABLE static QString getIoEFile(QString patID);
    Q_INVOKABLE static QString getTreatmentPlanFile(QString patID);
    Q_INVOKABLE static QString getNewPatInterviewFile(QString patID);
    Q_INVOKABLE static QString getDentalPlansFile(QString patID);
    Q_INVOKABLE static QString getOtherMedInfoFile(QString patID);
    Q_INVOKABLE static QString getLedgerFile(QString patID);
    Q_INVOKABLE static QString getClaimsFile(QString patID);
    Q_INVOKABLE static QString getFullBillingFile(QString patID);
    Q_INVOKABLE static QString getRemovableStatusFile(QString patID);
    Q_INVOKABLE static QString getCOVIDStatusFile(QString patID);
    Q_INVOKABLE static QString getCaseNoteFile(QString patID);
    Q_INVOKABLE static QString getPatientMessagesFile(QString patID);
    Q_INVOKABLE static QString getPatientScanDir(QString patID);
    Q_INVOKABLE static QString getPatientProphyPrefs(QString patID);
    Q_INVOKABLE static QString getPatientPrevTx(QString patID);
    Q_INVOKABLE static QString getPatientPrevPerioHxDirectory(QString patID);
    Q_INVOKABLE static QString getPatientFamilyDirectory(QString patID);

    //Doctor Text File
    Q_INVOKABLE static QString getDocInfoFile(QString docID);
    Q_INVOKABLE static QString getWorkingDocInfoFile();

    //Practice Info Files
    Q_INVOKABLE static QString getLocalPracticePreferenceFile();
    Q_INVOKABLE static QString getLocalDefaultChairFile();
    Q_INVOKABLE static QString getLocalSensorCalibrationFile();
    Q_INVOKABLE static QString getLocalChairStatus();
    Q_INVOKABLE static QString getLocalPracticeCOVIDFile();
    Q_INVOKABLE static QString getLocalNewPatientCommentFile();
    Q_INVOKABLE static QString getLocalAppointmentsDirectory();
    Q_INVOKABLE static QString getLocalTaskFile();
    Q_INVOKABLE static QString getLocalPracticeFeesFile();
    Q_INVOKABLE static QString getPracticeFeesFile(QString pracID);
    Q_INVOKABLE static QString getPracticePreferenceFile(QString pracID);
    Q_INVOKABLE static QString getPracticeChairFile(QString pracID);
    Q_INVOKABLE static QString getPracticeLabsFile(QString pracID);

    //Image files
    Q_INVOKABLE static QString getProfileImageFile(QString patID);
    Q_INVOKABLE static QString getProfileImageDir(QString patID);
    Q_INVOKABLE static QString getPatientCardFile(QString patID);
    Q_INVOKABLE static QString getRadiographDir(QString patID);
    Q_INVOKABLE static QString getFGTPDir(QString patID);
    Q_INVOKABLE static QString getIntraOralDir(QString patID);
    Q_INVOKABLE static QString getConsentImageFile(QString patID, QDate day, QString procedure);
    Q_INVOKABLE static QString getTempImageLocation();


signals:

public slots:
};

#endif // CDFILELOCATIONS_H
