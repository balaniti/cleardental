// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9

import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Extras 2.15
import QtQuick.Scene3D 2.14

Transform {
    id: distalTransForm

    function isMaxillary() {
        var toothNumber = toothScene3D.toothNumb;
        var intValOfTooth = parseInt(toothNumber);
        var returnMe = true;
        if(isNaN(intValOfTooth)) { //pediatric
            var charCode = toothNumber.charCodeAt(0);
            var jCharCode = "J".charCodeAt(0); //(A->J; K->T)
            returnMe = (charCode <=jCharCode);
        }
        else {
            returnMe = (intValOfTooth <= 16);
        }
        return returnMe; //just in case...
    }

    property real setRoll: 0
    property var firstRotation: isMaxillary() ? fromEulerAngles(-60,0,0) :  fromEulerAngles(60,0,0)

    onSetRollChanged: {

        var secondRotation = fromEulerAngles(0,setRoll,0);

        var w_a = firstRotation.scalar;
        var x_a = firstRotation.x;
        var y_a = firstRotation.y;
        var z_a = firstRotation.z;

        var w_b = secondRotation.scalar;
        var x_b = secondRotation.x;
        var y_b = secondRotation.y;
        var z_b = secondRotation.z;

        var w_c = w_a * w_b - x_a * x_b - y_a * y_b - z_a * z_b;
        var x_c = w_a * x_b + x_a * w_b + y_a * z_b - z_a * y_b;
        var y_c = w_a * y_b - x_a * z_b + y_a * w_b + z_a * x_b;
        var z_c = w_a * z_b + x_a * y_b - y_a * x_b + z_a * w_b;

        rotation = Qt.quaternion(w_c,x_c,y_c,z_c)
    }

    PropertyAnimation {
        duration: 10000
        from: 0
        to: (360)
        loops: Animation.Infinite
        target: distalTransForm
        property: "setRoll"
        running: true
    }
    //rotationX: 60
}

