// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9

import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Extras 2.15
import QtQuick.Scene3D 2.14

Entity {

    id: distalToothEntity

    property string baseMeshSource
    property string toothAspect
    property string setColor
    property bool makeMissing: false

    CD3DToothPhongMaterial {
        id: distalMaterial
        materialColor: setColor
        makeTransparent: makeMissing

    }

    Mesh {
        id: distalToothMesh
        source: baseMeshSource
        meshName: toothAspect
    }

    CD3DAnimatedRotation {
        id: distalTransForm
    }



    components: [ distalToothMesh, distalMaterial, distalTransForm ]

}
