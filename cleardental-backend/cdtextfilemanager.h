// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef CDTEXTFILEMANAGER_H
#define CDTEXTFILEMANAGER_H

#include <QObject>

class CDTextfileManager : public QObject
{
    Q_OBJECT
public:
    explicit CDTextfileManager(QObject *parent = nullptr);

    Q_INVOKABLE static void saveFile(QString getFileName, QString getFileText);
    Q_INVOKABLE static QString readFile(QString getFileName);
    Q_INVOKABLE static bool doesFileExist(QString getFileName);

};

#endif // CDTEXTFILEMANAGER_H
