// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cdfilewatcher.h"
#include "cdgitmanager.h"

#include <QFile>
#include <QDir>
#include <QFileInfo>

CDFileWatcher::CDFileWatcher(QObject *parent) : QObject(parent)
{
    connect(&m_Watcher,SIGNAL(fileChanged(QString)),this,SLOT(handleUpdate(QString)));
    connect(&m_Watcher,SIGNAL(directoryChanged(QString)),this,SLOT(handleUpdate(QString)));
}

QString CDFileWatcher::getFileName() const
{
    return m_fileName;
}

void CDFileWatcher::setFileName(QString fileName)
{
    m_fileName = fileName;
    QFile checker(m_fileName);
    if(!checker.exists()) {
        checker.open(QIODevice::WriteOnly);
        checker.close();
        CDGitManager::commitData("Added in blank file " + fileName + " for the purpose of watching it");
        m_Watcher.addPath(fileName);
    } else {
        QFileInfo fInfo(m_fileName);
        if(fInfo.isDir()) {
            recurseDir(m_fileName);
        }
        else {
            m_Watcher.addPath(fileName);
        }
    }
}

void CDFileWatcher::handleUpdate(QString fileName)
{
    if(!m_Watcher.files().contains(fileName)) {
        m_Watcher.addPath(fileName);
    }
    emit fileUpdated();
}

void CDFileWatcher::recurseDir(QString readDir)
{
    m_Watcher.addPath(readDir);
    QDir baseDir(readDir);
    QStringList subsequentDirs = baseDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
    foreach(QString subDir, subsequentDirs) {
        QString findMe = baseDir.absolutePath() + "/" + subDir;
        recurseDir(findMe);
    }
}
