// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.0
import QtQuick.Controls.Material 2.12
import QtQuick.Controls 2.2

Button {
    icon.name: "edit-delete"
    Material.accent: Material.Red
    highlighted: true
    icon.width: 32
    icon.height: 32
    font.pointSize: 18
}
