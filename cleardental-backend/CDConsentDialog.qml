// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.14
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtGraphicalEffects 1.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDTranslucentDialog {
    id: consentDialog

    CDFileLocations {
        id: m_fileLocs
    }

    Settings {
        id: locPrefs

        category: "Consents"
        fileName: m_fileLocs.getLocalPracticePreferenceFile();

        Component.onCompleted: {
            if(consentType === "ext") {
                consentText.text = value("ExtConsent","");
            }
            else if(consentType === "opt") {
                consentText.text = value("GenProcedureConsent","");
            }
        }
    }

    ColumnLayout {
        CDTranslucentPane {
            ColumnLayout {
                CDHeaderLabel {
                    function getHeaderText() {
                        var returnMe = "Consent for ";
                        if(consentType === "ext") {
                            returnMe += " Extraction"
                        }
                        else if(consentType === "opt") {
                            returnMe += " Filling"
                        }
                        return returnMe;
                    }
                    text: getHeaderText()
                }

                Flickable {
                    Layout.minimumHeight: 300
                    Layout.fillWidth: true
                    Layout.maximumWidth: 800
                    Layout.maximumHeight: 500
                    clip: true
                    contentWidth: 800
                    contentHeight: consentText.contentHeight
                    ScrollBar.vertical: ScrollBar { }
                    Label {
                        id: consentText
                        textFormat: TextEdit.RichText
                        wrapMode: TextEdit.WordWrap
                        anchors.fill: parent
                        font.pointSize: 16
                    }
                }

                Canvas {
                    id: drawCanvas
                    Layout.minimumHeight:  240
                    Layout.maximumHeight: 240
                    Layout.minimumWidth: 800
                    Layout.maximumWidth: 800
                    Layout.alignment: Qt.AlignHCenter
                    property int dotlength: 0
                    property var dots: []
                    onPaint: {
                        var ctx = getContext("2d");
                        ctx.clearRect(0, 0, width, height);
                        ctx.fillStyle = Qt.rgba(0, 0, 0, 1);
                        ctx.beginPath();
                        ctx.rect(1,1,width-1,height-1);
                        ctx.stroke();

                        ctx.beginPath();
                        for(var i=0;i<dots.length;i+=2) {
                            if(dots[i]===",") {
                                ctx.stroke();
                                ctx.beginPath();
                            }
                            else {
                                ctx.lineTo(dots[i],dots[i+1]);
                            }
                        }
                        ctx.stroke();
                    }

                    MouseArea {
                        anchors.fill: parent
                        onPositionChanged: {
                            drawCanvas.dots.push(mouseX);
                            drawCanvas.dots.push(mouseY);
                            drawCanvas.requestPaint();
                            drawCanvas.dotlength++;
                        }
                        onReleased: {
                            drawCanvas.dots.push(",");
                            drawCanvas.dots.push(",");
                        }
                    }
                }

            }
        }

        RowLayout {
            CDGitManager {
                id: gitMan
            }

            CDCancelButton {
                text: "Cancel Singature"
                Material.accent: Material.Red
                icon.name: "dialog-cancel"
                onClicked: consentDialog.close()
            }

            Label {
                Layout.fillWidth: true
            }

            CDAddButton {
                text: "Complete Signature"
                icon.name: "dialog-ok"
                onClicked: {
                    var signURL = m_fileLocs.getConsentImageFile(PATIENT_FILE_NAME, new Date(), consentString);
                    drawCanvas.save(signURL);
                    gitMan.commitData("Added consent for " + PATIENT_FILE_NAME);
                    consentDialog.close();
                }

                enabled: drawCanvas.dotlength > 10
            }
        }
    }

}
