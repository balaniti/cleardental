// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

QtObject {

    property string eds_EDI_CLAIM_URL: "https://web2.edsedi.com/api/claims" //add in "/{claimID}" for specific claim
    property string eds_EDI_NEW_CLAIM_URL: "https://web2.edsedi.com/api/claims/new"

    property var m_locs: CDFileLocations{}
    property var patPersonalSettings: Settings {}
    property var loadInsSettings:  Settings {}
    property var loadProviderSettings:  Settings {}
    property var locPrefsSettings: Settings {
        fileName: m_locs.getLocalPracticePreferenceFile()
        category: "ClaimsInfo"
    }

    function generateNEW_EDSEDI_JSON_string(getPatID, getList, isPreEstimate=false, sendToSecondary=false,
                                        authorizePayment=true,sendPerio=false,imageArray=[]) {
        var claimCounter = locPrefsSettings.value("ClaimCounter",0);
        claimCounter++;
        locPrefsSettings.setValue("ClaimCounter",claimCounter);
        var sendMe = ({
                          "providerEntityType":"Group",
                          "claimTransactionType":"Statement",
                          "claimEpsdt":"N",
                          "eligibleForEdsAttach":"Yes",
                          "diagnosisCodeQualifier": null,
                          "eligibleForNeaAttach":null,
                          "accidentOccupational": null,
                          "accidentAuto": null,
                          "accidentOther": null,
                          "accidentDate": null,
                          "autoAccidentState": null,
                          "isOrthodontics": null,
                      });
        sendMe["claimId"] =claimCounter;
        sendMe["importDate"] =new Date();
        loadInsSettings.fileName = m_locs.getDentalPlansFile(getPatID);


        //Load up all of the patient's personal information now
        patPersonalSettings.fileName = m_locs.getPersonalFile(getPatID);

        patPersonalSettings.category = "Address";
        var patCity = patPersonalSettings.value("City","");
        var patState = patPersonalSettings.value("State","");
        var patAddr1 = patPersonalSettings.value("StreetAddr1","");
        var patAddr2 = patPersonalSettings.value("StreetAddr2","");
        var patZip = patPersonalSettings.value("Zip","");

        patPersonalSettings.category = "Name";
        var patFirst = patPersonalSettings.value("FirstName","");
        var patLast = patPersonalSettings.value("LastName","");

        patPersonalSettings.category = "Personal";
        var patDOB = patPersonalSettings.value("DateOfBirth",""); // "month/day/year"
        var patSex = patPersonalSettings.value("Sex","")[0];

        //Primary Ins Info
        loadInsSettings.category = "Primary";

        sendMe["payerId"] = loadInsSettings.value("ClearingHouseID","");
        sendMe["carrierName"] = loadInsSettings.value("Name","");
        sendMe["carrierAddress1"] = loadInsSettings.value("Addr","");
        sendMe["carrierAddress2"] = "";
        sendMe["carrierCity"] = loadInsSettings.value("City","");
        sendMe["carrierState"] = loadInsSettings.value("State","");
        sendMe["carrierZip"] = loadInsSettings.value("Zip","");

        //Secondary ins info
        loadInsSettings.category = "Secondary";

        var hasSecondary = JSON.parse( loadInsSettings.value("Dental",false) || loadInsSettings.value("Medical",false));
        if(hasSecondary) {
            sendMe["secondaryInsuredId"] = loadInsSettings.value("SubID","");
            sendMe["secondaryGroup"] = loadInsSettings.value("GroupID","");
            sendMe["secondaryCarrierName"] = loadInsSettings.value("Name","");
            sendMe["secondaryCarrierAddress1"] = loadInsSettings.value("Addr","");
            sendMe["secondaryCarrierAddress2"] = "";
            sendMe["secondaryCarrierCity"] = loadInsSettings.value("City","");
            sendMe["secondaryCarrierState"] = loadInsSettings.value("State","");
            sendMe["secondaryCarrierZip"] = loadInsSettings.value("Zip","");
            sendMe["secondaryEmployer"] = loadInsSettings.value("EmployerName","");
            sendMe["secondaryRelationshipToInsuredDescription"] = loadInsSettings.value("PolHolderRel","");

            if(sendMe["secondaryRelationshipToInsuredDescription"] === "Self") {
                sendMe["secondaryRelationshipToInsuredCode"] = 18;

                sendMe["secondaryInsuredLast"] = patLast;
                sendMe["secondaryInsuredFirst"] = patFirst;
                sendMe["secondaryInsuredSex"] = patSex;
                sendMe["secondaryInsuredDob"] = patDOB;
            }
            else {
                if(sendMe["secondaryRelationshipToInsuredDescription"] === "Spouse") {
                    sendMe["secondaryRelationshipToInsuredCode"] = "01";
                }
                else if(sendMe["secondaryRelationshipToInsuredDescription"] === "Dependent") {
                    sendMe["secondaryRelationshipToInsuredCode"] = 19;
                }
                else {
                    sendMe["secondaryRelationshipToInsuredCode"] = 29;
                }

                sendMe["secondaryInsuredLast"] = loadInsSettings.value("PolHolderLast","");
                sendMe["secondaryInsuredFirst"] = loadInsSettings.value("PolHolderFirst","");
                sendMe["secondaryInsuredSex"] = loadInsSettings.value("PolHolderGender","")[0];
                sendMe["secondaryInsuredDob"] = loadInsSettings.value("PolHolderDOB","");
            }
        }
        else {
            sendMe["secondaryInsuredId"] = "";
            sendMe["secondaryGroup"] = "";
            sendMe["secondaryCarrierName"] = "";
            sendMe["secondaryCarrierAddress1"] = "";
            sendMe["secondaryCarrierAddress2"] = "";
            sendMe["secondaryCarrierCity"] = "";
            sendMe["secondaryCarrierState"] = "";
            sendMe["secondaryCarrierZip"] = "";
            sendMe["secondaryEmployer"] = "";
            sendMe["secondaryRelationshipToInsuredDescription"] = "";
            sendMe["secondaryInsuredLast"] = "";
            sendMe["secondaryInsuredFirst"] = "";
            sendMe["secondaryInsuredSex"] = "";
            sendMe["secondaryInsuredDob"] = "";
        }


        //Subscriber info
        loadInsSettings.category = "Primary";
        sendMe["insuredId"] =  loadInsSettings.value("SubID","");
        sendMe["insuredGroupNumber"] =  loadInsSettings.value("GroupID","");
        sendMe["relationshipToInsuredDescription"] =  loadInsSettings.value("PolHolderRel","");
        if(sendMe["relationshipToInsuredDescription"] === "Self") {
            sendMe["relationshipToInsuredCode"] = 18;
            sendMe["insuredLast"] = patLast;
            sendMe["insuredFirst"] = patFirst;
            sendMe["insuredAddress1"] = patAddr1;
            sendMe["insuredAddress2"] = patAddr2;
            sendMe["insuredCity"] = patCity;
            sendMe["insuredState"] = patState;
            sendMe["insuredZip"] = patZip;
            sendMe["insuredSex"] = patSex;
            sendMe["insuredDob"] = patDOB;
        }
        else {
            if(sendMe["relationshipToInsuredDescription"] === "Spouse") {
                sendMe["relationshipToInsuredCode"] = "01";
            }
            else if(sendMe["relationshipToInsuredDescription"] === "Dependent") {
                sendMe["relationshipToInsuredCode"] = 19;
            }
            else {
                sendMe["relationshipToInsuredCode"] = 29;
            }

            sendMe["insuredLast"] = loadInsSettings.value("PolHolderLast","");
            sendMe["insuredFirst"] = loadInsSettings.value("PolHolderFirst","");
            sendMe["insuredAddress1"] = loadInsSettings.value("PolHolderAddress","");
            sendMe["insuredAddress2"] = "";
            sendMe["insuredCity"] = loadInsSettings.value("PolHolderCity","");
            sendMe["insuredState"] = loadInsSettings.value("PolHolderState","");
            sendMe["insuredZip"] = loadInsSettings.value("PolHolderZip","");
            sendMe["insuredSex"] = loadInsSettings.value("PolHolderGender","")[0];
            sendMe["insuredDob"] = loadInsSettings.value("PolHolderDOB","");
        }


        //patient info
        sendMe["patientLast"] = patLast;
        sendMe["patientFirst"] = patFirst;
        sendMe["patientDob"] = patDOB;
        sendMe["patientAddress1"] = patAddr1;
        sendMe["patientAddress2"] = patAddr2;
        sendMe["patientCity"] = patCity;
        sendMe["patientState"] = patState;
        sendMe["patientZip"] = patZip;
        sendMe["patientSex"] = patSex;
        sendMe["patientId"] = getPatID;

        //All the procedures
        var transactionArray = [];
        var claimTotal = 0;
        var providerID = "";
        for(var i=0;i<getList.length;i++) {
            var addMe = ({});
            var proDateObj = new Date(getList[i]["ProcedureDate"]);
            addMe["procedureDate"] =(proDateObj.getMonth()+1) + "/" + proDateObj.getDate() + "/" +
                    proDateObj.getFullYear();
            addMe["procedureCode"] = getList[i]["Procedure"]["DCode"];
            addMe["procedureDescription"] = getList[i]["Procedure"]["ProcedureName"];

            if("Tooth" in getList[i]["Procedure"]) {
                addMe["toothNumbers"] = getList[i]["Procedure"]["Tooth"];
            }

            addMe["quantity"] = 1;
            if(getList[i]["Procedure"]["BasePrice"].includes(".")) {
                addMe["transactionFee"] =  getList[i]["Procedure"]["BasePrice"];
            }
            else {
                addMe["transactionFee"] =  getList[i]["Procedure"]["BasePrice"] + ".00";
            }


            claimTotal+= parseFloat(getList[i]["Procedure"]["BasePrice"]);


            if("Surfaces" in getList[i]["Procedure"]) {
                addMe["toothSurfaces"] = getList[i]["Procedure"]["Surfaces"];
            }

            if("Location" in getList[i]["Procedure"]) {
                var procLoc = getList[i]["Procedure"]["Location"];
                addMe["oralCavityDescription"] = procLoc;
                if(procLoc === "AA") {
                    addMe["oralCavityCode"] = "00";
                } else if(procLoc === "UA") {
                    addMe["oralCavityCode"] = "01";
                } else if(procLoc === "LA") {
                    addMe["oralCavityCode"] = "02";
                } else if(procLoc === "UR") {
                    addMe["oralCavityCode"] = "10";
                } else if(procLoc === "UL") {
                    addMe["oralCavityCode"] = "20";
                } else if(procLoc === "LL") {
                    addMe["oralCavityCode"] = "30";
                } else if(procLoc === "LR") {
                    addMe["oralCavityCode"] = "40";
                }
            }
            else {
                addMe["oralCavityCode"] = "";
            }


            addMe["toothSystem"] = "JP";
            addMe["diagPointer"] = "";
            transactionArray.push(addMe);

            //Yes, technically if there were multiple providers, only the last one would be set
            //but I am going to assume that will be handled before the list is actually sent over
            providerID = getList[i]["ProviderUsername"];
        }
        sendMe["transactions"] = transactionArray;
        sendMe["claimTotal"] = claimTotal.toFixed(2).toString();

        var todayObj = new Date();
        var todayString = (todayObj.getMonth()+1) + "/" + todayObj.getDate() + "/" + todayObj.getFullYear();
        sendMe["patientSignature"] = "Signature on File";
        sendMe["patientSignatureDate"] = todayString;
        if(authorizePayment) {
            sendMe["authorizePaymentSignature"] = "Signature on File";
            sendMe["authorizePaymentDate"] = todayString;
        }
        else {
            sendMe["authorizePaymentSignature"] = "No";
        }


        sendMe["placeOfTreatment"] = 11;
        sendMe["enclosures"] = null;


        //Provider info
        loadProviderSettings.fileName =  m_locs.getDocInfoFile(providerID);
        locPrefsSettings.category= "GeneralInfo";
        sendMe["renderingProviderNpi"] = loadProviderSettings.value("NPINumb","");
        sendMe["renderingProviderLast"] =  loadProviderSettings.value("LastName","");
        sendMe["renderingProviderFirst"] = loadProviderSettings.value("FirstName","");
        sendMe["renderingLicenseNum"] = loadProviderSettings.value("LicenseID","");
        sendMe["renderingAddress1"] = locPrefsSettings.value("AddrLine1","");
        sendMe["renderingAddress2"] = locPrefsSettings.value("AddrLine2","");
        sendMe["renderingCity"] = locPrefsSettings.value("City","");
        sendMe["renderingState"] = locPrefsSettings.value("State","");
        sendMe["renderingZip"] = locPrefsSettings.value("Zip","");
        sendMe["renderingPhone"] = locPrefsSettings.value("PhoneNumber","");
        sendMe["renderingTaxonomy"] = locPrefsSettings.value("Taxonomy","");

        //Clinic info
        sendMe["billingProviderName"] = locPrefsSettings.value("PracticeName","");
        sendMe["locationName"] = locPrefsSettings.value("PracticeName","");
        sendMe["billingAddress1"] = locPrefsSettings.value("AddrLine1","");
        sendMe["billingAddress2"] = locPrefsSettings.value("AddrLine2","");
        sendMe["billingCity"] = locPrefsSettings.value("City","");
        sendMe["billingState"] = locPrefsSettings.value("State","");
        sendMe["billingZip"] = locPrefsSettings.value("Zip","");
        sendMe["billingProviderNpi"] = locPrefsSettings.value("PracticeNPI","");
        sendMe["billingTaxId"] = locPrefsSettings.value("FederalTaxID","");
        sendMe["billingLicenseNumber"] = loadProviderSettings.value("LicenseID","");
        sendMe["billingPhoneNumber"] = locPrefsSettings.value("PhoneNumber","");

        //console.debug(JSON.stringify(sendMe,null,"\t"));

        locPrefsSettings.category = "Accounts";

        var userName = locPrefsSettings.value("EDSEDIUsername","");
        var password = locPrefsSettings.value("EDSEDIPassword","");

        var xhr = new XMLHttpRequest();
        xhr.open("PUT", "https://web2.edsedi.com/api/claims/new",true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.setRequestHeader('Authorization','Basic ' + Qt.btoa(userName + ":" + password));
        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                var responseJSON= xhr.responseText;
                var responseObj = JSON.parse(responseJSON);
                console.debug(JSON.stringify(responseObj));
            }
        }
        xhr.send(JSON.stringify(sendMe));
    }

    //TODO: Remove this whole function!
    function generateEDSEDI_JSON_string(getPatID, getList, isPreEstimate=false, sendToSecondary=false,
                                        authorizePayment=true,sendPerio=false,imageArray=[]) {
        var claimCounter = locPrefsSettings.value("ClaimCounter",0);
        claimCounter++;
        locPrefsSettings.setValue("ClaimCounter",claimCounter);
        var sendMe = ({
                          "providerEntityType":"Group",
                          "claimTransactionType":"Statement",
                          "claimEpsdt":"N",
                          "eligibleForEdsAttach":"Yes",
                          "diagnosisCodeQualifier": null,
                          "eligibleForNeaAttach":null,
                          "accidentOccupational": null,
                          "accidentAuto": null,
                          "accidentOther": null,
                          "accidentDate": null,
                          "autoAccidentState": null,
                          "isOrthodontics": null,
                      });
        sendMe["claimId"] =claimCounter;
        sendMe["importDate"] =new Date();
        loadInsSettings.fileName = m_locs.getDentalPlansFile(getPatID);


        //Load up all of the patient's personal information now
        patPersonalSettings.fileName = m_locs.getPersonalFile(getPatID);

        patPersonalSettings.category = "Address";
        var patCity = patPersonalSettings.value("City","");
        var patState = patPersonalSettings.value("State","");
        var patAddr1 = patPersonalSettings.value("StreetAddr1","");
        var patAddr2 = patPersonalSettings.value("StreetAddr2","");
        var patZip = patPersonalSettings.value("Zip","");

        patPersonalSettings.category = "Name";
        var patFirst = patPersonalSettings.value("FirstName","");
        var patLast = patPersonalSettings.value("LastName","");

        patPersonalSettings.category = "Personal";
        var patDOB = patPersonalSettings.value("DateOfBirth",""); // "month/day/year"
        var patSex = patPersonalSettings.value("Sex","")[0];

        //Primary Ins Info
        loadInsSettings.category = "Primary";

        sendMe["payerId"] = loadInsSettings.value("ClearingHouseID","");
        sendMe["carrierName"] = loadInsSettings.value("Name","");
        sendMe["carrierAddress1"] = loadInsSettings.value("Addr","");
        sendMe["carrierAddress2"] = "";
        sendMe["carrierCity"] = loadInsSettings.value("City","");
        sendMe["carrierState"] = loadInsSettings.value("State","");
        sendMe["carrierZip"] = loadInsSettings.value("Zip","");

        //Secondary ins info
        loadInsSettings.category = "Secondary";

        var hasSecondary = JSON.parse( loadInsSettings.value("Dental",false) || loadInsSettings.value("Medical",false));
        if(hasSecondary) {
            sendMe["secondaryInsuredId"] = loadInsSettings.value("SubID","");
            sendMe["secondaryGroup"] = loadInsSettings.value("GroupID","");
            sendMe["secondaryCarrierName"] = loadInsSettings.value("Name","");
            sendMe["secondaryCarrierAddress1"] = loadInsSettings.value("Addr","");
            sendMe["secondaryCarrierAddress2"] = "";
            sendMe["secondaryCarrierCity"] = loadInsSettings.value("City","");
            sendMe["secondaryCarrierState"] = loadInsSettings.value("State","");
            sendMe["secondaryCarrierZip"] = loadInsSettings.value("Zip","");
            sendMe["secondaryEmployer"] = loadInsSettings.value("EmployerName","");
            sendMe["secondaryRelationshipToInsuredDescription"] = loadInsSettings.value("PolHolderRel","");

            if(sendMe["secondaryRelationshipToInsuredDescription"] === "Self") {
                sendMe["secondaryRelationshipToInsuredCode"] = 18;

                sendMe["secondaryInsuredLast"] = patLast;
                sendMe["secondaryInsuredFirst"] = patFirst;
                sendMe["secondaryInsuredSex"] = patSex;
                sendMe["secondaryInsuredDob"] = patDOB;
            }
            else {
                if(sendMe["secondaryRelationshipToInsuredDescription"] === "Spouse") {
                    sendMe["secondaryRelationshipToInsuredCode"] = "01";
                }
                else if(sendMe["secondaryRelationshipToInsuredDescription"] === "Dependent") {
                    sendMe["secondaryRelationshipToInsuredCode"] = 19;
                }
                else {
                    sendMe["secondaryRelationshipToInsuredCode"] = 29;
                }

                sendMe["secondaryInsuredLast"] = loadInsSettings.value("PolHolderLast","");
                sendMe["secondaryInsuredFirst"] = loadInsSettings.value("PolHolderFirst","");
                sendMe["secondaryInsuredSex"] = loadInsSettings.value("PolHolderGender","")[0];
                sendMe["secondaryInsuredDob"] = loadInsSettings.value("PolHolderDOB","");
            }
        }
        else {
            sendMe["secondaryInsuredId"] = "";
            sendMe["secondaryGroup"] = "";
            sendMe["secondaryCarrierName"] = "";
            sendMe["secondaryCarrierAddress1"] = "";
            sendMe["secondaryCarrierAddress2"] = "";
            sendMe["secondaryCarrierCity"] = "";
            sendMe["secondaryCarrierState"] = "";
            sendMe["secondaryCarrierZip"] = "";
            sendMe["secondaryEmployer"] = "";
            sendMe["secondaryRelationshipToInsuredDescription"] = "";
            sendMe["secondaryInsuredLast"] = "";
            sendMe["secondaryInsuredFirst"] = "";
            sendMe["secondaryInsuredSex"] = "";
            sendMe["secondaryInsuredDob"] = "";
        }


        //Subscriber info
        loadInsSettings.category = "Primary";
        sendMe["insuredId"] =  loadInsSettings.value("SubID","");
        sendMe["insuredGroupNumber"] =  loadInsSettings.value("GroupID","");
        sendMe["relationshipToInsuredDescription"] =  loadInsSettings.value("PolHolderRel","");
        if(sendMe["relationshipToInsuredDescription"] === "Self") {
            sendMe["relationshipToInsuredCode"] = 18;
            sendMe["insuredLast"] = patLast;
            sendMe["insuredFirst"] = patFirst;
            sendMe["insuredAddress1"] = patAddr1;
            sendMe["insuredAddress2"] = patAddr2;
            sendMe["insuredCity"] = patCity;
            sendMe["insuredState"] = patState;
            sendMe["insuredZip"] = patZip;
            sendMe["insuredSex"] = patSex;
            sendMe["insuredDob"] = patDOB;
        }
        else {
            if(sendMe["relationshipToInsuredDescription"] === "Spouse") {
                sendMe["relationshipToInsuredCode"] = "01";
            }
            else if(sendMe["relationshipToInsuredDescription"] === "Dependent") {
                sendMe["relationshipToInsuredCode"] = 19;
            }
            else {
                sendMe["relationshipToInsuredCode"] = 29;
            }

            sendMe["insuredLast"] = loadInsSettings.value("PolHolderLast","");
            sendMe["insuredFirst"] = loadInsSettings.value("PolHolderFirst","");
            sendMe["insuredAddress1"] = loadInsSettings.value("PolHolderAddress","");
            sendMe["insuredAddress2"] = "";
            sendMe["insuredCity"] = loadInsSettings.value("PolHolderCity","");
            sendMe["insuredState"] = loadInsSettings.value("PolHolderState","");
            sendMe["insuredZip"] = loadInsSettings.value("PolHolderZip","");
            sendMe["insuredSex"] = loadInsSettings.value("PolHolderGender","")[0];
            sendMe["insuredDob"] = loadInsSettings.value("PolHolderDOB","");
        }


        //patient info
        sendMe["patientLast"] = patLast;
        sendMe["patientFirst"] = patFirst;
        sendMe["patientDob"] = patDOB;
        sendMe["patientAddress1"] = patAddr1;
        sendMe["patientAddress2"] = patAddr2;
        sendMe["patientCity"] = patCity;
        sendMe["patientState"] = patState;
        sendMe["patientZip"] = patZip;
        sendMe["patientSex"] = patSex;
        sendMe["patientId"] = getPatID;

        //All the procedures
        var transactionArray = [];
        var claimTotal = 0;
        var providerID = "";
        for(var i=0;i<getList.length;i++) {
            var addMe = ({});
            var proDateObj = new Date(getList[i]["ProcedureDate"]);
            addMe["procedureDate"] =(proDateObj.getMonth()+1) + "/" + proDateObj.getDate() + "/" +
                    proDateObj.getFullYear();
            addMe["procedureCode"] = getList[i]["Procedure"]["DCode"];
            addMe["procedureDescription"] = getList[i]["Procedure"]["ProcedureName"];

            if("Tooth" in getList[i]["Procedure"]) {
                addMe["toothNumbers"] = getList[i]["Procedure"]["Tooth"];
            }

            addMe["quantity"] = 1;
            if(getList[i]["Procedure"]["BasePrice"].includes(".")) {
                addMe["transactionFee"] =  getList[i]["Procedure"]["BasePrice"];
            }
            else {
                addMe["transactionFee"] =  getList[i]["Procedure"]["BasePrice"] + ".00";
            }


            claimTotal+= parseFloat(getList[i]["Procedure"]["BasePrice"]);


            if("Surfaces" in getList[i]["Procedure"]) {
                addMe["toothSurfaces"] = getList[i]["Procedure"]["Surfaces"];
            }

            if("Location" in getList[i]["Procedure"]) {
                var procLoc = getList[i]["Procedure"]["Location"];
                addMe["oralCavityDescription"] = procLoc;
                if(procLoc === "AA") {
                    addMe["oralCavityCode"] = "00";
                } else if(procLoc === "UA") {
                    addMe["oralCavityCode"] = "01";
                } else if(procLoc === "LA") {
                    addMe["oralCavityCode"] = "02";
                } else if(procLoc === "UR") {
                    addMe["oralCavityCode"] = "10";
                } else if(procLoc === "UL") {
                    addMe["oralCavityCode"] = "20";
                } else if(procLoc === "LL") {
                    addMe["oralCavityCode"] = "30";
                } else if(procLoc === "LR") {
                    addMe["oralCavityCode"] = "40";
                }
            }
            else {
                addMe["oralCavityCode"] = "";
            }


            addMe["toothSystem"] = "JP";
            addMe["diagPointer"] = "";
            transactionArray.push(addMe);

            //Yes, technically if there were multiple providers, only the last one would be set
            //but I am going to assume that will be handled before the list is actually sent over
            providerID = getList[i]["ProviderUnixID"];
        }
        sendMe["transactions"] = transactionArray;
        sendMe["claimTotal"] = claimTotal.toFixed(2).toString();

        var todayObj = new Date();
        var todayString = (todayObj.getMonth()+1) + "/" + todayObj.getDate() + "/" + todayObj.getFullYear();
        sendMe["patientSignature"] = "Signature on File";
        sendMe["patientSignatureDate"] = todayString;
        if(authorizePayment) {
            sendMe["authorizePaymentSignature"] = "Signature on File";
            sendMe["authorizePaymentDate"] = todayString;
        }
        else {
            sendMe["authorizePaymentSignature"] = "No";
        }


        sendMe["placeOfTreatment"] = 11;
        sendMe["enclosures"] = null;


        //Provider info
        loadProviderSettings.fileName =  m_locs.getDocInfoFile(providerID);
        locPrefsSettings.category= "GeneralInfo";
        sendMe["renderingProviderNpi"] = loadProviderSettings.value("NPINumb","");
        sendMe["renderingProviderLast"] =  loadProviderSettings.value("LastName","");
        sendMe["renderingProviderFirst"] = loadProviderSettings.value("FirstName","");
        sendMe["renderingLicenseNum"] = loadProviderSettings.value("LicenseID","");
        sendMe["renderingAddress1"] = locPrefsSettings.value("AddrLine1","");
        sendMe["renderingAddress2"] = locPrefsSettings.value("AddrLine2","");
        sendMe["renderingCity"] = locPrefsSettings.value("City","");
        sendMe["renderingState"] = locPrefsSettings.value("State","");
        sendMe["renderingZip"] = locPrefsSettings.value("Zip","");
        sendMe["renderingPhone"] = locPrefsSettings.value("PhoneNumber","");
        sendMe["renderingTaxonomy"] = locPrefsSettings.value("Taxonomy","");

        //Clinic info
        sendMe["billingProviderName"] = locPrefsSettings.value("PracticeName","");
        sendMe["locationName"] = locPrefsSettings.value("PracticeName","");
        sendMe["billingAddress1"] = locPrefsSettings.value("AddrLine1","");
        sendMe["billingAddress2"] = locPrefsSettings.value("AddrLine2","");
        sendMe["billingCity"] = locPrefsSettings.value("City","");
        sendMe["billingState"] = locPrefsSettings.value("State","");
        sendMe["billingZip"] = locPrefsSettings.value("Zip","");
        sendMe["billingProviderNpi"] = locPrefsSettings.value("PracticeNPI","");
        sendMe["billingTaxId"] = locPrefsSettings.value("FederalTaxID","");
        sendMe["billingLicenseNumber"] = loadProviderSettings.value("LicenseID","");
        sendMe["billingPhoneNumber"] = locPrefsSettings.value("PhoneNumber","");

        console.debug(JSON.stringify(sendMe,null,"\t"));

        locPrefsSettings.category = "Accounts";

        var userName = locPrefsSettings.value("EDSEDIUsername","");
        var password = locPrefsSettings.value("EDSEDIPassword","");

        var xhr = new XMLHttpRequest();
        xhr.open("PUT", "https://web2.edsedi.com/api/claims/new",true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.setRequestHeader('Authorization','Basic ' + Qt.btoa(userName + ":" + password));
        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                var responseJSON= xhr.responseText;
                var responseObj = JSON.parse(responseJSON);
                console.debug(JSON.stringify(responseObj));
            }
        }
        xhr.send(JSON.stringify(sendMe));
    }
    
    function getEDSEDIClaimInfo(getClaimID) {
        locPrefsSettings.category = "Accounts";

        var userName = locPrefsSettings.value("EDSEDIUsername","");
        var password = locPrefsSettings.value("EDSEDIPassword","");

        //console.debug(eds_EDI_CLAIM_URL + "/?ClaimId="+getClaimID);

        var xhr = new XMLHttpRequest();
        xhr.open("GET", eds_EDI_CLAIM_URL + "?ClaimId="+getClaimID,true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.setRequestHeader('Authorization','Basic ' + Qt.btoa(userName + ":" + password));
        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                console.debug(xhr.responseText);
            }
        }
        xhr.send();
        
    }

    function getEDSEDIPayerList() {
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "https://web2.edsedi.com/eds/List_Payers",true);
        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                return xhr.responseText;
            }
        }
        xhr.send();
    }

}
