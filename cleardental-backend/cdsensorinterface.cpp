// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cdsensorinterface.h"

#define HANU_VENDOR 1633
#define HANU_PRODUCT 17408
#define HANU_SIZE_2_WIDTH 1300
#define HANU_SIZE_2_HEIGHT 1706
#define HANU_SIZE_1_WIDTH 1000
#define HANU_SIZE_1_HEIGHT 1506

#include <QDebug>

static CDSensorInterface *m_Instance = nullptr;

CDSensorInterface *CDSensorInterface::getInstance()
{
    if(m_Instance == nullptr) {
        m_Instance = new CDSensorInterface;
    }



    return m_Instance;
}

void CDSensorInterface::meetAndGreet()
{
    m_deviceList.clear();
    m_deviceSizes.clear();
    m_serialNumbers.clear();
    m_vendorTypes.clear();

    libusb_device **devs; //pointer to pointer of device, used to retrieve a list of devices
    libusb_context *ctx = nullptr; //a libusb session
    int r; //for return values
    ssize_t cnt; //holding number of devices in list
    r = libusb_init(&ctx); //initialize the library for the session we just declared
    if(r < 0) {
        //returnMe["Status"] = "Init Error";
    }
    //libusb_set_debug(ctx, 3); //set verbosity level to 3, as suggested in the documentation

    cnt = libusb_get_device_list(ctx, &devs); //get the list of devices
    if(cnt < 0) {
        //returnMe["Status"] = "Get Device Error";
    }
    //qDebug()<<cnt<<" Devices in list."<<endl;

    for(int i=0;i<cnt;i++) {
        libusb_device *dev = devs[i];
        libusb_device_descriptor desc;
        libusb_get_device_descriptor(dev, &desc);
        if((desc.idProduct == HANU_PRODUCT) && (desc.idVendor == HANU_VENDOR)) {
            QVariantMap info = getHanuDeviceInfo(dev);
            m_deviceList.append(dev);
            m_deviceSizes.append(info.value("SensorSize").toInt());
            m_serialNumbers.append(info.value("SerialNumber").toString());
            m_vendorTypes.append(CDSensorInterface::HANUMATSU);
        }
    }

    libusb_free_device_list(devs, 1); //free the list, unref the devices in it

    //libusb_exit(ctx); //needs to be called to end the

}

int CDSensorInterface::sensorCount()
{
    return m_deviceList.count();
}

int CDSensorInterface::getSize(int sensorIndex)
{
    return m_deviceSizes.at(sensorIndex);
}

int CDSensorInterface::getSensorWidth(int sensorIndex)
{
    int returnMe=-1;
    int sensorSize = m_deviceSizes[sensorIndex];
    if(m_vendorTypes.at(sensorIndex) == HANUMATSU) {
        if(sensorSize == 1) {
            returnMe = HANU_SIZE_1_WIDTH;
        }
        else if(sensorSize ==2) {
            returnMe = HANU_SIZE_2_WIDTH;
        }
    }

    return returnMe;
}

int CDSensorInterface::getSensorHeight(int sensorIndex)
{
    int returnMe=-1;
    int sensorSize = m_deviceSizes[sensorIndex];
    if(m_vendorTypes.at(sensorIndex) == HANUMATSU) {
        if(sensorSize == 1) {
            returnMe = HANU_SIZE_1_HEIGHT;
        }
        else if(sensorSize ==2) {
            returnMe = HANU_SIZE_2_HEIGHT;
        }
    }

    return returnMe;
}

QString CDSensorInterface::getSerialNumber(int index)
{
    return m_serialNumbers.at(index);
}

QVector<quint16> CDSensorInterface::requestBlackImage(int sensorIndex)
{
    QVector<quint16> returnMe;
    if(m_vendorTypes.at(sensorIndex) == HANUMATSU) {
        returnMe = getHanuBlackImage(sensorIndex);
    }
    return returnMe;
}

QVector<quint16> CDSensorInterface::requestWhiteImage(int sensorIndex)
{
    QVector<quint16> returnMe;
    if(m_vendorTypes.at(sensorIndex) == HANUMATSU) {
        returnMe = getHanuWhiteImage(sensorIndex);
    }
    return returnMe;
}


CDSensorInterface::CDSensorInterface(QObject *parent)
    : QObject{parent}
{

}

void CDSensorInterface::sendUSBData(QByteArray dataToSend, int endpoint, libusb_device_handle *dev_handle)
{
    int actual; //used to find out how many bytes were written

    //qDebug()<<"Writing Data..."<<Qt::endl;
    unsigned char* sendMeReal = (unsigned char*) dataToSend.data();
    int r = libusb_bulk_transfer(dev_handle,  (LIBUSB_ENDPOINT_OUT |  endpoint), sendMeReal, dataToSend.length(), &actual, 0);
    if(r == 0 && actual == dataToSend.length()) //we wrote the bytes successfully
        qDebug()<<"USB Write went well";
    else
        qDebug()<<"Write Error with actual being " << actual <<" bytes" << Qt::endl;
}

QByteArray CDSensorInterface::readUSBData(int endpoint, libusb_device_handle *dev_handle, int readBytes)
{
    QByteArray returnMe;
    int actual;

    unsigned char *realBackData = new unsigned char[readBytes]; //data to read

    libusb_bulk_transfer(dev_handle, (LIBUSB_ENDPOINT_IN | endpoint), realBackData, readBytes, &actual, 0);

    for(int i=0;i<actual;i++) {
        returnMe.append(realBackData[i]);
    }

    if(actual != readBytes) {
        qDebug()<<"Requested " << readBytes<< " but only got back " << realBackData;
    }

    return returnMe;
}

QVariantMap CDSensorInterface::getHanuDeviceInfo(libusb_device *dev)
{
    QVariantMap returnMe;

    libusb_device_handle *dev_handle; //a device handle

    libusb_open(dev,&dev_handle);


    //    if(dev_handle == nullptr)
    //        qDebug()<<"Cannot open device"<<endl;
    //    else
    //        qDebug()<<"Device Opened"<<endl;


    if(libusb_kernel_driver_active(dev_handle, 0) == 1) { //find out if kernel driver is attached
        //qDebug()<<"Kernel Driver Active"<<endl;
        if(libusb_detach_kernel_driver(dev_handle, 0) == 0) { //detach it
            //qDebug()<<"Kernel Driver Detached!"<<endl;
        }
    }

    int r = libusb_claim_interface(dev_handle, 0); //claim interface 0 (the first) of device (mine had jsut 1)
    if(r < 0) {
        //returnMe["Status"] = "Cannot Claim Interface";
        return returnMe;
    }
    //qDebug()<<"Claimed Interface"<<endl;

    QByteArray sendMe = QByteArray::fromHex("c80601b801b500007530abcd");

    sendUSBData(sendMe,2,dev_handle);

    QByteArray backData = readUSBData(6,dev_handle,16);

    QString hexDataBack = backData.toHex();

    QString firmware = hexDataBack.right(2);
    QString sensorType = hexDataBack.mid(14 * 2,2);
    QString lotNumber = hexDataBack.mid(11*2,2) + hexDataBack.mid(10*2,2);
    QString serialNumber = sensorType + lotNumber;
    returnMe["Firmware"] = firmware;
    returnMe["LotNumber"] = lotNumber;
    returnMe["SerialNumber"] = serialNumber;

    int intSensorVal = sensorType.toInt(nullptr,16);
    if(intSensorVal < 128) {
        returnMe["SensorSize"] = 1;
    }
    else {
        returnMe["SensorSize"] = 2;
    }

    r = libusb_release_interface(dev_handle, 0); //release the claimed interface
    if(r!=0) {
        //returnMe["Status"] = "Cannot Release Interface";
        return returnMe;
    }
    //qDebug()<<"Released Interface"<<endl;

    libusb_close(dev_handle); //close the device we opened

    //qDebug()<<returnMe;

    return returnMe;

}

QVector<quint16> CDSensorInterface::getHanuBlackImage(int deviceIndex)
{
    QVector<quint16> returnMe;
    libusb_device_handle *dev_handle; //a device handle
    int error = libusb_open(m_deviceList.at(deviceIndex),&dev_handle);
    if(error) {
        qDebug()<<"Open error: " <<error;
    }


    libusb_claim_interface(dev_handle, 0); //claim interface 0 (the first) of device (mine had jsut 1)

    if(m_deviceSizes.at(deviceIndex) == 2) {
        QByteArray sendMe = QByteArray::fromHex("c80601bd01b500e4e1c0abcd"); //frame 1
        sendUSBData(sendMe,2,dev_handle);
        QByteArray backData = readUSBData(6,dev_handle,16); //frame 2

        sendMe = QByteArray::fromHex("c80601b801b500e4e1c0abcd"); //frame 3
        sendUSBData(sendMe,2,dev_handle);
        backData = readUSBData(6,dev_handle,16); //frame 4

        sendMe = QByteArray::fromHex("c80301b801b500e4e1c0abcd"); //frame 5
        sendUSBData(sendMe,2,dev_handle);
        backData = readUSBData(6,dev_handle,16); //frame6

        backData = readUSBData(6,dev_handle,4435616); //frame 7

        returnMe.resize(2217800);

        int realCounter =0;
        for(int i=16;i<4435616;i+=2) {
            quint8 valLow = backData.at(i);
            quint8 valHigh = backData.at(i+1);
            quint16 addMe = valLow + (256*valHigh);
            returnMe[realCounter] = addMe;
            realCounter++;
        }
    }
    else { //Sensor size 1
        QByteArray sendMe = QByteArray::fromHex("c80601bd01b500e4e1c0abcd"); //frame 1
        sendUSBData(sendMe,2,dev_handle);
        QByteArray backData = readUSBData(6,dev_handle,16); //frame 2

        sendMe = QByteArray::fromHex("c80601b801b500e4e1c0abcd"); //frame 3
        sendUSBData(sendMe,2,dev_handle);
        backData = readUSBData(6,dev_handle,16); //frame 4

        sendMe = QByteArray::fromHex("c80301b801b500e4e1c0abcd"); //frame 5
        sendUSBData(sendMe,2,dev_handle);
        backData = readUSBData(6,dev_handle,16); //frame6

        backData = readUSBData(6,dev_handle,3012016); //frame 7

        returnMe.resize(1506000);

        int realCounter =0;
        for(int i=16;i<3012016;i+=2) {
            quint8 valLow = backData.at(i);
            quint8 valHigh = backData.at(i+1);
            quint16 addMe = valLow + (256*valHigh);
            returnMe[realCounter] = addMe;
            realCounter++;
        }

    }

    libusb_release_interface(dev_handle, 0); //release the claimed interface
    libusb_close(dev_handle); //close the device we opened

    qDebug()<<"Got the black image for sensorIndex: " <<deviceIndex;

    return returnMe;

}

QVector<quint16> CDSensorInterface::getHanuWhiteImage(int deviceIndex)
{
    QVector<quint16> returnMe;

    libusb_device_handle *dev_handle; //a device handle

    int error = libusb_open(m_deviceList[deviceIndex],&dev_handle);
    qDebug()<<error;

    libusb_claim_interface(dev_handle, 0); //claim interface 0 (the first) of device (mine had jsut 1)

    if(m_deviceSizes[deviceIndex] == 2) {
        QByteArray sendMe = QByteArray::fromHex("c80601bd01b500e4e1c0abcd"); //frame 1
        sendUSBData(sendMe,2,dev_handle);
        QByteArray backData = readUSBData(6,dev_handle,16); //frame 2

        sendMe = QByteArray::fromHex("c80601b801b500e4e1c0abcd"); //frame 3
        sendUSBData(sendMe,2,dev_handle);
        backData = readUSBData(6,dev_handle,16); //frame 4

        sendMe = QByteArray::fromHex("c80101b801b500e4e1c0abcd"); //frame 5
        sendUSBData(sendMe,2,dev_handle);
        backData = readUSBData(6,dev_handle,16); //frame6

        backData = readUSBData(6,dev_handle,4435616); //frame 7 and 8

        for(int i=16;i<4435616;i+=2) {
            quint8 valLow = backData.at(i);
            quint8 valHigh = backData.at(i+1);
            quint16 addMe = valLow + (256*valHigh);
            returnMe.append(addMe);
        }
    }
    else { //Sensor size 1
        QByteArray sendMe = QByteArray::fromHex("c80601bd01b500e4e1c0abcd"); //frame 1
        sendUSBData(sendMe,2,dev_handle);
        QByteArray backData = readUSBData(6,dev_handle,16); //frame 2

        sendMe = QByteArray::fromHex("c80601b801b500e4e1c0abcd"); //frame 3
        sendUSBData(sendMe,2,dev_handle);
        backData = readUSBData(6,dev_handle,16); //frame 4

        sendMe = QByteArray::fromHex("c80101b801b500e4e1c0abcd"); //frame 5
        sendUSBData(sendMe,2,dev_handle);
        backData = readUSBData(6,dev_handle,16); //frame6

        backData = readUSBData(6,dev_handle,3012016); //frame 7

        for(int i=16;i<3012016;i+=2) {
            quint8 valLow = backData.at(i);
            quint8 valHigh = backData.at(i+1);
            quint16 addMe = valLow + (256*valHigh);
            returnMe.append(addMe);
        }

    }

    libusb_release_interface(dev_handle, 0); //release the claimed interface
    libusb_close(dev_handle); //close the device we opened

    return returnMe;

}
