// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

QtObject {

    property string baseURL: "https://www.dentalxchange.com/"

    property var m_locs: CDFileLocations{}
    property var patPersonalSettings: Settings {}
    property var loadInsSettings:  Settings {}
    property var loadProviderSettings:  Settings {}
    property var locPrefsSettings: Settings {
        fileName: m_locs.getLocalPracticePreferenceFile()
        category: "ClaimsInfo"
    }

    function convertTaxonomyToDentalXChangeType(getTaxo) {
        if(getTaxo==="1223G0001X") { //General Practice
            return "GP";
        }
        else if(getTaxo==="1223D0001X") { //Dental Public Health
            return "PUBLIC";
        }
        else if(getTaxo==="1223E0200X") { //Endodontics
            return "ENDO";
        }
        else if(getTaxo==="1223X0400X") { //Orthodontics
            return "ORTHO";
        }
        else if(getTaxo==="1223P0221X") { //Pediatric
            return "PEDI";
        }
        else if(getTaxo==="1223P0300X") { //Periodontics
            return "PERI";
        }
        else if(getTaxo==="1223P0700X") { //Prosthodontics
            return "PROS";
        }
        else if(getTaxo==="1223P0106X") { //Pathology
            return "ORALMAXP";
        }
        else if(getTaxo==="1223D0008X") { //Radiology
            return "ORALMAXR";
        }
        else if(getTaxo==="1223S0112X") { //Surgery
            return "ORALMAX1";
        }
        else { //??????
            return "DENT" //I actually have no clue what it even means....
        }
    }

    //https://developer.dentalxchange.com/claim-api#tag/Claim/operation/claims-submit
    function dentalXchangeSubmitClaim(getPatID, getProcedureList, getProviderID, isPreEstimate=false,
                                      sendToSecondary=false, authorizePayment=true,sendPerio=false,imageArray=[]) {

        locPrefsSettings.category = "Accounts";
        var userName = locPrefsSettings.value("DentalXChangeUsername","");
        var password = locPrefsSettings.value("DentalXChangePassword","");

        loadProviderSettings.filename = m_locs.getDocInfoFile(getProviderID);


        locPrefsSettings.category = "GeneralInfo";
        var taxo = locPrefsSettings.value("Taxonomy","1223G0001X");

        var sendMe = ({});

        var sendClaim = ({});

        var providers = [];

        var billingProvider = ({}); //we will assume billing = practice
        billingProvider["type"] = "BILLING";
        billingProvider["specialty"] = convertTaxonomyToDentalXChangeType(taxo);
        billingProvider["taxId"] = locPrefsSettings.value("FederalTaxID","1234567890");
        billingProvider["billingNpi"] = locPrefsSettings.value("PracticeNPI"," ");

        var billingProviderAddress = ({});
        billingProviderAddress["address1"] = locPrefsSettings.value("AddrLine1"," ");
        billingProviderAddress["address2"] = locPrefsSettings.value("AddrLine2"," ");
        billingProviderAddress["city"] = locPrefsSettings.value("City"," ");
        billingProviderAddress["state"] = locPrefsSettings.value("State"," ");
        billingProviderAddress["zipCode"] = locPrefsSettings.value("Zip"," ");
        billingProviderAddress["entityType"] = 2; //1 = Individual; 2= Organization

        billingProvider["addresses"] = [billingProviderAddress];
        providers.push(billingProvider);

        var renderingProvider = ({});
        renderingProvider["type"] = "RENDERING";
        renderingProvider["specialty"] = convertTaxonomyToDentalXChangeType(loadProviderSettings.value("Taxonomy",""));
        renderingProvider["licenseNumber"] =  loadProviderSettings.value("LicenseID"," ")
    }

}
