// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

RowLayout {
    id: copyLabel
    property alias text: tArea.text
    property alias readOnly: tArea.readOnly
    property alias placeholderText: tArea.placeholderText
    property alias inputMethodHints: tArea.inputMethodHints
    property alias strikeout: tArea.font.strikeout

    TextField {
        id: tArea
        text: copyLabel.text
        Layout.fillWidth: true
        //selectByMouse: true
    }
    Button {
        icon.name: "edit-copy"
        text: "Copy"
        onClicked: {
            tArea.selectAll();
            tArea.copy();
        }
    }
}
