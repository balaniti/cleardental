// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cdradiographmanager.h"

#include <QDir>
#include <QDebug>
#include <QSettings>
#include <QList>

#include "cddefaults.h"
#include "cdfilelocations.h"
#include "cdaquireradiograph.h"

CDRadiographManager::CDRadiographManager(QObject *parent) : QObject(parent)
{

}

QStringList CDRadiographManager::getRadiographDates(QString patName)
{
    QStringList returnMe;
    QList<QDate> sortMe;
    QString basePath = CDFileLocations::getRadiographDir(patName);
    QDir radioDir(basePath);

    QStringList months = radioDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
    foreach(QString month, months) {
        QDir monthDir(basePath + "/" + month);
        QStringList days = monthDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
        foreach(QString day, days) {
            QDir dayDir(basePath + "/" + month + "/" + day);
            QStringList years = dayDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
            foreach(QString year, years) {
                QString addString = month + "-" + day + "-" + year;
                QDate addMe = QDate::fromString(addString,"MMM-d-yyyy");
                sortMe.append(addMe);
            }
        }
    }

    std::sort(sortMe.begin(),sortMe.end(),dComp);

    foreach(QDate date,sortMe) {
        QString addMe = date.toString("MMM/d/yyyy");
        returnMe.append(addMe);
    }

    return returnMe;
}

QString CDRadiographManager::getBWRadiographDueDate(QString patName)
{
    QString returnMe;
    QList<QDate> sortMe;
    QString basePath = CDFileLocations::getRadiographDir(patName);
    QDir radioDir(basePath);

    QStringList months = radioDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
    foreach(QString month, months) {
        QDir monthDir(basePath + "/" + month);
        QStringList days = monthDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
        foreach(QString day, days) {
            QDir dayDir(basePath + "/" + month + "/" + day);
            QStringList years = dayDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
            foreach(QString year, years) {
                QDir yearDir(basePath + "/" + month + "/" + day + "/" + year);
                QStringList files = yearDir.entryList(QDir::Files | QDir::NoDotAndDotDot);
                int bwCount =0;
                foreach(QString file, files) {
                    if(file.contains("BW")) {
                        bwCount++;
                    }
                }
                if(bwCount > 1) { //Most dental plans say you need at least 2 BWs
                    QString addString = month + "-" + day + "-" + year;
                    QDate addMe = QDate::fromString(addString,"MMM-d-yyyy");
                    sortMe.append(addMe);
                }
            }
        }
    }

    if(sortMe.length() > 0) {
        std::sort(sortMe.begin(),sortMe.end(),dComp);
        QDate lastBW = sortMe.last();

        QSettings pracPref(CDFileLocations::getLocalPracticePreferenceFile(),QSettings::IniFormat);
        pracPref.beginGroup("Updates");
        int addMonths = pracPref.value("BWs",6).toInt();
        QDate dueDate = lastBW.addMonths(addMonths);
        QDate today = QDate::currentDate();
        if(dueDate > today) {
            returnMe = "Later (" + dueDate.toString("MMM/d/yyyy") + ")";
        }
        else {
            returnMe = "Now (Due " + dueDate.toString("MMM/d/yyyy") + ")";
        }

    }
    else {
        returnMe = "Now (No BWs Taken)";
    }

    return returnMe;

}

QStringList CDRadiographManager::getRadiographPaths(QString patName, QString date)
{
    QStringList returnMe;
    QString basePath = CDFileLocations::getRadiographDir(patName);
    QDir checkDir(basePath + "/" + date + "/");
    QStringList filenames = checkDir.entryList(QDir::Files | QDir::NoDotAndDotDot);
    foreach(QString fileName,filenames) {
        QString addMe = basePath + "/" + date + "/" + fileName;
        returnMe.append(addMe);
    }
    return returnMe;
}

QStringList CDRadiographManager::getRadiographsForTooth(QString patName, QString getTooth)
{
    return getRadiographsForTooth(patName,getTooth,false);
}

QStringList CDRadiographManager::getRadiographsForTooth(QString patName, QString getTooth, bool includeBad)
{
    QStringList returnMe;
    QList<QPair<QString,QString>> fullList = getAllRadiographImages(patName,true);
    QList<CDAquireRadiograph::RadiographType> filterList = CDAquireRadiograph::getTypesFromToothNumber(getTooth);

    QStringList strFilterList;
    foreach(CDAquireRadiograph::RadiographType radioType, filterList) {
        strFilterList.append(CDAquireRadiograph::getStringFromType(radioType));
    }

    for(int i=0;i<fullList.length();i++) {
        QPair<QString,QString> file = fullList.at(i);

        foreach(QString filterItem, strFilterList) {
            if(file.first.contains(filterItem)) {
                if(!includeBad) {
                    if(!file.first.contains(".bad")) {
                        returnMe.append(file.second);
                    }
                }
                else {
                    returnMe.append(file.second);
                }
            }
        }
    }

    return returnMe;
}

QStringList CDRadiographManager::getRadiographFromType(QString patName, int getType,
                                                       bool includeBad)
{
    QStringList returnMe;
    QList<QPair<QString,QString>> fullList = getAllRadiographImages(patName,true);
    CDAquireRadiograph::RadiographType typeEnum = (CDAquireRadiograph::RadiographType) getType;
    QString whatToLookFor = CDAquireRadiograph::getStringFromType(typeEnum);

    for(int i=0;i<fullList.length();i++) {
        QPair<QString,QString> file = fullList.at(i);
        if(file.first.contains(whatToLookFor)) {
            if(!includeBad) {
                if(!file.first.contains(".bad")) {
                    returnMe.append(file.second);
                }
            }
            else {
                returnMe.append(file.second);
            }
        }
    }

    return returnMe;
}

QStringList CDRadiographManager::getRadiographsForQuad(QString patName, CDRadiographManager::OralQuad getQuad)
{
    QStringList returnMe;

    QString basePath = CDFileLocations::getRadiographDir(patName);
    int start =0;
    int end =0;
    switch (getQuad) {
    case QUAD_UPPER_RIGHT:
        start = 1;
        end = 8;
        break;
    case QUAD_UPPER_LEFT:
        start = 9;
        end = 16;
        break;
    case QUAD_LOWER_LEFT:
        start = 17;
        end = 24;
        break;
    case QUAD_LOWER_RIGHT:
        start = 25;
        end = 32;
        break;
    }

    QList<CDAquireRadiograph::RadiographType> filterList;
    for(int i=start;i<=end;i++) {
        QString sendMe = QString::number(i);
        filterList.append(CDAquireRadiograph::getTypesFromToothNumber(sendMe));
    }
    QStringList strFilterList;
    foreach(CDAquireRadiograph::RadiographType radioType, filterList) {
        strFilterList.append(CDAquireRadiograph::getStringFromType(radioType) + ".png");
    }

    QList<QPair<QString,QString>> fullList = getAllRadiographImages(patName);
    for(int i=0;i<fullList.length();i++) {
        QPair<QString,QString> file = fullList.at(i);
        if(strFilterList.contains(file.first)) {
            returnMe.append(file.second);
        }
    }

    return returnMe;
}

bool CDRadiographManager::dComp(QDate left, QDate right)
{
    return left < right;

}

bool CDRadiographManager::dListComp(QVariantList left, QVariantList right)
{
    QDate dateLeft = left.at(0).toDate();
    QDate dateRight = right.at(0).toDate();
    return dateLeft > dateRight; //we actually want to go from latest radiograph to the first ever
}

QList<QPair<QString,QString>> CDRadiographManager::getAllRadiographImages(QString patName,bool sortResults)
{
    QString basePath = CDFileLocations::getRadiographDir(patName);
    QDir radioDir(basePath);
    QList<QPair<QString,QString>> returnMe;
    QList<QVariantList> sortList;
    QStringList months = radioDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
    foreach(QString month, months) {
        QDir monthDir(basePath + "/" + month);
        QStringList days = monthDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
        foreach(QString day, days) {
            QDir dayDir(basePath + "/" + month + "/" + day);
            QStringList years = dayDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
            foreach(QString year, years) {
                QDir yearDir(basePath + "/" + month + "/" + day + "/" + year);
                QStringList files = yearDir.entryList(QDir::Files | QDir::NoDotAndDotDot);
                foreach(QString file, files) {
                    QPair<QString,QString> addMe;
                    addMe.first = file;
                    addMe.second = yearDir.absolutePath() + "/" + file;
                    returnMe.append(addMe);

                    if(sortResults) {
                        QDate addDate = QDate::fromString(month + "/" + day + "/" + year,"MMM/d/yyyy");
                        QVariantList addToSort;
                        addToSort.append(addDate);
                        addToSort.append(addMe.first);
                        addToSort.append(addMe.second);
                        sortList.append(addToSort);
                    }
                }
            }
        }
    }

    if(sortResults) {
        std::sort(sortList.begin(),sortList.end(),dListComp);
        returnMe.clear();
        foreach(QVariantList item, sortList) {
            QPair<QString,QString> addMe;
            addMe.first = item.at(1).toString();
            addMe.second = item.at(2).toString();
            returnMe.append(addMe);
        }
    }

    return returnMe;
}
