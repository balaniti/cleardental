// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cdconstloader.h"

#include <QFile>
#include <QGuiApplication>

CDConstLoader::CDConstLoader(QObject *parent) : QObject(parent)
{

}

QString CDConstLoader::getDCodesJSON()
{
    QString returnMe = "";
    QFile dCodeReader(":/DCodes.json");
    dCodeReader.open(QIODevice::ReadOnly | QIODevice::Text);
    returnMe = dCodeReader.readAll();
    return returnMe;
}

QString CDConstLoader::getUSDentalPlanListJSON()
{
    QString returnMe = "";
    QFile dPlanReader(":/USDentalPlanList.json");
    dPlanReader.open(QIODevice::ReadOnly | QIODevice::Text);
    returnMe = dPlanReader.readAll();
    return returnMe;
}

QString CDConstLoader::getCommonPatTranslationsJSON()
{
    QString returnMe = "";
    QFile dPlanReader(":/PatientQuestionTranslations.json");
    dPlanReader.open(QIODevice::ReadOnly | QIODevice::Text);
    returnMe = dPlanReader.readAll();
    return returnMe;
}

QString CDConstLoader::getNodeType()
{
    QFile readNodeType("/nodeType.txt");
    QString returnMe = "";
    if(readNodeType.open(QIODevice::Text | QIODevice::ReadOnly)) {
        returnMe = readNodeType.readAll();
        readNodeType.close();
    }
    returnMe = returnMe.trimmed();
    return returnMe;
}

bool CDConstLoader::isDebugMode()
{
#ifdef QT_DEBUG
    return true;
#else
    return false;
#endif
}

bool CDConstLoader::goTransparent()
{
    return QGuiApplication::platformName() == "xcb";

}

