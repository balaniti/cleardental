// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import Qt.labs.settings 1.1

CDTranslucentPane {
    id: parentConsentPane
    backMaterialColor: Material.Purple

    function generateCaseNoteString() {
        var returnme = "";
        if(visible) {
            if(whoWithPatientBox.currentText == "Other") {
                returnme += "Patient presented with " + otherPersonTextField.text + ".\n";
            }
            else {
                returnme += "Patient presented with " + whoWithPatientBox.currentText + ".\n";
            }

            returnme+= "Behavior: " + behaviorBox.currentValue + "\n"

        }

        return returnme;
    }

    CDCommonFunctions {
        id: m_comFuns
    }

    ColumnLayout {
        CDHeaderLabel {
            text: "Pediatric"
        }

        RowLayout {
            CDDescLabel {
                text: "Patient presented with"
            }

            ComboBox {
                id: whoWithPatientBox
                model: ["Mother", "Father", "Guardian", "Other"]
            }
            TextField {
                id: otherPersonTextField

                opacity: (whoWithPatientBox.currentIndex ===3) ? 1:0
                Behavior on opacity {
                    PropertyAnimation {
                        duration: 300
                    }
                }
                Layout.minimumWidth: 300
            }
        }
        RowLayout {
            CDDescLabel {
                text: "Behavior"
            }
            ComboBox {
                id: behaviorBox
                model: ["Excellent","Very Good","Good","Fair","Poor / Aborted"]
            }
        }

        Label {
            Component.onCompleted: {
                if(!("Tooth" in procedureObj)) {
                    return;
                }
                var makeText = ""
                //console.debug(procedureObj["Tooth"])
                if(isNaN(procedureObj["Tooth"])) { //is pediatric
                    var charCode = procedureObj["Tooth"].toUpperCase().charCodeAt(0);
                    if(charCode <= 74) {
                        makeText = "Maxillary "
                        if(charCode <= 69) {
                            makeText+= "right "
                            if(charCode === 65) { //A
                                makeText += "2nd molar"
                            }
                            else if(charCode === 66) { //B
                                makeText += "1st molar"
                            }
                            else if(charCode === 67) { //C
                                makeText += "Canine"
                            }
                            else if(charCode === 68) { //D
                                makeText += "Lateral incisor"
                            }
                            else if(charCode === 69) { //E
                                makeText += "Central incisor"
                            }
                        }
                        else {
                            makeText += "left "
                            if(charCode === 70) { //F
                                makeText += "Central incisor"
                            }
                            else if(charCode === 71) { //G
                                makeText += "Lateral incisor"
                            }
                            else if(charCode === 72) { //H
                                makeText += "Canine"
                            }
                            else if(charCode === 73) { //I
                                makeText += "1st molar"
                            }
                            else if(charCode === 74) { //J
                                makeText += "2nd molar"
                            }
                        }
                    } else {
                        makeText = "Mandibular "
                        if(charCode <= 79) {
                            makeText+= "left "
                            if(charCode === 75) { //K
                                makeText += "2nd molar"
                            }
                            else if(charCode === 76) { //L
                                makeText += "1st molar"
                            }
                            else if(charCode === 77) { //M
                                makeText += "Canine"
                            }
                            else if(charCode === 78) { //N
                                makeText += "Lateral incisor"
                            }
                            else if(charCode === 79) { //O
                                makeText += "Central incisor"
                            }
                        }
                        else {
                            makeText += "right "
                            if(charCode === 80) { //P
                                makeText += "Central incisor"
                            }
                            else if(charCode === 81) { //Q
                                makeText += "Lateral incisor"
                            }
                            else if(charCode === 82) { //R
                                makeText += "Canine"
                            }
                            else if(charCode === 83) { //S
                                makeText += "1st molar"
                            }
                            else if(charCode === 84) { //T
                                makeText += "2nd molar"
                            }
                        }
                    }
                    text = makeText;
                }
            }
        }
    }

    Component.onCompleted: {
        var yearsOld = m_comFuns.getYearsOld(PATIENT_FILE_NAME);
        if(yearsOld < 18) {
            parentConsentPane.visible = true;
        }
        else {
            parentConsentPane.visible = false;
        }
    }
}
