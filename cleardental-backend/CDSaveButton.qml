// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12

Button {
    id: saveButton
    text: "Save"
    icon.name: "document-save"
    Material.accent: Material.LightBlue
    highlighted: true
    icon.width: 32
    icon.height: 32
    font.pointSize: 18

    function showSaveToolTip() {
        saveToolTip.show("Saved!", 3000);
    }

    ToolTip {
        id: saveToolTip
        anchors.centerIn: parent
        background: Rectangle {
            anchors.fill: parent
            color: Material.color(Material.LightGreen)
            radius: 10
        }
    }

}
