// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.14
import QtQuick.Controls.Material 2.12

Button {
    text: "Finish Procedure"
    Material.accent: Material.Lime
    icon.name: "dialog-ok"
    icon.width: 32
    icon.height: 32
    font.pointSize: 18
    highlighted: true
}

