// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTranslucentPane {
    id: newPatInfoPane
    property string patientID: "ignoreMe"
    property bool showDentalPlan: false

    backMaterialColor: Material.Blue

    CDFileLocations {
        id: m_fileLocs
    }

    Settings {
        id: m_newPatInt
    }

    ColumnLayout {
        anchors.fill: parent
        CDHeaderLabel {
            text: "New Patient Interview"
        }

        Flickable {
            Layout.fillWidth: true
            Layout.fillHeight: true
            clip: true
            contentHeight: infoCol.height

            ScrollBar.vertical: ScrollBar { }

            GridLayout {
                id: infoCol
                columns: 2
                columnSpacing: 20
                width: parent.width
            }
        }
    }

    Component {
        id: descLabel
        CDDescLabel {

        }
    }

    Component {
        id: regLabel
        CDCopyLabel {

        }
    }

    function addRow(strDesc, strValue) {
        if(strValue === undefined) {
            strValue = "";
        }

        var desLabObj = descLabel.createObject(infoCol);
        desLabObj.text = strDesc;

        var regLabObj = regLabel.createObject(infoCol);
        regLabObj.text = strValue;

    }

    onPatientIDChanged: {
        if(patientID === "ignoreMe") {
            return;
        }

        m_newPatInt.fileName = m_fileLocs.getNewPatInterviewFile(patientID);

        var inPain = JSON.parse(m_newPatInt.value("PatientInPain",false));
        if(inPain) {
            addRow("In Pain", "Yes");
            addRow("PainLength", m_newPatInt.value("PainLength"));
            addRow("HowSoon", m_newPatInt.value("HowSoon"));
            addRow("WhereFeel", m_newPatInt.value("WhereFeel"));
            addRow("WantSave", m_newPatInt.value("WantSave"));

        }

        else {
            addRow("In Pain", "No");
            addRow("Cc", m_newPatInt.value("Cc"));
            addRow("BracesBefore", m_newPatInt.value("BracesBefore"));
            addRow("DocIssue", m_newPatInt.value("DocIssue"));
            addRow("GetDone", m_newPatInt.value("GetDone"));
            addRow("GumBleed", m_newPatInt.value("GumBleed"));
            addRow("HadSCRP", m_newPatInt.value("HadSCRP"));
            addRow("HaveHeadaches", m_newPatInt.value("HaveHeadaches"));
            addRow("LastCleaning", m_newPatInt.value("LastCleaning"));
            addRow("NightGrind", m_newPatInt.value("NightGrind"));
            addRow("OftenBrush", m_newPatInt.value("OftenBrush"));
            addRow("OftenFloss", m_newPatInt.value("OftenFloss"));
            addRow("SleepIssue", m_newPatInt.value("SleepIssue"));
            addRow("WhereChip", m_newPatInt.value("WhereChip"));
            addRow("WhereSen", m_newPatInt.value("WhereSen"));
        }

        addRow("RefName", m_newPatInt.value("RefName"));
        addRow("RefSource", m_newPatInt.value("RefSource"));
        addRow("OtherInfoText", m_newPatInt.value("OtherInfoText"));

        //Prefs
        m_newPatInt.fileName = m_fileLocs.getPersonalFile(patientID);
        m_newPatInt.category = "Preferences";
        addRow("AvailableDays", m_newPatInt.value("AvailableDays"));
        addRow("AvailableTime", m_newPatInt.value("AvailableTime"));
        addRow("PreferredContact", m_newPatInt.value("PreferredContact"));

        //Email
        m_newPatInt.category = "Emails";
        addRow("Email", m_newPatInt.value("Email"));

        //Phone
        m_newPatInt.category = "Phones";
        addRow("CellPhone", m_newPatInt.value("CellPhone"));
        addRow("HomePhone", m_newPatInt.value("HomePhone"));

        m_newPatInt.category = "Work";
        addRow("WorkPhone", m_newPatInt.value("WorkPhone"));

        //Dental Plan
        if(showDentalPlan) {
            m_newPatInt.fileName = m_fileLocs.getDentalPlansFile(patientID);
            m_newPatInt.category = "Primary";
            addRow("Pri Dental Plan Name", m_newPatInt.value("Name"));
            addRow("Pri Dental Plan ID", m_newPatInt.value("SubID",""));
            m_newPatInt.category = "Secondary";
            addRow("Sec Dental Plan Name", m_newPatInt.value("Name"));

        }

    }

}
