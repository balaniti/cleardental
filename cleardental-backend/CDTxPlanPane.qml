// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0

CDTranslucentPane {
    backMaterialColor: Material.Yellow

    ColumnLayout {
        CDHeaderLabel {
            text: "Remaining Treatment"
        }
        Label {
            text: "<ul><li>None</li></ul>";
        }
    }
}
