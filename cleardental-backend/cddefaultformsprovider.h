// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef CDDEFAULTCONSENTPROVIDER_H
#define CDDEFAULTCONSENTPROVIDER_H

#include <QObject>

class CDDefaultFormsProvider : public QObject
{
    Q_OBJECT
public:
    explicit CDDefaultFormsProvider(QObject *parent = nullptr);
    Q_INVOKABLE static QString getDefaultConsent(QString consentName);
    Q_INVOKABLE static QString getDefaultPostOp(QString procedureName);

public slots:
};

#endif // CDDEFAULTCONSENTPROVIDER_H
