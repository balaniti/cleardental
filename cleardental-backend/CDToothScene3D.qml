// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick 2.2 as QQ2
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

import Qt3D.Core 2.0
import Qt3D.Render 2.15
import Qt3D.Input 2.0
import Qt3D.Extras 2.15
import QtQuick.Scene3D 2.14

Scene3D {
    id: toothScene3D

    property string toothNumb: ""
    property string baseSource: ""

    property bool mirrorMe: false
    property var existingList: []
    property var txPlanList: []
    property var chartingEntities: []

    property var m_comFuns: CDCommonFunctions {}




    function addCompsToTooth(getComp, getSurfaces, getIsTxPlan) {
        for(var i=0;i<getSurfaces.length;i++) {
            var compEnt = getComp.createObject(sceneRoot, { baseMeshSource: toothScene3D.baseSource,
                                                   toothAspect: getSurfaces[i],
                                                   isTxPlan: getIsTxPlan});
            chartingEntities.push(compEnt);
        }
    }


    function reloadInformation() {

        if(toothScene3D.baseSource.length < 1) {
            return; //no point in showing anything without a tooth Number
        }



        while(chartingEntities.length > 0) {
            chartingEntities.pop().destroy();
        }


        var component = Qt.createComponent("qrc:/CD3DToothEntity.qml");

        var baseTooth = component.createObject(sceneRoot, { baseMeshSource: toothScene3D.baseSource,
                                                   toothAspect: "", //the whole tooth, nothing but the tooth
                                                   setColor: "white"}); //maybe we should add in yellow teeth someday ;-)
        chartingEntities.push(baseTooth);

        //All the existing
        var i;
        var surfaces;
        var en2;
        var enf;
        for(i=0;i<existingList.length;i++) {
            var existAttr = existingList[i];
            if(existAttr === "Missing") {
                baseTooth.makeMissing = true;
            }
            else if(existAttr.startsWith("Composite")) {
                var compComp = Qt.createComponent("qrc:/CD3DCompMaterial.qml");
                surfaces = existAttr.split(" ")[1];
                addCompsToTooth(compComp,surfaces,false);
            }
            else if(existAttr.startsWith("Amalgam")) {
                var amalgamComp = Qt.createComponent("qrc:/CD3DAmalgamMaterial.qml");
                surfaces = existAttr.split(" ")[1];
                addCompsToTooth(amalgamComp,surfaces,false);
            }
            else if(existAttr.includes("Crown")) {
                var crownComp = Qt.createComponent("qrc:/CD3DCrownMaterial.qml");
                if(m_comFuns.isPost(toothNumb)) {
                    surfaces = "MODBLV";
                }
                else {
                    surfaces = "MIDFLV";
                }
                addCompsToTooth(crownComp,surfaces,false);
                en2= crownComp.createObject(sceneRoot, { baseMeshSource: toothScene3D.baseSource,
                                                toothAspect: "Enamel2",
                                                isTxPlan: false});
                chartingEntities.push(en2);
                enf= crownComp.createObject(sceneRoot, { baseMeshSource: toothScene3D.baseSource,
                                                toothAspect: "EnamelF",
                                                isTxPlan: false});
                chartingEntities.push(enf);
            }
            else if(existAttr.startsWith("Endodontically Treated")) {
                var rctComp = Qt.createComponent("qrc:/CD3DRCTMaterial.qml");
                enf= rctComp.createObject(sceneRoot, { baseMeshSource: toothScene3D.baseSource,
                                              toothAspect: "Cementum",
                                              isTxPlan: false});
                chartingEntities.push(enf);

            }
            else if(existAttr.startsWith("Impacted")) {
                var impactedComp = Qt.createComponent("qrc:/CD3DImpactedMaterial.qml");
                enf= impactedComp.createObject(sceneRoot, { });
                chartingEntities.push(enf);
            }
            else if(existAttr.startsWith("Sealant")) {
                var sealantComp = Qt.createComponent("qrc:/CD3DSealantMaterial.qml");
                enf= sealantComp.createObject(sceneRoot, { baseMeshSource: toothScene3D.baseSource,
                                                  toothAspect: "O",
                                                  isTxPlan: false });
                chartingEntities.push(enf);
            }
        }



        //All the tx planned items
        for(i=0;i<txPlanList.length;i++) {
            var procedureName = txPlanList[i]["ProcedureName"];
            if(procedureName.includes("Extraction")) {
                var extComp = Qt.createComponent("qrc:/CD3DExtractionEntity.qml");
                var extObj = extComp.createObject(sceneRoot,{});
                chartingEntities.push(extObj);
            }
            else if(procedureName.includes("Watch")) {
                var watchComp = Qt.createComponent("qrc:/CD3DWatchMaterial.qml");
                surfaces = txPlanList[i]["Surfaces"];
                addCompsToTooth(watchComp,surfaces,false);
            }

            else if(procedureName.includes("Composite")) {
                compComp = Qt.createComponent("qrc:/CD3DCompMaterial.qml");
                surfaces = txPlanList[i]["Surfaces"];
                addCompsToTooth(compComp,surfaces,true);
            }
            else if(procedureName.includes("Amalgam")) {
                amalgamComp = Qt.createComponent("qrc:/CD3DAmalgamMaterial.qml");
                surfaces = txPlanList[i]["Surfaces"];
                addCompsToTooth(amalgamComp,surfaces,true);
            }
            else if(procedureName.includes("Amalgam")) {
                amalgamComp = Qt.createComponent("qrc:/CD3DAmalgamMaterial.qml");
                surfaces = txPlanList[i]["Surfaces"];
                addCompsToTooth(amalgamComp,surfaces,true);
            }
            else if(procedureName.includes("Crown")) {
                crownComp = Qt.createComponent("qrc:/CD3DCrownMaterial.qml");
                if(m_comFuns.isPost(toothNumb)) {
                    surfaces = "MODBLV";
                }
                else {
                    surfaces = "MIDFLV";
                }
                addCompsToTooth(crownComp,surfaces,true);
                en2= crownComp.createObject(sceneRoot, { baseMeshSource: toothScene3D.baseSource,
                                                toothAspect: "Enamel2",
                                                isTxPlan: true});
                chartingEntities.push(en2);
                enf= crownComp.createObject(sceneRoot, { baseMeshSource: toothScene3D.baseSource,
                                                toothAspect: "EnamelF",
                                                isTxPlan: true});
                chartingEntities.push(enf);
            }
            else if(procedureName.includes("RCT")) {
                rctComp = Qt.createComponent("qrc:/CD3DRCTMaterial.qml");
                enf= rctComp.createObject(sceneRoot, { baseMeshSource: toothScene3D.baseSource,
                                              toothAspect: "Cementum",
                                              isTxPlan: true});
                chartingEntities.push(enf);
            }
            else if(procedureName.includes("Sealant")) {
                sealantComp = Qt.createComponent("qrc:/CD3DSealantMaterial.qml");
                enf= sealantComp.createObject(sceneRoot, { baseMeshSource: toothScene3D.baseSource,
                                                  toothAspect: "O",
                                                  isTxPlan: true });
                chartingEntities.push(enf);

            }
        }
    }

    transform: [
        Matrix4x4 {
            matrix: Qt.matrix4x4(mirrorMe ? -1 : 1, 0, 0, 0,
                                 0, 1, 0, 0,
                                 0, 0, 1, 0,
                                 0, 0, 0, 1)
        },
        Translate {
            x: mirrorMe ? toothScene3D.width : 1
        }
    ]

    onToothNumbChanged: {
        var intValOfTooth = parseInt(toothNumb);
        if(isNaN(intValOfTooth)) { //pediatric

            if( (toothNumb == "A") ||
                    (toothNumb == "B") ||
                    (toothNumb == "C") ||
                    (toothNumb == "D") ||
                    (toothNumb == "E") ||
                    (toothNumb == "P") ||
                    (toothNumb == "R") ||
                    (toothNumb == "S") ||
                    (toothNumb == "T") ){
                toothScene3D.baseSource = "qrc:/3dModels/tooth"+toothNumb+".obj";;
            }
            else if(toothNumb == "Q") {
                toothScene3D.baseSource = "qrc:/3dModels/toothP.obj";
            }
            else if( (toothNumb == "O") ||
                    (toothNumb == "N") ) {
                toothScene3D.baseSource = "qrc:/3dModels/toothP.obj";
                mirrorMe = true;
            }
            else {
                mirrorMe = true;
                if(toothNumb == "F") {
                    toothScene3D.baseSource = "qrc:/3dModels/toothE.obj";
                }
                else if(toothNumb == "G") {
                    toothScene3D.baseSource = "qrc:/3dModels/toothD.obj";
                }
                else if(toothNumb == "H") {
                    toothScene3D.baseSource = "qrc:/3dModels/toothC.obj";
                }
                else if(toothNumb == "I") {
                    toothScene3D.baseSource = "qrc:/3dModels/toothB.obj";
                }
                else if(toothNumb == "J") {
                    toothScene3D.baseSource = "qrc:/3dModels/toothA.obj";
                }
                else if(toothNumb == "K") {
                    toothScene3D.baseSource = "qrc:/3dModels/toothT.obj";
                }
                else if(toothNumb == "L") {
                    toothScene3D.baseSource = "qrc:/3dModels/toothS.obj";
                }
                else if(toothNumb == "M") {
                    toothScene3D.baseSource = "qrc:/3dModels/toothR.obj";
                }
            }
        }
        else { //adult
            if((intValOfTooth === 23) || (intValOfTooth === 26)) {
                toothScene3D.baseSource = "qrc:/3dModels/tooth25.obj";
            }
            else if(intValOfTooth >= 1 && intValOfTooth <= 8) {
                toothScene3D.baseSource = "qrc:/3dModels/tooth"+toothNumb+".obj";
            }
            else if(intValOfTooth >= 9 && intValOfTooth <= 16) {
                var newTop = 17-intValOfTooth;
                toothScene3D.baseSource = "qrc:/3dModels/tooth"+newTop+".obj";
                mirrorMe = true;
            }
            else if(intValOfTooth >= 17 && intValOfTooth <= 24) {
                var from25 = 25- intValOfTooth ;
                var newBottom = 24+from25;
                toothScene3D.baseSource = "qrc:/3dModels/tooth"+newBottom+".obj";
                mirrorMe = true;

            }
            else {
                toothScene3D.baseSource = "qrc:/3dModels/tooth"+toothNumb+".obj";
            }
        }

        reloadInformation();


    }

    Entity {
        id: sceneRoot



        Camera {
            id: camera
            projectionType: CameraLens.PerspectiveProjection
            fieldOfView: 45
            aspectRatio: toothScene3D.width / toothScene3D.height
            nearPlane : 0.1
            farPlane : 1000.0
            position: Qt.vector3d( 0.0, 0.0, 40.0 )
            upVector: Qt.vector3d( 0.0, 1.0, 0.0 )
            viewCenter: Qt.vector3d( 0.0, 0.0, 0.0 )
        }


        Entity {
            components: [
                DirectionalLight {
                    id: dLight
                    color: "white"
                    intensity: .4
                    worldDirection: Qt.vector3d(0,0,-1);

                }]
        }

        components: [
            RenderSettings {
                activeFrameGraph: ForwardRenderer {
                    clearColor: Qt.rgba(0, 0.0, 0, 0)
                    camera: camera
                }

            }
        ]


        Transform {
            id: toothTransform

            PropertyAnimation {
                duration: 50000
                from: 0
                to: (360)
                loops: Animation.Infinite
                target: toothTransform
                property: "rotationY"
                running: true
            }

        }

    }


}
