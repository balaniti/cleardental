// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0

Pane {

    property bool showMarcaine: false
    property bool showCarbocaine: true

    background: Rectangle {
        anchors.fill: parent
        radius: 10
        color: Material.color(Material.LightBlue)
        opacity: .5
    }

    function generateCaseNoteString() {
        var mLperCarpule = 1.7;
        var returnMe = "";
        var totalCarps = lidoBox.value + septoBox.value + carboBox.value +
                marBox.value;
        if(totalCarps === 0) {
            returnMe += "No local anesthetic was used.\n"
        }

        if(lidoBox.value > 0) {
            var lidoML = lidoBox.value * mLperCarpule;
            returnMe += lidoBox.value + " carpules ("+ lidoML +" mL) of 2% Lidocine with " +
                    "1:100,000 epinephrine was administered.\n"
        }

        if(septoBox.value > 0) {
            var septoML = septoBox.value * mLperCarpule;
            returnMe += septoBox.value + " carpules ("+ septoML +" mL) of 4% Septocaine with " +
                    "1:100,000 epinephrine was administered.\n"
        }

        if(carboBox.value > 0) {
            var carboML = carboBox.value * mLperCarpule;
            returnMe += carboBox.value + " carpules ("+ carboML +" mL) of 3% Carbocaine with " +
                    "no epinephrine was administered.\n"
        }

        if(marBox.value > 0) {
            var marML = marBox.value * mLperCarpule;
            returnMe += marBox.value + " carpules ("+ marML +" mL) of 0.5% Marcaine with " +
                    "1:200,000 epinephrine was administered.\n"
        }
        return returnMe;
    }

    GridLayout {
        columns: 2
        CDHeaderLabel {
            text: "Local Anesthetics"
            Layout.columnSpan: 2
        }
        Label {
            text: "2% Lidocaine with 1:100,000 epinephrine"
            font.bold: true
        }
        SpinBox {
            id: lidoBox
            from: 0
            to: 9
        }
        Label {
            text: "4% Septocaine with 1:100,000 epinephrine"
            font.bold: true
        }
        SpinBox {
            id: septoBox
            from: 0
            to: 9
        }
        Label {
            text: "3% Carbocaine"
            font.bold: true
        }
        SpinBox {
            id: carboBox
            from: 0
            to: 9
        }
        Label {
            text: "0.5% Marcaine with 1:200,000 epinephrine"
            font.bold: true
            visible: showMarcaine
        }
        SpinBox {
            id: marBox
            from: 0
            to: 9
            visible: showMarcaine
        }
    }
}
