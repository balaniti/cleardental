// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.14
import QtQuick.Controls.Material 2.12

Button {
    id: saveCloseButton
    text: "Save and Close"
    Material.accent: Material.Cyan
    icon.name: "document-save"
    icon.width: 32
    icon.height: 32
    font.pointSize: 18
    highlighted: true

    onVisibleChanged: {
        if(visible) {
            rootWin.keyboardSaveButton = saveCloseButton;
        }
    }

}
