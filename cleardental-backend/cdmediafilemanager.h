// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef CDMEDIAFILEMANAGER_H
#define CDMEDIAFILEMANAGER_H

#include <QObject>

class CDMediaFileManager : public QObject
{
    Q_OBJECT
public:
    explicit CDMediaFileManager(QObject *parent = nullptr);

    Q_INVOKABLE static QStringList getMusicTypes();
    Q_INVOKABLE static QStringList getMusicList(QString getType);
    Q_INVOKABLE static QStringList getMusicPics(QString getType);

    Q_INVOKABLE static QStringList getVideoTypes();
    Q_INVOKABLE static QStringList getVideoList(QString getType);

    Q_INVOKABLE static QStringList getSupportedGames();
    Q_INVOKABLE static void launchGame(QString gameName);

signals:

};

#endif // CDMEDIAFILEMANAGER_H
