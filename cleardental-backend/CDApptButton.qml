// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQml.Models 2.1
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1
import dental.clear 1.0

Button {
    id: schButton
    property string comments;
    property string patientID;
    property string procedures;
    property string providerID;
    property string status;
    property string fileName;
    property int duration;
    property var matColor;
    property string startTime;

    property string patDOB;
    property string cellPhoneNumber;
    property string homePhoneNumber;
    property string emailAddr;
    property string prefContactMethod;

    property var schButtonDia;

    property bool hasAlert: false
    property string schAlert: ""
    property string insName: ""

    Material.accent: ((status === "Left") || (status === "No Show")) ? Material.Grey : matColor
    opacity:  .75

    onPatientIDChanged: {
        iniReader.fileName = fLocs.getPersonalFile(patientID);
        iniReader.category = "Name"

        if(iniReader.value("PreferredName","").length > 1) {
            patLabel.text = "\"" + iniReader.value("PreferredName","") + "\" " +iniReader.value("LastName","")
        }

        else {
            patLabel.text = iniReader.value("FirstName","") + " " +iniReader.value("LastName","")
        }



        iniReader.category = "Personal";
        patDOB = iniReader.value("DateOfBirth","")

        iniReader.category = "Phones"
        cellPhoneNumber = iniReader.value("CellPhone","")
        homePhoneNumber = iniReader.value("HomePhone","")

        iniReader.category = "Emails"
        emailAddr = iniReader.value("Email","")

        iniReader.category = "Preferences"
        prefContactMethod = iniReader.value("PreferredContact","");
        if(iniReader.value("ScheduleAlert","").length > 0) {
            hasAlert = true;
            schAlert = iniReader.value("ScheduleAlert","");
        }

        var lang = iniReader.value("PreferredLanguage","en");

        if(lang === "pt") {
            languageFlagImage.source = "qrc:/flags/Flag_of_Brazil.png"
        }
        else if(lang === "es") {
            languageFlagImage.source = "qrc:/flags/Flag_of_Spain.png"
        }

        iniReader.fileName = fLocs.getDentalPlansFile(patientID);
        iniReader.category = "Primary";
        iniReader.sync();
        insName = iniReader.value("Name","");
    }

    onProviderIDChanged: {
        iniReader.fileName = fLocs.getDocInfoFile(providerID)
        iniReader.category = ""
        var setColor = iniReader.value("ProviderColor","?");
        matColor = Material.color( Material[setColor],Material.Shade50);
    }

    onStatusChanged: {
        if(status == "Blocker") {
            highlighted = false;
            opacity = .5;
        }
    }

    highlighted: true

    Settings {
        id: iniReader
    }

    Image {
        id: profilePic
        anchors.left: parent.left
        anchors.margins: 5
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        //width: Math.min(schButton.width,schButton.height)
        //height: width
        width: schButton.width/2
        fillMode: Image.PreserveAspectFit
        //opacity: .75
        visible: rootWin.viewMode === "Full"
        asynchronous: true
        cache: false
        source: patientID.length > 0 ? "file://" + fLocs.getProfileImageFile(patientID) : ""
    }

    Label {
        anchors.left: parent.left
        anchors.margins: 5
        anchors.bottom: parent.bottom
        text: insName.length > 1 ? insName : "Cash"
        color: Material.background
        style: Text.Outline
        styleColor: Material.foreground
        visible: (status != "Blocker") && (rootWin.viewMode === "Full")
    }

    ColumnLayout {
        id: regularInfo
        anchors.left: profilePic.right
        anchors.right: parent.right
        anchors.margins: 5
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        Label {
            id: patLabel
            font.bold: true
            visible: rootWin.viewMode === "Full"
            style: Text.Outline; styleColor: Material.background
        }

        Label {
            id: procedureLabel
            text: {
                var nameList = [];
                var returnMe ="";
                if(procedures.length > 0) {
                    var txList = JSON.parse(procedures);
                    for(var i=0;i<txList.length;i++) {
                        nameList.push(commonFuns.makeTxItemString(txList[i]));
                    }
                    returnMe= nameList.join("\n");
                }
                return returnMe;
            }

            visible: procedures.length > 0
            style: Text.Outline; styleColor: Material.background
            wrapMode: Text.WordWrap
            Layout.maximumWidth: regularInfo.width

        }
        Label {
            text: comments
            font.bold: status == "Blocker"
            visible: rootWin.viewMode === "Full"
            style: Text.Outline; styleColor: Material.background
            wrapMode: Text.WordWrap
            Layout.maximumWidth: regularInfo.width
        }

        Label {
            text: status
            style: Text.Outline; styleColor: Material.background
        }

        SequentialAnimation {
            loops: Animation.Infinite
            running: hasAlert
            PropertyAnimation {
                target: regularInfo
                property: "opacity"
                from: 1
                to: 0
                duration: 2000
                easing.type: Easing.OutQuint
            }
            PropertyAnimation {
                target: regularInfo
                property: "opacity"
                from: 0
                to: 1
                duration: 2000
                easing.type: Easing.OutQuint
            }
        }
    }

    Image {
        id: languageFlagImage
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        width: 32
        height: 32
        fillMode:Image.PreserveAspectFit
        anchors.margins: 5
    }

    Label {
        id: alertLabel
        anchors.centerIn: parent
        color: Material.color(Material.Red);
        text: schAlert
        font.pointSize: 16
        style: Text.Outline;
        styleColor: Material.color(Material.Orange)
        SequentialAnimation {
            loops: Animation.Infinite
            running: hasAlert
            PropertyAnimation {
                target: alertLabel
                property: "opacity"
                from: 0
                to: 1
                duration: 2000
                easing.type: Easing.OutQuint
            }
            PropertyAnimation {
                target: alertLabel
                property: "opacity"
                from: 1
                to: 0
                duration: 2000
                easing.type: Easing.OutQuint
            }
        }
    }




    onClicked: {

        if(typeof(schButtonDia) === "undefined") {

            if(status == "Blocker") {
                var comp = Qt.createComponent("EditBlockerDialog.qml");
                schButtonDia= comp.createObject(this);
            }
            else {
                comp = Qt.createComponent("SchButtonDia.qml");
                schButtonDia= comp.createObject(this);
            }
        }

        //console.debug(comp.errorString())

        if(schButtonDia.visible) {
            schButtonDia.close();
        }
        else {
            schButtonDia.open();
        }
    }

}
