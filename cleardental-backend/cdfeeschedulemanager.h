// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef CDFEESCHEDULEMANAGER_H
#define CDFEESCHEDULEMANAGER_H

#include <QObject>

class CDFeeScheduleManager : public QObject
{
    Q_OBJECT
public:
    explicit CDFeeScheduleManager(QObject *parent = nullptr);



public slots:
    static QStringList getListOfPlans(QString pracID);
    static QStringList getLocalListOfPlans();


};

#endif // CDFEESCHEDULEMANAGER_H
