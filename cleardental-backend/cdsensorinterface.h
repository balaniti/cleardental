// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef CDSENSORINTERFACE_H
#define CDSENSORINTERFACE_H

#include <QObject>
#include <QVariantMap>

#include <libusb-1.0/libusb.h>

class CDSensorInterface : public QObject
{
    Q_OBJECT
public:

    enum SensorVendorType {
        HANUMATSU, //APEX / EDLEN IMAGING EI / RAYSCAN RIO / SIGMA BIORAY HR / TUXEDO
        WOODPECKER //Guilin Woodpecker Medical Instrument
    };
    Q_ENUM(SensorVendorType)

    static CDSensorInterface* getInstance();

    void meetAndGreet(); //handshake all available devices

    int sensorCount();
    int getSize(int sensorIndex);
    int getSensorWidth(int sensorIndex);
    int getSensorHeight(int sensorIndex);
    QString getSerialNumber(int sensorIndex);

    //WARNING: THESE functions ASSUME YOU ARE ALREADY ON ANOTHER THREAD; OTHERWISE YOU WILL BE LOCKED!!!
    QVector<quint16> requestBlackImage(int sensorIndex);
    QVector<quint16> requestWhiteImage(int sensorIndex);
    bool requestCancel(int sensorIndex);
    //--!


private:
    explicit CDSensorInterface(QObject *parent = nullptr);
    void sendUSBData(QByteArray dataToSend, int endpoint, libusb_device_handle *dev_handle);
    QByteArray readUSBData(int endpoint, libusb_device_handle *dev_handle, int readBytes);

    QVariantMap getHanuDeviceInfo(libusb_device *dev);
    QVector<quint16> getHanuBlackImage(int deviceIndex);
    QVector<quint16> getHanuWhiteImage(int deviceIndex);

    QList<libusb_device *> m_deviceList;
    QList<int> m_deviceSizes;
    QList<QString> m_serialNumbers;
    QList<SensorVendorType> m_vendorTypes;



};

#endif // CDSENSORINTERFACE_H
