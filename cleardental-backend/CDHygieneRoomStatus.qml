// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtGraphicalEffects 1.12
import QtQuick.Window 2.10

Drawer {
    id: drawer

    property bool showOtherRooms: true

    property string textToShow: ""

    background: Rectangle {
        gradient: Gradient {
                 GradientStop { position: 0.0; color: "white" }
                 GradientStop { position: 0.5; color: "white" }
                 GradientStop { position: 1.0; color: "transparent" }
             }
        anchors.fill: parent
    }


    ColumnLayout {
        anchors.left:  parent.left
        anchors.right: parent.right
        anchors.margins: 10

//        CDHeaderLabel {
//            text: "Current Time"
//        }

//        Label {
//            id: currTimeLabel
//            function getDateTimeString() {

//                var currentTime = new Date();
//                text = currentTime.toLocaleString();

//            }

//            Layout.fillWidth: true
//            horizontalAlignment: Text.AlignHCenter

//            SequentialAnimation {
//                id: updateTimeAni
//                loops: Animation.Infinite
//                ScriptAction {
//                    script: currTimeLabel.getDateTimeString()
//                }
//                PauseAnimation { duration:  1000 }
//            }

//            Component.onCompleted: updateTimeAni.running = true
//        }


//        MenuSeparator {Layout.fillWidth: true}

        CDCancelButton {
            icon.name: ""
            text: "Launch Multimedia App"

            CDToolLauncher {
                id: m_Launcher
            }

            onClicked: {
                m_Launcher.launchTool(CDToolLauncher.Media_Controls);
                drawer.close();
            }
        }

        MenuSeparator {Layout.fillWidth: true}

        CDHeaderLabel {
            text: "Translation"
        }

        CDConstLoader {
            id: translationLoader
            property var questionDatabase: []
            Component.onCompleted: {
                var questionJSON = translationLoader.getCommonPatTranslationsJSON();
                questionDatabase = JSON.parse(questionJSON);
                var makeModel = [];
                for(var i=0;i<questionDatabase.length;i++) {
                    makeModel.push(questionDatabase[i]["Summary"]);
                }
                questionBox.model = makeModel;
            }
        }

        GridLayout {
            columns: 2
            Layout.fillWidth: true
            columnSpacing: 5
            CDDescLabel {
                text: "Language"
            }

            ComboBox {
                id: langBox
                model: ["Portuguese","Spanish"]
                Layout.fillWidth: true
                onCurrentIndexChanged: {
                    textToShow = translationLoader.questionDatabase[questionBox.currentIndex][model[currentIndex]]
                }
            }

            CDDescLabel {
                text: "Question"
            }
            ComboBox {
                id: questionBox
                Layout.fillWidth: true
                onCurrentIndexChanged: {
                    textToShow = translationLoader.questionDatabase[currentIndex][langBox.currentValue]
                }
            }
        }
        Switch {
            id: showQuestionSwitch
            icon.name: ""
            text: "Show Question"
            checked: false
        }

        Window {
            x:0
            y:0
            width: 1920
            height: 1080
            visible: showQuestionSwitch.checked
            flags: Qt.WA_TranslucentBackground | Qt.FramelessWindowHint
            color: "transparent"
            CDTranslucentPane {
                anchors.centerIn: parent
                backMaterialColor: Material.Orange
                Label {
                    font.pointSize: 42
                    text: textToShow
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }
            }
        }

        Window {
            x:0
            y:1080
            width: 1920
            height: 1080
            visible: showQuestionSwitch.checked
            flags: Qt.WA_TranslucentBackground | Qt.FramelessWindowHint
            color: "transparent"
            CDTranslucentPane {
                anchors.centerIn: parent
                backMaterialColor: Material.Orange
                Label {
                    font.pointSize: 42
                    text: textToShow
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }
            }
        }



    }
}
