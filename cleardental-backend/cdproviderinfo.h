// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef CDPROVIDERINFO_H
#define CDPROVIDERINFO_H

#include <QObject>

class CDProviderInfo : public QObject
{
    Q_OBJECT
public:
    explicit CDProviderInfo(QObject *parent = nullptr);

    Q_INVOKABLE static QString getCurrentProviderUsername();
    Q_INVOKABLE static QString signData(QString data);

signals:

public slots:
};

#endif // CDPROVIDERINFO_H
