// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cdprinter.h"

#include <QDebug>
#include <QPainter>
#include <QPrinter>
#include <QDir>
#include <QProcess>
#include <QSettings>
#include <QStaticText>
#include <QJsonDocument>
#include <QJsonArray>
#include <QFontMetrics>
#include <QPageSize>
#include <QMarginsF>

#include "cdfilelocations.h"
#include "cdproviderinfo.h"
#include "cdbillingestimator.h"


CDPrinter::CDPrinter(QObject *parent) : QObject(parent)
{

}

void CDPrinter::printRx(QVariantMap inputData)
{

    QPrinter printer;
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setOutputFileName(QDir::tempPath() + "/printMe-"+CDProviderInfo::getCurrentProviderUsername()+".pdf");
    printer.setPageSize(QPageSize(QPageSize::Letter));
    printer.setPageMargins(QMarginsF(1,1,1,1),QPageLayout::Inch);
    QPainter painter;

    if (! painter.begin(&printer)) { // failed to open file
        qWarning("failed to open file, is it writable?");
        return;
    }


    QFont regularFont("FreeSerif",12);
    QFont bigFont("FreeSerif",98);
    QFont headerFont("FreeSerif",24);


    QString userName = QDir::home().dirName();
    QSettings docInfoSettings(CDFileLocations::getDocInfoFile(userName),
                              QSettings::IniFormat);
    QSettings locationInfoSettings(
                CDFileLocations::getLocalPracticePreferenceFile(),
                QSettings::IniFormat);
    locationInfoSettings.beginGroup("GeneralInfo");
    QSettings patInfoSettings(
                CDFileLocations::getPersonalFile(inputData["PatID"].toString()),
            QSettings::IniFormat);
    patInfoSettings.beginGroup("Address");

    //First draw the header with prac info

    QString headerText = locationInfoSettings.value("PracticeName").toString();
    headerText += "\n" +locationInfoSettings.value("AddrLine1").toString();
    if(locationInfoSettings.contains("AddrLine2") &&
            locationInfoSettings.value("AddrLine2").toString().length() > 1) {
        headerText += "\n" +
                locationInfoSettings.value("AddrLine2").toString();
    }
    headerText += "\n" +locationInfoSettings.value("City").toString();
    headerText += ", " +locationInfoSettings.value("State").toString();
    headerText += " " +locationInfoSettings.value("Zip").toString();
    headerText += "\nPhone Number: " +locationInfoSettings.value("PhoneNumber").toString();
    //headerText += "\nWebsite: " +locationInfoSettings.value("Website").toString();

    QRect headerArea(0,0,0,0);
    headerArea.setWidth(printer.width());
    headerArea.setHeight(printer.height() / 6);
    painter.setFont(headerFont);
    painter.drawText(headerArea,Qt::AlignHCenter|Qt::AlignTop,headerText);


    painter.drawLine(0,headerArea.height(),printer.width(),headerArea.height());

    //Now write the Patient's Info
    QString patText = "Patient's Name: " + inputData["FirstName"].toString() +
            " " + inputData["LastName"].toString() + "\n";
    patText += "Date of Birth: " + inputData["DOB"].toString() + "\n";
    patText += "Address: " + patInfoSettings.value("StreetAddr1").toString();
    if(patInfoSettings.value("StreetAddr2","").toString().length() > 1) {
        patText += ", " + patInfoSettings.value("StreetAddr2").toString();
    }
    patText += ", " + patInfoSettings.value("City").toString();
    patText += ", " + patInfoSettings.value("State").toString();
    patText += " " + patInfoSettings.value("Zip").toString();

    QRect secondArea(0,
                      headerArea.height(),
                      printer.width(),
                      printer.height()/8);
    painter.setFont(regularFont);
    painter.drawText(secondArea,Qt::AlignLeft|Qt::AlignVCenter,patText);


    //Now for information about the doctor
    QString docText = "Prescriber's Name: " +
            docInfoSettings.value("FirstName").toString() + " " +
            docInfoSettings.value("LastName").toString() + "\n";
    docText += "NPI: " + docInfoSettings.value("NPINumb").toString() + "\n";
    docText += "DEA: " + docInfoSettings.value("DEANumb").toString() + "\n";
    docText += "License Number: " +
            docInfoSettings.value("LicenseID").toString();
    painter.drawText(secondArea,Qt::AlignRight|Qt::AlignVCenter,docText);

    painter.drawLine(0,secondArea.y() + secondArea.height(),
                     printer.width(),secondArea.y() + secondArea.height());


    //Now write the actual drug Rx
    QString drugText = "Prescription Date: " +
            inputData["PrescriptionDate"].toString() + "\n";
    drugText+= "Drug Name: " + inputData["DrugName"].toString() + "\n";
    drugText+= "Form: " + inputData["Form"].toString() + "\n";
    drugText+= "Strength: " + inputData["Strength"].toString() + "\n";
    drugText+= "Dispense Total: " + inputData["DispenseTotal"].toString() + "\n";
    drugText+= "Schedule: " + inputData["Schedule"].toString() + "\n";
    drugText+= "Days: " + inputData["Days"].toString() + "\n";
    drugText+= "Refills: " + inputData["Refills"].toString() + "\n";
    if((inputData["Instructions"].isValid()) && (inputData["Instructions"].toString().length() > 1)) {
        drugText+= "Instructions: " + inputData["Instructions"].toString() + "\n";
    }
    if(inputData["AllowGeneric"].toBool()) {
        drugText+= "Generic Substitution Allowed";
    }
    else {
        drugText+= "Dispense as Written";
    }

    QRect rxInfoArea(printer.width()/2,
                      secondArea.y() + secondArea.height(),
                      printer.width()/2,
                      printer.height()/3);
    painter.drawText(rxInfoArea,Qt::AlignLeft|Qt::AlignVCenter|Qt::TextWordWrap,drugText);
    painter.drawLine(0,rxInfoArea.y() + rxInfoArea.height(),
                     printer.width(),rxInfoArea.y() + rxInfoArea.height());


    rxInfoArea.setX(0);
    rxInfoArea.setWidth(printer.width()/2);
    painter.setFont(bigFont);
    painter.drawText(rxInfoArea,Qt::AlignHCenter|Qt::AlignVCenter,"℞");

    QString sigText = "Doctor Signature: ________________________";
    painter.setFont(headerFont);
    QRect sigRect(0,
                  rxInfoArea.y() + rxInfoArea.height(),
                  printer.width(),
                  printer.height()/10);
    painter.drawText(sigRect,Qt::AlignHCenter|Qt::AlignVCenter,sigText);


    painter.end();

    QProcess printerPro;
    printerPro.setProgram("/usr/bin/lpr");
    QStringList arguments;
    arguments<< printer.outputFileName();
    printerPro.setArguments(arguments);
    printerPro.start();
    printerPro.waitForFinished();
}

void CDPrinter::printPostOpInstr(QString opName)
{
    qDebug()<<opName;
}

void CDPrinter::printReminder(QString patID, QDate when)
{
    qDebug()<<patID;
    qDebug()<<when;

}

void CDPrinter::printClaimForm(QVariantList inputList)
{
    //qDebug()<<inputData.keys();

    QString patID = inputList.at(0).toHash().value("PatientName").toString();


    QSettings dentalPlanInfo(CDFileLocations::getDentalPlansFile(patID),QSettings::IniFormat);

    dentalPlanInfo.beginGroup("Primary");

    QPrinter printer;
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setOutputFileName(QDir::tempPath() + "/printMe-"+CDProviderInfo::getCurrentProviderUsername()+".pdf");
    printer.setPageSize(QPageSize(QPageSize::Letter));
    printer.setResolution(300);

    QPainter painter;
    if (! painter.begin(&printer)) { // failed to open file
        qWarning("failed to open file, is it writable?");
        return;
    }


    //First draw the background
    QPixmap backImg = QPixmap(":/2019ADAClaimForm.png");
    //painter.drawImage(printer.pageRect(),backImg);
    QPainter backPainter(&backImg);

    QFont setFonter("Roboto Mono",24);
    backPainter.setFont(setFonter);

    QSettings patInfo(CDFileLocations::getPersonalFile(patID),QSettings::IniFormat);

//https://www.ada.org/~/media/ADA/Publications/Files/ADADentalClaimCompletionInstructions_v2019_2020.pdf?la=en

    //Section 1
    backPainter.drawText(110,210,200,200,Qt::AlignLeft | Qt::AlignTop,"X");

    //Section 3
    QString sec3Text = dentalPlanInfo.value("Name").toString() + "\n" +
            dentalPlanInfo.value("Addr").toString() + "\n" +
            dentalPlanInfo.value("City").toString() + ", " +
            dentalPlanInfo.value("State").toString() + " " +
            dentalPlanInfo.value("Zip").toString();
    backPainter.drawText(75,500,1150,175,Qt::AlignLeft | Qt::AlignTop,sec3Text);

    //Section 4
    dentalPlanInfo.endGroup();
    dentalPlanInfo.beginGroup("Secondary");
    if(dentalPlanInfo.value("Name").toString().length() > 1) {
        if(dentalPlanInfo.value("Dental").toBool()) {
            backPainter.drawText(231,760,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
        }
        if(dentalPlanInfo.value("Medical").toBool()) {
            backPainter.drawText(469,760,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
        }

        //Section 5
        QString sec5Text = dentalPlanInfo.value("PolHolderLast").toString() + ", " +
                dentalPlanInfo.value("PolHolderFirst").toString() + " " +
                dentalPlanInfo.value("PolHolderMiddle").toString();
        backPainter.drawText(75,850,1200,50,Qt::AlignLeft | Qt::AlignTop,sec5Text);

        //Section 6
        QString sec6Text = dentalPlanInfo.value("PolHolderDOB").toString();
        backPainter.drawText(75,950,400,50,Qt::AlignLeft | Qt::AlignTop,sec6Text);

        //Section 7
        QString sec7Gender = dentalPlanInfo.value("PolHolderGender").toString();
        if(sec7Gender == "Male") {
            backPainter.drawText(531,963,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
        }
        else if(sec7Gender == "Female") {
            backPainter.drawText(591,963,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
        }
        else { //Because according to the ADA, if you aren't male or female, you must be "unknown"
            backPainter.drawText(655,963,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
        }

        //Section 8
        QString sec8Text = dentalPlanInfo.value("SubID").toString();
        backPainter.drawText(727,950,525,50,Qt::AlignLeft | Qt::AlignTop,sec8Text);

        //Section 9
        QString sec9Text = dentalPlanInfo.value("GroupID").toString();
        backPainter.drawText(75,1045,400,50,Qt::AlignLeft | Qt::AlignTop,sec9Text);

        //Section 10
        QString sec10Rel = dentalPlanInfo.value("PolHolderRel").toString();
        if(sec10Rel == "Self") {
            backPainter.drawText(527,1060,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
        }
        else if(sec10Rel == "Spouse") {
            backPainter.drawText(680,1060,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
        }
        else if(sec10Rel == "Dependent") {
            backPainter.drawText(860,1060,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
        }
        else {
            backPainter.drawText(1070,1060,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
        }

        //Section 11
        QString sec11Text = dentalPlanInfo.value("Name").toString() + "\n" +
                dentalPlanInfo.value("Addr").toString() + "\n" +
                dentalPlanInfo.value("City").toString() + ", " +
                dentalPlanInfo.value("State").toString() + " " +
                dentalPlanInfo.value("Zip").toString();
        backPainter.drawText(75,1160,1150,175,Qt::AlignLeft | Qt::AlignTop,sec11Text);
    }
    dentalPlanInfo.endGroup();
    dentalPlanInfo.beginGroup("Primary");

    //Section 12
    QString relToHolder = dentalPlanInfo.value("PolHolderRel").toString();
    QString sec12Text = "";
    QString sec13Text = "";
    if(relToHolder == "Self") {
        patInfo.beginGroup("Name");
        sec12Text = patInfo.value("LastName").toString() + ", " +
                patInfo.value("FirstName").toString() + " " +
                patInfo.value("MiddleName").toString() + "\n";
        patInfo.endGroup();
        patInfo.beginGroup("Address");
        sec12Text+= patInfo.value("StreetAddr1").toString() + "\n" +
                patInfo.value("StreetAddr2").toString() + "\n" +
                patInfo.value("City").toString() + ", " +
                patInfo.value("State").toString() + " " +
                patInfo.value("Zip").toString();
        patInfo.endGroup();
        backPainter.drawText(1300,400,1180,190,Qt::AlignLeft | Qt::AlignTop,sec12Text);

        //Section 13
        patInfo.beginGroup("Personal");
        sec13Text = patInfo.value("DateOfBirth").toString();
        patInfo.endGroup();
        backPainter.drawText(1300,650,400,50,Qt::AlignLeft | Qt::AlignTop,sec13Text);

    }
    else {
        sec12Text = dentalPlanInfo.value("PolHolderLast").toString() + ", " +
                dentalPlanInfo.value("PolHolderFirst").toString() + " " +
                dentalPlanInfo.value("PolHolderMiddle").toString() + "\n" +
                dentalPlanInfo.value("PolHolderAddress").toString() + "\n" +
                dentalPlanInfo.value("PolHolderCity").toString() + ", " +
                dentalPlanInfo.value("PolHolderState").toString() + " " +
                dentalPlanInfo.value("PolHolderZip").toString();
        backPainter.drawText(1300,400,1180,190,Qt::AlignLeft | Qt::AlignTop,sec12Text);

        //Section 13
        sec13Text = dentalPlanInfo.value("PolHolderDOB").toString();
        backPainter.drawText(1300,650,400,50,Qt::AlignLeft | Qt::AlignTop,sec13Text);
    }

    //Section 14
    QString sec14Gender = "";
    if(relToHolder == "Self") {
        patInfo.beginGroup("Personal");
        sec14Gender = patInfo.value("Sex","").toString();
        patInfo.endGroup();
    }
    else {
        sec14Gender = dentalPlanInfo.value("PolHolderGender").toString();
    }
    if(sec14Gender == "Male") {
        backPainter.drawText(1732,660,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
    }
    else if(sec14Gender == "Female") {
        backPainter.drawText(1790,660,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
    }
    else { //Because according to the ADA, if you aren't male or female, you must be "unknown"
        backPainter.drawText(1850,660,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
    }

    //Section 15
    QString sec15Text = dentalPlanInfo.value("SubID").toString();
    backPainter.drawText(1950,650,525,50,Qt::AlignLeft | Qt::AlignTop,sec15Text);

    //Section 16
    QString sec16Text = dentalPlanInfo.value("GroupID").toString();
    backPainter.drawText(1300,750,380,50,Qt::AlignLeft | Qt::AlignTop,sec16Text);

    //Section 17
    QString sec17Text = dentalPlanInfo.value("EmployerName").toString();
    backPainter.drawText(1700,750,750,50,Qt::AlignLeft | Qt::AlignTop,sec17Text);

    //Section 18
    QString sec18Text = dentalPlanInfo.value("PolHolderRel").toString();
    if(sec18Text == "Self") {
        backPainter.drawText(1340,910,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
    }
    else if(sec18Text == "Spouse") {
        backPainter.drawText(1490,910,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
    }
    else if(sec18Text == "Dependent") {
        backPainter.drawText(1670,910,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
    }
    else {
        backPainter.drawText(1940,910,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
    }

    //There is no 19 for now

    //Section 20
    patInfo.beginGroup("Name");
    QString sec20Text = patInfo.value("LastName").toString() + ", " +
            patInfo.value("FirstName").toString() + " " +
            patInfo.value("MiddleName").toString() + "\n";
    patInfo.endGroup();
    patInfo.beginGroup("Address");
    sec20Text+= patInfo.value("StreetAddr1").toString() + "\n" +
            patInfo.value("StreetAddr2").toString() + "\n" +
            patInfo.value("City").toString() + ", " +
            patInfo.value("State").toString() + " " +
            patInfo.value("Zip").toString();
    backPainter.drawText(1300,1010,1190,190,Qt::AlignLeft | Qt::AlignTop,sec20Text);

    //Section 21
    patInfo.endGroup();
    patInfo.beginGroup("Personal");
    QString sec21Text = patInfo.value("DateOfBirth").toString();
    backPainter.drawText(1300,1250,400,50,Qt::AlignLeft | Qt::AlignTop,sec21Text);

    //Section 22
    QString sec22Gender = patInfo.value("Sex","").toString(); //Accoring to the ADA; Sex = Gender
//    QString sec22Gender = patInfo.value("Gender","").toString();
//    if(sec22Gender.length() < 2) {
//        sec22Gender = patInfo.value("Sex","").toString();
//    }
    if(sec22Gender == "Male") {
        backPainter.drawText(1730,1260,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
    }
    else if(sec22Gender == "Female") {
        backPainter.drawText(1800,1260,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
    }
    else {
        backPainter.drawText(1850,1260,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
    }

    //Section 23
    QString sec23Text = patID;
    backPainter.drawText(1950,1250,530,50,Qt::AlignLeft | Qt::AlignTop,sec23Text);

    int yMarker = 1450;
    int totFee = 0;
    for (int i = 0; i < inputList.length(); i++) {
        QHash<QString, QVariant> procedureInfo = inputList.at(i).toHash().value("Procedure").toHash();

        //Section 24
        QString sec24Text = inputList.at(i).toHash().value("ProcedureDate").toString();
        QDateTime sec24Date = QDateTime::fromString(sec24Text);
        backPainter.drawText(120,yMarker,300,50,Qt::AlignLeft | Qt::AlignTop,sec24Date.toString("MM/d/yyyy"));

        //Section 25
    //https://www.ada.org/~/media/ADA/Publications/Files/CDTCode_AreaOfOralCavity_ToothTable_V3_PublicPosting2020Jan.pdf
        QString dCode = procedureInfo.value("DCode").toString();

        //TODO: add in all the dcodes

        //Section 26
        backPainter.drawText(525,yMarker,200,200,Qt::AlignLeft | Qt::AlignTop,"JP");

        //Section 27
        if(procedureInfo.contains("Tooth")) {
            QString toothNumb = procedureInfo.value("Tooth").toString();
            backPainter.drawText(610,yMarker,200,200,Qt::AlignLeft | Qt::AlignTop,toothNumb);
        }

        //Section 28
        if(procedureInfo.contains("Surfaces")) {
            QString surfaces = procedureInfo.value("Surfaces").toString();
            backPainter.drawText(975,yMarker,200,200,Qt::AlignLeft | Qt::AlignTop,surfaces);
        }

        //Section 29
        backPainter.drawText(1155,yMarker,200,200,Qt::AlignLeft | Qt::AlignTop,dCode);

        //Section 29a
        //TODO: Add diagnosis codes


        //Section 29b
        backPainter.drawText(1480,yMarker,200,200,Qt::AlignLeft | Qt::AlignTop,"01");


        //Section 30
        QString procedureName = procedureInfo.value("ProcedureName").toString();
        backPainter.drawText(1580,yMarker,700,200,Qt::AlignLeft | Qt::AlignTop,procedureName);

        //Section 31
        QString fee = "$" + procedureInfo.value("BasePrice").toString();
        if(fee.indexOf(".") == -1) {
            fee = fee + ".00";
        }
        backPainter.drawText(2300,yMarker,700,200,Qt::AlignLeft | Qt::AlignTop,fee);
        totFee += procedureInfo.value("BasePrice").toInt();

        yMarker += 51;
    }

    //Section 32
    backPainter.drawText(2273,2060,700,200,Qt::AlignLeft | Qt::AlignTop,"$" + QString::number(totFee) + ".00");

    //Section 33
    QSettings hardTissueCart(CDFileLocations::getHardTissueChartFile(patID),QSettings::IniFormat);

    for(int i=1;i<=32;i++) {
        QString toothVal = hardTissueCart.value(QString::number(i)).toString();
        QStringList toothVals = toothVal.split("|");
        foreach(QString val, toothVals) {
            if(val.contains("Missing",Qt::CaseInsensitive)) {
                if(i <= 16) { //maxillary
                    backPainter.drawText(111 + (60 * (i-1)),2010,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
                }
                else { //mandibular
                    backPainter.drawText(111 + (60 * (32-i)),2060,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
                }
            }
        }
    }

    //Section 34
    backPainter.drawText(1485,1960,200,200,Qt::AlignLeft | Qt::AlignTop,"A B");

    //Section 34a
    //TODO: Add in diagnosis codes

    //Section 38
    backPainter.drawText(1542,2260,200,200,Qt::AlignLeft | Qt::AlignTop,"11");

    //Section 39
    //TODO: Integrate with radiographs for submissions
    backPainter.drawText(2175,2300,200,200,Qt::AlignLeft | Qt::AlignTop,"N");

    //Section 40
    //TODO: Integrate with ortho module
    backPainter.drawText(1335,2400,200,200,Qt::AlignLeft | Qt::AlignTop,"X");

    //Section 43
    //TODO: Integrate with pros
    backPainter.drawText(1605,2510,200,200,Qt::AlignLeft | Qt::AlignTop,"X");

    //Section 48
    QString providerUnixID = inputList.at(0).toHash().value("ProviderUnixID").toString();
    QSettings docInfoSettings(CDFileLocations::getDocInfoFile(providerUnixID),QSettings::IniFormat);
    QSettings localInfoSettings(CDFileLocations::getLocalPracticePreferenceFile(),QSettings::IniFormat);
    localInfoSettings.beginGroup("GeneralInfo");
//    QString sec48Text = docInfoSettings.value("FirstName").toString() + " " +
//            docInfoSettings.value("LastName").toString() + "\n";
    QString sec48Text = localInfoSettings.value("PracticeName").toString() + "\n";
    sec48Text +=localInfoSettings.value("AddrLine1").toString() +  "\n";
    sec48Text +=localInfoSettings.value("AddrLine2").toString() +  "\n";
    sec48Text +=localInfoSettings.value("City").toString() +  ", ";
    sec48Text +=localInfoSettings.value("State").toString() +  " ";
    sec48Text +=localInfoSettings.value("Zip").toString();
    backPainter.drawText(75,2850,1150,200,Qt::AlignLeft | Qt::AlignTop,sec48Text);

    //Section 49
    QString sec49Text = localInfoSettings.value("PracticeNPI","").toString();
    backPainter.drawText(75,3090,400,200,Qt::AlignLeft | Qt::AlignTop,sec49Text);

    //Section 50
    QString sec50Text = docInfoSettings.value("LicenseID","").toString();
    backPainter.drawText(470,3090,400,200,Qt::AlignLeft | Qt::AlignTop,sec50Text);

    //Section 51
    QString sec51Text = localInfoSettings.value("FederalTaxID","").toString();
    backPainter.drawText(855,3090,400,200,Qt::AlignLeft | Qt::AlignTop,sec51Text);

    //Section 52
    QString sec52Text = localInfoSettings.value("PhoneNumber","").toString();
    backPainter.fillRect(220,3155,270,40,QBrush(Qt::white));
    backPainter.drawText(220,3155,400,200,Qt::AlignLeft | Qt::AlignTop,sec52Text);

    //Section 54
    QString sec54Text = docInfoSettings.value("NPINumb").toString();
    backPainter.drawText(1355,2955,400,200,Qt::AlignLeft | Qt::AlignTop,sec54Text);


    //Section 55
    backPainter.drawText(2110,2960,400,200,Qt::AlignLeft | Qt::AlignTop,sec50Text);

    //Section 56
    QString sec56Text = localInfoSettings.value("AddrLine1").toString() +  "\n";
    sec56Text +=localInfoSettings.value("AddrLine2").toString() +  "\n";
    sec56Text +=localInfoSettings.value("City").toString() +  ", ";
    sec56Text +=localInfoSettings.value("State").toString() +  " ";
    sec56Text +=localInfoSettings.value("Zip").toString();
    QFont setFonterSmall("Roboto Mono",12);
    backPainter.setFont(setFonterSmall);
    backPainter.drawText(1260,3040,1150,200,Qt::AlignLeft | Qt::AlignTop,sec48Text);
    backPainter.setFont(setFonter);

    //Section 57
    backPainter.fillRect(1422,3155,270,40,QBrush(Qt::white));
    backPainter.drawText(1422,3155,400,200,Qt::AlignLeft | Qt::AlignTop,sec52Text);


    painter.drawImage(printer.pageLayout().paintRectPixels(printer.resolution()),
                      backImg.toImage());
    painter.end();


    QProcess printerPro;
    printerPro.setProgram("/usr/bin/lpr");
    QStringList arguments;
    arguments<< printer.outputFileName();
    printerPro.setArguments(arguments);
    printerPro.start();
    printerPro.waitForFinished();
}

void CDPrinter::printClaimForm(QString patientID, QVariantList claims)
{
    QSettings dentalPlanInfo(CDFileLocations::getDentalPlansFile(patientID),QSettings::IniFormat);

    dentalPlanInfo.beginGroup("Primary");

    QPrinter printer;
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setOutputFileName(QDir::tempPath() + "/printMe-"+CDProviderInfo::getCurrentProviderUsername()+".pdf");
    printer.setPageSize(QPageSize(QPageSize::Letter));
    printer.setResolution(300);

    QPainter painter;
    if (! painter.begin(&printer)) { // failed to open file
        qWarning("failed to open file, is it writable?");
        return;
    }

    //First draw the background
    QPixmap backImg = QPixmap(":/2019ADAClaimForm.png");
    //painter.drawImage(printer.pageRect(),backImg);
    QPainter backPainter(&backImg);

    QFont setFonter("Roboto Mono",24);
    backPainter.setFont(setFonter);

    QSettings patInfo(CDFileLocations::getPersonalFile(patientID),QSettings::IniFormat);

//https://www.ada.org/~/media/ADA/Publications/Files/ADADentalClaimCompletionInstructions_v2019_2020.pdf?la=en
    //Section 1
    backPainter.drawText(110,210,200,200,Qt::AlignLeft | Qt::AlignTop,"X");

    //Section 3
    QString sec3Text = dentalPlanInfo.value("Name").toString() + "\n" +
            dentalPlanInfo.value("Addr").toString() + "\n" +
            dentalPlanInfo.value("City").toString() + ", " +
            dentalPlanInfo.value("State").toString() + " " +
            dentalPlanInfo.value("Zip").toString();
    backPainter.drawText(75,500,1150,175,Qt::AlignLeft | Qt::AlignTop,sec3Text);

    //Section 4
    dentalPlanInfo.endGroup();
    dentalPlanInfo.beginGroup("Secondary");
    if(dentalPlanInfo.value("Name").toString().length() > 1) {
        if(dentalPlanInfo.value("Dental").toBool()) {
            backPainter.drawText(231,760,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
        }
        if(dentalPlanInfo.value("Medical").toBool()) {
            backPainter.drawText(469,760,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
        }

        //Section 5
        QString sec5Text = dentalPlanInfo.value("PolHolderLast").toString() + ", " +
                dentalPlanInfo.value("PolHolderFirst").toString() + " " +
                dentalPlanInfo.value("PolHolderMiddle").toString();
        backPainter.drawText(75,850,1200,50,Qt::AlignLeft | Qt::AlignTop,sec5Text);

        //Section 6
        QString sec6Text = dentalPlanInfo.value("PolHolderDOB").toString();
        backPainter.drawText(75,950,400,50,Qt::AlignLeft | Qt::AlignTop,sec6Text);

        //Section 7
        QString sec7Gender = dentalPlanInfo.value("PolHolderGender").toString();
        if(sec7Gender == "Male") {
            backPainter.drawText(531,963,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
        }
        else if(sec7Gender == "Female") {
            backPainter.drawText(591,963,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
        }
        else { //Because according to the ADA, if you aren't male or female, you must be "unknown"
            backPainter.drawText(655,963,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
        }

        //Section 8
        QString sec8Text = dentalPlanInfo.value("SubID").toString();
        backPainter.drawText(727,950,525,50,Qt::AlignLeft | Qt::AlignTop,sec8Text);

        //Section 9
        QString sec9Text = dentalPlanInfo.value("GroupID").toString();
        backPainter.drawText(75,1045,400,50,Qt::AlignLeft | Qt::AlignTop,sec9Text);

        //Section 10
        QString sec10Rel = dentalPlanInfo.value("PolHolderRel").toString();
        if(sec10Rel == "Self") {
            backPainter.drawText(527,1060,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
        }
        else if(sec10Rel == "Spouse") {
            backPainter.drawText(680,1060,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
        }
        else if(sec10Rel == "Dependent") {
            backPainter.drawText(860,1060,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
        }
        else {
            backPainter.drawText(1070,1060,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
        }

        //Section 11
        QString sec11Text = dentalPlanInfo.value("Name").toString() + "\n" +
                dentalPlanInfo.value("Addr").toString() + "\n" +
                dentalPlanInfo.value("City").toString() + ", " +
                dentalPlanInfo.value("State").toString() + " " +
                dentalPlanInfo.value("Zip").toString();
        backPainter.drawText(75,1160,1150,175,Qt::AlignLeft | Qt::AlignTop,sec11Text);
    }
    dentalPlanInfo.endGroup();
    dentalPlanInfo.beginGroup("Primary");

    //Section 12
    QString relToHolder = dentalPlanInfo.value("PolHolderRel").toString();
    QString sec12Text = "";
    QString sec13Text = "";
    if(relToHolder == "Self") {
        patInfo.beginGroup("Name");
        sec12Text = patInfo.value("LastName").toString() + ", " +
                patInfo.value("FirstName").toString() + " " +
                patInfo.value("MiddleName").toString() + "\n";
        patInfo.endGroup();
        patInfo.beginGroup("Address");
        sec12Text+= patInfo.value("StreetAddr1").toString() + "\n" +
                patInfo.value("StreetAddr2").toString() + "\n" +
                patInfo.value("City").toString() + ", " +
                patInfo.value("State").toString() + " " +
                patInfo.value("Zip").toString();
        patInfo.endGroup();
        backPainter.drawText(1300,400,1180,190,Qt::AlignLeft | Qt::AlignTop,sec12Text);

        //Section 13
        patInfo.beginGroup("Personal");
        sec13Text = patInfo.value("DateOfBirth").toString();
        patInfo.endGroup();
        backPainter.drawText(1300,650,400,50,Qt::AlignLeft | Qt::AlignTop,sec13Text);

    }
    else {
        sec12Text = dentalPlanInfo.value("PolHolderLast").toString() + ", " +
                dentalPlanInfo.value("PolHolderFirst").toString() + " " +
                dentalPlanInfo.value("PolHolderMiddle").toString() + "\n" +
                dentalPlanInfo.value("PolHolderAddress").toString() + "\n" +
                dentalPlanInfo.value("PolHolderCity").toString() + ", " +
                dentalPlanInfo.value("PolHolderState").toString() + " " +
                dentalPlanInfo.value("PolHolderZip").toString();
        backPainter.drawText(1300,400,1180,190,Qt::AlignLeft | Qt::AlignTop,sec12Text);

        //Section 13
        sec13Text = dentalPlanInfo.value("PolHolderDOB").toString();
        backPainter.drawText(1300,650,400,50,Qt::AlignLeft | Qt::AlignTop,sec13Text);
    }

    //Section 14
    QString sec14Gender = "";
    if(relToHolder == "Self") {
        patInfo.beginGroup("Personal");
        sec14Gender = patInfo.value("Sex","").toString();
        patInfo.endGroup();
    }
    else {
        sec14Gender = dentalPlanInfo.value("PolHolderGender").toString();
    }
    if(sec14Gender == "Male") {
        backPainter.drawText(1732,660,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
    }
    else if(sec14Gender == "Female") {
        backPainter.drawText(1790,660,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
    }
    else { //Because according to the ADA, if you aren't male or female, you must be "unknown"
        backPainter.drawText(1850,660,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
    }

    //Section 15
    QString sec15Text = dentalPlanInfo.value("SubID").toString();
    backPainter.drawText(1950,650,525,50,Qt::AlignLeft | Qt::AlignTop,sec15Text);

    //Section 16
    QString sec16Text = dentalPlanInfo.value("GroupID").toString();
    backPainter.drawText(1300,750,380,50,Qt::AlignLeft | Qt::AlignTop,sec16Text);

    //Section 17
    QString sec17Text = dentalPlanInfo.value("EmployerName").toString();
    backPainter.drawText(1700,750,750,50,Qt::AlignLeft | Qt::AlignTop,sec17Text);

    //Section 18
    QString sec18Text = dentalPlanInfo.value("PolHolderRel").toString();
    if(sec18Text == "Self") {
        backPainter.drawText(1340,910,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
    }
    else if(sec18Text == "Spouse") {
        backPainter.drawText(1490,910,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
    }
    else if(sec18Text == "Dependent") {
        backPainter.drawText(1670,910,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
    }
    else {
        backPainter.drawText(1940,910,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
    }

    //There is no 19 for now

    //Section 20
    patInfo.beginGroup("Name");
    QString sec20Text = patInfo.value("LastName").toString() + ", " +
            patInfo.value("FirstName").toString() + " " +
            patInfo.value("MiddleName").toString() + "\n";
    patInfo.endGroup();
    patInfo.beginGroup("Address");
    sec20Text+= patInfo.value("StreetAddr1").toString() + "\n" +
            patInfo.value("StreetAddr2").toString() + "\n" +
            patInfo.value("City").toString() + ", " +
            patInfo.value("State").toString() + " " +
            patInfo.value("Zip").toString();
    backPainter.drawText(1300,1010,1190,190,Qt::AlignLeft | Qt::AlignTop,sec20Text);

    //Section 21
    patInfo.endGroup();
    patInfo.beginGroup("Personal");
    QString sec21Text = patInfo.value("DateOfBirth").toString();
    backPainter.drawText(1300,1250,400,50,Qt::AlignLeft | Qt::AlignTop,sec21Text);

    //Section 22
    QString sec22Gender = patInfo.value("Sex","").toString(); //Accoring to the ADA; Sex = Gender
    if(sec22Gender == "Male") {
        backPainter.drawText(1730,1260,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
    }
    else if(sec22Gender == "Female") {
        backPainter.drawText(1800,1260,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
    }
    else {
        backPainter.drawText(1850,1260,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
    }

    //Section 23
    QString sec23Text = patientID;
    backPainter.drawText(1950,1250,530,50,Qt::AlignLeft | Qt::AlignTop,sec23Text);

    int yMarker = 1450;
    int totFee = 0;
    for (int i = 0; i < claims.length(); i++) {
        QHash<QString, QVariant> procedureInfo = claims.at(i).toHash().value("Procedure").toHash();

        //Section 24
        QString sec24Text = claims.at(i).toHash().value("ProcedureDate").toString();
        QDateTime sec24Date = QDateTime::fromString(sec24Text);
        backPainter.drawText(120,yMarker,300,50,Qt::AlignLeft | Qt::AlignTop,sec24Date.toString("MM/d/yyyy"));

        //Section 25
    //https://www.ada.org/~/media/ADA/Publications/Files/CDTCode_AreaOfOralCavity_ToothTable_V3_PublicPosting2020Jan.pdf
        QString dCode = procedureInfo.value("DCode").toString();

        //TODO: add in all the dcodes

        //Section 26
        backPainter.drawText(525,yMarker,200,200,Qt::AlignLeft | Qt::AlignTop,"JP");

        //Section 27
        if(procedureInfo.contains("Tooth")) {
            QString toothNumb = procedureInfo.value("Tooth").toString();
            backPainter.drawText(610,yMarker,200,200,Qt::AlignLeft | Qt::AlignTop,toothNumb);
        }

        //Section 28
        if(procedureInfo.contains("Surfaces")) {
            QString surfaces = procedureInfo.value("Surfaces").toString();
            backPainter.drawText(975,yMarker,200,200,Qt::AlignLeft | Qt::AlignTop,surfaces);
        }

        //Section 29
        backPainter.drawText(1155,yMarker,200,200,Qt::AlignLeft | Qt::AlignTop,dCode);

        //Section 29a
        //TODO: Add diagnosis codes


        //Section 29b
        backPainter.drawText(1480,yMarker,200,200,Qt::AlignLeft | Qt::AlignTop,"01");


        //Section 30
        QString procedureName = procedureInfo.value("ProcedureName").toString();
        backPainter.drawText(1580,yMarker,700,200,Qt::AlignLeft | Qt::AlignTop,procedureName);

        //Section 31
        QString fee = "$" + procedureInfo.value("BasePrice").toString();
        if(fee.indexOf(".") == -1) {
            fee = fee + ".00";
        }
        backPainter.drawText(2300,yMarker,700,200,Qt::AlignLeft | Qt::AlignTop,fee);
        totFee += procedureInfo.value("BasePrice").toInt();

        yMarker += 51;
    }

    //Section 32
    backPainter.drawText(2273,2060,700,200,Qt::AlignLeft | Qt::AlignTop,"$" + QString::number(totFee) + ".00");

    //Section 33
    QSettings hardTissueCart(CDFileLocations::getHardTissueChartFile(patientID),QSettings::IniFormat);

    for(int i=1;i<=32;i++) {
        QString toothVal = hardTissueCart.value(QString::number(i)).toString();
        QStringList toothVals = toothVal.split("|");
        foreach(QString val, toothVals) {
            if(val.contains("Missing",Qt::CaseInsensitive)) {
                if(i <= 16) { //maxillary
                    backPainter.drawText(111 + (60 * (i-1)),2010,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
                }
                else { //mandibular
                    backPainter.drawText(111 + (60 * (32-i)),2060,200,200,Qt::AlignLeft | Qt::AlignTop,"X");
                }
            }
        }
    }

    //Section 34
    backPainter.drawText(1485,1960,200,200,Qt::AlignLeft | Qt::AlignTop,"A B");

    //Section 34a
    //TODO: Add in diagnosis codes

    //Section 38
    backPainter.drawText(1542,2260,200,200,Qt::AlignLeft | Qt::AlignTop,"11");

    //Section 39
    //TODO: Integrate with radiographs for submissions
    backPainter.drawText(2175,2300,200,200,Qt::AlignLeft | Qt::AlignTop,"N");

    //Section 40
    //TODO: Integrate with ortho module
    backPainter.drawText(1335,2400,200,200,Qt::AlignLeft | Qt::AlignTop,"X");

    //Section 43
    //TODO: Integrate with pros
    backPainter.drawText(1605,2510,200,200,Qt::AlignLeft | Qt::AlignTop,"X");

    //Section 48
    QString providerUnixID = claims.at(0).toHash().value("ProviderUsername").toString();
    QSettings docInfoSettings(CDFileLocations::getDocInfoFile(providerUnixID),QSettings::IniFormat);
    QSettings localInfoSettings(CDFileLocations::getLocalPracticePreferenceFile(),QSettings::IniFormat);
    localInfoSettings.beginGroup("GeneralInfo");
//    QString sec48Text = docInfoSettings.value("FirstName").toString() + " " +
//            docInfoSettings.value("LastName").toString() + "\n";
    QString sec48Text = localInfoSettings.value("PracticeName").toString() + "\n";
    sec48Text +=localInfoSettings.value("AddrLine1").toString() +  "\n";
    sec48Text +=localInfoSettings.value("AddrLine2").toString() +  "\n";
    sec48Text +=localInfoSettings.value("City").toString() +  ", ";
    sec48Text +=localInfoSettings.value("State").toString() +  " ";
    sec48Text +=localInfoSettings.value("Zip").toString();
    backPainter.drawText(75,2850,1150,200,Qt::AlignLeft | Qt::AlignTop,sec48Text);

    //Section 49
    QString sec49Text = localInfoSettings.value("PracticeNPI","").toString();
    backPainter.drawText(75,3090,400,200,Qt::AlignLeft | Qt::AlignTop,sec49Text);

    //Section 50
    QString sec50Text = docInfoSettings.value("LicenseID","").toString();
    backPainter.drawText(470,3090,400,200,Qt::AlignLeft | Qt::AlignTop,sec50Text);

    //Section 51
    QString sec51Text = localInfoSettings.value("FederalTaxID","").toString();
    backPainter.drawText(855,3090,400,200,Qt::AlignLeft | Qt::AlignTop,sec51Text);

    //Section 52
    QString sec52Text = localInfoSettings.value("PhoneNumber","").toString();
    backPainter.fillRect(220,3155,270,40,QBrush(Qt::white));
    backPainter.drawText(220,3155,400,200,Qt::AlignLeft | Qt::AlignTop,sec52Text);

    //Section 54
    QString sec54Text = docInfoSettings.value("NPINumb").toString();
    backPainter.drawText(1355,2955,400,200,Qt::AlignLeft | Qt::AlignTop,sec54Text);


    //Section 55
    backPainter.drawText(2110,2960,400,200,Qt::AlignLeft | Qt::AlignTop,sec50Text);

    //Section 56
    QString sec56Text = localInfoSettings.value("AddrLine1").toString() +  "\n";
    sec56Text +=localInfoSettings.value("AddrLine2").toString() +  "\n";
    sec56Text +=localInfoSettings.value("City").toString() +  ", ";
    sec56Text +=localInfoSettings.value("State").toString() +  " ";
    sec56Text +=localInfoSettings.value("Zip").toString();
    QFont setFonterSmall("Roboto Mono",12);
    backPainter.setFont(setFonterSmall);
    backPainter.drawText(1260,3040,1150,200,Qt::AlignLeft | Qt::AlignTop,sec48Text);
    backPainter.setFont(setFonter);

    //Section 57
    backPainter.fillRect(1422,3155,270,40,QBrush(Qt::white));
    backPainter.drawText(1422,3155,400,200,Qt::AlignLeft | Qt::AlignTop,sec52Text);


    painter.drawImage(printer.pageLayout().paintRectPixels(printer.resolution()),
                      backImg.toImage());
    painter.end();


    QProcess printerPro;
    printerPro.setProgram("/usr/bin/lpr");
    QStringList arguments;
    arguments<< printer.outputFileName();
    printerPro.setArguments(arguments);
    printerPro.start();
    printerPro.waitForFinished();



}


void CDPrinter::printTreatmentPlan(QString patID, QVariantMap phaseList)
{
    QPrinter printer;
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setOutputFileName(QDir::tempPath() + "/printMe-"+CDProviderInfo::getCurrentProviderUsername()+".pdf");
    printer.setPageSize(QPageSize(QPageSize::Letter));
    printer.setResolution(300);

    QPainter painter;
    if (! painter.begin(&printer)) { // failed to open file
        qWarning("failed to open file, is it writable?");
        return;
    }

    QFont setFonter("Barlow Semi Condensed",24);
    painter.setFont(setFonter);

    QString headerText = "Treatment Plan for " + patID;
    QRect headerArea(0,0,0,0);
    headerArea.setWidth(printer.width());
    headerArea.setHeight(printer.height() / 10);
    painter.drawText(headerArea,Qt::AlignHCenter|Qt::AlignVCenter,headerText);
    painter.drawLine(0,headerArea.height(),printer.width(),headerArea.height());
    setFonter.setPointSize(8);
    painter.setFont(setFonter);
    int yMarker = printer.height() /10;

    QSettings billingInfo(CDFileLocations::getDentalPlansFile(patID),QSettings::IniFormat);
    billingInfo.beginGroup("Primary");

    bool hasDentalPlan = billingInfo.value("Name").toString().length() > 2;
    QVariantMap billingEst;

    if(hasDentalPlan) {
        QString billInfoString = "Dental Plan: " + billingInfo.value("Name").toString() + " | ";
        billInfoString += "Annual Deductible: $" +  billingInfo.value("AnnDeductAll").toString() + " | ";
        billInfoString += "Major Procedure Deductible: $" +  billingInfo.value("AnnDeductBasicMajor").toString() + " | ";
        billInfoString += "Preventative Coverage:" +  billingInfo.value("PrevPercent").toString() + "% | ";
        billInfoString += "Diagnostic Coverage:" +  billingInfo.value("DiagPercent").toString() + "% | ";
        billInfoString += "Major Coverage:" +  billingInfo.value("MajorPercent").toString() + "% | ";
        billInfoString += "Annual Max: $" +  billingInfo.value("AnnMax").toString();
        painter.drawText(0,yMarker-12,billInfoString);
        billingEst = CDBillingEstimator::calculatePatientPortions(patID,phaseList);
    }

    int phaseCount = phaseList.keys().length();
    yMarker+= 100;

    int PROCEDURE_X = 50;
    int LOCATION_X = 750;
    int BASE_PRICE_X = 1250;
    int PLAN_PAY_X = 1500;
    int YOU_PAY_X = 1750;

    QVariantList phaseEstimates;

    for(int phaseI=0;phaseI<phaseCount;phaseI++) {
        //first just do the unphased
        setFonter.setPointSize(24);
        painter.setFont(setFonter);

        QVariantList txInPhaseList;
        if(phaseI == 0) {
            painter.drawText(10,yMarker,"Unphased");
            txInPhaseList = phaseList["Unphased"].toList();
            if(hasDentalPlan) {
                phaseEstimates = billingEst["Unphased"].toList();
            }
        }
        else {
            painter.drawText(10,yMarker,"Phase " + QString::number(phaseI));
            txInPhaseList = phaseList[QString::number(phaseI)].toList();
            if(hasDentalPlan) {
                phaseEstimates = billingEst[QString::number(phaseI)].toList();
            }
        }

        setFonter.setPointSize(12);
        setFonter.setBold(true);
        setFonter.setUnderline(true);
        painter.setFont(setFonter);
        yMarker+= 100;
        painter.drawText(PROCEDURE_X,yMarker,"Procedure Name");
        painter.drawText(LOCATION_X,yMarker,"Location / Tooth");
        painter.drawText(BASE_PRICE_X,yMarker,"Base Price");
        if(hasDentalPlan) {
            painter.drawText(PLAN_PAY_X,yMarker,"Plan Pays");
            painter.drawText(YOU_PAY_X,yMarker,"You Pay");
        }

        setFonter.setBold(false);
        setFonter.setUnderline(false);
        painter.setFont(setFonter);
        yMarker+= 100;
        int phaseTotalPrice =0;
        int phaseTotalDentalPlanPay=0;
        int phaseTotalPatientPay=0;
        for(int i=0;i<txInPhaseList.length();i++) {
            QVariantMap txItem = txInPhaseList[i].toMap();
            painter.drawText(PROCEDURE_X,yMarker,txItem["ProcedureName"].toString());
            if(txItem.contains("Tooth")) {
                painter.drawText(LOCATION_X,yMarker,txItem["Tooth"].toString());
            }
            if(txItem.contains("BasePrice")) {
                painter.drawText(BASE_PRICE_X,yMarker,"$" + txItem["BasePrice"].toString());
                phaseTotalPrice+= txItem["BasePrice"].toInt();
            }
            else {
                painter.drawText(BASE_PRICE_X,yMarker,"N/A");
            }

            if(hasDentalPlan) {
                QVariantMap txEst = phaseEstimates[i].toMap();
                int insPortion = txEst.value("InsPortion",0).toInt();
                int ptPortion = txEst.value("PatientPortion",0).toInt();
                painter.drawText(PLAN_PAY_X,yMarker, "$" + QString::number(insPortion));
                painter.drawText(YOU_PAY_X,yMarker, "$" + QString::number(ptPortion));
                phaseTotalDentalPlanPay+=insPortion;
                phaseTotalPatientPay+=ptPortion;
            }
            yMarker+= 75;
        }

        painter.drawLine(BASE_PRICE_X,yMarker-65,YOU_PAY_X+250,yMarker-65);
        painter.drawText(BASE_PRICE_X - 250,yMarker, "Total: ");
        painter.drawText(BASE_PRICE_X,yMarker, "$" +  QString::number(phaseTotalPrice));
        painter.drawText(PLAN_PAY_X,yMarker, "$" +  QString::number(phaseTotalDentalPlanPay));
        painter.drawText(YOU_PAY_X,yMarker, "$" +  QString::number(phaseTotalPatientPay));
        painter.drawLine(0,yMarker+50,printer.width(),yMarker+50);
        yMarker+= 150;
    }

    painter.end();

    QProcess printerPro;
    printerPro.setProgram("/usr/bin/lpr");
    QStringList arguments;
    arguments<< printer.outputFileName();
    printerPro.setArguments(arguments);
    printerPro.start();
    printerPro.waitForFinished();
}

// Assumes that no single line needs more than one line break...
void CDPrinter::printCaseNotes(QVariantList caseNotesItems)
{
    QPrinter printer;
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setOutputFileName(QDir::tempPath() + "/printMe-"+CDProviderInfo::getCurrentProviderUsername()+".pdf");
    qDebug() << printer.outputFileName();
    printer.setPageSize(QPageSize(QPageSize::Letter));
    printer.setResolution(300);

    QPainter painter;
    if(!painter.begin(&printer)) { // failed to open file
        qWarning("failed to open file, is it writable?");
        return;
    }

    QFont setFonter("Barlow Semi Condensed",24);
    QFontMetrics fontMet(setFonter);
    painter.setFont(setFonter);

    QString headerText = "Case Notes";
    QRect headerArea(0,0,0,0);
    headerArea.setWidth(printer.width());
    headerArea.setHeight(printer.height() / 10);
    painter.drawText(headerArea,Qt::AlignHCenter|Qt::AlignVCenter,headerText);
    int yMarker = printer.height() /10;
    painter.drawLine(0,yMarker,printer.width(),yMarker);

    int DATE_X = 50;
    int PATIENT_X = DATE_X + 450;
    int PROVIDER_X = PATIENT_X + 400;
    int SIGNED_X = PROVIDER_X + 300;
    int PROCEDURE_X = SIGNED_X + 250;
    int NOTES_X = DATE_X + 100;

    int procedureWidth = printer.width()-PROCEDURE_X-150;
    int caseNoteWidth = printer.width()-NOTES_X-150;

    setFonter.setPointSize(8);
    painter.setFont(setFonter);

    yMarker += 75;

    for(int i=0;i<caseNotesItems.count();i++) {

//        painter.drawLine(0,yMarker,printer.width(),yMarker);
//        yMarker += 75;

        QVariantMap lineItem = caseNotesItems.at(i).toMap();

        setFonter.setBold(true);
        painter.setFont(setFonter);

        // Date
        QDate dateObj;
        QString dateName = lineItem.value("DateTime").toString().section(' ', 0, 2) + " " + lineItem.value("DateTime").toString().section(' ', 4, 4);
        dateObj = QDate::fromString(dateName, "ddd MMM d yyyy");
        painter.drawText(DATE_X,yMarker,"Date:");
        setFonter.setBold(false);
        painter.setFont(setFonter);
        painter.drawText(DATE_X+75,yMarker,dateObj.toString("dddd, MMMM d, yyyy"));

        // Patient Name
        QString patName = lineItem.value("PatientID").toString();
        patName.truncate(patName.length()-13);
        setFonter.setBold(true);
        painter.setFont(setFonter);
        painter.drawText(PATIENT_X,yMarker,"Patient:");
        setFonter.setBold(false);
        painter.setFont(setFonter);
        painter.drawText(PATIENT_X+105,yMarker,patName);

        // Provider
        setFonter.setBold(true);
        painter.setFont(setFonter);
        painter.drawText(PROVIDER_X,yMarker,"Provider:");
        setFonter.setBold(false);
        painter.setFont(setFonter);
        painter.drawText(PROVIDER_X+125,yMarker,lineItem.value("Provider").toString());

        // Signed
        setFonter.setBold(true);
        painter.setFont(setFonter);
        painter.drawText(SIGNED_X,yMarker,"Signed:");
        setFonter.setBold(false);
        painter.setFont(setFonter);
        painter.drawText(SIGNED_X+100,yMarker,lineItem.value("Final").toString());

        // Procedure Name
        QJsonDocument jsonDoc = QJsonDocument::fromJson(lineItem.value("ProcedureObject").toString().toUtf8());
        QVariantList list = jsonDoc.array().toVariantList();
        QVariantMap procedureObj;
        if (!list.isEmpty())
            procedureObj = list.at(0).toMap();
        QString procedureName = "";
        if(procedureObj.isEmpty()) {
            procedureName += "N/A";
        }

        if(procedureObj.value("ProcedureName") == "Watch") {
            procedureName += "Watch #" + procedureObj.value("Tooth").toString();
        }
        else if(procedureObj.value("ProcedureName") == "Composite") {
            procedureName += procedureObj.value("Surfaces").toString() + " Composite #" +
                    procedureObj.value("Tooth").toString();
        }
        else if(procedureObj.value("ProcedureName") == "Amalgam") {
            procedureName +=  procedureObj.value("Surfaces").toString() + " Amalgam #" +
                    procedureObj.value("Tooth").toString();
        }
        else if(procedureObj.value("ProcedureName") == "Crown") {
            procedureName +=  procedureObj.value("Material").toString() + " Crown #" +
                    procedureObj.value("Tooth").toString();
        }
        else if(procedureObj.value("ProcedureName") == "Extraction") {
            procedureName += "Extract #" + procedureObj.value("Tooth").toString();
        }
        else if(procedureObj.value("ProcedureName") == "Implant Placement") {
            procedureName += "Place Implant #" + procedureObj.value("Tooth").toString();
        }
        else if(procedureObj.value("ProcedureName") == "Root Canal Treatment") {
            procedureName += "RCT #" + procedureObj.value("Tooth").toString();

        }
        else if(procedureObj.value("ProcedureName") == "Post and Core") {
            procedureName +=  procedureObj.value("Material").toString() + " Post and Core #" +
                    procedureObj.value("Tooth").toString();
        }
        else {
            procedureName += procedureObj.value("ProcedureName").toString();

            if(procedureObj.contains("Tooth")) {
                procedureName += " #" + procedureObj.value("Tooth").toString();
            }
            else if(procedureObj.contains("Location")) {
                procedureName += " " + procedureObj.value("Location").toString();
            }
        }
        setFonter.setBold(true);
        painter.setFont(setFonter);
        painter.drawText(PROCEDURE_X,yMarker,"Procedure:");
        setFonter.setBold(false);
        painter.setFont(setFonter);
        QString procedure = lineItem.value("Final").toString();
        if (fontMet.horizontalAdvance(procedure) < procedureWidth)
            painter.drawText(PROCEDURE_X+150,yMarker,procedureName);
        else {
            int lineSections = -1;
            while (fontMet.horizontalAdvance(procedure.section(' ', 0,lineSections)) > procedureWidth)
                lineSections--;
            painter.drawText(PROCEDURE_X+150,yMarker,procedureName.section(' ',0,lineSections));
            yMarker += 50;
            painter.drawText(PROCEDURE_X+150,yMarker,procedureName.section(' ',lineSections+1,-1));
        }

        // Case Notes
        if (yMarker >= printer.height() - headerArea.height()) {
            printer.newPage();
            yMarker = headerArea.height();
        } else {
            yMarker += 50;
        }
        QString notes = lineItem.value("CaseNote").toString();
        int numSection = 0;
        QString currLine = notes.section('\n', numSection, numSection);
        painter.drawText(NOTES_X, yMarker, currLine);
        while (currLine != notes.section('\n',-1,-1)) {
            if (yMarker >= printer.height() - headerArea.height()) {
                printer.newPage();
                yMarker = headerArea.height();
            } else {
                yMarker += 50;
            }

            numSection++;
            currLine = notes.section('\n',numSection,numSection);
            int lineWidth = fontMet.horizontalAdvance(currLine);
            if (lineWidth < caseNoteWidth) {
                painter.drawText(NOTES_X, yMarker, currLine);
            } else {
                int lineSections = -1;
                while (fontMet.horizontalAdvance(currLine.section(' ', 0,lineSections)) > caseNoteWidth)
                    lineSections--;
                painter.drawText(NOTES_X,yMarker,currLine.section(' ',0,lineSections));
                yMarker += 50;
                painter.drawText(NOTES_X,yMarker,currLine.section(' ',lineSections+1,-1));
            }
        }

        if (yMarker + 150 >= printer.height() - headerArea.height() && i < caseNotesItems.count()-1) {
            printer.newPage();
            yMarker = headerArea.height();
        } else {
            yMarker += 150;
        }
    }

    painter.end();

    QProcess printerPro;
    printerPro.setProgram("/usr/bin/lpr");
    QStringList arguments;
    arguments<< printer.outputFileName();
    printerPro.setArguments(arguments);
    printerPro.start();
    printerPro.waitForFinished();
}

void CDPrinter::printPatientLedger(QString patID, QVariantList procedureList)
{
    QPrinter printer;
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setOutputFileName(QDir::tempPath() + "/printMe-"+CDProviderInfo::getCurrentProviderUsername()+".pdf");
    printer.setPageSize(QPageSize(QPageSize::Letter));
    printer.setResolution(300);

    QPainter painter;
    if (! painter.begin(&printer)) { // failed to open file
        qWarning("failed to open file, is it writable?");
        return;
    }

    QFont setFonter("Barlow Semi Condensed",18);
    painter.setFont(setFonter);

    QSettings localPracInfo = QSettings(CDFileLocations::getLocalPracticePreferenceFile(),QSettings::IniFormat);
    localPracInfo.beginGroup("GeneralInfo");
    QString headerText = localPracInfo.value("PracticeName").toString() + "\n";
    headerText += localPracInfo.value("AddrLine1").toString() +"\n";
    if(localPracInfo.value("AddrLine2").toString().length() > 1) {
        headerText += localPracInfo.value("AddrLine2").toString() +"\n";
    }
    headerText += localPracInfo.value("City").toString() +", " + localPracInfo.value("State").toString() + " " + localPracInfo.value("Zip").toString() + "\n";
    headerText += localPracInfo.value("PhoneNumber").toString() +"\n";
    headerText += localPracInfo.value("Website").toString() +"\n";


    headerText += "Ledger for " + patID;
    QRect headerArea(0,0,0,0);
    headerArea.setWidth(printer.width());
    headerArea.setHeight(printer.height() / 5);
    painter.drawText(headerArea,Qt::AlignHCenter|Qt::AlignVCenter,headerText);
    painter.drawLine(0,headerArea.height(),printer.width(),headerArea.height());
    int yMarker = printer.height() /5;

    int DATE_X = 50;
    int AMOUNT_X = 600;
    int NOTES_X = 1050;

    yMarker+=50;
    setFonter.setPointSize(12);
    setFonter.setBold(true);
    painter.setFont(setFonter);
    painter.drawText(DATE_X,yMarker,"Date");
    painter.drawText(AMOUNT_X,yMarker,"Amount");
    painter.drawText(NOTES_X,yMarker,"Notes");
    setFonter.setPointSize(8);
    setFonter.setBold(false);
    painter.setFont(setFonter);

    qreal balance =0;
    qreal totalWriteoffs =0;

    for(int i=0;i<procedureList.count();i++) {

        QVariantMap procedure = procedureList.at(i).toMap();

        yMarker+= 75;

        if(yMarker > printer.height()) {
            printer.newPage();
            yMarker = 75;
        }

        //First print the name of the procedure
        QString procedureInfoString="";
        QVariantMap txItemObj = procedure.value("Procedure").toMap();

        if(txItemObj.value("ProcedureName") == "Watch") {
            procedureInfoString += "Watch #" + txItemObj.value("Tooth").toString();
        }
        else if(txItemObj.value("ProcedureName") == "Composite") {
            procedureInfoString += txItemObj.value("Surfaces").toString() + " Composite #" +
                    txItemObj.value("Tooth").toString();
        }
        else if(txItemObj.value("ProcedureName") == "Amalgam") {
            procedureInfoString +=  txItemObj.value("Surfaces").toString() + " Amalgam #" +
                    txItemObj.value("Tooth").toString();
        }
        else if(txItemObj.value("ProcedureName") == "Crown") {
            procedureInfoString +=  txItemObj.value("Material").toString() + " Crown #" +
                    txItemObj.value("Tooth").toString();
        }
        else if(txItemObj.value("ProcedureName") == "Extraction") {
            procedureInfoString += "Extract #" + txItemObj.value("Tooth").toString();
        }
        else if(txItemObj.value("ProcedureName") == "Implant Placement") {
            procedureInfoString += "Place Implant #" + txItemObj.value("Tooth").toString();
        }
        else if(txItemObj.value("ProcedureName") == "Root Canal Treatment") {
            procedureInfoString += "RCT #" + txItemObj.value("Tooth").toString();

        }
        else if(txItemObj.value("ProcedureName") == "Post and Core") {
            procedureInfoString +=  txItemObj.value("Material").toString() + " Post and Core #" +
                    txItemObj.value("Tooth").toString();
        }
        else {
            procedureInfoString += txItemObj.value("ProcedureName").toString();

            if(txItemObj.contains("Tooth")) {
                procedureInfoString += " #" + txItemObj.value("Tooth").toString();
            }
            else if(txItemObj.contains("Location")) {
                procedureInfoString += " " + txItemObj.value("Location").toString();
            }
        }

        painter.setPen(QColor(128,0,0));

        painter.drawText(NOTES_X,yMarker,procedureInfoString);

        // Procedure Date
        if(procedure.contains("ProcedureDate")) {
            painter.drawText(DATE_X,yMarker,formatDate(procedure.value("ProcedureDate").toString()));
        }

        // Procedure Cost
        QString baseAmount = txItemObj.value("BasePrice").toString();
        qreal parsedAmount = txItemObj.value("BasePrice").toFloat();
        painter.drawText(AMOUNT_X,yMarker,formatMoney(baseAmount));
        balance+= parsedAmount;

        painter.setPen(QColor(0,128,0));

        //Now go though each payment for the procedure
        QVariantList paymentList = procedure.value("Payments").toList();
        for(int j=0;j<paymentList.length();j++) {
            QVariantMap payment = paymentList.at(j).toMap();
            QString paymentTypeString = payment.value("Type").toString();
            yMarker+= 75;

            if(yMarker > printer.height()) {
                printer.newPage();
                yMarker = 75;
            }

            //Date of the payment
            painter.drawText(DATE_X,yMarker,formatDate(payment.value("Date").toString()));


            //Amount of the payment
            QString paymentAmount = payment.value("Amount").toString();
            qreal parsedPaymentAmount = payment.value("Amount").toFloat();
            painter.drawText(AMOUNT_X,yMarker,formatMoney(paymentAmount));
            balance-= parsedPaymentAmount;

            //Information about the payment
            QString paymentInfoString="";

            if(paymentTypeString == "Cash") {
                paymentInfoString = paymentTypeString;
            }
            else if(paymentTypeString == "Check Payment") {
                paymentInfoString = paymentTypeString;
            }
            else if(paymentTypeString == "Credit Card") {
                paymentInfoString = paymentTypeString + "; Confirmation Number:" +
                        payment.value("ConfirmationNumber").toString();
            }
            else if(paymentTypeString == "Dental Plan Payment") {
                paymentInfoString = paymentTypeString + "; Write Off: $" +
                        payment.value("WriteOff").toString();
                totalWriteoffs+= payment.value("WriteOff").toFloat();
                balance-= payment.value("WriteOff").toFloat();
            }
            else if(paymentTypeString == "Credit") {
                paymentInfoString = paymentTypeString;
                totalWriteoffs+= payment.value("Amount").toFloat();
            }
            painter.drawText(NOTES_X,yMarker,paymentInfoString);
        }
    }

    painter.setPen(QColor(0,0,0));

    yMarker += 50;
    painter.drawLine(0,yMarker,printer.width(),yMarker);

    yMarker+= 50;
    setFonter.setBold(true);
    painter.setFont(setFonter);
    painter.drawText(DATE_X,yMarker,"Balance");
    setFonter.setBold(false);
    painter.setFont(setFonter);
    if((balance > 0) && (balance < 0.01)) { //stupid javascript IEEE floats
        balance =0;
    }
    painter.drawText(AMOUNT_X,yMarker,"$" + QString::number(balance,'f',2));

    yMarker+= 50;
    setFonter.setBold(true);
    painter.setFont(setFonter);
    painter.drawText(DATE_X,yMarker,"Writeoffs");
    setFonter.setBold(false);
    painter.setFont(setFonter);
    painter.drawText(AMOUNT_X,yMarker,"$" + QString::number(totalWriteoffs,'f',2));

    painter.end();

    QProcess printerPro;
    printerPro.setProgram("/usr/bin/lpr");
    QStringList arguments;
    arguments<< printer.outputFileName();
    printerPro.setArguments(arguments);
    printerPro.start();
    printerPro.waitForFinished();
}

QVariantList CDPrinter::printPeriodontalChart(QString patID, bool reallyPrint)
{
    QVariantList returnMe;
    QSettings perioSettingsFile(CDFileLocations::getPerioChartFile(patID),QSettings::IniFormat);
    QSettings reviewFile(CDFileLocations::getReviewsFile(patID),QSettings::IniFormat);

    QPrinter printer;
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setOutputFileName(QDir::tempPath() + "/printMe-"+CDProviderInfo::getCurrentProviderUsername()+".pdf");
    qDebug() << printer.outputFileName();
    printer.setPageSize(QPageSize(QPageSize::Letter));
    printer.setPageOrientation(QPageLayout::Landscape);
    printer.setResolution(300);

    QPainter painter;
    if(!painter.begin(&printer)) { // failed to open file
        qWarning("failed to open file, is it writable?");
        returnMe.append("/dev/null/");
        return returnMe;
    }

    QFont setFonter("Barlow Semi Condensed",24);
    QFontMetrics fontMet(setFonter);
    painter.setFont(setFonter);

    QString headerText = "Periodontal chart for " + patID + " taken " + reviewFile.value("PerioCharting").toString();
    QRect headerArea(0,0,0,0);
    headerArea.setWidth(printer.width());
    headerArea.setHeight(printer.height() / 10);
    painter.drawText(headerArea,Qt::AlignHCenter|Qt::AlignVCenter,headerText);
    int yMarker = printer.height() /10;
    painter.drawLine(0,yMarker,printer.width(),yMarker);

    setFonter.setPointSize(8);
    painter.setFont(setFonter);

    yMarker += 100;

    //Maxillary teeth
    int boxWidth = printer.width() / 17 ;
    int boxHeight = 500;
    int newYMarker = yMarker + 1000;
    for(int i=0;i<=33;i++) {
        QRect topArea,middleArea,bottomArea,moblityArea;
        if(i<= 16) {
            painter.drawRect((boxWidth * i),yMarker,boxWidth,boxHeight);
            topArea = QRect(boxWidth*i,yMarker,boxWidth,boxHeight / 4);
            middleArea = QRect(boxWidth*i,yMarker + ((boxHeight)/4),boxWidth,boxHeight / 4);
            bottomArea = QRect(boxWidth*i,yMarker + ((boxHeight*2)/4),boxWidth,boxHeight / 4);
            moblityArea =QRect(boxWidth*i,yMarker + ((boxHeight*3)/4),boxWidth,boxHeight / 4);
        }
        else {
            int horzI = 33-i;
            painter.drawRect((boxWidth * horzI),newYMarker,boxWidth,boxHeight);
            topArea = QRect(boxWidth*horzI,newYMarker,boxWidth,boxHeight / 4);
            middleArea = QRect(boxWidth*horzI,newYMarker + ((boxHeight)/4),boxWidth,boxHeight / 4);
            bottomArea = QRect(boxWidth*horzI,newYMarker + ((boxHeight*2)/4),boxWidth,boxHeight / 4);
            moblityArea =QRect(boxWidth*horzI,newYMarker + ((boxHeight*3)/4),boxWidth,boxHeight / 4);
        }


        if((i == 0) || (i==33)) {
            painter.drawText(topArea,Qt::AlignHCenter|Qt::AlignVCenter,"Tooth #");
            painter.drawText(middleArea,Qt::AlignHCenter|Qt::AlignVCenter,"Pocket Depth");
            painter.drawText(bottomArea,Qt::AlignHCenter|Qt::AlignVCenter,"Recession");
            painter.drawText(moblityArea,Qt::AlignHCenter|Qt::AlignVCenter,"Moblity");

            setFonter.setPointSize(5);
            painter.setFont(setFonter);
            painter.drawText(middleArea,Qt::AlignRight|Qt::AlignTop,"Facial/Buccal");
            painter.drawText(middleArea,Qt::AlignRight|Qt::AlignBottom,"Lingual");
            painter.drawText(bottomArea,Qt::AlignRight|Qt::AlignTop,"Facial/Buccal");
            painter.drawText(bottomArea,Qt::AlignRight|Qt::AlignBottom,"Lingual");
            setFonter.setPointSize(8);
            painter.setFont(setFonter);
        }
        else {
            QString toothStr = QString::number(i);
            painter.drawText(topArea,Qt::AlignHCenter|Qt::AlignVCenter,toothStr);

            if(isMissing(patID,toothStr)) {
                painter.drawText(middleArea,Qt::AlignHCenter|Qt::AlignVCenter,"Missing");
                painter.drawText(bottomArea,Qt::AlignHCenter|Qt::AlignVCenter,"Missing");
                painter.drawText(moblityArea,Qt::AlignHCenter|Qt::AlignVCenter,"Missing");
            }
            else {
                perioSettingsFile.beginGroup(toothStr);
                QString buccalPocketsStr = perioSettingsFile.value("BuccalPockets","WNL").toString();
                if(buccalPocketsStr =="WNL") {
                    painter.drawText(middleArea,Qt::AlignHCenter|Qt::AlignTop,"3  3  3");
                }
                else {
                    QString replacecom = buccalPocketsStr.replace(',', "  ");
                    painter.drawText(middleArea,Qt::AlignHCenter|Qt::AlignTop,replacecom);
                }

                QString lingualPocketsStr = perioSettingsFile.value("LingualPockets","WNL").toString();
                if(lingualPocketsStr =="WNL") {
                    painter.drawText(middleArea,Qt::AlignHCenter|Qt::AlignBottom,"3  3  3");
                }
                else {
                    QString replacecom = lingualPocketsStr.replace(',', "  ");
                    painter.drawText(middleArea,Qt::AlignHCenter|Qt::AlignBottom,replacecom);
                }

                QString buccalRecessionStr = perioSettingsFile.value("BuccalRecession","_,_,_").toString();
                if(buccalRecessionStr == "_,_,_") {
                    painter.drawText(bottomArea,Qt::AlignHCenter|Qt::AlignTop,"None");
                }
                else {
                    QString replacecom = buccalRecessionStr.replace(',', "  ");
                    painter.drawText(bottomArea,Qt::AlignHCenter|Qt::AlignTop,replacecom);
                }

                QString lingualRecessionStr = perioSettingsFile.value("LingualRecession","_,_,_").toString();
                if(lingualRecessionStr == "_,_,_") {
                    painter.drawText(bottomArea,Qt::AlignHCenter|Qt::AlignBottom,"None");
                }
                else {
                    QString replacecom = lingualRecessionStr.replace(',', "  ");
                    painter.drawText(bottomArea,Qt::AlignHCenter|Qt::AlignBottom,replacecom);
                }

                QString mobility = perioSettingsFile.value("Mobility","None").toString();
                painter.drawText(moblityArea,Qt::AlignHCenter|Qt::AlignVCenter,mobility);

                perioSettingsFile.endGroup();
            }



        }
    }
    painter.drawLine(0,yMarker + ((boxHeight)/4),printer.width(),yMarker + ((boxHeight)/4));
    painter.drawLine(0,yMarker + ((boxHeight*2)/4),printer.width(),yMarker + ((boxHeight*2)/4));
    painter.drawLine(0,yMarker + ((boxHeight*3)/4),printer.width(),yMarker + ((boxHeight*3)/4));

    painter.drawLine(0,newYMarker + ((boxHeight)/4),printer.width(),newYMarker + ((boxHeight)/4));
    painter.drawLine(0,newYMarker + ((boxHeight*2)/4),printer.width(),newYMarker + ((boxHeight*2)/4));
    painter.drawLine(0,newYMarker + ((boxHeight*3)/4),printer.width(),newYMarker + ((boxHeight*3)/4));


    painter.end();

    if(reallyPrint) {
        QProcess printerPro;
        printerPro.setProgram("/usr/bin/lpr");
        QStringList arguments;
        arguments<< printer.outputFileName();
        printerPro.setArguments(arguments);
        printerPro.start();
        printerPro.waitForFinished();
        returnMe.append(printer.outputFileName());
    }
    else {
        QFile readPDF(printer.outputFileName());
        readPDF.open(QIODevice::ReadOnly);
        returnMe.append(reviewFile.value("PerioCharting").toString());
        returnMe.append(readPDF.readAll());
    }

    return returnMe;
}

QChar CDPrinter::getProcedureCoverageType(QString DCode)
{
    QChar returnMe = '?';
    if(DCode.startsWith("D0")) {
        returnMe = 'D';
    }
    else if(DCode.startsWith("D1")) {
        returnMe = 'P';
    }
    else /*if(DCode.startsWith("D2"))*/ {
        returnMe = 'M';
    }

    return returnMe;
}

bool CDPrinter::isMissing(QString patID, QString toothNumber)
{
    QSettings hardTissueSettingsFile(CDFileLocations::getHardTissueChartFile(patID),QSettings::IniFormat);
    QString hardTissueString = hardTissueSettingsFile.value(toothNumber,"").toString();
    return hardTissueString.contains("Missing", Qt::CaseInsensitive);
}

QString CDPrinter::formatDate(QString inputString)
{
    QDate dateObj;
    if (inputString.at(0) == '2') {
        dateObj = QDate::fromString(inputString, Qt::ISODate);
    }
    else {
        QString dateName = inputString.section(' ', 0, 2) + " " + inputString.section(' ', 4, 4);
        dateName.truncate(24);
        dateObj = QDate::fromString(dateName, "ddd MMM d yyyy");
    }
    return dateObj.toString("dddd, MMMM d, yyyy");
}

QString CDPrinter::formatMoney(QString inputString)
{
    if(inputString.indexOf(".") == -1) {
        inputString = "$" + inputString + ".00";
    } else {
        inputString = "$" + inputString ;
    }
    return inputString;
}
