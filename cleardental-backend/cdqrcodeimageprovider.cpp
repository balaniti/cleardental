// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cdqrcodeimageprovider.h"

#include <qrencode.h>


CDQRCodeImageProvider::CDQRCodeImageProvider() : QQuickImageProvider(QQuickImageProvider::Image)
{

}

QImage CDQRCodeImageProvider::requestImage(const QString &id, QSize *size, const QSize &requestedSize)
{
    Q_UNUSED(requestedSize);
    QString useMe = id;
    useMe = useMe.replace("%0A","\n");
    //qDebug()<<"Asking to encode: " << useMe;
    QRcode *qrcode = QRcode_encodeString(useMe.toStdString().c_str(), 0, QR_ECLEVEL_H,QR_MODE_8, 1);
    //qDebug()<<"Width ended up being : " << qrcode->width;
    QImage returnMe(qrcode->width,qrcode->width,QImage::Format_Mono);
    QRgb black = qRgb(0,0,0);
    QRgb white = qRgb(255,255,255);
    returnMe.setColor(0,white);
    returnMe.setColor(1,black);
    for(int x=0;x<qrcode->width;x++) {
        for (int y=0;y<qrcode->width ; y++) {
            int index = x + (y*qrcode->width);
            bool isBlack = (qrcode->data[index] % 2)==1;
            //qDebug()<<"x: " << x << " y: " << y << " value: " << qrcode->data[index] << " is black: " << isBlack;
            if(isBlack) {
                returnMe.setPixel(x,y,1);
            } else {
                returnMe.setPixel(x,y,0);
            }
        }
    }
    size->setWidth(qrcode->width);
    size->setHeight(qrcode->width);
    return returnMe;
}
