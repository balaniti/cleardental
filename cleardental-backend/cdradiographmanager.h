// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef CDRADIOGRAPHMANAGER_H
#define CDRADIOGRAPHMANAGER_H

#include <QObject>
#include <QDate>

#include "cdaquireradiograph.h"

class CDRadiographManager : public QObject
{
    Q_OBJECT
public:
    explicit CDRadiographManager(QObject *parent = nullptr);

    enum OralQuad {
        QUAD_UPPER_RIGHT,
        QUAD_UPPER_LEFT,
        QUAD_LOWER_LEFT,
        QUAD_LOWER_RIGHT
    };
    Q_ENUM(OralQuad)

public slots:
    Q_INVOKABLE static QStringList getRadiographDates(QString patName);
    Q_INVOKABLE static QString getBWRadiographDueDate(QString patName);
    Q_INVOKABLE static QStringList getRadiographPaths(QString patName,QString date);
    Q_INVOKABLE static QStringList getRadiographsForTooth(QString patName,QString getTooth);
    Q_INVOKABLE static QStringList getRadiographsForTooth(QString patName,QString getTooth, bool includeBad);
    Q_INVOKABLE static QStringList getRadiographFromType(QString patName, int getType,
                                                         bool includeBad);
    Q_INVOKABLE static QStringList getRadiographsForQuad(QString patName,OralQuad getQuad);

private:
    static bool dComp(QDate left, QDate right);
    static bool dListComp(QVariantList left, QVariantList right);
    static QList<QPair<QString,QString>> getAllRadiographImages(QString patName, bool sortResults= false);

};

#endif // CDRADIOGRAPHMANAGER_H
