// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef CDAQUIRERADIOGRAPH_H
#define CDAQUIRERADIOGRAPH_H

#include <QObject>
#include <QVariantList>
#include <QVariantMap>
#include <QImage>
#include <QThread>

#include "cdsensorworker.h"

class CDAquireRadiograph : public QObject
{
    Q_OBJECT
public:
    explicit CDAquireRadiograph(QObject *parent = nullptr);

    enum RadiographType {
        INVALID_RADIOGRAPH_TYPE,
        BW_Right_Distal,
        BW_Right_Mesial,
        BW_Left_Mesial,
        BW_Left_Distal,
        PA_Maxillary_Right_Distal,
        PA_Maxillary_Right_Mesial,
        PA_Maxillary_Anterior_Right,
        PA_Maxillary_Anterior_Center,
        PA_Maxillary_Anterior_Left,
        PA_Maxillary_Left_Mesial,
        PA_Maxillary_Left_Distal,
        PA_Mandibular_Left_Distal,
        PA_Mandibular_Left_Mesial,
        PA_Mandibular_Anterior_Left,
        PA_Mandibular_Anterior_Center,
        PA_Mandibular_Anterior_Right,
        PA_Mandibular_Right_Mesial,
        PA_Mandibular_Right_Distal,
        Panoramic,
        FMX_SINGLE_IMAGE
    }; Q_ENUM(RadiographType)

    enum SensorStatus {
        NOT_STARTED,
        CALIBRATING,
        SENDING_REQUEST,
        WAITING_FOR_RESULT,
        READING_RESULT,
        PROCESSING_RESULT,
        AWAITING_USER_INPUT,
        SAVING,
        DONE,
        CANCELLED
    }; Q_ENUM(SensorStatus)


    signals:
        void gotExposure();


public slots:
    QVariantList getSensorList();
    SensorStatus getSensorStatus(int sensorIndex);
    void requestExposure(int sensorIndex,RadiographType rType);
    QString saveExposure(QString patientID, RadiographType rType);
    void rejectExposure(QString patientID, RadiographType rType);

    static QString getFakeExposure(QString patientID, RadiographType rType);
    static bool allowFakeExposures();
    static QString tempRadiographLocation();

    bool hasCalibration(QString serialID);
    void takeCalibrationExposure(int sensorIndex,int currentIndex, int calibrationCount =3);

    static int getRotationAmount(RadiographType rType);
    static QString getStringFromType(RadiographType rType);
    static QString getUserStringFromType(RadiographType rType);
    static QList<RadiographType> getTypesFromToothNumber(QString getTooth);
    static QVariantList getQMLTypesFromToothNumber(QString getTooth);

signals:
    void sensorStatusUpdated(int sensorIndex, SensorStatus newStatus);

private:
    void setCalibration(QString serialID,QVector<quint16> calibration);
    QVector<quint16> getCalibration(QString serialID);

    void handleBlackResult(int sensorID, QVector<quint16> result);
    void handleExposureResult(int sensorID, QVector<quint16> result);

    QList<CDSensorWorker*> m_workers;
    QList<SensorStatus> m_sensorStatus;
    QList<QVector<quint16> > m_blackVals;
    QList<RadiographType> m_radiographRequestTypes;
};

#endif // CDAQUIRERADIOGRAPH_H
