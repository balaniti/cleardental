// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cddefaults.h"

#include <QDir>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickImageProvider>
#include <QDebug>
#include <QCoreApplication>
#include <QIcon>
#include <QResource>
#include <QStyleHints>
#ifdef Q_OS_LINUX
#include <QProcess>
#include <QWindow>
#endif

#include "cdtextfilemanager.h"
#include "cdgitmanager.h"
#include "cdimagemanager.h"
#include "cdtoollauncher.h"
#include "cdtoollauncher.h"
#include "cdpatientmanager.h"
#include "cdfilelocations.h"
#include "cdfilewatcher.h"
#include "cdpatientfilemanager.h"
#include "cdradiographmanager.h"
#include "cdproviderinfo.h"
#include "cdconstloader.h"
#include "cdspellchecker.h"

#define DEF_CDD_PATH QString(QDir::homePath() + "/clearDentalData/")

CDDefaults::CDDefaults(QObject *parent) : QObject(parent){}

QString CDDefaults::getBaseDataPath()
{
    return DEF_CDD_PATH;
}

QString CDDefaults::getPatDir() {
    //TODO: use a system varible to get the proper location
    return DEF_CDD_PATH + "patData/";
}

QString CDDefaults::getPatID()
{
    return QCoreApplication::arguments().at(1);
}

QString CDDefaults::getDocPrefDir() {
    //TODO: use a system varible to get the proper location
    return DEF_CDD_PATH + "docPref/";
}

QString CDDefaults::getLocationsDir()
{
    //TODO: use a system varible to get the proper location
    return DEF_CDD_PATH + "locInfo/";
}

QString CDDefaults::getLocalLocationInfoDir()
{
    //TODO: use a system varible to get the proper location
    //the trailing '/' messes up the symbolic link
    return DEF_CDD_PATH + "locInfo/local";
}

QString CDDefaults::getScheduleDir(QString location)
{
    //TODO: use a system varible to get the proper location
    return DEF_CDD_PATH + "locInfo/"+location+
            "/appointments/";
}

QString CDDefaults::getLocalScheduleDir()
{
    return DEF_CDD_PATH + "locInfo/local/appointments/";
}

QString CDDefaults::getExtraSoftwareDir()
{
    //TODO: use a system varible to get the proper location
    return DEF_CDD_PATH + "extraSoftware/";
}

void CDDefaults::setAppDefaults()
{
    QFile readNodeType("/nodeType.txt");
    if(readNodeType.open(QIODevice::Text | QIODevice::ReadOnly)) {
        QString line = readNodeType.readLine();
        if(line.contains("touch")) {
            qputenv("QT_IM_MODULE", QByteArray("qtvirtualkeyboard"));
        }
        readNodeType.close();
    }

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setOrganizationName("Clear.Dental");
    QCoreApplication::setOrganizationDomain("clear.dental");
    QIcon::setThemeName("breeze");
}

void CDDefaults::registerQMLTypes()
{
    qmlRegisterType<CDTextfileManager>("dental.clear", 1, 0, "CDTextFileManager");
    qmlRegisterType<CDGitManager>("dental.clear", 1, 0, "CDGitManager");
    qmlRegisterType<CDToolLauncher>("dental.clear", 1, 0, "CDToolLauncher");
    qmlRegisterType<CDPatientManager>("dental.clear", 1, 0, "CDPatientManager");
    qmlRegisterType<CDFileLocations>("dental.clear", 1, 0, "CDFileLocations");
    qmlRegisterType<CDFileWatcher>("dental.clear", 1, 0, "CDFileWatcher");
    qmlRegisterType<CDPatientFileManager>("dental.clear", 1, 0, "CDPatientFileManager");
    qmlRegisterType<CDRadiographManager>("dental.clear", 1, 0, "CDRadiographManager");
    qmlRegisterType<CDProviderInfo>("dental.clear", 1, 0, "CDProviderInfo");
    qmlRegisterType<CDConstLoader>("dental.clear", 1, 0, "CDConstLoader");
    qmlRegisterType<CDSpellChecker>("dental.clear", 1, 0, "CDSpellChecker");
}

QIcon CDDefaults::defaultColorIcon()
{
    QIcon returnMe(":/icons/bareLogo");

    QString programName = QCoreApplication::arguments().at(0);
    if(programName.endsWith("cleardental-schedule")) {
        returnMe = QIcon(":/icons/scheduleLogo");
    }
    else if(programName.endsWith("cleardental-review-casenotes")) {
        returnMe = QIcon(":/icons/caseNotesLogo");
    }
    else if(programName.endsWith("cleardental-new-patient")) {
        returnMe = QIcon(":/icons/importNewPatientLogo");
    }
    else if(programName.endsWith("cleardental-media-controls")) {
        returnMe = QIcon(":/icons/mediaControlsLogo");
    }
    else if(programName.endsWith("cleardental-messaging")) {
        returnMe = QIcon(":/icons/messagingLogo");
    }
    else if(programName.endsWith("cleardental-production-reports")) {
        returnMe = QIcon(":/icons/reportsLogo");
    }
    else if(programName.endsWith("cleardental-schedule")) {
        returnMe = QIcon(":/icons/scheduleLogo");
    }
    else if(programName.endsWith("cleardental-select-patient")) {
        returnMe = QIcon(":/icons/selectPatientLogo");
    }
    else if(programName.endsWith("cleardental-dentalplan-claims")) {
        returnMe = QIcon(":/icons/sendClaimsLogo");
    }
    else if(programName.endsWith("cleardental-task-manager")) {
        returnMe = QIcon(":/icons/taskManager");
    }


    return returnMe;
}

void CDDefaults::enableBlurBackground()
{
    if(QGuiApplication::platformName() == "xcb") {
        for(int i=0;i<QGuiApplication::allWindows().count();i++) {
            WId id = QGuiApplication::allWindows().at(i)->winId();
            QProcess blurProcess;
            blurProcess.setProgram("/usr/bin/xprop");
            QStringList pArgs;
            pArgs<<"-f"<<"_KDE_NET_WM_BLUR_BEHIND_REGION"<<"32c"<<"-set"<<"_KDE_NET_WM_BLUR_BEHIND_REGION"<<"0"<<"-id"<<
                   QString::number(id);
            blurProcess.setArguments(pArgs);
            blurProcess.startDetached();
        }
    }
    else if(QGuiApplication::platformName() == "wayland") {
        qDebug()<<"Wayland!!!";

    }
}

void CDDefaults::setGuiApplicationSettings(QGuiApplication *app)
{
    Q_UNUSED(app)
    //app->styleHints()->setStartDragDistance(100);

}

