// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef CDSCHEDULEREADER_H
#define CDSCHEDULEREADER_H

#include <QObject>
#include <QDate>
#include <QVariant>
#include <QVariantList>
#include <QSettings>

#include "cdappointment.h"

class CDScheduleManager : public QObject
{
    Q_OBJECT
public:
    explicit CDScheduleManager(QObject *parent = nullptr);

    Q_INVOKABLE static QStringList getAppointments(QDate date);
    Q_INVOKABLE static QStringList getAppointments(QDate date,QString getChairID);
    Q_INVOKABLE static QStringList getAppointments(QDate date,QString getChairID, int getStartTime,
                                                   int getDuration);
    Q_INVOKABLE static QStringList getAppointments(QString getPatID);
    Q_INVOKABLE static int getNoShowAppointments(QString getPatID);
    Q_INVOKABLE static QList<QString> getChairs(QDate date);
    Q_INVOKABLE static QString addAppointment(QDate getDate, QString getTime,
                                              QString getChair, QString getPatID,
                                              QString getProcedures,
                                              QString getProviderID,
                                              int getDurationMins,
                                              QString getComments);
    Q_INVOKABLE static QString addBlocker(QDate getDate, bool allDay,
                                          QString getTime, bool allChairs,
                                          QString getChair,int getDurationMins,
                                          QString getComments);
    Q_INVOKABLE static void removeAppointment(QString filename);
    Q_INVOKABLE static void moveAppointment(QString filename, QDate newDate);
    Q_INVOKABLE static int convertTimeToMinsSinceMidnight(QString inputTime);
    Q_INVOKABLE static QString convertMinsSinceMidnightToString(int inputTime);

    Q_INVOKABLE static QDateTime getDateTimeForPatDirAppt(QString apptFile);

    Q_INVOKABLE static QVariantList getSearchResult(QString searchString);
    Q_INVOKABLE static QList<QPair<QDate,QString>> getAllAppointmentsEver();

    //Q_INVOKABLE QDateTime getDateTimeForScheduleDirAppt(QString apptFile);


private:
    static QVariant addAppointmentToSearchResult(QSettings *appt, QDate getDate);
    static bool dComp(QVariantList left, QVariantList right);

};

#endif // CDSCHEDULEREADER_H
