// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cdreporter.h"
#include "cdschedulemanager.h"

#include <QSettings>
#include <QDebug>

CDReporter::CDReporter(QObject *parent) : QObject(parent){}


QList<QDateTime> CDReporter::getDaysFromRange(QDateTime startDay, QDateTime endday)
{
    QList<QDateTime> returnMe;
    QDateTime addMe = startDay;
    returnMe.append(addMe);

    while(addMe.date() != endday.date()) {
        addMe = QDateTime(addMe.date().addDays(1).startOfDay());
        returnMe.append(addMe);

    }
    return returnMe;
}

bool CDReporter::dayInRange(QDate getDate, QDateTime startDay, QDateTime endday)
{
    QDate startDate = startDay.date();
    QDate endDate = endday.date();

    return ((getDate >= startDate) && (getDate <= endDate));
}

bool CDReporter::checkValid(QDateTime startDay, QDateTime endday)
{
    return ((startDay.isValid()) && (endday.isValid()) && (startDay <= endday));
}

int CDReporter::patientsSeen(QDateTime startDay, QDateTime endday)
{
    if(!checkValid(startDay,endday)) {
        return 0;
    }

    QList<QDateTime> range = getDaysFromRange(startDay,endday);
    int returnMe =0;

    foreach(QDateTime dt, range) {
        QStringList apptFiles = CDScheduleManager::getAppointments(dt.date());

        foreach(QString appt, apptFiles) {
            QSettings apptFile(appt,QSettings::IniFormat);
            QString status = apptFile.value("Status","Unconfirmed").toString();
            if(status == "Left") {
                returnMe++;
            }
        }
    }

    return returnMe;
}

int CDReporter::patientsNoShowed(QDateTime startDay, QDateTime endday)
{
    if(!checkValid(startDay,endday)) {
        return 0;
    }

    QList<QDateTime> range = getDaysFromRange(startDay,endday);
    int returnMe =0;

    foreach(QDateTime dt, range) {
        QStringList apptFiles = CDScheduleManager::getAppointments(dt.date());

        foreach(QString appt, apptFiles) {
            QSettings apptFile(appt,QSettings::IniFormat);
            QString status = apptFile.value("Status","Unconfirmed").toString();
            if(status == "No Show") {
                returnMe++;
            }
        }
    }

    return returnMe;
}

int CDReporter::totalProduction(QDateTime startDay, QDateTime endday)
{
    if(!checkValid(startDay,endday)) {
        return 0;
    }

    int returnMe =0;
    QStringList patIds = CDPatientManager::getAllPatientIds();

    foreach (QString patID, patIds) {
        QFile ledgerFile(CDFileLocations::getFullBillingFile(patID));
        if(ledgerFile.exists()) {
            ledgerFile.open(QIODevice::ReadOnly | QIODevice::Text);
            QByteArray jsonText = ledgerFile.readAll();
            QJsonDocument ledgerDoc = QJsonDocument::fromJson(jsonText);
            QJsonArray procedureArray = ledgerDoc.array();
            foreach(QJsonValue procedureValue, procedureArray) {
                QVariantMap procedureObj = procedureValue.toObject().toVariantMap();
                if(procedureObj.contains("ProcedureDate")) {  //make sure the procedure was completed
                    QString dateStr = procedureObj.value("ProcedureDate").toString();
                    QString dateName = dateStr.section(' ', 0, 2) + " " + dateStr.section(' ', 4, 4);
                    dateName.truncate(24);
                    QDate dateObj = QDate::fromString(dateName, "ddd MMM d yyyy");
                    if(dayInRange(dateObj,startDay,endday)) { //make sure the procedure was done in rage
                        double getAmount = procedureObj.value("Procedure").toMap().value("BasePrice").toDouble();
                        if(getAmount > 0) {
                            returnMe += (int) (getAmount * 100);
                        }
                    }
                }
            }
        }
    }
    return returnMe;
}

int CDReporter::totalProduction(QDateTime startDay, QDateTime endday, QString providerID) {
    if(!checkValid(startDay,endday)) {
        return 0;
    }

    int returnMe =0;
    QStringList patIds = CDPatientManager::getAllPatientIds();

    foreach (QString patID, patIds) {
        QFile ledgerFile(CDFileLocations::getFullBillingFile(patID));
        if(ledgerFile.exists()) {
            ledgerFile.open(QIODevice::ReadOnly | QIODevice::Text);
            QByteArray jsonText = ledgerFile.readAll();
            QJsonDocument ledgerDoc = QJsonDocument::fromJson(jsonText);
            QJsonArray procedureArray = ledgerDoc.array();
            foreach(QJsonValue procedureValue, procedureArray) {
                QVariantMap procedureObj = procedureValue.toObject().toVariantMap();
                if(procedureObj.contains("ProcedureDate")) {  //make sure the procedure was completed
                    QString dateStr = procedureObj.value("ProcedureDate").toString();
                    QString dateName = dateStr.section(' ', 0, 2) + " " + dateStr.section(' ', 4, 4);
                    dateName.truncate(24);
                    QDate dateObj = QDate::fromString(dateName, "ddd MMM d yyyy");
                    if(dayInRange(dateObj,startDay,endday)) { //make sure the procedure was done in rage
                        QString whoDidProcedure = procedureObj.value("ProviderUsername").toString();
                        if(providerID == whoDidProcedure) { //make sure it was done by the right person
                            double getAmount = procedureObj.value("Procedure").toMap().value("BasePrice").toDouble();
                            if(getAmount > 0) {
                                returnMe += (int) (getAmount * 100);
                            }
                        }
                    }
                }
            }
        }
    }
    return returnMe;
}

int CDReporter::totalCollections(QDateTime startDay, QDateTime endday)
{
    if(!checkValid(startDay,endday)) {
        return 0;
    }

    int returnMe =0;
    QStringList patIds = CDPatientManager::getAllPatientIds();

    foreach (QString patID, patIds) {
        QFile ledgerFile(CDFileLocations::getFullBillingFile(patID));
        if(ledgerFile.exists()) {
            ledgerFile.open(QIODevice::ReadOnly | QIODevice::Text);
            QByteArray jsonText = ledgerFile.readAll();
            QJsonDocument ledgerDoc = QJsonDocument::fromJson(jsonText);
            QJsonArray procedureArray = ledgerDoc.array();
            foreach(QJsonValue procedureValue, procedureArray) {
                QVariantMap procedureObj = procedureValue.toObject().toVariantMap();
                QVariantList paymentList = procedureObj.value("Payments").toList();
                for(int i=0;i<paymentList.length();i++) {
                    QVariantMap paymentItem = paymentList.at(i).toMap();
                    QDate paymentDate = QDate::fromString(paymentItem.value("Date").toString(),Qt::ISODate);
                    if(dayInRange(paymentDate,startDay,endday)) { //make sure the procedure was done in rage
                        double getAmount = paymentItem.value("Amount").toDouble();
                        bool isCredit =  (paymentItem.value("Type").toString() == "Credit");
                        if((getAmount > 0) && (!isCredit)) {
                            returnMe += (int) (getAmount * 100);
                        }
                    }
                }
            }
        }
    }
    return returnMe;
}

int CDReporter::totalCollections(QDateTime startDay, QDateTime endday, QString providerID)
{
    if(!checkValid(startDay,endday)) {
        return 0;
    }

    int returnMe =0;
    QStringList patIds = CDPatientManager::getAllPatientIds();

    foreach (QString patID, patIds) {
        QFile ledgerFile(CDFileLocations::getFullBillingFile(patID));
        if(ledgerFile.exists()) {
            ledgerFile.open(QIODevice::ReadOnly | QIODevice::Text);
            QByteArray jsonText = ledgerFile.readAll();
            QJsonDocument ledgerDoc = QJsonDocument::fromJson(jsonText);
            QJsonArray procedureArray = ledgerDoc.array();
            foreach(QJsonValue procedureValue, procedureArray) {
                QVariantMap procedureObj = procedureValue.toObject().toVariantMap();
                QVariantList paymentList = procedureObj.value("Payments").toList();
                for(int i=0;i<paymentList.length();i++) {
                    QVariantMap paymentItem = paymentList.at(i).toMap();
                    QDate paymentDate = QDate::fromString(paymentItem.value("Date").toString(),Qt::ISODate);
                    if(dayInRange(paymentDate,startDay,endday)) { //make sure the procedure was done in rage
                        QString whoDidProcedure = procedureObj.value("ProviderUsername").toString();
                        if(providerID == whoDidProcedure) { //make sure it was done by the right person
                            double getAmount = paymentItem.value("Amount").toDouble();
                            bool isCredit =  (paymentItem.value("Type").toString() == "Credit");
                            if((getAmount > 0) && (!isCredit)) {
                                returnMe += (int) (getAmount * 100);
                            }
                        }
                    }
                }
            }
        }
    }
    return returnMe;
}

int CDReporter::totalWriteoffs(QDateTime startDay, QDateTime endday)
{
    if(!checkValid(startDay,endday)) {
        return 0;
    }

    int returnMe =0;
    QStringList patIds = CDPatientManager::getAllPatientIds();

    foreach (QString patID, patIds) {
        QFile ledgerFile(CDFileLocations::getFullBillingFile(patID));
        if(ledgerFile.exists()) {
            ledgerFile.open(QIODevice::ReadOnly | QIODevice::Text);
            QByteArray jsonText = ledgerFile.readAll();
            QJsonDocument ledgerDoc = QJsonDocument::fromJson(jsonText);
            QJsonArray procedureArray = ledgerDoc.array();
            foreach(QJsonValue procedureValue, procedureArray) {
                QVariantMap procedureObj = procedureValue.toObject().toVariantMap();
                QVariantList paymentList = procedureObj.value("Payments").toList();
                for(int i=0;i<paymentList.length();i++) {
                    QVariantMap paymentItem = paymentList.at(i).toMap();
                    QDate paymentDate = QDate::fromString(paymentItem.value("Date").toString(),Qt::ISODate);
                    if(dayInRange(paymentDate,startDay,endday)) { //make sure the procedure was done in rage
                        double getAmount =0;

                        if(paymentItem.value("Type") == "Credit") {
                            getAmount = paymentItem.value("Amount").toDouble();
                        }

                        else if(paymentItem.value("Type") == "Dental Plan Payment") {
                            getAmount = paymentItem.value("WriteOff").toDouble();
                        }

                        if(getAmount > 0) {
                            returnMe += (int) (getAmount * 100);
                        }
                    }
                }
            }
        }
    }
    return returnMe;
}

int CDReporter::totalAmountBilledOut(QDateTime startDay, QDateTime endday) {
    if(!checkValid(startDay,endday)) {
        return 0;
    }

    int returnMe =0;
    QStringList patDirs = CDPatientManager::getAllPatientDirs();

    foreach (QString patDir, patDirs) {
        QFile ledgerFile(patDir + "/claims.json");
//    QStringList patIDs = CDPatientManager::getAllPatientIds();
//    foreach (QString patID, patIDs) {
//        QFile ledgerFile(CDFileLocations::getClaimsFile(patID));
        if(ledgerFile.exists()) {
            ledgerFile.open(QIODevice::ReadOnly | QIODevice::Text);
            QByteArray jsonText = ledgerFile.readAll();
            QJsonDocument ledgerDoc = QJsonDocument::fromJson(jsonText);
            QJsonArray itemArray = ledgerDoc.array();
            foreach(QJsonValue item, itemArray) {
                QJsonObject obj = item.toObject();
                if(obj.contains("SentDate")) {
                    QString date = obj.value("SentDate").toString();
                    QStringList dateList = date.split("/");
                    QDate dateObj(dateList[2].toInt(),dateList[0].toInt(),dateList[1].toInt());
                    if(dayInRange(dateObj,startDay,endday)) {
                        double getAmount = obj.value("ClaimAmount").toVariant().toDouble();
                        returnMe += (int) (getAmount * 100);
                    }
                }
            }
        }
    }
    return returnMe;
}

QStringList CDReporter::getDueBalances()
{
    QStringList returnMe;

    //int start = QTime::currentTime().msecsSinceStartOfDay();

    QStringList patIDs = CDPatientManager::getAllPatientIds();
    foreach (QString patID, patIDs) {
        int bal = getBalance(patID);
        if(bal>1) {
            returnMe.append(patID);
        }
    }

    //int end = QTime::currentTime().msecsSinceStartOfDay();
    //qDebug()<<(end-start);

    return returnMe;
}

int CDReporter::getBalance(QString patID)
{
    int returnMe=0;
    returnMe += getCharges(patID);
    returnMe -= getPayments(patID);
    return returnMe;
}

QString CDReporter::getDentalPlanName(QString patID)
{
    QString returnMe;

    QSettings dentalPlanInfoSettings(CDFileLocations::getDentalPlansFile(patID),QSettings::IniFormat);
    dentalPlanInfoSettings.beginGroup("Primary");
    returnMe = dentalPlanInfoSettings.value("Name","").toString();
    if(returnMe.length() <1) {
        dentalPlanInfoSettings.beginGroup("Membership");
        bool hasMembership = dentalPlanInfoSettings.value("HasMembership",false).toBool();
        if(hasMembership) {
            returnMe = "Square Membership Plan";
        }
        else {
            returnMe = "Cash";
        }
    }

    return returnMe;
}

int CDReporter::getPayments(QString patID)
{
    int returnMe = 0;
    QFile ledgerFile(CDFileLocations::getFullBillingFile(patID));
    if(ledgerFile.exists()) {
        ledgerFile.open(QIODevice::ReadOnly | QIODevice::Text);
        QByteArray jsonText = ledgerFile.readAll();
        QJsonDocument ledgerDoc = QJsonDocument::fromJson(jsonText);
        QJsonArray procedureArray = ledgerDoc.array();
        foreach(QJsonValue jProcedure, procedureArray) {
            QVariantList paymentList = jProcedure.toObject().toVariantMap().value("Payments").toList();
            for(int x=0;x<paymentList.length();x++) {
                QVariantMap paymentInfo = paymentList.at(x).toMap();
                double getAmount = paymentInfo.value("Amount").toDouble();
                returnMe += (int) (getAmount * 100);
                QString paymentType = paymentInfo.value("Type").toString();
                if(paymentType == "Dental Plan Payment") {
                    getAmount = paymentInfo.value("WriteOff").toDouble();
                    returnMe += (int) (getAmount * 100);
                }
            }
        }
    }
    return returnMe;
}

int CDReporter::getCollections(QString patID)
{
    int returnMe = 0;
    QFile ledgerFile(CDFileLocations::getFullBillingFile(patID));
    if(ledgerFile.exists()) {
        ledgerFile.open(QIODevice::ReadOnly | QIODevice::Text);
        QByteArray jsonText = ledgerFile.readAll();
        QJsonDocument ledgerDoc = QJsonDocument::fromJson(jsonText);
        QJsonArray procedureArray = ledgerDoc.array();
        foreach(QJsonValue jProcedure, procedureArray) {
            QVariantList paymentList = jProcedure.toObject().toVariantMap().value("Payments").toList();
            for(int x=0;x<paymentList.length();x++) {
                QVariantMap paymentInfo = paymentList.at(x).toMap();
                QString paymentType = paymentInfo.value("Type").toString();
                if(paymentType != "Credit") {
                    double getAmount = paymentInfo.value("Amount").toDouble();
                    returnMe += (int) (getAmount * 100);
                }
            }
        }
    }
    return returnMe;

}

int CDReporter::getCharges(QString patID)
{
    int returnMe = 0;
    QFile ledgerFile(CDFileLocations::getFullBillingFile(patID));
    if(ledgerFile.exists()) {
        ledgerFile.open(QIODevice::ReadOnly | QIODevice::Text);
        QByteArray jsonText = ledgerFile.readAll();
        QJsonDocument ledgerDoc = QJsonDocument::fromJson(jsonText);
        QJsonArray procedureArray = ledgerDoc.array();
        foreach(QJsonValue jProcedure, procedureArray) {
            QVariantMap procedureInfo = jProcedure.toObject().toVariantMap().value("Procedure").toMap();
            double getAmount = procedureInfo.value("BasePrice").toDouble();
            returnMe += (int) (getAmount * 100);
        }
    }
    return returnMe;
}

QVariantMap CDReporter::getProcedureCounts(QDateTime startDay, QDateTime endday)
{
    QVariantMap returnMe;
    if(!checkValid(startDay,endday)) {
        return returnMe;
    }
    QStringList patIDs = CDPatientManager::getAllPatientIds();
    foreach (QString patID, patIDs) {
        QFile ledgerFile(CDFileLocations::getFullBillingFile(patID));
        if(ledgerFile.exists()) {
            ledgerFile.open(QIODevice::ReadOnly | QIODevice::Text);
            QByteArray jsonText = ledgerFile.readAll();
            QJsonDocument ledgerDoc = QJsonDocument::fromJson(jsonText);
            QJsonArray procedureArray = ledgerDoc.array();
            foreach(QJsonValue procedureValue, procedureArray) {
                QVariantMap procedureObj = procedureValue.toObject().toVariantMap();
                if(procedureObj.contains("ProcedureDate")) {  //make sure the procedure was completed
                    QString dateStr = procedureObj.value("ProcedureDate").toString();
                    QString dateName = dateStr.section(' ', 0, 2) + " " + dateStr.section(' ', 4, 4);
                    dateName.truncate(24);
                    QDate dateObj = QDate::fromString(dateName, "ddd MMM d yyyy");
                    if(dayInRange(dateObj,startDay,endday)) { //make sure the procedure was done in rage
                        QString procedureName =  procedureObj.value("Procedure").toMap().value("ProcedureName").toString();
                        QString dCode = procedureObj.value("Procedure").toMap().value("DCode").toString();
                        QVariantList paymentList = procedureObj.value("Payments").toList();
                        int collections=0;
                        for(int x=0;x<paymentList.length();x++) {
                            QVariantMap paymentInfo = paymentList.at(x).toMap();
                            QString paymentType = paymentInfo.value("Type").toString();
                            if(paymentType != "Credit") {
                                double getAmount = paymentInfo.value("Amount").toDouble();
                                collections += (int) (getAmount * 100);
                            }
                        }

                        QVariantList proList;
                        if(returnMe.contains(dCode)) {
                            proList = returnMe[dCode].toList();
                            int counter = proList[0].toInt();
                            proList[0] = counter +1;
                            int colSoFar = proList[2].toInt();
                             proList[2] = colSoFar + collections;
                        }
                        else {
                            proList.append(1);
                            proList.append(procedureName);
                            proList.append(collections);
                        }

                        returnMe[dCode] = proList;

                    }
                }
            }
        }
    }

    return returnMe;
}

QStringList CDReporter::needsAppt()
{
    QStringList returnMe;

    QStringList patIDs = CDPatientManager::getAllPatientIds();
    foreach (QString patID, patIDs) {
        QStringList apptFiles = CDScheduleManager::getAppointments(patID);

        QString fileName = CDReporter::getLastApptFile(patID);
        QFile lastAppt(fileName);
        if (lastAppt.exists()) {
            QString dateName = fileName.section('/',-1,-1);
            dateName = dateName.section('@',0,0);
            QDate nextAppt = QDate::fromString(dateName,"MMM-d-yyyy");
            QDate today = QDate::currentDate();

            QString patFilename = CDFileLocations::getPersonalFile(patID);
            QSettings reader(patFilename, QSettings::IniFormat);
            reader.beginGroup("Preferences");
            bool dismissed = reader.value("Dismissed").toBool();
            bool doNotSchedule = reader.value("DoNotSchedule").toBool();
            reader.endGroup();
            bool goodToSchedule = !(dismissed || doNotSchedule);
            if ((today > nextAppt) && goodToSchedule) {
                returnMe.append(patID);
            }
        }
    }
    return returnMe;
}

QString CDReporter::getLastAppt(QString patID)
{
    QString fileName = CDReporter::getLastApptFile(patID);
    QFile lastAppt(fileName);
    if (lastAppt.exists()) {
        QString dateName = fileName.section('/',-1,-1);
        dateName = dateName.section('@',0,0);
        return QDate::fromString(dateName,"MMM-d-yyyy").toString("dddd, MMMM d, yyyy");
    } else {
        return "No last appointment";
    }
}

QString CDReporter::getLastApptFile(QString patID)
{
    QStringList apptFiles = CDScheduleManager::getAppointments(patID);
    QString fileName = "";
    if (apptFiles.size() > 0) {
        QString futureApptPString = apptFiles[0];
        QString futureApptFString = futureApptPString.section("/",-1,-1);
        QString futureApptString = futureApptFString.split("@")[0];
        QDate mostFutureApptDate = QDate::fromString(futureApptString, "MMM-d-yyyy");

        for (int apptI = 0; apptI < apptFiles.length(); apptI++) {
            QString currApptPathString = apptFiles[apptI];
            QString currApptFileString = currApptPathString.section("/", -1, -1);
            QString currApptString = currApptFileString.split("@")[0];
            QDate currApptDate = QDate::fromString(currApptString, "MMM-d-yyyy");

            if (mostFutureApptDate.daysTo(currApptDate) > 0) {
                mostFutureApptDate = currApptDate;
                fileName = apptFiles[apptI];
            }
        }
    }
    return fileName;
}

int CDReporter::getTotalPatCount()
{
    QStringList patIDs = CDPatientManager::getAllPatientIds();
    return patIDs.count();
}

int CDReporter::get30DayPatCount()
{
    int returnMe=0;
    QStringList patIDs = CDPatientManager::getAllPatientIds();
    uint64_t thirtyDaysAgo = QDateTime::currentSecsSinceEpoch() - 2592000;
    foreach(QString patID, patIDs) {
        QString interviewLoc = CDFileLocations::getNewPatInterviewFile(patID);
        QSettings readInterview(interviewLoc,QSettings::IniFormat);
        uint64_t submitSecs = readInterview.value("SubmitTime",0).toULongLong();
        if(submitSecs > thirtyDaysAgo) {
            returnMe++;
        }
    }
    return returnMe;
}

QVariantMap CDReporter::getTotalReferrals()
{
    QStringList patIDs = CDPatientManager::getAllPatientIds();
    QVariantMap returnMe;
    //returnMe: {ReferralSource , { patientType : count} }
    foreach(QString patID, patIDs) {
        QString interviewLoc = CDFileLocations::getNewPatInterviewFile(patID);
        QSettings readInterview(interviewLoc,QSettings::IniFormat);
        QString refSource = readInterview.value("RefSource","").toString();
        QString patStatusString = QVariant::fromValue(getPatientStatusType(patID)).toString();

        if(refSource.length() > 0) {
            QVariantMap refMap = returnMe.value(refSource,QVariantMap()).toMap();
            int count = refMap.value(patStatusString,0).toInt();
            count++;
            refMap[patStatusString] = count;
            returnMe[refSource] = refMap;
        }

        QVariantMap totalMap = returnMe.value("Total",QVariantMap()).toMap();
        int totalCount = totalMap.value(patStatusString,0).toInt();
        totalCount++;
        totalMap[patStatusString] = totalCount;
        returnMe["Total"] = totalMap;
    }

    return returnMe;
}

QVariantMap CDReporter::get30DayReferrals()
{
    QVariantMap returnMe;
    QStringList patIDs = CDPatientManager::getAllPatientIds();
    uint64_t thirtyDaysAgo = QDateTime::currentSecsSinceEpoch() - 2592000;

    foreach(QString patID, patIDs) {
        QString interviewLoc = CDFileLocations::getNewPatInterviewFile(patID);
        QSettings readInterview(interviewLoc,QSettings::IniFormat);
        uint64_t submitSecs = readInterview.value("SubmitTime",0).toULongLong();
        if(submitSecs > thirtyDaysAgo) {
            QString refSource = readInterview.value("RefSource","").toString();
            QString patStatusString = QVariant::fromValue(getPatientStatusType(patID)).toString();

            if(refSource.length() > 0) {
                QVariantMap refMap = returnMe.value(refSource,QVariantMap()).toMap();
                int count = refMap.value(patStatusString,0).toInt();
                count++;
                refMap[patStatusString] = count;
                returnMe[refSource] = refMap;
            }

            QVariantMap totalMap = returnMe.value("Total",QVariantMap()).toMap();
            int totalCount = totalMap.value(patStatusString,0).toInt();
            totalCount++;
            totalMap[patStatusString] = totalCount;
            returnMe["Total"] = totalMap;
        }
    }


    return returnMe;
}

CDReporter::PatientStatusType CDReporter::getPatientStatusType(QString patID)
{
    PatientStatusType returnMe = INVALID_STATUS_TYPE;
    QStringList apptFileList = CDScheduleManager::getAppointments(patID);
    qint64 currentUnixTime = QDateTime::currentSecsSinceEpoch();
    QSettings personalFileReader(CDFileLocations::getPersonalFile(patID),QSettings::IniFormat);
    personalFileReader.beginGroup("Preferences");
    bool isDismissed = personalFileReader.value("Dismissed",false).toBool();
    bool isDoNotSchedule = personalFileReader.value("DoNotSchedule",false).toBool();

    if(isDismissed || isDoNotSchedule) { //first check if the patient was dismissed (hard or soft)
        returnMe = DISMISSED;
    }
    else if(apptFileList.length() == 0) {
        returnMe = NEVERMINDER;
    }
    else if(apptFileList.length() == 1) {
        QSettings readAppt(apptFileList.at(0),QSettings::IniFormat);
        QString apptStatus = readAppt.value("Status","Unconfirmed").toString();
        QDateTime apptDT = CDScheduleManager::getDateTimeForPatDirAppt(apptFileList.at(0));
        qint64 unixTime = apptDT.toSecsSinceEpoch();
        if(unixTime > currentUnixTime ) {
            returnMe = ACTIVE_PATIENT;
        }
        else if(apptStatus == "Unconfirmed") {
            returnMe = NEVER_SEEN;
        }
        else if(apptStatus == "No Show") {
            returnMe = NEVER_SEEN;
        }
        else if(apptStatus == "Left") {
            returnMe = ONCE_SEEN;
        }
        else {
            returnMe = FORGOTTEN;
        }
    }
    else {
        //first need to find the "last" appointment the patient had
        int lastApptIndex =0;
        int lastApptTime =0; //UNIX time
        for(int i=0;(i<apptFileList.length()) && (returnMe!=ACTIVE_PATIENT);i++) {
            QDateTime apptDT = CDScheduleManager::getDateTimeForPatDirAppt(apptFileList.at(i));
            qint64 unixTime = apptDT.toSecsSinceEpoch();
            if(unixTime > currentUnixTime ) {
                returnMe = ACTIVE_PATIENT;
            }
            else if(unixTime > lastApptTime) {
                lastApptIndex = i;
                lastApptTime = unixTime;
            }
        }
        if(returnMe != ACTIVE_PATIENT) {
            QSettings readAppt(apptFileList.at(lastApptIndex),QSettings::IniFormat);
            QString apptStatus = readAppt.value("Status","Unconfirmed").toString();
            if(apptStatus == "Left") {
                returnMe = FORGOTTEN;
            }
            else if(apptStatus == "No Show") {
                returnMe = GHOSTED;
            }
            else {
                returnMe = FORGOTTEN;
            }
        }
    }

//    if(returnMe == INVALID_STATUS_TYPE) {
//        qDebug()<<"Invalid Type: " << patID;
//    }

    return returnMe;

}

qreal CDReporter::getCollectionsFromRefferalType(QString referralSource)
{
    qreal returnMe =0.0;
    QStringList patIDs = CDPatientManager::getAllPatientIds();
    foreach(QString patID, patIDs) {
        QString interviewLoc = CDFileLocations::getNewPatInterviewFile(patID);
        QSettings readInterview(interviewLoc,QSettings::IniFormat);
        QString patRefSource = readInterview.value("RefSource","").toString();

        if(patRefSource == referralSource) {
            returnMe += getCollections(patID);
        }
    }

    return returnMe;

}
