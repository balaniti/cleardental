// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef CDDRUGLIST_H
#define CDDRUGLIST_H

#include <QObject>

class CDDrugList : public QObject
{
    Q_OBJECT
public:
    explicit CDDrugList(QObject *parent = nullptr);

signals:

public slots:
    static QStringList getSmallDrugList();
    static QStringList getBigDrugList();
};

#endif // CDDRUGLIST_H
