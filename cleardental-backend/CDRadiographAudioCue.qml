// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Window 2.10
import QtMultimedia 5.12

Item {

    property string playSecond: ""
    property string playThird: ""

    function playReady(getType) {
        cuePlaylist.clear();
        cuePlaylist.addItem("qrc:/voice/readyFor");

        switch(getType) {
        case CDRadiographSensor.PA_Maxillary_Right_Distal:
            cuePlaylist.addItem("qrc:/voice/post");
            cuePlaylist.addItem("qrc:/voice/max");
            cuePlaylist.addItem("qrc:/voice/right");
            cuePlaylist.addItem("qrc:/voice/distal");
            cuePlaylist.addItem("qrc:/voice/pa");
            break;
        case CDRadiographSensor.PA_Maxillary_Right_Mesial:
            cuePlaylist.addItem("qrc:/voice/post");
            cuePlaylist.addItem("qrc:/voice/max");
            cuePlaylist.addItem("qrc:/voice/right");
            cuePlaylist.addItem("qrc:/voice/mesial");
            cuePlaylist.addItem("qrc:/voice/pa");
            break;
        case CDRadiographSensor.PA_Maxillary_Anterior_Right:
            cuePlaylist.addItem("qrc:/voice/ant");
            cuePlaylist.addItem("qrc:/voice/max");
            cuePlaylist.addItem("qrc:/voice/right");
            cuePlaylist.addItem("qrc:/voice/pa");
            break;
        case CDRadiographSensor.PA_Maxillary_Anterior_Center:
            cuePlaylist.addItem("qrc:/voice/ant");
            cuePlaylist.addItem("qrc:/voice/max");
            cuePlaylist.addItem("qrc:/voice/center");
            cuePlaylist.addItem("qrc:/voice/pa");
            break;
        case CDRadiographSensor.PA_Maxillary_Anterior_Left:
            cuePlaylist.addItem("qrc:/voice/ant");
            cuePlaylist.addItem("qrc:/voice/max");
            cuePlaylist.addItem("qrc:/voice/left");
            cuePlaylist.addItem("qrc:/voice/pa");
            break;
        case CDRadiographSensor.PA_Maxillary_Left_Mesial:
            cuePlaylist.addItem("qrc:/voice/post");
            cuePlaylist.addItem("qrc:/voice/max");
            cuePlaylist.addItem("qrc:/voice/left");
            cuePlaylist.addItem("qrc:/voice/mesial");
            cuePlaylist.addItem("qrc:/voice/pa");
            break;
        case CDRadiographSensor.PA_Maxillary_Left_Distal:
            cuePlaylist.addItem("qrc:/voice/post");
            cuePlaylist.addItem("qrc:/voice/max");
            cuePlaylist.addItem("qrc:/voice/left");
            cuePlaylist.addItem("qrc:/voice/distal");
            cuePlaylist.addItem("qrc:/voice/pa");
            break;
        case CDRadiographSensor.BW_Right_Distal:
            cuePlaylist.addItem("qrc:/voice/bw");
            cuePlaylist.addItem("qrc:/voice/right");
            cuePlaylist.addItem("qrc:/voice/distal");
            break;
        case CDRadiographSensor.BW_Right_Mesial:
            cuePlaylist.addItem("qrc:/voice/bw");
            cuePlaylist.addItem("qrc:/voice/right");
            cuePlaylist.addItem("qrc:/voice/mesial");
            break;
        case CDRadiographSensor.BW_Left_Mesial:
            cuePlaylist.addItem("qrc:/voice/bw");
            cuePlaylist.addItem("qrc:/voice/left");
            cuePlaylist.addItem("qrc:/voice/mesial");
            break;
        case CDRadiographSensor.BW_Left_Distal:
            cuePlaylist.addItem("qrc:/voice/bw");
            cuePlaylist.addItem("qrc:/voice/left");
            cuePlaylist.addItem("qrc:/voice/distal");
            break;
        case CDRadiographSensor.PA_Mandibular_Right_Distal:
            cuePlaylist.addItem("qrc:/voice/post");
            cuePlaylist.addItem("qrc:/voice/man");
            cuePlaylist.addItem("qrc:/voice/right");
            cuePlaylist.addItem("qrc:/voice/distal");
            cuePlaylist.addItem("qrc:/voice/pa");
            break;
        case CDRadiographSensor.PA_Mandibular_Right_Mesial:
            cuePlaylist.addItem("qrc:/voice/post");
            cuePlaylist.addItem("qrc:/voice/man");
            cuePlaylist.addItem("qrc:/voice/right");
            cuePlaylist.addItem("qrc:/voice/mesial");
            cuePlaylist.addItem("qrc:/voice/pa");
            break;
        case CDRadiographSensor.PA_Mandibular_Anterior_Right:
            cuePlaylist.addItem("qrc:/voice/ant");
            cuePlaylist.addItem("qrc:/voice/man");
            cuePlaylist.addItem("qrc:/voice/right");
            cuePlaylist.addItem("qrc:/voice/pa");
            break;
        case CDRadiographSensor.PA_Mandibular_Anterior_Center:
            cuePlaylist.addItem("qrc:/voice/ant");
            cuePlaylist.addItem("qrc:/voice/man");
            cuePlaylist.addItem("qrc:/voice/center");
            cuePlaylist.addItem("qrc:/voice/pa");
            break;
        case CDRadiographSensor.PA_Mandibular_Anterior_Left:
            cuePlaylist.addItem("qrc:/voice/ant");
            cuePlaylist.addItem("qrc:/voice/man");
            cuePlaylist.addItem("qrc:/voice/left");
            cuePlaylist.addItem("qrc:/voice/pa");
            break;
        case CDRadiographSensor.PA_Mandibular_Left_Mesial:
            cuePlaylist.addItem("qrc:/voice/post");
            cuePlaylist.addItem("qrc:/voice/man");
            cuePlaylist.addItem("qrc:/voice/left");
            cuePlaylist.addItem("qrc:/voice/mesial");
            cuePlaylist.addItem("qrc:/voice/pa");
            break;
        case CDRadiographSensor.PA_Mandibular_Left_Distal:
            cuePlaylist.addItem("qrc:/voice/post");
            cuePlaylist.addItem("qrc:/voice/man");
            cuePlaylist.addItem("qrc:/voice/left");
            cuePlaylist.addItem("qrc:/voice/distal");
            cuePlaylist.addItem("qrc:/voice/pa");
            break;
        }

        audioComp.play();
    }


    Audio {
        id: audioComp
        playlist: cuePlaylist
    }

    Playlist {
        id: cuePlaylist
    }


}
