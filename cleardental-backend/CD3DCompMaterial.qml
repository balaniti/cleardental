// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9

import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Extras 2.15
import QtQuick.Scene3D 2.14

Entity {

    id: compToothEntity

    property bool isTxPlan: false
    property string toothAspect: ""
    property string baseMeshSource: ""

    CD3DToothPhongMaterial {
        id: compMaterial
        property real alphaAmount: .5
        makeColor: Qt.rgba(0,0,1,alphaAmount);

        SequentialAnimation {
            PropertyAnimation {
                duration: 500
                from: .2
                to: .8
                target: compMaterial
                property: "alphaAmount"
            }
            PropertyAnimation {
                duration: 500
                from: .8
                to: .2
                target: compMaterial
                property: "alphaAmount"
            }
            loops: Animation.Infinite
            running: isTxPlan
        }

    }

    Mesh {
        id: compToothMesh
        source: baseMeshSource
        meshName: toothAspect
    }

    CD3DAnimatedRotation {
        id: compTransForm
        scale: 1.01
    }



    components: [ compToothMesh, compMaterial, compTransForm ]

}
