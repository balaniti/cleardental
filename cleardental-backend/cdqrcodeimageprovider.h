// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef CDQRCODEIMAGEPROVIDER_H
#define CDQRCODEIMAGEPROVIDER_H

#include <QQuickImageProvider>
#include <QImage>

class CDQRCodeImageProvider : public QQuickImageProvider
{
public:
    CDQRCodeImageProvider();
    QImage requestImage(const QString &id, QSize *size, const QSize &requestedSize) override;

};

#endif // CDQRCODEIMAGEPROVIDER_H
