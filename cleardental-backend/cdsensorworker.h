// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef CDSENSORWORKER_H
#define CDSENSORWORKER_H

#include <QThread>

class CDSensorWorker : public QThread
{
    Q_OBJECT
public:
    explicit CDSensorWorker(QObject *parent = nullptr);

    enum RequestType {
        BLACK_IMAGE,
        EXPOSED_IMAGE,
        CANCEL,
        INVALID_REQUEST
    };

    void run();

    void setDeviceIndex(int setIndex);
    void setRequestType(RequestType getRequest);
    QVector<quint16> getResults();


signals:
    void gotBlackImage(int sensorIndex, QVector<quint16> results);
    void gotExposedImage(int sensorIndex, QVector<quint16> results);


private:
    int m_deviceIndex;
    RequestType m_requestType;
    QVector<quint16> m_results;
};

#endif // CDSENSORWORKER_H
