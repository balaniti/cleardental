// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cdclearinghousecomm.h"

#include <QSettings>
#include <QJsonDocument>
#include <QJsonArray>
#include <QDir>
#include <QDebug>
#include <QDate>
#include <QDateTime>
#include <QDomDocument>
#include <QDomElement>
#include <QTextStream>
#include <QFile>
#include <QFileInfo>

#include "cdfilelocations.h"
#include "cdproviderinfo.h"
#include "cdprinter.h"

#define END_SEGMENT QString("~\n")
#define SEP_ELEMENT QString("*")
#define SEP_SUB_ELEMENT QString(":")
#define REP_ID QString("^")

CDClearinghouseComm::CDClearinghouseComm(QObject *parent) : QObject(parent)
{

}

QString CDClearinghouseComm::generate837ClaimString(QVariantMap inputData, bool claimPrimary)
{
    QString returnMe ="";

    QSettings dentalPlanInfo(CDFileLocations::getDentalPlansFile(inputData.value("PatientName").toString()),
                             QSettings::IniFormat);
    if(claimPrimary) {
        dentalPlanInfo.beginGroup("Primary");
    }
    else {
        dentalPlanInfo.beginGroup("Secondary");
    }

    QSettings patInfo(CDFileLocations::getPersonalFile(inputData.value("PatientName").toString()),
                      QSettings::IniFormat);

    QSettings docInfoSettings(CDFileLocations::getDocInfoFile(inputData.value("ProviderUnixID","").toString()),
                              QSettings::IniFormat);
    QSettings localInfoSettings(CDFileLocations::getLocalPracticePreferenceFile(),QSettings::IniFormat);
    localInfoSettings.beginGroup("GeneralInfo");
    int icnCounter = localInfoSettings.value("InsuranceInterchangeControlNumberCounter",1).toInt();
    localInfoSettings.setValue("InsuranceInterchangeControlNumberCounter",icnCounter+1);
    QString icnStr = QString::number(icnCounter).rightJustified(9,'0');

//https://www.aftermarketsuppliers.org/sites/aftermarketsuppliers.org/files/isa%20iea%20interchg.pdf
//https://dentaquest.com/getattachment/State-Plans/Regions/Colorado/Health-First-Colorado/Provider-Page/Training-and-Provider-Relations/477272_1_1_HIPAA-Companion-Guide.pdf/?lang=en-US
//https://www.mvphealthcare.com/wp-content/uploads/download-manager-files/MVP-Health-Care-Dental-Implementation-Guide-837D.pdf
//https://www.ihs.gov/rpms/training/course-materials/?p=rpmsTraining%5CThirdPartyBillingAndAccountsReceivable%5C11_Understanding_837_5010_Files.pdf&flname=11_Understanding_837_5010_Files.pdf&download=1


    //First, lets start off with the ISA Header
    returnMe+= "ISA" + SEP_ELEMENT;
    returnMe+="00" + SEP_ELEMENT;                           //AUTHORIZATION INFORMATION QUALIFIER (just "00") [ISA01]
    returnMe+=makePad("",10) + SEP_ELEMENT;                 //AUTHORIZATION INFORMATION (blank) [ISA02]
    returnMe+="00" + SEP_ELEMENT;                           //SECURITY INFORMATION QUALIFIER [ISA03]
    returnMe+=makePad("",10) + SEP_ELEMENT;                 //SECURITY INFORMATION (blank) [ISA04]
    returnMe+="ZZ" + SEP_ELEMENT;                           //INTERCHANGE ID QUALIFIER (SenderID / Federal EIN) [ISA05]
    returnMe+=makePad(localInfoSettings.value("FederalTaxID","").
                      toString(),14) + SEP_ELEMENT;         //INTERCHANGE ID (SenderID / Federal EIN) [ISA06]
    returnMe+="ZZ" + SEP_ELEMENT;                           //INTERCHANGE ID QUALIFIER (RECEIVER1) [ISA07]
    returnMe+=dentalPlanInfo.value("ClearingHouseID").toString()
            + SEP_ELEMENT;                                  //INTERCHANGE ID QUALIFIER (RECEIVER1) [ISA08]
    returnMe+=QDate::currentDate().toString("yyMMdd")
            + SEP_ELEMENT;                                  //InterchangeDate [ISA09]
    returnMe+=QTime::currentTime().toString("HHmm")+
            SEP_ELEMENT ;                                   //InterchangeTime [ISA10]
    returnMe+=REP_ID + SEP_ELEMENT;                         //INTERCHANGE CONTROL STANDARDS IDENTIFIER [ISA11]
    returnMe+= "00501"+ SEP_ELEMENT;                        //INTERCHANGE CONTROL VERSION NUMBER [ISA12]
    returnMe+= icnStr+SEP_ELEMENT;                          //InterchangeControlNumber [ISA13]
    returnMe+= "1"+ SEP_ELEMENT;                            //ACKNOWLEDGMENT REQUESTED [ISA14]
    #ifdef QT_DEBUG
        returnMe+= "T"+ SEP_ELEMENT;                        //TEST INDICATOR [ISA15]
    #else
        returnMe+= "P"+ SEP_ELEMENT;                        //TEST INDICATOR [ISA15]
    #endif
    returnMe+=SEP_SUB_ELEMENT+END_SEGMENT;

    // GS-Functional Group Header
    returnMe += "GS"+SEP_ELEMENT+  //[GS header]
            "HC"+SEP_ELEMENT; //[GS01]
    returnMe+=makePad(localInfoSettings.value("FederalTaxID","").
                      toString(),14) + SEP_ELEMENT;         //INTERCHANGE ID (SenderID / Federal EIN) [GS02]
    returnMe+=dentalPlanInfo.value("ClearingHouseID").toString()
            + SEP_ELEMENT;                                  //INTERCHANGE ID QUALIFIER (RECEIVER1) [GS03]
    returnMe+=QDate::currentDate().toString("yyMMdd")
            + SEP_ELEMENT;                                  //InterchangeDate [GS04]
    returnMe+=QTime::currentTime().toString("HHmm")+
            SEP_ELEMENT ;                                   //InterchangeTime [GS05]
    returnMe+= icnStr+SEP_ELEMENT;                          //InterchangeControlNumber [GS06]
    returnMe+= "X"+SEP_ELEMENT;                             //RESPONSIBLE AGENCY CODE [GS07]
    returnMe+= "005010X222A1"+SEP_ELEMENT;                  //Industry Identifier Code  [GS08]
    returnMe+=SEP_SUB_ELEMENT+END_SEGMENT;

    //ST transaction set
    returnMe += "ST"+SEP_ELEMENT+  //[GS header]
            "837"+SEP_ELEMENT; //[ST01]
    returnMe+=SEP_SUB_ELEMENT+END_SEGMENT;

    //BEGINNING OF HIERARCHICAL TRANSACTION
    returnMe += "BHT" + SEP_ELEMENT +                       //BHT header
            "0019" + SEP_ELEMENT;                           //HIERARCHICAL STRUCTURE CODE [BHT01]
    returnMe += "00" + SEP_ELEMENT;                         //TRANSACTION SET PURPOSE CODE (0=original) [BHT02]
    returnMe+= icnStr+SEP_ELEMENT;                          //REFERENCE IDENTIFICATION [BHT03]
    returnMe+=QDate::currentDate().toString("yyMMdd")
            + SEP_ELEMENT;                                  //InterchangeDate [BHT04]
    returnMe+=QTime::currentTime().toString("HHmm")+
            SEP_ELEMENT ;                                   //InterchangeTime [BHT05]




    qDebug()<<returnMe;

    return returnMe;
}

QString CDClearinghouseComm::makePad(QString input, int pad)
{
    return input.leftJustified(pad,' ');
}

QString CDClearinghouseComm::sendEDSEDIPeriodontalAttachment(QString patID, QString EDSEDIClaimID)
{
    QVariantList perioPDFArray = CDPrinter::printPeriodontalChart(patID,false);
    QString dateString = perioPDFArray[0].toString();
    QByteArray arrayData = perioPDFArray[1].toByteArray();

    QSettings localPassSettings(CDFileLocations::getLocalPracticePreferenceFile(),QSettings::IniFormat);
    localPassSettings.beginGroup("Accounts");

    QDomDocument document;
    QDomProcessingInstruction instr = document.createProcessingInstruction("xml", "version='1.0' encoding='us-ascii'");
    document.appendChild(instr);
    QDomElement root = document.createElement("methodCall");
    document.appendChild(root);

    QDomElement methodName = document.createElement("methodName");
    methodName.appendChild(document.createTextNode("submitAttachmentImages"));
    root.appendChild(methodName);

    //Header information

    QDomElement header = document.createElement("header");

    QDomElement userId = document.createElement("userId");
    userId.appendChild(document.createTextNode(localPassSettings.value("EDSEDIUsername","").toString()));
    header.appendChild(userId);

    QDomElement pass = document.createElement("pass");
    pass.appendChild(document.createTextNode(localPassSettings.value("EDSEDIPassword","").toString()));
    header.appendChild(pass);

    QDomElement version = document.createElement("version");
    version.appendChild(document.createTextNode("1"));
    header.appendChild(version);

    root.appendChild(header);

    //Contents of the actual request
    QDomElement content = document.createElement("content");

    QDomElement edsClaimId = document.createElement("edsClaimId");
    edsClaimId.appendChild(document.createTextNode(EDSEDIClaimID));
    content.appendChild(edsClaimId);

    QDomElement attachmentId = document.createElement("attachmentId");
    attachmentId.appendChild(document.createTextNode(EDSEDIClaimID));
    content.appendChild(attachmentId);

    QDomElement imageCount = document.createElement("imageCount");
    imageCount.appendChild(document.createTextNode("1"));
    content.appendChild(imageCount);

    QDomElement images = document.createElement("images");
    content.appendChild(images);

    QDomElement image = document.createElement("image");
    images.appendChild(image);

    QDomElement fileName = document.createElement("fileName");
    image.appendChild(fileName);

    QDomElement documentTypeCode = document.createElement("documentTypeCode");
    documentTypeCode.appendChild(document.createTextNode("7"));
    image.appendChild(documentTypeCode);

    QDomElement orientationCode = document.createElement("orientationCode");
    orientationCode.appendChild(document.createTextNode("Right"));
    image.appendChild(orientationCode);

    QDomElement fileSize = document.createElement("fileSize");
    fileSize.appendChild(document.createTextNode(QString::number(arrayData.length())));
    image.appendChild(fileSize);

    QDomElement fileData = document.createElement("fileData");
    fileData.appendChild(document.createTextNode(arrayData.toBase64()));
    image.appendChild(fileData);

    QDomElement fileExt = document.createElement("fileExt");
    fileExt.appendChild(document.createTextNode("pdf"));
    image.appendChild(fileData);

    QDomElement narrative = document.createElement("narrative");
    image.appendChild(narrative);

    QDomElement dateTaken = document.createElement("dateTaken");
    dateTaken.appendChild(document.createTextNode(dateString));
    image.appendChild(dateTaken);

    root.appendChild(content);

    return document.toString(5);

}

QString CDClearinghouseComm::sendRadiographAttachments(QString patID, QString EDSEDIClaimID, QStringList filenames)
{
    Q_UNUSED(patID) //I may figure out a use later on
    QSettings localPassSettings(CDFileLocations::getLocalPracticePreferenceFile(),QSettings::IniFormat);
    localPassSettings.beginGroup("Accounts");

    QDomDocument document;
    QDomProcessingInstruction instr = document.createProcessingInstruction("xml", "version='1.0' encoding='us-ascii'");
    document.appendChild(instr);
    QDomElement root = document.createElement("methodCall");
    document.appendChild(root);

    QDomElement methodName = document.createElement("methodName");
    methodName.appendChild(document.createTextNode("submitAttachmentImages"));
    root.appendChild(methodName);

    //Header information

    QDomElement header = document.createElement("header");

    QDomElement userId = document.createElement("userId");
    userId.appendChild(document.createTextNode(localPassSettings.value("EDSEDIUsername","").toString()));
    header.appendChild(userId);

    QDomElement pass = document.createElement("pass");
    pass.appendChild(document.createTextNode(localPassSettings.value("EDSEDIPassword","").toString()));
    header.appendChild(pass);

    QDomElement version = document.createElement("version");
    version.appendChild(document.createTextNode("1"));
    header.appendChild(version);

    root.appendChild(header);

    //Contents of the actual request
    QDomElement content = document.createElement("content");

    QDomElement edsClaimId = document.createElement("edsClaimId");
    edsClaimId.appendChild(document.createTextNode(EDSEDIClaimID));
    content.appendChild(edsClaimId);

    QDomElement attachmentId = document.createElement("attachmentId");
    attachmentId.appendChild(document.createTextNode(EDSEDIClaimID));
    content.appendChild(attachmentId);

    QDomElement imageCount = document.createElement("imageCount");
    imageCount.appendChild(document.createTextNode(QString::number(filenames.count())));
    content.appendChild(imageCount);

    QDomElement images = document.createElement("images");
    content.appendChild(images);

    for(int i=0;i<filenames.count();i++) {
        QFile fileToSend(filenames[i]);
        fileToSend.open(QIODevice::ReadOnly);
        QByteArray byteRep = fileToSend.readAll();
        QFileInfo fileInfo(fileToSend);
        QDateTime fileTime = fileInfo.fileTime(QFileDevice::FileBirthTime);

        QDomElement image = document.createElement("image");
        images.appendChild(image);

        QDomElement fileName = document.createElement("fileName");
        fileName.appendChild(document.createTextNode(fileToSend.fileName()));
        image.appendChild(fileName);

        QDomElement documentTypeCode = document.createElement("documentTypeCode");
        documentTypeCode.appendChild(document.createTextNode("16"));
        image.appendChild(documentTypeCode);

        QDomElement orientationCode = document.createElement("orientationCode");
        orientationCode.appendChild(document.createTextNode("Right"));
        image.appendChild(orientationCode);

        QDomElement fileSize = document.createElement("fileSize");
        fileSize.appendChild(document.createTextNode(QString::number(byteRep.length())));
        image.appendChild(fileSize);

        QDomElement fileData = document.createElement("fileData");
        fileData.appendChild(document.createTextNode(byteRep.toBase64()));
        image.appendChild(fileData);

        QDomElement fileExt = document.createElement("fileExt");
        fileExt.appendChild(document.createTextNode("png"));
        image.appendChild(fileData);

        QDomElement narrative = document.createElement("narrative");
        image.appendChild(narrative);

        QDomElement dateTaken = document.createElement("dateTaken");
        dateTaken.appendChild(document.createTextNode(fileTime.toString("MM/dd/yyyy")));
        image.appendChild(dateTaken);
    }

    root.appendChild(content);

    return document.toString(5);
}

//QString CDClearinghouseComm::generateEdsediXMLClaimString(QVariantMap inputData, bool claimPrimary, bool isPreEstimate)
//{
//    QSettings dentalPlanInfo(CDFileLocations::getDentalPlansFile(inputData.value("PatientName").toString()),
//                             QSettings::IniFormat);
//    if(claimPrimary) {
//        dentalPlanInfo.beginGroup("Primary");
//    }
//    else {
//        dentalPlanInfo.beginGroup("Secondary");
//    }

//    QSettings patInfo(CDFileLocations::getPersonalFile(inputData.value("PatientName").toString()),
//                      QSettings::IniFormat);

//    QDomDocument document;

//    QDomElement claimType = document.createElement("claimType");

//    if(isPreEstimate) {
//        claimType.appendChild(document.createTextNode("Preauth"));
//    }
//    else {
//        claimType.appendChild(document.createTextNode("Statement"));
//    }
//    document.appendChild(claimType);

//    QDomElement primPayerName = document.createElement("primPayerName");
//    primPayerName.appendChild(document.createTextNode(dentalPlanInfo.value("Name").toString()));
//    document.appendChild(primPayerName);

//    QDomElement numPrimPayerAddressLines = document.createElement("numPrimPayerAddressLines");
//    numPrimPayerAddressLines.appendChild(document.createTextNode("2"));
//    document.appendChild(numPrimPayerAddressLines);

//    QString primPayerAddressText = dentalPlanInfo.value("Addr").toString() + "\n" +
//            dentalPlanInfo.value("City").toString() + ", " +
//            dentalPlanInfo.value("State").toString() + " " +
//            dentalPlanInfo.value("Zip").toString();
//    QDomElement primPayerAddress = document.createElement("primPayerAddress");
//    primPayerAddress.appendChild(document.createTextNode(primPayerAddressText));
//    document.appendChild(primPayerAddress);

//    QDomElement primPayerId = document.createElement("primPayerId");
//    primPayerId.appendChild(document.createTextNode(dentalPlanInfo.value("ClearingHouseID").toString()));
//    document.appendChild(primPayerId);

//    //<otherCoverage>: For the secondary plan.... will implement later

//    QString relToHolder = dentalPlanInfo.value("PolHolderRel").toString();
//    QString sec12aText = "";
//    QString sec12bText = "";
//    QString sec13Text = "";
//    if(relToHolder == "Self") {
//        patInfo.beginGroup("Name");
//        sec12aText = patInfo.value("LastName").toString() + ", " +
//                patInfo.value("FirstName").toString() + " " +
//                patInfo.value("MiddleName").toString() + "\n";
//        patInfo.endGroup();
//        patInfo.beginGroup("Address");
//        sec12bText = patInfo.value("StreetAddr1").toString() + "\n" +
//                patInfo.value("StreetAddr2").toString() + "\n" +
//                patInfo.value("City").toString() + ", " +
//                patInfo.value("State").toString() + " " +
//                patInfo.value("Zip").toString();
//        patInfo.endGroup();

//        //Section 13
//        patInfo.beginGroup("Personal");
//        sec13Text = patInfo.value("DateOfBirth").toString();
//        patInfo.endGroup();

//    }
//    else {
//        sec12aText = dentalPlanInfo.value("PolHolderLast").toString() + ", " +
//                dentalPlanInfo.value("PolHolderFirst").toString() + " " +
//                dentalPlanInfo.value("PolHolderMiddle").toString() + "\n";
//        sec12bText = dentalPlanInfo.value("PolHolderAddress").toString() + "\n" +
//                dentalPlanInfo.value("PolHolderCity").toString() + ", " +
//                dentalPlanInfo.value("PolHolderState").toString() + " " +
//                dentalPlanInfo.value("PolHolderZip").toString();

//        //Section 13
//        sec13Text = dentalPlanInfo.value("PolHolderDOB").toString();
//    }


//    QDomElement primSubName = document.createElement("primSubName");
//    primSubName.appendChild(document.createTextNode(sec12aText));
//    document.appendChild(primSubName);


//    QDomElement numPrimSubAddressLines = document.createElement("numPrimSubAddressLines");
//    numPrimSubAddressLines.appendChild(document.createTextNode(QString::number(sec12bText.count('\n'))));
//    document.appendChild(numPrimSubAddressLines);

//    QDomElement primSubAddress = document.createElement("primSubAddress");
//    primSubAddress.appendChild(document.createTextNode(sec12bText));
//    document.appendChild(primSubAddress);

//    QDomElement primSubDOB = document.createElement("primSubDOB");
//    primSubAddress.appendChild(document.createTextNode(sec13Text));
//    document.appendChild(primSubDOB);


//    //Section 14
//    QString sec14Gender = "";
//    if(relToHolder == "Self") {
//        patInfo.beginGroup("Personal");
//        sec14Gender = patInfo.value("Sex","").toString();
//        patInfo.endGroup();
//    }
//    else {
//        sec14Gender = dentalPlanInfo.value("PolHolderGender").toString();
//    }

//    QDomElement primSubSex = document.createElement("primSubSex");
//    primSubSex.appendChild(document.createTextNode(sec14Gender));
//    document.appendChild(primSubSex);

//    QString sec15Text = dentalPlanInfo.value("SubID").toString();
//    QDomElement primSubID = document.createElement("primSubID");
//    primSubID.appendChild(document.createTextNode(sec15Text));
//    document.appendChild(primSubID);

//    QString sec16Text = dentalPlanInfo.value("GroupID").toString();
//    QDomElement primSubGroup = document.createElement("primSubGroup");
//    primSubGroup.appendChild(document.createTextNode(sec16Text));
//    document.appendChild(primSubGroup);


//    QString sec17Text = dentalPlanInfo.value("EmployerName").toString();
//    QDomElement primSubEmployer = document.createElement("primSubEmployer");
//    primSubGroup.appendChild(document.createTextNode(sec17Text));
//    document.appendChild(primSubEmployer);

//    QString sec18Text = dentalPlanInfo.value("PolHolderRel").toString();
//    QDomElement patRelToPrimSub = document.createElement("patRelToPrimSub");
//    primSubGroup.appendChild(document.createTextNode(sec18Text));
//    document.appendChild(patRelToPrimSub);


//    patInfo.beginGroup("Name");
//    QString sec20aText = patInfo.value("LastName").toString() + ", " +
//            patInfo.value("FirstName").toString() + " " +
//            patInfo.value("MiddleName").toString() + "\n";
//    patInfo.endGroup();


//    QDomElement patName = document.createElement("patName");
//    patName.appendChild(document.createTextNode(sec20aText));
//    document.appendChild(patName);

//    QDomElement numPatAddressLines = document.createElement("numPatAddressLines");
//    numPatAddressLines.appendChild(document.createTextNode("3"));
//    document.appendChild(numPatAddressLines);

//    QDomElement patAddressInfo = document.createElement("patAddressInfo");

//    patInfo.beginGroup("Address");
//    QString patAddrLine1 = patInfo.value("StreetAddr1").toString();
//    QString patAddrLine2 =patInfo.value("StreetAddr2").toString();
//    QString patAddrLine3 =patInfo.value("City").toString() + ", " +
//            patInfo.value("State").toString() + " " +
//            patInfo.value("Zip").toString();

//    QDomElement patAddress1 = document.createElement("patAddress");
//    numPatAddressLines.appendChild(document.createTextNode(patAddrLine1));
//    patAddressInfo.appendChild(patAddress1);

//    QDomElement patAddress2 = document.createElement("patAddress");
//    numPatAddressLines.appendChild(document.createTextNode(patAddrLine2));
//    patAddressInfo.appendChild(patAddress2);

//    QDomElement patAddress3 = document.createElement("patAddress");
//    numPatAddressLines.appendChild(document.createTextNode(patAddrLine3));
//    patAddressInfo.appendChild(patAddress3);

//    document.appendChild(patAddressInfo);


//    patInfo.endGroup();
//    patInfo.beginGroup("Personal");
//    QString sec21Text = patInfo.value("DateOfBirth").toString();
//    QDomElement patDOB = document.createElement("patDOB");
//    patDOB.appendChild(document.createTextNode(sec21Text));
//    document.appendChild(patDOB);

//    QString sec22Gender = patInfo.value("Sex","").toString(); //Accoring to the ADA; Sex = Gender
//    QDomElement patSex = document.createElement("patSex");
//    patSex.appendChild(document.createTextNode(sec22Gender));
//    document.appendChild(patSex);

//    QString sec23Text = inputData.value("PatientName").toString();
//    QDomElement patID = document.createElement("patID");
//    patID.appendChild(document.createTextNode(sec23Text));
//    document.appendChild(patID);


//    QVariantList procedureList = inputData.value("ProcedureArray").toList();
//    QString claimCountString = QString::number(procedureList.count());
//    QDomElement numTransactions = document.createElement("numTransactions");
//    numTransactions.appendChild(document.createTextNode(claimCountString));
//    document.appendChild(numTransactions);

//    int totFee = 0;
//    for(int i=0;i<procedureList.count();i++) {
//        QVariantMap procedureInfo = procedureList.at(i).toMap();
//        //Section 24
//        QString sec24Text = inputData.value("ProcedureDate").toString();
//        QDateTime sec24Date = QDateTime::fromString(sec24Text);
//        QDomElement procDate = document.createElement("procDate");
//        procDate.appendChild(document.createTextNode(sec24Date.toString("MM/dd/yyyy")));
//        document.appendChild(procDate);


//        //Section 25
//    //https://www.ada.org/~/media/ADA/Publications/Files/CDTCode_AreaOfOralCavity_ToothTable_V3_PublicPosting2020Jan.pdf
//        QString dCode = procedureInfo.value("DCode").toString();
//        QDomElement OC = document.createElement("OC");
//        if(procedureInfo.contains("Location")) {
//            if(procedureInfo.value("Location") == "UA") {
//                OC.appendChild(document.createTextNode("01"));
//            }
//            else if(procedureInfo.value("Location") == "LA") {
//                OC.appendChild(document.createTextNode("02"));
//            }
//            else if(procedureInfo.value("Location") == "UR") {
//                OC.appendChild(document.createTextNode("10"));
//            }
//            else if(procedureInfo.value("Location") == "UL") {
//                OC.appendChild(document.createTextNode("20"));
//            }
//            else if(procedureInfo.value("Location") == "LL") {
//                OC.appendChild(document.createTextNode("30"));
//            }
//            else if(procedureInfo.value("Location") == "LR") {
//                OC.appendChild(document.createTextNode("40"));
//            }
//            else {
//                procDate.appendChild(document.createTextNode(""));
//            }

//        }
//        else {
//            OC.appendChild(document.createTextNode(""));
//        }
//        document.appendChild(OC);


//        //Section 26
//        QDomElement toothSystem = document.createElement("toothSystem");
//        toothSystem.appendChild(document.createTextNode("JP"));
//        document.appendChild(toothSystem);

//        //Section 27
//        if(procedureInfo.contains("Tooth")) {
//            QString toothNumb = procedureInfo.value("Tooth").toString();

//        }
//        else if(procedureInfo.contains("Location")) {

//        }

//        //Section 28
//        if(procedureInfo.contains("Surfaces")) {
//            QString surfaces = procedureInfo.value("Surfaces").toString();

//        }

//        //Section 29


//        //Section 29a
//        //TODO: Add diagnosis codes


//        //Section 29b
//        //"01"


//        //Section 30
//        QString procedureName = procedureInfo.value("ProcedureName").toString();


//        //Section 31
//        QString fee = "$" + procedureInfo.value("BasePrice").toString();
//        if(fee.indexOf(".") == -1) {
//            fee = fee + ".00";
//        }

//        totFee += procedureInfo.value("BasePrice").toInt();
//    }


//    return document.toString();
//}
