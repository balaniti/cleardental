// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.14
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDTranslucentDialog {
    id: finishDialog
    anchors.centerIn:  Overlay.overlay
    modal: Qt.WindowModal

    property alias caseNoteString: caseNoteText.text
    property bool willNeedRx: false
    property bool dieOnClose: true
    property var txItemsToComplete: []
    property var txItemsSelected: []

    CDTextFileManager {
        id: m_textFileMan
    }

    CDGitManager {
        id: m_gitMan
    }

    CDFileLocations {
        id: m_fileLocs
    }

    CDCommonFunctions {
        id: m_comFun
    }

    Settings {
        id: m_pracUpdates
        fileName: m_fileLocs.getLocalPracticePreferenceFile()
        category: "Updates"
    }

    Settings {
        id: m_perioDx
        fileName: m_fileLocs.getPerioChartFile(PATIENT_FILE_NAME)
        category: "Diagnosis"
    }

    Settings {
        id: m_docPrefs
        fileName: m_fileLocs.getWorkingDocInfoFile()
    }

    Settings {
        id: m_DentalPlanInfo
        fileName: m_fileLocs.getDentalPlansFile(PATIENT_FILE_NAME);
    }

    CDProviderInfo {
        id: proInfo
    }

    function fillOutFields() {
        compCol.refreshList();
        refreshNextTxPlans();
        fillOutProphyExamDone();
    }

    function updateTxItemsSelected() {
        txItemsSelected = [];
        for(var i=0;i<txItemsToComplete.length;i++) {
            var checkBox = repComplete.itemAt(i);
            if(checkBox.checked) {
                txItemsSelected.push(checkBox.txItemObject);
            }
        }
    }

    function isObjEqual(objA,objB) {
        var aKeys = Object.keys(objA);
        var returnMe = true;

        for(var i=0;i<aKeys.length;i++) {
            var key = aKeys[i];
            if(key in objB) {
                if(objB[key] !== objA[key]) {
                    return false;
                }
            }
            else {
                return false;
            }
        }

        return returnMe;

    }

    function wasSelected(txItem) {
        var returnMe = false;
        for(var i=0;i<txItemsSelected.length;i++) {
            if(isObjEqual(txItemsSelected[i],txItem)) {
                returnMe = true;
                i = txItemsSelected.length
            }
        }
        return returnMe;
    }

    function refreshNextTxPlans() {
        var txPlanItems = m_comFun.getPrimaryTxPlanItems(PATIENT_FILE_NAME);
        updateTxItemsSelected();
        nvComboBox.futureTx = [];
        for(var i=0;i<txPlanItems.length;i++) {
            if(!wasSelected(txPlanItems[i])) {
                var addMe = {
                    DisplayString : m_comFun.makeTxItemString(txPlanItems[i]),
                    ValueObj: txPlanItems[i]
                }
                nvComboBox.futureTx.push(addMe);
            }
        }
        nvComboBox.model = nvComboBox.futureTx;
    }

    function fillOutProphyExamDone() {
        var caseNoteJSON = m_textFileMan.readFile(m_fileLocs.getCaseNoteFile(PATIENT_FILE_NAME));

        var prophyDueDate = new Date();
        var examDueDate = new Date();
        var monthsBetweenExam = m_pracUpdates.value("Recare",12);
        var monthsBetweenProphy = m_perioDx.value("ProphyMonths",6);

        var prophyList = m_comFun.getProcedureHistory(PATIENT_FILE_NAME,"Prophy");
        var examList = m_comFun.getProcedureHistoryBroad(PATIENT_FILE_NAME,"Exam");

        if(prophyList.length > 0) {
            var lastProphy =prophyList[prophyList.length -1];
            var lastProphyDate = new Date(lastProphy["DateTime"]);
            lastProphyLabel.text = lastProphyDate.toLocaleDateString() + " (Due: " +
                    getDueDate(lastProphyDate,monthsBetweenProphy,prophyDueDate).toLocaleDateString() + ")";
        }
        if(examList.length > 0) {
            var lastExam =examList[examList.length -1];
            var lastExamDate = new Date(lastExam["DateTime"]);
            lastExamLabel.text = lastExamDate.toLocaleDateString() + " (Due: " +
                    getDueDate(lastExamDate,monthsBetweenExam,examDueDate).toLocaleDateString() + ")";
        }
    }

    function getDueDate(lastOne,monthsBetween,currentDueDate) {
        var candidateDue = new Date(lastOne.getFullYear(),lastOne.getMonth() +
                                    parseInt(monthsBetween), lastOne.getDate());
        var candidateDueMills = candidateDue.getTime();
        var currentDueDateMills = currentDueDate.getTime();
        if(candidateDueMills > currentDueDateMills ) {
            return candidateDue;
        }
        return currentDueDate;
    }

    function addCaseNote() {
        var caseNotesJSON = m_textFileMan.readFile(m_fileLocs.getCaseNoteFile(PATIENT_FILE_NAME));
        var caseNotesObj = [];
        if(caseNotesJSON.length > 0) {
            caseNotesObj  = JSON.parse(caseNotesJSON);
        }

        var addMe = ({});
        addMe["DateTime"] = Date();
        addMe["Provider"] =m_docPrefs.value("UnixID");
        addMe["CaseNote"] = caseNoteText.text;
        addMe["Final"] = finalize.checked;
        if(finalize.checked) {
            addMe["Signature"] = proInfo.signData(caseNoteText.text);
        }
        addMe["ProcedureObject"] = JSON.stringify(txItemsToComplete);
        addMe["NextVisit"] = JSON.stringify(nvComboBox.currentValue);

        caseNotesObj.push(addMe);
        m_textFileMan.saveFile(m_fileLocs.getCaseNoteFile(PATIENT_FILE_NAME),JSON.stringify(caseNotesObj, null, '\t'));
    }

    function addToLedger() {
        var ledgerJSON = m_textFileMan.readFile(m_fileLocs.getFullBillingFile(PATIENT_FILE_NAME));
        var ledgerObj = [];
        if(ledgerJSON.length > 1) {
            ledgerObj = JSON.parse(ledgerJSON);
        }

        for(var i=0;i<txItemsSelected.length;i++) {
            var txObj = txItemsSelected[i];
            var addLine = {
                ProcedureDate: Date(),
                Procedure: txObj,
                Payments: [],
                ClaimStatus: "Unsent",
                ProviderUsername: m_docPrefs.value("UnixID")
            };
            //TODO: check for pre-payments before adding in a blank one
            ledgerObj.push(addLine);
        }

        ledgerJSON= JSON.stringify(ledgerObj, null, '\t');
        m_textFileMan.saveFile(m_fileLocs.getFullBillingFile(PATIENT_FILE_NAME), ledgerJSON);
    }

    function removeCurrentTxFromPlanAndSetNextVisit() {
        var jsonTxPlans = m_textFileMan.readFile(m_fileLocs.getTreatmentPlanFile(PATIENT_FILE_NAME));
        var txPlansObj = ({});

        if(jsonTxPlans.length > 2) {
            txPlansObj = JSON.parse(jsonTxPlans);
            var txPlanNames = Object.keys(txPlansObj);
            for(var txPlanNameI =0; txPlanNameI< txPlanNames.length; txPlanNameI++) {
                var txPlan = txPlansObj[txPlanNames[txPlanNameI]];
                var phaseNames = Object.keys(txPlan);
                for(var phasNameI=0;phasNameI < phaseNames.length;phasNameI++ ) {
                    var phaseList = txPlan[phaseNames[phasNameI]];
                    for(var phaseItemI=0;phaseItemI < phaseList.length; phaseItemI++) {
                        var txObj = phaseList[phaseItemI];
                        var txObjJSON = JSON.stringify(txObj);
                        //console.debug(txObjJSON);
                        if(wasSelected(txObj)) {
                            phaseList.splice(phaseItemI,1);
                            phaseItemI--;
                        }
                        else if(m_comFun.isEqual(txObj,nvComboBox.currentValue)) {
                            phaseList[phaseItemI]["NextVisit"] = true;
                        }
                        else {
                            delete phaseList[phaseItemI]["NextVisit"]; //remove all other "NextVisits"
                        }
                    }
                }
            }
        }

        jsonTxPlans= JSON.stringify(txPlansObj, null, '\t');
        m_textFileMan.saveFile(m_fileLocs.getTreatmentPlanFile(PATIENT_FILE_NAME),jsonTxPlans);
    }


    function completeProcedurePaperwork() {
        addCaseNote();
        updateTxItemsSelected();

        if(txItemsSelected.length != 0) {
            addToLedger();
            removeCurrentTxFromPlanAndSetNextVisit();
            m_gitMan.commitData("Completed procedure " + m_comFun.makeTxItemString(txItemsToComplete[0])+
                                " for " + PATIENT_FILE_NAME);
        }
        else {
            m_gitMan.commitData("Completed procedure for " + PATIENT_FILE_NAME);
        }
    }

    ColumnLayout {
        CDTranslucentPane {
            backMaterialColor: Material.Pink

            ColumnLayout {
                CDHeaderLabel {
                    text: "Finish Procedure"
                }

                RowLayout {
                    Layout.alignment: Qt.AlignHCenter
                    CDDescLabel {
                        text: "Which procedures to complete"
                    }
                    Flickable {
                        Layout.fillWidth: true
                        Layout.minimumHeight: 100
                        Layout.maximumHeight: 200
                        Layout.preferredHeight: contentHeight
                        contentHeight: compCol.height
                        contentWidth: compCol.width

                        ScrollBar.vertical: ScrollBar { }

                        clip: true
                        ColumnLayout {
                            id: compCol

                            function refreshList() {
                                for(var i=0;i<txItemsToComplete.length;i++) {
                                    if(typeof txItemsToComplete[i]== 'undefined') {
                                        txItemsToComplete.splice(i);
                                        i--;
                                    }
                                }

                                repComplete.model = txItemsToComplete.length;
                            }

                            Repeater {
                                id: repComplete
                                CheckBox {
                                    property var txItemObject: txItemsToComplete[index]
                                    checked: true
                                    text: m_comFun.makeTxItemString(txItemsToComplete[index])
                                    onCheckedChanged: {
                                        refreshNextTxPlans();
                                    }
                                }
                            }
                        }
                    }
                }

                RowLayout {
                    Layout.alignment: Qt.AlignHCenter
                    Label {
                        text: "Post operative instructions"
                        font.bold: true
                    }
                    ComboBox {
                        id: postOpBox
                        model: ["Nothing","Print","Text"]
                    }
                }

                CheckBox {
                    id: patTolProcedure
                    Layout.alignment: Qt.AlignHCenter
                    text: "Patient tolerated the procedure well"
                    onCheckedChanged: {
                        var alreadyThere = false;
                        var changeText = patTolProcedure.text + ".\n";
                        if(caseNoteText.text.indexOf(changeText) >= 0) {
                            alreadyThere = true;
                        }

                        if(checked) {
                            if(!alreadyThere) {
                                if(!caseNoteText.text.endsWith("\n")) {
                                    caseNoteText.text += "\n";
                                }

                                caseNoteText.text = caseNoteText.text +changeText;
                            }
                        }
                        else {
                            if(alreadyThere) {
                                caseNoteText.text = caseNoteText.text.replace(changeText,"")
                            }
                        }
                    }
                }
                Button {
                    Layout.alignment: Qt.AlignHCenter
                    CDToolLauncher {
                        id: launcher
                    }
                    text: "Make a New Prescription"
                    onClicked: launcher.launchTool(CDToolLauncher.Prescription,PATIENT_FILE_NAME)
                    visible: willNeedRx
                }

                RowLayout {
                    Layout.alignment: Qt.AlignHCenter
                    CDDescLabel {
                        text: "Last Exam Done"
                    }
                    Label {
                        id: lastExamLabel
                        text: "Never"
                    }
                    Button {
                        text: "Add Exam"
                        onClicked: {
                            var txObj = ({});

                            if(m_comFun.hadACompExam(PATIENT_FILE_NAME)) {
                                txObj["ProcedureName"] = "Periodic Exam";
                                txObj["DCode"] = "D0120";
                            }
                            else {
                                txObj["ProcedureName"] = "Comprehensive Exam";
                                txObj["DCode"] = "D0150";
                            }

                            m_comFun.addTxItem(PATIENT_FILE_NAME,"Primary","Unphased",txObj);
                            refreshNextTxPlans();
                            visible = false;
                        }

                        visible: {
                            for (var i in comFuns.getAllTxPlanItems(PATIENT_FILE_NAME)) {
                                var txItemObj = comFuns.getAllTxPlanItems(PATIENT_FILE_NAME)[i];
                                if(txItemObj["ProcedureName"].includes("Comprehensive Exam") ||
                                        txItemObj["ProcedureName"].includes("Periodic Exam")) {
                                    return false;
                                }
                            }
                            return true;
                        }
                    }

                }

                RowLayout {
                    CDDescLabel {
                        text: "Last Prophy Done"
                    }
                    Label {
                        id: lastProphyLabel
                        text: "Never"
                    }
                    Button {
                        text: "Add Prophy"
                        onClicked: {
                            var txObj = ({});
                            txObj["ProcedureName"] = "Prophy";
                            txObj["DCode"] = "D1110";
                            m_comFun.addTxItem(PATIENT_FILE_NAME,"Primary","Unphased",txObj);
                            refreshNextTxPlans();
                            visible = false;
                        }
                        visible: {
                            for (var i in comFuns.getAllTxPlanItems(PATIENT_FILE_NAME)) {
                                var txItemObj = comFuns.getAllTxPlanItems(PATIENT_FILE_NAME)[i];
                                if(txItemObj["ProcedureName"].includes("Prophy")) {
                                    return false;
                                }
                            }
                            return true;
                        }
                    }
                }

                RowLayout {
                    Layout.alignment: Qt.AlignHCenter
                    CDDescLabel {
                        text: "Next Visit"
                    }
                    ComboBox {
                        id: nvComboBox
                        property var futureTx:[]
                        Layout.minimumWidth: 300
                        textRole: "DisplayString"
                        valueRole: "ValueObj"
                    }
                }

                CDCaseNoteTextArea {
                    id: caseNoteText
                    Layout.minimumHeight: 300
                    Layout.fillWidth: true
                    Layout.maximumWidth: rootWin.width - 64
                }
                CheckBox {
                    id: finalize
                    Layout.alignment: Qt.AlignHCenter
                    text: "Sign and Finalize Case Note"
                }
                RowLayout {
                    Layout.alignment: Qt.AlignHCenter
                    CDDescLabel {
                        text: "Procedure Provider"
                    }
                    Label {
                        Component.onCompleted:  {
                            text= m_docPrefs.value("FirstName") + " " + m_docPrefs.value("LastName") + " " +
                                    m_docPrefs.value("Title")
                        }
                    }
                }
            }
        }

        RowLayout {
            CDCancelButton {
                text: "Back to procedure"
                onClicked: reject();
            }

            Label {
                Layout.fillWidth: true
            }

            Button {
                id: compProcedureButton
                text: "Complete Procedure"
                Material.accent: Material.Cyan
                icon.name: "dialog-ok"
                highlighted: true
                icon.width: 32
                icon.height: 32
                font.pointSize: 18
                onClicked: {
                    completeProcedurePaperwork();
                    if(dieOnClose) {
                        Qt.quit();
                    }
                    else {
                        accept();
                    }
                }
            }
        }
    }

    onVisibleChanged: {
        if(visible) {
            fillOutFields();
            rootWin.keyboardSaveButton = compProcedureButton //this only works the second time
            //because rootWin will make keyboardSaveButton = undefined after the first enter which happens
            //after this function completes
        }
    }
}
