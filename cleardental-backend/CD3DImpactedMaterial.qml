// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9

import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Extras 2.15
import QtQuick.Scene3D 2.14

Entity {

    id: compToothEntity

    property bool isTxPlan: false
    property string toothAspect: ""
    property string baseMeshSource: ""

    CD3DToothPhongMaterial {
        id: compMaterial
        makeColor: Qt.rgba(1,.7,.7,.4);

    }

    CuboidMesh {
        id: compToothMesh
        xExtent: 10
        yExtent: 25
        zExtent: 10
    }

    CD3DAnimatedRotation {
        id: compTransForm
        scale: 1.01
    }



    components: [ compToothMesh, compMaterial, compTransForm ]

}
