// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cdscanner.h"

#include <QProcess>
#include <QDir>
#include <QDebug>
#include <QFile>

#include "cddefaults.h"
#include "cdfilelocations.h"
#include "cdgitmanager.h"
#include "cdproviderinfo.h"

CDScanner::CDScanner(QObject *parent) : QObject(parent)
{

}

QString CDScanner::scanCard(bool withDriversLicense)
{
    QProcess scanImage;
    scanImage.setProgram("/usr/bin/scanimage");
    QString returnMe = QDir::tempPath() + "/saveMe-"+CDProviderInfo::getCurrentProviderUsername()+".png";

    QStringList args;
    args<<"--format=png";
    args<<"--output-file="+ returnMe;
    args<<"--mode=color";
    args<<"--resolution=600";
    args<<"-x 90";
    if(withDriversLicense) {
        args<<"-y 115";
    }
    else {
        args<<"-y 60";
    }

    scanImage.setArguments(args);
    scanImage.start();
    scanImage.waitForFinished();

    return returnMe;
}

void CDScanner::saveCard(QString patID)
{
    QDir mkdir(CDFileLocations::getPatientScanDir(patID));

    qDebug() << CDFileLocations::getPatientScanDir(patID);

    if(!mkdir.exists()) {
        mkdir.mkpath(".");
    }
    QFile original(QDir::tempPath() + "/saveMe-"+CDProviderInfo::getCurrentProviderUsername()+".png");
    bool copySuccess = original.copy(CDFileLocations::getPatientCardFile(patID));
    if (!copySuccess) {
        qDebug() << "ready to remove this file:\n" << CDFileLocations::getPatientCardFile(patID);
        original.remove(CDFileLocations::getPatientCardFile(patID));
        original.copy(CDFileLocations::getPatientCardFile(patID));
    }
    CDGitManager::commitData("Added the card for " + patID);
}
