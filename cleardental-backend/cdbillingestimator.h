// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef CDBILLINGESTIMATOR_H
#define CDBILLINGESTIMATOR_H

#include <QObject>
#include <QVariantMap>
#include <QSettings>

class CDBillingEstimator : public QObject
{
    Q_OBJECT
public:
    explicit CDBillingEstimator(QObject *parent = nullptr);

    enum TxType {
        EXAM,
        DIAGNOSTIC,
        PREVENTATIVE,
        CLEANING,
        FLUORIDE,
        BASIC,
        MAJOR,
        ORTHO,
        NON_COVERED
    };
    Q_ENUM(TxType)

    Q_INVOKABLE static QVariantMap calculatePatientPortions(QString getPatID, QVariantMap getTxPlan);
    Q_INVOKABLE static TxType getTxType(QVariantMap getTx);

private:



};

#endif // CDBILLINGESTIMATOR_H
