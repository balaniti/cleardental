// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9

import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Extras 2.15
import QtQuick.Scene3D 2.14
PhongMaterial {
    property string materialColor: "white"
    property color makeColor: whiteRGBA
    property bool makeTransparent: false
    id: material
    ambient:  makeColor
    diffuse: makeColor
    specular: makeColor

    onMaterialColorChanged: {
        if(materialColor === "white") {
            makeColor = whiteRGBA
        }
        else if(materialColor === "red") {
            makeColor = redRGBA
        }
        else if(materialColor === "green") {
            makeColor = greenRGBA
        }
    }

    property color redRGBA: Qt.rgba( .8,0, 0, makeTransparent ? 0.2 : 1);
    property color greenRGBA: Qt.rgba( 0,.8, 0, makeTransparent ? 0.2 : 1);
    property color whiteRGBA: Qt.rgba( .8,.8, .8, makeTransparent ? 0.2 : 1);
}
