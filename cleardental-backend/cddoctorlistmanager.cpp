// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "cddoctorlistmanager.h"

#include <QDir>
#include <QDebug>
#include <QRandomGenerator>
#include <QSettings>

#include "cddefaults.h"
#include "cdgitmanager.h"
#include "cdproviderinfo.h"

CDDoctorListManager::CDDoctorListManager(QObject *parent) : QObject(parent){}

QStringList CDDoctorListManager::getListOfProviderFiles()
{
    QStringList returnMe;
    QDir doctorDir(CDDefaults::getDocPrefDir());
    foreach(QString docFolder,doctorDir.entryList(QDir::AllDirs | QDir::NoDotAndDotDot)) {
        QString addMe =doctorDir.canonicalPath() + "/" + docFolder + "/prefs.ini";
        QSettings fileReader(addMe,QSettings::IniFormat);
        QString providerType = fileReader.value("ProviderType","").toString();
        if((providerType == "General Dentist") || (providerType == "Hygienist")) {
            returnMe.append(addMe);
        }
    }
    return returnMe;
}

QStringList CDDoctorListManager::getListOfStaffFiles()
{
    QStringList returnMe;
    QDir doctorDir(CDDefaults::getDocPrefDir());
    foreach(QString docFolder,doctorDir.entryList(QDir::AllDirs | QDir::NoDotAndDotDot)) {
        QString addMe =doctorDir.canonicalPath() + "/" + docFolder + "/prefs.ini";
        returnMe.append(addMe);
    }
    return returnMe;
}

QString CDDoctorListManager::addProvider(QVariantMap data)
{
    QString returnMe="";

    returnMe = data.value("UnixID").toString();

    QDir allDoctorDir(CDDefaults::getDocPrefDir());
    allDoctorDir.mkpath(returnMe);
    QSettings writeMe(allDoctorDir.canonicalPath() + "/" + returnMe +
                      "/prefs.ini",QSettings::IniFormat);

    foreach(QVariant key, data.keys()) {
        writeMe.setValue(key.toString(),data.value(key.toString()));
    }
    writeMe.sync();

    CDGitManager::commitData("Added provider: " + returnMe);

    return returnMe;
}

void CDDoctorListManager::removeProvider(QString providerID)
{
    QDir doctorDir(CDDefaults::getDocPrefDir() + providerID);
    doctorDir.removeRecursively();

    CDGitManager::commitData("Removed provider: " + providerID);
}

QString CDDoctorListManager::currentProvider()
{
    return CDProviderInfo::getCurrentProviderUsername();
}
