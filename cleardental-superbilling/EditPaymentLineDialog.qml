// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTranslucentDialog {
    id: editPaymentLineDialog

    property var paymentObj: ({})
    property int paymentLineIndex: -1

    ColumnLayout {
        CDTranslucentPane {
            ColumnLayout {
                CDHeaderLabel {
                    text: "Edit Payment"
                }

                AddDentalPlanPaymentPane {
                    id: dentalPlanPayment
                    visible: paymentObj["Type"] === "Dental Plan Payment"
                    Layout.fillWidth: true
                }

                AddCreditCardPaymentPane {
                    id: creditCardPayment
                    visible: paymentObj["Type"] === "Credit Card"
                    Layout.fillWidth: true
                }

                AddCashPaymentPane {
                    id: cashPayment
                    Layout.fillWidth: true
                    visible: paymentObj["Type"] === "Cash"
                }
                AddCheckPaymentPane {
                    id: checkPayment
                    Layout.fillWidth: true
                    visible: paymentObj["Type"] === "Check"
                }
                AddWriteoffPane {
                    id: writeoffPane
                    Layout.fillWidth: true
                    visible: paymentObj["Type"] === "Credit"
                }
                RowLayout {
                    CDDescLabel {
                        text: "Payment Date"
                    }

                    CDDatePicker {
                        id: datePicker
                    }
                }


            }
        }

        RowLayout {
            CDCancelButton {
                onClicked: {
                    editPaymentLineDialog.reject();
                }
            }
            CDDeleteButton {
                text: "Delete Payment"
                onClicked: {
                    deletePane.visible = true;
                }
            }

            Label {
                Layout.fillWidth: true
            }
            CDAddButton {
                icon.name: "document-edit"
                text: "Edit Payment"
                onClicked: {
                    if(paymentObj["Type"] === "Dental Plan Payment") {
                        paymentObj["Amount"] = dentalPlanPayment.getPlanPayment;
                        paymentObj["PlanName"] = dentalPlanPayment.getPlanName;
                        paymentObj["MaxAllowed"] = dentalPlanPayment.getMaxAllowed;
                        paymentObj["PatientPortion"] = dentalPlanPayment.getPatientPortion;
                        paymentObj["WriteOff"] = dentalPlanPayment.getWriteOff;
                    }
                    else if(paymentObj["Type"] === "Credit Card") {
                        paymentObj["Amount"] = creditCardPayment.getAmountGotten;
                        paymentObj["ConfirmationNumber"] = creditCardPayment.getConfirmNumber;
                    }
                    else if(paymentObj["Type"] === "Cash") {
                        paymentObj["Amount"] = cashPayment.getAmountGotten;
                    }
                    else if(paymentObj["Type"] === "Check") {
                        paymentObj["Amount"] = checkPayment.getAmountGotten;
                    }
                    else if(paymentObj["Type"] === "Credit") {
                        paymentObj["Amount"] = writeoffPane.getAmountGotten;
                        paymentObj["Reason"] = writeoffPane.getReason;
                    }

                    paymentObj["Date"] = datePicker.getDateObj();

                    editPaymentLineDialog.accept();
                }
            }
        }

        CDTranslucentPane {
            id: deletePane
            visible: false
            backMaterialColor: Material.Red
            GridLayout {
                columns: 2
                CDHeaderLabel {
                    text: "Are you sure? There is no undo!"
                    Layout.columnSpan: 2
                }
                CDCancelButton {
                    text: "No, nevermind"
                    onClicked: {
                        deletePane.visible = false;
                    }
                }
                CDDeleteButton {
                    text: "Yes, delete the payment"
                    onClicked: {
                        paymentObj["DeleteMe"]  = true;
                        editPaymentLineDialog.accept();
                    }
                }
            }
        }

        Component.onCompleted: {
            if(paymentObj["Type"] === "Dental Plan Payment") {
                dentalPlanPayment.getMaxAllowed = paymentObj["MaxAllowed"]
                dentalPlanPayment.getPlanPayment = paymentObj["Amount"]
                dentalPlanPayment.setPlanName( paymentObj["PlanName"])
                dentalPlanPayment.getPatientPortion = paymentObj["PatientPortion"]
                dentalPlanPayment.getWriteOff = paymentObj["WriteOff"]
                dentalPlanPayment.showNewClaimStatus = false
            }
            else if(paymentObj["Type"] === "Credit Card") {
                creditCardPayment.getAmountGotten = paymentObj["Amount"]
                creditCardPayment.getConfirmNumber = paymentObj["ConfirmationNumber"]
            }
            else if(paymentObj["Type"] === "Cash") {
                cashPayment.getAmountGotten =  paymentObj["Amount"]
            }
            else if(paymentObj["Type"] === "Check") {
                checkPayment.getAmountGotten = paymentObj["Amount"]
            }
            else if(paymentObj["Type"] === "Credit") {
                writeoffPane.getAmountGotten = paymentObj["Amount"]
                writeoffPane.getReason = paymentObj["Reason"]
            }

            datePicker.setDateObj(new Date(paymentObj["Date"]))
        }
    }
}
