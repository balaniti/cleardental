// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTranslucentDialog {
    id: addLoosePaymentDialog

    property real totalAmount: 0

    ColumnLayout {
        CDTranslucentPane {

            GridLayout {
                columns: 2
                CDHeaderLabel {
                    text: "Add new Payment"
                    Layout.columnSpan: 2
                }

                CDDescLabel {
                    text: "Payment Type"
                }

                ComboBox {
                    id: paymentTypeBox
                    model: ["Credit Card","Cash","Check", "Special Credit"]
                    Layout.minimumWidth: 200
                }
                AddCreditCardPaymentPane {
                    id: creditCardPayment
                    Layout.columnSpan: 2
                    visible: paymentTypeBox.currentIndex == 0
                    Layout.fillWidth: true

                    onGetAmountGottenChanged: {
                        totalAmount = getAmountGotten;
                    }
                }

                AddCashPaymentPane {
                    id: cashPayment
                    Layout.columnSpan: 2
                    Layout.fillWidth: true
                    visible: paymentTypeBox.currentIndex == 1

                    onGetAmountGottenChanged: {
                        totalAmount = getAmountGotten;
                    }
                }
                AddCheckPaymentPane {
                    id: checkPayment
                    Layout.columnSpan: 2
                    Layout.fillWidth: true
                    visible: paymentTypeBox.currentIndex == 2

                    onGetAmountGottenChanged: {
                        totalAmount = getAmountGotten;
                    }
                }
                AddWriteoffPane {
                    id: writeoffPane
                    Layout.columnSpan: 2
                    Layout.fillWidth: true
                    visible: paymentTypeBox.currentIndex == 3

                    onGetAmountGottenChanged: {
                        totalAmount = getAmountGotten;
                    }
                }

                CDDescLabel {
                    text: "Procedures"
                }

                ProcedureScrollView {
                    id: txPlanBox
                    Layout.minimumWidth: 360
                    Layout.minimumHeight: 360
                    paymentAmount: totalAmount
                }

                CDDescLabel {
                    text: "Total Procedure Payment"
                }

                Label {
                    text: "$" + txPlanBox.totaledUpPayments
                    font.bold: totalAmount != txPlanBox.totaledUpPayments
                    color: totalAmount == txPlanBox.totaledUpPayments ?
                               Material.foreground : Material.color(Material.Red)
                }

                CDDescLabel {
                    text: "Provider"
                }
                CDProviderBox {
                    id: providerBox
                    Layout.fillWidth: true
                }
            }
        }

        RowLayout {
            Layout.fillWidth: true
            CDCancelButton {
                onClicked: addLoosePaymentDialog.reject();
            }
            Label {
                Layout.fillWidth: true
            }
            CDAddButton {
                text: "Add Payment"
                onClicked: {
                    var addPayment = ({});

                    if(paymentTypeBox.currentIndex == 0) { //AddCreditCardPaymentPane
                        addPayment["Type"] = "Credit Card";
                        addPayment["ConfirmationNumber"] = creditCardPayment.getConfirmNumber;
                    }
                    else if(paymentTypeBox.currentIndex == 1) { //AddCashPaymentPane
                        addPayment["Type"] = "Cash";
                    }
                    else if(paymentTypeBox.currentIndex == 2) { //AddCheckPaymentPane
                        addPayment["Type"] = "Check";
                    }
                    else if(paymentTypeBox.currentIndex == 3) { //AddCreditPane
                        addPayment["Type"] = "Credit";
                        addPayment["Reason"] = clinicCreditPane.getReason;
                    }

                    addPayment["Date"] = new Date(); //today

                    //Go through the list of selected procedures (could be completed or not)
                    for(var i=0;i<txPlanBox.checkboxArray.length;i++) {
                        if(txPlanBox.checkboxArray[i].checked) {
                            var addPaymentForTx =  Object.assign({}, addPayment);
                            var amountForTx = parseFloat(txPlanBox.textFieldArray[i].text);
                            addPaymentForTx["Amount"] = amountForTx;

                            if(txPlanBox.scrollModel[i].FromLedger) { //already in ledger
                                var ledgerIndex = txPlanBox.scrollModel[i]["LedgerIndex"];
                                rootWin.ledgerList[ledgerIndex]["Payments"].push(addPaymentForTx);
                            }
                            else { //we need to add it to the actual ledger
                                var addMe = ({});
                                addMe["ClaimStatus"] = "Unsent";
                                addMe["ProviderUsername"] = providerBox.providerIDs[providerBox.currentIndex];
                                addMe["Procedure"] = txPlanBox.checkboxArray[i].procedureObject;
                                addMe["Payments"] = [addPaymentForTx];

                                rootWin.ledgerList.push(addMe);
                            }
                        }
                    }

                    var ledgerListString = JSON.stringify(rootWin.ledgerList, null, '\t');
                    ledMan.saveFile(fLocs.getFullBillingFile(PATIENT_FILE_NAME),ledgerListString);
                    gitMan.commitData("Added payment for "  + PATIENT_FILE_NAME + " of " + JSON.stringify(addPayment));
                    addLoosePaymentDialog.accept();
                }
            }
        }
    }
}


