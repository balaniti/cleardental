// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

Drawer {
    id: patBillingdrawer

    property var dentalPlanList: []

    property alias prefWidth: billingInfoFlick.contentWidth


    background: Rectangle {
        radius: 10
        color: Material.backgroundColor
    }

    CDConstLoader {
        id: constLoader
    }

    Settings {
        id: dentalPlanSettings
        fileName: fLocs.getDentalPlansFile(PATIENT_FILE_NAME);
    }

    Flickable {
        id: billingInfoFlick
        anchors.fill: parent
        anchors.margins: 10
        contentHeight: plansGrid.height
        contentWidth: plansGrid.width
        flickableDirection: Flickable.VerticalFlick

        ScrollBar.vertical: ScrollBar{}

        ColumnLayout {
            id: plansGrid

            CheckBox {
                id: membershipPlan
                text: "Subscribed under membership plan"
            }

            GroupBox {
                title: "Primary Dental Plan"
                label: CDHeaderLabel {
                    text: "Primary Dental Plan"
                }

                RowLayout {
                    spacing: 10

                    GridLayout {
                        columns: 2
                        Layout.alignment: Qt.AlignTop

                        CDDescLabel {
                            text: "Dental Plan Company Name"
                        }

                        ComboBox {
                            id: priDenPlanName
                            Layout.fillWidth: true
                            textRole: "PlanName"
                            model: JSON.parse(constLoader.getUSDentalPlanListJSON())
                            editable: true

                            onActivated: {
                                var selectedObj = priDenPlanName.model[index];
                                priDenPlanName.editText = selectedObj["PlanName"]
                                priDenPlanAddr.text = selectedObj["Address"]
                                priDenPlanCity.text = selectedObj["City"]
                                priDenPlanState.setStateVal = selectedObj["State"]
                                priDenPlanZip.text = selectedObj["Zip"]
                                priClearingHouseID.text = selectedObj["PayerID"]
                            }
                        }

                        CDDescLabel {
                            text: "Dental Plan Company Address"
                        }

                        CDCopyLabel {
                            id: priDenPlanAddr
                            Layout.fillWidth: true
                        }

                        CDDescLabel {
                            text: "Dental Plan Company City"
                        }

                        CDCopyLabel {
                            id: priDenPlanCity
                            Layout.fillWidth: true
                        }

                        CDDescLabel {
                            text: "Dental Plan Company State"
                        }

                        CDStatePicker {
                            id: priDenPlanState
                            Layout.fillWidth: true
                        }

                        CDDescLabel {
                            text: "Dental Plan Company Zip"
                        }

                        CDCopyLabel {
                            id: priDenPlanZip
                            Layout.fillWidth: true
                        }

                        CDDescLabel {
                            text: "Policyholder/Subscriber ID"
                        }

                        CDCopyLabel {
                            id: priSubID
                            Layout.fillWidth: true
                        }

                        CDDescLabel {
                            text: "Plan/Group Number"
                        }

                        CDCopyLabel {
                            id: priGroupNumb
                            Layout.fillWidth: true
                        }

                        CDDescLabel {
                            text: "Employer Name"
                        }


                        CDCopyLabel {
                            id: priEmployerName
                            Layout.fillWidth: true
                        }

                        CDDescLabel {
                            text: "Relationship to Policyholder"
                        }

                        ComboBox {
                            id: priPolRel
                            Layout.fillWidth: true
                            model: ["Self","Spouse","Dependent","Other"]
                        }

                        GridLayout {
                            Layout.columnSpan: 2
                            visible: priPolRel.currentIndex != 0 //not self
                            columns: 2

                            CDDescLabel {
                                text: "Policyholder First Name"
                            }

                            CDCopyLabel {
                                id: priPolFirstName
                                Layout.fillWidth: true
                            }

                            CDDescLabel {
                                text: "Policyholder Middle Name"
                            }

                            CDCopyLabel {
                                id: priPolMiddleName
                                Layout.fillWidth: true
                            }

                            CDDescLabel {
                                text: "Policyholder Last Name"
                            }

                            CDCopyLabel {
                                id: priPolLastName
                                Layout.fillWidth: true
                            }

                            CDDescLabel {
                                text: "Policyholder Address"
                            }

                            CDCopyLabel {
                                id: priPolAddr
                                Layout.fillWidth: true
                            }

                            CDDescLabel {
                                text: "Policyholder City"
                            }

                            CDCopyLabel {
                                id: priPolCity
                                Layout.fillWidth: true
                            }

                            CDDescLabel {
                                text: "Policyholder State"
                            }

                            CDStatePicker {
                                id: priPolState
                                Layout.fillWidth: true
                            }

                            CDDescLabel {
                                text: "Policyholder Zip"
                            }

                            CDCopyLabel {
                                id: priPolZip
                                Layout.fillWidth: true
                            }

                            CDDescLabel {
                                text: "Policyholder Date of Birth"
                            }

                            CDDatePicker {
                                id: priPolDob
                                Layout.fillWidth: true
                            }

                            CDDescLabel {
                                text: "Policyholder Gender"
                            }

                            RowLayout {
                                id: priPolGender
                                RadioButton {
                                    id: priPolMale
                                    text: "Male"
                                }

                                RadioButton {
                                    id: priPolFemale
                                    text: "Female"
                                }

                                RadioButton {
                                    id: priPolOtherGender
                                    text: "Other"
                                }

                                ButtonGroup {
                                    id: priPolGenderGroup
                                    buttons: [priPolMale,priPolFemale,priPolOtherGender]
                                }
                            }
                        }

                        CDDescLabel {
                            text: "Clearing House ID"
                        }

                        CDCopyLabel {
                            id: priClearingHouseID
                            Layout.fillWidth: true
                        }

                        CDDescLabel {
                            text: "Claims phone number"
                        }

                        CDCopyLabel {
                            id: priClaimsPhoneNumber
                            Layout.fillWidth: true
                        }

                        CDDescLabel {
                            text: "Claims fax number"
                        }

                        CDCopyLabel {
                            id: priClaimsFaxNumber
                            Layout.fillWidth: true
                        }

                        CheckBox {
                            id: priPayDoctor
                            text: "Pays OON Doctor Directly"
                            Layout.columnSpan: 2
                        }

                    }

                    GroupBox {
                        Layout.maximumWidth: 2
                        Layout.minimumWidth: 2
                        Layout.fillHeight: true
                    }


                    GridLayout {
                        columns: 2
                        Layout.alignment: Qt.AlignTop



                        CDDescLabel {
                            text: "Diagnostic Coverage"
                        }

                        CDPercentSpinBox {
                            id: priDiagPercent
                        }

                        CDDescLabel {
                            text: "Preventative Coverage"
                        }

                        CDPercentSpinBox {
                            id: priPrevPercent
                        }

                        CDDescLabel {
                            text: "Basic Coverage"
                        }

                        CDPercentSpinBox {
                            id: priBasicPercent
                        }

                        CDDescLabel {
                            text: "Major Coverage"
                        }

                        CDPercentSpinBox {
                            id: priMajorPercent
                        }

                        CDDescLabel {
                            text: "Annual Max ($)"
                        }

                        CDCopyLabel {
                            id: priAnnMax
                            Layout.fillWidth: true
                        }

                        CDDescLabel {
                            text: "Amount Used So Far ($)"
                        }

                        CDCopyLabel {
                            id: priAmountUsed
                            Layout.fillWidth: true
                        }

                        RowLayout {
                            Layout.fillWidth: true
                            Layout.columnSpan: 2
                            CheckBox {
                                id: proHasRollover
                                text: "Has Rollover"
                            }
                            CDDescLabel {
                                visible: proHasRollover.checked
                                text: " Amount ($)"
                            }
                            CDCopyLabel {
                                visible: proHasRollover.checked
                                id: proRolloverAmt
                            }
                        }

                        CDDescLabel {
                            text: "Annual Deductable\n(including exams / cleaning) ($) "
                        }

                        CDCopyLabel {
                            id: priAnnDeductAll
                            Layout.fillWidth: true
                        }

                        CDDescLabel {
                            text: "Annual Deductable\n(basic and major) ($)"
                        }

                        CDCopyLabel {
                            id: priAnnDeductBasicMajor
                            Layout.fillWidth: true
                        }

                        CDDescLabel {
                            text: "Deductable Met ($)"
                        }

                        CDCopyLabel {
                            id: priAnnDeductMet
                            Layout.fillWidth: true
                        }

                        CDDescLabel {
                            text: "Termination Date"
                        }

                        CDDatePicker {
                            id: priTermDate
                        }

                        CDDescLabel {
                            text: "Waiting Period"
                        }

                        CDDatePicker {
                            id: priWaitPeriod
                        }

                        CDDescLabel {
                            text: "Last Exam"
                        }

                        CDDatePicker {
                            id: priLastExam
                        }

                        CDDescLabel {
                            text: "Last BWs"
                        }

                        CDDatePicker {
                            id: priLastBW
                        }

                        CDDescLabel {
                            text: "Last Pan"
                        }

                        CDDatePicker {
                            id: priLastPan
                        }

                        CDDescLabel {
                            text: "Cleaning Frequency"
                        }

                        FrequencyComboBox {
                            id: prophyFreq
                            Layout.fillWidth: true
                        }

                        CDDescLabel {
                            text: "Bitewing Frequency"
                        }

                        FrequencyComboBox {
                            id: bwFreq
                            Layout.fillWidth: true
                        }

                        CDDescLabel {
                            text: "FMX Frequency (years)"
                        }

                        SpinBox {
                            id: fmxFeq
                            from: 1
                        }

                        CDDescLabel {
                            text: "Panoramic Frequency (years)"
                        }

                        SpinBox {
                            id: panFeq
                            from: 1
                        }

                        CDDescLabel {
                            text: "Comprehensive Exam\nFrequency (years)"
                        }

                        SpinBox {
                            id: compExFeq
                            from: 1
                        }

                        RowLayout {
                            Layout.columnSpan: 2
                            CheckBox {
                                id: priFlu
                                text: "Fluoride Coverage"
                            }

                            RowLayout {
                                visible: priFlu.checked
                                CDDescLabel {
                                    text: " Frequency"
                                }
                                FrequencyComboBox {
                                    id: priFluFreq
                                    Layout.minimumWidth: 148
                                }
                                CDDescLabel {
                                    text: "age limit"
                                }
                                SpinBox {
                                    id: priFluAge
                                    from: 1
                                }
                            }
                        }

                        RowLayout {
                            Layout.columnSpan: 2
                            CheckBox {
                                id: priSealant
                                text: "Sealant Coverage"
                            }

                            RowLayout {
                                visible: priSealant.checked
                                CDDescLabel {
                                    text: "frequency (years)"
                                }
                                SpinBox {
                                    id: priSealFreq
                                    from: 1
                                }
                                CDDescLabel {
                                    text: "age limit"
                                }
                                SpinBox {
                                    id: priSealAge
                                    from: 1
                                }
                            }
                        }

                        CDDescLabel {
                            text: "Type of Plan (EPO/PPO/etc.)"
                        }

                        CDCopyLabel {
                            id: priaddInfo
                            Layout.fillWidth: true
                        }

                        CDDescLabel {
                            text: "Comments"
                        }

                        TextArea {
                            id: priComments
                            wrapMode: TextEdit.WordWrap
                            Layout.fillWidth: true
                        }

                    }
                }

            }

            GroupBox {
                title: "Secondary Dental Plan"
                label: CDHeaderLabel {
                    text: "Secondary Dental Plan"
                }
                ColumnLayout {

                    RowLayout {
                        CheckBox {
                            id: secCoverDental
                            text: "Covers Dental"
                        }

                        CheckBox {
                            id: secCoverMed
                            text: "Covers Medical"
                        }
                    }

                    GridLayout {
                        visible: secCoverDental.checked | secCoverMed.checked
                        columns: 2

                        CDDescLabel {
                            text: "Dental Plan Company Name"
                        }

                        CDCopyLabel {
                            id: secDenPlanName
                            Layout.fillWidth: true
                        }

                        CDDescLabel {
                            text: "Dental Plan Company Address"
                        }

                        CDCopyLabel {
                            id: secDenPlanAddr
                            Layout.fillWidth: true
                        }

                        CDDescLabel {
                            text: "Dental Plan Company City"
                        }

                        CDCopyLabel {
                            id: secDenPlanCity
                            Layout.fillWidth: true
                        }

                        CDDescLabel {
                            text: "Dental Plan Company State"
                        }

                        CDStatePicker {
                            id: secDenPlanState
                            Layout.fillWidth: true
                        }

                        CDDescLabel {
                            text: "Dental Plan Company Zip"
                        }

                        CDCopyLabel {
                            id: secDenPlanZip
                            Layout.fillWidth: true
                        }

                        CDDescLabel {
                            text: "Policyholder/Subscriber ID"
                        }

                        CDCopyLabel {
                            id: secSubID
                            Layout.fillWidth: true
                        }

                        CDDescLabel {
                            text: "Plan/Group Number"
                        }

                        CDCopyLabel {
                            id: secGroupNumb
                            Layout.fillWidth: true
                        }

                        CDDescLabel {
                            text: "Employer Name"
                        }

                        CDCopyLabel {
                            id: secEmployerName
                            Layout.fillWidth: true
                        }

                        CDDescLabel {
                            text: "Policyholder First Name"
                        }

                        CDCopyLabel {
                            id: secPolFirstName
                            Layout.fillWidth: true
                        }

                        CDDescLabel {
                            text: "Policyholder Middle Name"
                        }

                        CDCopyLabel {
                            id: secPolMiddleName
                            Layout.fillWidth: true
                        }

                        CDDescLabel {
                            text: "Policyholder Last Name"
                        }

                        CDCopyLabel {
                            id: secPolLastName
                            Layout.fillWidth: true
                        }

                        CDDescLabel {
                            text: "Policyholder Address"
                        }

                        CDCopyLabel {
                            id: secPolAddr
                            Layout.fillWidth: true
                        }

                        CDDescLabel {
                            text: "Policyholder City"
                        }

                        CDCopyLabel {
                            id: secPolCity
                            Layout.fillWidth: true
                        }

                        CDDescLabel {
                            text: "Policyholder State"
                        }

                        CDStatePicker {
                            id: secPolState
                            Layout.fillWidth: true
                        }

                        CDDescLabel {
                            text: "Policyholder Zip"
                        }

                        CDCopyLabel {
                            id: secPolZip
                            Layout.fillWidth: true
                        }

                        CDDescLabel {
                            text: "Policyholder Date of Birth"
                        }

                        CDDatePicker {
                            id: secPolDob
                            Layout.fillWidth: true
                        }

                        CDDescLabel {
                            text: "Policyholder Gender"
                        }

                        RowLayout {
                            id: secPolGender
                            RadioButton {
                                id: secPolMale
                                text: "Male"
                            }

                            RadioButton {
                                id: secPolFemale
                                text: "Female"
                            }

                            RadioButton {
                                id: secPolOtherGender
                                text: "Other"
                            }

                            ButtonGroup {
                                id: secPolGenderGroup
                                buttons: [priPolMale,priPolFemale,priPolOtherGender]
                            }
                        }

                        CDDescLabel {
                            text: "Relationship to Policyholder"
                        }

                        ComboBox {
                            id: secPolRel
                            Layout.fillWidth: true
                            model: ["Self","Spouse","Dependent","Other"]
                        }

                    }

                }
            }

            MenuSeparator{Layout.columnSpan: 2;Layout.fillWidth: true;}

            RowLayout {
                CDButton {
                    enabled: cardImage.status == Image.Ready
                    text: "See Card"
                    onClicked: cardDia.open();
                    CDTranslucentDialog {
                        id: cardDia
                        Image {
                            id: cardImage
                            source: "file://" + fLocs.getPatientCardFile(PATIENT_FILE_NAME);
                            fillMode: Image.PreserveAspectFit
                            anchors.fill: parent
                            MouseArea {
                                anchors.fill: parent
                                onClicked: {
                                    cardDia.close();
                                }
                            }
                        }
                    }
                }

                Label {
                    Layout.fillWidth: true;
                }


                CDSaveAndCloseButton {
                    text: "Save and Close"
                    onClicked: {
                        dentalPlanSettings.category = "Primary";
                        dentalPlanSettings.setValue("Name",priDenPlanName.editText);
                        dentalPlanSettings.setValue("Addr",priDenPlanAddr.text);
                        dentalPlanSettings.setValue("City",priDenPlanCity.text);
                        dentalPlanSettings.setValue("State",priDenPlanState.currentValue);
                        dentalPlanSettings.setValue("Zip",priDenPlanZip.text);
                        dentalPlanSettings.setValue("SubID",priSubID.text);
                        dentalPlanSettings.setValue("GroupID",priGroupNumb.text);
                        dentalPlanSettings.setValue("EmployerName",priEmployerName.text);
                        dentalPlanSettings.setValue("PolHolderFirst",priPolFirstName.text);
                        dentalPlanSettings.setValue("PolHolderMiddle",priPolMiddleName.text);
                        dentalPlanSettings.setValue("PolHolderLast",priPolLastName.text);
                        dentalPlanSettings.setValue("PolHolderAddress",priPolAddr.text);
                        dentalPlanSettings.setValue("PolHolderCity",priPolCity.text);
                        dentalPlanSettings.setValue("PolHolderState",priPolState.currentValue);
                        dentalPlanSettings.setValue("PolHolderZip",priPolZip.text);
                        dentalPlanSettings.setValue("PolHolderDOB",priPolDob.getDate());
                        dentalPlanSettings.setValue("PolHolderGender",priPolGenderGroup.checkedButton.text);
                        dentalPlanSettings.setValue("PolHolderRel",priPolRel.currentText);
                        dentalPlanSettings.setValue("ClaimsPhoneNumber",priClaimsPhoneNumber.text);
                        dentalPlanSettings.setValue("ClaimsFaxNumber",priClaimsFaxNumber.text);
                        dentalPlanSettings.setValue("ClearingHouseID",priClearingHouseID.text);
                        dentalPlanSettings.setValue("PayDoctor",priPayDoctor.checked);
                        dentalPlanSettings.setValue("DiagPercent",priDiagPercent.value);
                        dentalPlanSettings.setValue("PrevPercent",priPrevPercent.value);
                        dentalPlanSettings.setValue("BasicPercent",priBasicPercent.value);
                        dentalPlanSettings.setValue("MajorPercent",priMajorPercent.value);
                        dentalPlanSettings.setValue("AnnMax",priAnnMax.text);
                        dentalPlanSettings.setValue("AnnDeductAll",priAnnDeductAll.text);
                        dentalPlanSettings.setValue("AnnDeductBasicMajor",priAnnDeductBasicMajor.text);
                        dentalPlanSettings.setValue("AnnDeductMet",priAnnDeductMet.text);
                        dentalPlanSettings.setValue("LastExam",priLastExam.getDate());
                        dentalPlanSettings.setValue("LastBW",priLastBW.getDate());
                        dentalPlanSettings.setValue("LastPan",priLastPan.getDate());
                        dentalPlanSettings.setValue("AdditionalInfo",priaddInfo.text);
                        dentalPlanSettings.setValue("Comments",priComments.text);
                        dentalPlanSettings.setValue("HasRollover",proHasRollover.checked);
                        dentalPlanSettings.setValue("RolloverAmt",proRolloverAmt.text);
                        dentalPlanSettings.setValue("TerminationDate",priTermDate.getDate());
                        dentalPlanSettings.setValue("AmountUsed",priAmountUsed.text);
                        dentalPlanSettings.setValue("CleaningFreq",prophyFreq.currentValue);
                        dentalPlanSettings.setValue("BWFreq",bwFreq.currentValue);
                        dentalPlanSettings.setValue("FMXFreq",fmxFeq.value);
                        dentalPlanSettings.setValue("PanFreq",panFeq.value);
                        dentalPlanSettings.setValue("CompExamFreq",compExFeq.value);
                        dentalPlanSettings.setValue("FluorideCoverage",priFlu.checked);
                        dentalPlanSettings.setValue("FluorideFrequency",priFluFreq.currentValue);
                        dentalPlanSettings.setValue("FluorideAgeLimit",priFluAge.value);
                        dentalPlanSettings.setValue("SealantCoverage",priSealant.checked);
                        dentalPlanSettings.setValue("SealantFrequency",priSealFreq.value);
                        dentalPlanSettings.setValue("SealantAgeLimit",priSealAge.value);

                        dentalPlanSettings.category = "Secondary";
                        dentalPlanSettings.setValue("Dental",secCoverDental.checked);
                        dentalPlanSettings.setValue("Medical",secCoverMed.checked);
                        dentalPlanSettings.setValue("Name",secDenPlanName.text);
                        dentalPlanSettings.setValue("Addr",secDenPlanAddr.text);
                        dentalPlanSettings.setValue("City",secDenPlanCity.text);
                        dentalPlanSettings.setValue("State",secDenPlanState.currentValue);
                        dentalPlanSettings.setValue("Zip",secDenPlanZip.text);
                        dentalPlanSettings.setValue("SubID",secSubID.text);
                        dentalPlanSettings.setValue("GroupID",secGroupNumb.text);
                        dentalPlanSettings.setValue("EmployerName",secEmployerName.text);
                        dentalPlanSettings.setValue("PolHolderFirst",secPolFirstName.text);
                        dentalPlanSettings.setValue("PolHolderMiddle",secPolMiddleName.text);
                        dentalPlanSettings.setValue("PolHolderLast",secPolLastName.text);
                        dentalPlanSettings.setValue("PolHolderAddress",secPolAddr.text);
                        dentalPlanSettings.setValue("PolHolderCity",secPolCity.text);
                        dentalPlanSettings.setValue("PolHolderState",secPolState.currentValue);
                        dentalPlanSettings.setValue("PolHolderZip",secPolZip.text);
                        dentalPlanSettings.setValue("PolHolderDOB",secPolDob.getDate());
                        dentalPlanSettings.setValue("PolHolderGender",secPolGenderGroup.checkedButton.text);
                        dentalPlanSettings.setValue("PolHolderRel",secPolRel.currentText);

                        dentalPlanSettings.category = "Membership"
                        dentalPlanSettings.setValue("HasMembership",membershipPlan.checked);

                        dentalPlanSettings.sync();

                        gitMan.commitData("Updated patient's dental plan for " + PATIENT_FILE_NAME);

                        patBillingdrawer.close();
                        newIns = true;
                    }
                }
            }
        }

    }

    Component.onCompleted: {
        dentalPlanSettings.category = "Primary";
        priDenPlanName.editText = dentalPlanSettings.value("Name","");
        priDenPlanAddr.text = dentalPlanSettings.value("Addr","");
        priDenPlanCity.text = dentalPlanSettings.value("City","");
        priDenPlanState.setStateVal = dentalPlanSettings.value("State","");
        priDenPlanZip.text = dentalPlanSettings.value("Zip","");
        priSubID.text = dentalPlanSettings.value("SubID","");
        priGroupNumb.text = dentalPlanSettings.value("GroupID","");
        priEmployerName.text = dentalPlanSettings.value("EmployerName","");
        priPolFirstName.text = dentalPlanSettings.value("PolHolderFirst","");
        priPolMiddleName.text = dentalPlanSettings.value("PolHolderMiddle","");
        priPolLastName.text = dentalPlanSettings.value("PolHolderLast","");
        priPolAddr.text = dentalPlanSettings.value("PolHolderAddress","");
        priPolCity.text = dentalPlanSettings.value("PolHolderCity","");
        priPolState.setStateVal = dentalPlanSettings.value("PolHolderState","");
        priPolZip.text = dentalPlanSettings.value("PolHolderZip","");
        priPolDob.setDate(dentalPlanSettings.value("PolHolderDOB",""));
        if(dentalPlanSettings.value("PolHolderGender","") === "Male") {
            priPolMale.checked = true;
        }
        else if(dentalPlanSettings.value("PolHolderGender","") === "Female") {
            priPolFemale.checked = true;
        }
        else {
            priPolOtherGender.checked = true;
        }
        priPolRel.currentIndex = priPolRel.find(dentalPlanSettings.value("PolHolderRel",""));
        priClaimsPhoneNumber.text = dentalPlanSettings.value("ClaimsPhoneNumber","")
        priClaimsFaxNumber.text = dentalPlanSettings.value("ClaimsFaxNumber","")
        priClearingHouseID.text = dentalPlanSettings.value("ClearingHouseID","")
        priPayDoctor.checked = dentalPlanSettings.value("PayDoctor","")
        priDiagPercent.value = dentalPlanSettings.value("DiagPercent",0)
        priPrevPercent.value = dentalPlanSettings.value("PrevPercent",0)
        priBasicPercent.value = dentalPlanSettings.value("BasicPercent",0)
        priMajorPercent.value = dentalPlanSettings.value("MajorPercent",0)
        priAnnMax.text = dentalPlanSettings.value("AnnMax","")
        priAnnDeductAll.text = dentalPlanSettings.value("AnnDeductAll","")
        priAnnDeductBasicMajor.text = dentalPlanSettings.value("AnnDeductBasicMajor","")
        priAnnDeductMet.text= dentalPlanSettings.value("AnnDeductMet","")
        priLastExam.setDate(dentalPlanSettings.value("LastExam","1/1/1970"))
        priLastBW.setDate(dentalPlanSettings.value("LastBW","1/1/1970"))
        priLastPan.setDate(dentalPlanSettings.value("LastPan","1/1/1970"))
        priaddInfo.text = dentalPlanSettings.value("AdditionalInfo","");
        priComments.text = dentalPlanSettings.value("Comments","");
        proHasRollover.checked = JSON.parse(dentalPlanSettings.value("HasRollover",false));
        proRolloverAmt.text =dentalPlanSettings.value("RolloverAmt","");
        priTermDate.setDate(dentalPlanSettings.value("TerminationDate","1/1/1970"));
        priAmountUsed.text = dentalPlanSettings.value("AmountUsed","");
        prophyFreq.currentIndex = prophyFreq.indexOfValue( dentalPlanSettings.value("CleaningFreq","1p6"))
        bwFreq.currentIndex = bwFreq.indexOfValue( dentalPlanSettings.value("BWFreq","1p12"))
        fmxFeq.value = dentalPlanSettings.value("FMXFreq",1);
        panFeq.value = dentalPlanSettings.value("PanFreq",1);
        compExFeq.value = dentalPlanSettings.value("CompExamFreq",1);
        priFlu.checked = JSON.parse(dentalPlanSettings.value("FluorideCoverage",false))
        priFluFreq.currentIndex = priFluFreq.indexOfValue(dentalPlanSettings.value("FluorideFrequency","1py"))
        priFluAge.value =  dentalPlanSettings.value("FluorideAgeLimit",18);
        priSealant.checked = JSON.parse(dentalPlanSettings.value("SealantCoverage",false))
        priSealFreq.value = dentalPlanSettings.value("SealantFrequency",7);
        priSealAge.value =  dentalPlanSettings.value("SealantAgeLimit",18);

        dentalPlanSettings.category = "Secondary";
        secCoverDental.checked = JSON.parse(dentalPlanSettings.value("Dental"));
        secCoverMed.checked = JSON.parse(dentalPlanSettings.value("Medical"));
        secDenPlanName.text = dentalPlanSettings.value("Name","");
        secDenPlanAddr.text = dentalPlanSettings.value("Addr","");
        secDenPlanCity.text = dentalPlanSettings.value("City","");
        secDenPlanState.setStateVal = dentalPlanSettings.value("State","");
        secDenPlanZip.text = dentalPlanSettings.value("Zip","");
        secSubID.text = dentalPlanSettings.value("SubID","");
        secGroupNumb.text = dentalPlanSettings.value("GroupID","");
        secEmployerName.text = dentalPlanSettings.value("EmployerName","");
        secPolFirstName.text = dentalPlanSettings.value("PolHolderFirst","");
        secPolMiddleName.text = dentalPlanSettings.value("PolHolderMiddle","");
        secPolLastName.text = dentalPlanSettings.value("PolHolderLast","");
        secPolAddr.text = dentalPlanSettings.value("PolHolderAddress","");
        secPolCity.text = dentalPlanSettings.value("PolHolderCity","");
        secPolState.setStateVal = dentalPlanSettings.value("PolHolderState","");
        secPolZip.text = dentalPlanSettings.value("PolHolderZip","");
        secPolDob.setDate(dentalPlanSettings.value("PolHolderDOB",""));
        if(dentalPlanSettings.value("PolHolderGender","") === "Male") {
            secPolMale.checked = true;
        }
        else if(dentalPlanSettings.value("PolHolderGender","") === "Female") {
            secPolFemale.checked = true;
        }
        else {
            secPolOtherGender.checked = true;
        }
        secPolRel.currentIndex = secPolRel.find(dentalPlanSettings.value("PolHolderRel",""));

        dentalPlanSettings.category = "Membership";
        membershipPlan.checked = JSON.parse(dentalPlanSettings.value("HasMembership",false));

        dentalPlanList = JSON.parse(constLoader.getUSDentalPlanListJSON());
    }


}
