// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTranslucentDialog {
    id: editProcedureDia

    property var procedureLedgerObj: ({})

    ColumnLayout {
        CDTranslucentPane {
            Layout.fillWidth: true
            GridLayout {
                columns: 2
                CDHeaderLabel {
                    text: "Edit Ledger Procedure"
                    Layout.columnSpan: 2
                }
                CDDescLabel {
                    text: "Base Price"
                }

                TextField {
                    id: basePriceField
                }

                CDDescLabel {
                    text: "Procedure Completed"
                }

                CheckBox {
                    id: procedureCompletedBox
                }

                CDDescLabel {
                    visible: procedureCompletedBox.checked
                    text: "Procedure Date"
                }

                CDCalendarDatePicker {
                    id: procedureDateCal
                    visible: procedureCompletedBox.checked
                }

                CDDescLabel {
                    text: "Claim Status"
                }

                ComboBox {
                    id: claimStatusBox
                    editable: false
                    model: ["Unsent", "Sent", "Do Not Submit", "Rejected", "Submitted Again", "Given Up",
                        "Paid Partially", "Fully Paid"]
                    Layout.fillWidth: true
                }

                CDDescLabel {
                    text: "Additional Comment"
                }
                TextField {
                    id: commentField
                    Layout.fillWidth: true
                }
            }
        }

        RowLayout {
            CDCancelButton {
                onClicked: {
                    editProcedureDia.reject();
                }
            }
            CDDeleteButton {
                text: "Delete Procedure"
                onClicked: {
                    deletePane.visible = true;
                }
            }

            Label {
                Layout.fillWidth: true
            }
            CDAddButton {
                icon.name: "document-edit"
                text: "Edit Procedure"
                onClicked: {
                    procedureLedgerObj["Procedure"]["BasePrice"] = basePriceField.text;
                    procedureLedgerObj["Comment"] = commentField.text;
                    if(procedureCompletedBox.checked) {
                        procedureLedgerObj["ProcedureDate"] = procedureDateCal.myDate
                    }
                    else {
                        delete procedureLedgerObj["ProcedureDate"]
                    }
                    procedureLedgerObj["ClaimStatus"] = claimStatusBox.currentValue
                    editProcedureDia.accept();
                }
            }
        }

        CDTranslucentPane {
            id: deletePane
            visible: false
            backMaterialColor: Material.Red
            GridLayout {
                columns: 2
                CDHeaderLabel {
                    text: "Are you sure? There is no undo!"
                    Layout.columnSpan: 2
                }
                CDCancelButton {
                    text: "No, nevermind"
                    onClicked: {
                        deletePane.visible = false;
                    }
                }
                CDDeleteButton {
                    text: "Yes, delete the procedure"
                    onClicked: {
                        procedureLedgerObj["DeleteMe"]  = true;
                        editProcedureDia.accept();
                    }
                }
            }
        }
    }


    Component.onCompleted: {
        basePriceField.text = procedureLedgerObj["Procedure"]["BasePrice"];
        if("ProcedureDate" in procedureLedgerObj) {
            procedureCompletedBox.checked = true;
            procedureDateCal.myDate = new Date( procedureLedgerObj["ProcedureDate"])
        }
        else {
            procedureCompletedBox.checked = false;
        }
        claimStatusBox.currentIndex = claimStatusBox.find( procedureLedgerObj["ClaimStatus"])
        if("Comment" in procedureLedgerObj) {
            commentField.text = procedureLedgerObj["Comment"];
        }
    }

}
