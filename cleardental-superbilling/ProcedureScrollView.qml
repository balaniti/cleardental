// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

ListView {
    id: procedureScrollView
    clip: true

    ScrollBar.vertical: ScrollBar{}


    property var scrollModel: []

    property var checkboxArray: []
    property var textFieldArray: []

    property real paymentAmount: 0

    property real totaledUpPayments: 0

    function isPaidOff(getLedgerItem) {
        var paymentArray = getLedgerItem["Payments"];
        var basePrice = parseFloat( getLedgerItem["Procedure"]["BasePrice"]);
        var sumAmounts =0;
        for(var i=0;i<paymentArray.length;i++) {
            var paymentItem = paymentArray[i];
            if(paymentItem["Type"] === "Dental Plan Payment") {
                sumAmounts += parseFloat(paymentItem["WriteOff"]);
            }
            sumAmounts += parseFloat(paymentItem["Amount"]);
        }

        //console.debug("Procedure: " + getLedgerItem["Procedure"]["ProcedureName"] + " paid to $" + sumAmounts);

        return sumAmounts >= basePrice;
    }

    function alreadyInModel(getProcedure) {
        for(var i=0;i<scrollModel.length;i++) {
            if(comFuns.isEqual(getProcedure,scrollModel[i].ProcedureObj)) {
                return true;
            }
        }
        return false;
    }


    function loadAllProcedures() { //will load all treatment planned and completed procedures
        this.model = 0;
        scrollModel = [];
        checkboxArray= [];
        textFieldArray = [];


        //first do the completed procedures that HAVE NOT been paid off
        for(var i=0;i<rootWin.ledgerList.length;i++) {
            if(!isPaidOff(rootWin.ledgerList[i])) {
                var addMe = {
                    ProcedureObj: rootWin.ledgerList[i]["Procedure"],
                    FromLedger: true,
                    LedgerIndex: i
                };
                scrollModel.push(addMe);
            }
        }

        //then all the the treatment planned procedures; not including the ones that have been already partially paid
        var txPlanList = comFuns.getAllTxPlanItems(PATIENT_FILE_NAME);
        for(i=0;i<txPlanList.length;i++) {
            if(!alreadyInModel(txPlanList[i])) {
                //console.debug(JSON.stringify(txPlanList[i]));
                addMe = {
                    ProcedureObj: txPlanList[i],
                    FromLedger: false
                }

                scrollModel.push(addMe);
            }
        }

        this.model = scrollModel.length
    }

    function updateCheckmarks() {
        var checkIndexes = [];
        for(var i=0;i<checkboxArray.length;i++) {
            if(checkboxArray[i].checked) {
                checkIndexes.push(i);
            }
        }

        if(checkIndexes.length == 1) {
            textFieldArray[checkIndexes[0]].text = paymentAmount;
        }

        else {
            var leftOver = paymentAmount;
            for(var w=0;w<checkIndexes.length;w++) {
                //console.debug(JSON.stringify(checkboxArray[checkIndexes[w]].procedureObject));
                var basePrice = parseFloat( checkboxArray[checkIndexes[w]].procedureObject["BasePrice"]);
                if(leftOver > basePrice) {
                    textFieldArray[checkIndexes[w]].text = basePrice;
                    leftOver-= basePrice;
                }
                else {
                    textFieldArray[checkIndexes[w]].text = leftOver;
                    leftOver =0;
                }
            }
        }

    }

    function updateTextInput() {
        totaledUpPayments = 0;
        for(var i=0;i<checkboxArray.length;i++) {
            if(checkboxArray[i].checked) {
                totaledUpPayments+= parseFloat(textFieldArray[i].text);
            }
        }
    }

    delegate: RowLayout {
        width: 360
        height: 50
        CheckBox {
            id: procedureCheckBox

            text: {
                var proStr = comFuns.makeTxItemString(scrollModel[index]["ProcedureObj"]);
                proStr+= " ($" + scrollModel[index]["ProcedureObj"]["BasePrice"] + ")";
                return proStr;
            }
            property var procedureObject: scrollModel[index]["ProcedureObj"];
            property bool selectionObject: scrollModel[index];
            onCheckedChanged: {
                updateCheckmarks();
            }



            Component.onCompleted: {
                checkboxArray.push(this);
            }


            background: Rectangle {
                color: scrollModel[index].FromLedger ? Material.color(Material.LightGreen)
                                                     : "transparent"
            }
        }

        TextField {
            visible: procedureCheckBox.checked
            Layout.fillWidth: true

            Component.onCompleted: {
                textFieldArray.push(this);
            }

            onTextChanged: {
                updateTextInput();
            }
        }
    }

    onVisibleChanged: {
        if(visible) {
            loadAllProcedures();
        }
    }




}
