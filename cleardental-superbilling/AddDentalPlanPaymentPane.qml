// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTranslucentPane {
    id: dentalPlanPaymentPane

    backMaterialColor: Material.Yellow

    property real txBasePrice: 0.0
    property alias getMaxAllowed: maxAllowed.text
    property alias getPlanPayment: planPayment.text
    property alias getPatientPortion: patPortion.text
    property alias getWriteOff: writeOff.text
    property alias getPlanName: whichPlanBox.currentValue
    property alias getNewClaimStatus: newClaimStatus.currentValue

    property bool enteredMaxAllowed: false
    property bool enteredPlanPayment: false
    property bool enteredPatientPortion: false
    property bool enteredWriteoff: false
    property bool showNewClaimStatus: true

    function goThroughValues() {
        // txBasePrice = writeOff + ptPortion + dentalPlanPayment
        // maxAllowed = ptPortion + dentalPlanPayment
        // txBasePrice = writeOff + maxAllowed
        var flMaxAllowed = parseFloat( maxAllowed.text);
        var flPlanPayment = parseFloat( planPayment.text);
        var flPtPortion = parseFloat( patPortion.text);
        var flWriteOff = parseFloat( writeOff.text);
        var setMaxAllowed =0;
        var setPlanPayment =0;
        var setPtPortion =0;
        var setWriteOff = 0;

        if (enteredMaxAllowed) {
            if (enteredPlanPayment) {
                if (enteredPatientPortion) {
                    if (enteredWriteoff) {
                        // All variables are true
                        // Nothing needs to be done
                    } else {
                        // enteredWriteoff is false
                        // all other's are true
                        setWriteOff = txBasePrice - flPlanPayment - flPtPortion;
                    }
                } else {
                    if (enteredWriteoff) {
                        // enteredPatientPortion is false
                        setPtPortion = txBasePrice - flPlanPayment - flWriteOff;
                    } else {
                        // enteredPatientPortion and enteredWriteoff are false
                        // enteredMaxAllowed and enteredPlanPayment are true
                        // For now, we will assume patient pays nothing
                        setPtPortion = 0;
                        setWriteOff = txBasePrice - flPlanPayment;
                    }
                }
            } else {
                if (enteredPatientPortion) {
                    if (enteredWriteoff) {
                        // enteredPlanPayment is false
                        setPlanPayment = flMaxAllowed - flWriteOff - flPtPortion;
                    } else {
                        // enteredPlanPayment and enteredWriteoff are false
                        setPlanPayment = 0;
                        setWriteOff = txBasePrice - flPtPortion;
                    }
                } else {
                    if (enteredWriteoff) {
                        // enteredPlanPayment and enteredPatientPortion are false
                        setPlanPayment = 0;
                        setPtPortion = txBasePrice - setWriteOff;
                    } else {
                        // enteredPlanPayment, enteredPatientPortion, and enteredWriteoff are false
                        setPlanPayment = flMaxAllowed;
                        setPtPortion = 0;
                        setWriteOff = txBasePrice - flMaxAllowed;
                    }
                }
            }
        } else {
            if (enteredPlanPayment) {
                if (enteredPatientPortion) {
                    if (enteredWriteoff) {
                        // enteredMaxAllowed is false
                        setMaxAllowed = flPlanPayment + flPtPortion
                    } else {
                        // enteredMaxAllowed and enteredWriteoff are false
                        setMaxAllowed = flPlanPayment + flPtPortion
                        setWriteOff = txBasePrice - setMaxAllowed
                    }
                } else {
                    if (enteredWriteoff) {
                        // enteredMaxAllowed and enteredPatientPortion are false
                        // enteredPlanPayment and enteredWriteoff are true
                        setMaxAllowed = txBasePrice - (flWriteOff + flPlanPayment)
                        setPtPortion =  setMaxAllowed -  flPlanPayment
                    } else {
                        // enteredMaxAllowed, enteredPatientPortion, and enteredWriteoff are false
                        // only enteredPlanPayment is true
                        setMaxAllowed = flPlanPayment;
                        setPtPortion = 0;
                        setWriteOff = txBasePrice - flPlanPayment;
                    }
                }
            } else {
                if (enteredPatientPortion) {
                    if (enteredWriteoff) {
                        // enteredMaxAllowed and enteredPlanPayment are false
                        // enteredPatientPortion and enteredWriteoff are true
                        setMaxAllowed = flWriteOff + flPtPortion
                        setPlanPayment = 0;

                    } else {
                        // enteredMaxAllowed, enteredPlanPayment, and enteredWriteoff are false
                        // only enteredPatientPortion is true
                        setMaxAllowed = flPtPortion;
                        setPlanPayment = 0;
                        setWriteOff = txBasePrice - flPtPortion
                    }
                } else {
                    if (enteredWriteoff) {
                        // enteredMaxAllowed, enteredPlanPayment, and enteredPatientPortion are false
                        // only enteredWriteoff is true
                        setMaxAllowed = txBasePrice - flWriteOff;
                        setPlanPayment =0;
                        setPtPortion = setMaxAllowed;

                    } else {
                        // All variables are false
                        // Nothing can be done
                    }
                }
            }
        }

        //Now set the textboxes accordingly

        if(!enteredMaxAllowed) {
            maxAllowed.text = setMaxAllowed.toFixed(2);
        }
        if(!enteredPatientPortion) {
            patPortion.text = setPtPortion.toFixed(2);
        }
        if(!enteredPlanPayment) {
            planPayment.text = setPlanPayment.toFixed(2);
        }
        if(!enteredWriteoff) {
            writeOff.text = setWriteOff.toFixed(2);
        }
    }

    function updateInsPayValues(getFrom) {
        if(getFrom === "maxAllowed") {
            dentalPlanPaymentPane.enteredMaxAllowed = true;
        }
        else if(getFrom === "planPayment") {
            dentalPlanPaymentPane.enteredPlanPayment = true;
        }
        else if(getFrom === "patPortion") {
            dentalPlanPaymentPane.enteredPatientPortion = true;
        }
        else if(getFrom === "writeOff") {
            dentalPlanPaymentPane.enteredWriteoff = true
        }
        goThroughValues();
    }

    function setPlanName(getNewName) {
        whichPlanBox.currentIndex = whichPlanBox.find(getNewName);
    }

    GridLayout {
        columns: 2

        CDDescLabel {
            text: "Which dental plan"
        }

        ComboBox {
            id: whichPlanBox
            Layout.fillWidth: true
            Layout.minimumWidth: 360
            property bool newInsVal: rootWin.newIns
            editable: true

            onNewInsValChanged: {
                if(rootWin.newIns) {
                    refreshDentalPlans();
                    rootWin.newIns = false;
                }
            }

            function refreshDentalPlans() {
                dentalPlanSettings.category = "Primary";
                var priName = dentalPlanSettings.value("Name","N/A");

                dentalPlanSettings.category = "Secondary";
                var secName = dentalPlanSettings.value("Name","N/A");

                model = [priName,secName];
            }

            Component.onCompleted: {
                refreshDentalPlans();
            }
        }

        CDDescLabel {
            text: "How much paid by plan ($)"
        }

        TextField {
            id: planPayment
            Layout.fillWidth: true
            onTextEdited: updateInsPayValues("planPayment");
            onActiveFocusChanged: {
                if(activeFocus) {
                    selectAll();
                }
            }
            validator: DoubleValidator {}

        }

        CDDescLabel {
            text: "Max Allowed ($)"
        }

        TextField {
            id: maxAllowed
            Layout.fillWidth: true
            onTextEdited: updateInsPayValues("maxAllowed");
            onActiveFocusChanged: {
                if(activeFocus) {
                    selectAll();
                }
            }
            validator: DoubleValidator {}
        }

        CDDescLabel {
            text: "Patient portion ($)"
        }

        TextField {
            id: patPortion
            Layout.fillWidth: true
            onTextEdited: updateInsPayValues("patPortion");
            onActiveFocusChanged: {
                if(activeFocus) {
                    selectAll();
                }
            }
            validator: DoubleValidator {}
        }


        CDDescLabel {
            text: "Write Off ($)"
        }

        TextField {
            id: writeOff
            Layout.fillWidth: true
            onTextEdited: updateInsPayValues("writeOff");
            onActiveFocusChanged: {
                if(activeFocus) {
                    selectAll();
                }
            }
            validator: DoubleValidator {}
        }

        CDDescLabel {
            text: "Set the Claim Status to"
            visible: showNewClaimStatus
        }

        ComboBox {
            id: newClaimStatus
            model: ["Fully Paid", "Paid Partially"]
            Layout.minimumWidth: 240
            visible: showNewClaimStatus
        }

    }
}
