QT += quick

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main-super-billing.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /usr/bin/
!isEmpty(target.path): INSTALLS += target

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../cleardental-backend/release/ -lcleardental-backend
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../cleardental-backend/debug/ -lcleardental-backend
else:unix: LIBS += -L$$OUT_PWD/../cleardental-backend/ -lcleardental-backend

INCLUDEPATH += $$PWD/../cleardental-backend
DEPENDPATH += $$PWD/../cleardental-backend
