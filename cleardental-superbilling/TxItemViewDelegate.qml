// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtGraphicalEffects 1.15
import QtQuick.Particles 2.15

Rectangle {
    id: txItemViewDelegate

    property real paidPercentage: 0
    property var procedureLedgerObject: ({}) //just to make is easy; don't write in to it
    property int ledgerIndex: -1
    height: txInfoRow.height + paymentsGrid.height + (radius * 2)
    radius: 10
    border.color: Material.foreground
    color: Qt.rgba(1,1,0,.2) //yellow = no money

    clip: true

    property bool fullyPaid: totalCollectionsFromTx + totalWriteoffsFromTx >= basePrice

    property real totalCollectionsFromTx: 0
    property real totalWriteoffsFromTx: 0
    property real basePrice: parseFloat(procedureLedgerObject["Procedure"]["BasePrice"])

    Row {
        opacity: .5
        id: rectRows
        visible: totalWriteoffsFromTx + totalCollectionsFromTx > 1
        Rectangle {
            id: payAmountRect
            border.color: Material.foreground
            height: txItemViewDelegate.height
            width: txItemViewDelegate.width * (totalCollectionsFromTx / basePrice )
            color: Material.color(Material.Green)

            BillingParticleSystem {
                id: creditCardParticles
                anchors.fill: parent
                emitString: "💳"
                running: visible
            }

            BillingParticleSystem {
                id: cashParticles
                anchors.fill: parent
                emitString: "💵"
                running: visible
            }

            BillingParticleSystem {
                id: dentalPlanPaymentParticles
                anchors.fill: parent
                emitString: "🪙"
                running: visible
            }
            clip: true

        }

        Rectangle {
            id: writeOffRect
            border.color: Material.foreground
            height: txItemViewDelegate.height
            width: txItemViewDelegate.width * (totalWriteoffsFromTx / basePrice )
            color: Material.color(Material.Purple)

            BillingParticleSystem {
                id: insuranceWriteoff
                anchors.fill: parent
                emitString: "✍"
                goingUp: false
                running: visible
            }
            BillingParticleSystem {
                id: clinicCredit
                anchors.fill: parent
                emitString: "💰"
                goingUp: false
                running: visible
            }
            clip: true
        }
    }



    RowLayout {
        id: txInfoRow
        x: txItemViewDelegate.radius
        width: txItemViewDelegate.width - (txItemViewDelegate.radius*2) - addPaymentButton.width

        CDDescLabel {
            text: "Procedure"
        }
        Label {
            text: ("DCode" in procedureLedgerObject["Procedure"]) ?
                      comFuns.makeTxItemString(procedureLedgerObject["Procedure"]) + " (" +
                      procedureLedgerObject["Procedure"]["DCode"] +")" :
                      comFuns.makeTxItemString(procedureLedgerObject["Procedure"])
        }
        Label {
            Layout.fillWidth: true
        }
        CDDescLabel {
            text: "Date of Procedure"
        }
        Label {
            text: {
                if("ProcedureDate" in procedureLedgerObject) {
                    return (new Date(procedureLedgerObject["ProcedureDate"])).toLocaleDateString();
                }
                else {
                    return "Not Completed"
                }
            }
        }

        Label {
            Layout.fillWidth: true
        }

        CDDescLabel {
            text: "Base Price"
        }
        Label {
            text: "$" + procedureLedgerObject["Procedure"]["BasePrice"]
        }

        Label {
            Layout.fillWidth: true
        }
        CDDescLabel {
            text: "Claim Status"
        }

        Label {
            text: procedureLedgerObject["ClaimStatus"] ;
        }

        Label {
            Layout.fillWidth: true
        }

        CDDescLabel {
            text: "Total Production from Procedure"
        }

        Label {
            text: "$" + txItemViewDelegate.totalCollectionsFromTx.toFixed(2);
        }

        Label {
            Layout.fillWidth: true
        }

        CDDescLabel {
            text: "Comment"
        }

        Label {
            text: {
                if("Comment" in procedureLedgerObject) {
                    return procedureLedgerObject["Comment"]
                }
                return "";
            }
        }

        Label {
            Layout.fillWidth: true
        }

        CDButton {
            icon.name: "document-edit"
            onClicked: editProcedureLineDialog.open();
        }
    }

    AddTxPaymentDialog {
        id: addTxPaymentDialog
        setTxBasePrice: parseFloat(procedureLedgerObject["Procedure"]["BasePrice"])
        totalCollectionsSoFar: parseFloat(txItemViewDelegate.totalCollectionsFromTx)
        totalWriteOffsSoFar: txItemViewDelegate.totalWriteoffsFromTx
        ledgerItemObject: procedureLedgerObject
        onAccepted: {
            rootWin.ledgerList[ledgerIndex]["Payments"].push(paymentItemObject);
            if(newClaimStatus.length > 0) {
                rootWin.ledgerList[ledgerIndex]["ClaimStatus"] = newClaimStatus;
            }

            rootWin.saveAndRefresh("added " + paymentItemObject["Type"] + " payment");
        }
    }

    EditProcedureLineDialog {
        id: editProcedureLineDialog
        procedureLedgerObj: txItemViewDelegate.procedureLedgerObject
        onAccepted: {
            if("DeleteMe" in editProcedureLineDialog.procedureLedgerObj) {
                rootWin.ledgerList.splice(ledgerIndex,1);
                rootWin.saveAndRefresh("deleted procedure");
            }
            else {
                rootWin.ledgerList[ledgerIndex] = editProcedureLineDialog.procedureLedgerObj
                rootWin.saveAndRefresh("edited procedure");
            }


        }
    }

    CDAddButton {
        id: addPaymentButton
        text: "Add Payment"
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        anchors.margins: 10
        onClicked: addTxPaymentDialog.open();
    }

    GridLayout {
        id: paymentsGrid
        anchors.left: txInfoRow.left
        anchors.top: txInfoRow.bottom
        anchors.right: addPaymentButton.left
        anchors.margins:5
        columns: 5

        Component {
            id: labComp
            Label {

            }
        }
        Component {
            id: buttonComp
            CDButton {
                icon.name: "document-edit"
                Layout.maximumHeight: 42
                Layout.maximumWidth: 42
                property alias setPaymentLineIndex:editPaymentLineDialog.paymentLineIndex
                property alias setPaymentObject:editPaymentLineDialog.paymentObj
                onClicked: {
                    editPaymentLineDialog.open();
                }

                EditPaymentLineDialog {
                    id: editPaymentLineDialog

                    onAccepted: {
                        if("DeleteMe" in paymentObj) {
                            //console.debug("paymentLineIndex: " + paymentLineIndex)
                            //console.debug("ledgerIndex: " + ledgerIndex)
                            rootWin.ledgerList[ledgerIndex]["Payments"].splice(paymentLineIndex,1);
                            saveAndRefresh("removed payment");
                        }
                        else {
                            rootWin.ledgerList[ledgerIndex]["Payments"][paymentLineIndex] = paymentObj;
                            saveAndRefresh("updated payment");
                        }
                    }
                }
            }
        }

        RowLayout {
            Layout.columnSpan: parent.columns
            CDDescLabel {
                text: "Payments"

            }
            Label {
                font.italic: true
                text: {
                    if(fullyPaid) {
                        return ""
                    }
                    else {
                        return "Needs $" + (basePrice - totalCollectionsFromTx - totalWriteoffsFromTx) + "";
                    }
                }
            }
        }

        CDDescLabel {
            text: "Type"
        }
        CDDescLabel {
            text: "Date"
        }
        CDDescLabel {
            text: "Amount"
        }
        CDDescLabel {
            text: "Comment"
        }
        CDDescLabel {
            text: "Edit"
        }

        function loadPayments() {
            var paymentArray = procedureLedgerObject["Payments"];
            for(var i=0;i<paymentArray.length;i++) {
                var paymentItem = paymentArray[i];
                labComp.createObject(paymentsGrid,{text: paymentItem["Type"]})
                labComp.createObject(paymentsGrid,{text: Qt.formatDate(paymentItem["Date"],"MMM/d/yyyy")})
                labComp.createObject(paymentsGrid,{text: "$" + paymentItem["Amount"]})

                //Add in comments

                if(paymentItem["Type"] === "Credit Card") {
                    labComp.createObject(paymentsGrid,{text: "Recepit: " + paymentItem["ConfirmationNumber"]})
                }
                else if(paymentItem["Type"] === "Credit") {
                    labComp.createObject(paymentsGrid,{text: paymentItem["Reason"]})

                }
                else if(paymentItem["Type"] === "Dental Plan Payment") {
                    var makeText = "Writeoff: $" + paymentItem["WriteOff"] + ";" + " PP: $" +
                            paymentItem["PatientPortion"];
                    labComp.createObject(paymentsGrid,{text: makeText})
                }
                else {
                    labComp.createObject(paymentsGrid,{text: ""})
                }
                buttonComp.createObject(paymentsGrid,{setPaymentObject:paymentItem,setPaymentLineIndex:i})
            }

            txItemViewDelegate.paidPercentage = totalCollectionsFromTx/ procedureLedgerObject["Procedure"]["BasePrice"];
        }

        Component.onCompleted: {
            loadPayments();
        }

    }
}
