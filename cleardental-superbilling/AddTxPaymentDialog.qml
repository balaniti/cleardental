// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTranslucentDialog {
    id: addTxPaymentDialog

    property var ledgerItemObject: ({})
    property var paymentItemObject: ({})
    property real setTxBasePrice: 0
    property real totalCollectionsSoFar: 0
    property real totalWriteOffsSoFar: 0
    property string newClaimStatus: ""

    onVisibleChanged: {
        if(visible) {
            datePicker.setDateObj(rootWin.lastUsedDate);
        }
    }

    ColumnLayout {
        CDTranslucentPane {
            GridLayout {
                columns: 2
                CDHeaderLabel {

                    Layout.columnSpan: 2

                    Component.onCompleted: {
                        text = "Add Treatment Payment for " + comFuns.makeTxItemString(ledgerItemObject["Procedure"]) +
                              " (" + ledgerItemObject["Procedure"]["DCode"] + ")";
                        if("ProcedureDate" in ledgerItemObject) {
                            text += " done on " + (new Date(ledgerItemObject["ProcedureDate"])).toLocaleDateString()
                        }

                    }
                }
                CDDescLabel {
                    text: "Payment Type"
                }

                ComboBox {
                    id: paymentType
                    model: ["Dental Plan Payment","Credit Card","Cash","Check","Writeoff"]
                    Layout.minimumWidth: 360
                }

                AddDentalPlanPaymentPane {
                    id: dentalPlanPayment
                    Layout.columnSpan: 2
                    visible: paymentType.currentIndex == 0
                    Layout.fillWidth: true
                    txBasePrice: setTxBasePrice
                }

                AddCreditCardPaymentPane {
                    id: creditCardPayment
                    Layout.columnSpan: 2
                    visible: paymentType.currentIndex == 1
                    Layout.fillWidth: true
                }

                AddCashPaymentPane {
                    id: cashPayment
                    Layout.columnSpan: 2
                    Layout.fillWidth: true
                    visible: paymentType.currentIndex == 2
                }
                AddCheckPaymentPane {
                    id: checkPayment
                    Layout.columnSpan: 2
                    Layout.fillWidth: true
                    visible: paymentType.currentIndex == 3
                }
                AddWriteoffPane {
                    id: writeOffPane
                    Layout.columnSpan: 2
                    Layout.fillWidth: true
                    visible: paymentType.currentIndex == 4
                    isTx: true
                    autoWriteOffAmount: setTxBasePrice - totalCollectionsSoFar - totalWriteOffsSoFar;
                }
                CDDescLabel {
                    text: "Payment Date"
                }
                CDDatePicker {
                    id: datePicker
                }
            }
        }

        RowLayout {
            Layout.fillWidth: true
            CDCancelButton {
                onClicked: addTxPaymentDialog.reject();
            }
            Label {
                Layout.fillWidth: true
            }
            CDAddButton {
                text: "Add Payment"
                onClicked: {
                    paymentItemObject = ({});
                    if(paymentType.currentIndex == 0) { //AddDentalPlanPaymentPane
                        paymentItemObject["Type"] = "Dental Plan Payment";
                        paymentItemObject["Amount"] = dentalPlanPayment.getPlanPayment;
                        paymentItemObject["PlanName"] = dentalPlanPayment.getPlanName;
                        paymentItemObject["MaxAllowed"] = dentalPlanPayment.getMaxAllowed;
                        paymentItemObject["PatientPortion"] = dentalPlanPayment.getPatientPortion;
                        paymentItemObject["WriteOff"] = dentalPlanPayment.getWriteOff;
                        addTxPaymentDialog.newClaimStatus = dentalPlanPayment.getNewClaimStatus;
                    }
                    else if(paymentType.currentIndex == 1) { //AddCreditCardPaymentPane
                        paymentItemObject["Type"] = "Credit Card";
                        paymentItemObject["Amount"] = creditCardPayment.getAmountGotten;
                        paymentItemObject["ConfirmationNumber"] = creditCardPayment.getConfirmNumber;
                    }
                    else if(paymentType.currentIndex == 2) { //AddCashPaymentPane
                        paymentItemObject["Type"] = "Cash";
                        paymentItemObject["Amount"] = cashPayment.getAmountGotten;
                    }
                    else if(paymentType.currentIndex == 3) { //AddCheckPaymentPane
                        paymentItemObject["Type"] = "Check";
                        paymentItemObject["Amount"] = checkPayment.getAmountGotten;
                    }
                    else if(paymentType.currentIndex == 4) { //AddWriteoffPane
                        paymentItemObject["Type"] = "Credit";
                        paymentItemObject["Amount"] = writeOffPane.getAmountGotten;
                        paymentItemObject["Reason"] = writeOffPane.getReason;
                    }

                    paymentItemObject["Date"] = datePicker.getDateObj();
                    rootWin.lastUsedDate = datePicker.getDateObj();

                    addTxPaymentDialog.accept();
                }
            }
        }
    }

}
