// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDAppWindow {
    id: rootWin
    title: qsTr("Billing " + " [ID: " + PATIENT_FILE_NAME + "]")

    property var ledgerList: []
    property bool newIns: false;
    property var lastUsedDate:  new Date()

    function saveAndRefresh(commitInfo) {
        var ledgerListString = JSON.stringify(ledgerList,null,"\t");
        ledMan.saveFile(fLocs.getFullBillingFile(PATIENT_FILE_NAME),ledgerListString);
        gitMan.commitData("Updated ledger for " + PATIENT_FILE_NAME + " with " + commitInfo);
        billingListView.oldScrollAmount = billingListView.contentY
        rootWin.ledgerList = []; //force a refresh
        rootWin.ledgerList = JSON.parse(ledgerListString);
        billingListView.contentY = billingListView.oldScrollAmount
    }

    function refreshView() {
        var ledgerListString = ledMan.readFile(fLocs.getFullBillingFile(PATIENT_FILE_NAME));
        if(ledgerListString.length < 2) {
            rootWin.ledgerList = [];
        }
        else {
            rootWin.ledgerList = JSON.parse(ledgerListString);
        }
    }

    CDCommonFunctions {
        id: comFuns
    }

    CDFileLocations {
        id: fLocs
    }

    CDTextFileManager {
        id: ledMan
    }

    CDGitManager {
        id: gitMan
    }

    CDConstLoader{
        id: commonConsts
    }

    Settings {
        id: dentalPlanSettings
        fileName: fLocs.getDentalPlansFile(PATIENT_FILE_NAME);
    }

    header: CDPatientToolBar {
        headerText: "Billing: "
        ToolButton {
             icon.name: "application-menu"
             icon.width: 64
             icon.height: 64
             onClicked: drawer.open();
             anchors.left: parent.left
         }
    }

    BillingDrawer {
        id: drawer
        width: prefWidth + 20
        height: rootWin.height
    }

    BillingListView {
        id: billingListView
        anchors.fill: parent
        anchors.margins: 10
    }

    AddLoosePaymentDialog {
        id: addPaymentDialog
        anchors.centerIn: parent
        onAccepted: refreshView()
    }

    ManualAddHistoryDialog {
        id: manualAddHistoryDialog
        anchors.centerIn: parent
        onAccepted: refreshView()
    }

    CDCancelButton {
        id: printButton
        icon.name: "document-print"
        text: "Print Ledger"
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.margins: 10

        CDPrinter {
            id: printer
        }

        onClicked: {
            printer.printPatientLedger(PATIENT_FILE_NAME,ledgerList)
        }
    }

    CDButton {
        icon.name: "document-export"
        text: "Submit Claims"
        anchors.left: parent.left
        anchors.bottom: printButton.top
        anchors.margins: 10
        onClicked: {
            submitClaimsDia.open();
        }
    }

    CDAddButton {
        id: addLoosePaymentButton
        text: "Add Spread Payment"
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: 10
        onClicked:  {
            addPaymentDialog.open();
        }
    }

    AddSpreadPaymentDialog {
        id: addSpreadPaymentDia
    }

    SubmitClaimsDialog {
        id: submitClaimsDia

        onAccepted: {
            saveAndRefresh("Submitted claims for " + PATIENT_FILE_NAME);
        }

    }

    CDAddButton {
        id: manualAddHistoryButton
        text: "Manual Add History"
        anchors.bottom: addLoosePaymentButton.top
        anchors.right: parent.right
        anchors.margins: 10
        visible: commonConsts.isDebugMode()
        onClicked: manualAddHistoryDialog.open();
    }

    CDTranslucentPane {
        id: totalPane
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins: 10
        setOpacity: .75
        property bool patOwes: false
        backMaterialColor: patOwes ? Material.Red : Material.Green
        RowLayout {
            id: summaryRow

            CDDescLabel {
                text: "Current Balance"
            }
            Label {
                text: {
                    var returnMe =0;
                    for(var w=0;w<ledgerList.length;w++) {
                        returnMe += parseFloat( ledgerList[w]["Procedure"]["BasePrice"]);
                        var paymentArray = ledgerList[w]["Payments"];
                        for(var i=0;i<paymentArray.length;i++) {
                            var paymentItem = paymentArray[i];
                            if(paymentItem["Type"] === "Dental Plan Payment") {
                                returnMe -= parseFloat(paymentItem["WriteOff"]);
                            }
                            returnMe -= parseFloat(paymentItem["Amount"]);
                        }
                    }
                    totalPane.patOwes = returnMe > 0.01;
                    return "$"+returnMe.toFixed(2);
                }
            }
            Label {
                Layout.minimumWidth: 50
            }
            CDDescLabel {
                text: "Total Collections"
            }
            Label {
                text: {
                    var returnMe =0;
                    for(var w=0;w<ledgerList.length;w++) {
                        var paymentArray = ledgerList[w]["Payments"];
                        for(var i=0;i<paymentArray.length;i++) {
                            var paymentItem = paymentArray[i];
                            if(paymentItem["Type"] !== "Credit") {
                                returnMe += parseFloat(paymentItem["Amount"]);
                            }
                        }
                    }
                    return "$"+returnMe.toFixed(2);
                }
            }
            Label {
                Layout.minimumWidth: 50
            }
            CDDescLabel {
                text: "Total Writeoffs"
            }
            Label {
                text: {
                    var returnMe =0;
                    for(var w=0;w<ledgerList.length;w++) {
                        var paymentArray = ledgerList[w]["Payments"];
                        for(var i=0;i<paymentArray.length;i++) {
                            var paymentItem = paymentArray[i];
                            if(paymentItem["Type"] === "Credit") {
                                returnMe += parseFloat(paymentItem["Amount"]);
                            }
                            else if(paymentItem["Type"] === "Dental Plan Payment") {
                                returnMe += parseFloat(paymentItem["WriteOff"]);
                            }
                        }
                    }
                    return "$"+returnMe.toFixed(2);
                }
            }

        }

    }

    Component.onCompleted: {
        refreshView();
    }

}
