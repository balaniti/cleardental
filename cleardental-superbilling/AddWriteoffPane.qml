// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTranslucentPane {
    id: creditPane

    backMaterialColor: Material.Yellow

    property alias getAmountGotten: amountCredit.text
    property alias getReason: reasonForCredit.text
    property bool isTx: false
    property real autoWriteOffAmount: 0

    ColumnLayout {
        GridLayout {
            columns: 2
            CheckBox {
                Layout.columnSpan: 2
                text: "Write Off Remaining Balance"
                visible: isTx
                onCheckedChanged: {
                    if(checked) {
                        amountCredit.text = autoWriteOffAmount
                    }
                    else {
                        amountCredit.text = ""
                    }
                    amountCredit.enabled = !checked;
                }
            }

            Label {
                text: "Amount to credit ($)"
                font.bold: true
            }
            TextField {
                id: amountCredit
                Layout.minimumWidth: 150
                validator: DoubleValidator {}
            }

            Label {
                text: "Reason Why"
                font.bold: true
            }
            TextField {
                id: reasonForCredit
                Layout.minimumWidth: 150
            }
        }
    }
}
