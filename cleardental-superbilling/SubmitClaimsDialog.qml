// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQuick.Controls.Material 2.15
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTranslucentDialog {
    id: submitClaimsDia

    function getSelectedClaims() {
        var returnMe = [];

        for(var i=0;i<claimsGrid.children.length;i++) {
            var childItem = claimsGrid.children[i];
            if("procedureObject" in childItem) {
                if(childItem.checked) {
                    returnMe.push(childItem["procedureObject"]);
                }
            }
        }
        return returnMe;
    }

    function markClaims(getNewStatus) {
        var procedureIndex =0;
        for(var i=0;i<claimsGrid.children.length;i++) {
            var childItem = claimsGrid.children[i];
            if("procedureObject" in childItem) {
                if(childItem.checked) {
                     rootWin.ledgerList[procedureIndex]["ClaimStatus"] = getNewStatus;
                }
                procedureIndex++;
            }
        }

    }

    function refreshClaims() {
        //first take out all the previous comps
        for(var i=0;i<claimsGrid.children.length;i++) {
            var childItem = claimsGrid.children[i];
            if("deleteMeLater" in childItem) {
                childItem.destroy();
            }
        }

        //now add in the new comps
        for(var p=0;p<rootWin.ledgerList.length;p++) {
            var procedureItem = rootWin.ledgerList[p];
            var unsent = (procedureItem["ClaimStatus"]  === "Unsent");
            checkComp.createObject(claimsGrid,{checked:unsent,procedureObject:procedureItem});
            labComp.createObject(claimsGrid,{text:comFuns.makeTxItemString(procedureItem["Procedure"])});

            if("ProcedureDate" in procedureItem) {
                var formattedDate = (new Date(procedureItem["ProcedureDate"])).toLocaleDateString();
                labComp.createObject(claimsGrid,{text:formattedDate});
            }
            else {
                labComp.createObject(claimsGrid,{text:"Not Completed"});
            }
            labComp.createObject(claimsGrid,{text:procedureItem["ClaimStatus"]});
            labComp.createObject(claimsGrid,{text: "$" + procedureItem["Procedure"]["BasePrice"]});
        }
    }

    ColumnLayout {
        CDTranslucentPane {
            Layout.minimumWidth: claimsGrid.width + 50
            Layout.minimumHeight: 800

            Flickable {
                anchors.fill: parent
                contentHeight: claimsGrid.height
                contentWidth:  claimsGrid.width
                clip: true
                ScrollBar.vertical: ScrollBar { }

                GridLayout {
                    id: claimsGrid
                    columns: 5
                    CDHeaderLabel {
                        text: "Submit Claims"
                        Layout.columnSpan: claimsGrid.columns
                    }

                    RowLayout {
                        Layout.columnSpan: claimsGrid.columns
                        CDDescLabel {
                            text: "Submit to"
                        }
                        ComboBox {
                            id: submitionType
                            model: ["ClearingHouse (EDS/EDI)", "Clearinghouse (DentalXChange)","Print Here",
                            "Mark as Sent"]
                            Layout.fillWidth: true
                        }
                    }

                    RowLayout {
                        visible: submitionType.currentIndex == 3
                        Layout.columnSpan: claimsGrid.columns
                        CDDescLabel {
                            text: "Marked sent method"
                        }
                        TextField {
                            id: sentMethodField
                            text: "Via Web Portal"
                        }

                    }

                    CDDescLabel {
                        text: "Submit"
                    }

                    CDDescLabel {
                        text: "Procedure Name"
                    }

                    CDDescLabel {
                        text: "Procedure Date"
                    }

                    CDDescLabel {
                        text: "Claim Status"
                    }

                    CDDescLabel {
                        text: "Base Price"
                    }


                    Component {
                        id: labComp
                        Label {
                            property bool deleteMeLater: true

                        }
                    }
                    Component {
                        id: checkComp
                        CheckBox {
                            property bool deleteMeLater: true
                            property var procedureObject: ({})
                        }
                    }
                }

            }


        }
        RowLayout {
            CDCancelButton {
                onClicked: {
                    submitClaimsDia.reject();
                }
            }
            Label {
                Layout.fillWidth: true
            }
            CDButton {
                icon.name: "document-export"
                text: "Send Claims"
                highlighted: true

                CDClearingHouseFunctions {
                    id: chFuns
                }

                CDPrinter {
                    id: claimPrinter
                }

                onClicked: {
                    if(submitionType.currentIndex == 0) { //EDS/EDI
                        chFuns.generateNEW_EDSEDI_JSON_string(PATIENT_FILE_NAME,getSelectedClaims())
                        markClaims("Sent (EDS/EDI)");
                    }
                    else if(submitionType.currentIndex == 1) { //DentalXChange
                        //TODO: Actually implement this!

                    }
                    else if(submitionType.currentIndex == 2) { //Print
                        claimPrinter.printClaimForm(PATIENT_FILE_NAME,getSelectedClaims());
                        markClaims("Printed");
                    }
                    else if(submitionType.currentIndex == 3) { //Mark as Sent
                        markClaims("Sent (" + sentMethodField.text + ")");

                    }

                    submitClaimsDia.accept();
                }
            }
        }
    }

    onVisibleChanged: {
        if(visible) {
            refreshClaims();
        }
    }

}
