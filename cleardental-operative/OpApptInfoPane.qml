// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1
import dental.clear 1.0

CDTranslucentPane {

    CDFileLocations {
        id: fLocs
    }

    Settings {
        id: pracPrefs
        fileName:fLocs.getLocalPracticePreferenceFile()
        category: "Style"
    }

    Settings {
        id: hardTissueSettings
        fileName: fLocs.getHardTissueChartFile(PATIENT_FILE_NAME)

    }

    ChangeSurfaceDialog {
        id: changeSurfaceDialog
    }

    function generateCaseNoteString() {
        var returnMe ="";

        var fullAttrString = hardTissueSettings.value(procedureObj["Tooth"],"");

        if(remCariesBox.checked) {
            returnMe +="Caries was removed.\n";
            fullAttrString = comFuns.remHardTissueAttr(fullAttrString,"Decay");
        }

        fullAttrString = comFuns.addHardTissueAttr(fullAttrString,"Composite: " + procedureObj["Surfaces"]);
        hardTissueSettings.setValue(procedureObj["Tooth"],fullAttrString);

        returnMe+= isoBox.editText + " was used for isolation."
        if(isoSizeBox.visible) {
            returnMe+= " Size: " + isoSizeBox.displayText + ".\n";
        }
        else {
            returnMe+= "\n";
        }

        if(matrixBox.editText != "None") {
            returnMe+= matrixBox.editText + " matrix was used.\n"

        }

        returnMe+= etchBox.displayText + " was used.\n"
        returnMe+= bondingBox.editText + " was used for bonding.\n"

        if(!noLinerCheck.checked) {
            returnMe+= linerBox.editText + " was used as a liner.\n"
        }
        returnMe+= "Tooth was restored using " + compositeBox.editText + " and light cured in layers.\n"
        if((procedureObj["Surfaces"].includes("O")) || procedureObj["Surfaces"].includes("L")) {
            //If there is no opposing tooth, you can just remove this line so whatever....
            returnMe += "Occlusion was adjusted.\n";
        }

        return returnMe;
    }

    ColumnLayout {

        RowLayout {
            CDHeaderLabel {
                text: "Operative Appointment"
            }
            CDButton {
                text: "Change surfaces"
                onClicked: {
                    changeSurfaceDialog.open();
                }
            }
        }

        GridLayout {
            columns: 2
            Layout.fillWidth: true
            columnSpacing: 20

            CheckBox{
                id: remCariesBox
                text: "Removed caries"
                checked: true
                Layout.columnSpan: 2
            }

            Label {
                text: "Isolation"
                font.bold: true
            }

            RowLayout {
                ComboBox {
                    id: isoBox
                    editable: true
                    Layout.fillWidth: true

                    Component.onCompleted: {
                        var defIso = pracPrefs.value("DefaultIsolation", "Rubber Dam" );
                        var startModel =  ["Rubber Dam", "Cotton", "Isolite", "Isodry","DryShield"];
                        if(startModel.indexOf(defIso) === -1) {
                            startModel.push(defIso);
                        }
                        model = startModel;
                        currentIndex = find(defIso)
                    }
                }
                RowLayout {
                    visible: (isoBox.currentIndex == 2) || (isoBox.currentIndex == 4)
                    Label {
                        Layout.minimumWidth: 12
                    }
                    CDDescLabel {
                        text: "Size"
                    }
                    ComboBox {
                        id: isoSizeBox
                        model: ["Pediatric", "Small", "Medium", "Large"]
                        currentIndex: 2
                    }
                }


            }



            Label {
                text: "Etch"
                font.bold: true
            }

            ComboBox {
                id: etchBox
                Layout.fillWidth: true
                model: ["Complete Etch", "Selective Etch", "No Etch"]
            }

            Label {
                text: "Bonding Agent"
                font.bold: true
            }

            ComboBox {
                id: bondingBox
                editable: true
                Layout.fillWidth: true

                Component.onCompleted: {
                    var defBond = pracPrefs.value("DefaultBondingAgent", "OptiBond" );
                    var startModel = ["OptiBond","All-Bond Universal","BeautiBond",
                                      "CLEARFIL SE PROTECT","Cosmedent Complete","G-aenial Bond",
                                      "Scotchbond™", "Safco Tru-Bond LC"]
                    if(startModel.indexOf(defBond) === -1) {
                        startModel.push(defBond);
                    }
                    model = startModel;
                    currentIndex = find(defBond)
                }
            }

            Label {
                text: "Matrix"
                font.bold: true
            }

            ComboBox {
                id: matrixBox
                editable: true
                Layout.fillWidth: true

                Component.onCompleted: {
                    var defMatrix = pracPrefs.value("DefaultMatrix", "Garrison/Composi-Tight" );
                    var startModel =  ["Garrison/Composi-Tight","Tofflemire","Mylar Strip","None"];
                    if(startModel.indexOf(defMatrix) === -1) {
                        startModel.push(defMatrix);
                    }
                    model = startModel;
                    currentIndex = find(defMatrix)
                }
            }

            Label {
                text: "Liner"
                font.bold: true
            }

            RowLayout {
                CheckBox {
                    id: noLinerCheck
                    text: "No liner used"
                    checked: true
                }

                ComboBox {
                    id: linerBox
                    opacity: noLinerCheck.checked ? 0:1
                    //visible: opacity > 0
                    Behavior on opacity {
                        PropertyAnimation {
                            duration: 300
                        }
                    }

                    editable: true
                    Layout.minimumWidth: 150

                    Component.onCompleted: {
                        var defLiner = pracPrefs.value("DefaultLiner", "Vitrebond™" );
                        var startModel = ["Vitrebond™","Durelon™","Lime-Lite™", "Fuji II LC"]
                        if(startModel.indexOf(defLiner) === -1) {
                            startModel.push(defLiner);
                        }
                        model = startModel;
                        currentIndex = find(defLiner)
                    }

                }
            }

            Label {
                text: "Composite"
                font.bold: true
            }
            ComboBox {
                id: compositeBox
                editable: true
                Layout.fillWidth: true

                Component.onCompleted: {
                    var defComposite = pracPrefs.value("DefaultComposite", "Filtek™ Supreme Ultra" );
                    var startModel = ["Filtek™ Supreme Ultra","Gradia","Esthet-X","Safco Illusion Plus"]
                    if(startModel.indexOf(defComposite) === -1) {
                        startModel.push(defComposite);
                    }
                    model = startModel;
                    currentIndex = find(defComposite)
                }
            }
        }
    }


}
