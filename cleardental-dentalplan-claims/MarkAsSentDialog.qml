// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Window 2.10


CDTranslucentDialog {
    id: markAsSentDialog

    property var markAsSentList: []

    CDCommonFunctions {
        id: m_comFuns;
    }

    CDFileLocations {
        id: m_locs
    }

    CDTextFileManager {
        id: m_tMan
    }

    CDGitManager {
        id: m_gitMan
    }

    Component {
        id: labelComp
        Label {
            id: lab
            property bool totalSus: true //The Imposter!!!!
        }
    }


    onMarkAsSentListChanged: {
        for(var k=0;k<claimGridLayout.children.length;k++) {
            var child = claimGridLayout.children[k];
            if("totalSus" in child) {
                child.destroy();
            }
        }


        for(var i=0;i<markAsSentList.length;i++) {
            labelComp.createObject(claimGridLayout,{"text":markAsSentList[i]["PatientName"]});
            labelComp.createObject(claimGridLayout,{"text": new Date(markAsSentList[i]["ProcedureDate"]).
                                       toLocaleDateString()});
            labelComp.createObject(claimGridLayout,{"text":
                                       m_comFuns.makeTxItemString(markAsSentList[i]["Procedure"])});
            labelComp.createObject(claimGridLayout,{"text":markAsSentList[i]["Status"]});
        }
    }

    function makeSent(getPatID,getIndex) {
        var claimsList = JSON.parse(m_tMan.readFile(m_locs.getClaimsFile(getPatID)));
        var makeMeSent = claimsList[getIndex]
        makeMeSent["Status"] = "Sent";
        makeMeSent["SentDate"] = (new Date()).toString();
        makeMeSent["SentTo"] = sentToField.text;
        m_tMan.saveFile(m_locs.getClaimsFile(getPatID),JSON.stringify(claimsList,null,"\t"));
        gitMan.commitData("Marked claim as sent for " + getPatID);
    }

    ColumnLayout {
        CDTranslucentPane {
            Layout.fillWidth: true
            backMaterialColor: Material.Pink
            ColumnLayout {
                CDHeaderLabel {
                    id: headLab
                    text:"Mark as Sent?"
                }

                Label {
                    text: "Are you sure you want to mark these as Sent?"
                }

                GridLayout {
                    id: claimGridLayout
                    columns: 4
                    CDDescLabel {
                        text: "Patient ID";
                    }
                    CDDescLabel {
                        text: "Procedure Date"
                    }

                    CDDescLabel {
                        text: "Procedure Information"
                    }
                    CDDescLabel {
                        text: "Current Status"
                    }

                }
                MenuSeparator {
                    Layout.fillWidth: true
                }

                RowLayout {
                    CDDescLabel {
                        text: "Sent via which method?"
                    }
                    TextField {
                        id: sentToField
                        text: "Portal"
                    }
                }
            }
        }

        RowLayout {
            CDCancelButton {
                text: "No, nevermind"
                onClicked: markAsSentDialog.reject();
            }

            Label {
                Layout.fillWidth: true
            }

            CDAddButton {
                text: "Yes, mark them as sent"
                icon.name:  "document-send"
                onClicked: {
                    for(var i=0;i<markAsSentList.length;i++) {
                        makeSent(markAsSentList[i]["PatientName"],markAsSentList[i]["ClaimIndex"])
                    }
                    markAsSentDialog.accept();
                }
            }
        }
    }



}
