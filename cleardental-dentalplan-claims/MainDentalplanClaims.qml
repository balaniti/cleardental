// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Window 2.10

CDAppWindow {
    id: rootWin
    title: qsTr("Dental Plan Claims")

    property int filterIndex: 0
    property alias typedInPatName: patNameBox.text
    property alias compressClaims: compressClaimsCheckBox.checked

    header: CDBlankToolBar {
        headerText: "Dental Plan Claims"

        Label {
            id: filterLabel
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter
            anchors.margins: 10
            font.bold: true
            text: "Filter"
            font.pointSize: 16
        }

        ComboBox {
            id: filterBox
            anchors.left: filterLabel.right
            anchors.verticalCenter: parent.verticalCenter
            anchors.margins: 20
            width: 250
            model: ["All Unfiled Claims from Today","All Claims from Today", "All Unfiled Claims",
                "All Claims from Patient","All claims ever","All Rejected Claims"]
            onCurrentIndexChanged: {
                rootWin.filterIndex = currentIndex;
                claimListView.forceRefresh();

            }
        }
        CDPatientComboBox {
            id: patNameBox
            anchors.left: filterBox.right
            anchors.verticalCenter: parent.verticalCenter
            anchors.margins: 20
            width: 250
            height: 64
            font.pointSize: 16
            opacity: filterBox.currentIndex === 3 ? 1:0
            enabled: opacity > 0
            Behavior on opacity {
                PropertyAnimation {
                    duration: 300
                }
            }

            onTextChanged: {
                claimListView.forceRefresh();
            }
        }

        CheckBox {
            id: compressClaimsCheckBox
            anchors.left: patNameBox.right
            anchors.verticalCenter: parent.verticalCenter
            anchors.margins: 20
            text: "Compress Claims"
            checked: true
            visible: false
        }
    }

    ClaimsListView {
        id: claimListView
        anchors.fill: parent
        anchors.leftMargin: 10
        anchors.rightMargin: 10
    }

    CDButton {
        text: "Select All"
        anchors.bottomMargin: 10
        anchors.rightMargin: 10
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        onClicked: claimListView.selectAllItems();

    }

    Component.onCompleted: {
        if(PATIENT_FILE_NAME.length > 0) {
            filterBox.currentIndex = 3;
            patNameBox.text = PATIENT_FILE_NAME;
        }
    }

}
