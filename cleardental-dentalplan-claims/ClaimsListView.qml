// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Window 2.10

ListView {
    id: claimListView
    property int rowWidth: (rootWin.width-100) / 10
    ScrollBar.vertical: ScrollBar { }

    property var claimsListModel: []
    property var selectedClaimsList: []
    property int selectedClaimsListCount :selectedClaimsList.length
    model: claimsListModel.length
    headerPositioning: ListView.OverlayHeader
    footerPositioning: ListView.OverlayFooter
    clip: true


    function forceRefresh() {
        model = [];
        claimsListModel = [];
        docListMan.makeDictionary();
        clearSelected();

        if(rootWin.filterIndex === 0) { //All unfiled claims from today
            var fullPatList = patMan.getAllPatients();
            for(var patI=0;patI<fullPatList.length;patI++) {
                var claimsJSON = textMan.readFile(fileLocs.getClaimsFile(fullPatList[patI]["PatID"]));
                if(claimsJSON.length > 0) {
                    var claimsOBJ = JSON.parse(claimsJSON);
                    for(var claimI=0;claimI<claimsOBJ.length;claimI++) {
                        var dateOfClaim = new Date(claimsOBJ[claimI]["ProcedureDate"]);
                        if(isToday(dateOfClaim) && (claimsOBJ[claimI]["Status"] === "Unsent")) {
                            claimsOBJ[claimI]["PatientName"] = fullPatList[patI]["PatID"];
                            claimsOBJ[claimI]["ClaimIndex"] = claimI;
                            if("NPI" in claimsOBJ[claimI]) {
                                claimsOBJ[claimI]["Provider"] = docListMan.npiDict[claimsOBJ[claimI]["NPI"]];
                            }
                            claimsListModel.push(claimsOBJ[claimI]);
                        }
                    }
                }
            }
        }
        else if(rootWin.filterIndex === 1) { //All claims from today
            fullPatList = patMan.getAllPatients();
            for(patI=0;patI<fullPatList.length;patI++) {
                claimsJSON = textMan.readFile(fileLocs.getClaimsFile(fullPatList[patI]["PatID"]));
                if(claimsJSON.length > 0) {
                    claimsOBJ = JSON.parse(claimsJSON);
                    for(claimI=0;claimI<claimsOBJ.length;claimI++) {
                        dateOfClaim = new Date(claimsOBJ[claimI]["ProcedureDate"]);
                        if(isToday(dateOfClaim)) {
                            claimsOBJ[claimI]["PatientName"] = fullPatList[patI]["PatID"];
                            claimsOBJ[claimI]["ClaimIndex"] = claimI;
                            if("NPI" in claimsOBJ[claimI]) {
                                claimsOBJ[claimI]["Provider"] = docListMan.npiDict[claimsOBJ[claimI]["NPI"]];
                            }
                            claimsListModel.push(claimsOBJ[claimI]);
                        }
                    }
                }
            }
        }
        else if(rootWin.filterIndex === 2) { //All unfiled claims
            fullPatList = patMan.getAllPatients();
            for(patI=0;patI<fullPatList.length;patI++) {
                claimsJSON = textMan.readFile(fileLocs.getClaimsFile(fullPatList[patI]["PatID"]));
                if(claimsJSON.length > 0) {
                    claimsOBJ = JSON.parse(claimsJSON);
                    for(claimI=0;claimI<claimsOBJ.length;claimI++) {
                        if(claimsOBJ[claimI]["Status"] === "Unsent") {
                            claimsOBJ[claimI]["PatientName"] = fullPatList[patI]["PatID"];
                            claimsOBJ[claimI]["ClaimIndex"] = claimI;
                            if("NPI" in claimsOBJ[claimI]) {
                                claimsOBJ[claimI]["Provider"] = docListMan.npiDict[claimsOBJ[claimI]["NPI"]];
                            }
                            claimsListModel.push(claimsOBJ[claimI]);
                        }
                    }
                }
            }

        }
        else if(rootWin.filterIndex === 3) { //All Claims from Patient
            dateOfClaim = "";
            claimsJSON = textMan.readFile(fileLocs.getClaimsFile(rootWin.typedInPatName));
            if(claimsJSON.length > 0) {
                claimsOBJ = JSON.parse(claimsJSON);
                for(claimI=0;claimI<claimsOBJ.length;claimI++) {
                    claimsOBJ[claimI]["PatientName"] = rootWin.typedInPatName;
                    claimsOBJ[claimI]["ClaimIndex"] = claimI;
                    if("NPI" in claimsOBJ[claimI]) {
                        claimsOBJ[claimI]["Provider"] = docListMan.npiDict[claimsOBJ[claimI]["NPI"]];
                    }
                    claimsListModel.push(claimsOBJ[claimI]);
                }
            }
        }
        else if(rootWin.filterIndex === 4) { //All claims ever
            fullPatList = patMan.getAllPatients();
            for(patI=0;patI<fullPatList.length;patI++) {
                claimsJSON = textMan.readFile(fileLocs.getClaimsFile(fullPatList[patI]["PatID"]));
                if(claimsJSON.length > 0) {
                    claimsOBJ = JSON.parse(claimsJSON);
                    for(claimI=0;claimI<claimsOBJ.length;claimI++) {
                        claimsOBJ[claimI]["PatientName"] = fullPatList[patI]["PatID"];
                        claimsOBJ[claimI]["ClaimIndex"] = claimI;
                        if("NPI" in claimsOBJ[claimI]) {
                            claimsOBJ[claimI]["Provider"] = docListMan.npiDict[claimsOBJ[claimI]["NPI"]];
                        }
                        claimsListModel.push(claimsOBJ[claimI]);

                    }
                }
            }
        }

        else if(rootWin.filterIndex === 5) { //All Rejected Claims
            fullPatList = patMan.getAllPatients();
            for(patI=0;patI<fullPatList.length;patI++) {
                claimsJSON = textMan.readFile(fileLocs.getClaimsFile(fullPatList[patI]["PatID"]));
                if(claimsJSON.length > 0) {
                    claimsOBJ = JSON.parse(claimsJSON);
                    for(claimI=0;claimI<claimsOBJ.length;claimI++) {
                        if(claimsOBJ[claimI]["Status"] === "Rejected") {
                            claimsOBJ[claimI]["PatientName"] = fullPatList[patI]["PatID"];
                            claimsOBJ[claimI]["ClaimIndex"] = claimI;
                            if("NPI" in claimsOBJ[claimI]) {
                                claimsOBJ[claimI]["Provider"] = docListMan.npiDict[claimsOBJ[claimI]["NPI"]];
                            }
                            claimsListModel.push(claimsOBJ[claimI]);
                        }
                    }
                }
            }
        }
        model = claimsListModel;
    }

    function selectAllItems() {
        selectedClaimsList = [];
        for(var i=0;i<claimsListModel.length;i++) {
            selectedClaimsList.push(i);
        }
        selectedClaimsListCount = selectedClaimsList.length;
        forceUpdateSelections();
    }

    function addSelected(getIndex) {
        selectedClaimsList.push(getIndex);
        selectedClaimsListCount = selectedClaimsList.length;
    }

    function removeSelected(getIndex) {
        var loc = indexOfSelectedItem(getIndex);
        if(loc > -1) {
            selectedClaimsList.splice(loc,1);
        }
        selectedClaimsListCount = selectedClaimsList.length;
    }

    function isSelectedItem(getIndex) {
        return indexOfSelectedItem(getIndex) > -1;
    }

    function indexOfSelectedItem(getIndex) {
        var returnMe = -1;
        for(var i=0;i<selectedClaimsList.length;i++) {
            if(selectedClaimsList[i] === getIndex) {
                return i;
            }
        }
        return returnMe;
    }

    function toggleDayPatGroup(getIndex) {
        var loc = indexOfSelectedItem(getIndex);
        if(loc > -1) {
            removeSelected(getIndex);
        } else {
            addSelected(getIndex);
        }
    }

    function clearSelected() {
        selectedClaimsList = [];
        selectedClaimsListCount = selectedClaimsList.length;
        forceUpdateSelections();
    }

    function selectUnsentForPatient(getPatID) {
        for(var i=0;i<claimsListModel.length;i++) {
            if(claimsListModel[i]["PatientName"] === getPatID) {
                if(!isSelectedItem(i)) {
                    if(claimsListModel[i]["Status"] === "Unsent") {
                        addSelected(i);
                    }
                }
            }
        }
        forceUpdateSelections();
    }

    function forceUpdateSelections() {
        for(var i=0;i< claimListView.contentItem.children.length;i++) {
            var child = claimListView.contentItem.children[i];
            if("isSelected" in child) {
                child.isSelected = claimListView.indexOfSelectedItem(child.modelIndex) > -1
            }
        }

    }

    function isToday(getDate) {
        var getDateObj =new Date(getDate);
        var today = new Date();

        return ( (getDateObj.getMonth() === today.getMonth()) &&
                (getDateObj.getDate() === today.getDate()) &&
                (getDateObj.getFullYear() === today.getFullYear()) );
    }

    function isSameDate(dateA,dateB) {
        var dateAObj = new Date(dateA);
        var dateBObj = new Date(dateB);

        return ( (dateAObj.getMonth() === dateBObj.getMonth()) &&
                (dateAObj.getDate() === dateBObj.getDate()) &&
                (dateAObj.getFullYear() === dateBObj.getFullYear()) );
    }

    function saveStatusChanges(indexToUpdate, newStatus) {
        var patName = claimsListModel[indexToUpdate]["PatientName"];
        var procedureDate = claimsListModel[indexToUpdate]["ProcedureDate"];
        var claimsJSON = textMan.readFile(fileLocs.getClaimsFile(patName));
        var claimsArray = JSON.parse(claimsJSON);

        for(var i=0;i<claimsArray.length;i++) {
            var claimLine = claimsArray[i];
            var claimLineDate = claimLine["ProcedureDate"];
            if(isSameDate(claimLineDate,procedureDate)) {
                claimsArray[i]["Status"] = newStatus;
            }
        }
        claimsJSON = JSON.stringify(claimsArray, null, '\t');
        textMan.saveFile(fileLocs.getClaimsFile(patName),claimsJSON);
        gitMan.commitData("Set the claims status for " + patName);
        forceRefresh();
    }

    function getProcedures(procedures) {
        var returnMe = "";
        returnMe += procedures[0]["ProcedureName"];
        for (var i = 1; i < procedures.length; i++) {
            returnMe += '\n' + procedures[i]["ProcedureName"];
        }
        return returnMe;
    }

    function getPrices(procedures) {
        var returnMe = "";
        returnMe += procedures[0]["BasePrice"];
        for (var i = 1; i < procedures.length; i++) {
            returnMe += '\n' + procedures[i]["BasePrice"];
        }
        return returnMe;
    }

    header: Pane {
        z: 1024
        background: Rectangle {
            color: Material.color(Material.LightBlue)
            anchors.fill: parent
            opacity: .75
        }


        RowLayout {
            Label {
                font.bold: true
                Layout.minimumWidth: claimListView.rowWidth
                text: "Patient Name"
            }
            Label {
                font.bold: true
                Layout.minimumWidth: claimListView.rowWidth
                text: "Procedure Date"
            }
            Label {
                font.bold: true
                Layout.minimumWidth: claimListView.rowWidth
                text: "Procedure"
            }
            Label {
                font.bold: true
                Layout.minimumWidth: claimListView.rowWidth
                text: "Provider"
            }
            Label {
                font.bold: true
                Layout.minimumWidth: claimListView.rowWidth
                text: "Claim Amount"
            }
            Label {
                font.bold: true
                Layout.minimumWidth: claimListView.rowWidth
                text: "Status"
            }
            Label {
                font.bold: true
                Layout.minimumWidth: claimListView.rowWidth
                text: "Sent To"
            }

            Label {
                font.bold: true
                Layout.minimumWidth: claimListView.rowWidth
                text: "Sent Date"
            }
            Label {
                font.bold: true
                Layout.minimumWidth: claimListView.rowWidth
                text: "Paid Amount (Writeoff)"
            }
            Label {
                font.bold: true
                Layout.minimumWidth: claimListView.rowWidth
            }
        }
    }

    footer: RowLayout {
        z:2
        opacity: claimListView.selectedClaimsListCount > 0 ? 1 : 0
        visible: opacity > 0

        Behavior on opacity {
            PropertyAnimation {
                duration: 300
            }
        }

        CDAddButton {
            icon.name: "document-print"
            text: "Print"

            PrintClaimDialog {
                id: printDia
                onAccepted: claimListView.forceRefresh();
            }

            onClicked: {
                var makeList = [];
                for(var i=0;i<selectedClaimsList.length;i++) {
                    makeList.push(claimsListModel[selectedClaimsList[i]]);
                }

                printDia.printList = makeList;
                printDia.open();
            }
        }
        CDAddButton {
            icon.name:  "document-send"
            text: "EClaim"

            ClearingHouseDialog {
                id: clearingHouseDia
                onAccepted: claimListView.forceRefresh();
            }

            onClicked: {
                var makeList = [];
                for(var i=0;i<selectedClaimsList.length;i++) {
                    makeList.push(claimsListModel[selectedClaimsList[i]]);
                }

                clearingHouseDia.sendClaimList = makeList;
                clearingHouseDia.open();
            }

        }
        CDButton {
            text: "Mark as Sent"
            onClicked: {
                var makeList = [];
                for(var i=0;i<selectedClaimsList.length;i++) {
                    makeList.push(claimsListModel[selectedClaimsList[i]]);
                }

                markAsSentDialog.markAsSentList = makeList;
                markAsSentDialog.open();
            }

            MarkAsSentDialog {
                id: markAsSentDialog
                onAccepted: claimListView.forceRefresh();
            }
        }

        CDButton {
            text: "Clear Selected"
            onClicked: {
                claimListView.clearSelected();
            }
        }


    }

    delegate:  Component {
        Pane {
            id: claimPane
            property bool isSelected: claimListView.indexOfSelectedItem(index) > -1
            property int modelIndex: index

            onIsSelectedChanged: {
                mouseOverClaim.updateBack();
            }

            background: Rectangle {
                id: backRect
                color: "blue"
                opacity: isSelected ? 0.50 : 0

                MouseArea {
                    id: mouseOverClaim

                    anchors.fill: parent

                    function updateBack() {
                        if(containsMouse) {
                            if(claimPane.isSelected) {
                                parent.opacity = 1
                            }
                            else {
                                parent.opacity = .1
                            }
                        }
                        else {
                            if(claimPane.isSelected) {
                                parent.opacity = .50

                            }
                            else {
                                parent.opacity = 0
                            }
                        }
                    }

                    onContainsMouseChanged: {
                       updateBack();
                    }
                    hoverEnabled: true

                    onClicked: {
                        if(claimPane.isSelected) {
                            claimListView.removeSelected(index);
                            claimPane.isSelected = false;
                            backRect.opacity = 0.25;
                        } else {
                            claimListView.addSelected(index);
                            claimPane.isSelected = true;
                            backRect.opacity = 1;
                        }
                    }

                    onDoubleClicked: {
                        selectUnsentForPatient(claimsListModel[index]["PatientName"]);
                    }
                }

                Behavior on opacity {
                    PropertyAnimation {
                        duration: 300
                    }
                }
            }
            RowLayout {
                ClaimRowLabel {
                    setText: claimsListModel[index]["PatientName"]
                }
                ClaimRowLabel {
                    setText: new Date(claimsListModel[index]["ProcedureDate"]).toLocaleDateString()
                }
                ClaimRowLabel {
                    setText: comFuns.makeTxItemString(claimsListModel[index]["Procedure"])
                }
                ClaimRowLabel {
                    setText: claimsListModel[index]["ProviderUnixID"]
                }
                ClaimRowLabel {
                    setText: claimsListModel[index]["Procedure"]["BasePrice"]
                }
                ClaimRowLabel {
                    setText: claimsListModel[index]["Status"]
                }
                ClaimRowLabel {
                    setText: claimsListModel[index]["SentTo"]
                }
                ClaimRowLabel {
                    setText:  new Date(claimsListModel[index]["SentDate"]).toLocaleDateString()
                }
                ClaimRowLabel {
                    setText: {
                        var returnMe = "";
                        if("PaidAmount" in claimsListModel[index]) {
                            returnMe +=  "$" + parseFloat(claimsListModel[index]["PaidAmount"]).toFixed(2);
                        }
                        if("WriteOff" in claimsListModel[index]) {
                            returnMe +=  " ($" + parseFloat(claimsListModel[index]["WriteOff"]).toFixed(2) + ")";
                        }
                    }
                }

                RowLayout {
                    Layout.minimumWidth: claimListView.rowWidth
                    CDButton {
                        icon.name: "document-edit"

                        EditClaimDialog {
                            id: editClaimDia
                            claimObj: claimsListModel[index]
                            onAccepted: claimListView.forceRefresh();
                        }
                        onClicked: {
                            editClaimDia.open();
                        }
                    }
                    CDDeleteButton {
                        DeleteClaimDialog {
                            id: deleteClaimDia
                            claimObj: claimsListModel[index]
                            onAccepted: claimListView.forceRefresh();
                        }

                        onClicked: {
                            deleteClaimDia.open();
                        }

                    }
                }
            }
        }
    }

    CDPatientManager {
        id: patMan
    }

    CDTextFileManager {
        id: textMan
    }

    CDPatientFileManager {
        id: patFileMan
    }

    CDFileLocations {
        id: fileLocs
    }

    Settings {
        id: docSettings
    }

    CDGitManager {
        id: gitMan
    }

    CDCommonFunctions {
        id: comFuns
    }

    CDDoctorListManager {
        id: docListMan
        property var npiDict: ({})

        function makeDictionary() {
            npiDict = ({});
            var doctFileList = docListMan.getListOfProviderFiles();
            for(var i=0;i<doctFileList.length;i++) {
                docSettings.fileName = doctFileList[i];
                var npiNumber = docSettings.value("NPINumb","");
                var unixID = docSettings.value("UnixID","");
                npiDict[npiNumber] = unixID;
            }
        }
    }

//    CDClearingHouseFunctions {
//        id: houseFuns


//    }

//    Component.onCompleted: {
//        houseFuns.getEDSEDIClaimInfo("");
//    }





}
