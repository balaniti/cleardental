// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDTranslucentPane {
    id: perioGradePane
    backMaterialColor: Material.Pink

    property string perioGrade: perioGradeGroup.checkedButton.text
    
    ColumnLayout {
        Label {
            text: "Grade"
            font.bold: true
            Layout.fillWidth: true
            horizontalAlignment: Text.AlignHCenter
        }
        RowLayout {
            id: genPerioGrade
            
            RadioButton {
                id: perioGradeA
                text: "Grade A"
                checked: true
                ToolTip.visible: pressed
                ToolTip.text: "No loss over 5 years\n" +
                              "Bone Loss / age < 0.25\n" +
                              "No Smoking\n" +
                              "Non-diabetic"
            }
            RadioButton {
                id: perioGradeB
                text: "Grade B"
                ToolTip.visible: pressed
                ToolTip.text: "<2mm bone loss over 5 years\n" +
                              "% Bone Loss / age between 0.25 and 1.0\n"+
                              "<10 cigaretts per day\n" +
                              "HbA1c < 7%"
            }
            RadioButton {
                id: perioGradeC
                text: "Grade C"
                ToolTip.visible: pressed
                ToolTip.text: ">=2mm bone loss over 5 years\n" +
                              "% Bone Loss / age > 1.0\n" +
                              ">=10 cigaretts per day\n" +
                              "HbA1c >= 7%"
            }
            ButtonGroup {
                id: perioGradeGroup
                buttons: [perioGradeA,perioGradeB,perioGradeC]
            }
        }
    }
}
