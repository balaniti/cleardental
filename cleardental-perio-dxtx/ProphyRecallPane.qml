// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDTranslucentPane {
    id: prophyRecallPane
    backMaterialColor: Material.Lime

    property alias recallValue: prophyRecallBox.value

    ColumnLayout {
        Label {
            text: "Prophy Recall"
            font.pointSize: 24
            font.underline: true
        }
        
        SpinBox {
            id: prophyRecallBox
            textFromValue: function(value,locale) {
                return value + " months";
            }
            from: 1
            to: 24
            value: 6
        }
    }
}
