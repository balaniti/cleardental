// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

Popup {
    id: toothInfo
    
    property int popNumb: 0
    property var buccalPockets:["_","_","_"]
    property var buccalRec:["_","_","_"]
    property var lingualPockets:["_","_","_"]
    property var lingualRec:["_","_","_"]
    property var creatorColor: Material.Green

    background: Rectangle {
        anchors.fill: parent
        color: "white"
        radius: 10
        opacity: .90
    }
    
    
    
    function runPop(getX,getY,toothNumb) {

        if(getX>300) {
            getX = 300
        }

        x = getX;
        y = getY;
        popNumb = toothNumb;

        perioChartSettings.category = toothNumb
        var fullBuccalPockets =perioChartSettings.value("BuccalPockets","_,_,_");
        if(fullBuccalPockets !== "WNL") {
            buccalPockets = fullBuccalPockets.split(",");
        }
        else {
            buccalPockets = ["😊","😊","😊"]
        }
        var fullBuccalRec =perioChartSettings.value("BuccalRecession","_,_,_");
        buccalRec = fullBuccalRec.split(",");


        var fullLingualPockets =perioChartSettings.value("LingualPockets","_,_,_");
        if(fullLingualPockets !== "WNL") {
            lingualPockets = fullLingualPockets.split(",");
        }
        else {
            lingualPockets = ["😊","😊","😊"]
        }
        var fullLingualRec =perioChartSettings.value("LingualRecession","_,_,_");
        lingualRec = fullLingualRec.split(",");
        
        open();
        fadeOut.start();
    }
    
    SequentialAnimation {
        id: fadeOut
        PauseAnimation {
            duration: 5000
        }
        ScriptAction {
            script: toothInfo.close()
        }
    }
    
    ColumnLayout {
        Label {
            Layout.fillWidth: true
            text: "Buccal"
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
        }
        
        GridLayout {
            columns: 4
            Label {
                text: "Pocket Depths"
            }
            
            Repeater {
                model: 3
                CDPerioLabel {
                    perioVal: toothInfo.buccalPockets[index]
                }
            }
            
            Label {
                text: "Recession"
            }
            
            Repeater {
                model: 3
                CDPerioLabel {
                    perioVal: toothInfo.buccalRec[index]
                }
            }
            
        }
        
        Label {
            text: toothInfo.popNumb
            font.pointSize: 32
            Layout.fillWidth: true
            horizontalAlignment: Text.AlignHCenter
            color: Material.color(creatorColor)
            style: Text.Outline
            styleColor: Material.foreground
        }
        
        GridLayout {
            columns: 4
            Label {
                text: "Pocket Depths"
            }
            
            Repeater {
                model: 3
                CDPerioLabel {
                    perioVal: toothInfo.lingualPockets[index]
                }
            }
            
            Label {
                text: "Recession"
            }
            
            Repeater {
                model: 3
                CDPerioLabel {
                    perioVal: toothInfo.lingualRec[index]
                }
            }
        }
        
        Label {
            Layout.fillWidth: true
            text: "Lingual"
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
        }
    }
    
    exit: Transition {
        NumberAnimation {
            property: "opacity";
            from: 1.0;
            to: 0.0;
            duration: 300
        }
    }
}
