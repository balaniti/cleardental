// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDTranslucentPane {
    id: perioStagePane
    backMaterialColor: Material.Red

    property string perioStage: perioStageGroup.checkedButton.text

    ColumnLayout {
        Label {
            text: "Stage"
            font.bold: true
            Layout.fillWidth: true
            horizontalAlignment: Text.AlignHCenter
        }
        RowLayout {
            id: genPerioStage
            
            RadioButton {
                id: perioStage1
                text: "Stage I"
                ToolTip.visible: pressed
                checked: true
                ToolTip.text: "1 – 2 mm CAL\n" +
                              "<=4mm probing depth\n" +
                              "<15% Bone loss (mostly horizontal)\n" +
                              "No Tooth Loss"
            }
            RadioButton {
                id: perioStage2
                text: "Stage II"
                ToolTip.visible: pressed
                ToolTip.text: "3 – 4 mm CAL\n" +
                              "<=5mm probing depth\n" +
                              "15-33% Bone loss (mostly horizontal)\n" +
                              "No Tooth Loss"
            }
            RadioButton {
                id: perioStage3
                text: "Stage III"
                ToolTip.visible: pressed
                ToolTip.text: ">=5 mm CAL\n" +
                              ">6mm probing depth\n" +
                              ">33% Bone loss\n" +
                              ">=3mm Vertical Bone loss\n" +
                              "<=4 teeth lost"
            }
            RadioButton {
                id: perioStage4
                text: "Stage IV"
                ToolTip.visible: pressed
                ToolTip.text: ">=5 mm CAL\n" +
                              ">6mm probing depth\n" +
                              ">33% Bone loss\n" +
                              ">=3mm Vertical Bone loss\n" +
                              "Complex Rehab needed\n" +
                              ">5 teeth lost"
            }
            
            ButtonGroup {
                id: perioStageGroup
                buttons: [perioStage1,perioStage2,perioStage3,
                    perioStage4]
            }
        }
    }
}
