// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDTranslucentPane {
    id: gingDxPane
    backMaterialColor: Material.Purple
    property string gingDx: gingButtGroup.checkedButton.text

    ButtonGroup {
        id: gingButtGroup
        buttons: [genSlightGing,genModerateGing,genSevereGing]
    }

    ColumnLayout {
        Label {
            text: "Severity"
            font.bold: true
            Layout.fillWidth: true
            horizontalAlignment: Text.AlignHCenter
        }
        RowLayout {
            RadioButton {
                id: genSlightGing
                text: "Slight"
            }
            RadioButton {
                id: genModerateGing
                text: "Moderate"
                checked: true
            }
            RadioButton {
                id: genSevereGing
                text: "Severe"
            }
        }
    }
}
