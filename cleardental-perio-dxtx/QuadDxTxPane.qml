// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDTranslucentPane {
    id: quadDxTxPane
    backMaterialColor: Material.Green

    property string quadName: "ll"

    property bool enableTreatment: true

    function generateTxPlanObj() {
        var returnMe = ({});
        if(!enableTreatment || noTx.checked) {
            return returnMe;
        }

        if(smallQuad.checked) {
            returnMe["Location"] = quadName.toUpperCase();
            returnMe["ProcedureName"] = "SCRP (<3 Teeth)";
            returnMe["DCode"] = "D4342";
            returnMe["BasePrice"] = pricesSettings.value("D4342",0);
        }
        else if(fullQuad.checked) {
            returnMe["Location"] = quadName.toUpperCase();
            returnMe["ProcedureName"] = "SCRP (4+ Teeth)";
            returnMe["DCode"] = "D4341";
            returnMe["BasePrice"] = pricesSettings.value("D4341",0);
        }

        return returnMe;
    }

    function getStartToothNumber() {
        if(quadName == "ur") {
            return 1;
        }
        else if(quadName == "ul") {
            return 9;
        }
        else if(quadName == "ll") {
            return 17;
        }
        else if(quadName == "lr") {
            return 25;
        }
    }

    function isMissing(toothNumb) {
        if((toothNumb < 1) || (toothNumb > 32)) {
            return true;
        }
        var returnMe = false;
        var info = missingTeethSettings.value(toothNumb,"");
        var parts = info.split("|");
        for(var i=0;i<parts.length;i++) {
            if(parts[i].trim().startsWith("Missing")) {
                return true;
            }
        }

        return returnMe;
    }

    function combineVals(a,b) {
        if(a === "_") {
            a = 0;
        } else {
            a = parseInt(a);
        }
        if(b==="_") {
            b=0;
        } else {
            b = parseInt(b);
        }

        return a+b;
    }

    function getColor(toothNumb) {
        var biggestNumb = 1;

        perioChartSettings.category = toothNumb;
        var buccalPockets = perioChartSettings.value("BuccalPockets","_,_,_");
        if(buccalPockets !== "WNL") {
            var buccalPocketArray = buccalPockets.split(",");
            var buccalRec = perioChartSettings.value("BuccalRecession","_,_,_");
            var buccalRecArray = buccalRec.split(",");
            for(var b=0;b<buccalRecArray.length;b++) {
                var sum = combineVals(buccalPocketArray[b],buccalRecArray[b]);
                biggestNumb = Math.max(biggestNumb,sum);
            }
        }

        var lingualPockets = perioChartSettings.value("LingualPockets","_,_,_");
        if(lingualPockets !== "WNL") {
            var lingualPocketArray = lingualPockets.split(",");
            var lingualRec = perioChartSettings.value("LingualRecession","_,_,_");
            var lingualRecArray = lingualRec.split(",");
            for(var l=0;l<lingualPocketArray.length;l++) {
                sum = combineVals(lingualPocketArray[l],lingualRecArray[l]);
                biggestNumb = Math.max(biggestNumb,sum);
            }
        }

        if(biggestNumb <= 3) {
            return Material.Green;
        }
        else if(biggestNumb <= 6) {
            return Material.Orange;
        }
        else {
            return Material.Red;
        }
    }


    Settings {
        id: missingTeethSettings
        fileName: fLocs.getHardTissueChartFile(PATIENT_FILE_NAME)
    }


    ColumnLayout {
        anchors.fill: parent
        Label {
            Label {
                function abvToFullQuadName() {
                    if(quadName == "ul") {
                        return "Upper Left"
                    }
                    else if(quadName == "ur") {
                        return "Upper Right"
                    }
                    else if(quadName == "ll") {
                        return "Lower Left"
                    }
                    else if(quadName == "lr") {
                        return "Lower Right"
                    }
                }
                text: abvToFullQuadName();
                font.pointSize: 24
                font.underline: true
            }
        }

        Flickable {
            contentHeight: 320
            contentWidth:  (320*radioMan.radioFileLocs.length) +10
            clip: true
            Layout.fillWidth: true
            Layout.minimumHeight: 320
            CDRadiographManager {
                id: radioMan
                property var radioFileLocs: []
                Component.onCompleted: {
                    if(quadName == "ur") {
                        radioFileLocs = radioMan.getRadiographsForQuad(PATIENT_FILE_NAME,
                                                                   CDRadiographManager.QUAD_UPPER_RIGHT);
                    }
                    else if(quadName == "ul") {
                        radioFileLocs = radioMan.getRadiographsForQuad(PATIENT_FILE_NAME,
                                                                   CDRadiographManager.QUAD_UPPER_LEFT);
                    }
                    else if(quadName == "ll") {
                        radioFileLocs = radioMan.getRadiographsForQuad(PATIENT_FILE_NAME,
                                                                   CDRadiographManager.QUAD_LOWER_LEFT);
                    }
                    else if(quadName == "lr") {
                        radioFileLocs = radioMan.getRadiographsForQuad(PATIENT_FILE_NAME,
                                                                   CDRadiographManager.QUAD_LOWER_RIGHT);
                    }
                }
            }

            CDFileLocations {
                id: radioLocs
            }

            RowLayout {
                Repeater {
                    model: radioMan.radioFileLocs.length
                    Image {
                        source: "file://" + radioMan.radioFileLocs[index]
                        Layout.maximumWidth: 320
                        Layout.maximumHeight: 320
                        fillMode: Image.PreserveAspectCrop
                        mipmap: true

                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                radioDia.setSource = parent.source
                                radioDia.open()
                            }
                        }

                        Label {
                            id: radioLabel
                            anchors.centerIn: parent
                            style: Text.Outline
                            styleColor: Material.background
                            //font.pointSize: 24
                            Component.onCompleted: {
                                var scr = radioMan.radioFileLocs[index]
                                var filename = scr.substr(scr.lastIndexOf("/") + 1);
                                var fileSee = filename.substr(0,filename.lastIndexOf("."));
                                var fileUser = fileSee.replace(/_/g, " ");

                                var radioDir = radioLocs.getRadiographDir(PATIENT_FILE_NAME);
                                var dateLong = scr.replace(radioDir,"");
                                var dater = dateLong.replace( "/"  + filename,"");

                                text = fileUser + "\n" + dater;
                            }
                        }

                        Dialog {
                            id: radioDia
                            parent: Overlay.overlay
                            width: rootWin.width
                            height: rootWin.height
                            property var setSource: ""

                            background: Rectangle {
                                anchors.fill: parent
                                color: "black"
                                opacity: .8
                            }

                            Image {
                                source: radioDia.setSource
                                anchors.fill: parent
                                fillMode: Image.PreserveAspectFit
                                Label {
                                    id: headerLabelForRadio
                                    text: radioLabel.text
                                    anchors.bottom: parent.bottom
                                    anchors.right: parent.right
                                    anchors.margins: 10
                                    color: Material.background
                                    style: Text.Outline
                                    styleColor: Material.foreground
                                    font.pointSize: 32
                                    onVisibleChanged: {
                                        if(visible) {
                                            fadeOutLabel.start();
                                        }
                                        else {
                                            opacity = 1;
                                        }
                                    }

                                    PropertyAnimation  {
                                        id: fadeOutLabel
                                        target: headerLabelForRadio
                                        property: "opacity"
                                        from: 1
                                        to: 0
                                        duration: 4000
                                    }
                                }

                                MouseArea {
                                    anchors.fill: parent
                                    onClicked: {
                                        radioDia.close();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        RowLayout {
            Layout.alignment: Qt.AlignHCenter

            Repeater {
                model: 8

                Button {
                    text: index +getStartToothNumber()
                    enabled: !isMissing(index +getStartToothNumber())
                    Layout.maximumWidth: 50
                    Material.accent: getColor(index +getStartToothNumber())
                    highlighted: true
                    onClicked: {
                        toothInfo.creatorColor =getColor(index +getStartToothNumber())
                        toothInfo.runPop(x,y,index+getStartToothNumber());
                    }

                    Label {
                        visible: parent.enabled
                        anchors.top: parent.top
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: ""
                        Component.onCompleted: {
                            var toothNumb = index +getStartToothNumber();
                            if(!rootWin.loadedPastValues) {
                                rootWin.loadPastValues();
                            }
                            if(Object.keys(rootWin.allPerioDepths).length < 2) {
                                visible = false;
                            }
                            else {
                                for(var w=0;w<3;w++) {
                                    var cals = [];
                                    for(var i=0;i<rootWin.sortedDates.length;i++) {
                                        var pocket = allPerioDepths[sortedDates[i].str][toothNumb]
                                                ["Buccal"]["Pockets"][w];
                                        var recession = allPerioDepths[sortedDates[i].str][toothNumb]
                                                ["Buccal"]["Recession"][w];
                                        cals[i] = combineVals(pocket,recession);
                                    }
                                    var diff= 0;
                                    for(i=0;i<cals.length-2;i++) {
                                        diff+= (cals[i] - cals[i+2]);
                                    }
                                    if(diff < 0) { //we have lowered the CAL over time
                                        text += "😊"
                                    }
                                    else if(diff > 0) { //the CAL is become bigger over time
                                        text += "🙁"
                                    }
                                    else { //stayed the same
                                        text += "😐"

                                    }
                                }
                            }
                        }
                    }

                    Label {
                        visible: parent.enabled
                        anchors.bottom: parent.bottom
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: ""
                        Component.onCompleted: {
                            var toothNumb = index +getStartToothNumber();
                            if(!rootWin.loadedPastValues) {
                                rootWin.loadPastValues();
                            }
                            if(Object.keys(rootWin.allPerioDepths).length < 2) {
                                visible = false;
                            }
                            else {
                                for(var w=0;w<3;w++) {
                                    var cals = [];
                                    for(var i=0;i<rootWin.sortedDates.length;i++) {
                                        var pocket = allPerioDepths[sortedDates[i].str][toothNumb]
                                                ["Lingual"]["Pockets"][w];
                                        var recession = allPerioDepths[sortedDates[i].str][toothNumb]
                                                ["Lingual"]["Recession"][w];
                                        cals[i] = combineVals(pocket,recession);
                                    }
                                    var diff= 0;
                                    for(i=0;i<cals.length-2;i++) {
                                        diff-= (cals[i] - cals[i+2]);
                                    }
                                    if(diff < 0) { //we have lowered the CAL over time
                                        text += "😊"
                                    }
                                    else if(diff > 0) { //the CAL is become bigger over time
                                        text += "🙁"
                                    }
                                    else { //stayed the same
                                        text += "😐"
                                    }
                                }
                            }
                        }
                    }
                }
            }

            ToothInfoPopup {
                id: toothInfo
            }
        }

        Label {
            enabled: enableTreatment
            text: "Localized Treatment:"
            font.bold: true
        }
        RowLayout {
            enabled: enableTreatment
            RadioButton {
                id: noTx
                text: "None"
                checked: true
            }
            RadioButton {
                id: smallQuad
                text: "Quad SCRP (<3 Teeth)"
            }
            RadioButton {
                id: fullQuad
                text: "Quad SCRP (4+ Teeth)"
            }
        }
    }



}
