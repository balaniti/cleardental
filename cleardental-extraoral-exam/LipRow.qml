// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.2
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Window 2.10

RowLayout {
    id: lipRow

    function loadPrevData(prevString) {
        if((prevString.length < 2) || (prevString === "WNL")) {
            return;
        }

        lipsWNL.checked = false;
        coldSore.checked =prevString.includes(coldSore.text);
        macule.checked =prevString.includes(macule.text);
        mucocele.checked =prevString.includes(mucocele.text);
        otherLips.checked =prevString.includes(otherLips.text);
        if(otherLips.checked) {
            lipDetails.text = prevString.substring(prevString.indexOf("(")+1,prevString.indexOf(")"));
        }
    }
    
    function generateSaveString() {
        var returnMe = "";
        if(lipsWNL.checked) {
            returnMe = "WNL";
        }
        else {
           if(coldSore.checked) {
               returnMe = coldSore.text + " Noted; "
           }
           if(macule.checked) {
               returnMe += macule.text + " Noted; "
           }
           if(mucocele.checked) {
               returnMe += mucocele.text + " Noted; "
           }
           if(otherLips.checked) {
               returnMe += otherLips.text + " Noted; "
           }
           if(lipDetails.text.length > 1) {
               returnMe += "(" + lipDetails.text + ")"
           }
        }
        
        return returnMe;
    }
    
    CheckBox {
        id: lipsWNL
        text: "WNL"
        checked: true
        onClicked: {
            if(checked) {
                coldSore.checked = false;
                macule.checked = false;
                mucocele.checked = false;
                otherLips.checked = false;
            }
        }
    }
    
    CheckBox {
        id: coldSore
        text: "Cold Sore"
        onClicked: {
            if(checked) {
                lipsWNL.checked = false;
            }
        }
    }
    
    CheckBox {
        id: macule
        text: "Macule"
        onClicked: {
            if(checked) {
                lipsWNL.checked = false;
            }
        }
    }
    
    CheckBox {
        id: mucocele
        text: "Mucocele noted"
        onClicked: {
            if(checked) {
                lipsWNL.checked = false;
            }
        }
    }
    
    CheckBox {
        id: otherLips
        text: "Other Lip Lesion"
        onClicked: {
            if(checked) {
                lipsWNL.checked = false;
            }
        }
    }

    RowLayout {
        id: details
        opacity: lipsWNL.checked ? 0 : 1
        visible: opacity > 0
        Behavior on opacity {
            NumberAnimation {
                duration: 300
            }
        }

        CDDescLabel {
            text: "Details"
        }
        TextField {
            id: lipDetails
            Layout.minimumWidth: 240
        }
    }
    
}
