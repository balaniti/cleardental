// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2

Row {

    function loadPrevData(prevString) {
        if((prevString.length < 2) || (prevString === "WNL")) {
            return;
        }
        if(prevString.startsWith(maxPain.text)) {
            maxPain.checked = true;
            var side = prevString.split(":")[1].trim();
            maxPainLeft.checked = side.includes("Left");
            maxPainRight.checked = side.includes("Right");
        }
        else {
            maxOther.checked = true;
            maxOtherInput.text = prevString;
        }
    }


    function generateSaveString() {
        var returnMe = "";
        if(maxWNL.checked) {
            returnMe = "WNL";
        }
        else if(maxPain.checked) {
            returnMe = "Pain on Palpation";
            var left = maxPainLeft.checked;
            var right = maxPainRight.checked;
            if(left && right) {
                returnMe+= ": Left and Right Sides";
            }
            else if(left) {
                returnMe+= ": Left Side";
            }
            else if(right) {
                returnMe+= ": Right Side";
            }
        }
        else if(maxOther.checked) {
            returnMe = maxOtherInput.text;
        }

        return returnMe;
    }

    RadioButton {
        id: maxWNL
        checked: true
        text: "WNL"
    }
    RadioButton {
        id: maxPain
        text: "Pain on Palpation";
    }

    CheckBox {
        id: maxPainLeft
        opacity: maxPain.checked ? 1: 0
        visible: opacity > 0
        text: "Left"
        Behavior on opacity {
            PropertyAnimation {
                duration: 400
            }
        }
    }

    CheckBox {
        id: maxPainRight
        opacity: maxPain.checked ? 1: 0
        visible: opacity > 0
        text: "Right"
        Behavior on opacity {
            PropertyAnimation {
                duration: 400
            }
        }
    }

    RadioButton {
        id: maxOther
        text: "Other"
        Behavior on opacity {
            PropertyAnimation {
                duration: 400
            }
        }
    }

    TextField {
        id: maxOtherInput
        opacity: maxOther.checked ? 1 :0
        Behavior on opacity {
            PropertyAnimation {
                duration: 400
            }
        }
    }

    move: Transition {
        NumberAnimation {
            properties: "x"
            duration: 200
        }
    }
}
