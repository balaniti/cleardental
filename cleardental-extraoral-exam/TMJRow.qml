// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2

Row {

    function loadPrevData(prevString) {
        if((prevString.length < 2) || (prevString === "WNL")) {
            return;
        }
        if(prevString.startsWith(tmjPain.text)) {
            tmjPain.checked = true;
            var side = prevString.split(":")[1].trim();
            tmjPainLeft.checked = side.includes("Left");
            tmjPainRight.checked = side.includes("Right");
        }
        else if(prevString.startsWith(tmjClick.text)) {
            tmjClick.checked = true;
            var tmjAttrs = prevString.split(":")[1].trim();
            var checks = [tmjClickOpen,tmjClickClose,tmjClickLeft,tmjClickRight];
            for(var i=0;i<4;i++) {
                if(tmjAttrs.includes(checks[i].text)) {
                    checks[i].checked = true;
                }
            }
        }
        else if(prevString.startsWith(tmjCrepitus.text)) {
            tmjCrepitus.checked = true;
            tmjAttrs = prevString.split(":")[1].trim();
            var checkscrep = [tmjCrepitusOpen,tmjCrepitusClose,tmjCrepitusLeft,tmjCrepitusRight];
            for(var w=0;w<4;w++) {
                if(tmjAttrs.includes(checkscrep[w].text)) {
                    checkscrep[w].checked = true;
                }
            }
        }

        else {
            tmjOther.checked = true;
            tmjOtherInput.text = prevString;
        }
    }

    function generateSaveString() {
        var returnMe = "";
        if(tmjWNL.checked) {
            returnMe = "WNL";
        }
        else if(tmjPain.checked) {
            returnMe = "Pain on Palpation";
            var left = tmjPainLeft.checked;
            var right = tmjPainRight.checked;
            if(left && right) {
                returnMe+= ": Left and Right Sides";
            }
            else if(left) {
                returnMe+= ": Left Side";
            }
            else if(right) {
                returnMe+= ": Right Side";
            }
        }

        else if(tmjClick.checked) {
            returnMe = "Clicking";
            var checks = [tmjClickOpen,tmjClickClose,tmjClickLeft,
                          tmjClickRight];
            var adders = "";
            for(var i=0;i<4;i++) {
                if(checks[i].checked) {
                    adders += checks[i].text + " ";
                }
            }
            if(adders.length > 0) {
                returnMe += ": " + adders;
                returnMe = returnMe.substring(0, returnMe.length - 1);
            }
        }

        else if(tmjCrepitus.checked) {
            returnMe = "Crepitus";
            var checkscrep = [tmjCrepitusOpen,tmjCrepitusClose,tmjCrepitusLeft,
                          tmjCrepitusRight];
            var adderscrep = "";
            for(var w=0;w<4;w++) {
                if(checkscrep[w].checked) {
                    adderscrep += checkscrep[w].text + " ";
                }
            }
            if(adderscrep.length > 0) {
                returnMe += ": " + adderscrep;
                returnMe = returnMe.substring(0, returnMe.length - 1);
            }
        }

        else if(tmjOther.checked) {
            returnMe = tmjOtherInput.text;
        }

        return returnMe;
    }

    RadioButton {
        id: tmjWNL
        checked: true
        text: "WNL"
    }
    RadioButton {
        id: tmjPain
        text: "Pain on Palpation";
    }

    CheckBox {
        id: tmjPainLeft
        opacity: tmjPain.checked ? 1: 0
        visible: opacity > 0
        text: "Left"
        Behavior on opacity {
            PropertyAnimation {
                duration: 400
            }
        }
    }

    CheckBox {
        id: tmjPainRight
        opacity: tmjPain.checked ? 1: 0
        visible: opacity > 0
        text: "Right"
        Behavior on opacity {
            PropertyAnimation {
                duration: 400
            }
        }
    }

    RadioButton {
        id: tmjClick
        text: "Click";
    }

    CheckBox {
        id: tmjClickOpen
        opacity: tmjClick.checked ? 1: 0
        visible: opacity > 0
        text: "On Opening"
        Behavior on opacity {
            PropertyAnimation {
                duration: 400
            }
        }
    }

    CheckBox {
        id: tmjClickClose
        opacity: tmjClick.checked ? 1: 0
        visible: opacity > 0
        text: "On Closing"
        Behavior on opacity {
            PropertyAnimation {
                duration: 400
            }
        }
    }

    CheckBox {
        id: tmjClickLeft
        opacity: tmjClick.checked ? 1: 0
        visible: opacity > 0
        text: "on the Left"
        Behavior on opacity {
            PropertyAnimation {
                duration: 400
            }
        }
    }

    CheckBox {
        id: tmjClickRight
        opacity: tmjClick.checked ? 1: 0
        visible: opacity > 0
        text: "on the Right"
        Behavior on opacity {
            PropertyAnimation {
                duration: 400
            }
        }
    }

    RadioButton {
        id: tmjCrepitus
        text: "Crepitus";
    }

    CheckBox {
        id: tmjCrepitusOpen
        opacity: tmjCrepitus.checked ? 1: 0
        visible: opacity > 0
        text: "On Opening"
        Behavior on opacity {
            PropertyAnimation {
                duration: 400
            }
        }
    }

    CheckBox {
        id: tmjCrepitusClose
        opacity: tmjCrepitus.checked ? 1: 0
        visible: opacity > 0
        text: "On Closing"
        Behavior on opacity {
            PropertyAnimation {
                duration: 400
            }
        }
    }

    CheckBox {
        id: tmjCrepitusLeft
        opacity: tmjCrepitus.checked ? 1: 0
        visible: opacity > 0
        text: "Left"
        Behavior on opacity {
            PropertyAnimation {
                duration: 400
            }
        }
    }

    CheckBox {
        id: tmjCrepitusRight
        opacity: tmjCrepitus.checked ? 1: 0
        visible: opacity > 0
        text: "Right"
        Behavior on opacity {
            PropertyAnimation {
                duration: 400
            }
        }
    }

    RadioButton {
        id: tmjOther
        text: "Other"
        Behavior on opacity {
            PropertyAnimation {
                duration: 400
            }
        }
    }

    TextField {
        id: tmjOtherInput
        opacity: tmjOther.checked ? 1 :0
        Behavior on opacity {
            PropertyAnimation {
                duration: 400
            }
        }
    }

    move: Transition {
        NumberAnimation {
            properties: "x"
            duration: 200
        }
    }
}
