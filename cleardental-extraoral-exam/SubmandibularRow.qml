// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2

Row {

    function loadPrevData(prevString) {
        if((prevString.length < 2) || (prevString === "WNL")) {
            return;
        }
        if(prevString.startsWith(submanPain.text)) {
            submanPain.checked = true;
            var side = prevString.split(":")[1].trim();
            submanPainLeft.checked = side.includes("Left");
            submanPainRight.checked = side.includes("Right");
        }
        else {
            submanOther.checked = true;
            submanOtherInput.text = prevString;
        }
    }

    function generateSaveString() {
        var returnMe = "";
        if(submanWNL.checked) {
            returnMe = "WNL";
        }
        else if(submanPain.checked) {
            returnMe = "Pain on Palpation";
            var left = submanPainLeft.checked;
            var right = submanPainRight.checked;
            if(left && right) {
                returnMe+= ": Left and Right Sides";
            }
            else if(left) {
                returnMe+= ": Left Side";
            }
            else if(right) {
                returnMe+= ": Right Side";
            }
        }
        else if(submanOther.checked) {
            returnMe = submanOtherInput.text;
        }

        return returnMe;
    }


    RadioButton {
        id: submanWNL
        checked: true
        text: "WNL"
    }

    RadioButton {
        id: submanPain
        text: "Pain on Palpation";
    }

    CheckBox {
        id: submanPainLeft
        opacity: submanPain.checked ? 1: 0
        visible: opacity > 0
        text: "Left"
        Behavior on opacity {
            PropertyAnimation {
                duration: 400
            }
        }
    }

    CheckBox {
        id: submanPainRight
        opacity: submanPain.checked ? 1: 0
        visible: opacity > 0
        text: "Right"
        Behavior on opacity {
            PropertyAnimation {
                duration: 400
            }
        }
    }

    RadioButton {
        id: submanOther
        text: "Other"
        Behavior on opacity {
            PropertyAnimation {
                duration: 400
            }
        }
    }

    TextField {
        id: submanOtherInput
        opacity: submanOther.checked ? 1 :0
        Behavior on opacity {
            PropertyAnimation {
                duration: 400
            }
        }
    }

    move: Transition {
        NumberAnimation {
            properties: "x"
            duration: 200
        }
    }
}
