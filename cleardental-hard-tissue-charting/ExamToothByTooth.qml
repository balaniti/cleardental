// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtGraphicalEffects 1.11
import QtQuick.Layouts 1.3
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTransparentPage {
    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 20

        CDTranslucentPane {
            Layout.alignment: Qt.AlignHCenter
            backMaterialColor: Material.Blue
            CDHeaderLabel {
                text: "Current Tooth: " + rootWin.currentTooth
            }
        }

        RowLayout {
            ColumnLayout {
                ToothDataPane {
                    id: dataPane
                }
                CDReviewRadiographPane {
                    id: radioPane
                    toothToReview: rootWin.currentTooth
                    Layout.fillWidth: true
                }
            }

            DiagnosisPane {
                id: dxPane
                Layout.fillWidth: true
            }
            TxPlanPane {
                id: txPane
                Layout.fillWidth: true
            }
        }
    }



    Button {
        text: rootWin.currentTooth === 32 ? "Finish Exam" : "Save + Next Tooth"
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        Material.accent: rootWin.currentTooth === 32 ?  Material.Cyan : Material.LightBlue
        icon.name: "document-save"

        highlighted: true
        icon.width: 32
        icon.height: 32
        font.pointSize: 18

        CDFileLocations {
            id: fileLocs
        }

        Settings {
            id: hardTissueFile
            fileName: fileLocs.getHardTissueChartFile(PATIENT_FILE_NAME)
        }

        Settings {
            id: reviewsSaver
            fileName: fileLocs.getReviewsFile(PATIENT_FILE_NAME);
        }

        CDGitManager {
            id: gitMan
        }

        onClicked:  {
            if(rootWin.currentTooth === 32) {
                commonFuns.updateReviewFile("HardTissueExam",PATIENT_FILE_NAME);
                gitMan.commitData("Updated Hard Tissue Exam for " + PATIENT_FILE_NAME);
                Qt.quit();
            }
            else {
                var dx = dxPane.generateDxString();
                hardTissueFile.setValue(rootWin.currentTooth, dx);
                txPane.save();

                dxPane.clearInputs();
                txPane.clearValues();

                rootWin.currentTooth++;

                dataPane.updateValues();
                dxPane.parseDxString(hardTissueFile.value(currentTooth,""));
                txPane.loadTooth();
            }
        }

        Component.onCompleted: {
            dataPane.updateValues();
            dxPane.parseDxString(hardTissueFile.value(currentTooth,""));
        }
    }

    Button {
        text: "Save + Prev Tooth"
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: 10
        Material.accent: Material.Pink
        icon.name: "document-save"
        enabled: rootWin.currentTooth !== 1

        highlighted: true
        icon.width: 32
        icon.height: 32
        font.pointSize: 18

        onClicked:  {
            var dx = dxPane.generateDxString();
            hardTissueFile.setValue(rootWin.currentTooth, dx);
            txPane.save();

            dxPane.clearInputs();
            txPane.clearValues();

            rootWin.currentTooth--;

            dataPane.updateValues();
            dxPane.parseDxString(hardTissueFile.value(currentTooth,""));
            txPane.loadTooth();
        }
    }


}
