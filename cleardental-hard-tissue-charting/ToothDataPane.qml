// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtGraphicalEffects 1.11
import QtQuick.Layouts 1.3
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTranslucentPane {
    backMaterialColor: Material.BlueGrey

    function updateValues() {
        perioValueGrid.updatePerioLabels();
    }

    GridLayout {
        flow: GridLayout.TopToBottom
        anchors.fill: parent
        Label {
            text: "Findings"
            font.pointSize: 24
            font.underline: true
        }

        Label {
            text: "Previous treatment"
            font.bold: true
            font.pointSize: 16
        }
        Label {

            CDCommonFunctions {
                id: _commonFuns
            }
            function getDateString(theDate) {
                var dd = String(theDate.getDate()).padStart(2, '0');
                var mm = String(theDate.getMonth() + 1).padStart(2, '0');//January is 0!
                var yyyy = theDate.getFullYear();

                var returnMe = mm + '/' + dd + '/' + yyyy;
                return returnMe;
            }

            Component.onCompleted: {
                var hx = _commonFuns.getToothHistory(PATIENT_FILE_NAME,rootWin.currentTooth);
                if(hx.length > 0) {
                    var makeMeText = "";
                    for(var i=0;i<hx.length;i++) {
                        makeMeText += getDateString(new Date(hx[i][0])) + " " +
                                _commonFuns.makeTxItemString(hx[i][1]) +  "\n";
                    }
                    text = makeMeText;
                }
                else {
                    text = "None"
                }

            }
        }

        ColumnLayout {
            Layout.row:2
            Layout.column: 1

            Settings {
                id: perioChartData
                fileName: fileLocs.getPerioChartFile(PATIENT_FILE_NAME)
                category: rootWin.currentTooth
            }

            Settings {
                id: reviewsData
                fileName: fileLocs.getReviewsFile(PATIENT_FILE_NAME)
            }

            CDFileLocations {
                id: fileLocs
            }

            ColumnLayout {
                id: perioHeaderRow
                Label {
                    text: "Clinical Attachment Loss"
                    font.bold: true
                    font.pointSize: 16
                }
                RowLayout {
                    Label {
                        text: "Taken:"
                        font.bold: true
                    }
                    Label {
                        Component.onCompleted: {
                            text=reviewsData.value("PerioCharting","Never")
                        }
                    }
                }
            }

            GridLayout {
                id: perioValueGrid
                Layout.minimumWidth: perioHeaderRow.width
                columns: 5
                rows: 4

                function updatePerioLabels() {
                    var buccalPockets =perioChartData.value("BuccalPockets","_,_,_");
                    if(buccalPockets !== "WNL") {
                        buccalPockets = buccalPockets.split(",");
                        var buccalRecession=perioChartData.value("BuccalRecession","_,_,_").split(",");
                        buccLeft.combineVals(buccalPockets[0],buccalRecession[0]);
                        buccCenter.combineVals(buccalPockets[1],buccalRecession[1]);
                        buccRight.combineVals(buccalPockets[2],buccalRecession[2]);
                    }
                    else {
                        buccLeft.perioVal = "😊";
                        buccCenter.perioVal = "😊";
                        buccRight.perioVal = "😊";
                    }

                    var lingualPockets =perioChartData.value("LingualPockets","_,_,_");
                    if(lingualPockets !== "WNL") {
                        lingualPockets = lingualPockets.split(",");
                        var lingualRecession=perioChartData.value("LingualRecession","_,_,_").split(",");
                        lingualLeft.combineVals(lingualPockets[0],lingualRecession[0]);
                        lingualCenter.combineVals(lingualPockets[1],lingualRecession[1]);
                        lingualRight.combineVals(lingualPockets[2],lingualRecession[2]);
                    }
                    else {
                        lingualLeft.perioVal = "😊";
                        lingualCenter.perioVal = "😊";
                        lingualRight.perioVal = "😊";
                    }
                }

                Label {
                    Layout.columnSpan: 5
                    text: "Buccal"
                    Layout.fillWidth: true
                    horizontalAlignment: Text.AlignHCenter
                }
                Label {
                    Layout.rowSpan: 2
                    Layout.fillWidth: true
                    text: "Mesial"
                    horizontalAlignment: Text.AlignRight
                }

                CDPerioLabel {
                    id: buccLeft
                }
                CDPerioLabel {
                    id: buccCenter
                }
                CDPerioLabel {
                    id: buccRight
                }

                Label {
                    Layout.rowSpan: 2
                    text: "Distal"
                    Layout.fillWidth: true
                }

                CDPerioLabel {
                    id: lingualLeft
                }
                CDPerioLabel {
                    id: lingualCenter
                }
                CDPerioLabel {
                    id: lingualRight
                }

                Label {
                    Layout.columnSpan: 5
                    text: "Lingual"
                    Layout.fillWidth: true
                    horizontalAlignment: Text.AlignHCenter
                }
            }
        }
    }


}
