// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtGraphicalEffects 1.11
import QtQuick.Layouts 1.3
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0

CDTransparentPage {
    CDTranslucentPane {
        anchors.centerIn: parent
        backMaterialColor: Material.Green
        font.pointSize: 64
        ColumnLayout {
            spacing: 64
            CDHeaderLabel {
                text: "Select Style"
            }

            Button {
                text: "Go tooth by tooth"
                onClicked: {
                    mainStack.push("ExamToothByTooth.qml")
                }
            }

            Button {
                text: "Select the tooth then chart it"
                onClicked: {
                    mainStack.push("SelectToothToChart.qml")
                }
            }
        }
    }
}


