// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtGraphicalEffects 1.11
import QtQuick.Layouts 1.3
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0

CDTranslucentPane {
    contentWidth: 350

    backMaterialColor: Material.LightGreen

    function generateDxString() {
        var dxList=[];
        if(compBox.checked) {
            var addMe = "Composite: "+compSurfaces.getSurfaces();
            dxList.push(addMe);
        }

        if(amalBox.checked) {
            var addMeAmal = "Amalgam: " + amalSurfaces.getSurfaces();
            dxList.push(addMeAmal);
        }

        if(crownBox.checked) {
            var addMeCrown = "Crown: " + crownDxRow.getCrownTypeName();
            dxList.push(addMeCrown);
        }

        if(implantCheck.checked) {
            dxList.push("Implant");
        }

        if(rctCheck.checked) {
            dxList.push("Endodontically Treated");
        }

        if(missingCheck.checked) {
            dxList.push("Missing");
        }

        if(decayBox.checked) {
            var addMeDecay = "Decay: " + decaySurfaces.getSurfaces();
            dxList.push(addMeDecay);
        }

        if(fractureBox.checked) {
            var addMeFracture = "Fracture: " + fractureSurfaces.getSurfaces();
            dxList.push(addMeFracture);
        }

        if(paAbsBox.checked) {
            dxList.push("Periapical abscess");
        }

        if(necroBox.checked) {
            dxList.push("Necrotic");
        }
        if(impactBox.checked) {
            dxList.push("Impacted");
        }

        if(moreDxDia.isPrimary) {
            dxList.push("IsPrimary");
        }
        if(moreDxDia.hasCustom) {
            dxList.push(moreDxDia.customText);
        }
        if(moreExDia.hasSealant) {
            dxList.push("Sealant");
        }

        if(moreExDia.hasCustom) {
            dxList.push(moreExDia.customText);
        }



        return commonFuns.makeHardTissueString(dxList);
    }

    function parseDxString(getDx) {
        var dxArray = commonFuns.getHardTissueAttrs(getDx);
        for(var i=0;i<dxArray.length;i++) {
            var dxString = dxArray[i].trim();
            if(dxString.startsWith("Composite")) {
                compBox.checked = true;
                var compParts = dxString.split(":");
                compSurfaces.parseSurfaces(compParts[1]);
            }
            else if(dxString.startsWith("Amalgam")) {
                amalBox.checked = true;
                var amalParts = dxString.split(":");
                amalSurfaces.parseSurfaces(amalParts[1]);
            }
            else if(dxString.startsWith("Crown")) {
                crownBox.checked = true;
                var crownParts = dxString.split(":");
                crownDxRow.setCrownTypeByName(crownParts[1]);
            }
            else if(dxString.startsWith("Implant")) {
                implantCheck.checked = true;
            }
            else if(dxString.startsWith("Endodontically Treated")) {
                rctCheck.checked = true;
            }
            else if(dxString.startsWith("Missing")) {
                missingCheck.checked = true;
            }
            else if(dxString.startsWith("Decay")) {
                decayBox.checked = true;
                var decayParts = dxString.split(":");
                decaySurfaces.parseSurfaces(decayParts[1]);
            }
            else if(dxString.startsWith("Fracture")) {
                fractureBox.checked = true;
                var fractureParts = dxString.split(":");
                fractureSurfaces.parseSurfaces(fractureParts[1]);
            }
            else if(dxString.startsWith("Periapical abscess")) {
                paAbsBox.checked = true;
            }
            else if(dxString.startsWith("Necrotic")) {
                necroBox.checked = true;
            }
            else if(dxString.startsWith("Impacted")) {
                impactBox.checked = true;
            }
            else if(dxString.startsWith("IsPrimary")) {
                moreDxDia.isPrimary = true;
            }
            else if(dxString.startsWith("Sealant")) {
                moreExDia.hasSealant = true;
            }
            else { //just assume it is custom; if there are multiple customs.... well... too bad
                moreExDia.hasCustom = true;
                moreExDia.customText = dxString;
            }
        }
    }

    function clearInputs() {
        compBox.checked = false;
        compSurfaces.clearSurfaces();
        amalBox.checked = false;
        amalSurfaces.clearSurfaces();
        crownBox.checked = false;
        implantCheck.checked = false;
        rctCheck.checked = false;
        missingCheck.checked = false;
        decayBox.checked = false;
        decaySurfaces.clearSurfaces();
        fractureBox.checked = false;
        fractureSurfaces.clearSurfaces();
        paAbsBox.checked = false;
        necroBox.checked = false;
        impactBox.checked = false;
        moreDxDia.hasCustom = false;
        moreDxDia.customText = "";
        moreDxDia.isPrimary = false;
        moreExDia.hasSealant = false;
        moreExDia.hasCustom = false;
        moreExDia.customText = "";
    }

    MoreDxDialog {
        id: moreDxDia
    }

    MoreExistingDialog {
        id: moreExDia
    }

    Column {

        CDHeaderLabel {
            text: "Existing"
        }
        ColumnLayout {
            CheckBox {
                id: compBox
                text: "Composite"

            }

            CDSurfaceRow {
                id: compSurfaces
                workingToothNumb: rootWin.currentTooth
                visible: compBox.checked
                opacity: compBox.checked  ? 1:0
            }
        }

        ColumnLayout {
            CheckBox {
                id: amalBox
                text: "Amalgam"
            }

            CDSurfaceRow {
                id: amalSurfaces
                workingToothNumb: rootWin.currentTooth
                visible: amalBox.checked
                opacity: amalBox.checked  ? 1:0
            }
        }

        ColumnLayout {
            CheckBox {
                id: crownBox
                text: "Crown"
            }
            CrownTypeRow {
                id: crownDxRow
                visible: crownBox.checked
                opacity: crownBox.checked ? 1:0
                Behavior on opacity {
                    NumberAnimation {
                        duration: 300
                    }
                }
            }
        }

        CheckBox {
            id: implantCheck
            text: "Implant"
        }
        CheckBox {
            id: rctCheck
            text: "RCT"
        }


        CheckBox {
            id: impactBox
            text: "Impacted"
        }

        Button {
            text: "More..."
            onClicked: {
                moreExDia.open();
            }
        }

        CDHeaderLabel {
            text: "Diagnosis"
        }
        CheckBox {
            id: missingCheck
            text: "Missing"
        }

        ColumnLayout {
            CheckBox {
                id: decayBox
                text: "Decay"
            }

            CDSurfaceRow {
                id: decaySurfaces
                workingToothNumb: rootWin.currentTooth
                visible: decayBox.checked
                opacity: decayBox.checked  ? 1:0
            }
        }

        ColumnLayout {
            CheckBox {
                id: fractureBox
                text: "Chip/Fracture"
            }

            CDSurfaceRow {
                id: fractureSurfaces
                workingToothNumb: rootWin.currentTooth
                visible: fractureBox.checked
                opacity: fractureBox.checked  ? 1:0
            }
        }

        CheckBox {
            id: paAbsBox
            text: "Periapical abscess"
        }

        CheckBox {
            id: necroBox
            text: "Necrotic"
        }


        Button {
            text: "More..."
            onClicked: {
                moreDxDia.open();
            }
        }

        move: Transition {
            NumberAnimation {
                properties: "x,y"
                duration: 300
            }
        }
    }
}
