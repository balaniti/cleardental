// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtGraphicalEffects 1.11
import QtQuick.Layouts 1.3
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTransparentPage {
    id: selectToothPage
    function handleToothClicked(toothNumb) {
        rootWin.currentTooth = toothNumb;
        mainStack.push("ExamFastTooth.qml");
    }

    CDFileLocations {
        id: fLocs
    }

    Settings {
        id: hardTissueReader
        fileName: fLocs.getHardTissueChartFile(PATIENT_FILE_NAME);
    }

    CDCommonFunctions {
        id: comFuns
    }


    function makeTxPlanArray(getTooth) {
        var txPlanItems = comFuns.getAllTxPlanItems(PATIENT_FILE_NAME);
        var returnMe = [];
        for(var i=0;i<txPlanItems.length;i++) {
            var txItem = txPlanItems[i];
            if("Tooth" in txItem) {
                // @disable-check M126
                if(txItem["Tooth"] == getTooth) {
                    returnMe.push(txItem);
                }
            }
        }
        return returnMe;
    }

    function makeExistingArray(getTooth) {
        var fullString = hardTissueReader.value(getTooth,"");
        return comFuns.getHardTissueAttrs(fullString);
    }


    CDTranslucentPane {
        anchors.centerIn: parent
        backMaterialColor: Material.Green
        ColumnLayout {
            CDHeaderLabel {
                text: "Select tooth:"
                font.pointSize: 72
            }
            GridLayout {
                Layout.alignment: Qt.AlignHCenter
                columns: 16
                rowSpacing: 25
                columnSpacing: 25
                Repeater {
                    id: maxButs
                    model: 16
                    Button {
                        property int toothButtonNumber: index+1
                        Layout.minimumWidth: 90
                        Layout.minimumHeight: 200
                        font.pointSize: 42

                        CDToothScene3D {
                            id: toothScene3D
                            anchors.fill: parent
                            anchors.topMargin: 10
                            toothNumb: parent.toothButtonNumber
                            existingList: makeExistingArray(parent.toothButtonNumber)
                            txPlanList: makeTxPlanArray(parent.toothButtonNumber)

                            onVisibleChanged: {
                                if(visible) {
                                    existingList= makeExistingArray(parent.toothButtonNumber)
                                    txPlanList= makeTxPlanArray(parent.toothButtonNumber)
                                    reloadInformation();
                                }
                            }
                        }

                        Label {
                            anchors.top: parent.top
                            text: parent.toothButtonNumber
                            anchors.horizontalCenter: parent.horizontalCenter
                        }

                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                handleToothClicked(toothButtonNumber);
                            }
                        }
                    }
                }
                Repeater {
                    id: manButs
                    model: 16
                    Button {
                        property int toothButtonNumber: 32-index
                        Layout.minimumWidth: 90
                        Layout.minimumHeight: 200
                        font.pointSize: 42

                        CDToothScene3D {
                            id: toothScene3DBottom
                            anchors.fill: parent
                            anchors.topMargin: 10
                            toothNumb: parent.toothButtonNumber
                            existingList: makeExistingArray(parent.toothButtonNumber)
                            txPlanList: makeTxPlanArray(parent.toothButtonNumber)

                            onVisibleChanged: {
                                if(visible) {
                                    existingList= makeExistingArray(parent.toothButtonNumber)
                                    txPlanList= makeTxPlanArray(parent.toothButtonNumber)
                                    reloadInformation();
                                }
                            }
                        }

                        Label {
                            anchors.bottom: parent.bottom
                            text: parent.toothButtonNumber
                            anchors.horizontalCenter: parent.horizontalCenter
                        }

                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                handleToothClicked(toothButtonNumber);
                            }
                        }
                    }
                }
            }
        }
    }



    CDFinishProcedureButton {
        text: "Finish Charting"
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10

        CDGitManager {
            id: gitMan
        }


        onClicked:  {
            commonFuns.updateReviewFile("HardTissueExam",PATIENT_FILE_NAME);
            gitMan.commitData("Updated Hard Tissue Exam for " + PATIENT_FILE_NAME);
            Qt.quit();
        }
    }
}


