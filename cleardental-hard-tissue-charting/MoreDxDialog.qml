// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtGraphicalEffects 1.11
import QtQuick.Layouts 1.3
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0

CDTranslucentDialog {
    id: moreDxDia

    property alias isPrimary: isPrimaryTooth.checked
    property alias hasCustom: customDxBox.checked
    property alias customText: customDxText.text

    CDTranslucentPane {
        ColumnLayout {
            CDHeaderLabel {
                text: "More Diagnosis"
            }
            CheckBox {
                id: isPrimaryTooth
                text: "Is a Primary Tooth"
            }
            RowLayout {
                CheckBox {
                    id: customDxBox
                    text: "Custom"
                }
                TextField {
                    id: customDxText
                    visible: customDxBox.checked
                }
            }


            RowLayout {
                Layout.fillWidth: true
                Label {
                    Layout.fillWidth: true
                }
                CDButton {
                    text: "Done"
                    highlighted: true
                    Material.accent: Material.Cyan
                    onClicked: {
                        moreDxDia.close();
                    }
                }
            }
        }
    }
}
