// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtGraphicalEffects 1.11
import QtQuick.Layouts 1.3
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0

CDTranslucentDialog {
    id: moreExDia

    property alias hasSealant: hasSealantBox.checked
    property alias hasCustom: customExBox.checked
    property alias customText: customExText.text

    CDTranslucentPane {
        ColumnLayout {
            CDHeaderLabel {
                text: "More Existings"
            }
            CheckBox {
                id: hasSealantBox
                text: "Sealant"
            }
            RowLayout {
                CheckBox {
                    id: customExBox
                    text: "Custom"
                }
                TextField {
                    id: customExText
                    visible: customExBox.checked
                }
            }


            RowLayout {
                Layout.fillWidth: true
                Label {
                    Layout.fillWidth: true
                }
                CDButton {
                    text: "Done"
                    highlighted: true
                    Material.accent: Material.Cyan
                    onClicked: {
                        moreExDia.close();
                    }
                }
            }
        }
    }
}
