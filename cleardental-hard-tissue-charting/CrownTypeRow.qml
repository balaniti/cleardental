// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtGraphicalEffects 1.11
import QtQuick.Layouts 1.3
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0


RowLayout {
    function getCrownTypeName() {
        return crownButGroup.checkedButton.ToolTip.text;
    }

    function setCrownTypeByName(setName) {
        setName = setName.trim();
        for(var i=0;i<crownButGroup.buttons.length;i++) {
            if(crownButGroup.buttons[i].ToolTip.text === setName) {
                crownButGroup.buttons[i].checked = true;
            }
        }
    }

    RadioButton {
        id: ss
        text: "S/S"
        ToolTip.visible: pressed
        ToolTip.text: "Stainless Steel"
    }
    RadioButton {
        id: pfm
        text: "PFM"
        ToolTip.visible: down
        ToolTip.text: "Porcelain Fused to Metal"
    }
    RadioButton {
        id: gold
        text: "Gold"
        ToolTip.visible: down
        ToolTip.text: "Full Cast Gold"
    }
    RadioButton {
        id: zir
        text: "Zir"
        ToolTip.visible: down
        ToolTip.text: "Zirconia"
        checked: true
    }

    RadioButton {
        id: ls2
        text: "LS2"
        ToolTip.visible: down
        ToolTip.text: "Lithium Disilicate"
    }

    ButtonGroup {
        id: crownButGroup
        buttons: [ss,pfm,gold,zir,ls2]
    }
}
