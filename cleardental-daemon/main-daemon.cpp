// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include <QCoreApplication>
#include <QDebug>
#include <QTimer>

#include "cdpatientreminder.h"
#include "cdfilelocations.h"
#include "cdpatientmanager.h"
#include "cdtextfilemanager.h"
#include "cdfilelocations.h"
#include "cdgitmanager.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    qDebug()<<"Starting the daemon process";

    //First update all the messages
    CDFileLocations fLocs;
    QSettings accountInfo(fLocs.getLocalPracticePreferenceFile(), QSettings::IniFormat);
    QString username = accountInfo.value("Accounts/VoipMSUsername","").toString();
    QString password = accountInfo.value("Accounts/VoipMSPassword","").toString();
    QString apiRequestString = "https://voip.ms/api/v1/rest.php?api_username="+username+
            "&api_password="+password+"&method=getSMS&type=1&all_messages=1";
    CDPatientReminder patReminder;
    patReminder.get(apiRequestString);

    // Happy birthday
    QList<QVariant> patients = CDPatientManager::getAllPatients();
    QDate today = QDate::currentDate();

    if (QDateTime::currentDateTime().time() > QTime(9,0)) {
        foreach (QVariant pat, patients) {
            QString patDOBString = pat.toMap().value(CDPatientManager::getDOBString()).toString();
            QDate patDOB = QDate::fromString(patDOBString, "M/d/yyyy");

            if (patDOB.day() == today.day() && patDOB.month() == today.month()) {
                QVariantMap patInfo = pat.toMap();
                QString patID = patInfo.value(CDPatientManager::getPatIDString()).toString();
                QSettings iniReader(CDFileLocations::getPersonalFile(patID),QSettings::IniFormat);
                iniReader.beginGroup("Preferences");
                QString name = patInfo.value(CDPatientManager::getPreferredNameString()).toString();
                if (name == "")
                    name = patInfo.value(CDPatientManager::getFirstNameString()).toString();
                QString dst = patInfo.value("CellPhone").toString();
                if (dst == "")
                    dst = patInfo.value("HomePhone").toString();


                QString message = "Greetings, " + name + "! This is Zen Family Dental wishing you a very"+
                        " Happy Birthday! 🎉🎂";
                if (iniReader.value("PreferedLanguage") == "es") {
                    message = "¡Saludos, " + name + "! ¡Este es Zen Family Dental deseándoles un"+
                            " Feliz Cumpleaños! 🎉🎂";
                } else if (iniReader.value("PreferedLanguage") == "pt") {
                    message = "Saudações, " + name + "! Aqui é a Zen Family Dental te desejando um "+
                            "Feliz Aniversário! 🎉🎂";
                }

                qDebug() << name << dst << message;

                QString msgJSON = CDTextfileManager::readFile(CDFileLocations::getPatientMessagesFile(patID));
                QJsonArray patMessages = QJsonDocument::fromJson(msgJSON.toUtf8()).array();
                QVariantMap prevText;
                QDate msgDate = today;
                bool msgSent = false;
                int msgI = patMessages.size();
                while (msgI > 0 && msgDate == today) {
                    msgI--;         // Find The most recent text they sent
                    prevText = patMessages[msgI].toObject().toVariantMap();
                    msgDate = QDate::fromString(prevText.value("Time").toString(),Qt::ISODate);
                    if (prevText.value("Contents").toString() == message)
                        msgSent = true;
                }

                if (!msgSent) {
                    QString apiBirthdayString = "https://voip.ms/api/v1/rest.php?api_username="+username+
                            "&api_password="+password+"&method=sendSMS&did=5083873733&dst="+dst+"&message="+message;
                    patReminder.get(apiBirthdayString);

                    QVariantList msgList = patMessages.toVariantList();
                    QVariantMap addMe;
                    addMe.insert("Time", QDateTime::currentDateTime());
                    addMe.insert("Sender", "Us (5083873733)");
                    addMe.insert("Reciever", patID);
                    addMe.insert("Contents", message);

                    msgList.append(addMe);
                    QString addMeJson(QJsonDocument::fromVariant(msgList).toJson(QJsonDocument::Indented));

                    CDTextfileManager::saveFile(CDFileLocations::getPatientMessagesFile(patID), addMeJson);

                }
            }
        }
        CDGitManager::commitData("Sent happy birthday messages");
    }

    QTimer *timer = new QTimer();
    QObject::connect(timer, &QTimer::timeout, &a,&QCoreApplication::quit);
    timer->start(1000*10);

    qDebug()<<"Ending the daemon process";


    return a.exec();
}
