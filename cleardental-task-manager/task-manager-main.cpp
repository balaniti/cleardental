// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickImageProvider>
#include <QFont>
#include <QIcon>
#include <QDebug>

#include "cddefaults.h"
#include "cddoctorlistmanager.h"


int main(int argc, char *argv[])
{
    CDDefaults::setAppDefaults();
    QCoreApplication::setApplicationName("ClearDental-Task-Manager");

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
    QGuiApplication app(argc, argv);
    app.setWindowIcon(CDDefaults::defaultColorIcon());

    CDDefaults::registerQMLTypes();
    qmlRegisterType<CDDoctorListManager>("dental.clear", 1, 0,"CDDoctorListManager");

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/TaskManagerMain.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    CDDefaults::enableBlurBackground();

    return app.exec();
}
