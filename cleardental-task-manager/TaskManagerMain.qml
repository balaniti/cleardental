// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Window 2.10
import QtQml.Models 2.1
import QtQuick.Controls.Material 2.15
import Qt.labs.settings 1.1
import QtGraphicalEffects 1.14
import Qt.labs.qmlmodels 1.0
import dental.clear 1.0

CDAppWindow {
    id: rootWin
    visible: true
    width: 1024
    height: 768
    title: qsTr("Clear Dental Task Manager")

    property var providerListModel: []
    property var taskModel: ({});
    property bool showCompletedTasks: false
    property bool firstTime: true

    function refreshList() {
        rootWin.providerListModel = []

        while(tabBar.contentChildren.length > 0) {
            tabBar.takeItem(0).destroy();
        }

        //First get a list of providers
        var iniFiles = providerListMan.getListOfStaffFiles();
        for(var i=0;i<iniFiles.length;i++) {
            var iniFile = iniFiles[i];
            providerReader.fileName = iniFile;
            var addMe = {
                firstName: providerReader.value("FirstName",""),
                lastName: providerReader.value("LastName",""),
                unixID: providerReader.value("UnixID","")
            };
            rootWin.providerListModel.push(addMe);
        }

        var currentUsername = providerListMan.currentProvider();

        //Now make the Tabs
        for(i=0;i<rootWin.providerListModel.length;i++) {
            var setID = rootWin.providerListModel[i].unixID;
            tabButtonComp.createObject(tabBar,{text: setID});
            if((setID === currentUsername) && (rootWin.firstTime)) {
                tabBar.currentIndex = i;
                rootWin.firstTime = false;
            }
        }

        var taskJSON = fReaderWriter.readFile(fLocs.getLocalTaskFile());
        if(taskJSON.length > 2) {
            rootWin.taskModel = JSON.parse(taskJSON);
        }
        else {
            rootWin.taskModel = {};
        }

        for(i=0;i<stackLayout.children.length;i++) {
            stackLayout.children[i].destroy();
        }

        //Now make the Pages in the stack layout
        for(i=0;i<rootWin.providerListModel.length;i++) {
            setID = rootWin.providerListModel[i].unixID;
            var taskArray = [];
            if(setID in rootWin.taskModel) {
                taskArray = rootWin.taskModel[setID];
            }
            pageComp.createObject(stackLayout,{unixID:setID,
                                      firstName: rootWin.providerListModel[i].firstName,
                                      lastName: rootWin.providerListModel[i].lastName,
                                      taskList: taskArray});
        }

    }

    CDFileLocations {
        id: fLocs
    }

    Settings {
        id: providerReader
    }

    CDTextFileManager {
        id: fReaderWriter
    }

    CDDoctorListManager {
        id: providerListMan
    }

    CDGitManager {
        id: gitMan
    }

    header: CDBlankToolBar {
        headerText: rootWin.title

        RowLayout {
            anchors.fill: parent
            ToolButton {
                id: forceRefresh
                icon.name: "view-refresh"
                icon.width: 32
                icon.height: 32

                onClicked: {
                    rootWin.refreshList();
                }
            }
            Label {
                Layout.fillWidth: true
            }
            CheckBox {
                text: "Show completed tasks"
                checked: rootWin.showCompletedTasks
                onCheckedChanged: {
                    rootWin.showCompletedTasks = checked;
                    rootWin.refreshList();
                }
            }

        }


    }

    TabBar {
        id: tabBar
        width: parent.width
    }

    StackLayout {
        id: stackLayout
        //width: parent.width
        currentIndex: tabBar.currentIndex
        anchors.top: tabBar.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
    }

    Component {
        id: tabButtonComp
        TabButton {}
    }

    Component {
        id: pageComp
        ListView {
            property string unixID: ""
            property string firstName: ""
            property string lastName: ""
            property var taskList: []
            property var modelList: []
            clip: true
            header: CDHeaderLabel {
                text: "Tasks for " + firstName + " " + lastName + " (" + unixID + ")"
            }
            model: modelList.length
            delegate: RowLayout {
                width: parent.width
                height: 50
                CDCopyLabel {
                    readOnly: true
                    text: modelList[index]["showMe"]
                    strikeout: modelList[index]["completed"]
                }
                DelayButton {
                    text: "Complete"
                    delay: 3000
                    visible: !modelList[index]["completed"]
                    onActivated: {
                        var myI = modelList[index]["taskIndex"];
                        rootWin.taskModel[unixID][myI]["completed"] = true;
                        fReaderWriter.saveFile(fLocs.getLocalTaskFile(),JSON.stringify(rootWin.taskModel,null,"\t"));
                        gitMan.commitData(unixID + " completed task of " + modelList[index]["showMe"]);
                        rootWin.refreshList();
                    }
                }
                DelayButton {
                    text: "UnComplete"
                    delay: 3000
                    visible: modelList[index]["completed"]
                    onActivated: {
                        var myI = modelList[index]["taskIndex"];
                        rootWin.taskModel[unixID][myI]["completed"] = false;
                        fReaderWriter.saveFile(fLocs.getLocalTaskFile(),JSON.stringify(rootWin.taskModel,null,"\t"));
                        gitMan.commitData(unixID + " undid task of " + modelList[index]["showMe"]);
                        rootWin.refreshList();
                    }
                }

            }

            Component.onCompleted: {
                for(var i=0;i<taskList.length;i++) {
                    var taskInfo = taskList[i]["taskInfo"];
                    var isCompleted = taskList[i]["completed"];
                    var addMe = {
                        showMe: taskInfo,
                        completed: isCompleted,
                        taskIndex: i
                    };
                    if(isCompleted) {
                        if(rootWin.showCompletedTasks) {
                            modelList.push(addMe)
                        }
                    }
                    else {
                        modelList.push(addMe);
                    }
                }
                model = modelList.length;
            }
        }
    }

    CDAddButton {
        text: "Add task"

        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        AddTaskDialog {
            id: addTaskDia
            onAccepted: {
                rootWin.refreshList();
            }
        }
        onClicked: {
            addTaskDia.open();
        }
    }



    Component.onCompleted: {
        refreshList();
    }
}
