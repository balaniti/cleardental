// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.14
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1
import QtGraphicalEffects 1.14

CDTranslucentDialog {
    id: addTaskDia

    ColumnLayout {

        CDTranslucentPane {
            GridLayout {
                columns: 2
                CDHeaderLabel {
                    Layout.columnSpan: 2
                    text: "Add Task"
                }
                CDDescLabel {
                    text: "To Whom"
                }
                ComboBox {
                    id: staffComboBox
                    Layout.minimumWidth: 360

                    textRole: "Label"
                    valueRole: "UnixID"

                    Component.onCompleted:  {
                        var makeModel = [];
                        var currentUsername = providerListMan.currentProvider();
                        var makeIndex =0;
                        for(var i=0;i<rootWin.providerListModel.length;i++) {
                            var addMe = {};
                            addMe["UnixID"] = rootWin.providerListModel[i]["unixID"];
                            if(rootWin.providerListModel[i]["unixID"] === currentUsername) {
                                addMe["Label"] = "[You] " + rootWin.providerListModel[i]["firstName"] + " " +
                                        rootWin.providerListModel[i]["lastName"] + " (" +
                                        rootWin.providerListModel[i]["unixID"] + ")";
                                makeIndex = i;
                            }
                            else {
                                addMe["Label"] = rootWin.providerListModel[i]["firstName"] + " " +
                                        rootWin.providerListModel[i]["lastName"] + " (" +
                                        rootWin.providerListModel[i]["unixID"] + ")";
                            }
                            makeModel.push(addMe);
                        }

                        model = makeModel;
                        currentIndex = makeIndex;
                    }
                }
                CDDescLabel {
                    text: "Task information"
                }
                TextField {
                    id: taskInfoField
                    Layout.minimumWidth: 360
                }
            }
        }
        RowLayout {
            CDCancelButton {
                onClicked: {
                    addTaskDia.reject();
                }
            }
            Label{Layout.fillWidth: true;}
            CDAddButton {
                text: "Add Task"
                onClicked: {
                    var selectedID = staffComboBox.currentValue;
                    var addMe = {
                        taskInfo: taskInfoField.text,
                        completed: false
                    };

                    //first check if there is any list for this person to begin with
                    if(selectedID in rootWin.taskModel) {
                        rootWin.taskModel[selectedID].push(addMe);
                    }
                    else {
                        rootWin.taskModel[selectedID] = [addMe];
                    }

                    fReaderWriter.saveFile(fLocs.getLocalTaskFile(),JSON.stringify(rootWin.taskModel,null,"\t"));
                    gitMan.commitData("Added task for " + selectedID + " of " + taskInfoField.text);

                    addTaskDia.accept();

                }
            }

        }
    }

}
