// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.0
import QtQuick.Controls.Material 2.12
import QtQuick.Controls 2.2
import dental.clear 1.0
import QtQuick.Layouts 1.10

Button {
    property var radiographEnum: CDRadiographSensor.INVALID
    property bool isSelected: false;
    text: senData.getUserStringFromType(radiographEnum).split(" ").join("\n");
    highlighted: true
    icon.width: 32
    icon.height: 32
    font.pointSize: 18
    Layout.alignment: Qt.AlignHCenter
    Material.accent: isSelected? Material.Blue :  Material.Grey

    CDRadiographSensor {
        id: senData
    }

    onClicked: {
        isSelected = !isSelected;
    }

}
