// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDTransparentPage {
    property alias radiographEnums: sensorPane.locationList
    ColumnLayout {
        anchors.centerIn: parent
        CDSensorPane {
            id: sensorPane
            Layout.alignment: Qt.AlignHCenter
        }
    }

    CDFinishProcedureButton {
        opacity: sensorPane.doneTakingRadiographs ? 1 :0
        visible: opacity > 0
        Behavior on opacity {
            PropertyAnimation {
                duration: 300
            }
        }
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10

        CDGitManager {
            id: gitMan
        }

        CDCommonFunctions {
            id: comFuns
        }

        onClicked: {
            gitMan.commitData("Added radiographs for " + PATIENT_FILE_NAME);
            var completeMe = [];
            completeMe = completeMe.concat(comFuns.findTxDCode(PATIENT_FILE_NAME,"D0210")); //FMX
            completeMe = completeMe.concat(comFuns.findTxDCode(PATIENT_FILE_NAME,"D0270")); //1BW
            completeMe = completeMe.concat(comFuns.findTxDCode(PATIENT_FILE_NAME,"D0272")); //2BW
            completeMe = completeMe.concat(comFuns.findTxDCode(PATIENT_FILE_NAME,"D0273")); //3BW
            completeMe = completeMe.concat(comFuns.findTxDCode(PATIENT_FILE_NAME,"D0274")); //4BW
            completeMe = completeMe.concat(comFuns.findTxDCode(PATIENT_FILE_NAME,"D0220")); //1stPA
            completeMe = completeMe.concat(comFuns.findTxDCode(PATIENT_FILE_NAME,"D0230")); //addPAs
            finDia.txItemsToComplete = completeMe;
            finDia.caseNoteString = "Radiographs taken";
            finDia.open();
        }
    }

    CDFinishProcedureDialog {
        id: finDia
    }



}

