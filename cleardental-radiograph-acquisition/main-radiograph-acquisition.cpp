// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickImageProvider>
#include <QFont>
#include <QIcon>
#include <QDebug>

#include "cddefaults.h"
#include "cdradiographmanager.h"
#include "cdaquireradiograph.h"

int main(int argc, char *argv[])
{
    CDDefaults::setAppDefaults();
    QCoreApplication::setApplicationName("ClearDental-Radiograph-Acquisition");

    QGuiApplication app(argc, argv);
    app.setWindowIcon(CDDefaults::defaultColorIcon());

    if(QCoreApplication::arguments().count()<2) {
        qDebug()<<"You need to give the patient's name as an argument!";
        return -32;
    }

    CDDefaults::registerQMLTypes();
    qmlRegisterType<CDRadiographManager>("dental.clear", 1, 0, "CDRadiographManager");
    qmlRegisterType<CDAquireRadiograph>("dental.clear", 1, 0, "CDRadiographSensor");

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("PATIENT_FILE_NAME",QCoreApplication::arguments().at(1));
    if(QCoreApplication::arguments().count()>2) {
        engine.rootContext()->setContextProperty("RADIOGRAPHS_TO_TAKE",QCoreApplication::arguments().at(2));
    }

    engine.load(QUrl(QStringLiteral("qrc:/MainRadiographAcquisition.qml")));

    CDDefaults::enableBlurBackground();

    return app.exec();
}
