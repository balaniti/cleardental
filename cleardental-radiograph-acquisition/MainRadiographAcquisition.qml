// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDAppWindow {
    id: rootWin
    title: "Radiograph Acquisition: [ID: " + PATIENT_FILE_NAME + "]"

    header: CDPatientToolBar {
        headerText: "Aquire Radiographs:"
        ToolButton {
            icon.name: "go-previous"
            visible: mainStack.depth > 1
            icon.width: 64
            icon.height: 64
            anchors.left: parent.left
            onClicked: mainStack.pop();
        }
    }

    StackView {
        id: mainStack
        initialItem: "StartPage.qml"
        anchors.fill: parent
    }

}
