// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Window 2.10

CDAppWindow {
    id: rootWin
    title: qsTr("Vitals [ID: " + PATIENT_FILE_NAME + "]")

    CDGitManager {
        id: gitMan
    }

    CDFileLocations {
        id: fileLocs
    }

    Settings {
        id: settingsFile
        fileName: fileLocs.getVitalsFile(PATIENT_FILE_NAME)
    }

    CDCommonFunctions {
        id: comFun
    }

    header: CDPatientToolBar {
        headerText: "Vitals:"
    }

    CDTranslucentPane {
        backMaterialColor: Material.Green
        anchors.centerIn: parent

        GridLayout {
            columns: 2
            anchors.margins: 10
            rowSpacing: 10
            columnSpacing: 10

            CDHeaderLabel {
                text: "Today's Vitals"
                Layout.columnSpan: 2
            }

            Label {
                text: "Blood Pressure"
                font.bold: true
            }
            RowLayout {
                TextField {
                    id: sysField
                    inputMethodHints: Qt.ImhDigitsOnly
                    maximumLength: 3
                    validator: IntValidator {
                        top: 999
                        bottom: 0
                    }
                    onEditingFinished: htLabel.checkBP();
                }
                Label {
                    text: "/"
                }
                TextField {
                    id: diaField
                    inputMethodHints: Qt.ImhDigitsOnly
                    maximumLength: 3
                    validator: IntValidator {
                        top: 999
                        bottom: 0
                    }
                    onEditingFinished: htLabel.checkBP();
                }

                Label {
                    id: htLabel

                    function checkBP() {
                        var sys = parseInt(sysField.text);
                        var dia = parseInt(diaField.text);

                        if((sys > 180) || (dia > 120)) {
                            htLabel.text = "Hypertensive crisis"
                        }
                        else if((sys > 140) || (dia > 90)) {
                            htLabel.text = "Stage 2 hypertension"
                        }
                        else if((sys > 130) || (dia > 80)) {
                            htLabel.text = "Stage 1 hypertension"
                        }
                        else if(sys > 120) {
                            htLabel.text = "Elevated blood pressure"
                        }
                        else {
                            htLabel.text = ""
                        }
                    }
                    color: Material.color(Material.Red)
                    Layout.fillWidth: true
                    opacity: text.length > 0 ? 1 : 0
                    Behavior on opacity {
                        PropertyAnimation {
                            duration: 300
                        }
                    }

                }
            }

            Label {
                text: "Pulse"
                font.bold: true
            }
            RowLayout {
                TextField {
                    id: pulseField
                    maximumLength: 3
                    validator: IntValidator {
                        top: 999
                        bottom: 0
                    }
                    onEditingFinished: tyInfo.checkPulse();
                    inputMethodHints: Qt.ImhDigitsOnly
                }
                Label {
                    id: tyInfo

                    function checkPulse() {
                        if(parseInt(pulseField.text)>100) {
                            tyInfo.text = "Tachycardia"
                        } else {
                            tyInfo.text = ""
                        }
                    }

                    color: Material.color(Material.Red)
                    opacity: text.length > 0 ? 1 : 0
                    Behavior on opacity {
                        PropertyAnimation {
                            duration: 300
                        }
                    }
                }
            }

            Label {
                text: "Respiratory rate"
                font.bold: true
            }
            RowLayout {
                TextField {
                    id: respField
                    inputMethodHints: Qt.ImhDigitsOnly
                }
                Label {
                    text: "breaths per minute"
                }
            }

            Label {
                text: "Body temperature"
                font.bold: true
            }
            RowLayout {
                TextField {
                    id: tempField
                    inputMethodHints: Qt.ImhDigitsOnly
                }
                Label {
                    text: "°F"
                }
                Label {
                    text: "High body temperature"
                    color: Material.color(Material.Red)
                    opacity: tempField.text >= 100 ? 1 : 0
                    Behavior on opacity {
                        PropertyAnimation {
                            duration: 300
                        }
                    }
                }
            }

            Label {
                text: "Height"
                font.bold: true
            }
            RowLayout {
                ComboBox {
                    id: ftBox
                    model: 9
                    Component.onCompleted: {
                        var fullHi = settingsFile.value("Height","");
                        if(fullHi.length > 0) {
                            var ft = fullHi.split(" ")[0];
                            currentIndex  = Number(ft.slice(0,-2));
                        }
                    }
                    onCurrentIndexChanged: bmiInfo.calcBMI();
                }
                Label {
                    text: "ft.\t"
                }

                ComboBox {
                    id: inBox
                    model: 12
                    Component.onCompleted: {
                        var fullHi = settingsFile.value("Height","");
                        if(fullHi.length > 0) {
                            var ft = fullHi.split(" ")[1];
                            currentIndex  = Number(ft.slice(0,-2));
                        }
                    }
                    onCurrentIndexChanged: bmiInfo.calcBMI();
                }
                Label {
                    text: "in."
                }
            }

            Label {
                text: "Weight"
                font.bold: true
            }
            RowLayout {
                TextField {
                    id: weightField
                    maximumLength: 3
                    validator: IntValidator {
                        top: 999
                        bottom: 0
                    }
                    inputMethodHints: Qt.ImhDigitsOnly
                    onTextChanged: bmiInfo.calcBMI();
                    Component.onCompleted:  {
                        text = settingsFile.value("Weight","");
                    }
                }
                Label {
                    text: "lb.\t"
                }
                Label {
                    id: bmiInfo

                    function calcBMI() {
                        if(weightField.length < 1) {
                            return;
                        }

                        var inches = (ftBox.currentIndex * 12) + inBox.currentIndex;
                        var insq = inches * inches;
                        var bmi = (parseInt(weightField.text) * 703) / insq;
                        bmiInfo.text = "BMI is " + Math.floor(bmi) + ": ";
                        if(bmi < 18.5) {
                            bmiInfo.text += "Underweight";
                            bmiInfo.color = "red"
                        } else if(bmi > 30) {
                            bmiInfo.text += "Obese";
                            bmiInfo.color = "red"
                        } else if(bmi > 25) {
                            bmiInfo.text += "Overweight";
                            bmiInfo.color = "red"
                        }
                        else {
                            bmiInfo.text += "Normal";
                            bmiInfo.color = "green"
                        }
                    }

                    Behavior on color {
                        ColorAnimation {
                            duration: 300
                        }
                    }
                }
            }

            Label {
                text: "Pain"
                font.bold: true
            }
            RowLayout {
                Slider {
                    id: painSlider
                    from: 1
                    to: 10
                    value: 1
                    stepSize: 1
                    Layout.minimumWidth: 300
                }
                Label {
                    text: painSlider.value
                    verticalAlignment: Text.AlignVCenter
                }
                Label {
                    text: "😊"
                    font.pointSize: 32
                    visible: painSlider.value==1
                }
                Label {
                    text: "😀"
                    font.pointSize: 32
                    visible: painSlider.value==2
                }
                Label {
                    text: "😐"
                    font.pointSize: 32
                    visible: painSlider.value> 2 && painSlider.value< 5
                }
                Label {
                    text: "😑"
                    font.pointSize: 32
                    visible: painSlider.value==5
                }
                Label {
                    text: "🙁"
                    font.pointSize: 32
                    visible: painSlider.value==6
                }
                Label {
                    text: "😟"
                    font.pointSize: 32
                    visible: painSlider.value==7
                }
                Label {
                    text: "😧"
                    font.pointSize: 32
                    visible: painSlider.value==8
                }
                Label {
                    text: "😭"
                    font.pointSize: 32
                    visible: painSlider.value>8
                }
            }
        }
    }

    CDSaveAndCloseButton {
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10

        onClicked:  {
            settingsFile.setValue("BloodPressure", sysField.text + "/" +
                                  diaField.text);
            settingsFile.setValue("Pulse", pulseField.text);
            settingsFile.setValue("RespiratoryRate",respField.text);
            settingsFile.setValue("BodyTemp",tempField.text);
            settingsFile.setValue("Height",ftBox.currentText + "ft " +
                                  inBox.currentText + "in");
            settingsFile.setValue("Weight",weightField.text);
            settingsFile.setValue("Pain",painSlider.value);
            settingsFile.sync();

            comFun.updateReviewFile("Vitals",PATIENT_FILE_NAME);


            gitMan.commitData("Updated vitals for " + PATIENT_FILE_NAME);
            Qt.quit();
        }
    }
}
