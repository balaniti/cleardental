// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

// Updated 5/21/2022 Alex Vernes

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Dialogs 1.3
import QtMultimedia 5.11
import QtQuick.Controls.Material 2.12

CDTransparentPage {
    id: photoPage

    property bool hasCandiate: false

    CDFileLocations {
        id: fileLocs
    }

    CDGitManager {
        id: gitManager
    }

    CDPatientFileManager {
        id: fileManager
    }

    CDTranslucentPane {
        anchors.centerIn: parent

        GridLayout {
            id: photoGrid
            columns: 2

            CDHeaderLabel {
                text: "Manage Patient's Photo"
                Layout.columnSpan: 2
            }

            Label {
                text: "Current Photo:";
                font.pointSize: 16
                Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
            }
            Image {
                id: currPhoto
                source: "file://" + fileLocs.getProfileImageFile(PATIENT_FILE_NAME);
                fillMode: Image.PreserveAspectFit
                Layout.maximumHeight: rootWin.height /3
                Layout.maximumWidth:  rootWin.width/3
                Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                cache: false
            }

            Button {
                text: "Set photo from image file"
                Layout.columnSpan: 2
                Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                onClicked: {
                    fileDialog.open();
                }
            }
            Button {
                text: "Take photo with webcam"
                Layout.columnSpan: 2
                Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                onClicked: {
                    photoDia.open();
                }
            }



            Label {
                text: "New Photo"
                font.pointSize: 16
                visible: hasCandiate
            }

            Image {
                id: candidateImg
                source: fileDialog.fileUrl
                Layout.maximumHeight: rootWin.height /3
                Layout.maximumWidth:  rootWin.width/3
                Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                fillMode: Image.PreserveAspectFit
                visible: hasCandiate
            }
        }

    }



    FileDialog {
        id: fileDialog
        title: "Please choose a file"
        nameFilters: [ "Image files (*.jpg *.png)", "All files (*)" ]
        folder: shortcuts.pictures
        onAccepted: {
            hasCandiate = true;
        }
    }

    property double selectedX
    property double selectedY
    property double selectedWidth

    TakePhotoDialog{
        id: photoDia
        anchors.centerIn: parent
        onAccepted: {
            candidateImg.source =  "file://" + fileLocs.getTempImageLocation()
            hasCandiate = true;
            console.debug(selector.selectedX, selector.selectedY, selector.selectedWidth)
            selectedX = selector.selectedX
            selectedY = selector.selectedY
            selectedWidth = selector.selectedWidth
            selector.destroy();
        }
    }


    CDSaveButton {
        id: saveButton
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: 10

        onClicked:  {
            if(hasCandiate) {
                fileManager.importNewProfileImage(PATIENT_FILE_NAME,candidateImg.source, selectedX, selectedY, selectedWidth);
            }


            gitManager.commitData("Updated profile photo for " + PATIENT_FILE_NAME);
            hasCandiate = false;
            currPhoto.source =""
            currPhoto.source ="file://" + fileLocs.getProfileImageFile(PATIENT_FILE_NAME)
        }
    }


    Button {
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        highlighted: true
        Material.accent: Material.Teal
        icon.name: "preflight-verifier"
        text: "Save and Exit"
        icon.width: 32
        icon.height: 32
        font.pointSize: 18
        onClicked: {
            if(hasCandiate) {
                fileManager.importNewProfileImage(PATIENT_FILE_NAME,candidateImg.source, selectedX, selectedY, selectedWidth);
            }


            gitManager.commitData("Updated profile photo for " + PATIENT_FILE_NAME);
            hasCandiate = false;
            currPhoto.source =""
            currPhoto.source ="file://" + fileLocs.getProfileImageFile(PATIENT_FILE_NAME)
            Qt.quit();
        }
    }
}
