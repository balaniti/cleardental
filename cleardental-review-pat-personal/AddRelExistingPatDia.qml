// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Controls.Material 2.12

CDTranslucentDialog {
    id: addRelExistingPatDia

    ColumnLayout {
        CDTranslucentPane {
            ColumnLayout {
                CDHeaderLabel {
                    text: "Add existing patient as relationship"
                }

                GridLayout {
                    columns: 2
                    CDDescLabel {
                        text: "Patient ID"
                    }
                    CDPatientComboBox {
                        id: patCombo
                        Layout.fillWidth: true
                    }
                    CDDescLabel {
                        text: "Relationship Type"
                    }
                    ComboBox {
                        id: relType
                        model: ["Spouse","Parent","Dependent","Sibling"]
                    }
                }
            }
        }
        RowLayout {
            CDCancelButton {
                onClicked: addRelExistingPatDia.reject();
            }

            Label {
                Layout.fillWidth: true
            }

            CDAddButton {
                text: "Add Relationship"

                CDPatientFileManager {
                    id: patFileMan
                }

                onClicked:  {
                    patFileMan.addRelationship(PATIENT_FILE_NAME,patCombo.text,relType.currentValue);
                    addRelExistingPatDia.accept();
                }
            }
        }
    }
}
