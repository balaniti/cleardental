// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

// Updated 5/21/2022 Alex Vernes

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import dental.clear 1.0

import Qt.labs.settings 1.1
import QtQuick.Controls.Material 2.12

CDTransparentPage {
    id: preferencePage

    function getDays() {
        var returnMe="";
        returnMe += (sundayBox.checked ? "Su " : "");
        returnMe += (mondayBox.checked ? "M " : "");
        returnMe += (tuesdayBox.checked ? "Tu " : "");
        returnMe += (wednesdayBox.checked ? "W " : "");
        returnMe += (thursdayBox.checked ? "Th " : "");
        returnMe += (fridayBox.checked ? "F " : "");
        returnMe += (saturdayBox.checked ? "Sa " : "");
        return returnMe;
    }

    function getHours() {
        var returnMe="";
        returnMe += (eMorningBox.checked ? "EMorning " : "");
        returnMe += (morningBox.checked ? "Morning " : "");
        returnMe += (eAfternoonBox.checked ? "EAfter " : "");
        returnMe += (lAfternoonBox.checked ? "LAfter " : "");
        returnMe += (eveningsBox.checked ? "Eve " : "");
        returnMe += (lEveningsBox.checked ? "LEve " : "");
        return returnMe;
    }

    function loadData() {
        var langValue = prefSett.value("PreferredLanguage","en");
        for(var i=0;i<langModel.count;i++) {
            if(langModel.get(i).value === langValue) {
                lang.currentIndex = i;
            }
        }

        var theDays = prefSett.value("AvailableDays");
        sundayBox.checked = (theDays.search("Su") > -1 );
        mondayBox.checked = (theDays.search("M") > -1 );
        tuesdayBox.checked = (theDays.search("Tu") > -1 );
        wednesdayBox.checked = (theDays.search("W") > -1 );
        thursdayBox.checked = (theDays.search("Th") > -1 );
        fridayBox.checked = (theDays.search("F") > -1 );
        saturdayBox.checked = (theDays.search("Sa") > -1 );

        var prefContactVal = prefSett.value("PreferredContact","");
        for(i=0;i<contactModel.count;i++) {
            if(contactModel.get(i).value === prefContactVal) {
                prefContact.currentIndex = i;
            }
        }

        var theHoursString = prefSett.value("AvailableTime","");
        var theHours = theHoursString.split(" ");
        eMorningBox.checked = (theHours.includes("EMorning") );
        morningBox.checked = (theHours.includes("Morning") );
        eAfternoonBox.checked = (theHours.includes("EAfter") );
        lAfternoonBox.checked = (theHours.includes("LAfter")  );
        eveningsBox.checked = (theHours.includes("Eve"));
        lEveningsBox.checked = (theHours.includes("LEve")  );

        var prefProvider = prefSett.value("PreferredDoctor","");
        for(i=0;i<prefDoctor.providerIDs.length;i++) {
            if(prefDoctor.providerIDs[i] === prefProvider) {
                prefDoctor.currentIndex = i;
            }
        }

        //console.log("dismissed file value: " + prefSett.value("Dismissed", false));
        dismissed.checked = JSON.parse(prefSett.value("Dismissed", false));
        //console.log("dismissed display value: " + dismissed.checked);
        doNotSchedule.checked = JSON.parse(prefSett.value("DoNotSchedule", false));

        schAlert.text = prefSett.value("ScheduleAlert","");
    }

    function saveData() {
        prefSett.setValue("AvailableDays",getDays());
        prefSett.setValue("AvailableTime",getHours());
        prefSett.setValue("PreferredContact",prefContact.currentValue);
        prefSett.setValue("PreferredDoctor",prefDoctor.providerIDs[prefDoctor.currentIndex]);
        prefSett.setValue("PreferredLanguage",lang.currentValue);
        prefSett.setValue("ScheduleAlert",schAlert.text);

        prefSett.setValue("Dismissed", dismissed.checked);
        prefSett.setValue("DoNotSchedule", doNotSchedule.checked);
        prefSett.sync();

        rootWin.updateReview();

        gitMan.commitData("Updated Personal Prefs for " + PATIENT_FILE_NAME);
    }

    Settings {
        id: prefSett
        fileName: fileLocs.getPersonalFile(PATIENT_FILE_NAME)
        category: "Preferences"
        Component.onCompleted: loadData()
    }

    CDGitManager {
        id: gitMan
    }

    CDTranslucentPane {
        anchors.centerIn: parent
        GridLayout {
            id: preferenceGrid
            columns: 2

            columnSpacing: 20

            CDHeaderLabel {
                text: "Patient's Preferences"
                Layout.columnSpan: 2
            }


            CDDescLabel {text: "Preferred Language";}
            ComboBox {
                id: lang
                editable: false
                textRole: "key"
                valueRole: "value"
                currentIndex: 0
                model: ListModel {
                    id: langModel
                    ListElement { key: "English"; value: "en" }
                    ListElement { key: "Español"; value: "es" }
                    ListElement { key: "Português"; value: "pt" }
                    ListElement { key: "Français"; value: "fr" }
                    ListElement { key: "官话"; value: "zh" }
                    //TODO: add many more languages
                }
            }

            CDDescLabel {text: "Available Days";}

            RowLayout {
                id: dayRow

                CheckBox {
                    id: sundayBox
                    text: "Sunday"
                } CheckBox {
                    id: mondayBox
                    text: "Monday"
                } CheckBox {
                    id: tuesdayBox
                    text: "Tuesday"
                } CheckBox {
                    id: wednesdayBox
                    text: "Wednesday"
                } CheckBox {
                    id: thursdayBox
                    text: "Thursday"
                } CheckBox {
                    id: fridayBox
                    text: "Friday"
                } CheckBox {
                    id: saturdayBox
                    text: "Saturday"
                }

            }

            CDDescLabel {text: "Preferred Time";}
            RowLayout {
                CheckBox {
                    id: eMorningBox
                    text: "Early Mornings (5AM-7AM)"
                }
                CheckBox {
                    id: morningBox
                    text: "Mornings (7AM-9AM)"
                }
                CheckBox {
                    id: eAfternoonBox
                    text: "Early Afternoons (9AM-1PM)"
                }
                CheckBox {
                    id: lAfternoonBox
                    text: "Late Afternoons (1PM-4PM)"
                }
                CheckBox {
                    id: eveningsBox
                    text: "Evenings (4PM-6PM)"
                }
                CheckBox {
                    id: lEveningsBox
                    text: "Late Evenings (6PM-8PM)"
                }

            }

            CDDescLabel {text: "Preferred Contact Method";}
            ComboBox {
                id: prefContact
                editable: false
                textRole: "key"
                valueRole: "value"
                Layout.minimumWidth: 250
                model: ListModel {
                    id: contactModel
                    ListElement { key: "Call Home"; value: "callHome" }
                    ListElement { key: "Call Cell"; value: "callCell" }
                    ListElement { key: "Call Work"; value: "callWork" }
                    ListElement { key: "Text Cell"; value: "textCell" }
                    ListElement { key: "E-mail Personal Addr"; value: "emailPersonalAddr" }
                    ListElement { key: "E-mail Work Addr"; value: "mailWorkAddr" }
                    ListElement { key: "Send Facebook Message"; value: "sendFacebook" }
                    ListElement { key: "Send Instagram Message"; value: "sendInstagram" }
                    ListElement { key: "Send WhatsApp Message"; value: "sendWhatsApp" }
                }
            }

            CDDescLabel {text: "Preferred Doctor";}
            CDProviderBox {
                id: prefDoctor
                Layout.minimumWidth: 250
            }

            CDDescLabel {
                text: "Schedule Alert"
            }

            RowLayout {
                TextField {
                    id: schAlert
                    Layout.minimumWidth: 520
                }

                CheckBox {
                    id: dismissed
                    text: "Hard Dismissal"
                }
                CheckBox {
                    id: doNotSchedule
                    text: "Do Not Schedule (soft dismissal)"
                }

            }
        }
    }

    CDSaveButton {
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: 10
        onClicked: {
            saveData();
            showSaveToolTip();
        }
    }

    SaveExitButton {
        onClicked: {
            saveData();
            Qt.quit();
        }
    }

}
