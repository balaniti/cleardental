// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Controls.Material 2.12

CDTranslucentDialog {
    id: addRelNewPatDia

    ColumnLayout {
        CDTranslucentPane {
            ColumnLayout {
                CDHeaderLabel {
                    text: "Add new patient with relationship"
                }

                GridLayout {
                    columns: 2
                    CDDescLabel {
                        text: "Relationship Type"
                    }
                    ComboBox {
                        id: relType
                        model: ["Spouse","Parent","Dependent","Sibling"]
                    }
                    CDDescLabel {
                        text: "First Name"
                    }
                    TextField {
                        id: fNameField
                    }
                    CDDescLabel {
                        text: "Last Name"
                    }
                    TextField {
                        id: lNameField
                    }
                    CDDescLabel {
                        text: "Date of Birth"
                    }
                    CDDatePicker {
                        id: dobPicker
                    }
                    CDDescLabel{text:"Sex";}
                    ButtonGroup { id: radioGroup }
                    Row {
                        id: sexRow

                        RadioButton {
                            id: maleRadio
                            checked: true
                            text: qsTr("Male")
                            ButtonGroup.group: radioGroup
                        }

                        RadioButton {
                            id: femaleRadio
                            text: qsTr("Female")
                            ButtonGroup.group: radioGroup
                        }

                        RadioButton {
                            id: otherRadio
                            text: "Other"
                            ButtonGroup.group: radioGroup
                        }
                    }
                    CheckBox {
                        id: sameAddrCheck
                        text: "Same Address"
                        checked: true
                        Layout.columnSpan: 2
                    }

                    CheckBox {
                        id: sameDentalPlanCheck
                        text: "Same Dental Plan"
                        checked: true
                        Layout.columnSpan: 2
                    }

                    CheckBox {
                        id: samePhoneCheck
                        text: "Same Phone Number"
                        checked: true
                        Layout.columnSpan: 2
                    }

                }
            }
        }
        RowLayout {
            CDCancelButton {
                onClicked: addRelNewPatDia.reject();
            }

            Label {
                Layout.fillWidth: true
            }

            CDAddButton {
                text: "Add Patient and Relationship"

                CDPatientFileManager {
                    id: patFileMan
                }

                onClicked:  {
                    patFileMan.addNewPatWithRelationship(PATIENT_FILE_NAME,relType.currentValue,
                                                         fNameField.text,lNameField.text,
                                                         dobPicker.getDateObj(),radioGroup.checkedButton.text,
                                                         sameAddrCheck.checked,
                                                         sameDentalPlanCheck.checked,samePhoneCheck.checked);
                    addRelNewPatDia.accept();
                }
            }
        }
    }
}
