// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

// Updated 5/21/2022 Alex Vernes

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Controls.Material 2.12

CDTransparentPage {
    id: namePage

    function loadData() {
        firstName.text = nameSett.value("FirstName");
        middleName.text = nameSett.value("MiddleName","");
        lastName.text = nameSett.value("LastName","");
        namePhonetic.text = nameSett.value("NamePhonetic","");
        preferredName.text = nameSett.value("PreferredName","");
    }

    function saveData() {
        nameSett.setValue("FirstName",firstName.text);
        nameSett.setValue("MiddleName",middleName.text);
        nameSett.setValue("LastName",lastName.text);
        nameSett.setValue("NamePhonetic",namePhonetic.text);
        nameSett.setValue("PreferredName",preferredName.text);
        nameSett.sync();

        rootWin.updateReview();

        gitMan.commitData("Updated Patient's Name (formerly known as " + PATIENT_FILE_NAME + ")");
    }

    Settings {
        id: nameSett
        fileName: fileLocs.getPersonalFile(PATIENT_FILE_NAME)
        category: "Name"
        Component.onCompleted: loadData()
    }

    CDFileLocations {
        id: fileLocs
    }

    CDGitManager {
        id: gitMan
    }

    CDTranslucentPane {
        anchors.centerIn: parent

        GridLayout {
            id: nameGrid
            columns: 2
            anchors.centerIn: parent
            columnSpacing: 20

            CDHeaderLabel {
                text: "Patient's Name"
                Layout.columnSpan: 2
            }

            Label {text: "First Name";font.bold: true}
            CDCopyLabel {
                id:firstName; //Kimi no Na wa?
                placeholderText: "Enter First Name"
                Layout.minimumWidth: 250
            }

            Label {text: "Middle Name";font.bold: true}
            CDCopyLabel{id:middleName;
                placeholderText: "Enter middle Name"
                Layout.minimumWidth: 250
            }

            Label {text: "Last Name";font.bold: true}
            CDCopyLabel{id:lastName;
                placeholderText: "Enter last Name"
                Layout.minimumWidth: 250
            }

            Label {text: "Preferred Name";font.bold: true}
            CDCopyLabel{id:preferredName;
                placeholderText: "What name does the patient prefer"
                Layout.minimumWidth: 250
            }

            Label {text: "How to phonetically say the same";font.bold: true}
            CDCopyLabel{id:namePhonetic;
                placeholderText: "How to phonetically say the name"
                Layout.minimumWidth: 250
            }
        }
    }

    CDSaveButton {
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: 10
        onClicked: {
            saveData();
            showSaveToolTip();
        }
    }

    SaveExitButton {
        onClicked: {
            saveData();
            Qt.quit();
        }
    }
}
