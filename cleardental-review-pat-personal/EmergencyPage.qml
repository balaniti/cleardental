// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

// Updated 5/21/2022 Alex Vernes

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1
import QtQuick.Controls.Material 2.12

CDTransparentPage {
    id: emergencyPage

    function loadData() {
        emergencyName.text = emerSett.value("EmergencyName","");
        emergencyPhone.text = emerSett.value("EmergencyPhone","");
        emergencyRelation.text = emerSett.value("EmergencyRelation","");
    }

    function saveData() {
        emerSett.setValue("EmergencyName",emergencyName.text);
        emerSett.setValue("EmergencyPhone",emergencyPhone.text);
        emerSett.setValue("EmergencyRelation",emergencyRelation.text);
        emerSett.sync();

        rootWin.updateReview();

        gitMan.commitData("Updated Emergency Contact for " + PATIENT_FILE_NAME);
    }

    Settings {
        id: emerSett
        fileName: fileLocs.getPersonalFile(PATIENT_FILE_NAME)
        category: "Emergency"
        Component.onCompleted: loadData()
    }

    CDFileLocations {
        id: fileLocs
    }

    CDGitManager {
        id: gitMan
    }

    CDTranslucentPane {
        anchors.centerIn: parent

        GridLayout {
            id: emergencyGrid
            columns: 2
            columnSpacing: 20

            CDHeaderLabel {
                text: "Patient's Emergency Contacts"
                Layout.columnSpan: 2
            }

            Label {text: "Emergency Contact's Name";font.bold: true}
            CDCopyLabel {
                id:emergencyName;
                placeholderText: "Emergency Contact's Name"
                Layout.fillWidth: true
            }

            Label {text: "Emergency Contact's Phone";font.bold: true}
            CDCopyLabel {
                id:emergencyPhone;
                placeholderText: "Emergency Contact's Phone"
                inputMethodHints: Qt.ImhDialableCharactersOnly
                Layout.fillWidth: true
            }

            Label {text: "Relationship with patient";font.bold: true}
            CDCopyLabel {
                id:emergencyRelation;
                placeholderText: "Relationship with patient"
                Layout.fillWidth: true
            }
        }
    }

    CDSaveButton {
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: 10
        onClicked: {
            saveData();
            showSaveToolTip();
        }
    }

    SaveExitButton {
        onClicked: {
            saveData();
            Qt.quit();
        }
    }

}
