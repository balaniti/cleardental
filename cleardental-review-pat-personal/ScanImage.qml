// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import dental.clear 1.0
import Qt.labs.settings 1.1

CDTransparentPage {
    id: addrPage


    CDScanner {
        id: scanner
    }

    CDFileLocations {
        id: filelocs
    }

    CDTranslucentDialog {
        id: newScanImageDia

        function makeANewScan() {
            candiateImg.source = "";
            scanImgButton.visible = false;
            var scanFile =  scanner.scanCard(twoCardCheckBox.checked);
            candiateImg.source = "file://" + scanFile;
        }

        ColumnLayout {
            CDTranslucentPane {
                ColumnLayout {
                    CDHeaderLabel {
                        text: "Scan new card(s)"
                        Layout.columnSpan: 2
                    }
                    CheckBox {
                        id: twoCardCheckBox
                        text: "Two cards (driver's license + insurance ID)";
                        checked: true
                    }
                    CDAddButton {
                        id: scanImgButton
                        text: "Scan a new Image"
                        Layout.columnSpan: 2
                        icon.name: "document-scan"
                        onClicked: newScanImageDia.makeANewScan();
                    }

                    RowLayout {
                        visible: candiateImg.status == Image.Ready
                        CDDescLabel {
                            text: "Scanned image"
                        }
                        Image {
                            id: candiateImg
                            fillMode: Image.PreserveAspectFit
                            Layout.maximumWidth: 256
                            Layout.maximumHeight: 512
                        }
                    }

                    RowLayout {
                        id: acceptRejectRow
                        Layout.fillWidth: true
                        visible: candiateImg.status == Image.Ready
                        CDCancelButton {
                            text:"No good, try again..."
                            onClicked: newScanImageDia.makeANewScan();
                        }
                        Label {
                            Layout.fillWidth: true
                        }

                        CDSaveButton {
                            text:"Looks good! Use it!"
                            onClicked: {
                                scanner.saveCard(PATIENT_FILE_NAME);
                                newScanImageDia.close();
                                curCardImage.source = "file://" + filelocs.getPatientCardFile(PATIENT_FILE_NAME);
                            }
                        }
                    }
                }
            }

            RowLayout {
                CDCancelButton {
                    onClicked: newScanImageDia.close();
                }
            }
        }
    }

    CDTranslucentPane {
        anchors.centerIn: parent

        GridLayout {
            id: scanCardGrid
            columns: 2
            anchors.centerIn: parent
            columnSpacing: 20

            CDHeaderLabel {
                text: "Patient's Card"
                Layout.columnSpan: 2
            }

            CDDescLabel {
                text: "Current Image"
            }

            Image {
                id: curCardImage
                source: "file://" + filelocs.getPatientCardFile(PATIENT_FILE_NAME);
                visible: status == Image.Ready
                Layout.maximumWidth: 256
                Layout.maximumHeight: 512
                fillMode: Image.PreserveAspectFit
            }

            Label {
                text: "No scanned image at this point"
                visible:  curCardImage.status != Image.Ready
            }

            CDCancelButton {
                text: "Scan a new Image"
                Layout.columnSpan: 2
                icon.name: "document-scan"
                onClicked: newScanImageDia.open();

            }

        }
    }
}
