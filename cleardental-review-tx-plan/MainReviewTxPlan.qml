// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDAppWindow {
    id: rootWin
    title: "Review Treatment Plan [ID: " + PATIENT_FILE_NAME + "]"

    property var treatmentPlansObj: ({})
    property string selectedTxPlan: ""
    property string fakeTxPlanName: "🌷🌹🌷" //for force refresh
    property var phasePanels: []
    property var dispPanels1: []
    property var dispPanels2: []
    property int paneWidth: 350;
    property int dispPaneWidth: 245;
    property int paneMargin: 20;
    property int currentPhaseCount: 0
    property var insInfo: ({})
    property var phaseCalculations: ({})
    onSelectedTxPlanChanged: {
        generatePhasePanels();
    }

    CDBillingEstimator {
        id: billEst
    }

    header: CDPatientToolBar {
        headerText: "Review Treatment Plan"
    }

    CDFileLocations {
        id: fLocs
    }

    CDTextFileManager {
        id: textMan
    }

    CDGitManager {
        id: gitMan
    }

    CDCommonFunctions {
        id: comFuns
    }

    CDPrinter {
        id: printer
    }

    Settings {
        id: insInfoSet
        fileName: fLocs.getDentalPlansFile(PATIENT_FILE_NAME);
    }

    CDConstLoader {
        id: constLoader

        Component.onCompleted: {
            //First case scenario: cash
            insInfoSet.category = "Primary";
            var patInsName =  insInfoSet.value("Name","");
            if(patInsName === "") {
                rootWin.insInfo = {
                    "Name": "Cash"
                }
                return;
            }

            //Second case: local membership plan
            insInfoSet.category = "Membership"
            if(!JSON.parse(insInfoSet.value("HasMembership",false))) {
                rootWin.insInfo = {
                    "Name": "🦷Membership Plan" //Unicode to prevent any dental plans with a similar name
                }
                return;
            }

            //It has a name
            var allPlans = JSON.parse(constLoader.getUSDentalPlanListJSON())
            for(var i=0;i<allPlans.length;i++) {
                var planName = allPlans[i]["PlanName"]
                if(patInsName.startsWith(planName)) {
                    rootWin.insInfo = allPlans[i];
                }
            }
        }
    }

    Component {
        id: phaseComp
        PhasePane {

        }
    }

    Component {
        id: phaseDisp
        PhasePaneDisplay {

        }
    }

    function preCalcTotals() {
        phaseCalculations = billEst.calculatePatientPortions(
                    PATIENT_FILE_NAME,
                    rootWin.treatmentPlansObj[rootWin.selectedTxPlan]);
    }

    function calculatePhaseTotal(getPhaseName) {
        var txPlanList = phaseCalculations[getPhaseName];
        var returnMe=0;
        for(var i=0;i<txPlanList.length;i++) {
            returnMe += txPlanList[i]["PatientPortion"];
        }

        return returnMe;
    }

    function loadTxPlanFile() {
        var txPlansJSON = textMan.readFile(fLocs.getTreatmentPlanFile(PATIENT_FILE_NAME));
        if(txPlansJSON.length > 0) {
            rootWin.treatmentPlansObj = JSON.parse(txPlansJSON);
        }
        else {
            var makeObj = ({}); //this is to ensure that the other elements pick up on a new object update
            comFuns.ensurePrimaryAndUnphased(makeObj);
            rootWin.treatmentPlansObj = makeObj;
        }

        selectedTxPlan = Object.keys(rootWin.treatmentPlansObj)[0];
        generatePhasePanels();
    }

    function findTxByName(getName)  {
        var phaseList = treatmentPlansObj[selectedTxPlan]["Unphased"];
        var returnMe = [];
        for(var i=0;i<phaseList.length;i++) {
            if(phaseList[i]["ProcedureName"] === getName) {
                returnMe.push(phaseList[i]);
            }
        }
        return returnMe;
    }

    function moveToPhase(txList, newPhase) {
        if(!(newPhase in treatmentPlansObj[selectedTxPlan])) {
            treatmentPlansObj[selectedTxPlan][newPhase] = [];
        }
        for(var i=0;i<txList.length;i++) {
            moveTx(txList[i],"Unphased",newPhase);
        }
    }

    function phaseSortProcedures() {
        ///Phase 1: Prophy and/or SCRP
        var fullSCRP = findTxByName("SCRP (4+ Teeth)");
        var smallSCRP = findTxByName("SCRP (<3 Teeth)");
        var debride = findTxByName("Full Mouth Debridement");
        var prophies = findTxByName("Prophy");
        if((fullSCRP.length > 0 ) || (smallSCRP.length > 0)) {
            moveToPhase(fullSCRP,"1");
            moveToPhase(smallSCRP,"1");
            moveToPhase(prophies,"5");
        }
        else if(debride.length > 0) {
            moveToPhase(debride,"1");
            moveToPhase(prophies,"5");
        }
        else {
            moveToPhase(prophies,"1");
        }

        //Also Phase 1: Extractions (don't @ me!)
        moveToPhase(findTxByName("Extraction"),"1");

        ///Phase 2: Fillings
        moveToPhase(findTxByName("Amalgam"),"2");
        moveToPhase(findTxByName("Composite"),"2");

        //Phase 3: Fixed single teeth
        moveToPhase(findTxByName("Root Canal Treatment"),"3");
        moveToPhase(findTxByName("Post and Core"),"3");
        moveToPhase(findTxByName("Crown"),"3");
        moveToPhase(findTxByName("Pontic Crown"),"3");
        moveToPhase(findTxByName("Abutment Crown"),"3");

        //Phase 4: Implants and/or removable
        moveToPhase(findTxByName("Implant Placement"),"4");
        moveToPhase(findTxByName("Complete Denture"),"4");
        moveToPhase(findTxByName("Partial Denture"),"4");

        //Phase 5: Recall Stuff
        moveToPhase(findTxByName("Reline"),"5");
        moveToPhase(findTxByName("Recare Exam"),"5");

        generatePhasePanels();
    }

    function removeLastPhase() {
        delete treatmentPlansObj[selectedTxPlan][currentPhaseCount-1];
        generatePhasePanels();
    }

    function moveTx(txItem, oldPhase, newPhase) {
        var txPlanObj = treatmentPlansObj[selectedTxPlan];
        var oldPhaseList = txPlanObj[oldPhase];
        var newPhaseList = txPlanObj[newPhase];
        if(typeof newPhaseList === "undefined") {
            return;
        }

        var oldIndex = oldPhaseList.indexOf(txItem);
        oldPhaseList.splice(oldIndex,1);
        newPhaseList.push(txItem);
    }

    function handleDrop(draggedItem) {
        var legitX = draggedItem.x + (draggedItem.width / 2);
        //console.debug(legitX);
        if(draggedItem.oldPhase === "Unphased") {
            var phaseNumber = legitX / (paneWidth + paneMargin);
            phaseNumber = parseInt(phaseNumber);
            if(phaseNumber > 0) {
                moveTx(draggedItem.txObj,"Unphased",phaseNumber);
            }
        }
        else {
            var prevPhaseNumber = parseInt(draggedItem.oldPhase);
            var newPhaseNumber = legitX / (paneWidth + paneMargin);
            newPhaseNumber = parseInt(newPhaseNumber);
            if(newPhaseNumber === 0) {
                newPhaseNumber = "Unphased";
            }

            moveTx(draggedItem.txObj,prevPhaseNumber,newPhaseNumber);
        }
        generatePhasePanels();
    }

    function generatePhasePanels() {
        //first delete any existing ones
        while(phasePanels.length > 0) {
            phasePanels.pop().destroy();
            dispPanels1.pop().destroy();
            dispPanels2.pop().destroy();
        }

        //Now pre-calculate the phase totals before we load up the panes
        preCalcTotals();

        //First do the unphased
        var unphaseList = treatmentPlansObj[selectedTxPlan]["Unphased"];
        var addUnPh = phaseComp.createObject(phaseFlick.contentItem, {
                                                 phaseName: "Unphased"
                                             });
        addUnPh.width = paneWidth
        addUnPh.height = phaseFlick.height-20
        addUnPh.x = 0;

        var addUnPh2 = phaseDisp.createObject(secWin.contentItem, {
                                                 phaseName: "Unphased"
                                             });
        addUnPh2.width = dispPaneWidth
        addUnPh2.height = phaseFlick.height-20
        addUnPh2.x = 10;
        addUnPh2.y = 50;

        var addUnPh3 = phaseDisp.createObject(thirdWin.contentItem, {
                                                 phaseName: "Unphased"
                                             });
        addUnPh3.width = dispPaneWidth
        addUnPh3.height = phaseFlick.height-20
        addUnPh3.x = 10;
        addUnPh3.y = 50;

        phasePanels.push(addUnPh);
        dispPanels1.push(addUnPh2);
        dispPanels2.push(addUnPh3);

        //Now go through the other phases
        var phaseNames = Object.keys(treatmentPlansObj[selectedTxPlan]);
        for(var pi=0;pi<phaseNames.length-1;pi++) {
            var setphaseName = phaseNames[pi];
            var addComp = phaseComp.createObject(phaseFlick.contentItem, {
                                                     phaseName: setphaseName
                                                 });
            addComp.width = paneWidth
            addComp.height = phaseFlick.height-20
            addComp.x = (pi+1) * (paneWidth+paneMargin);

            var addComp2 = phaseDisp.createObject(secWin.contentItem, {
                                                     phaseName: setphaseName
                                                 });
            addComp2.width = dispPaneWidth
            addComp2.height = phaseFlick.height-20
            addComp2.x = (pi+1) * (dispPaneWidth+paneMargin) + 10;
            addComp2.y = 50;

            var addComp3 = phaseDisp.createObject(thirdWin.contentItem, {
                                                     phaseName: setphaseName
                                                 });
            addComp3.width = dispPaneWidth
            addComp3.height = phaseFlick.height-20
            addComp3.x = (pi+1) * (dispPaneWidth+paneMargin) + 10;
            addComp3.y = 50;

            phasePanels.push(addComp);
            dispPanels1.push(addComp2);
            dispPanels2.push(addComp3);
        }
        currentPhaseCount = phaseNames.length;
    }

    TxPlanSelectRow {
        id: txPlanSelectRow
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins: 20
        anchors.top: parent.top
    }

    Flickable {
        id: phaseFlick
        anchors.top: txPlanSelectRow.bottom
        anchors.bottom: saveButton.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 10
        contentWidth:(currentPhaseCount*(paneWidth+paneMargin)) - paneMargin
        contentHeight: height
        ScrollBar.horizontal: ScrollBar { }
    }

    CDConstLoader {
        id: constsL;
    }

    ApplicationWindow {
        id: secWin
        x: 0
        y: 0
        width: 1920
        height: 1080
        visible: (Qt.application.screens.length > 2) && (constsL.getNodeType() === "touch")
        flags:{
            if(constsL.getNodeType() === "touch") {
                return Qt.WA_TranslucentBackground | Qt.FramelessWindowHint;
            }
            return Qt.WA_TranslucentBackground;
        }
        font.family: "Barlow Semi Condensed"
        font.pointSize: 12

        color: "transparent"

        background: Rectangle {
            color: "white"
            opacity: constsL.goTransparent() ? .5 : 1
        }
    }

    ApplicationWindow {
        id: thirdWin
        x: 0
        y: 1080
        width: 1920
        height: 1080
        visible: (Qt.application.screens.length > 2) && (constsL.getNodeType() === "touch")
        flags:{
            if(constsL.getNodeType() === "touch") {
                return Qt.WA_TranslucentBackground | Qt.FramelessWindowHint;
            }
            return Qt.WA_TranslucentBackground;
        }
        font.family: "Barlow Semi Condensed"
        font.pointSize: 12

        color: "transparent"

        background: Rectangle {
            color: "white"
            opacity: constsL.goTransparent() ? .5 : 1
        }
    }

    CDSaveAndCloseButton {
        id: saveButton
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        onClicked: {
            var txPlansJSON = JSON.stringify(rootWin.treatmentPlansObj,null,'\t');
            textMan.saveFile(fLocs.getTreatmentPlanFile(PATIENT_FILE_NAME),txPlansJSON);
            gitMan.commitData("Updated treatment plan for " + PATIENT_FILE_NAME);
            Qt.quit();
        }
    }

    CDCancelButton {
        icon.name: "document-print"
        text: "Print Treatment Plan"
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins: 10
        onClicked: {
            printer.printTreatmentPlan(PATIENT_FILE_NAME,treatmentPlansObj[selectedTxPlan])
        }
    }

    ManualAddTxDia {
        id: manualAddTxDia
        anchors.centerIn: parent
    }

    Component.onCompleted: {
        loadTxPlanFile();
    }

}
