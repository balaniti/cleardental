// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.14
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1
import QtGraphicalEffects 1.14

CDTranslucentDialog {
    id: editTxDia

    ColumnLayout {
        id: colLay
        Layout.fillWidth: true
        CDTranslucentPane {
            Layout.fillWidth: true
            GridLayout {
                id: gridLay
                columns: 2
                CDHeaderLabel {
                    Layout.columnSpan: 2
                    text: "Edit Treatment"
                }
            }
        }

        RowLayout {
            Layout.columnSpan: 2
            Layout.fillWidth: true

            CDCancelButton {
                onClicked: {
                    editTxDia.close();
                }
            }

            Label {
                Layout.fillWidth: true
            }

            CDDeleteButton {
                text: "Delete Treatment"
                onClicked: {
                    rootWin.treatmentPlansObj[rootWin.selectedTxPlan][phaseName].splice(index,1);
                    rootWin.generatePhasePanels();
                    editTxDia.close();

                }
            }

            CDAddButton {
                icon.name: "dialog-ok"
                text: "Complete Procedure"
                onClicked: {
                    finProcedureDia.txItemsToComplete = [txItem.txObj];
                    finProcedureDia.open();
                }

                CDFinishProcedureDialog {
                    id: finProcedureDia
                    dieOnClose: false

                    onAccepted: {
                        rootWin.loadTxPlanFile();
                    }
                }
            }


            Label {
                Layout.fillWidth: true
            }

            CDSaveButton {
                text: "Save Changes"
                onClicked: {
                    for(var i=1;i<gridLay.children.length;i+=2) {
                        var key = gridLay.children[i].text;
                        var value = gridLay.children[i+1].text;
                        txItem.txObj[key] = value;
                    }

                    rootWin.generatePhasePanels();

                    editTxDia.close()
                }
            }
        }
    }


    Component {
        id: boldLabel
        Label {
            font.bold: true
        }
    }

    Component {
        id: textInput
        TextField {
            Layout.minimumWidth: 350
        }
    }

    Component.onCompleted: {
        var keys = Object.keys(txItem.txObj);
        for(var i=0;i<keys.length;i++) {
            boldLabel.createObject(gridLay,{text: keys[i]});
            textInput.createObject(gridLay,{text:txItem.txObj[keys[i]]});
        }
    }
    
}
