// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.14
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1
import QtGraphicalEffects 1.14

Item {
    id: txItem
    property string oldPhase: phaseName
    property var txObj: rootWin.treatmentPlansObj[rootWin.selectedTxPlan][phaseName][index]

    function generateLabelText() {
        var returnMe = txObj["ProcedureName"];
        if("Tooth" in txObj) {
            returnMe += " #" + txObj["Tooth"]
        }
        else if("Location" in txObj) {
            returnMe += " " + txObj["Location"]

        }
        return returnMe;
    }

    y: (75 * index)
    height: 65
    x: 0
    width: 300
    z: 1024

    Rectangle {
        anchors.fill: parent
        color: Material.color(Material.Cyan)
        opacity: .5
        radius: 10
    }


    RowLayout {
        anchors.fill: parent
        anchors.margins: 10
        Label {
            Layout.maximumWidth: 200
            elide: Text.ElideRight
            text: generateLabelText()
        }
        Label {
            Layout.fillWidth: true
        }

        Button {
            icon.name: "document-edit"
            Layout.maximumWidth: 64
            onClicked: {
                editTxDia.open();
            }
        }
    }
    
    DragHandler {
        id: dragHandle
        property bool wasActive: false
        onActiveChanged: {
            if(!active) {
                rootWin.handleDrop(parent)
            }
        }
    }
    
    Drag.active: dragHandle.active
    Drag.hotSpot.x: width/2
    Drag.hotSpot.y: height/2
    parent:  dragHandle.active ? phaseFlick.contentItem :
                                 txItemsFlickable.contentItem

    EditTxDia {
        id: editTxDia
        anchors.centerIn: Overlay.overlay
    }
}
