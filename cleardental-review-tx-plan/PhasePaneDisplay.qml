// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.14
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1
import QtGraphicalEffects 1.14

CDTranslucentPane {
    id: phasePane
    backMaterialColor: Material.LightBlue

    property string phaseName: ""


    Label {
        id: headerLabel
        text: (phaseName === "Unphased") ? phaseName :
                                           "Phase " + phaseName
        font.pointSize: 24
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.margins: 10
    }

    Button {
        highlighted: true
        anchors.right: parent.right
        anchors.top: parent.top
        Material.accent: Material.Red
        icon.name: "list-remove"
        width: 32
        onClicked: {
            rootWin.removeLastPhase();
        }

        visible: (repItems.count === 0) &&
                 (phaseName === (rootWin.currentPhaseCount -1).toString() )
    }

    ColumnLayout {
        id: txItemsFlickable
        anchors.top: headerLabel.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 0
        anchors.topMargin: 10
        clip: true

        Repeater {
            id: repItems
            model: rootWin.treatmentPlansObj[rootWin.selectedTxPlan][phaseName].length
                TxItemDisplay {
                    id: txItemDisp
                }
        }

    }

    CDDescLabel {
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        Component.onCompleted: {
            var total = rootWin.calculatePhaseTotal(phaseName)
            text = "Phase total: $" + total;
        }
    }

}
