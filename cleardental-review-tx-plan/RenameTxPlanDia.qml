// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDTranslucentDialog {
    id: renameTxPlanDia
    property int teatPlanIndex: 0
    property string oldName:""
    
    function openWithNameAndIndex(name,index) {
        oldName = name;
        newNameField.text = name;
        teatPlanIndex = index;
        open();
    }

    ColumnLayout {
        CDTranslucentPane {
            GridLayout {
                columns: 2
                CDHeaderLabel {
                    text: "Rename treatment plan"
                    Layout.columnSpan: 2
                }
                CDDescLabel {
                    text: "New Name"
                }
                TextField {
                    id: newNameField
                }
            }
        }
        RowLayout {
            CDCancelButton {
                onClicked: {
                    reject();
                }
            }
            Label {Layout.fillWidth: true}
            CDSaveAndCloseButton {
                text: "Rename"
                Material.accent: Material.Cyan
                onClicked: {
                    rootWin.treatmentPlansObj[newNameField.text] = rootWin.treatmentPlansObj[oldName];
                    delete rootWin.treatmentPlansObj[oldName];
                    selectedTxPlan = newNameField.text;
                    accept();
                }
            }
        }
    }
}
