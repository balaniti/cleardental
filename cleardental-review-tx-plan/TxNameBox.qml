// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.14
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1

ComboBox {

    CDConstLoader {
        id: constLoader
    }

    valueRole: "DCode"
    textRole: "ProcedureName"

    editable: true


    Component.onCompleted: {
        var jsonText = constLoader.getDCodesJSON();
        var jsonObj = JSON.parse(jsonText);
        model = jsonObj;
    }

}
