// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDAppWindow {
    id: rootWin
    title: qsTr("Rx [ID: " + PATIENT_FILE_NAME + "]")

    header: CDPatientToolBar {
        headerText: "Prescription:"
    }

    CDPrinter {
        id: printer
    }
    CDFileLocations {
        id: fLocs
    }

    Settings {
        id: personalInfo
        fileName: fLocs.getPersonalFile(PATIENT_FILE_NAME)
    }

    CDTextFileManager {
        id: fileMan
    }

    RowLayout {
        anchors.centerIn: parent

        MakeRxPane {
            id: rxPane
        }

        ColumnLayout {
            id: rightCol
            CDMedReviewPane {
                id: medPlane
                Layout.minimumWidth: medPlane.buttonWidth
            }

            CDTranslucentPane {
                id: previousRxPane
                backMaterialColor: Material.Blue
                Layout.fillWidth: true
                ColumnLayout {
                    Layout.fillWidth: true
                    CDHeaderLabel {
                        id: prevHeader
                        text: "Previous Prescriptions"
                    }

                    PrevRxList {
                        id: prevRxList
                        height: 240
                    }
                }
            }
        }
    }

    CDButton {
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        text: "Save and Send Rx"
        highlighted: true
        Material.background: Material.Cyan
        icon.name: "document-send"
        CDGitManager {id: gitManager}

        onClicked: {
            var currentMedList = fileMan.readFile(fLocs.getMedicationsFile(PATIENT_FILE_NAME));
            var jsonList;
            if(currentMedList.length === 0) {
                jsonList = [];
            } else {
                jsonList = JSON.parse(currentMedList);
            }

            var addMe = rxPane.makeObj();
            jsonList.push(addMe);
            var jsonString= JSON.stringify(jsonList, null, '\t');
            fileMan.saveFile(fLocs.getMedicationsFile(PATIENT_FILE_NAME),jsonString);


            if(rxPane.printMe) {
                //Already saved JSON, so we can do anything to the object
                //before we sent it to the printer
                personalInfo.category = "Name"
                addMe["FirstName"] = personalInfo.value("FirstName","");
                addMe["LastName"] = personalInfo.value("LastName","");
                personalInfo.category = "Personal"
                addMe["DOB"] = personalInfo.value("DateOfBirth","");
                addMe["PatID"] = PATIENT_FILE_NAME;
                printer.printRx(addMe);
            }
            gitManager.commitData("Rx made for " + addMe["DrugName"] + " for " + PATIENT_FILE_NAME);

            Qt.quit();
        }
    }

}
