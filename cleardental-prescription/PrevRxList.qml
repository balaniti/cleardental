// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

ListView {
    id: prevRxList
    Layout.minimumWidth: medPlane.buttonWidth - 25
    property var setModel: [];
    model: setModel.length

    clip: true
    ScrollBar.vertical: ScrollBar{id: prevRxScBr}

    function loadPrevRx() {
        setModel = [];

        var currentMedList = fileMan.readFile(fLocs.getMedicationsFile(PATIENT_FILE_NAME));
        if(currentMedList.length === 0) {
            setModel = [];
        } else {
            setModel = JSON.parse(currentMedList);
        }
        model = setModel.length
    }


    delegate:  RowLayout {
        width: parent.width - prevRxScBr.width
        Label {
            text: prevRxList.setModel[index].DrugName
            Layout.minimumWidth: 320
        }
        Label {
            text:{
                if(prevRxList.setModel[index].PrescribedHere) {
                    return prevRxList.setModel[index].PrescriptionDate
                }
                else {
                    return "Outside Rx"
                }
            }

            Layout.minimumWidth: 320
        }
        Button {
            text: "Reprint Rx"
            enabled: prevRxList.setModel[index].PrescribedHere
            onClicked: {
                personalInfo.category = "Name"
                prevRxList.setModel[index]["FirstName"] = personalInfo.value("FirstName","");
                prevRxList.setModel[index]["LastName"] = personalInfo.value("LastName","");
                personalInfo.category = "Personal"
                prevRxList.setModel[index]["DOB"] = personalInfo.value("DateOfBirth","");
                prevRxList.setModel[index]["PatID"] = PATIENT_FILE_NAME;
                printer.printRx(prevRxList.setModel[index]);
            }
        }
    }

    Component.onCompleted: {
        loadPrevRx();
    }
}
