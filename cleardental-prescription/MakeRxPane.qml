// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import QtQuick.Controls.Material 2.12
import dental.clear 1.0
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDTranslucentPane {
    id: makeRxPane
    backMaterialColor: Material.Green

    property alias printMe: printRx.checked

    function makeObj() {
        var returnMe = {"DrugName": drugBox.editText,
            "PrescriptionDate" : dateRow.getDate(),
            "UsedFor": reasonfield.text,
            "PrescribedHere": true,
            "Strength": strBox.editText,
            "Amount": amountBox.value,
            "Form" : formBox.editText,
            "Schedule": scheduleBox.editText,
            "Days" : rxDayBox.value,
            "Refills" : noRefills.checked ?
                            "None" : refillAmountBox.value,
            "Instructions" : instructField.text,
            "AllowGeneric" : allowGeneric.checked,
            "DispenseTotal" : totalDispBox.value
        };
        return returnMe;
    }

    function autogenDisp() {
        var perDay;
        switch(scheduleBox.currentIndex) {
        case 0: //QD (daily)
        case 4: //QHS (before bed)
        case 9: //PRN (as needed)
        case 12: //IM (intramuscularly)
        case 13: //IV (intravenously)
        case 14: //QTT (drops)
            perDay = 1;
            break;
        case 1: //BID (twice a day)
            perDay = 2;
            break;
        case 2: //TID (three times a day)
        case 7: //Q8H (every 8 hours)
        case 10: //AC (before meals)
        case 11: //PC (after meals)
            perDay = 3;
            break;
        case 3: //QID (four times a day)
        case 6: //Q6H (every 6 hours)
            perDay = 4;
            break;
        case 5: //Q4H (every 4 hours)
            perDay = 6;
            break;
        case 8: //QOD (every other day)
            perDay = 0.5;
            break;
        }
        totalDispBox.value = perDay * rxDayBox.value;
    }

    GridLayout {
        columns: 2
        columnSpacing: 10

        CDHeaderLabel {
            text: "New Prescription"
            Layout.columnSpan: 2
        }

        Label {
            text: "Rx Date"
            font.bold: true
        }

        CDDatePicker {
            id: dateRow
        }

        Label {
            text: "Drug"
            font.bold: true
        }
        DrugBox {
            id: drugBox
            Layout.minimumWidth: 350
            Layout.fillWidth: true
        }
        Label {
            text: "Reason"
            font.bold: true
        }
        TextField {
            id: reasonfield
            Layout.fillWidth: true
        }

        Label {
            text: "Strength"
            font.bold: true
        }
        RowLayout {
            ComboBox {
                id: strBox
                model: defaultModel
                editable: true
                ListModel {
                    id: defaultModel
                    ListElement { text: "50 mg" }
                    ListElement { text: "150 mg" }
                    ListElement { text: "250 mg" }
                    ListElement { text: "300 mg" }
                    ListElement { text: "350 mg" }
                    ListElement { text: "500 mg" }
                    ListElement { text: "750 mg" }
                    ListElement { text: "1000 mg" }
                }
                currentIndex: 5
                Layout.minimumWidth: 200
            }

            CDDescLabel {
                id: kidWarningLabel
                color: Material.color(Material.Yellow)

                CDCommonFunctions {
                    id: m_ComFuns
                }

                CDFileLocations {
                    id: m_locs
                }

                Settings {
                    id: vitalsData
                    fileName: m_locs.getVitalsFile(PATIENT_FILE_NAME)
                }

                function updateText() {
                    text = ""; //by default
                    var age = m_ComFuns.getYearsOld(PATIENT_FILE_NAME);
                    if(age < 13) {
                        var pounds = vitalsData.value("Weight",-1);
                        if(pounds > 0) {
                            var kg = pounds / 2.20462;
                            var getItem = drugBox.model.get(drugBox.currentIndex);
                            if(scheduleBox.currentIndex == 1) { //BID
                                if(getItem["pediatric_BID_Low"] > 0) {
                                    var lowmg = Math.round( getItem["pediatric_BID_Low"] * kg / 2.0);
                                    var highmg = Math.round(Math.min(getItem["pediatric_BID_High"] * kg / 2,
                                                          getItem["pediatric_BID_Max"] * 1.0));
                                    text = "Recommended: " + lowmg + " - " + highmg + " mg";
                                }
                            }
                            else if(scheduleBox.currentIndex == 0) { //QD
                                if(getItem["pediatric_QD_Low"] > 0) {
                                    lowmg = Math.round(getItem["pediatric_QD_Low"] * kg / 2.0);
                                    highmg = Math.round( Math.min(getItem["pediatric_QD_High"] * kg / 2.0,
                                                          getItem["pediatric_QD_Max"] * 1.0));
                                    text = "Recommended: " + lowmg + " - " + highmg + " mg";
                                }
                            }
                            else if(scheduleBox.currentIndex == 3) { //QID
                                if(getItem["pediatric_QID_Low"] > 0) {
                                    lowmg = Math.round(getItem["pediatric_QID_Low"] * kg / 2.0);
                                    highmg = Math.round( Math.min(getItem["pediatric_QID_High"] * kg / 2.0,
                                                          getItem["pediatric_QID_Max"] * 1.0));
                                    text = "Recommended: " + lowmg + " - " + highmg + " mg";
                                }
                            }
                        }
                    }
                }
            }

        }



        Label {
            text: "Amount"
            font.bold: true
        }
        SpinBox {
            id: amountBox
            from: 1
            to: 99
            value: 1
        }

        Label {
            text: "Form"
            font.bold: true
        }
        ComboBox {
            id: formBox
            model: ["tablet", //0
                "solution",   //1
                "cream"]      //2
            editable: true
            currentIndex: 0
        }



        Label {
            text: "Schedule"
            font.bold: true
        }
        ComboBox {
            id: scheduleBox
            model: ["QD",  //0
                "BID",     //1
                "TID",     //2
                "QID",     //3
                "QHS",     //4
                "Q4H",     //5
                "Q6H",     //6
                "Q8H",     //7
                "QOD",     //8
                "PRN",     //9
                "AC",      //10
                "PC",      //11
                "IM",      //12
                "IV",      //13
                "QTT",     //14
                "As directed"] //15
            editable: true

            onCurrentIndexChanged: {
                autogenDisp();
                kidWarningLabel.updateText();
            }

        }

        Label {
            text: "Days"
            font.bold: true
        }
        SpinBox {
            id: rxDayBox
            from: 0
            to: 365
            value: 1
            editable: true
            textFromValue: function(value) {
                if(value===0)  {
                    return "N/A"
                }
                else {
                    return value;
                }
            }

            onValueChanged: {
                autogenDisp();
            }
        }

        CDDescLabel {
            text: "Dispense Total:"
        }

        SpinBox {
            id: totalDispBox
            from: 1
            to: 100
            editable: true
        }

        Label {
            text: "Refills"
            font.bold: true
        }
        RowLayout {
            CheckBox {
                id: noRefills
                text: "None"
                checked: true
            }
            Label {
                text: "Number of refills: "
                opacity: noRefills.checked ? 0 : 1
                Behavior on opacity {
                    PropertyAnimation {
                        duration: 300
                    }
                }
            }
            SpinBox {
                id: refillAmountBox
                from: 1
                to: 99
                opacity: noRefills.checked ? 0 : 1
                Behavior on opacity {
                    PropertyAnimation {
                        duration: 300
                    }
                }
            }
        }

        Label {
            text: "Instructions"
            font.bold: true
        }
        TextField {
            id: instructField
            Layout.fillWidth: true
        }


        CheckBox {
            id: allowGeneric
            text: "Allow Generic"
            checked: true
            Layout.columnSpan: 2
        }

        CheckBox {
            id: printRx
            Layout.columnSpan: 2
            text: "Print copy of Rx here"
            checked: true
        }
    }
}
