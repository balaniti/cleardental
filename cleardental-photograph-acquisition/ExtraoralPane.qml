// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.12
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1
import QtMultimedia 5.12
import QtQuick.Dialogs 1.2

CDTransparentPage {

    GridLayout {
        id: gridOfPics
        anchors.centerIn: parent
        columns: 6
        rowSpacing: 20
        columnSpacing: 20
        property alias fileDialog : fDia

        property var pictureNames: ["Full Face Rest","Full Face Smile",
            "Lateral Full Face Rest", "Lateral Full Face Smile",
            "Smile Lateral Left", "Smile Lateral Right",
            "Front Maxillary Teeth","Front Mandibular Teeth",
            "Right Lateral Closed Teeth", "Right Lateral Open Teeth",
            "Left Lateral Closed Teeth", "Left Lateral Open Teeth",
            "Maxillary Occlusal", "Mandibular Occlusal", "Retracted Open",
            "Retracted Closed"]

        CDPatientFileManager {
            id: patFMan
        }

        CDGitManager {
            id: gitMan
        }

        FileDialog {
            id: fDia
            property int setIndex:0

            onAccepted: {
                console.debug(fLocs.getFGTPDir(PATIENT_FILE_NAME) +gridOfPics.pictureNames[setIndex] +".jpg");
                patFMan.copyAbsFile(fDia.fileUrl,
                                    fLocs.getFGTPDir(PATIENT_FILE_NAME) + gridOfPics.pictureNames[setIndex] + ".jpg")

                gitMan.commitData("Added extraoral picture for " + PATIENT_FILE_NAME);
                repPanel.model = 0;
                repPanel.model = gridOfPics.pictureNames.length;
            }
        }

        Repeater {
            id: repPanel
            model: gridOfPics.pictureNames.length
            CDTranslucentPane {
                Layout.minimumWidth: 256
                Layout.minimumHeight: 256
                backMaterialColor: Material.Grey

                CDFileLocations {
                    id: fLocs
                }

                Image {
                    source: "file://" + fLocs.getFGTPDir(PATIENT_FILE_NAME) +gridOfPics.pictureNames[index] +".jpg"
                    anchors.fill: parent
                    fillMode: Image.PreserveAspectFit

                }

                Label {
                    anchors.fill: parent
                    anchors.margins: 10
                    text: gridOfPics.pictureNames[index]
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignBottom

                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        gridOfPics.fileDialog.setIndex = index;
                        gridOfPics.fileDialog.open();
                    }
                }
            }
        }
    }
}
