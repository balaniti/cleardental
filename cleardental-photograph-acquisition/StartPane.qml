// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.12
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import Qt.labs.settings 1.1

CDTransparentPage {
    id: startPane

    CDTranslucentPane {
        anchors.centerIn: parent
        ColumnLayout {
            spacing: 25
            CDHeaderLabel {
                text: "What kind of camera?"
            }
            Button {
                text: "Intraoral Camera"
                onClicked: {
                    mainStack.push("IntraoralPane.qml")
                }
            }
            Button {
                text: "Extraoral Camera"
                onClicked: {
                    mainStack.push("ExtraoralPane.qml")
                }
            }
        }

    }


}
