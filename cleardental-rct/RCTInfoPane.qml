// Copyright 2024 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.10
import dental.clear 1.0
import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.1

CDTranslucentPane {

    function generateCaseNoteString() {
        var returnMe ="";

        returnMe += "Isolation: " + isoBox.currentText + "\n";
        returnMe+= "Canals Found: " + foundCanals.value + "\n";

        for(var i=0;i<foundCanals.value;i++) {
            var rowl =canalReps.itemAt(i);
            returnMe += "Working length of " + rowl.children[1].currentText +
                    " canal was " + rowl.children[2].value + "mm\n";
        }

        returnMe+= "Irrigated using " + irrigationbox.currentValue + "\n";
        returnMe += rotaryBox.currentValue + " used for rotary\n";
        returnMe += "Obturated using " + obturationBox.currentValue + "\n";
        returnMe+= "Backfilled using " + backfillBox.currentValue + "\n";
        if(finishedRCTCheckBox.checked) {
            returnMe += "Root canal treatment for tooth was completed\n";
        }

        return returnMe;
    }

    ColumnLayout {
        CDHeaderLabel {
            text: "Root Canal Treatment Info"
        }

        RowLayout {
            CDDescLabel {
                text: "Canals Found"
            }
            SpinBox {
                id: foundCanals
                from: 0
                to: 10
                value: 2
            }
        }


        Repeater {
            id: canalReps
            model: foundCanals.value
            RowLayout {
                CDDescLabel {
                    text: "Working length"
                }
                ComboBox {
                    model: ["Buccal", "Lingual", "Mesial Buccal", "Distal Buccal", "Mesial Lingual", "Distal Lingual"]
                    editable: true
                    Layout.minimumWidth: 150
                    currentIndex: index
                }

                SpinBox {
                    from: 4
                    to: 50
                    value: 10

                    textFromValue: function(value) {
                        return value + " mm";
                    }
                }
            }
        }

        GridLayout {
            columns: 2
            CDDescLabel {
                text: "Isolation"
            }
            ComboBox {
                id: isoBox
                editable: true
                Layout.fillWidth: true
                Component.onCompleted: {
                    var defIso = "Rubber Dam";
                    var startModel =  ["Rubber Dam", "Cotton", "Isolite"];
                    if(startModel.indexOf(defIso) === -1) {
                        startModel.push(defIso);
                    }
                    model = startModel;
                    currentIndex = find(defIso)
                }
            }

            CDDescLabel {
                text: "Irrigation"
            }
            ComboBox {
                id: irrigationbox
                model: ["Sodium hypochlorite", "EDTA", "Chlorhexidine digluconate" , "None"]
                Layout.fillWidth: true
            }

            CDDescLabel {
                text: "Rotary used"
            }

            ComboBox {
                id: rotaryBox
                model: ["ProFile", "WaveOne®", "EdgeFile®", "ProTaper", "None"]
                Layout.fillWidth: true
            }
            CDDescLabel {
                text: "Obturation"
            }

            ComboBox {
                id: obturationBox
                Layout.fillWidth: true
                model: ["Gutta-percha", "Guttacore", "Not Done"]
            }
            CDDescLabel {
                text: "Backfill"
            }

            ComboBox {
                id: backfillBox
                model: ["Gutta-percha", "Guttacore"]
                Layout.fillWidth: true
            }
        }

        CheckBox {
            id: finishedRCTCheckBox
            text: "Finished RCT for tooth"
        }
    }

}
